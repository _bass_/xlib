import os
import sys
import argparse
import hashlib
from Crypto.Cipher import AES


fwDesc = {
	'version': "",
	'imgId': 0,
	'data': bytes(),
	'dataEnc': bytes(),
	'keyEnc': bytes(),
	'ivEnc': bytes(),
	'keySign': bytes(),
	'metadata': bytes(),
	'typeEnc': "",
	'typeSign': "",
	'tagData' : bytes(),
	'tagDataEnc' : bytes()
}

# see TCryptType
cryptTypes = ['none', 'aes128-cbc']

# see TTagType
tagTypes = ['sha256']

# see TSignType
signTypes = ['sha256-aes128cbc']


def cmdGenKey (args):
	# FIXME: get key size for current encryption type
	keySize = 16
	key = os.urandom(keySize)
	
	with open(args.filename, 'wb') as f:
		f.write(key)


def cmdPack (args):
	fwDesc['version'] = args.version
	fwDesc['imgId'] = args.id
	fwDesc['typeEnc'] = args.type_enc
	fwDesc['typeTag'] = args.type_tag
	fwDesc['typeSign'] = args.type_sign

	with open(args.ekey, 'rb') as f:
		fwDesc['keyEnc'] = f.read()
	if (len(fwDesc['keyEnc']) == 0):
		print('Wrong encryption key file')
		return

	with open(args.skey, 'rb') as f:
		fwDesc['keySign'] = f.read()
	if (len(fwDesc['keySign']) == 0):
		print('Wrong signature key file')
		return

	with open(args.infile, 'rb') as f:
		fwDesc['data'] = f.read()
	if (len(fwDesc['data']) == 0):
		print('Wrong source file')
		return

	isSuccess = encode()
	if (not isSuccess):
		print('Encryption error')
		return

	calcDataTags()
	fillMetadata()
	saveImage(args.outfile)


def fillMetadata ():
	fwDesc['metadata'] = bytes()

	# Prefix (magic)
	fwDesc['metadata'] += "SIMG".encode('ascii')

	# metaVersion
	fwDesc['metadata'] += b'\x00'

	# imgId
	fwDesc['metadata'] += fwDesc['imgId'].to_bytes(1, 'big')

	# reserved
	fwDesc['metadata'] += b'\x00'
	fwDesc['metadata'] += b'\x00'

	# typeCrypt
	fwDesc['metadata'] += cryptTypes.index(fwDesc['typeEnc']).to_bytes(1, 'big')

	# typeTag
	fwDesc['metadata'] += tagTypes.index(fwDesc['typeTag']).to_bytes(1, 'big')

	# reserved
	fwDesc['metadata'] += b'\x00'
	fwDesc['metadata'] += b'\x00'

	# imgSize
	fwDesc['metadata'] += len(fwDesc['dataEnc']).to_bytes(4, 'little')

	# dataSize
	fwDesc['metadata'] += len(fwDesc['data']).to_bytes(4, 'little')

	# version
	v = fwDesc['version'].split('.')
	fwDesc['metadata'] += int(v[0]).to_bytes(1, 'big')
	fwDesc['metadata'] += int(v[1]).to_bytes(1, 'big')
	fwDesc['metadata'] += int(v[2]).to_bytes(1, 'big')
	fwDesc['metadata'] += b'\x00'

	# crypt::params [32 bytes]
	# crypt::aes
	# FIXME: different type processing
	fwDesc['metadata'] += fwDesc['ivEnc']
	fwDesc['metadata'] += bytes([0 for i in range(len(fwDesc['ivEnc']), 32)])

	# tagImg
	fwDesc['metadata'] += fwDesc['tagDataEnc']

	# tagData
	fwDesc['metadata'] += fwDesc['tagData']

	# typeSign
	fwDesc['metadata'] += signTypes.index(fwDesc['typeSign']).to_bytes(1, 'big')

	# reserved
	fwDesc['metadata'] += b'\x00'
	fwDesc['metadata'] += b'\x00'
	fwDesc['metadata'] += b'\x00'

	# meta::sign [64 bytes]
	# meta::sign::sha256aes
	# FIXME: different type processing
	iv = os.urandom(16)
	cipher = AES.new(fwDesc['keyEnc'], AES.MODE_CBC, iv=iv)

	fwDesc['metadata'] += iv
	fwDesc['metadata'] += bytes([0 for i in range(16)])

	hasher = hashlib.sha256()
	hasher.update(fwDesc['metadata'])
	hash = hasher.digest()
	sign = cipher.encrypt(hash)

	fwDesc['metadata'] += sign


def encode ():
	fwDesc['dataEnc'] = fwDesc['data']
	srcSize = len(fwDesc['data'])
	remain = srcSize % AES.block_size

	if (remain):
		fwDesc['dataEnc'] += bytes([0xff for i in range(remain, AES.block_size)])

	# FIXME: use Encryption type parameter

	cipher = AES.new(fwDesc['keyEnc'], AES.MODE_CBC)
	fwDesc['dataEnc'] = cipher.encrypt(fwDesc['dataEnc'])
	fwDesc['ivEnc'] = cipher.iv

	return True


def calcDataTags():
	# Data tag
	hasher = hashlib.sha256()
	hasher.update(fwDesc['data'])
	fwDesc['tagData'] = hasher.digest()

	# Encrypted data tag
	hasher = hashlib.sha256()
	hasher.update(fwDesc['dataEnc'])
	fwDesc['tagDataEnc'] = hasher.digest()


def saveImage (fname: str):
	with open(fname, 'wb') as f:
		f.write(fwDesc['metadata'])
		f.write(fwDesc['dataEnc'])


def createArgParser ():
	obj = argparse.ArgumentParser()
	parser = obj.add_subparsers(dest='command')

	cmd = parser.add_parser('genkey', help='Generate keys data')
	cmd.add_argument('-t', '--type', metavar='type', required=True, help='Key type')
	cmd.add_argument('filename', help='Output file name')

	cmd = parser.add_parser('pack', help='Create update package file')
	cmd.add_argument('--type_enc', required=True, help='Encryption type', choices=cryptTypes)
	cmd.add_argument('--type_tag', required=True, help='Data tag type', choices=tagTypes)
	cmd.add_argument('--type_sign', required=True, help='Signature type', choices=signTypes)
	cmd.add_argument('-v', '--version', metavar='version', required=True, help='Firmware image version')
	cmd.add_argument('--ekey', required=True, help='Encryption key')
	cmd.add_argument('--skey', required=True, help='Signing key')
	cmd.add_argument('--id', required=True, type=int, help='Image ID')
	cmd.add_argument('infile', help='Firwmare source data file name')
	cmd.add_argument('outfile', help='Update package file name')

	return obj


def runCmd ():
	parser = createArgParser()
	args = parser.parse_args()
	if (args.command == None):
		parser.print_help()
		sys.exit(1)
	commands[args.command](args)


commands = {
	'keygen': cmdGenKey,
	'pack': cmdPack
}


if __name__ == '__main__':
	runCmd()
