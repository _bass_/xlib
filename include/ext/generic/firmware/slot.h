//*****************************************************************************
//
// @brief   Firmware slot
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/block.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Firmware slot descriptor
    typedef struct
    {
        CDevBlock*  pDev;           // Block device driver
        uint        blockStart;     // Slot offset in blocks
        uint        blocksCount;    // Slot size in blocks
    } TFwSlot;
