//*****************************************************************************
//
// @brief   Update image helper interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "firmware/imghelper.h"
#include "crypt/ihash.h"
#include "crypt/icrypt.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Image magic number (start of metadata, "SIMG")
    #define SIMG_META_MAGIC             ( (uint32)'S' | 'I' << 8 | 'M' << 16 | 'G' << 24 )


// ----------------------------------------------------------------------------
// CImgHelperSimg
// ----------------------------------------------------------------------------

class CImgHelperSimg : public IImgHelper
{
    public:
        /**
          * @brief  Callback function to read encryptrion key
          * @param  buff: Buffer to save key data
          * @param  size: Buffer size
          * @param  params: Additional parameters
          * @return Key data size written in the buffer
          */
        typedef int (*TCbGetKey) (void* buff, uint size, void* params);

        /**
          * @brief  Constructor
          * @param  getKeyCallback: Function to read encryption key data
          */
        CImgHelperSimg (TCbGetKey getKeyCallback);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CImgHelperSimg ();

        /**
          * @brief  Reset helper's state
          * @param  None
          */
        void Reset (void);

        /**
          * @brief  Metadata validation
          * @param  data: Pointer to metadata
          * @param  size: Metadata size
          * @return      > 0 - missing amout of the metadata
          *              0 - metadata is valid
          *              < 0 - corrupted metadata
          */
        int CheckMetadata (const void* data, uint size);

        /**
          * @brief  Image data validation
          * @param  data: Pointer to image data / part of data
          * @param  size: Data size
          * @param  pMetadata: Pointer to valid image metadata for validation
          * @return          > 0 - missing amount of image data
          *                  0 - data block is valid
          *                  < 0 - corrupted data block
          */
        int CheckDataImg (const void* data, uint size, const void* pMetadata);

        /**
          * @brief  Decoded image data validation
          * @param  data: Pointer to decoded image data / part of data
          * @param  size: Data size
          * @param  pMetadata: Pointer to valid image metadata
          * @return          > 0 - missing amount of data for validation
          *                  0 - data block is valid
          *                  < 0 - corrupted data block
          */
        int CheckData (const void* data, uint size, const void* pMetadata);

        /**
          * @brief  Image data decoding
          * @param  src: Pointer to encoded image data / part of data
          * @param  srcSize: Source data size
          * @param  dst: Buffer for decoded data
          * @param  pDstSize: Pointer to buffer size. Actual data size will be stored there.
          * @param  pMetadata: Pointer to valid image metadata
          * @return          > 0 - Required amount of data to complete image decoding
          *                  0 - Successful decoding
          *                  < 0 - Error code
          */
        int DecodeImg (const void* src, uint srcSize, void* dst, uint* pDstSize, const void* pMetadata);

        /**
          * @brief  Get image ID
          * @param  pMetadata: Pointer to valid image metadata
          * @return Image ID or negative error code
          */
        int GetImgId (const void* pMetadata);

        /**
          * @brief  Get image size (encoded)
          * @param  pMetadata: Pointer to valid image metadata
          * @return Image size or 0 on error
          */
        uint GetDataSizeImg (const void* pMetadata);

        /**
          * @brief  Get decoded image size
          * @param  pMetadata: Pointer to valid image metadata
          * @return Decoded image size or 0 on error
          */
        uint GetDataSize (const void* pMetadata);

        /**
          * @brief  Get image version
          * @param  pMetadata: Pointer to valid image metadata
          * @return Version number or 0 on error
          */
        uint32 GetDataVersion (const void* pMetadata);

        /**
          * @brief  Get metadata offset in a slot
          * @return Metadata offset (bytes) from slot begining
          */
        uint32 GetBlockMetadataOffset (void);

        /**
          * @brief  Get data block offset in a slot
          * @return Data offset (bytes) from slot begining
          */
        uint32 GetBlockDataOffset (void);


        // Metadata structure
        #pragma pack(1)
        typedef struct
        {
            uint32      magic;
            uint8       metaVersion;
            uint8       imgId;
            char        reserved[2];

            struct
            {
                uint8   typeCrypt;  // TCryptType
                uint8   typeTag;    // TTagType
                char    reserved[2];
                uint32  imgSize;
                uint32  dataSize;
                uint32  version;

                union
                {
                    char    params[32];
                    struct
                    {
                        char    iv[16];
                    } aes;
                } crypt;

                char        tagImg[32];
                char        tagData[32];
            } data;

            struct
            {
                uint8   typeSign;   // TSignType
                char    reserved[3];
                union
                {
                    // Signature data is aligned to the end of the block
                    char    data[64];
                    struct
                    {
                        char    iv[16];
                        char    padding[16];
                        char    value[32];
                    } sha256aes;
                } sign;
            } meta;

        } TMetadata;
        #pragma pack()

    private:
        const TCbGetKey m_cbGetKey;
        uint            m_processedSize;
        IHash*          m_hasher;
        ICrypt*         m_coder;

        /**
          * @brief  Metadata integrity check by signature verification
          * @param  pMeta: Pointer to image metadata
          * @return 0 or negative error code
          */
        int verifyMetaSign (const TMetadata* pMeta);

        /**
          * @brief  Metadata tag calculation
          * @param  pMeta: Pointer to image metadata
          * @param  buff: Buffer to save tag value
          * @param  size: Buffer size
          * @return Tag data written in the buffer or negative error code
          */
        int calcMetaTag (const TMetadata* pMeta, void* buff, uint size);

        /**
          * @brief  Metadata tag verification
          * @param  pMeta: Pointer to image metadata
          * @param  tag: Tag data
          * @param  size: Tag data size
          * @return 0 (tag is valid) or negative error code
          */
        int checkMetaTag (const TMetadata* pMeta, const void* tag, uint size);

        /**
          * @brief  Data tag verification
          * @param  data: Pointer to decoded image data (part of data)
          * @param  size: Data size
          * @param  totalSize: Total image size
          * @param  tag: Data tag
          * @param  tagDataSize: Data tag size
          * @return 0 - Tag is valid
          *         > 0 - Number of processed bytes
          *         < 0 - Error code
          */
        int verifyDataTag (const void* data, uint size, uint totalSize, const void* tag, uint tagDataSize);
};
