//*****************************************************************************
//
// @brief   Update image helper interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// IImgHelper
// ----------------------------------------------------------------------------

class IImgHelper
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        IImgHelper () {}

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~IImgHelper () {}

        /**
          * @brief  Reset helper's state
          * @param  None
          */
        virtual void Reset (void) = 0;

        /**
          * @brief  Metadata validation
          * @param  data: Pointer to metadata
          * @param  size: Metadata size
          * @return      > 0 - missing amout of the metadata
          *              0 - metadata is valid
          *              < 0 - corrupted metadata
          */
        virtual int CheckMetadata (const void* data, uint size) = 0;

        /**
          * @brief  Image data validation
          * @param  data: Pointer to image data / part of data
          * @param  size: Data size
          * @param  pMetadata: Pointer to valid image metadata for validation
          * @return          > 0 - missing amount of image data
          *                  0 - data block is valid
          *                  < 0 - corrupted data block
          */
        virtual int CheckDataImg (const void* data, uint size, const void* pMetadata) = 0;

        /**
          * @brief  Decoded image data validation
          * @param  data: Pointer to decoded image data / part of data
          * @param  size: Data size
          * @param  pMetadata: Pointer to valid image metadata
          * @return          > 0 - missing amount of data for validation
          *                  0 - data block is valid
          *                  < 0 - corrupted data block
          */
        virtual int CheckData (const void* data, uint size, const void* pMetadata) = 0;

        /**
          * @brief  Image data decoding
          * @param  src: Pointer to encoded image data / part of data
          * @param  srcSize: Source data size
          * @param  dst: Buffer for decoded data
          * @param  pDstSize: Pointer to buffer size. Actual data size will be stored there.
          * @param  pMetadata: Pointer to valid image metadata
          * @return          > 0 - Required amount of data to complete image decoding
          *                  0 - Successful decoding
          *                  < 0 - Error code
          */
        virtual int DecodeImg (const void* src, uint srcSize, void* dst, uint* pDstSize, const void* pMetadata) = 0;

        /**
          * @brief  Get image ID
          * @param  pMetadata: Pointer to valid image metadata
          * @return Image ID or negative error code
          */
        virtual int GetImgId (const void* pMetadata) = 0;

        /**
          * @brief  Get image size (encoded)
          * @param  pMetadata: Pointer to valid image metadata
          * @return Image size or 0 on error
          */
        virtual uint GetDataSizeImg (const void* pMetadata) = 0;

        /**
          * @brief  Get decoded image size
          * @param  pMetadata: Pointer to valid image metadata
          * @return Decoded image size or 0 on error
          */
        virtual uint GetDataSize (const void* pMetadata) = 0;

        /**
          * @brief  Get image version
          * @param  pMetadata: Pointer to valid image metadata
          * @return Version number or 0 on error
          */
        virtual uint32 GetDataVersion (const void* pMetadata) = 0;

        /**
          * @brief  Get metadata offset in a slot
          * @return Metadata offset (bytes) from slot begining
          */
        virtual uint32 GetBlockMetadataOffset (void) = 0;

        /**
          * @brief  Get data block offset in a slot
          * @return Data offset (bytes) from slot begining
          */
        virtual uint32 GetBlockDataOffset (void) = 0;
};
