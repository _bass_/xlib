//*****************************************************************************
//
// @brief   Firmware loader base functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kthread.h"
#include "firmware/slot.h"
#include "firmware/imghelper.h"
#include "firmware/fwloader.h"


// ----------------------------------------------------------------------------
// CFwUpdateLoader
// ----------------------------------------------------------------------------

class CFwUpdateLoader
{
    public:
        /**
          * @brief  Constructor
          * @param  slotList: Slot descriptors list
          * @param  slotsLen: Number of items in the list
          * @param  loaders: Loaders list
          * @param  loadersLen: Number of items in the list
          * @param  pHelper: Pointer to image helper object
          */
        CFwUpdateLoader (const TFwSlot* slotList, uint slotsLen, CFwLoader* loaders, uint loadersLen, IImgHelper* pHelper);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CFwUpdateLoader ();

        /**
          * @brief  Load image with timeout
          * @param  timeout: Loading timeout (ms)
          * @return Loaded images map (according to position in slot list)
          */
        uint Load (uint timeout);

    private:
        const TFwSlot*  m_pSlotList;
        const uint      m_slotListLen;
        CFwLoader*      m_pLoadersList;
        const uint      m_loadersListLen;
        IImgHelper*     m_pHelper;

        /**
          * @brief  Image loading
          * @param  pLoader: Loader
          * @param  timeout: Loading timeout (ms)
          * @return ID of loaded image or negative error code
          */
        int load (CFwLoader* pLoader, uint32 timeout);
};
