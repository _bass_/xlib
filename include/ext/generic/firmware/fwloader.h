//*****************************************************************************
//
// @brief   Update image loader interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// CFwLoader
// ----------------------------------------------------------------------------

class CFwLoader
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CFwLoader () {};

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CFwLoader () {};

        /**
          * @brief  Loader initialization
          * @param  None
          * @return 0 or negative error code
          */
        virtual int Open () = 0;

        /**
          * @brief  Loader deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        virtual int Close () = 0;

        /**
          * @brief  Image loading
          * @param  buff: buffer for save image data
          * @param  size: buffer size
          * @param  pDataOffset: pointer to variable to save received data offset in the image
          * @return < 0 - error code
          *         0 - no data
          *         > 0 - data written in the buffer
          */
        virtual int Load (void* buff, uint size, uint* pDataOffset) = 0;
};
