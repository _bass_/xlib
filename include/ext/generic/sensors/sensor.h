//*****************************************************************************
//
// @brief   Generic sensors interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include <list>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Debug module name
    #define SENSOR_DEBUG_NAME                   "Sensor"

    // Delay value to turn off sensor periodical polling
    #define SENSOR_UPDATE_DELAY_NEVER           ( ~0u )

    // Sensor driver registration
    #define REGISTER_SENSOR(type, fn)           REGISTER_ITEM(xLib_sensors, const TSensDrvDesc, {type, fn})


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #if !defined(__ICCARM__) || _HAS_NAMESPACE
    using std::list;
    #endif

    // Data types
    typedef enum
    {
        SENSOR_DATA_TYPE_BOOL = 0,          // true/false, 1 byte
        SENSOR_DATA_TYPE_INT8,              // Signed integer, 1 byte
        SENSOR_DATA_TYPE_UINT8,             // Unsigned integer, 1 byte
        SENSOR_DATA_TYPE_INT,               // Signed integer, 4 bytes
        SENSOR_DATA_TYPE_UINT,              // Unsigned integer, 4 bytes
        SENSOR_DATA_TYPE_INT64,             // Signed integer, 8 bytes
        SENSOR_DATA_TYPE_UINT64,            // Unsigned integer, 8 bytes
        SENSOR_DATA_TYPE_FLOAT,             // Floating point number, 4 bytes
        SENSOR_DATA_TYPE_STRING,            // Null-terminated string
        SENSOR_DATA_TYPE_RAW                // Raw data, 2 bytes length + data block
    } TSensDType;

    // Channel descriptor
    typedef struct
    {
        const char* name;                   // Channel name
        uint        channel;                // Channel number
    } TSensChDesc;

    // Sensor descriptor (list of channel descriptors)
    typedef struct
    {
        uint                chCount;        // Number of channels in the list
        const TSensChDesc*  channels;       // Channel list
    } TSensDesc;

    // Channel state flags
    typedef union
    {
        char        fVal;
        struct
        {
            char    valid   : 1;            // Data is valid
            char    reserved: 7;
        };
    } TSensChState;

    // Callback functions
    // Callback base types
    typedef enum
    {
        SENSOR_CB_UPDATE_CHANGED = 0,       // Update time changed
        SENSOR_CB_VALUE_CHANGED,            // Channel value changed
        SENSOR_CB_STATE_CHANGED,            // Channel state changed
        SENSOR_CB_HAL_REQUEST,              // HAL request (parameters in TSensHalParams)
        SENSOR_CB_CUSTOM_START  = 100
    } TSensCbType;

    class CSensor;
    typedef struct
    {
        uint    type;                       // Notification type (TSensCbType or custom code)
        void    (*fn) (CSensor* pSensor, void* arg);
    } TSensCbDesc;

    typedef struct
    {
        uint                count;          // Number of descriptors in the list
        const TSensCbDesc*  list;           // List of callback descriptors
    } TSensCb;


    // Sensor driver descriptor
    typedef struct
    {
        uint16      type;                                           // Sensorr type (SENSOR_TYPE_xxx)
        CSensor*    (*creator) (uint32 id, const TSensCb* pCb);     // Sensor object creation function
    } TSensDrvDesc;
    
    // Request SENSOR_CB_HAL_REQUEST parameters
    typedef struct
    {
        int     result;                     // Operation result
        uint    channel;                    // Channel ID
        void*   data;                       // Additional data
        uint    size;                       // Data size
    } TSensHalParams;


// ----------------------------------------------------------------------------
// CSensor
// ----------------------------------------------------------------------------

class CSensor
{
    public:
        /**
          * @brief  Constructor
          * @param  id: Sensor ID
          * @param  type: Sensor type
          * @param  pCb: List of sensor's callback functions
          */
        CSensor (uint32 id, uint16 type, const TSensCb* pCb);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CSensor ();

        /**
          * @brief  Get sensor ID
          * @return Sensor ID
          */
        uint32 GetId (void) const { return m_id; }

        /**
          * @brief  Get sensor type
          * @return Sensor type
          */
        uint16 GetType (void) const { return m_type; }

        /**
          * @brief  Get delay value before next update
          * @return Remaining time before update (ms)
          */
        uint GetUpdateDelay (void) const;
        
        /**
          * @brief  Set configuration parameters
          * @param  params: Configuration parameters
          * @return 0 or negative error code
          */
        virtual int Configure (const void* params);

        /**
          * @brief  Polling function (update states and channel values...)
          * @param  None
          */
        virtual void Poll (void) = 0;

        /**
          * @brief  Sensor descriptor reading
          * @return Sensor descriptor or NULL on error
          */
        virtual const TSensDesc* GetDesc (void) const = 0;
        
        /**
          * @brief  Channel descriptor reading
          * @param  channel: Channel ID
          * @return Channel descriptor or NULL on error
          */
        const TSensChDesc* GetChDesc (uint channel) const;

        /**
          * @brief  Channel state reading
          * @param  channel: Channel ID
          * @return Channel state
          */
        virtual TSensChState GetChState (uint channel) const = 0;
        
        /**
          * @brief  Channel value reading (generic function)
          * @param  channel: Channel ID
          * @param  pType: Variable where channel value type will be saved
          * @param  buff: Channel value buffer
          * @param  size: Buffer size
          * @return Amount of data written in the buffer or negative error code
          */
        int GetValue (uint channel, TSensDType* pType, void* buff, uint size) const;

        /**
          * @brief  Channel value reading
          * @param  channel: Channel ID
          * @param  pVal: Variable where channel value will be saved
          * @return 0 or negative error code
          */
        int GetValue (uint channel, bool* pVal) const;
        int GetValue (uint channel, int8* pVal) const;
        int GetValue (uint channel, uint8* pVal) const;
        int GetValue (uint channel, int* pVal) const;
        int GetValue (uint channel, uint* pVal) const;
        int GetValue (uint channel, int64* pVal) const;
        int GetValue (uint channel, uint64* pVal) const;
        int GetValue (uint channel, float* pVal) const;
        
        /**
          * @brief  Set channel value (generic function)
          * @param  channel: Channel ID
          * @param  type: Channel value type
          * @param  data: Pointer to channel value
          * @param  size: Value data size
          * @return 0 or negative error code
          */
        int SetValue (uint channel, TSensDType type, const void* data, uint size);

        /**
          * @brief  Set channel value
          * @param  channel: Channel ID
          * @param  val: Channel value to set
          * @return 0 or negative error code
          */
        int SetValue (uint channel, bool val);
        int SetValue (uint channel, int8 val);
        int SetValue (uint channel, uint8 val);
        int SetValue (uint channel, int val);
        int SetValue (uint channel, uint val);
        int SetValue (uint channel, int64 val);
        int SetValue (uint channel, uint64 val);
        int SetValue (uint channel, float val);

        /**
          * @brief  Sensor object creation
          * @param  id: Sensor ID
          * @param  type: Sensor type
          * @param  pCb: List of sensor's callback functions
          * @return Sensor object or NULL on error
          */
        static CSensor* CreateSensor (uint32 id, uint16 type, const TSensCb* pCb);

        /**
          * @brief  Sensor object searching
          * @param  id: Sensor ID
          * @return Sensor object or NULL on error
          */
        static CSensor* GetSensor (uint32 id);

        /**
          * @brief  List of sensors objects
          * @return Pointer to sensor objects list
          */
        static const list<CSensor*>* GetSensors (void);

    protected:

        /**
          * @brief  Run sensor's callback funtion
          * @param  cbType: Type of callback function
          * @param  arg: Additional callback function arguments
          */
        void notify (uint cbType, void* arg = NULL);

        /**
          * @brief  Set delay before next sensor update
          * @param  delay: Delay value (ms)
          */
        void setUpdateDelay (uint delay);
        
        /**
          * @brief  Get channel index in sensor descriptor
          * @param  channel: Channel ID
          * @return Channel index or negative error code
          */
        int getChIndex (uint channel) const;

        /**
          * @brief  Get channel value
          * @param  channel: Channel ID
          * @param  pType: Variable where channel value type will be saved
          * @param  buff: Channel value buffer
          * @param  size: Buffer size
          * @return Amount of data written in the buffer or negative error code
          */
        virtual int getValue (uint channel, TSensDType* pType, void* buff, uint size) const = 0;
        
        /**
          * @brief  Set channel value (generic function)
          * @param  channel: Channel ID
          * @param  type: Channel value type
          * @param  data: Pointer to channel value
          * @param  size: Value data size
          * @return 0 or negative error code
          */
        virtual int setValue (uint channel, TSensDType type, const void* data, uint size) = 0;

    private:
        const uint32    m_id;           // Sensor ID
        const uint16    m_type;         // Sensor type
        const TSensCb*  m_pCb;          // Callback functions list
        uint32          m_updateTime;   // Timestamp of next sensor update
};
