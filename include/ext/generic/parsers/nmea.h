//*****************************************************************************
//
// @brief   NMEA 0183 protocol parser
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// CNmea
// ----------------------------------------------------------------------------

class CNmea
{
    public:
        // Supported NMEA message types
        typedef enum
        {
            GGA = 0,
            GLL,
            GSA,
            GSV,
            RMC,
            VTG,
            ZDA,
            MSG_TYPE_COUNT
        } TMsgType;

        typedef struct
        {
            uint32  hour : 5;
            uint32  min  : 6;
            uint32  sec  : 6;
            uint32  ms   : 15;
        } TTime;

        typedef struct
        {
            uint8   day;
            uint8   month;
            uint16  year;
        } TDate;

        typedef struct
        {
            float   latitude;
            float   longitude;
        } TCoord;

        typedef struct
        {
            TTime   time;
            TCoord  coord;
            uint8   quality;
            uint8   satInView;
            uint16  stationId;
            float   antAltitude;
            float   hdop;
            float   geoSeparation;
            float   ageDiff;
        } TMsgGga;

        typedef struct
        {
            TTime   time;
            TCoord  coord;
            bool    valid;
        } TMsgGll;

        typedef struct
        {
            char    modeSelection;
            uint8   mode;
            uint8   satId[12];
            float   pdop;
            float   hdop;
            float   vdop;
        } TMsgGsa;

        typedef struct
        {
            uint8   msgCount;
            uint8   msgNum;
            uint8   satCount;
            struct
            {
                uint8   id;
                uint8   elevation;
                uint16  azimuth;
                uint8   snr;
            } satelits[4];
        } TMsgGsv;

        typedef struct
        {
            TTime   time;
            TDate   date;
            TCoord  coord;
            bool    valid;
            float   speed;
            float   direction;
            float   declination;
        } TMsgRmc;

        typedef struct
        {
            float   directionPole;
            float   directionMag;
            float   speed;
        } TMsgVtg;

        typedef struct
        {
            TTime   time;
            TDate   date;
            int8    tzHours;
            uint8   tzMin;
        } TMsgZda;


        /**
          * @brief  Constructor
          * @param  None
          */
        CNmea ();

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CNmea () {}

        /**
          * @brief  Transfer data to the parser
          * @param  data: Raw data
          * @param  size: Data size
          */
        void Parse (const char* data, uint size);

        /**
          * @brief  Message data validation
          * @param  type: Message type
          * @return true - message was parsed and contains valid data
          */
        bool IsValid (TMsgType type) const;

        /**
          * @brief  Reset flag of valid message
          * @param  type: Message type
          */
        void Invalidate (TMsgType type);

        /**
          * @brief  Message data reading
          * @param  type: Message type
          * @return Pointer to message data
          */
        const void* GetMsg (TMsgType type);

    private:
        // Message container
        typedef struct
        {
            TMsgGga gga;
            TMsgGll gll;
            TMsgGsa gsa;
            TMsgGsv gsv;
            TMsgRmc rmc;
            TMsgVtg vtg;
            TMsgZda zda;
        } TContainer;

        char        m_buff[12];         // Values parser buffer
        uint8       m_buffIndex;        // Current byte index in the buffer
        char        m_crc;              // Message CRC
        TMsgType    m_msgType;          // Currently parsing message type
        uint        m_fieldIndex;       // Parsing field in the message
        uint        m_validMask;        // Mask of valid messages
        TContainer  m_storage;          // Parsed messages storage

        STATIC_ASSERT(sizeof(m_validMask) < MSG_TYPE_COUNT);

        /**
          * @brief  Find message in storage
          * @param  type: Message type
          * @param  pMsgSize: Pointe to variable to save message size
          * @return Pointer to message data
          */
        void* getMsgStorage (TMsgType type, uint* pMsgSize = NULL);

        /**
          * @brief  Reset message data in storage
          * @param  type: Message type
          */
        void resetData (TMsgType type);

        /**
          * @brief  Reset parser state
          * @param  None
          */
        void resetParser (void);

        /**
          * @brief  Find start of message and its type detection
          * @param  data: NMEA data block
          * @param  size: Data size
          * @return Number of processed data bytes
          */
        uint processType (const char* data, uint size);

        /**
          * @brief  Message data processing
          * @param  data: NMEA data block
          * @param  size: Data size
          * @return Number of processed data bytes
          */
        uint processData (const char* data, uint size);

        /**
          * @brief  On the fly message CRC calculation
          * @param  data: NMEA data block
          * @param  size: Data size
          */
        void addToCrc (const char* data, uint size);
};
