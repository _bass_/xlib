//*****************************************************************************
//
// @brief   Socket interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kqueue.h"
#include "dev/device.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Family code (invalid)
    // First free ID -> 4 (!!! update on new family addition !!!)
    #define NET_FAMILY_UNKNOWN              0

    // Option flags
    #define SOCK_F_NONBLOCK                 (1 << 0)

    #define SOCK_OPT(group, code)           ( (uint)code | ((uint)group << 16) )

    // Socket parameters
    #define SOCK_GROUP                      0
    #define SOCK_OPT_FLAGS                  SOCK_OPT(SOCK_GROUP, 1)
    #define SOCK_OPT_TIMEOUT                SOCK_OPT(SOCK_GROUP, 2)

    // Register socket
    CC_SECTION_DECLARE(xLib_sockDesc)
    #define REGISTER_SOCKET(family, type, fnCreate, fnParser)   CC_VAR_USED_DECLARATION static const TSockDesc sockDesc_##family##type CC_VAR_SECTION(xLib_sockDesc) = \
                                                                { \
                                                                    family, \
                                                                    type, \
                                                                    fnCreate, \
                                                                    fnParser \
                                                                }


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    class CDevNet;
    class CSocket;

    // Socket type
    typedef enum
    {
        SOCK_TYPE_RAW = 0,
        SOCK_TYPE_STREAM,
        SOCK_TYPE_DGRAM,
        SOCK_TYPE_COUNT
    } TSockType;

    // Working mode
    typedef enum
    {
        E_SOCK_MODE_UNKNOWN = 0,
        E_SOCK_MODE_CLIENT,
        E_SOCK_MODE_SERVER
    } TSockMode;

    // Generic network address
    typedef struct
    {
        uint    family;
        char    data[12];
    } TNetAddr;

    // Socket (network) buffer descriptor
    typedef struct
    {
        CSocket*    socket;             // Socket
        CDevNet*    pDev;               // Network interface
        void*       data;               // Data / buffer
        uint16      dataSize;           // Data size
        uint16      buffSize;           // Buffer size
        TNetAddr    addr;               // Remote address (for incoming packets)
    } TSkb;

    // Network packets parser
    // Returns:< 0 - broken packet, 0 - packet received, > 0 - receiving in progress (expected amount of data)
    typedef int (TNetParser) (TSkb* skb);

    // Socket descriptor
    typedef struct
    {
        uint        family;
        TSockType   type;
        // Create socket object handler
        CSocket*    (*create)(uint family, TSockType type);
        TNetParser* parser;
    } TSockDesc;


// ----------------------------------------------------------------------------
// CSocket
// ----------------------------------------------------------------------------

class CSocket
{
    friend class CDevNet;

    public:
        /**
          * @brief  Constructor
          * @param  family: Network (socket) protocol family
          * @param  type: Socket type
          * @param  packetCount: Number of buffered packets
          */
        CSocket (uint family, TSockType type, uint packetCount);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CSocket ();

        /**
          * @brief  Create socket object
          * @param  family: Network (socket) protocol family
          * @param  type: Socket type
          * @return Socket object or NULL on error
          */
        static CSocket* Create (uint family, TSockType type);

        /**
          * @brief  Get pointer to parser function
          * @param  family: Network (socket) protocol family
          * @return Pointer to parser or NULL on error
          */
        static TNetParser* GetParser (uint family);

        /**
          * @brief  Find socket for receiving the packet
          * @param  skb: Packet descriptor
          * @return Socket object or NULL on error
          */
        static CSocket* Route (TSkb* skb);

        /**
          * @brief  Polling handler
          * @param  None
          */
        virtual void Poll (void);

        /**
          * @brief  Set socket option
          * @param  code: Option code
          * @param  value: Pointer to option value
          * @return 0 or negative error code
          */
        virtual int SetOption (uint code, const void* value);

        /**
          * @brief  Get socket option value
          * @param  code: Option code
          * @param  value: Pointer to value buffer
          * @return 0 or negative error code
          */
        virtual int GetOption (uint code, void* value) const;

        /**
          * @brief  Write data into the socket
          * @param  data: Pointer to data
          * @param  size: Data size
          * @return Amount of sent data or negative error code
          */
        virtual int Write (const void* data, uint size);

        /**
          * @brief  Read data from the socket
          * @param  buff: Pointer to buffer for received data
          * @param  size: Buffer size
          * @return Amount of written into the buffer data or negative error code
          */
        virtual int Read (void* buff, uint size);

        /**
          * @brief  Send data to specified address
          * @param  pAddr: Recepient address
          * @param  data: Pointer to data
          * @param  size: Data size
          * @return Amount of sent data or negative error code
          */
        virtual int Send (const TNetAddr* pAddr, const void* data, uint size) = 0;

        /**
          * @brief  Receive data
          * @param  pAddr: Pointer to variable to save sender address
          * @param  buff: Pointer to buffer for received data
          * @param  size: Buffer size
          * @return Amount of written into the buffer data or negative error code
          */
        virtual int Recv (TNetAddr* pAddr, void* buff, uint size);

        /**
          * @brief  Setup connection with specified address
          * @param  addr: Remote address
          * @return 0 or negative error code
          */
        virtual int Connect (const TNetAddr* addr);

        /**
          * @brief  Disconnect from remote address
          * @return 0 or negative error code
          */
        virtual int Disconnect (void);
        
        /**
          * @brief  Setup socket for receiving incomming connections
          * @return 0 or negative error code
          */
        virtual int Listen (void);

        /**
          * @brief  Bind socket to specified interface address
          * @param  addr: Interface address
          * @return 0 or negative error code
          */
        virtual int Bind (const TNetAddr* addr);

        /**
          * @brief  Accept incoming connection
          * @param  pAddr: Pointer to variable to save client address
          * @return 0 or negative error code
          */
        virtual int Accept (TNetAddr* pAddr);

        /**
          * @brief  Read address of remote client
          * @param  pAddr: Pointer to variable to save client address
          */
        void GetAddr (TNetAddr* pAddr) const;

    protected:
        const uint          m_netFamily;    // Socket network protocol family
        const TSockType     m_type;         // Socket type
        KQueue              m_qIn;          // Queue of incoming packets
        TSockMode           m_mode;         // Working mode
        TNetAddr            m_addrLocal;    // Local address
        TNetAddr            m_addrRemote;   // Remote address
        uint                m_optFlags;     // Option flags (SOCK_F_xx)
        uint                m_optTimeout;   // Read/write timeout (seconds)

        /**
          * @brief  Network interface searching for packet sending
          * @param  addrRemote: Remote address
          * @return Pointer to network device object or NULL on error
          */
        CDevNet* route (const TNetAddr* addrRemote);

        /**
          * @brief  Incoming packets handler.
          *         Copying data to socket buffer, packet adding to incoming queue (m_qIn)
          * @param  skb: Socket packet descriptor
          */
        virtual void onReceivePacket (const TSkb* skb) = 0;

        /**
          * @brief  Checking for addresses matching for incoming packet
          * @param  addrRemote: Remote address
          * @param  addrLocal: Local address
          * @return true - addresses matched
          */
        virtual bool checkAddrIn (const TNetAddr* addrRemote, const TNetAddr* addrLocal) = 0;

        /**
          * @brief  Checking for addresses matching for outgoing packet
          * @param  addrRemote: Remote address
          * @param  pNetDev: Network interface
          * @return true - addresses matched
          */
        virtual bool checkAddrOut (const TNetAddr* addrRemote, CDevNet* pNetDev) = 0;

    private:
        CSocket*    m_nextSock;
};
