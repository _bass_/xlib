//*****************************************************************************
//
// @brief   TCP socket (lwip)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "net/socket.h"
#include "dev/net.h"
#include "lwip/api.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define NET_FAMILY_INET                 1

    #define IP_ADDR(a, b, c, d)             (                         \
                                                ((uint32)(d) << 24) | \
                                                ((uint32)(c) << 16) | \
                                                ((uint32)(b) << 8)  | \
                                                ((uint32)(a) << 0)    \
                                            )
    #define INADDR_ANY                      0


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // IPv4 address (see TNetAddr)
    typedef struct
    {
        uint    family;
        struct
        {
            uint32  addr;
            uint16  port;
        };
    } TAddrIp;


// ----------------------------------------------------------------------------
// CSockTcp
// ----------------------------------------------------------------------------

class CSockTcp : public CSocket
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CSockTcp ();

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CSockTcp ();

        /**
          * @brief  Send data to specified address
          * @param  pAddr: Recepient address
          * @param  data: Pointer to data
          * @param  size: Data size
          * @return Amount of sent data or negative error code
          */
        int Send (const TNetAddr* pAddr, const void* data, uint size);

        /**
          * @brief  Receive data
          * @param  pAddr: Pointer to variable to save sender address
          * @param  buff: Pointer to buffer for received data
          * @param  size: Buffer size
          * @return Amount of written into the buffer data or negative error code
          */
        int Recv (TNetAddr* pAddr, void* buff, uint size);

        /**
          * @brief  Read data from the socket
          * @param  buff: Pointer to buffer for received data
          * @param  size: Buffer size
          * @return Amount of written into the buffer data or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data into the socket
          * @param  data: Pointer to data
          * @param  size: Data size
          * @return Amount of sent data or negative error code
          */
        int Write (const void* buff, uint size);

        /**
          * @brief  Setup connection with specified address
          * @param  addr: Remote address
          * @return 0 or negative error code
          */
        int Connect (const TNetAddr* addr);

        /**
          * @brief  Disconnect from remote address
          * @return 0 or negative error code
          */
        int Disconnect (void);

        // TODO: server
//        virtual int     Listen (void);
//        virtual int     Accept (uint32 addr, uint port);

        /**
          * @brief  Bind socket to specified interface address
          * @param  addr: Interface address
          * @return 0 or negative error code
          */
        int Bind (const TNetAddr* addr);

    private:
        struct netconn* m_conn;         // Connection handler (lwip)
        struct netbuf*  m_pNetBuff;     // Active network buffer (lwip)
        uint            m_offset;       // Read amount of data of current packet

        /**
          * @brief  Incoming packets handler.
          *         Copying data to socket buffer, packet adding to incoming queue (m_qIn)
          * @param  skb: Socket packet descriptor
          */
        void onReceivePacket (const TSkb* skb);

        /**
          * @brief  Checking for addresses matching for incoming packet
          * @param  addrRemote: Remote address
          * @param  addrLocal: Local address
          * @return true - addresses matched
          */
        bool checkAddrIn (const TNetAddr* addrRemote, const TNetAddr* addrLocal);
        
        /**
          * @brief  Checking for addresses matching for outgoing packet
          * @param  addrRemote: Remote address
          * @param  pNetDev: Network interface
          * @return true - addresses matched
          */
        bool checkAddrOut (const TNetAddr* addrRemote, CDevNet* pNetDev);
};
