//*****************************************************************************
//
// @brief   CAN socket
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/can.h"
#include "net/socket.h"
#include "dev/net.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define NET_FAMILY_CAN                              2

    #if defined(CFG_SOCK_CAN_BUFF_PACK_COUNT)
        #define SOCK_CAN_BUFF_PACK_COUNT                CFG_SOCK_CAN_BUFF_PACK_COUNT
    #else
        #define SOCK_CAN_BUFF_PACK_COUNT                4
    #endif


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Address descriptor (see TNetAddr)
    typedef struct
    {
        uint    family;
        struct
        {
            TCanId  addr;
        };
    } TAddrCan;


// ----------------------------------------------------------------------------
// CSockCan
// ----------------------------------------------------------------------------

class CSockCan : public CSocket
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CSockCan ();

        /**
          * @brief  Send data to specified address
          * @param  pAddr: Recepient address
          * @param  data: Pointer to data
          * @param  size: Data size
          * @return Amount of sent data or negative error code
          */
        int Send (const TNetAddr* pAddr, const void* data, uint size);

    private:
        char    m_buffRx[SOCK_CAN_BUFF_PACK_COUNT][sizeof(TCanFrame::data)];
        uint    m_currBuffIndex;

        /**
          * @brief  Incoming packets handler.
          *         Copying data to socket buffer, packet adding to incoming queue (m_qIn)
          * @param  skb: Socket packet descriptor
          */
        void onReceivePacket (const TSkb* skb);
        
        /**
          * @brief  Checking for addresses matching for incoming packet
          * @param  addrRemote: Remote address
          * @param  addrLocal: Local address
          * @return true - addresses matched
          */
        bool checkAddrIn (const TNetAddr* addrRemote, const TNetAddr* addrLocal);
        
        /**
          * @brief  Checking for addresses matching for outgoing packet
          * @param  addrRemote: Remote address
          * @param  pNetDev: Network interface
          * @return true - addresses matched
          */
        bool checkAddrOut (const TNetAddr* addrRemote, CDevNet* pNetDev);
};
