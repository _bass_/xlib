//*****************************************************************************
//
// @brief   Base images functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/ui.h"
#include "ui/renderer.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    // Register new image format driver
    #define REGISTER_IMG_FORMAT(format, probe, creator)     static const CImage::TFormatDesc _img_format_desc = \
                                                            {format, probe, creator}; \
                                                            REGISTER_ITEM( \
                                                                xLib_ui_img, \
                                                                const CImage::TFormatDesc*, \
                                                                &_img_format_desc \
                                                            );


// ----------------------------------------------------------------------------
// CImage
// ----------------------------------------------------------------------------

class CImage
{
    public:
        // Supported image types
        enum EFormat
        {
            E_IMG_FORMAT_GIF,
            E_IMG_FORMAT_PNG,

            E_IMG_FORMAT_COUNT
        };

        // Image format driver descriptor (only for usage in REGISTER_IMG_FORMAT())
        typedef struct
        {
            EFormat format;
            bool (*hdlProbe) (const void* data, uint size);
            CImage* (*hdlCreator) (void);
        } TFormatDesc;

        // Animated image frame information
        struct TFrameInfo
        {
            uint16  left;       // Frame x-axis position relative to whole image position (pixels)
            uint16  top;        // Frame y-axis position relative to whole image position (pixels)
            uint16  width;      // Frame width (pixels)
            uint16  height;     // Frame height (pixels)
            uint16  duration;   // Frame duration (ms)
        };


        /**
         * @brief  Image object creation
         * @param  type: Image type
         * @return Pointer to created object or null on error
         */
        static CImage* CreateObject (EFormat type);

        /**
         * @brief  Image object creation
         * @param  data: Pointer to image data
         * @param  size: Image data size
         * @return Pointer to created object or null on error
         */
        static CImage* CreateObject (const void* data, uint size);

        /**
         * @brief  Constructor
         * @param  None
         */
        CImage ();

        /**
         * @brief  Constructor
         * @param  None
         */
        virtual ~CImage ();

        /**
         * @brief  Get current object image format
         * @return Image format
         */
        virtual EFormat GetFormat (void) = 0;

        /**
         * @brief  Load image data into the object
         * @param  data: Pointer to image data
         * @param  size: Image data size
         * @return 0 or negative error code
         */
        virtual int Load (const void* data, uint size) = 0;

        /**
         * @brief  Get image width
         * @return Image width (pixels)
         */
        virtual uint GetWidth (void) = 0;

        /**
         * @brief  Get image height
         * @return Image height (pixels)
         */
        virtual uint GetHeight (void) = 0;

        /**
         * @brief  Check if image is animated
         * @return true - image is animated
         */
        bool IsAnimated (void) { return GetFrames() > 0; }

        /**
         * @brief  Check if animated image is looped
         * @return true - animated image is looped
         */
        virtual bool IsLooped (void) { return false; }

        /**
         * @brief  Get number of frames of animated image
         * @return Number of frames of animated image
         */
        virtual uint GetFrames (void) { return 0; }

        /**
         * @brief  Get index of current frame
         * @return Index of current frame
         */
        virtual uint GetFrame (void) { return 0; }

        /**
         * @brief  Set frame number of animated image to render
         * @return Number of frames of animated image
         */
        virtual int SetFrame (uint index) { UNUSED_VAR(index); return -1; }

        /**
         * @brief  Get specified frame information
         * @return 0 or negative error code
         */
        virtual int GetFrameInfo (uint index, TFrameInfo* pInfo)
        {
            UNUSED_VAR(index);
            UNUSED_VAR(pInfo);
            return -1;
        }

        /**
         * @brief  Image (or frame) rendering
         * @param  renderer: Renderer
         * @return Number of processed pixels or negative error code
         */
        virtual int Render (CRenderer& renderer) = 0;

        /**
         * @brief  Image cropping
         * @param  pData: Pointer rendered image data
         * @param  width: Rendered image width
         * @param  height: Rendered image height
         * @param  x: X-offset of cropping start position
         * @param  y: Y-offset of cropping start position
         * @param  resWidth: Result image width
         * @param  resHeight: Result image height
         * @return Number of result image pixels (source data is overwritten) or negative error code
         */
        static int Crop (TColor* pData, uint width, uint height, uint x, uint y, uint resWidth, uint resHeight);
};
