//*****************************************************************************
//
// @brief   Renderer functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/ui.h"
#include "ui/area.h"


// ----------------------------------------------------------------------------
// CRenderer
// ----------------------------------------------------------------------------

class CRenderer
{
    public:
        // Blending mode
        enum TBlendMode
        {
            E_BLEND_NONE,   // New pixel will replace old one
            E_BLEND_ADD_FG, // New pixel will be added as a foreground (abowe previous pixel)
            E_BLEND_ADD_BG  // New pixel will be added as a background (below previous pixel)
        };

        /**
         * @brief Constructor
         * @param buff: Pointer to buffer to store pixels data (canvas)
         * @param width: Canvas width (pixels)
         * @param height: Canvas height (pixels)
         */
        CRenderer (TColor* buff, uint16 width, uint16 height);

        /**
         * @brief Get canvas width
         * @return Canvas width (pixels)
         */
        uint16 GetWidth (void) const { return m_canvasWidth; }

        /**
         * @brief Get canvas height
         * @return Canvas height (pixels)
         */
        uint16 GetHeight (void) const { return m_canvasHeight; }

        /**
         * @brief Set offset of rendering object related to canvas start point.
         *        This offset is used in PutPixel() and Fill() to properly convert coordinates
         *        into canvas coordinates.
         * @param value: Appropriate coordinate offset
         */
        void SetOffsetX (int16 value) { m_offsetX = value; }
        void SetOffsetY (int16 value) { m_offsetY = value; }

        /**
         * @brief Get offset of rendering object related to canvas start point.
         * @return Appropriate coordinate offset
         */
        int16 GetOffsetX (void) const { return m_offsetX; }
        int16 GetOffsetY (void) const { return m_offsetY; }

        /**
         * @brief Set blending mode
         * @param mode: Blending mode
         */
        void SetBlendMode (TBlendMode mode) { m_blendMode = mode; }

        /**
         * @brief Put pixel on the canvas
         * @param x: X-coordinate of pixel position
         * @param y: Y-coordinate of pixel position
         * @param color: Pixel color
         */
        void PutPixel (int16 x, int16 y, TColor color);

        /**
         * @brief Fill rectangle with specified color
         * @param x: X-coordinate of top-left corner of the rectangle
         * @param y: Y-coordinate of top-left corner of the rectangle
         * @param w: Rectangle width
         * @param h: Rectangle height
         * @param color: Color to fill with
         */
        void Fill (int16 x, int16 y, uint16 w, uint16 h, TColor color);

    private:
        TColor*         m_buff;
        const uint16    m_canvasWidth;
        const uint16    m_canvasHeight;
        int16           m_offsetX;
        int16           m_offsetY;
        TBlendMode      m_blendMode;

        /**
         * @brief Colors blending
         * @param pSrc: Pointer to source
         * @param pDst: Pointer to destination
         */
        void blend (const TColor* pSrc, TColor* pDst);
};
