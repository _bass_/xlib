//*****************************************************************************
//
// @brief   Font renderer for EmWin fonts
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "ui/font.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Defines to replace types with proper values in font file
    #define GUI_CHARINFO        TEmwinCharInfo
    #define GUI_FONT_PROP       TEmwinFontProp
    #define GUI_FONT            TEmwinFontDesc


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Font type
    typedef enum
    {
        GUI_FONTTYPE_PROP,      // Standard (UNUSED, brocken converter?)
        GUI_FONTTYPE_PROP_AA2,  // Antialiased, 2 bit per pixel (4 pixels in a byte)
        GUI_FONTTYPE_PROP_AA4,  // Antialiased, 4 bit per pixel (2 pixels in a byte)

        GUI_FONTTYPE_COUNT
    } TEmwinFontType;

    // Symbol descriptor
    typedef struct
    {
        uint8   width;
        uint8   width2;
        uint8   bytesPerLine;
        const uint8* pData;
    } TEmwinCharInfo;

    // Group of symbols descriptor
    typedef struct _TEmwinFontProp
    {
        uint16  firstChar;
        uint16  lastChar;
        const TEmwinCharInfo* pInfo;
        const struct _TEmwinFontProp* pNext;
    } TEmwinFontProp;

    // Font descriptors
    typedef struct
    {
        TEmwinFontType type;
        uint8   height;
        uint8   height2;
        uint8   magnification_x;
        uint8   magnification_y;
        struct
        {
            const TEmwinFontProp* pProp;
        };
        uint8   baseline;
        uint8   heightLowercase;
        uint8   heightCapital;
    } TEmwinFontDesc;


// ----------------------------------------------------------------------------
// CFontEmwin
// ----------------------------------------------------------------------------

class CFontEmwin: public IFont
{
    public:
        /**
         * @brief  Constructor
         * @param  pDesc: Pointer to font descriptor
         */
        CFontEmwin (const TEmwinFontDesc* pDesc);

        /**
         * @brief  Destructor
         * @param  None
         */
        ~CFontEmwin ();

        /**
         * @brief  Get font height
         * @return Font height (pixels)
         */
        uint GetHeight (void);

        /**
         * @brief  Get symbol width
         * @param  c: Symbol code
         * @return Symbol width (pixels)
         */
        uint GetWidth (int c);

        /**
         * @brief  Render symbol
         * @param  renderer: Renderer
         * @param  c: Symbol code
         * @param  color: Text color
         * @return 0 or negative error code
         */
        int Render (CRenderer& renderer, int c, TColor color);

    private:
        const TEmwinFontDesc* m_pDesc;
};
