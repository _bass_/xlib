//*****************************************************************************
//
// @brief   Base widget functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/ui.h"
#include "ui/area.h"
#include "ui/renderer.h"
#include "misc/macros.h"
#include <forward_list>


// ----------------------------------------------------------------------------
// CWidget
// ----------------------------------------------------------------------------

class CWidget
{
    public:
        // Callbacks for widgets managment
        typedef struct
        {
            /**
             * @brief Callback to notify about changes in a widget (need to redraw)
             * @param area: Changed area (absolute coordinates)
             */
            void (*onChange) (const CArea& area);
        } TCbDesc;

        // Offset type
        enum TOffset
        {
            E_OFFSET_REL,   // Relative to parent widget offset
            E_OFFSET_ABS,   // Absolute offset
        };

        // Event type
        enum EvType
        {
            E_EV_POINTER_PRESS,
            E_EV_POINTER_MOVE,
            E_EV_FOCUS_CHANGED,
            E_EV_KBD,
        };

        // Event data structure
        struct TEvent
        {
            EvType  type;
            union
            {
                struct
                {
                    uint16 x;
                    uint16 y;
                    union
                    {
                        struct
                        {
                            int16   dx;
                            int16   dy;
                        } move;
                        struct
                        {
                            bool    isPressed;
                        } press;
                    };
                } pointer;

                struct
                {
                    bool    state;      // True - focus is activated, false - lost
                } focus;

                struct
                {
                    bool    state;      // True - button is pressed, false - released
                    uint16  scanCode;
                    uint    symbol;
                    union
                    {
                        uint        val;
                        struct
                        {
                            uint    ctrl    : 1;
                            uint    shift   : 1;
                        };
                    } modifiers;
                } kbd;
            };
        };


        /**
         * @brief Get list of root objects (parents)
         * @return Pointer to the list or null
         */
        static const std::forward_list<CWidget*>* GetRootList (void);

        /**
         * @brief Set callbacks
         * @param Callback descriptor
         */
        static void SetCallbacks (const TCbDesc* desc);

        /**
         * @brief Constructor
         * @param parent: Parent widget (limits size and position of current one)
         */
        CWidget (CWidget* parent = nullptr);

        /**
         * @brief Destructor
         */
        virtual ~CWidget ();

        /**
         * @brief Get parent widget
         * @return Pointer to parent widget object or null
         */
        CWidget* GetParent (void) const;

        /**
         * @brief Get list of childrens
         * @return Pointer to list of children or null
         */
        const std::forward_list<CWidget*>* GetChildren (void) const;

        /**
         * @brief Get widget width
         * @return Widget width
         */
        uint16 GetWidth (void) const;

        /**
         * @brief Get widget height
         * @return Widget height
         */
        uint16 GetHeight (void) const;

        /**
         * @brief Set widget width
         * @param width: Widget width
         */
        void SetWidth (uint16 width);

        /**
         * @brief Set widget height
         * @param height: Widget height
         */
        void SetHeight (uint16 height);

        /**
         * @brief Get widget offset
         * @param type: Offset type
         * @return Offset value
         */
        TPoint GetOffset (TOffset type = E_OFFSET_REL) const;

        /**
         * @brief Set widget offset
         * @param x: X coordinate offset
         * @param y: Y coordinate offset
         * @param type: Offset type (determines coordinates base point)
         */
        void SetOffset (int16 x, int16 y, TOffset type = E_OFFSET_REL);

        /**
         * @brief Get widget area
         * @param offsetType: Base point offset type
         * @return Widget area
         */
        CArea GetArea (TOffset offsetType = E_OFFSET_REL) const;

        /**
         * @brief Set widget area
         * @param offsetType: Base point offset type
         */
        void SetArea (const CArea& area, TOffset offsetType = E_OFFSET_REL);

        /**
         * @brief Events processing
         * @param ev: Event data
         * @return True - event was processed, false - ignored
         */
        virtual bool ProcessEvent (const TEvent& ev) { UNUSED_VAR(ev); return false; }

        /**
         * @brief Widget content drawing
         * @param renderer: Renderer
         * @param area: Area of the content to draw (coordinates are relative to start point of the widget)
         * @return 0 or negative error code
         */
        virtual int Render (CRenderer& renderer, const CArea& area) = 0;

    protected:
        /**
         * @brief Notify about changes in the widget (need to redraw)
         * @param area: Changed area (relative to the start point of the widget)
         */
        void setChangedArea (const CArea& area);

    private:
        CArea m_area;                               // Widget area. Start point is offset from parent widget.
        CWidget* m_pParent;                         // Parent widget
        std::forward_list<CWidget*>* m_pChildList;  // List of child widgets

        /**
         * @brief Set parent widget
         * @param parent: Pointer to parent widget object
         */
        void setParent (CWidget* parent);

        /**
         * @brief Add child widget
         * @param pChild: Pointer to child widget
         */
        void childAdd (CWidget* pChild);

        /**
         * @brief Remove parent widget
         * @param pChild: Pointer to child widget to remove
         */
        void childRemove (CWidget* pChild);
};

