//*****************************************************************************
//
// @brief   PNG images renderer
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/image.h"


// ----------------------------------------------------------------------------
// CImagePng
// ----------------------------------------------------------------------------

class CImagePng: public CImage
{
    public:
        /**
         * @brief Constructor
         * @param None
         */
        CImagePng ();

        /**
         * @brief Constructor
         * @param None
         */
        ~CImagePng ();

        /**
         * @brief  Get current object image format
         * @return Image format
         */
        EFormat GetFormat (void) { return E_IMG_FORMAT_PNG; }

        /**
         * @brief  Load image data into the object
         * @param  data: Pointer to image data
         * @param  size: Image data size
         * @return 0 or negative error code
         */
        int Load (const void* data, uint size);

        /**
         * @brief  Get image width
         * @return Image width (pixels)
         */
        uint GetWidth (void);

        /**
         * @brief  Get image height
         * @return Image height (pixels)
         */
        uint GetHeight (void);

        /**
         * @brief  Image (or frame) rendering
         * @param  renderer: Renderer
         * @return Number of processed pixels or negative error code
         */
        int Render (CRenderer& renderer);

        struct TIhdr;

    private:
        struct TContext;

        const void*     m_data;         // Image data
        uint            m_dataSize;     // Image data size
        const TIhdr*    m_pIhdr;        // Pointer to IHDR chunk data inside image data
        uint            m_bytesInPixel; // Pixels data size (based on IHDR data)
        uint            m_bytesInLine;  // Image line data size (based on IHDR data)
        const uint8*    m_palette;      // Pointer to image palette data
        const uint8*    m_alphaTable;   // Pointer to transparency data table
        uint            m_alphaLen;     // Number of items in transparency table


        /**
         * @brief  Uncompressed data block processing. Reads data from input stream and copies data to output.
         * @param  pContext: Pointer to context data
         * @return 0 or negative error code
         */
        int processBlockUncompressed (TContext* pContext);

        /**
         * @brief  Compressed data block processing
         * @param  pContext: Pointer to context data
         * @param  dynamicTree: Type of decoding Huffman trees (dynamic or fixed)
         * @return 0 or negative error code
         */
        int processBlockHuffman (TContext* pContext, bool dynamicTree);

        /**
         * @brief  Compressed by Huffman codes data processing (decompression)
         * @param  pContext: Pointer to context data
         * @return 0 or negative error code
         */
        int processHuffmanData (TContext* pContext);

        /**
         * @brief  Fixed Huffman code trees creating (for literals/lengths and distances)
         * @param  pContext: Pointer to context data
         * @return 0 or negative error code
         */
        int buildFixedTables (TContext* pContext);

        /**
         * @brief  Dynamic Huffman code trees creation (for literals/lengths and distances)
         * @param  pContext: Pointer to context data
         * @return 0 or negative error code
         */
        int buildDynamicTables (TContext* pContext);

        /**
         * @brief  Huffman code trees destroying
         * @param  pContext: Pointer to context data
         * @return None
         */
        void destroyCodeTrees (TContext* pContext);

        /**
         * @brief  Output processing of decompressed symbol
         * @param  pContext: Pointer to context data
         * @param  byte: Decompressed byte
         * @return None
         */
        void out (TContext* pContext, uint8 byte);
};
