//*****************************************************************************
//
// @brief   GIF images renderer
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/image.h"


// ----------------------------------------------------------------------------
// CImageGif
// ----------------------------------------------------------------------------

class CImageGif: public CImage
{
    public:
        /**
         * @brief Constructor
         * @param None
         */
        CImageGif ();

        /**
         * @brief Constructor
         * @param None
         */
        ~CImageGif ();

        /**
         * @brief  Get current object image format
         * @return Image format
         */
        EFormat GetFormat (void) { return E_IMG_FORMAT_GIF; }

        /**
         * @brief  Load image data into the object
         * @param  data: Pointer to image data
         * @param  size: Image data size
         * @return 0 or negative error code
         */
        int Load (const void* data, uint size);

        /**
         * @brief  Get image width
         * @return Image width (pixels)
         */
        uint GetWidth (void);

        /**
         * @brief  Get image height
         * @return Image height (pixels)
         */
        uint GetHeight (void);

        /**
         * @brief  Check if animated image is looped
         * @return true - animated image is looped
         */
        bool IsLooped (void);

        /**
         * @brief  Get number of frames of animated image
         * @return Number of frames of animated image
         */
        uint GetFrames (void) { return m_frames; }

        /**
         * @brief  Get index of current frame
         * @return Index of current frame
         */
        uint GetFrame (void) { return m_currFrame; }

        /**
         * @brief  Set frame number of animated image to render
         * @return Number of frames of animated image
         */
        int SetFrame (uint index);

        /**
         * @brief  Get specified frame information
         * @return 0 or negative error code
         */
        int GetFrameInfo (uint index, TFrameInfo* pInfo);

        /**
         * @brief  Image (or frame) rendering
         * @param  renderer: Renderer
         * @return Number of processed pixels or negative error code
         */
        int Render (CRenderer& renderer);

    private:
        struct TFrameHdr;
        struct TExtGraphCtrl;
        struct TRenderDesc;
        struct TCodeTab;

        /**
         * @brief  Find specified type block
         * @param  blockType: Block type (GIF_BLOCK_xx or result of EXTENSION_CODE())
         * @param  index: Block index to find. If default value -1 is used specified type blocks will be counted.
         * @param  pBlock: Variable to save pointer to found block. Not used in counting mode.
         * @param  pSize: Variable to save found block size. Not used in counting mode.
         * @return Number of found blocks or negative error code
         */
        int findBlock (uint16 blockType, int index = -1, const void** pBlock = nullptr, uint* pSize = nullptr);

        /**
         * @brief  Get frame block
         * @param  frameIndex: Frame index to find
         * @param  pSize: Variable to save found block size
         * @param  pGraphDesc: Variable to save pointer to appropriate Graphic extension block
         * @return Pointer to frame image header or null on error
         */
        const TFrameHdr* getFrame (uint frameIndex, uint* pSize = nullptr, const TExtGraphCtrl** pGraphDesc = nullptr);

        /**
         * @brief  Render frame
         * @param  pDesc: Frame descriptor
         * @param  codesDataSize: Size of data block with codes
         * @param  pRenderDesc: Render descriptor
         * @return 0 or negative error code
         */
        int renderFrame (const TFrameHdr* pDesc, uint codesDataSize, TRenderDesc* pRenderDesc);

        /**
         * @brief Save pixel with specified color index into render buffer
         * @param pRenderDesc: Render descriptor
         * @param colorIndex: Index of pixel color
         */
        void putPixel (TRenderDesc* pRendDesc, uint8 colorIndex);

        /**
         * @brief  Code table creation
         * @param  pColorTable: Pointer to used color table
         * @param  initTabSize: Initial size of code table
         * @param  maxCodesCount: Maximum number of codes in the table
         * @return Pointer to created table descriptor or negative error code
         */
        TCodeTab* codeTableCreate (const void* pColorTable, uint16 initTabSize, uint maxCodesCount);

        /**
         * @brief Destroy code table
         * @param pDesc: Code table to destroy
         */
        void codeTableDestroy (TCodeTab* pDesc);

        /**
         * @brief  Clear code table
         * @param  pDesc: Code table to destroy
         */
        void codeTableClear (TCodeTab* pDesc);

        /**
         * @brief  Add code into code table
         * @param  pDesc: Code table descriptor
         * @param  prevCode: Previous code in the sequence of codes
         * @param  value: Code to add (last value in list of values)
         * @return Added code value
         */
        uint16 codeTableAdd (TCodeTab* pDesc, uint16 prevCode, uint16 value);

        /**
         * @brief  Read first value from sequence of code values
         * @param  pDesc: Code table descriptor
         * @param  code: Code of a sequence
         * @return First code value or 0 on error
         */
        uint16 codeTableGetFirst (TCodeTab* pDesc, uint16 code);

        /**
         * @brief  Count number of codes in specified sequence
         * @param  pDesc: Code table descriptor
         * @param  code: Code value
         * @return Nuber of values in sequence
         */
        uint codeTableCount (TCodeTab* pDesc, uint16 code);

        /**
         * @brief  Check if code exists in the table
         * @param  pDesc: Code table descriptor
         * @param  code: Code value
         * @return True - code presents in the table
         */
        bool codeTableIsCodeExists (TCodeTab* pDesc, uint16 code);

        /**
         * @brief Output sequence of color indexes (putPixel() is called for each one)
         * @param pDesc: Code table descriptor
         * @param pRenderDesc: Render descriptor
         */
        void codeTableOut (TCodeTab* pDesc, uint16 code, TRenderDesc* pRendDesc);


        const void* m_imgData;
        uint        m_imgDataSize;
        uint        m_frames;
        uint        m_currFrame;
};
