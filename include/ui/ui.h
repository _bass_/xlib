//*****************************************************************************
//
// @brief   Base UI functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/display.h"
#include "kernel/kthread.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    typedef union
    {
        uint8       data[4];
        struct
        {
            uint8   r;
            uint8   g;
            uint8   b;
            uint8   a;
        };
    } TColor;

    typedef struct
    {
        int16   x;
        int16   y;
    } TPoint;

    // Forward declaration
    class CWidget;


// ----------------------------------------------------------------------------
// UI
// ----------------------------------------------------------------------------

class UI: public KThread
{
    public:
        /**
         * @brief Constructor
         * @param pDisplay: Pointer to display device object
         */
        UI (CDevDisplay* pDisplay);

        /**
         * @brief Constructor
         * @param displayIndex: Index of registered display device
         */
        UI (uint displayIndex);

        /**
         * @brief Add input device to process events from it
         * @param devIndex: Input device system index
         */
        void AddInputDevice (uint devIndex);

        /**
         * @brief Set focus on specified widget
         * @param widget: Widget to set focus on or nullptr to clear current focus
         */
        void SetFocus (CWidget* widget);

    private:
        CDevDisplay*    m_pDisplay = nullptr;
        CWidget*        m_widgetInFocus = nullptr;

        /**
         * @brief Thread initialization
         */
        void init (void);

        /**
         * @brief Thread main function
         */
        void run (void);

        /**
         * @brief Update handler (redraw changed areas)
         */
        void update (void);

        /**
         * @brief Input events queue processing
         */
        void processEvents (void);
};
