//*****************************************************************************
//
// @brief   Font renderer interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/ui.h"
#include "ui/renderer.h"


// ----------------------------------------------------------------------------
// IFont
// ----------------------------------------------------------------------------

class IFont
{
    public:
        /**
         * @brief  Constructor
         * @param  width: Screen width (pixels)
         * @param  height: Screen height (pixels)
         */
        IFont ();

        /**
         * @brief  Destructor
         * @param  None
         */
        virtual ~IFont ();

        /**
         * @brief  Get font height
         * @return Font height (pixels)
         */
        virtual uint GetHeight (void) = 0;

        /**
         * @brief  Set font height
         * @param  height: Font height (pixels)
         * @return 0 or negative error code
         */
        virtual int SetHeight (uint height);

        /**
         * @brief  Get space between symbols
         * @return Space between symbols (pixels)
         */
        virtual uint GetSpace (void);

        /**
         * @brief  Get symbol width
         * @param  c: Symbol code
         * @return Symbol width (pixels)
         */
        virtual uint GetWidth (int c) = 0;

        /**
         * @brief  Get string width (only first line is used)
         * @param  str: Text string
         * @param  pLen: Pointer to string variable length. If it is null or value is 0, whole string is processed.
         *               The variable will contain number of processed symbols if pointer wasn't null.
         * @return Text width (pixels)
         */
        uint GetWidth (const char* str, uint* pLen = nullptr);

        /**
         * @brief  Render symbol
         * @param  renderer: Renderer
         * @param  c: Symbol code
         * @param  color: Text color
         * @return 0 or negative error code
         */
        virtual int Render (CRenderer& renderer, int c, TColor color) = 0;
};
