//*****************************************************************************
//
// @brief   Text widget
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/widget.h"
#include "ui/font.h"


// ----------------------------------------------------------------------------
// CWidgetText
// ----------------------------------------------------------------------------

class CWidgetText: public CWidget
{
    public:

        // Horizontal text align
        enum EAlign
        {
            E_ALIGN_LEFT,
            E_ALIGN_RIGHT,
            E_ALIGN_CENTER,
        };

        /**
         * @brief Constructor
         * @param parent: Parent widget (limits size and position of current one)
         */
        CWidgetText (CWidget* parent = nullptr);

        /**
         * @brief Destructor
         */
        ~CWidgetText ();

        /**
         * @brief Widget content drawing
         * @param renderer: Renderer
         * @param area: Area of the content to draw (coordinates are relative to start point of the widget)
         * @return 0 or negative error code
         */
        int Render (CRenderer& renderer, const CArea& area);

        /**
         * @brief Set widget text
         * @param text: Text string
         */
        void SetText (const char* text);

        /**
         * @brief Set widget font
         * @param font: Font to use
         */
        void SetFont (IFont* font);

        /**
         * @brief Set text color
         * @param color: Color value
         */
        void SetColor (TColor color);

        /**
         * @brief Set horizontal text align
         * @param align: Align value
         */
        void SetAlign (EAlign align);

        /**
         * @brief Copy constructor
         * @param o: Source object
         */
        CWidgetText (const CWidgetText& o) = delete;

    private:
        char* m_text = nullptr;
        IFont* m_font = nullptr;
        TColor m_color = {0xff, 0xff, 0xff, 0xff};
        EAlign m_align = E_ALIGN_LEFT;

        /**
         * @brief Get symbol area
         * @param index: Symbol index in the string
         * @param prevArea: Area of previous symbol. Data will be replaced with values for specefiead symbol.
         * @return 0 or negative error code
         */
        int getSymbolArea (uint index, CArea& prevArea);
};
