//*****************************************************************************
//
// @brief   Image widget
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "ui/widget.h"
#include "ui/image.h"
#include "kernel/ktimer.h"


// ----------------------------------------------------------------------------
// CWidgetImage
// ----------------------------------------------------------------------------

class CWidgetImage: public CWidget
{
    public:
        /**
         * @brief Constructor
         * @param parent: Parent widget (limits size and position of current one)
         */
        CWidgetImage (CWidget* parent = nullptr);

        /**
         * @brief Destructor
         */
        ~CWidgetImage ();

        /**
         * @brief Widget content drawing
         * @param renderer: Renderer
         * @param area: Area of the content to draw (coordinates are relative to start point of the widget)
         * @return 0 or negative error code
         */
        int Render (CRenderer& renderer, const CArea& area);

        /**
         * @brief Set image data to use
         * @param data: Image data
         * @param size: Image data size
         */
        void SetImage (const void* data, uint size);

        /**
         * @brief Copy constructor
         * @param o: Source object
         */
        CWidgetImage (const CWidgetImage& o) = delete;

    private:
        void* m_data = nullptr;
        uint m_size = 0;
        CImage* m_img = nullptr;
        KTimer* m_timer = nullptr;

        /**
         * @brief Release all allocated resources
         */
        void freeResources (void);

        /**
         * @brief Timer handler (anumaned images updating)
         * @param timer: Timer object
         * @param params: Additional handler parameters
         */
        static void onTimer (KTimer* timer, void* params);
};
