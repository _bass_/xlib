//*****************************************************************************
//
// @brief   Area functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// CArea
// ----------------------------------------------------------------------------

class CArea
{
    public:
        /**
         * @brief Default constructor (creates empty area)
         */
        CArea (void);

        /**
         * @brief Constructor
         * @param x0: X coordinate of start point (upper-left)
         * @param y0: Y coordinate of start point (upper-left)
         * @param width: Width of the area
         * @param height: Height of the area
         */
        CArea (int16 x0, int16 y0, uint16 width, uint16 height);

        /**
         * @brief Get X coordinate of start point
         * @return Value of the coordinate
         */
        int16 x0 () const { return m_x0; }

        /**
         * @brief Get Y coordinate of start point
         * @return Value of the coordinate
         */
        int16 y0 () const { return m_y0; }

        /**
         * @brief Get width of the area
         * @return Width value
         */
        uint16 width () const { return m_width; }

        /**
         * @brief Get height of the area
         * @return Height value
         */
        uint16 height () const { return m_height; }

        /**
         * @brief Set X coordinate of start point
         * @param x0: Coordinate value
         */
        void SetX0 (int16 x0) { m_x0 = x0; }

        /**
         * @brief Set Y coordinate of start point
         * @param y0: Coordinate value
         */
        void SetY0 (int16 y0) { m_y0 = y0; }

        /**
         * @brief Set width of the area
         * @param width: Width value
         */
        void SetWidth (uint16 width) { m_width = width; }

        /**
         * @brief Set height of the area
         * @param height: Height value
         */
        void SetHeight (uint16 height) { m_height = height; }

        /**
         * @brief Check if the area has intersection with specified one
         * @param area: Area to check intersection with
         * @return True - Areas have intersection otherwise - false
         */
        bool HasIntersection (const CArea& area) const;

        /**
         * @brief Check if the area contains the point
         * @param x: X coordinate of the point
         * @param y: Y coordinate of the point
         * @return True - The area includes the point, otherwise - false
         */
        bool Contains (int16 x, int16 y) const;

        /**
         * @brief Find intersection with specified area
         * @param area: Area to find intersection with
         * @return Intersection area
         */
        CArea Intersection (const CArea& area) const;

        /**
         * @brief Extend the area to include specified point
         * @param x: X coordinate of the point to include
         * @param y: Y coordinate of the point to include
         */
        void Extend (int16 x, int16 y);

        /**
         * @brief Join areas. Increase the area to include specified one.
         * @param area: Area to include
         */
        void Join (const CArea& area);

    private:
        int16 m_x0;
        int16 m_y0;
        uint16 m_width;
        uint16 m_height;
};
