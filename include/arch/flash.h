//*****************************************************************************
//
// @brief   Internal flash
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Writing modes
    enum EFlashWrite
    {
        EWRITE_WITH_EDIT         = 0,   // All unchanged data on a sector will be saved
        EWRITE_WITH_ERASE               // All data on transition to new sector will be earased
    };


// ----------------------------------------------------------------------------
// Flash
// ----------------------------------------------------------------------------

class FLASH
{
    public:
        /**
          * @brief  Internal flash module initialization
          * @param  None
          */
        static void Init (void);

        /**
          * @brief  Save data in internal flash memory
          * @param  addr: Start address to save data
          * @param  data: Pointer to data
          * @param  size: Data size
          * @param  mode: Writing mode
          */
        static int Write (uint addr, void* data, uint size, EFlashWrite mode = EWRITE_WITH_EDIT);
};
