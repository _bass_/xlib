//*****************************************************************************
//
// @brief   SPI driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default clock frequency (Hz)
    #define SPI_DEFAULT_SPEED           1000000


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Data format
    enum ESpiFormat
    {
        SPI_FORMAT_MSB      = 0,        // MSB first
        SPI_FORMAT_LSB      = 1         // LSB first
    };

    // SCK signal polarity
    enum ESpiCpol
    {
        SPI_CPOL_0          = 0,        // SCK = 0 during IDLE
        SPI_CPOL_1          = 1         // SCK = 1 during IDLE
    };

    // SCK signal phase
    enum ESpiCpha
    {
        SPI_CPHA_0          = 0,        // Data sampling on front of SCK
        SPI_CPHA_RISE       = 0,        // Data sampling on front of SCK
        SPI_CPHA_1          = 1,        // Data sampling on fall of SCK
        SPI_CPHA_FALL       = 1         // Data sampling on fall of SCK
    };

    // Configuration options
    typedef struct
    {
        uint            speed;          // Frequency of SCK
        ESpiFormat      format;         // Data format
        ESpiCpol        cpol;           // SCK signal polarity
        ESpiCpha        cpha;           // SCK signal phase
    } TSpiOptions;
