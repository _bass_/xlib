//*****************************************************************************
//
// @brief   IAR compiler macros
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #define _IAR_STR(s)                             #s
    #define _IAR_PRAGMA_SECTION(s)                  _IAR_STR(section=#s)
    #define CC_SECTION_DECLARE(name)                _Pragma( _IAR_PRAGMA_SECTION(name) )
    #define CC_SECTION_ADDR_START(name)             __section_begin(#name)
    #define CC_SECTION_ADDR_END(name)               __section_end(#name)

    #define CC_VAR_SECTION(name)                    @ #name
    #define CC_VAR_WEAK_DECLARATION                 __weak
    #define CC_VAR_USED_DECLARATION                 __root
