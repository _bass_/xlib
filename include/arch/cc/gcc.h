//*****************************************************************************
//
// @brief   GCC compiler macros
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #define GCC_SECTION_CONCAT(prefix, name)        prefix##name

    #define CC_SECTION_DECLARE(name)                extern void* GCC_SECTION_CONCAT(__start_, name); \
                                                    extern void* GCC_SECTION_CONCAT(__stop_, name);

    #define CC_SECTION_ADDR_START(name)             ( (void*) &GCC_SECTION_CONCAT(__start_, name) )
    #define CC_SECTION_ADDR_END(name)               ( (void*) &GCC_SECTION_CONCAT(__stop_, name) )

    #define CC_VAR_SECTION(name)                    __attribute__ ((section(#name), used))
    #define CC_VAR_WEAK_DECLARATION                 __attribute__ ((weak))
    #define CC_VAR_USED_DECLARATION                 __attribute__ ((used))
