//*****************************************************************************
//
// @brief   Compiler abstraction
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#if defined(__GNUC__)
    #include "cc/gcc.h"
#elif defined(__ICCARM__)
    #include "cc/iar.h"
#else
    #error "Unknown compiler"
#endif


#define _CC_DO_CAT(a, b)                                a##b
#define _CC_CAT(a, b)                                   _CC_DO_CAT(a, b)
#define _CC_VAR_NAME(g)                                 _CC_CAT(_CC_CAT(_ptr_,g), __COUNTER__)

/**
 * @brief  Macro to iterate through all registered items.
 * @param  sectionName: Section name (only name, without any quotes)
 * @param  dataType: Type of registered value
 * @param  itrName: Iterator variable name (pointer to the value)
 */
#define SECTION_FOREACH(sectionName, dataType, itrName) CC_SECTION_DECLARE(sectionName) \
                                                        for ( \
                                                            dataType* itrName = (dataType*)CC_SECTION_ADDR_START(sectionName); \
                                                            itrName && itrName < (dataType*)CC_SECTION_ADDR_END(sectionName); \
                                                            ++itrName \
                                                        )

/**
  * @brief  Allows to combine all registered items in a single group (specified by 'group' parameter, ).
  *         SECTION_FOREACH() might be used to iterate through all registered items.
  * @param  group: Group name (only name, without any quotes)
  * @param  type: Type of value to register
  * @param  ...: Value to register. It might be a pointer or in-place structure initialization (like {1, 2, 3})
  */
#define REGISTER_ITEM(group, type, ...)                 static type _CC_VAR_NAME(group) CC_VAR_SECTION(group) = __VA_ARGS__
