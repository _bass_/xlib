//*****************************************************************************
//
// @brief   UART driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default configuration parameters
    #define USART_DEFAULT_BAUDRATE              115200
    #define USART_DEFAULT_DATABITS              8
    #define USART_DEFAULT_PARITY                USART_PARITY_NONE
    #define USART_DEFAULT_STOPBITS              USART_STOPBITS_1


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    enum EUsartParity
    {
        USART_PARITY_NONE   = 0,
        USART_PARITY_ODD    = 1,
        USART_PARITY_EVEN   = 2,
    };

    enum EUsartStopbits
    {
        USART_STOPBITS_1    = 0,
        USART_STOPBITS_1_5  = 1,
        USART_STOPBITS_2    = 2,
    };

    // Configuration parameters
    typedef struct
    {
        uint            baudrate;
        uint            databits;
        EUsartStopbits  stopbits;
        EUsartParity    parity;
        TGpio           rts;
    } TUsartOptions;
