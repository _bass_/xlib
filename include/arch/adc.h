//*****************************************************************************
//
// @brief   ADC device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/device.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Common Ioctl codes
    #define IOCTL_ADC_GET_VDD                   IOCTL_CODE(10, 0)
    #define IOCTL_ADC_GET_TEMP                  IOCTL_CODE(10, 1)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // ADC channel descriptor
    typedef struct
    {
        uint    channel;                // Channel number
        TGpio   pin;                    // Channel GPIO
    } TAdcChDesc;

    // Configuration parameters
    typedef struct
    {
        uint                resolution; // Measurement resolution
        uint                freq;       // Converting frequency (0 - only single time conversion)
        uint                count;      // Number of descriptors in the descriptor list
        const TAdcChDesc*   channels;   // Pointer to ADC channels descriptor list
    } TAdcOptions;


// ----------------------------------------------------------------------------
///                                 CAdc
// ----------------------------------------------------------------------------

class CAdc : public CDevice
{
    public:
        /**
          * @brief  Constructor
          * @param  id: Device ID
          */
        CAdc (uint id) : m_id(id) {};
        
        /**
          * @brief  Read result of conversion (code)
          * @param  channel: Channel index
          * @return Conversion result (code) or negative error code
          */
        virtual int Read (uint channel) = 0;

        /**
          * @brief  Read result of conversion (voltage)
          * @param  channel: Channel index
          * @param  pValue: Variable to save conversion result
          * @return 0 or negative error code
          */
        virtual int Read (uint channel, float* pValue) = 0;
        
    protected:
        uint    m_id;
};
