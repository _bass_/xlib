//*****************************************************************************
//
// @brief   stdio device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// CDevStdio
// ----------------------------------------------------------------------------

class CDevStdio : public CDevChar
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CDevStdio ();

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Read device state
          * @param  None
          * @return Device state
          */
        TDevState GetState (void) const;

        /**
          * @brief  Send character to output stream
          * @param  c: character
          * @return 0 or negative error code
          */
        int PutChar (const char c);

        /**
          * @brief  Get character from input stream
          * @return Character or negative error code
          */
        int GetChar (void);

        /**
          * @brief  Send data to output stream
          * @param  data: Data
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* buff, uint size);

        /**
          * @brief  Read data from input stream
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Written data size or negative error code
          */
        int Read (void* buff, uint size);
};
