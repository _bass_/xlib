//*****************************************************************************
//
// @brief   Generic system functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    /**
      * @brief  Delay (microseconds)
      * @param  time: delay value (microseconds)
      */
    void delay_us (uint time);

    /**
      * @brief  Delay (milliseconds)
      * @param  time: delay value (milliseconds)
      */
    void delay_ms (uint time);

    /**
      * @brief  Read current ticks counter value
      * @return Ticks counter value
      */
    uint clock (void);

    /**
      * @brief  Perform CPU software reset
      * @param  None
      */
    void cpu_reset (void);

    /**
      * @brief  Restart reason detection
      * @param  pStr: Variable to save restart reason string
      * @return Restart reason code (platform depended) or negative error code
      */
    int getReasonRestart (const char** pStr = NULL);
