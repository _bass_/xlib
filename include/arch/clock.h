//*****************************************************************************
//
// @brief   Clock interaction interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Clock modes (profiles)
    typedef enum
    {
        CLK_MODE_HIGH,
        CLK_MODE_LOW
    } TClockMode;


// ----------------------------------------------------------------------------
// CLOCK
// ----------------------------------------------------------------------------

class CLOCK
{
    public:
        /**
          * @brief  Clock system initialization
          * @param  None
          */
        static void Init (void);

        /**
          * @brief  Peripheral module clock control
          * @param  id: Peripheral module ID
          * @param  state: State to set for the module (true - on, false - off)
          */
        static void Periph (uint id, bool state);
        
        /**
          * @brief  Get current CPU clock frequency
          * @param  None
          * @return Clock frequency (Hz)
          */
        static uint32 GetCpuFreq (void);
        
        /**
          * @brief  Set clock mode (profile)
          * @param  mode: Clock mode to set
          */
        static void SetMode (TClockMode mode);
};
