//*****************************************************************************
//
// @brief   System timer interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/irq.h"


// ----------------------------------------------------------------------------
// SYSTMR
// ----------------------------------------------------------------------------

class SYSTMR
{
    public:

        /**
          * @brief  Timer initialization
          * @param  freq: Timer frequency (Hz)
          */
        static void Init (uint freq);

        /**
          * @brief  Start timer
          * @param  None
          */
        static void Start (void);
        
        /**
          * @brief  Stop timer
          * @param  None
          */
        static void Stop (void);

        /**
          * @brief  Set timer IRQ (tick) handler
          * @param  handler: IRQ handler
          */
        static void SetHandler (THandlerISR* handler);
};
