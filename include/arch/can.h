//*****************************************************************************
//
// @brief   CAN device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define CAN_ID_STD_MASK         0x000007FF
    #define CAN_ID_EXT_MASK         0x1FFFFFFF


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Frame ID structure
    typedef struct
    {
        uint32  id          : 29;   // ID 11/29 bits
        uint32  ide         : 1;    // Flag of extended ID (29 bits)
        uint32  rtr         : 1;    // Flag of Remote Transmission Request (data request)
        uint32  reserved    : 1;
    } TCanId;

    // Frame structure
    typedef struct
    {
        TCanId      addr;
        uint8       len;
        uint8       data[8];
    } TCanFrame;
