//*****************************************************************************
//
// @brief   Watchdog driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// WDT
// ----------------------------------------------------------------------------

class WDT
{
    public:
        /**
          * @brief  Watchdog initialization
          * @param  timeout: Watchdog timeout (seconds)
          */
        static int Open (uint timeout);

        /**
          * @brief  Watchdog deinitialization (stop)
          * @return 0 or negative error code
          */
        static int Close (void);

        /**
          * @brief  Watchdog restart (renew counter)
          * @param  None
          */
        static void Restart (void);
};
