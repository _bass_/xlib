//*****************************************************************************
//
// @brief   I2C device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------


    // Direction bit mask for address (1 - read, 0 - write)
    #define I2C_ADDR_MASK_RW                0x01


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Address format
    enum EI2cAddrFormat
    {
        I2C_ADDR_FORMAT_7BIT    = 0,    // 7 bit
        I2C_ADDR_FORMAT_10BIT   = 1     // 10 bit
    };

    // Configuration options
    typedef struct
    {
        uint            speed;          // Bus speed (SCK frequency, Hz)
        uint            addr;           // Self address on the bus
        EI2cAddrFormat  addrFormat;     // Address format
    } TI2cOptions;
