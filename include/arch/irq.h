//*****************************************************************************
//
// @brief   IRQ subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // IRQ handler
    typedef void (THandlerISR)(void);

    // IRQ table vercor descriptor
    typedef union
    {
        THandlerISR*    hdl;
        void*           ptr;
    } TIsr;

    // IRQ priority levels
    typedef enum
    {
        IRQ_PRIORITY_HIGHEST = 0,
        IRQ_PRIORITY_HIGH    = 1,
        IRQ_PRIORITY_MID     = 2,
        IRQ_PRIORITY_LOW     = 3,
        IRQ_PRIORITY_LOWEST  = 4
    } TIntPriority;


// ----------------------------------------------------------------------------
// IRQ
// ----------------------------------------------------------------------------

class IRQ
{
    public:
        /**
          * @brief  IRQ subsystem initialization
          * @param  None
          */
        static void Init (void);

        /**
          * @brief  Set / remove IRQ handler
          * @param  irqId: IRQ vector ID
          * @param  handler: IRQ handler to set or NULL to delete
          * @param  priority: IRQ priority
          */
        static void SetHandler (uint irqId, THandlerISR* handler, uint priority = IRQ_PRIORITY_LOWEST);

        /**
          * @brief  Set IRQ handler priority
          * @param  irqId: IRQ vector ID
          * @param  priority: IRQ priority
          */
        static void SetPriority (uint irqId, uint priority);

        /**
          * @brief  Enable all IRQ
          * @param  None
          */
        static void Enable (void);

        /**
          * @brief  Enable specified IRQ
          * @param  irqId: IRQ vector ID
          */
        static void Enable (uint irqId);

        /**
          * @brief  Disable all IRQ
          * @param  None
          */
        static void Disable (void);

        /**
          * @brief  Disable specified IRQ
          * @param  irqId: IRQ vector ID
          */
        static void Disable (uint irqId);

        /**
          * @brief  Get active IRQ vector ID
          * @return Active IRQ vector ID or IRQ_NONE (there is no active IRQ)
          */
        static uint GetActive (void);
        
        /**
          * @brief  Temporary IRQ disabling with saving current IRQ state
          * @param  None
          */
        static void Lock (void);

        /**
          * @brief  Restore IRQ state saved in Lock()
          * @param  None
          */
        static void Unlock (void);
};
