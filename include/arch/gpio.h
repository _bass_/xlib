//*****************************************************************************
//
// @brief   GPIO control
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // GPIO modes
    #define GPIO_MODE_MASK          (0xF0000000)
    #define GPIO_MODE_IN            (0x10000000)
    #define GPIO_MODE_OUT           (0x20000000)
    #define GPIO_MODE_OD            (0x30000000)
    #define GPIO_MODE_ANALOG        (0x40000000)

    // Pull-up/pull-down resistors control
    #define GPIO_OPT_MASK           (0x0F000000)
    #define GPIO_OPT_PUP            (0x01000000)
    #define GPIO_OPT_PUD            (0x02000000)

    // Additonal platform-related options
    #define GPIO_ARCH_MASK          (0x00FFFFFF)
    // Speed/AF...

    // GPIO IRQ types
    #define GPIO_IRQ_RISING         (1 << 0)
    #define GPIO_IRQ_FALLING        (1 << 1)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // IRQ callback function
    typedef void (TGpioIrqCb) (TGpio pin);


// ----------------------------------------------------------------------------
// GPIO
// ----------------------------------------------------------------------------

class GPIO
{
    public:
        /**
          * @brief  Set pin configuration
          * @param  gpio: GPIO to configure
          * @param  params: Configuration (see GPIO_MODE_xx, GPIO_OPT_xx, GPIO_ARCH_xx macros)
          */
        static void Cfg (TGpio gpio, uint params);

        /**
          * @brief  Configure pin IRQ
          * @param  gpio: GPIO to configure
          * @param  mode: IRQ mode
          * @param  hdl: IRQ handler
          */
        static void CfgIrq (TGpio gpio, uint mode, TGpioIrqCb* hdl);

        /**
          * @brief  Set pin state
          * @param  gpio: GPIO to use
          * @param  state: GPIO state
          */
        static void Set (TGpio gpio, bool state);

        /**
          * @brief  Set pin state to 1
          * @param  gpio: GPIO to use
          */
        static void Set (TGpio gpio);

        /**
          * @brief  Set pin state to 0
          * @param  gpio: GPIO to use
          */
        static void Clear (TGpio gpio);

        /**
          * @brief  Toggle pin state
          * @param  gpio: GPIO to use
          */
        static void Invert (TGpio gpio);

        /**
          * @brief  Get pin current state
          * @param  gpio: GPIO to use
          * @return Pin state
          */
        static bool Get (TGpio gpio);
};
