//************************************************************************
//
// @brief   Cortex-M4 core registers
// @author  Vadim Mezhlumov
//
//************************************************************************
#pragma once

#include "arch/core/cm4/irq.h"


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    // First peripheral IRQ vector index
    #define IRQ_PERIPH_START_INDEX      16

    #define IRQ_ID_NMI                  2
    #define IRQ_ID_HARD_FAULT           3
    #define IRQ_ID_MEM_FAULT            4
    #define IRQ_ID_BUS_FAULT            5
    #define IRQ_ID_USAGE_FAULT          6
    #define IRQ_ID_SVC                  11
    #define IRQ_ID_DEBUG_MON            12
    #define IRQ_ID_PEND_SV              14
    #define IRQ_ID_SYSTICK              15

    #define  __CAST(type, value)            ( (type)(value) )


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // SysTick
    typedef struct
    {
        TReg32          CTRL;
        TReg32          LOAD;
        TReg32          VAL;
        TReg32          CALIB;
    } TCm4SysTick;

    // NVIC
    typedef struct
    {
        TReg32          ISER[3];
        TReg32          Reserved_0[29];
        TReg32          ICER[3];
        TReg32          Reserved_1[29];
        TReg32          ISPR[3];
        TReg32          Reserved_2[29];
        TReg32          ICPR[3];
        TReg32          Reserved_3[29];
        TReg32          IABR[3];
        TReg32          Reserved_4[61];
        TReg8           IP[128];
        TReg32          Reserved_5[695];
        TReg32          STIR;
    } TCm4Nvic;

    // SCB
    typedef struct
    {
        TReg32          CPUID;
        TReg32          ICSR;
        TReg32          VTOR;
        TReg32          AIRCR;
        TReg32          SCR;
        TReg32          CCR;
        TReg8           SHP[12];
        TReg32          SHCSR;
        TReg32          CFSR;
        TReg32          HFSR;
        TReg32          Reserved;
        TReg32          MMAR;
        TReg32          BFAR;
        TReg32          AFSR;
    } TCm4Scb;

    // FPU
    typedef struct
    {
        TReg32          CPACR;
        TReg32          Reserved[106];
        TReg32          FPCCR;
        TReg32          FPCAR;
        TReg32          AIRCR;
        TReg32          FPDSCR;
        // TReg32          FPSCR;      // Not mapped
    } TCm4Fpu;

    // MPU
    typedef struct
    {
        TReg32          TYPER;
        TReg32          CTRL;
        TReg32          RNR;
        TReg32          RBAR;
        TReg32          RASR;
        TReg32          RBAR_A1;
        TReg32          RASR_A1;
        TReg32          RBAR_A2;
        TReg32          RASR_A2;
        TReg32          RBAR_A3;
        TReg32          RASR_A3;
    } TCm4Mpu;

    // DWT
    typedef struct
    {
        TReg32          CTRL;
        TReg32          CYCCNT;
        TReg32          CPICNT;
        TReg32          EXCCNT;
        TReg32          SLEEPCNT;
        TReg32          LSUCNT;
        TReg32          FOLDCNT;
        TReg32          PCSR;
        TReg32          COMP0;
        TReg32          MASK0;
        TReg32          FUNCTION0;
        TReg32          RESERVED_0;
        TReg32          COMP1;
        TReg32          MASK1;
        TReg32          FUNCTION1;
        TReg32          RESERVED_1;
        TReg32          COMP2;
        TReg32          MASK2;
        TReg32          FUNCTION2;
        TReg32          RESERVED_3;
        TReg32          COMP3;
        TReg32          MASK3;
        TReg32          FUNCTION3;
    } TCm4Dwt;

    // Core Debug
    typedef struct
    {
        TReg32          DHCSR;
        TReg32          DCRSR;
        TReg32          DCRDR;
        TReg32          DEMCR;
    } TCm4CoreDbg;

    // ITM
    typedef struct
    {
        TReg32          PORT[32U];
        TReg32          RESERVED0[864];                                       
        TReg32          TER;
        TReg32          RESERVED1[15U];                                       
        TReg32          TPR;
        TReg32          RESERVED2[15U];                                               
        TReg32          TCR;
        TReg32          RESERVED3[29U];                                               
        TReg32          IWR;
        TReg32          IRR;
        TReg32          IMCR;
        TReg32          RESERVED4[43U];                                               
        TReg32          LAR;
        TReg32          LSR;
        TReg32          RESERVED5[6U];                                                
        TReg32          PID4;
        TReg32          PID5;
        TReg32          PID6;
        TReg32          PID7;
        TReg32          PID0;
        TReg32          PID1;
        TReg32          PID2;
        TReg32          PID3;
        TReg32          CID0;
        TReg32          CID1;
        TReg32          CID2;
        TReg32          CID3;
    } TCm4Itm;


// ----------------------------------------------------------------------------
// Registers
// ----------------------------------------------------------------------------

    #define  CM4_SYSTICK            __CAST(TCm4SysTick*,      0xE000E010u)
    #define  CM4_NVIC               __CAST(TCm4Nvic*,         0xE000E100u)
    #define  CM4_SCB                __CAST(TCm4Scb*,          0xE000ED00u)
    #define  CM4_FPU                __CAST(TCm4Fpu*,          0xE000ED88u)
    #define  CM4_MPU                __CAST(TCm4Mpu*,          0xE000ED90u)
    #define  CM4_CORE_DBG           __CAST(TCm4CoreDbg*,      0xE000EDF0u)
    #define  CM4_DWT                __CAST(TCm4Dwt*,          0xE0001000u)
    #define  CM4_ITM                __CAST(TCm4Itm*,          0xE0000000u)
    

// ----------------------------------------------------------------------------
// Registers fields
// ----------------------------------------------------------------------------

    // SysTick
    #define SYSTICK_CTRL_COUNTFLAG_POS          16
    #define SYSTICK_CTRL_COUNTFLAG_MSK          (1UL << SYSTICK_CTRL_COUNTFLAG_POS)
    #define SYSTICK_CTRL_CLKSOURCE_POS          2
    #define SYSTICK_CTRL_CLKSOURCE_MSK          (1UL << SYSTICK_CTRL_CLKSOURCE_POS)
    #define SYSTICK_CTRL_TICKINT_POS            1
    #define SYSTICK_CTRL_TICKINT_MSK            (1UL << SYSTICK_CTRL_TICKINT_POS)
    #define SYSTICK_CTRL_ENABLE_POS             0
    #define SYSTICK_CTRL_ENABLE_MSK             (1UL << SYSTICK_CTRL_ENABLE_POS)
    
    #define SYSTICK_LOAD_RELOAD_POS             0
    #define SYSTICK_LOAD_RELOAD_MSK             (0xFFFFFFUL << SYSTICK_LOAD_RELOAD_POS)
    
    #define SYSTICK_VAL_CURRENT_POS             0
    #define SYSTICK_VAL_CURRENT_MSK             (0xFFFFFFUL << SYSTICK_VAL_CURRENT_POS)
    
    #define SYSTICK_CALIB_TENMS_POS             0
    #define SYSTICK_CALIB_TENMS_MSK             (0xFFFFFFUL << SYSTICK_VAL_CURRENT_POS)
    
    // SCB
    #define SCB_CPUID_IMPLEMENTER_POS          24
    #define SCB_CPUID_IMPLEMENTER_MSK          (0xFFUL << SCB_CPUID_IMPLEMENTER_POS)
    #define SCB_CPUID_VARIANT_POS              20
    #define SCB_CPUID_VARIANT_MSK              (0xFUL << SCB_CPUID_VARIANT_POS)
    #define SCB_CPUID_ARCHITECTURE_POS         16
    #define SCB_CPUID_ARCHITECTURE_MSK         (0xFUL << SCB_CPUID_ARCHITECTURE_POS)
    #define SCB_CPUID_PARTNO_POS                4
    #define SCB_CPUID_PARTNO_MSK               (0xFFFUL << SCB_CPUID_PARTNO_POS)
    #define SCB_CPUID_REVISION_POS              0
    #define SCB_CPUID_REVISION_MSK             (0xFUL << SCB_CPUID_REVISION_POS)

    #define SCB_ICSR_NMIPENDSET_POS            31
    #define SCB_ICSR_NMIPENDSET_MSK            (1UL << SCB_ICSR_NMIPENDSET_POS)
    #define SCB_ICSR_PENDSVSET_POS             28
    #define SCB_ICSR_PENDSVSET_MSK             (1UL << SCB_ICSR_PENDSVSET_POS)
    #define SCB_ICSR_PENDSVCLR_POS             27
    #define SCB_ICSR_PENDSVCLR_MSK             (1UL << SCB_ICSR_PENDSVCLR_POS)
    #define SCB_ICSR_PENDSTSET_POS             26
    #define SCB_ICSR_PENDSTSET_MSK             (1UL << SCB_ICSR_PENDSTSET_POS)
    #define SCB_ICSR_PENDSTCLR_POS             25
    #define SCB_ICSR_PENDSTCLR_MSK             (1UL << SCB_ICSR_PENDSTCLR_POS)
    #define SCB_ICSR_ISRPENDING_POS            22
    #define SCB_ICSR_ISRPENDING_MSK            (1UL << SCB_ICSR_ISRPENDING_POS)
    #define SCB_ICSR_VECTPENDING_POS           12
    #define SCB_ICSR_VECTPENDING_MSK           (0x1FFUL << SCB_ICSR_VECTPENDING_POS)
    #define SCB_ICSR_RETTOBASE_POS             11
    #define SCB_ICSR_RETTOBASE_MSK             (1UL << SCB_ICSR_RETTOBASE_POS)
    #define SCB_ICSR_VECTACTIVE_POS             0
    #define SCB_ICSR_VECTACTIVE_MSK            (0x1FFUL << SCB_ICSR_VECTACTIVE_POS)

    #define SCB_VTOR_TBLOFF_POS                9
    #define SCB_VTOR_TBLOFF_MSK                (0x1FFFFFUL << SCB_VTOR_TBLOFF_POS)
    #define SCB_VTOR_TBLBASE_POS               29
    #define SCB_VTOR_TBLBASE_MSK               (0x1ul << SCB_VTOR_TBLBASE_POS)

    #define SCB_AIRCR_VECTKEY_POS              16
    #define SCB_AIRCR_VECTKEY_MSK              (0xFFFFUL << SCB_AIRCR_VECTKEY_POS)
    #define SCB_AIRCR_VECTKEYSTAT_POS          16
    #define SCB_AIRCR_VECTKEYSTAT_MSK          (0xFFFFUL << SCB_AIRCR_VECTKEYSTAT_POS)
    #define SCB_AIRCR_ENDIANESS_POS            15
    #define SCB_AIRCR_ENDIANESS_MSK            (1UL << SCB_AIRCR_ENDIANESS_POS)
    #define SCB_AIRCR_PRIGROUP_POS              8
    #define SCB_AIRCR_PRIGROUP_MSK             (7UL << SCB_AIRCR_PRIGROUP_POS)

    #define NVIC_PRIORITY_GROUP_0               ((uint32)0x700)
    #define NVIC_PRIORITY_GROUP_1               ((uint32)0x600)
    #define NVIC_PRIORITY_GROUP_2               ((uint32)0x500)
    #define NVIC_PRIORITY_GROUP_3               ((uint32)0x400)
    #define NVIC_PRIORITY_GROUP_4               ((uint32)0x300)

    #define SCB_AIRCR_SYSRESETREQ_POS           2
    #define SCB_AIRCR_SYSRESETREQ_MSK          (1UL << SCB_AIRCR_SYSRESETREQ_POS)
    #define SCB_AIRCR_VECTCLRACTIVE_POS         1
    #define SCB_AIRCR_VECTCLRACTIVE_MSK        (1UL << SCB_AIRCR_VECTCLRACTIVE_POS)
    #define SCB_AIRCR_VECTRESET_POS             0
    #define SCB_AIRCR_VECTRESET_MSK            (1UL << SCB_AIRCR_VECTRESET_POS)

    #define SCB_SCR_SEVONPEND_POS               4
    #define SCB_SCR_SEVONPEND_MSK              (1UL << SCB_SCR_SEVONPEND_POS)
    #define SCB_SCR_SLEEPDEEP_POS               2
    #define SCB_SCR_SLEEPDEEP_MSK              (1UL << SCB_SCR_SLEEPDEEP_POS)
    #define SCB_SCR_SLEEPONEXIT_POS             1
    #define SCB_SCR_SLEEPONEXIT_MSK            (1UL << SCB_SCR_SLEEPONEXIT_POS)

    #define SCB_CCR_STKALIGN_POS                9
    #define SCB_CCR_STKALIGN_MSK               (1UL << SCB_CCR_STKALIGN_POS)
    #define SCB_CCR_BFHFNMIGN_POS               8
    #define SCB_CCR_BFHFNMIGN_MSK              (1UL << SCB_CCR_BFHFNMIGN_POS)
    #define SCB_CCR_DIV_0_TRP_POS               4
    #define SCB_CCR_DIV_0_TRP_MSK              (1UL << SCB_CCR_DIV_0_TRP_POS)
    #define SCB_CCR_UNALIGN_TRP_POS             3
    #define SCB_CCR_UNALIGN_TRP_MSK            (1UL << SCB_CCR_UNALIGN_TRP_POS)
    #define SCB_CCR_USERSETMPEND_POS            1
    #define SCB_CCR_USERSETMPEND_MSK           (1UL << SCB_CCR_USERSETMPEND_POS)
    #define SCB_CCR_NONBASETHRDENA_POS          0
    #define SCB_CCR_NONBASETHRDENA_MSK         (1UL << SCB_CCR_NONBASETHRDENA_POS)

    #define SCB_SHCSR_USGFAULTENA_POS          18
    #define SCB_SHCSR_USGFAULTENA_MSK          (1UL << SCB_SHCSR_USGFAULTENA_POS)
    #define SCB_SHCSR_BUSFAULTENA_POS          17
    #define SCB_SHCSR_BUSFAULTENA_MSK          (1UL << SCB_SHCSR_BUSFAULTENA_POS)
    #define SCB_SHCSR_MEMFAULTENA_POS          16
    #define SCB_SHCSR_MEMFAULTENA_MSK          (1UL << SCB_SHCSR_MEMFAULTENA_POS)
    #define SCB_SHCSR_SVCALLPENDED_POS         15
    #define SCB_SHCSR_SVCALLPENDED_MSK         (1UL << SCB_SHCSR_SVCALLPENDED_POS)
    #define SCB_SHCSR_BUSFAULTPENDED_POS       14
    #define SCB_SHCSR_BUSFAULTPENDED_MSK       (1UL << SCB_SHCSR_BUSFAULTPENDED_POS)
    #define SCB_SHCSR_MEMFAULTPENDED_POS       13
    #define SCB_SHCSR_MEMFAULTPENDED_MSK       (1UL << SCB_SHCSR_MEMFAULTPENDED_POS)
    #define SCB_SHCSR_USGFAULTPENDED_POS       12
    #define SCB_SHCSR_USGFAULTPENDED_MSK       (1UL << SCB_SHCSR_USGFAULTPENDED_POS)
    #define SCB_SHCSR_SYSTICKACT_POS           11
    #define SCB_SHCSR_SYSTICKACT_MSK           (1UL << SCB_SHCSR_SYSTICKACT_POS)
    #define SCB_SHCSR_PENDSVACT_POS            10
    #define SCB_SHCSR_PENDSVACT_MSK            (1UL << SCB_SHCSR_PENDSVACT_POS)
    #define SCB_SHCSR_MONITORACT_POS            8
    #define SCB_SHCSR_MONITORACT_MSK           (1UL << SCB_SHCSR_MONITORACT_POS)
    #define SCB_SHCSR_SVCALLACT_POS             7
    #define SCB_SHCSR_SVCALLACT_MSK            (1UL << SCB_SHCSR_SVCALLACT_POS)
    #define SCB_SHCSR_USGFAULTACT_POS           3
    #define SCB_SHCSR_USGFAULTACT_MSK          (1UL << SCB_SHCSR_USGFAULTACT_POS)
    #define SCB_SHCSR_BUSFAULTACT_POS           1
    #define SCB_SHCSR_BUSFAULTACT_MSK          (1UL << SCB_SHCSR_BUSFAULTACT_POS)
    #define SCB_SHCSR_MEMFAULTACT_POS           0
    #define SCB_SHCSR_MEMFAULTACT_MSK          (1UL << SCB_SHCSR_MEMFAULTACT_POS)

    // ITM
    #define ITM_TCR_BUSY_POS                   23U
    #define ITM_TCR_BUSY_MSK                   (1UL << ITM_TCR_BUSY_Pos)
    #define ITM_TCR_TRACEBUSID_POS             16U
    #define ITM_TCR_TRACEBUSID_MSK             (0x7FUL << ITM_TCR_TRACEBUSID_POS)
    #define ITM_TCR_GTSFREQ_POS                10U
    #define ITM_TCR_GTSFREQ_MSK                (3UL << ITM_TCR_GTSFREQ_POS)
    #define ITM_TCR_TSPRESCALE_POS              8U
    #define ITM_TCR_TSPRESCALE_MSK             (3UL << ITM_TCR_TSPRESCALE_POS)
    #define ITM_TCR_SWOENA_POS                  4U
    #define ITM_TCR_SWOENA_MSK                 (1UL << ITM_TCR_SWOENA_POS)
    #define ITM_TCR_DWTENA_POS                  3U
    #define ITM_TCR_DWTENA_MSK                 (1UL << ITM_TCR_DWTENA_POS)
    #define ITM_TCR_SYNCENA_POS                 2U
    #define ITM_TCR_SYNCENA_MSK                (1UL << ITM_TCR_SYNCENA_POS)
    #define ITM_TCR_TSENA_POS                   1U
    #define ITM_TCR_TSENA_MSK                  (1UL << ITM_TCR_TSENA_POS)
    #define ITM_TCR_ITMENA_POS                  0U
    #define ITM_TCR_ITMENA_MSK                 (1UL << ITM_TCR_ITMENA_POS)

    // Core Debug
    #define CORE_DEBUG_DEMCR_TRACE_ENABLE       (1UL << 24)
