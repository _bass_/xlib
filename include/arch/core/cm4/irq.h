//*****************************************************************************
//
// @brief   Cortex-M4 IRQ
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/irq.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default priorities
    #define IRQ_PRIORITY_DMA                    IRQ_PRIORITY_HIGHEST
    #define IRQ_PRIORITY_USB                    IRQ_PRIORITY_HIGH
    #define IRQ_PRIORITY_ADC                    IRQ_PRIORITY_MID
    #define IRQ_PRIORITY_USART                  IRQ_PRIORITY_MID
