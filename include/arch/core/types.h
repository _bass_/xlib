//*****************************************************************************
//
// @brief   Base types
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Standard types aliases
    typedef unsigned char       uchar;
    typedef wchar_t             wchar;
    typedef unsigned int        uint;
    typedef unsigned long       ulong;

    typedef uint8_t             uint8;
    typedef int8_t              int8;
    typedef uint16_t            uint16;
    typedef int16_t             int16;
    typedef uint32_t            uint32;
    typedef int32_t             int32;
    typedef uint64_t            uint64;
    typedef int64_t             int64;
    
    // Registers
    typedef volatile uint8_t    TReg8;
    typedef volatile uint16_t   TReg16;
    typedef volatile uint32_t   TReg32;
