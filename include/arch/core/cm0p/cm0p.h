//*****************************************************************************
//
// @brief   Cortex-M0+ core registers
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/core/cm0p/irq.h"


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    // First peripheral IRQ vector index
    #define IRQ_PERIPH_START_INDEX      16

    #define IRQ_ID_NMI                  2
    #define IRQ_ID_HARD_FAULT           3
    #define IRQ_ID_SVC                  11
    #define IRQ_ID_PEND_SV              14
    #define IRQ_ID_SYSTICK              15

    #define  __CAST(type, value)        ( (type)(value) )


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // SysTick
    typedef struct
    {
        TReg32          CTRL;
        TReg32          LOAD;
        TReg32          VAL;
        TReg32          CALIB;
    } Cm0pSysTick;

    // NVIC
    typedef struct
    {
        TReg32          ISER;
        TReg32          Reserved_0[31];
        TReg32          ICER;
        TReg32          Reserved_1[31];
        TReg32          ISPR;
        TReg32          Reserved_2[31];
        TReg32          ICPR;
        TReg32          Reserved_3[95];
        TReg32          IP[8];
    } Cm0pNvic;

    // SCB
    typedef struct
    {
        TReg32          CPUID;
        TReg32          ICSR;
        TReg32          VTOR;
        TReg32          AIRCR;
        TReg32          SCR;
        TReg32          CCR;
        TReg32          RESERVED;
        TReg32          SHP2;
        TReg32          SHP3;
    } Cm0pScb;

    // Core Debug
    typedef struct
    {
        TReg32          DHCSR;
        TReg32          DCRSR;
        TReg32          DCRDR;
        TReg32          DEMCR;
    } Cm0pCoreDbg;


// ----------------------------------------------------------------------------
// Registers
// ----------------------------------------------------------------------------

    #define CM0P_SYSTICK            __CAST(Cm0pSysTick*,    0xE000E010u)
    #define CM0P_NVIC               __CAST(Cm0pNvic*,       0xE000E100u)
    #define CM0P_SCB                __CAST(Cm0pScb*,        0xE000ED00u)
    #define CM0P_CORE_DEBUG         __CAST(Cm0pCoreDbg*,    0xE000EDF0u)


// ----------------------------------------------------------------------------
// Registers fields
// ----------------------------------------------------------------------------

    // SysTick
    #define SYSTICK_CTRL_COUNTFLAG_POS         16
    #define SYSTICK_CTRL_COUNTFLAG_MSK         (1UL << SYSTICK_CTRL_COUNTFLAG_POS)
    #define SYSTICK_CTRL_CLKSOURCE_POS          2
    #define SYSTICK_CTRL_CLKSOURCE_MSK         (1UL << SYSTICK_CTRL_CLKSOURCE_POS)
    #define SYSTICK_CTRL_TICKINT_POS            1
    #define SYSTICK_CTRL_TICKINT_MSK           (1UL << SYSTICK_CTRL_TICKINT_POS)
    #define SYSTICK_CTRL_ENABLE_POS             0
    #define SYSTICK_CTRL_ENABLE_MSK            (1UL << SYSTICK_CTRL_ENABLE_POS)

    #define SYSTICK_LOAD_RELOAD_POS             0
    #define SYSTICK_LOAD_RELOAD_MSK            (0xFFFFFFUL << SYSTICK_LOAD_RELOAD_POS)

    #define SYSTICK_VAL_CURRENT_POS             0
    #define SYSTICK_VAL_CURRENT_MSK            (0xFFFFFFUL << SYSTICK_VAL_CURRENT_POS)

    #define SYSTICK_CALIB_NOREF_POS            31
    #define SYSTICK_CALIB_NOREF_MSK            (1UL << SYSTICK_CALIB_NOREF_POS)
    #define SYSTICK_CALIB_SKEW_POS             30
    #define SYSTICK_CALIB_SKEW_MSK             (1UL << SYSTICK_CALIB_SKEW_POS)
    #define SYSTICK_CALIB_TENMS_POS             0
    #define SYSTICK_CALIB_TENMS_MSK            (0xFFFFFFUL << SYSTICK_VAL_CURRENT_POS)

    // SCB
    #define SCB_CPUID_IMPLEMENTER_POS          24
    #define SCB_CPUID_IMPLEMENTER_MSK          (0xFFUL << SCB_CPUID_IMPLEMENTER_POS)
    #define SCB_CPUID_VARIANT_POS              20
    #define SCB_CPUID_VARIANT_MSK              (0xFUL << SCB_CPUID_VARIANT_POS)
    #define SCB_CPUID_ARCHITECTURE_POS         16
    #define SCB_CPUID_ARCHITECTURE_MSK         (0xFUL << SCB_CPUID_ARCHITECTURE_POS)
    #define SCB_CPUID_PARTNO_POS                4
    #define SCB_CPUID_PARTNO_MSK               (0xFFFUL << SCB_CPUID_PARTNO_POS)
    #define SCB_CPUID_REVISION_POS              0
    #define SCB_CPUID_REVISION_MSK             (0xFUL << SCB_CPUID_REVISION_POS)

    #define SCB_ICSR_NMIPENDSET_POS            31
    #define SCB_ICSR_NMIPENDSET_MSK            (1UL << SCB_ICSR_NMIPENDSET_POS)
    #define SCB_ICSR_PENDSVSET_POS             28
    #define SCB_ICSR_PENDSVSET_MSK             (1UL << SCB_ICSR_PENDSVSET_POS)
    #define SCB_ICSR_PENDSVCLR_POS             27
    #define SCB_ICSR_PENDSVCLR_MSK             (1UL << SCB_ICSR_PENDSVCLR_POS)
    #define SCB_ICSR_PENDSTSET_POS             26
    #define SCB_ICSR_PENDSTSET_MSK             (1UL << SCB_ICSR_PENDSTSET_POS)
    #define SCB_ICSR_PENDSTCLR_POS             25
    #define SCB_ICSR_PENDSTCLR_MSK             (1UL << SCB_ICSR_PENDSTCLR_POS)
    #define SCB_ICSR_ISRPENDING_POS            22
    #define SCB_ICSR_ISRPENDING_MSK            (1UL << SCB_ICSR_ISRPENDING_POS)
    #define SCB_ICSR_VECTPENDING_POS           12
    #define SCB_ICSR_VECTPENDING_MSK           (0x1FFUL << SCB_ICSR_VECTPENDING_POS)
    #define SCB_ICSR_RETTOBASE_POS             11
    #define SCB_ICSR_RETTOBASE_MSK             (1UL << SCB_ICSR_RETTOBASE_POS)
    #define SCB_ICSR_VECTACTIVE_POS             0
    #define SCB_ICSR_VECTACTIVE_MSK            (0x1FFUL << SCB_ICSR_VECTACTIVE_POS)

    #define SCB_VTOR_TBLOFF_POS                7
    #define SCB_VTOR_TBLOFF_MSK                (0x1FFFFFFUL << SCB_VTOR_TBLOFF_POS)

    #define SCB_AIRCR_VECTKEY_POS              16
    #define SCB_AIRCR_VECTKEY_MSK              (0xFFFFUL << SCB_AIRCR_VECTKEY_POS)
    #define SCB_AIRCR_VECTKEYSTAT_POS          16
    #define SCB_AIRCR_VECTKEYSTAT_MSK          (0xFFFFUL << SCB_AIRCR_VECTKEYSTAT_POS)
    #define SCB_AIRCR_ENDIANESS_POS            15
    #define SCB_AIRCR_ENDIANESS_MSK            (1UL << SCB_AIRCR_ENDIANESS_POS)
    #define SCB_AIRCR_SYSRESETREQ_POS           2
    #define SCB_AIRCR_SYSRESETREQ_MSK          (1UL << SCB_AIRCR_SYSRESETREQ_POS)
    #define SCB_AIRCR_VECTCLRACTIVE_POS         1
    #define SCB_AIRCR_VECTCLRACTIVE_MSK        (1UL << SCB_AIRCR_VECTCLRACTIVE_POS)

    #define SCB_SCR_SEVONPEND_POS               4
    #define SCB_SCR_SEVONPEND_MSK              (1UL << SCB_SCR_SEVONPEND_POS)
    #define SCB_SCR_SLEEPDEEP_POS               2
    #define SCB_SCR_SLEEPDEEP_MSK              (1UL << SCB_SCR_SLEEPDEEP_POS)
    #define SCB_SCR_SLEEPONEXIT_POS             1
    #define SCB_SCR_SLEEPONEXIT_MSK            (1UL << SCB_SCR_SLEEPONEXIT_POS)

    #define SCB_CCR_STKALIGN_POS                9
    #define SCB_CCR_STKALIGN_MSK               (1UL << SCB_CCR_STKALIGN_POS)
    #define SCB_CCR_BFHFNMIGN_POS               8
    #define SCB_CCR_BFHFNMIGN_MSK              (1UL << SCB_CCR_BFHFNMIGN_POS)
    #define SCB_CCR_DIV_0_TRP_POS               4
    #define SCB_CCR_DIV_0_TRP_MSK              (1UL << SCB_CCR_DIV_0_TRP_POS)
    #define SCB_CCR_UNALIGN_TRP_POS             3
    #define SCB_CCR_UNALIGN_TRP_MSK            (1UL << SCB_CCR_UNALIGN_TRP_POS)
    #define SCB_CCR_USERSETMPEND_POS            1
    #define SCB_CCR_USERSETMPEND_MSK           (1UL << SCB_CCR_USERSETMPEND_POS)
    #define SCB_CCR_NONBASETHRDENA_POS          0
    #define SCB_CCR_NONBASETHRDENA_MSK         (1UL << SCB_CCR_NONBASETHRDENA_POS)
