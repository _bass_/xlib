//*****************************************************************************
//
// @brief   Cortex-M0 IRQ
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default priorities
    #define IRQ_PRIORITY_DMA                    IRQ_PRIORITY_HIGHEST
    #define IRQ_PRIORITY_USB                    IRQ_PRIORITY_HIGH
    #define IRQ_PRIORITY_ADC                    IRQ_PRIORITY_MID
    #define IRQ_PRIORITY_USART                  IRQ_PRIORITY_MID
    #define IRQ_PRIORITY_CAN                    IRQ_PRIORITY_MID
