//*****************************************************************************
//
// @brief   Main CPU platform header
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cc.h"
#include "arch/core/types.h"


// CPU
#if     defined(__STM32F051C8__)
    #include "arch/core/cm0/cm0.h"
    #include "arch/cpu/stm32f0xx/cpu.h"

#elif   defined(__STM32L051C8__) || \
        defined(__STM32L051C6__)
    #include "arch/core/cm0p/cm0p.h"
    #include "arch/cpu/stm32l0xx/cpu.h"

#elif   defined(__STM32F100C6__) || \
        defined(__STM32F100RB__) || \
        defined(__STM32F103R6__) || \
        defined(__STM32F103C8__)
    #include "arch/core/cm3/cm3.h"
    #include "arch/cpu/stm32f10x/cpu.h"

#elif   defined(__STM32F205VE__) || \
        defined(__STM32F205RE__) || \
        defined(__STM32F207VG__)
    #include "arch/core/cm3/cm3.h"
    #include "arch/cpu/stm32f2xx/cpu.h"

#elif   defined(__STM32F411RC__)
    #include "arch/core/cm4/cm4.h"
    #include "arch/cpu/stm32f4xx/cpu.h"

#elif   defined(__NRF52840__)
    #include "arch/core/cm4/cm4.h"
    #include "arch/cpu/nrf52/cpu.h"

#elif   defined(__HOST_X86__)
    #include "arch/cpu/x86/cpu.h"

#else
    #error "--- Undefined CPU ---"
#endif

// Base modules
#include "arch/irq.h"
#include "arch/clock.h"
#include "arch/flash.h"
#include "arch/gpio.h"
