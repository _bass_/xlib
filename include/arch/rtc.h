//*****************************************************************************
//
// @brief   RTC module driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "misc/time.h"


// ----------------------------------------------------------------------------
// RTC
// ----------------------------------------------------------------------------

class RTC
{
    public:
        /**
          * @brief  Module initialization
          * @param  None
          */
        static void Init (void);

        /**
          * @brief  Read Unix timestamp
          * @return Unix timestamp (seconds)
          */
        static uint32 GetTime (void);

        /**
          * @brief  Read formatted datetime
          * @param  time: Pointer to variable to save current datetime value
          */
        static void GetTime (TTime* time);

        /**
          * @brief  Set time
          * @param  time: Time to set (Unix time)
          */
        static void SetTime (uint32 time);

        /**
          * @brief  Set time
          * @param  time: Time to set
          */
        static void SetTime (const TTime* pTm);
};
