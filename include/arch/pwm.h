//*****************************************************************************
//
// @brief   PWM module control interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// PWM
// ----------------------------------------------------------------------------

class PWM
{
    public:
        /**
          * @brief  Set PWM channel configuration (initialization)
          * @param  channel: PWM channel ID
          * @param  freq: PWM channel frequency
          */
        static void Configure (uint channel, uint freq);
        
        /**
          * @brief  Turn PWM channel on
          * @param  channel: PWM channel ID
          */
        static void Start (uint channel);

        /**
          * @brief  Turn PWM channel off
          * @param  channel: PWM channel ID
          */
        static void Stop (uint channel);
        
        /**
          * @brief  Set raw value of duty cycle
          * @param  channel: PWM channel ID
          * @param  value: Raw value of duty cycle
          */
        static void SetValue (uint channel, uint value);

        /**
          * @brief  Set value of duty cycle
          * @param  channel: PWM channel ID
          * @param  value: Value of duty cycle (%)
          */
        static void SetDuty (uint channel, uint duty);
};
