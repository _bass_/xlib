//*****************************************************************************
//
// @brief   DAC interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/gpio.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        uint            freq;           // Conversion frequency (0 - single conversion)
        uint            resolution;     // Conversion resolution (bits)
        TGpio           pin;            // DAC GPIO
    } TDacOptions;
