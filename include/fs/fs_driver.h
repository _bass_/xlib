//*****************************************************************************
//
// @brief   FS driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "fs/fs_file.h"


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    // FS types
    #define FS_TYPE_NONE                    0
    #define FS_TYPE_SLFS                    1


    // FS driver registration
    #define FS_REGISTER(type, hdl)          REGISTER_ITEM(xLib_fsList, const TFsRegDesc, {type, hdl})


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Partition FS descriptor
    struct _TFsDesc;
    typedef _TFsDesc TFsDesc;

    // FS object creation handler
    class CFs;
    typedef CFs* (TFsHandler) (const TFsDesc* pDesc);

    // Descriptor of registered FS driver
    typedef struct
    {
        uint        type;       // FS type (see. FS_TYPE_xxx)
        TFsHandler* hdl;
    } TFsRegDesc;


// ----------------------------------------------------------------------------
// CFs
// ----------------------------------------------------------------------------

class CFs
{
    public:
        /**
          * @brief  Constructor
          * @param  pDesc: FS descriptor of partition
          */
        CFs (const TFsDesc* pDesc);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CFs ();

        /**
          * @brief  Partition mounting
          * @return 0 or negative error code
          */
        virtual int Mount (void) = 0;

        /**
          * @brief  Partition unmounting
          * @return 0 or negative error code
          */
        virtual int Umount (void) = 0;

        /**
          * @brief  Partition formatting
          * @return 0 or negative error code
          */
        virtual int Format (void) = 0;

        /**
          * @brief  Open directory
          * @param  name: Directory name (path)
          * @return Directory descriptor index or negative error code
          */
        virtual int DirOpen (const char* name) = 0;

        /**
          * @brief  Close directory
          * @param  fd: Descriptor index
          * @return 0 or negative error code
          */
        virtual int DirClose (uint fd) = 0;

        /**
          * @brief  Read file info from the directory
          * @param  fd: Descriptor index
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        virtual int DirRead (uint fd, TFileInfo* pInfo) = 0;

        /**
          * @brief  Open file
          * @param  name: File name (path)
          * @param  mode: Opening mode (see FILE_MODE_xx in fs_file.h)
          * @return File descriptor index or negative error code
          */
        virtual int Fopen (const char* name, uint mode) = 0;

        /**
          * @brief  Close file
          * @param  fd: Opened file descriptor
          * @return 0 or negative error code
          */
        virtual int Fclose (uint fd) = 0;

        /**
          * @brief  Read file data
          * @param  fd: Opened file descriptor
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @param  offset: File data offset (from the beginning)
          * @return Number of bytes written in buff or negative error code
          */
        virtual int Fread (uint fd, void* buff, uint size, uint offset = 0) = 0;

        /**
          * @brief  Write data into file
          * @param  fd: Opened file descriptor
          * @param  data: Data to write
          * @param  size: Data size
          * @param  offset: Write offset (from the file beginning)
          * @return Number of written bytes or negative error code
          */
        virtual int Fwrite (uint fd, const void* data, uint size, uint offset = 0) = 0;

        /**
          * @brief  Read information about opened file
          * @param  fd: Opened file descriptor
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        virtual int Fstat (uint fd, TFileInfo* pInfo) = 0;

        /**
          * @brief  Create FS driver object
          * @param  pDesc: Pointer to FS descriptor
          * @return Pointer to created FS object or NULL
          */
        static CFs* build (const TFsDesc* pDesc);

    protected:
        const TFsDesc*  m_pDesc;    // Pointer to FS descriptor
};
