//*****************************************************************************
//
// @brief   VFS subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/block.h"
#include "fs/fs_driver.h"
#include "fs/fs_dir.h"
#include "fs/fs_file.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Partition FS descriptor
    typedef struct _TFsDesc
    {
        uint    devNum;             // Block device number
        uint    partition;          // Partition ID
        uint    fsType;             // FS code (see. FS_TYPE_xx)
        uint    offset;             // Block number of partition begining
        uint    size;               // Partition size in blocks
    } TFsDesc;


// ----------------------------------------------------------------------------
// VFS
// ----------------------------------------------------------------------------

class VFS
{
    public:
        /**
          * @brief  Initialization
          * @param  pFstab: Pointer to partitions table
          * @param  size: Number of entries in the table
          */
        static void Init (const TFsDesc* pFstab, uint size);

        /**
          * @brief  Deinitialization
          * @param  None
          */
        static void Uninit (void);

        /**
          * @brief  FS creation in partition (formatting)
          * @param  partId: Partition ID (partition index in partitions table)
          * @return 0 or negative error code
          */
        static int  Mkfs (uint partId);

        /**
          * @brief  Partition mounting
          * @param  partId: Partition ID (partition index in partitions table)
          * @return 0 or negative error code
          */
        static int  Mount (uint partId);

        /**
          * @brief  Partition unmounting
          * @param  partId: Partition ID (partition index in partitions table)
          * @return 0 or negative error code
          */
        static int  Umount (uint partId);

        /**
          * @brief  Get pointer to partition FS driver object
          * @param  partId: Partition ID (partition index in partitions table)
          * @return Pointer to driver object or NULL
          */
        static CFs* GetFs (uint partId);
};
