//*****************************************************************************
//
// @brief   Class for working with files
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "misc/time.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // File opening mode
    #define FILE_MODE_READ              (1 << 0)
    #define FILE_MODE_WRITE             (1 << 1)
    #define FILE_MODE_CREATE            (1 << 3)

    // Base position for file offset calculation
    #define FILE_SEEK_SET               0           // From the begining of file
    #define FILE_SEEK_CUR               1           // From current position
    #define FILE_SEEK_END               2           // From the end of file


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    class CFs;

    // File metadata
    typedef struct
    {
        const char* name;
        TTime       time;
        uint        size;
    } TFileInfo;


// ----------------------------------------------------------------------------
// CFile
// ----------------------------------------------------------------------------

class CFile
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CFile ();

        /**
          * @brief  Open file
          * @param  partId: Partition ID (partition index in partitions table)
          * @param  name: File name (path)
          * @param  mode: Opening mode (see FILE_MODE_xx)
          * @return File descriptor index or negative error code
          */
        int Open (uint partId, const char* name, uint mode);

        /**
          * @brief  Close file
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read file data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Number of bytes written in buff or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data into file
          * @param  data: Data to write
          * @param  size: Data size
          * @return Number of written bytes or negative error code
          */
        int Write (const void* data, uint size);

        /**
          * @brief  Move opened file cursor
          * @param  offset: Number of bytes to move for
          * @param  origin: Base point for offset calculation
          * @return 0 or negative error code
          */
        int Seek (uint offset, uint origin);

        /**
          * @brief  Read information about opened file
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        int Stat (TFileInfo* pInfo);

    private:
        CFs*    m_pFs;
        int     m_fd;
        uint    m_offset;
};
