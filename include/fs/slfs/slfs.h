//*****************************************************************************
//
// @brief   SLFS driver (Simple Log File System)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "fs/vfs.h"
#include "kernel/kmutex.h"
#include "misc/time.h"
#include <list>


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #if !defined(__ICCARM__) || _HAS_NAMESPACE
    using std::list;
    #endif


// ----------------------------------------------------------------------------
// CSlfs
// ----------------------------------------------------------------------------

class CSlfs : public CFs
{
    public:
        /**
          * @brief  Constructor
          * @param  pDesc: FS descriptor of partition
          */
        CSlfs (const TFsDesc* pDesc);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CSlfs ();

        /**
          * @brief  Create FS driver object
          * @param  pDesc: Pointer to FS descriptor
          * @return Pointer to created FS object or NULL
          */
        static CFs* buildFsObject (const TFsDesc* pDesc);

        /**
          * @brief  Partition mounting
          * @return 0 or negative error code
          */
        int Mount (void);

        /**
          * @brief  Partition unmounting
          * @return 0 or negative error code
          */
        int Umount (void);

        /**
          * @brief  Partition formatting
          * @return 0 or negative error code
          */
        int Format (void);

        /**
          * @brief  Open directory
          * @param  name: Directory name (path)
          * @return Directory descriptor index or negative error code
          */
        int DirOpen (const char* name);

        /**
          * @brief  Close directory
          * @param  fd: Descriptor index
          * @return 0 or negative error code
          */
        int DirClose (uint fd);

        /**
          * @brief  Read file info from the directory
          * @param  fd: Descriptor index
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        int DirRead (uint fd, TFileInfo* pInfo);

        /**
          * @brief  Open file
          * @param  name: File name (path)
          * @param  mode: Opening mode (see FILE_MODE_xx in fs_file.h)
          * @return File descriptor index or negative error code
          */
        int Fopen (const char* name, uint mode);

        /**
          * @brief  Close file
          * @param  fd: Opened file descriptor
          * @return 0 or negative error code
          */
        int Fclose (uint fd);

        /**
          * @brief  Read file data
          * @param  fd: Opened file descriptor
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @param  offset: File data offset (from the beginning)
          * @return Number of bytes written in buff or negative error code
          */
        int Fread (uint fd, void* buff, uint size, uint offset = 0);

        /**
          * @brief  Write data into file
          * @param  fd: Opened file descriptor
          * @param  data: Data to write
          * @param  size: Data size
          * @param  offset: Write offset (from the file beginning)
          * @return Number of written bytes or negative error code
          */
        int Fwrite (uint fd, const void* data, uint size, uint offset = 0);

        /**
          * @brief  Read information about opened file
          * @param  fd: Opened file descriptor
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        int Fstat (uint fd, TFileInfo* pInfo);

    private:
        #pragma pack(1)
        // Partition header structure
        typedef struct
        {
            uint32  magic;                  // Partition magic (SLFS_MAGIC_PARTITION)
            uint16  size;                   // Partition size (in blocks)
            uint16  count;                  // Counter of header writings
            char    isLooped;               // Flag of partition looping
            char    reserved[3];
        } THdrPart;

        // File header structure
        typedef struct
        {
            uint16  magic;                  // File header magic (SLFS_MAGIC_FILE)
            uint16  size;                   // File size (header + data, bytes)
            TTime   time;                   // File creation timestamp
            uint32  id;                     // File ID (name)
            uint16  dataOffset;             // File data offset (relative to the end of the header)
            char    reserved[2];
        } THdrFile;
        #pragma pack()

        // File descriptor
        typedef struct
        {
            THdrFile    hdr;                // File header
            uint32      addr;               // File (heder) address. Address is calculated from partition begining.
            uint        mode;               // Opening mode (For unused/free descriptors mode = 0)
            int         fd;                 // File descriptor ID
        } TFileDesc;

        THdrPart    m_partHdr;              // Local copy of actual partition header
        uint        m_currBlock;            // Index of current (edited) block with files
        uint        m_offset;               // Last file offset in current block
        KMutex      m_mutex;                // Mutex for access locking
        void*       m_cache;                // Pointer to block cache
        uint        m_cachedBlock;          // Number of block in the cache
        list<TFileDesc> m_fileList;         // List of opened file descriptors


        /**
          * @brief  Partition header validation
          * @param  pHdr: Partition header
          * @return true - header is valid
          */
        bool isPartHeaderValid (const THdrPart* pHdr);
        
        /**
          * @brief  Partition size validation
          * @return true - Partition size is valid
          */
        bool isPartSizeValid (void);

        /**
          * @brief  Reading current partition header
          * @return 0 or negative error code
          */
        int partHeaderRead (void);

        /**
          * @brief  Save (write) current partition header on disk
          * @param  currBlock: Current (edited) data block in partition
          * @return 0 or negative error code
          */
        int partHeaderSave (uint currBlock);

        /**
          * @brief  Read block data (with caching)
          * @param  index: Block index in partition
          * @return Pointer to read data or NULL
          */
        void* blockRead (uint index);

        /**
          * @brief  Freeing cached block data
          * @param  index: Block index in partition
          */
        void blockFree (uint index);
        
        /**
          * @brief  Write block data
          * @param  data: Block data
          * @param  index: Block index in partition
          * @param  erase: Flag of required erasing before writing data
          * @return 0 or negative error code
          */
        int blockWrite (void* data, uint index, bool erase = false);

        /**
          * @brief  File descriptor creation
          * @param  pDescVar: Pointer to save created descriptor
          * @return Descriptor index or negative error code
          */
        int fdCreate (TFileDesc** pDescVar);

        /**
          * @brief  File descriptor removing
          * @param  fd: File descriptor index
          * @return 0 or negative error code
          */
        int fdRemove (int fd);

        /**
          * @brief  Get file descriptor data
          * @param  fd: File descriptor index
          * @return Pointer to descriptor data or NULL
          */
        TFileDesc* fdGet (int fd);

        /**
          * @brief  Find file with specified ID (name)
          * @param  id: File ID
          * @param  pHdr: Pointer to save file header
          * @return File offset in partition or SLFS_ADDR_NOT_VALID on error
          */
        uint32 findFile (uint32 id, THdrFile* pHdr = NULL);

        /**
          * @brief  Find last file in block
          * @param  blockData: Block data for searching
          * @param  size: Block data size
          * @return Pointer to last file in the block or NULL
          */
        THdrFile* findLastFile (void* blockData, uint size);
        
        /**
          * @brief  File header validation
          * @param  pHdr: File header
          * @return true - the header is valid
          */
        bool isFileHeaderValid (const THdrFile* pHdr);
        
        /**
          * @brief  File descriptor data validation
          * @param  pDesc: File descriptor
          * @return true - the descriptor is valid
          */
        bool isFileDescValid (const TFileDesc* pDesc);
        
        /**
          * @brief  Check partition mounting state
          * @return true - partition is mounted
          */
        bool isMounted (void);

        /**
          * @brief  Read data about last file (block number and offset) from disc
          * @return 0 or negative error code
          */
        int syncLastFileData (void);

        /**
          * @brief  Get block size
          * @return Block size (bytes)
          */
        uint getBlockSize (void);

        /**
          * @brief  Get previous block index (with looping)
          * @return Block index or SLFS_BLOCK_NOT_VALID_INDEX on error
          */
        uint prevDataBlock (uint index);

        /**
          * @brief  Get next block index (with looping)
          * @return Block index
          */
        uint nextDataBlock (uint index);
};
