//*****************************************************************************
//
// @brief   Class for working with directories
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "fs/fs_file.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    class CFs;


// ----------------------------------------------------------------------------
// CDir
// ----------------------------------------------------------------------------

class CDir
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CDir ();

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CDir ();
        
        /**
          * @brief  Open directory
          * @param  partId: Partition ID (partition index in partitions table)
          * @param  name: Directory name (path)
          * @return 0 or negative error code
          */
        int Open (uint partId, const char* name);

        /**
          * @brief  Close directory
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read file info from the directory
          * @param  pInfo: Pointer to variable to save file info
          * @return 0 or negative error code
          */
        int Read (TFileInfo* pInfo);
        
    private:
        CFs*    m_pFs;
        int     m_fd;
};
