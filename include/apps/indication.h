//*****************************************************************************
//
// @brief   Simple discrete indication
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kthread.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Indication masks
    #define IND_MASK_OFF                0x00000000
    #define IND_MASK_ON                 0xffffffff


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Additional configuration flags
    typedef struct
    {
        uchar       invert      : 1;    // Flag of state inversion
        uchar       reserved    : 7;
        
    } TIndFlags;

    // Indication element descriptor (configuration)
    typedef struct
    {
        TGpio       pin;        // GPIO pin
        uint        pinCfg;     // GPIO pin configuration
        TIndFlags   flags;      // Additional flags
    } TIndDesc;
    
    // Indication element state
    typedef struct
    {
        uint32      mask;       // Current indication mask
        uint32      savedMask;  // Saved indication mask
        char        startPos;   // Indication step with changed state
        char        count;      // Repeat counter of indication cycles
    } TIndState;


// ----------------------------------------------------------------------------
// CIndication
// ----------------------------------------------------------------------------

class CIndication : public KThreadWait
{
    public:
        CIndication (const TIndDesc* pDesc, uint count, const char* name, uint stackSize, uint priority = THREAD_PRIORITY_DEFAULT);
        ~CIndication ();

    protected:
        void setMask (uint channel, uint32 mask, uint count = 0);

        virtual void onBlinkEnd (uint channel) { UNUSED_VAR(channel); }
        
    private:
        const TIndDesc* m_pDesc;                // Pointer to descriptors of indacation elements
        const uint      m_indCount;             // Number of indicators
        TIndState*      m_state;                // State variable of indacation element
        uint            m_step;                 // Current processing position in indication mask
        uint32          m_jiff;                 // Timestamp of the last updatinf of indication state
        
        void    run (void);
        
        uint    getCurrentStep (void);
};
