//*****************************************************************************
//
// @brief   Basic beeper control
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kthread.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Flag for activation looped playback
    #define BEEP_PLAY_ALWAYS                    (-1)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        uint    pwmChannel;
        void    (*hwCtrl) (bool state);     // Sound on/off control handler
    } TBeepCfg;

    // Note descriptor
    typedef struct
    {
        uint    freq;       // Frequency, Hz
        uint    time;       // Duration, ms
    } TMelody;


// ----------------------------------------------------------------------------
// CBeep
// ----------------------------------------------------------------------------

class CBeep : public KThreadWait
{
    public:
        CBeep (const TBeepCfg* pCfg, uint stackSize, uint priority = THREAD_PRIORITY_DEFAULT);
        virtual ~CBeep ();
    
        void    Play (const TMelody* pSound, uint replay = 0);
        const TMelody* GetMelody (void) const;
        
    protected:
        void    init (void);
        void    run (void);
        
        void    processMelody (void);
        
    private:
        const TBeepCfg* m_pCfg;
        const TMelody*  m_pMelody;
        uint            m_melodyIndex;
        uint            m_loop;
        
        void    hwCtrl (bool state);
};
