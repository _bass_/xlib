//*****************************************************************************
//
// @brief   Interface for hashing classes
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// IHash
// ----------------------------------------------------------------------------

class IHash
{
    public:
        IHash () {}
        virtual ~IHash () {}

        /**
          * @brief  Reset state
          * @param  None
          * @retval None
          */
        virtual void    Reset (void) = 0;

        /**
          * @brief  Hash calculation or start of partial calculation
          * @param  data: Data for hashing
          * @param  size: Data size
          * @param  totalSize: Total data size
          * @return Size of processed data or negative error code
          */
        virtual int     Calculate (const void* data, uint size, uint totalSize) = 0;


        /**
          * @brief  Apped data for hash calculation (continue calculation)
          * @param  data: Data for append
          * @param  size: Data size
          * @return Size of processed data or negative error code
          */
        virtual int     Append (const void* data, uint size) = 0;


        /**
          * @brief  Read hashing result
          * @param  buff: Buffer for result saving
          * @param  size: Buffer size
          * @return Data size written into the buffer or negative error code
          */
        virtual int     GetResult (void* buff, uint size) = 0;

        /**
          * @brief  Get hash size
          * @return Hash size (bytes) or negative error code
          */
        virtual int     GetSize (void) = 0;
};
