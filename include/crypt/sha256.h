//*****************************************************************************
//
// @brief   Hashing algorithm SHA-256 implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "crypt/ihash.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Hash size (bytes)
    #define SHA256_DIGEST_LENGTH                    32

    // Hashing block size (bytes)
    #define SHA256_BLOCK_SIZE                       (512 / 8)


// ----------------------------------------------------------------------------
// CSha256
// ----------------------------------------------------------------------------

class CSha256: public IHash
{
    public:
        CSha256 ();
        ~CSha256 ();

        void    Reset (void);
        int     Calculate (const void* data, uint size, uint totalSize);
        int     Append (const void* data, uint size);
        int     GetResult (void* buff, uint size);
        int     GetSize (void);

    private:
        uint32* m_pState;           // State data used in calculation steps
        uint32* m_pW;               // Auxiliary buffer for hash calculation
        uint    m_totalSize;        // Total processing data size
        uint    m_processedSize;    // Amount of already processed data

        void    update (const char* data);
        void    final (const char* data, uint totalSize);
};
