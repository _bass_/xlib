//*****************************************************************************
//
// @brief   Hashing algorithm SHA-512 implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "crypt/ihash.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Hash size (bytes)
    #define SHA512_DIGEST_LENGTH                    64

    // Hashing block size (bytes)
    #define SHA512_BLOCK_SIZE                       (1024 / 8)


// ----------------------------------------------------------------------------
// CSha512
// ----------------------------------------------------------------------------

class CSha512: public IHash
{
    public:
        CSha512 ();
        ~CSha512 ();

        void    Reset (void);
        int     Calculate (const void* data, uint size, uint totalSize);
        int     Append (const void* data, uint size);
        int     GetResult (void* buff, uint size);
        int     GetSize (void);

    private:
        uint64* m_pState;           // State data used in calculation steps
        uint64* m_pW;               // Auxiliary buffer for hash calculation
        uint    m_totalSize;        // Total processing data size
        uint    m_processedSize;    // Amount of already processed data

        void    update (const char* data);
        void    final (const char* data, uint totalSize);
};
