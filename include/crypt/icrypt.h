//*****************************************************************************
//
// @brief   Interface for encryption/decryption clasess
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// ICrypt
// ----------------------------------------------------------------------------

class ICrypt
{
    public:
        ICrypt () {}
        virtual ~ICrypt () {}

        /**
          * @brief  Set encryption parameters (keys, auxiliary data...)
          * @param  params: Encryption parameters
          * @return 0 or negative error code
          */
        virtual int     SetParams (const void* params) = 0;


        /**
          * @brief  Data encryption (start of partial encryption)
          * @param  data: Data (plaintext)
          * @param  size: Data size
          * @param  totalSize: Total data size
          * @param  buff: Buffer for encrypted data (ciphertext)
          * @param  pBuffSize: Buffer size (value will be replaced with written data size)
          * @return Size of processed data or negative error code
          */
        virtual int     Encode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize) = 0;


        /**
          * @brief  Data decryption (start of partial decryption)
          * @param  data: Encrypted data (ciphertext)
          * @param  size: Encrypted data size
          * @param  totalSize: Total encrypted data size
          * @param  buff: Buffer for decrypted data (plaintext)
          * @param  pBuffSize: Buffer size (value will be replaced with written data size)
          * @return Size of processed data or negative error code
          */
        virtual int     Decode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize) = 0;


        /**
          * @brief  Append data part for encryption/decryption (partial encryption/decryption)
          * @param  data: Source data
          * @param  size: Data size
          * @param  buff: Buffer for encrypted/decrypted data
          * @param  pBuffSize: Buffer size (value will be replaced with written data size)
          * @return Size of processed data or negative error code
          */
        virtual int     Append (const void* data, uint size, void* buff, uint* pBuffSize) = 0;
};
