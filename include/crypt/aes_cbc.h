//*****************************************************************************
//
// @brief   AES encryption (128/256, CBC mode)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "crypt/icrypt.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Encryption key size (bytes))
    #define AES_128_KEY_SIZE                16
    #define AES_256_KEY_SIZE                32

    // Encryption block size (bytes)
    #define AES_BLOCK_SIZE                  16


// ----------------------------------------------------------------------------
// CAesCbc
// ----------------------------------------------------------------------------

class CAesCbc : public ICrypt
{
    public:
        // Encryption parameters
        typedef struct
        {
            const void* pKey;
            const void* pIv;
        } TParams;

        CAesCbc (uint keySize);
        ~CAesCbc ();

        int     SetParams (const void* params);
        int     Encode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize);
        int     Decode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize);
        int     Append (const void* data, uint size, void* buff, uint* pBuffSize);
        
    private:
        // Internal service data of conversion process
        typedef struct
        {
            int     mode;
            uint    totalSize;
            uint    processedSize;
            char    prevBlock[2][AES_BLOCK_SIZE];
            char    keyBuff[AES_256_KEY_SIZE];
        } TMeta;

        const uint  m_keySize;              // Encryption key size (bytes)
        char        m_iv[AES_BLOCK_SIZE];   // Initialization vector
        void*       m_key;                  // Encryption key
        TMeta*      m_pMeta;                // Internal conversion data

        void    encodeBlock (char* keyBuff, char* pBlock, const char* pPrevBlock);
        void    decodeBlock (char* keyBuff, char* pBlock, const char* pPrevBlock);
};
