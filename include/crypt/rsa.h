//*****************************************************************************
//
// @brief   RSA encyption  (lite)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// CRsa
// ----------------------------------------------------------------------------

class CRsa
{
    public:
        CRsa (uint keyLen);
        
        int     Encode (void* plaintext, uint size, void* n);
        int     Decode (void* ciphertext, void* n, void* d);
        
    private:
        const uint  m_wordCount;    // Encryprion key size in words (uint16)

        // Data preparatino for encoding process (according to PKCS #1 v1.5)
        int     prepareData (char* data, uint size);
        
        int     modExp (uint16* m, uint16* n, uint16* pow, uint powSize);
        void    mul (uint16* res, uint16* a, uint16* b);
        void    mod (uint16* a, uint16* n);
        uint    add (uint16* res, uint16* a);
        void    sub (uint16* res, uint16* a);
        uint    mulsub (uint16* res, uint16* a, uint16 b);
        int     cmp (const uint16* a, const uint16* b);
};
