//*****************************************************************************
//
// @brief   Maths functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    /**
      * @brief  CRC8 calculation
      * @param  data: Pointer to data
      * @param  size: Data size
      * @param  crc: Initial CRC value
      * @return Calculated CRC value
      */
    uchar crc8 (const void* data, uint size, uchar crc = 0);

    /**
      * @brief  CRC16 CCITT calculation (x^16 + x^12 + x^5 + 1, X.25, HDLC, XMODEM, Bluetooth, SD...) 
      * @param  data: Pointer to data
      * @param  size: Data size
      * @param  crc: Initial CRC value
      * @return Calculated CRC value
      */
    uint16  crc16_ccitt (const void* data, uint size, uint crc = 0);

    /**
      * @brief  CRC16 CCITT calculation (x^16 + x^12 + x^5 + 1, X.25, HDLC, XMODEM, Bluetooth, SD...) 
      * @param  data: Pointer to data
      * @param  size: Data size
      * @param  crc: Initial CRC value
      * @return Calculated CRC value
      */
    uint16  crc16_ccitt_tab (const void* data, uint size, uint crc = 0);

    /**
     * @brief  CRC32 calculation (x^16 + x^12 + x^5 + 1, X.25, HDLC, XMODEM, Bluetooth, SD...)
     * @param  data: Pointer to data
     * @param  size: Data size
     * @param  crc: Initial CRC value
     * @return Calculated CRC value
     */
    uint32 crc32 (const void* data, uint size, uint crc = 0);
