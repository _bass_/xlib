//*****************************************************************************
//
// @brief   FIFO buffer
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// TFifo
// ----------------------------------------------------------------------------

class TFifo
{
    public:
        /**
          * @brief  Constructor
          * @param  size: Buffer size
          * @param  pBuff: [Optional] Pointer to allocated memory for the buffer
          */
        TFifo (uint size, void* pBuff = NULL);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~TFifo ();

        /**
          * @brief  Saved data size calculation
          * @param  None
          * @return Data size in the buffer
          */
        uint GetCount (void) { return (m_rd > m_wr) ? m_size - m_rd + m_wr : m_wr - m_rd ; }

        /**
          * @brief  Free size calculation
          * @param  None
          * @return Free size in the buffer
          */
        uint GetFree (void) { return (m_rd > m_wr) ? m_rd - m_wr - 1 : m_size - (m_wr - m_rd) - 1; }

        /**
          * @brief  Buffer reset (erase all data)
          * @param  None
          */
        void Flush (void) { m_rd = m_wr = 0; }

        /**
          * @brief  Buffer size calculation available for linear write
          * @param  pLen: Pointer to cariable to save linear buffer size
          * @return Pointer to the begining of the linear buffer
          */
        void* GetLineBuffWr (uint* pLen);

        /**
          * @brief  Buffer size calculation available for linear read
          * @param  pLen: Pointer to cariable to save linear buffer size
          * @return Pointer to the begining of the linear buffer
          */
        void* GetLineBuffRd (uint* pLen);

        /**
          * @brief  Reading data from the buffer
          * @param  buff: Pointer to buffer to save read data
          * @param  size: Buffer size
          * @return Amount of read data
          */
        uint Read (void* buff, uint size);

        /**
          * @brief  Writing data to the buffer
          * @param  data: Pointer to data for save
          * @param  size: Data size
          * @return Amount of saved in the buffer data
          */
        uint Write (const void* data, uint size);

        /**
          * @brief  Reading data without removing from the buffer
          * @param  buff: Pointer to buffer to save read data
          * @param  size: Buffer size
          * @return Amount of read data
          */
        uint Peek (void* buff, uint size);

        /**
          * @brief  Reading pointer modification. Emuation of reading data.
          * @param  len: Number of read data
          */
        void SetRead (uint len);

        /**
          * @brief  Writing pointer modification. Emuation of writing data.
          * @param  len: Number of written data
          */
        void SetWrite (uint len);

        /**
          * @brief  Save byte in the buffer
          * @param  c: Data byte
          * @return 0 on success or negative error code
          */
        int Push (const uchar c);

        /**
          * @brief  Reading byte from the buffer
          * @param  None
          * @return Byte value or negative error code
          */
        int Pop (void);

    private:
        char*   m_pBuff;        // Start of buffer memory
        uint    m_size;         // Buffer size
        uint    m_rd;           // Reading index
        uint    m_wr;           // Writing index
        bool    m_isBuffAlloc;  // Dynamic memory usage flag
};
