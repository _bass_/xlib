//*****************************************************************************
//
// @brief   Data average (moving average method)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// CAvgMov
// ----------------------------------------------------------------------------

template <typename T>
class CAvgMov
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CAvgMov () :
            m_win(0),
            m_summ(0),
            m_value(0)
        {
        }

        /**
          * @brief  Set averaging window
          * @param  window: Window size
          */
        void SetWindow (uint window)
        {
            m_win = window;
            m_summ = m_value * m_win;
        }
        
        /**
          * @brief  Set initial average value
          * @param  value: Average value
          */
        void SetValue (T value)
        {
            m_value = val;
            m_summ = val * m_win;
        }

        /**
          * @brief  Adding new raw value
          * @param  value: Raw value
          * @return Current (updated) average value
          */
        T Add (T value)
        {
            if (m_win)
            {
                m_summ -= m_summ / m_win;
                m_summ += value;
                m_value = m_summ / m_win;
            }
            else
            {
                m_value = value;
            }

            return m_value;
        }

        /**
          * @brief  Reading current average value
          * @return Current average value
          */
        T Value (void) const
        {
            return m_value;
        }

        /**
          * @brief  Reset current average value
          * @param  None
          */
        void Reset (void)
        {
            m_summ = 0;
            m_value = 0;
        }

    private:
        uint    m_win;      // Averaging window
        T       m_summ;     // Summ of values
        T       m_value;    // Current average value
};
