//*****************************************************************************
//
// @brief   Time functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kernel.h"
#include "kernel/kthread.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Structured time format
    typedef union
    {
        uint32      val;
        struct 
        {
            uint32  sec   : 6;          // 0..59
            uint32  min   : 6;          // 0..59
            uint32  hour  : 5;          // 0..23
            uint32  day   : 5;          // 1..31
            uint32  month : 4;          // 1..12
            uint32  year  : 6;          // 0..63
        };
    } TTime;

    // Time fields type
    typedef enum
    {
        TIME_TYPE_SEC,
        TIME_TYPE_MIN,
        TIME_TYPE_HOUR,
        TIME_TYPE_DAY,
        TIME_TYPE_MONTH,
        TIME_TYPE_YEAR
    } TTimeType;


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    /**
      * @brief  Get current time in Unixtime format
      * @param  None
      * @return Timestamp
      */
    uint32 time (void);

    /**
      * @brief  Get current time in structured format
      * @param  pTm: Variable to save time data
      */
    void time (TTime* pTm);

    /**
      * @brief  Set system time
      * @param  pTm: Pointer to time data
      */
    void SetTime (TTime* pTm);

    /**
      * @brief  Set system time
      * @param  time: Time value
      */
    void SetTime (uint32 time);
    
    /**
      * @brief  Day of week calculation
      * @param  day: Day of month [1 - 31]
      * @param  month: Month [1 - 12]
      * @param  year: Year (20xx)
      * @return Day of week (1 - Mon, ... 7 - Sun)
      */
    uint WhatDayTime (uint day, uint month, uint year);

    /**
      * @brief  Structured time to Unixtime conversion
      * @param  time: Structured time data
      * @return Timestamp in Unixtime format
      */
    uint32 TimeToUnixTime (const TTime& time);

    /**
      * @brief  Unixtime to structured time conversion
      * @param  time: Unixtime value
      * @param  pTm: Pointer to variable to save structured time data
      */
    void UnixTimeToTime (uint32 time, TTime* pTm);

    /**
      * @brief  Structured time value addition
      * @param  pTm: Structured time data
      * @param  field: Field type of structured time which should be changed
      * @param  value: Field value to add
      */
    void TimeAdd (TTime* pTm, TTimeType field, uint value);
    
    /**
      * @brief  Timeout checking
      * @param  startTime: Start system timestamp (ms)
      * @param  delay: Value of delay (ms)
      * @return true - timeout has passed, otherwise - false
      */
    inline bool IsTimeout (uint32 startTime, uint delay)
    {
        if (delay > THREAD_MAX_SLEEP_TIME) return true;
        
        uint32 jiff = Kernel::GetJiffies();
        int diff = jiff - startTime;
        
        return (diff > (int)delay);
    }
