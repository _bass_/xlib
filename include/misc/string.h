//*****************************************************************************
//
// @brief   String functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    /**
      * @brief  Hex-string to binary conversion
      * @param  pHexStr: Hex-string. Will be replaced with the result.
      * @return Number of bytes in result or negative error code
      */
    int HexToRaw (char* pHexStr);

    /**
      * @brief  Unicode to Windows-1251 string conversion
      * @param  data: Unicode string data
      * @param  size: Data size
      * @param  buff: Buffer for result string
      * @return Result string length or negative error code
      */
    int UnicodeToWin1251 (const char* data, uint size, char* buff);

    /**
      * @brief  Windows-1251 to Unicode string conversion
      * @param  data: Windows-1251 string data
      * @param  size: Data size
      * @param  buff: Buffer for result string
      * @param  buffSize: Buffer size
      * @return Result string size or negative error code
      */
    int Win1251ToUnicode (const char* data, uint size, char* buff, uint buffSize);

    /**
      * @brief  Big-endian to little-endian conversion for 16-bit words
      * @param  data: Pointer to original data, will be replaced by the result
      * @param  size: Data size
      * @return 0 on success or negative error code
      */
    int BeToLeWord (void* data, uint size);

    /**
      * @brief  Little-endian to big-endian conversion for 16-bit words
      * @param  data: Pointer to original data, will be replaced by the result
      * @param  size: Data size
      * @return 0 on success or negative error code
      */
    int LeToBeWord (void* data, uint size);

    /**
      * @brief  String with phone number format (international) verification
      * @param  phone: String with phone number
      * @return true - phone is valid
      */
    bool IsValidPhone (const char* phone);
