//*****************************************************************************
//
// @brief   Macros
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "kernel/kernel.h"
#include "kernel/kdebug.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define ENABLE                      true
    #define DISABLE                     false


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #if defined(CFG_SYS_TINY_ASSERT)
        #define assert(x)                   do { \
                                                if ( !(x) ) break; \
                                                IRQ::Disable(); \
                                                while(1); \
                                            } while(0);
    #else
        #define print_assert(file, line)    kprint("assert (%s:%d)", file, line)
        #define assert(x)                   do { \
                                                if ( !(x) ) break; \
                                                Kernel::EnterCritical(); \
                                                print_assert(__FILE__, __LINE__); \
                                                while(1); \
                                            } while(0)
    #endif


    #if defined(STATIC_ASSERT)
        #undef STATIC_ASSERT
    #endif
    #if defined(__cplusplus) && __cplusplus >= 201103L
        #define STATIC_ASSERT(exp)          static_assert(exp, "static_assert")
    #else
        #define STATIC_ASSERT(exp)          _ASSERT_DO_CHECK(exp, _ASSERT_VARNAME(_var_check_, __LINE__))
        #define _ASSERT_VARNAME(v, l)       _STR_DO_CONCAT(v, l)
        #define _ASSERT_DO_CHECK(exp, var)  static const char var [1 / (exp)] = {}; UNUSED_VAR(var)
    #endif


    // Number of elements in array calculation
    #define ARR_COUNT(arr)              ( sizeof(arr) / sizeof(arr[0]) )

    // Field offset in a structure calculation
    #define OFFSET(T, member)           ( (size_t) ((&((T*)0)->member)) )

    // Suppress warning about unused variables
    #define UNUSED_VAR(a)               (void)(a)

    // Reading the lowest byte
    #define LOW(a)                      ( (a) & 0xFF )
    // Reading the highest byte of 2-bytes number
    #define HIGH(a)                     ( ((a) >> 8) & 0xFF )

    // Create an unicode string
    #define _T(s)                       L##s

    // Strings concatenation
    #define _STR_DO_CONCAT(a, b)        a##b
    #define STR_CONCAT(a, b)            _STR_DO_CONCAT(a, b)

    #define MAX(x, y)                   ( ((x) > (y)) ? (x) : (y) )
    #define MIN(x, y)                   ( ((x) < (y)) ? (x) : (y) )
