//*****************************************************************************
//
// @brief   Platform-independent delayed events processing (see Kernel::PostEvent())
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kthread.h"
#include "kernel/kqueue.h"


// ----------------------------------------------------------------------------
// KNotifier
// ----------------------------------------------------------------------------

class KNotifier : public KThread
{
    public:
        /**
          * @brief  Add event to processing queue
          * @param  code: Event code
          * @param  params: Event parameters
          * @param  size: Parameters data size
          */
        static void AddEvent (uint code, void* params, uint size);
        
    private:
        /**
          * @brief  Constructor
          * @param  None
          */
        KNotifier ();

        /**
          * @brief  Thread mein function
          * @param  None
          */
        void run (void);
        

        static KNotifier*   m_pThread;              // Current thread object pointer
        static uint         m_lockThreadDestroy;    // Internal locker to prevent thread destroying after each event processing
        KQueue              m_evQueue;              // Events queue
};
