//*****************************************************************************
//
// @brief   Threads
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kernel.h"
#include "kernel/ktimer.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default thread stack size (bytes)
    #define THREAD_DEFAULT_STACK_SIZE       384

    // Threads priorities
    #if defined(CFG_THREAD_MAX_PROIRITIES)
        #define THREAD_MAX_PROIRITIES       CFG_THREAD_MAX_PROIRITIES
    #else
        #define THREAD_MAX_PROIRITIES       7
    #endif
    #if (THREAD_MAX_PROIRITIES < 2)
        #error "THREAD_MAX_PROIRITIES error"
    #endif
    
    #define THREAD_PRIORITY_DEFAULT         1
    #define THREAD_PRIORITY_MAX             (THREAD_MAX_PROIRITIES - 2)
    #define THREAD_PRIORITY_INIT            (THREAD_MAX_PROIRITIES - 1)
    
    // Max thread sleep time (ms)
    #define THREAD_MAX_SLEEP_TIME           ((uint32)~0 / 2)
    // Max thread sleep time (ticks)
    #define THREAD_MAX_SLEEP_TICKS          (THREAD_MAX_SLEEP_TIME / KERNEL_TICK_PERIOD)


// ----------------------------------------------------------------------------
// KThread
// ----------------------------------------------------------------------------

class KThread
{
    public:
        /**
          * @brief  Constructor
          * @param  name: Thread name
          * @param  stackSize: Thread stack size
          * @param  priority: Thread priority
          */
        KThread (const char* name, uint stackSize, uint priority);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~KThread ();
        
        /**
          * @brief  Suspend thread
          * @param  None
          */
        void Suspend (void);
        
        /**
          * @brief  Resume thread
          * @param  None
          */
        void Resume (void);

        /**
          * @brief  Thread entry point
          * @param  params: Thread parameters (pointer to KThread object)
          */
        static void EnterPoint (void* params);

        /**
          * @brief  Switch thread into sleep mode for specified time
          * @param  time: Minimum sleep state duration (ms)
          */
        static void sleep (uint time);
        
    protected:
        /**
          * @brief  Thread initialization
          * @param  None
          */
        virtual void init (void) {}

        /**
          * @brief  Thread main function
          * @param  None
          */
        virtual void run (void) = 0;
        
    private:
        void* m_handle;     // Thread handler

};


// ----------------------------------------------------------------------------
// KThreadWait
// ----------------------------------------------------------------------------

class KThreadWait : public KThread
{
    public:
        /**
          * @brief  Constructor
          * @param  name: Thread name
          * @param  stackSize: Thread stack size
          * @param  priority: Thread priority
          */
        KThreadWait (const char* name, uint stackSize, uint priority);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~KThreadWait ();
        
        /**
          * @brief  Timer handler (internal)
          * @param  timer: Timer object
          * @param  params: Additianal timer parameters
          */
        static void threadTimerHandler (KTimer* timer, void* params);
        
    protected:
        KTimer m_threadTimer;   // Timer for the thread waking up
        
        /**
          * @brief  Stop thread for specified time with ability to 
          *         wake it up early by Resume()
          * @param  time: Minimum sleep state duration if thread won't be resumed manually (ms)
          */
        void wait (uint time);
};
