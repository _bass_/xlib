//*****************************************************************************
//
// @brief   xLib system events
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "kernel/kernel.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define EV_SYS_RESTART                      EVENT_CODE(0xffff, 0)           // System restart (parameter: Reason of restart)
    #define EV_SYS_SCHEDULER_STARTED            EVENT_CODE(0xffff, 1)           // Scheduler was ran
    #define EV_SYS_MALLOC_FILED                 EVENT_CODE(0xffff, 2)           // Memory allocation error
    #define EV_SYS_STACK_OVERFLOW               EVENT_CODE(0xffff, 3)           // Steck overflow detected
    #define EV_SYS_TIME_UPDATED                 EVENT_CODE(0xffff, 4)           // System time was changed
    #define EV_SYS_CLOCK_CHANGED                EVENT_CODE(0xffff, 5)           // Clock source or/and frequency was changed

    #define EV_KEY_PRESSED                      EVENT_CODE(0xffff, 50)          // Button was pressed (parameter: button ID)
    #define EV_KEY_RELEASED                     EVENT_CODE(0xffff, 51)          // Button was released (parameter: button ID)
    #define EV_KEY_LONGPRESS                    EVENT_CODE(0xffff, 52)          // Button long press detected (parameter: button ID)

    #define EV_DEV_STATE_CHANGED                EVENT_CODE(0xffff, 100)         // Device state changed (parameter: TEvDevState)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

#if defined(DEV_OK)
    // EV_DEV_STATE_CHANGED event parameters
    typedef struct
    {
        CDevice*    pDev;
        uint        state;      // TDevState or extended state (values outside the TDevState range)
    } TEvDevState;
#endif
