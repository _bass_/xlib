//*****************************************************************************
//
// @brief   Mutexes interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// KMutex
// ----------------------------------------------------------------------------

class KMutex
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        KMutex (void);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~KMutex ();

        /**
          * @brief  Lock mutex
          * @param  None
          */
        void Lock (void);
        
        /**
          * @brief  Release mutex
          * @param  None
          */
        void Unlock (void);

    private:
        void*   m_mutex;    // Internal mutex object
};
