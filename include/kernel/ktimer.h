//*****************************************************************************
//
// @brief   Kernel software timer
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Handler of timeout completion
    class KTimer;
    typedef void (TTimerHdl) (KTimer* timer, void* params);


// ----------------------------------------------------------------------------
// KTimer
// ----------------------------------------------------------------------------

class KTimer
{
    public:
        /**
          * @brief  Constructor
          * @param  hdl: Timer (timeout completion) handler
          * @param  params: Additional handler parameters
          */
        KTimer (TTimerHdl* hdl, void* params);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~KTimer ();
        
        /**
          * @brief  Start timer
          * @param  time: Timer timeout
          * @return 0 or negative error code
          */
        int Start (uint time);

        /**
          * @brief  Stop timer
          * @param  None
          * @return 0 or negative error code
          */
        int Stop (void);

        /**
          * @brief  Restart timer with last timeout
          * @param  None
          * @return 0 or negative error code
          */
        int Restart (void);
        
        /**
          * @brief  Set / change timer timeout handler
          * @param  hdl: Timer handler
          * @param  params: Additional handler parameters
          */
        void SetHandler (TTimerHdl* hdl, void* params);
        
        /**
          * @brief  Generic timer service handler for timers processing
          * @param  timer: Timer object (unused)
          */
        static void genericTimerHandler (void* timer);
        
    protected:
        TTimerHdl*  m_cb;       // Timer handler
        void*       m_params;   // Timer handler parameters
        void*       m_handle;   // Timer internal descriptor
};
