//*****************************************************************************
//
// @brief   Debug IO subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cc.h"
#include "def_board.h"
#include <stdarg.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default debug level
    #if defined(CFG_KDEBUG_DEFAULT_LEVEL)
    #define KDEBUG_DEFAULT_LEVEL                        CFG_KDEBUG_DEFAULT_LEVEL
    #else
    #define KDEBUG_DEFAULT_LEVEL                        LVL_ALL
    #endif


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #if defined(CFG_SYS_NO_DEBUG)
        #define REGISTER_DEBUG(pCmd, pName, pHelp, handler)
        #define ATTACH_DEBUG(name)
        #define klog(...)
        #define kerror(...)
        #define kwarn(...)
        #define kinfo(...)
        #define kdebug(...)
        #define kprint(...)
        #define KDEBUG_SET_LEVEL(lvl)
        #define REGISTER_SYSLOG_HANDLER(hdl)

    #else
        // Debug module registration
        #define REGISTER_DEBUG(pCmd, pName, pHelp, handler) static TDbgLvl kDbgLevel = KDEBUG_DEFAULT_LEVEL; \
                                                            static const TDbgDesc kDbgDesc = { pCmd, pName, pHelp, &kDbgLevel, handler }; \
                                                            CC_VAR_USED_DECLARATION static const TDbgDesc* const pDbgDesc = &kDbgDesc; \
                                                            REGISTER_ITEM(xLib_kernel_dbgDesc, const TDbgDesc* const, pDbgDesc);


        // Debug output attachment to module with specified name
        #define ATTACH_DEBUG(name)                      static const TDbgDesc* pDbgDesc = kDebugFindDesc(name);

        // Modules debug output
        #define klog(...)                               do { \
                                                            kDebugPrint(pDbgDesc, LVL_NONE, __VA_ARGS__); \
                                                        } while(0)
        #define kerror(...)                             do { \
                                                            kDebugPrint(pDbgDesc, LVL_ERROR, __VA_ARGS__); \
                                                        } while(0)
        #define kwarn(...)                              do { \
                                                            kDebugPrint(pDbgDesc, LVL_WARN, __VA_ARGS__); \
                                                        } while(0)
        #define kinfo(...)                              do { \
                                                            kDebugPrint(pDbgDesc, LVL_INFO, __VA_ARGS__); \
                                                        } while(0)
        #define kdebug(...)                             do { \
                                                            kDebugPrint(pDbgDesc, LVL_DEBUG, __VA_ARGS__); \
                                                        } while(0)
        // Raw debug output without binding to any module
        #define kprint(...)                             kDebugPrint(NULL, LVL_NONE, __VA_ARGS__)

        // Set current debug level
        #define KDEBUG_SET_LEVEL(lvl)                   kDbgLevel = lvl

        // Debug messages loggin handler registration
        // To remove current logger call REGISTER_SYSLOG_HANDLER(NULL)
        #define REGISTER_SYSLOG_HANDLER(hdl)            extern TLogHdl* const pKdebugSyslogHandler = hdl
    #endif


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Debug levels
    typedef enum
    {
        LVL_NONE        = 0,
        LVL_ERROR       = 1,
        LVL_WARN        = 2,
        LVL_INFO        = 3,
        LVL_DEBUG       = 4,
        LVL_ALL         = 5
    } TDbgLvl;

    // Debug module descriptor
    typedef struct
    {
        const char* cmd;                                    // Command for access to the module
        const char* name;                                   // Module name (debug messages prefix)
        const char* help;                                   // Help text
        TDbgLvl*    pDebugLevel;                            // Pointer to variable to save current debug level
        void        (*hdl) (char** arg, int argCount);      // Commands handler
    } TDbgDesc;

    // Logging handler
    typedef void (TLogHdl) (TDbgLvl level, const char* name, const char* format, va_list* pArgs);


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

#if !defined(CFG_SYS_NO_DEBUG)
    #ifdef __cplusplus
    extern "C" {
    #endif

        /**
          * @brief  Formatted output of debug messages
          * @param  pDesc: Debug module descriptor
          * @param  level: Debug level of the message
          * @param  format: Message format
          */
        void kDebugPrint (const TDbgDesc* pDesc, TDbgLvl level, const char* format, ...);

        /**
          * @brief  Find debug module descriptor
          * @param  name: Debug module name
          * @return Debug module descriptor or NULL
          */
        const TDbgDesc* kDebugFindDesc (const char* name);

    #ifdef __cplusplus
    }
    #endif
#endif
