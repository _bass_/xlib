//*****************************************************************************
//
// @brief   RTOS core functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // System tick duration (ms)
    #define KERNEL_TICK_PERIOD                  (1000 / CFG_SYSTMR_FREQ)

    // Event code for catching all events
    #define KERNEL_EV_CODE_ALL                  0


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    // System event handler registration (TEventHdl)
    #define REGISTER_EVENT_CODE_HANDLER(code, hdl)  REGISTER_ITEM(xLib_kernel_evDesc, const TEvHdlDesc, {code, &hdl})

    #define REGISTER_EVENT_HANDLER(hdl)             REGISTER_EVENT_CODE_HANDLER(KERNEL_EV_CODE_ALL, hdl)

    #define EVENT_CODE(group, event)                ( (uint16)(group << 8) | (uint16)(event & 0xff) )

    // Background services registration
    // Services are called sequentially during system inactivity (IDLE).
    // Service handlers should minimize CPU time usage.
    #define REGISTER_BG_SERVICE(hdl)                REGISTER_ITEM(xLib_kernel_bgHdl, const TBgHdl, hdl)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Event hadndler descriptor
    typedef struct
    {
        uint    evCode;
        void    (*handler) (uint code, void* params);
    } TEvHdlDesc;

    // Background service handler
    typedef void (*TBgHdl) (void);


// ----------------------------------------------------------------------------
// Kernel
// ----------------------------------------------------------------------------

class Kernel
{
    public:
        /**
          * @brief  Start kernel (scheduler)
          * @param  None
          */
        static void Start (void);

        /**
          * @brief  Stop kernel (scheduler)
          * @param  None
          */
        static void Stop (void);

        /**
          * @brief  Activate scheduler with current thread blocking (switch to another thread)
          * @param  None
          */
        static void Schedule (void);

        /**
          * @brief  Critical section activation
          * @param  None
          */
        static void EnterCritical (void);

        /**
          * @brief  Critical section deactivation
          * @param  None
          */
        static void ExitCritical (void);

        /**
          * @brief  System time reading (time from system start)
          * @param  None
          * @return Number of system ticks
          */
        static uint32 GetJiffies (void);

        /**
          * @brief  CPU load reading
          * @param  None
          * @return CPU load (%)
          */
        static uint GetCpuLoad (void);

        /**
          * @brief  Event generation (in-place)
          * @param  code: Event code
          * @param  params: Event parameters
          */
        static void RiseEvent (uint code, void* params = NULL);

        /**
          * @brief  Event generation (delayed, processing from separate thread)
          * @param  code: Event code
          * @param  params: Event parameters
          * @param  size: Parameters data size
          */
        static void PostEvent (uint code, void* params = NULL, uint size = 0);
};
