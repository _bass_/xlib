//*****************************************************************************
//
// @brief   Input/output functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/char.h"
#include "kernel/kmutex.h"
#include <stdarg.h>


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    // Standard formatted output into kstdout stream
    #define printf(...)             fprintf(kstdout, __VA_ARGS__)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // IO stream descriptor
    typedef struct
    {
        CDevChar*   pDev;
        KMutex*     pMutex;
        char        buff[32];  // Buffer for output argument formatting
    } KStream;


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    /**
      * @brief  Formatted output
      * @param  stream: Stream object
      * @param  format: Output string format
      * @param  ...: Additional format arguments
      * @return Number of bytes written into the stream or negative error code
      */
    int fprintf (KStream* stream, const char* format, ...);

    /**
      * @brief  Formatted output
      * @param  stream: Stream object
      * @param  format: Output string format
      * @param  args: Additional format arguments
      * @return Number of bytes written into the stream or negative error code
      */
    int vfprintf (KStream* stream, const char* format, va_list args);

    /**
      * @brief  Formatted output into specified buffer
      * @param  buff: Output buffer
      * @param  size: Output buffer size
      * @param  format: Output string format
      * @param  ...: Additional format arguments
      * @return Number of bytes written into the buffer or negative error code
      */
    int sprintf (void* buff, uint size, const char* format, ...);

    /**
      * @brief  Formatted output into specified buffer
      * @param  buff: Output buffer
      * @param  size: Output buffer size
      * @param  format: Output string format
      * @param  args: Additional format arguments
      * @return Number of bytes written into the buffer or negative error code
      */
    int vsprintf (void* buff, uint size, const char* format, va_list args);

    /**
      * @brief  Read data from standard input stream (kstdin)
      * @param  None
      * @return Byte from the stream or negative error code
      */
    int getchar (void);


// ----------------------------------------------------------------------------
// Variables
// ----------------------------------------------------------------------------

    // Standard IO streams
    extern KStream* kstdout;
    extern KStream* kstdin;
