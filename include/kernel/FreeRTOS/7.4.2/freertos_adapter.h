//*****************************************************************************
//
// @brief   FreeRTOS adapter to be able to use in xLib
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "memory/heap.h"


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    // Redirecting memory allocation related functions
    #define pvPortMalloc                malloc
    #define vPortFree                   free
    #define xPortGetFreeHeapSize        heapFreeSize


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

    /**
      * @brief  Start task hook (see kernel.cpp)
      * @param  None
      */
    void kernelOnTaskIn (void);

    /**
      * @brief  End of task hook (see kernel.cpp)
      * @param  None
      */
    void kernelOnTaskOut (void);

#ifdef __cplusplus
}
#endif
