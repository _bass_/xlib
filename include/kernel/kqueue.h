//*****************************************************************************
//
// @brief   Queue interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// KQueue
// ----------------------------------------------------------------------------

class KQueue
{
    public:
        /**
          * @brief  Constructor
          * @param  len: Number of items in the queue
          * @param  itemSize: Single item size
          */
        KQueue (uint len, uint itemSize);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~KQueue ();

        /**
          * @brief  Add item in the queue
          * @param  pItem: Item to add
          */
        void Add (const void* pItem);

        /**
          * @brief  Read item from the queue. The read value will be deleted
          * @param  pItem: Pointer to variable to save item data
          * @param  timeout: Maximum timeout of waiting data in case of empty queue
          * @return 0 or negative error code
          */
        int Get (void* pItem, uint timeout = ~0u);

        /**
          * @brief  Read item from the queue (without deleting item from the queue)
          * @param  pItem: Pointer to variable to save item data
          * @return 0 or negative error code
          */
        int Peek (void* pItem);

        /**
          * @brief  Read number of items in the queue
          * @param  None
          * @return Number of items in the queue
          */
        uint Count (void);

        /**
          * @brief  Check if the queue is full
          * @param  None
          * @return true - the queue is full, otherwise false
          */
        bool IsFull (void);

    private:
        void* m_handler;    // Queue internal handler
};
