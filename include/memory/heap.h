//*****************************************************************************
//
// @brief   Dynamic memory manger (heap)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

#if defined(CFG_MEM_USE_STDLIB)
    #include <stdlib.h>

    #ifdef __cplusplus
    extern "C"
    {
    #endif
        
        inline size_t heapFreeSize (void)
        {
            return 0;
        }
    
    #ifdef __cplusplus
    }
    #endif

#else
    #include <string.h>

    #ifdef __cplusplus
    extern "C"
    {
    #endif

        void*   malloc (size_t size);
        void*   calloc (size_t num, size_t size);
        void    free (void* ptr);
        size_t  heapFreeSize (void);
    
    #ifdef __cplusplus
    }
    #endif

#endif
