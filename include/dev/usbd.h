//*****************************************************************************
//
// @brief   USB device interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/char.h"
#include "misc/fifo.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Descriptor types
    #define USB_DESCRIPTOR_DEVICE                   0x01
    #define USB_DESCRIPTOR_CONFIGURATION            0x02
    #define USB_DESCRIPTOR_STRING                   0x03
    #define USB_DESCRIPTOR_INTERFACE                0x04
    #define USB_DESCRIPTOR_ENDPOINT                 0x05
    #define USB_DESCRIPTOR_QUALIFIER                0x06
    #define USB_DESCRIPTOR_SPEED_CONFIGURATOR       0x07
    #define USB_DESCRIPTOR_INTERFACE_POWER          0x08
    #define USB_DESCRIPTOR_OTG                      0x09
    #define USB_DESCRIPTOR_DEBUG                    0x0A
    #define USB_DESCRIPTOR_ASSOCIATION              0x0B
    // Class-specific descriptors
    #define USB_DESCRIPTOR_CS_INTERFACE             0x24
    #define USB_DESCRIPTOR_CS_ENDPOINT              0x25

    // USB standards codes
    #define USB_2_0                                 0x0200
    #define USB_1_1                                 0x0110

    #define USB_CLASS_DEFAULT                       0x00
    #define USB_SUBCLASS_DEFAULT                    0x00
    #define USB_PROTOCOL_DEFAULT                    0x00

    // Device class codes
    // None - Device information will be defined in interface descriptor
    #define USB_CLASS_NONE                          0x00
    #define USB_CLASS_AUDIO                         0x01
    #define USB_CLASS_CDC                           0x02
    #define USB_CLASS_HID                           0x03
    #define USB_CLASS_PHYSICAL                      0x05
    #define USB_CLASS_STILL_IMAGING                 0x06
    #define USB_CLASS_PRINTER                       0x07
    #define USB_CLASS_MASS_STORAGE                  0x08
    #define USB_CLASS_HUB                           0x09
    #define USB_CLASS_CDC_DATA                      0x0A
    #define USB_CLASS_SMART_CARD                    0x0B
    #define USB_CLASS_CONTENT_SECURITY              0x0D
    #define USB_CLASS_VIDEO                         0x0E
    #define USB_CLASS_PESONAL_HEALTHCARE            0x0F
    #define USB_CLASS_DIAGNOSTIC                    0xDC
    #define USB_CLASS_WIRELESS_CONTROLLER           0xE0
    #define USB_CLASS_MISCELLANEOUS                 0xEF
    #define USB_CLASS_APPLICATION_SPECIFIC          0xFE
    #define USB_CLASS_VENDOR_SPECIFIC               0xFF

    // Interface class codes
    #define USB_INTERFACE_CLASS_COMMUNICATION       0x02

    // Types of power supply
    #define USB_POWER_BUS                           (1 << 7)
    #define USB_POWER_SELF                          (1 << 6)
    #define USB_POWER_REMOTE_WAKEUP                 (1 << 5)

    // Operation codes for CLEAR/SET FEATURE requests
    #define USB_ENDPOINT_HALT                       0
    #define USB_DEVICE_REMOTE_WAKEUP                1
    #define USB_TESTMODE                            2

    // Types and masks of requests
    #define USB_REQUEST_RESPONSE_FLAG               0x80
    #define USB_REQUEST_TYPE_MASK                   0x60
    #define USB_REQUEST_STANDARD                    0x00
    #define USB_REQUEST_CLASS                       0x20
    #define USB_REQUEST_VENDOR                      0x40

    // Requests
    #define USB_REQUEST_GET_STATUS                  0
    #define USB_REQUEST_CLEAR_FEATURE               1
    #define USB_REQUEST_RESERVED_0                  2
    #define USB_REQUEST_SET_FEATURE                 3
    #define USB_REQUEST_RESERVED_1                  4
    #define USB_REQUEST_SET_ADDRESS                 5
    #define USB_REQUEST_GET_DESCRIPTOR              6
    #define USB_REQUEST_SET_DESCRIPTOR              7
    #define USB_REQUEST_GET_CONFIGURATION           8
    #define USB_REQUEST_SET_CONFIGURATION           9
    #define USB_REQUEST_GET_INTERFACE               10
    #define USB_REQUEST_SET_INTERFACE               11
    #define USB_REQUEST_SYNCH_FRAME                 12

    // Request destination codes
    #define USB_REQUEST_DEST_MASK                   0x03
    #define USB_REQUEST_DEST_DEVICE                 0
    #define USB_REQUEST_DEST_INTERFACE              1
    #define USB_REQUEST_DEST_ENDPOINT               2
    #define USB_REQUEST_DEST_OTHER                  3

    // Masks of device attributes
    #define USB_CFG_ATTR_SELF_POWERED_MASK          (1 << 6)
    #define USB_CFG_ATTR_REMOTE_WKUP_MASK           (1 << 5)

    // Endpoints
    #define USB_EP_NUM_MASK                         0x0f
    #define USB_EP_DIR_MASK                         (1 << 7)
    #define USB_EP_DIR_OUT                          (0 << 7)
    #define USB_EP_DIR_IN                           (1 << 7)

    // Endpoints types
    #define USB_EP_TYPE_MASK                        0x03
    #define USB_EP_TYPE_CTRL                        0x00
    #define USB_EP_TYPE_ISO                         0x01
    #define USB_EP_TYPE_BULK                        0x02
    #define USB_EP_TYPE_INT                         0x03
    // Synchronization type for ISO endpoints
    #define USB_EP_SYNC_TYPE_MASK                   (3 << 2)
    #define USB_EP_SYNC_TYPE_NOSYNC                 (0 << 2)
    #define USB_EP_SYNC_TYPE_ASYNC                  (1 << 2)
    #define USB_EP_SYNC_TYPE_ADAPTIVE               (2 << 2)
    #define USB_EP_SYNC_TYPE_SYNC                   (3 << 2)
    // Usage type for ISO endpoints
    #define USB_EP_USAGE_TYPE_MASK                  (3 << 4)
    #define USB_EP_USAGE_TYPE_DATA                  (0 << 4)
    #define USB_EP_USAGE_TYPE_FEEDBACK              (1 << 4)
    #define USB_EP_USAGE_TYPE_EX_FEEDBACK           (2 << 4)


    // Languages
    #define USB_LANG_ID_EN                          0x0409
    #define USB_LANG_ID_RU                          0x0419


    // Consumption of current (mA)
    #define USB_POWER(x)                            (x / 2)

    // Endpoint address calculation
    #define USB_EP_ADDR(dir, num)                   ( dir | (num & USB_EP_NUM_MASK) )


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #pragma pack(1)

    // Generic header for all descriptors
    typedef struct
    {
        uchar   bLength;                        // Descriptor length (bytes)
        uchar   bDescriptorType;                // Descriptor type
    } TUsbDescriptorHeader;

    // Device descriptor
    typedef struct
    {
        uchar   bLength;                        // This descriptor length (bytes)
        uchar   bDescriptorType;                // Descriptor type
        uint16  bcdUSB;                         // USB version (BCD format)
        uchar   bDeviceClass;                   // Code of device class
        uchar   bDeviceSubClass;                // Subclass code
        uchar   bDeviveProtocol;                // Protocol
        uchar   bMaxPacketSize0;                // Max. packet size for endpoint 0
        uint16  idVendor;                       // Vendor ID
        uint16  idProduct;                      // Product ID
        uint16  bcdDevice;                      // Device version (BCD format)
        uchar   iManufacturer;                  // String index of manufacturer in string descriptor
        uchar   iProduct;                       // String index of product in string descriptor
        uchar   iSerialNumber;                  // String index of device serial number in string descriptor
        uchar   bNumConfigurations;             // Number of configurations
    } TUsbDevDescriptor;

    // Configuration descriptor
    typedef struct
    {
        uchar   bLength;                        // This descriptor length (bytes)
        uchar   bDescriptorType;                // Descriptor type
        uint16  wTotalLength;                   // Total data size for this configuration
        uchar   bNumInterfaces;                 // Number of interfaces in this configutation
        uchar   bConfigurationValue;            // Configuration value which is used for Set Configuration command
        uchar   iConfiguration;                 // String index of configuration description in string descriptor
        uchar   bmAttributes;                   // Configuration attributes
        uchar   bMaxPower;                      // Max. power supply of the device (x 2mA)
    } TUsbCfgDescriptor;

    // Interface descriptor
    typedef struct
    {
        uchar  bLength;                         // This descriptor length (bytes)
        uchar  bDescriptorType;                 // Descriptor type
        uchar  bInterfaceNumber;                // Number of the interface
        uchar  bAlternateSetting;               // Alternate setting value
        uchar  bNumEndpoints;                   // Number of endpoints used in the interface (EP0 is excluded)
        uchar  bInterfaceClass;                 // Code of interface class
        uchar  bInterfaceSubClass;              // Code of interface subclass
        uchar  bInterfaceProtocol;              // Code of interface protocol
        uchar  iInterface;                      // String index of interface description in string descriptor
    } TUsbIfaceDescriptor;

    // Endpoint descriptor
    typedef struct
    {
        uchar   bLength;                        // This descriptor length (bytes)
        uchar   bDescriptorType;                // Descriptor type
        uchar   bEndpointAddress;               // Endpoint address
        uchar   bmAttributes;                   // Endpoint type and additional attributes (used for isochronous endpoints)
        uint16  wMaxPacketSize;                 // Max. packet size of th endpoint (bytes)
        uchar   bInterval;                      // Polling interval of the endpoint (for ISO and INTERRUPT types)
    } TUsbEpDescriptor;

    // Request packet
    typedef union
    {
        struct
        {
            uchar   bmRequestType;              // Request type
            uchar   bRequest;                   // Request code
            uint16  wValue;                     // Value
            uint16  wIndex;                     // Index
            uint16  wLength;                    // Expected data length (bytes) in DATA phase
        };
        uchar data[8];
    } TUsbRequest;

    #pragma pack()

    // String descriptors list
    typedef struct
    {
        const TUsbDescriptorHeader** pStrings;  // Pointer to array of string descriptors
        uint                    count;          // Number of string descriptors
    } TUsbdStringList;

    // USB device driver descriptors
    typedef struct
    {
        TUsbDevDescriptor*      pDev;           // Pointer to devie descriptor
        TUsbCfgDescriptor**     pCfg;           // Pointer to configuration descriptors list
                                                // including interface and endpoints descriptors
        TUsbdStringList*        pStringList;    // String descriptors
    } TUsbdDescriptors;

    // Configuration parameters
    typedef struct
    {
        const TGpio             pinPUP;
        const TUsbdDescriptors* pDescriptors;
    } TUsbdOptions;

    // USB device state
    enum TUsbState
    {
        USB_STATE_IDLE          = 0,
        USB_STATE_ADDRESS
    };

    // Endpoint state
    enum TEpState
    {
        USB_EP_STATE_DISABLED   = 0,
        USB_EP_STATE_ENABLED
    };

    // Class-specific requests handler
    typedef int (TCsRequestHdl) (const TUsbRequest* request, void* arg);


// ----------------------------------------------------------------------------
// CDevUsbd
// ----------------------------------------------------------------------------

class CDevUsbd : public CDevice
{
    public:
        CDevUsbd ();
        virtual ~CDevUsbd ();

        /**
          * @brief  Send data through specified endpoint
          * @param  epNum: Endpoint number
          * @param  data: Data for send
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        virtual int Write (uint epNum, const void* data, uint size) = 0;

        /**
          * @brief  Read data from specified endpoint
          * @param  epNum: Endpoint number
          * @param  data: Buffer for read data
          * @param  size: Buffer size
          * @return Written into the buffer data size or negative error code
          */
        virtual int Read (uint epNum, void* buff, uint size) = 0;

        /**
          * @brief  Endpoints send buffer bonding
          * @param  epNum: Endpoint number
          * @param  pBuff: Pointer to buffer object
          */
        virtual void EpSetBuffTx (uint epNum, TFifo* pBuff) = 0;

        /**
          * @brief  Endpoints receive buffer bonding
          * @param  epNum: Endpoint number
          * @param  pBuff: Pointer to buffer object
          */
        virtual void EpSetBuffRx (uint epNum, TFifo* pBuff) = 0;

        /**
          * @brief  Set class-specific requests handler
          * @param  pHdl: Handler
          * @param  arg: Pointer to additional arguments for handler
          */
        void SetRequestHandler (TCsRequestHdl* pHdl, void* arg);

    protected:
        const TUsbdDescriptors* m_pDesc;    // Set of descriptors
        uint                    m_cfgNum;   // Number of active configuration
        uint                    m_devAddr;  // Device address (set during enumeration)

        /**
          * @brief  Searching for configuration descriptor
          * @param  num: Configuration number (0 means current configuration)
          * @return Pointer to configuration descriptor or NULL on error
          */
        const TUsbCfgDescriptor* getDescCfg (uint num = 0);

        /**
          * @brief  Searching for endpoint descriptor
          * @param  epNum: Endpoint number
          * @return Pointer to endpoint descriptor or NULL on error
          */
        const TUsbEpDescriptor* getDescEp (uint epNum);

        /**
          * @brief  Handler of standard requests 
          * @param  request: Request descriptor
          * @return 0 or negative error code (see DEV_xx)
          */
        int onRequest (const TUsbRequest* request);

        /**
          * @brief  Endpoint initialization
          * @param  pDesc: Endpoint descriptor
          */
        virtual void epInit (const TUsbEpDescriptor* pDesc) = 0;

        /**
          * @brief  Read endpoint state
          * @param  epNum: Endpoint number
          * @return Endpoint state
          */
        virtual TEpState epGetState (uint epNum) = 0;

        /**
          * @brief  Set endpoint state
          * @param  epNum: Endpoint number
          * @param  state: Endpoint state
          */
        virtual void epSetState (uint epNum, TEpState state) = 0;

    private:
        TCsRequestHdl*  m_pCsRequestHdl;        // Class-specific requests handler
        void*           m_pCsRequestHdlArg;     // Pointer to additional arguments for handler

        /**
          * @brief  Standard requests processing
          * @param  request: Request descriptor
          */
        void hdlReqGetStatus (const TUsbRequest* request);
        void hdlReqFeature (const TUsbRequest* request);
        void hdlReqSetAddress (const TUsbRequest* request);
        void hdlReqDescriptor (const TUsbRequest* request);
        void hdlReqConfig (const TUsbRequest* request);
        void hdlReqInterface (const TUsbRequest* request);
};
