//*****************************************************************************
//
// @brief   Input device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/device.h"
#include <forward_list>


// ----------------------------------------------------------------------------
// CDevInput
// ----------------------------------------------------------------------------

class CDevInput : public CDevice
{
    public:
        // Message type (events). Values must be power of 2.
        enum EMsgType
        {
            E_MSG_POINTER   = 1,
            E_MSG_KBD       = 2,
        };

        // Type of Pointer event
        enum EPointerEvent
        {
            E_EV_POINTER_MOVE,
            E_EV_POINTER_PRESS,
        };

        // Pointer event data
        struct TMsgPointer
        {
            EPointerEvent type;
            uint16  x;
            uint16  y;
            union
            {
                struct
                {
                    int16   dx;
                    int16   dy;
                } move;
                struct
                {
                    bool    isPressed;
                } press;
            };
        };

        // Keyboard event data
        struct TMsgKbd
        {
            bool    state;      // True - button is pressed, false - released
            uint16  scanCode;
            uint    symbol;
            union
            {
                uint    val;
                struct
                {
                    uint    ctrl    : 1;
                    uint    shift   : 1;
                };
            } modifiers;
        };

        // Generic event message
        struct TMsg
        {
            EMsgType    type;
            union
            {
                TMsgPointer pointer;
                TMsgKbd     kbd;
            };
        };

        // Events handler
        typedef void (*TMsgHdl)(CDevInput* dev, const TMsg& msg, void* param);

        /**
         * @brief Constructor
         * @param devIndex: System index of the device
         * @param supportedEvents: Mask of supported event types
         */
        CDevInput (uint devIndex, uint supportedEvents);

        /**
         * @brief Subscribe handler to get specified event notifications
         * @param msgType: Event type
         * @param hdl: Events handler
         * @param param: Events handler additional parameter to pass
         */
        void Subscribe (EMsgType msgType, TMsgHdl hdl, void* param = nullptr);

        /**
         * @brief Unsubscribe handler to stop getting specified event notifications
         * @param msgType: Event type
         * @param hdl: Events handler
         * @param param: Events handler additional parameter to pass
         */
        void Unsubscribe (EMsgType msgType, TMsgHdl hdl, void* param = nullptr);

    protected:
        /**
         * @brief Send event notification
         * @param msg: Event message data
         */
        void notify (const TMsg& msg);

    private:
        // Event handler descriptor
        struct THdlDesc
        {
            THdlDesc (EMsgType t, TMsgHdl h, void* p):
                type(t), hdl(h), param(p)
            {}

            EMsgType    type;
            TMsgHdl     hdl;
            void*       param;
        };

        const uint32                m_supportedEvents;  // Mask of supported events (see EMsgType)
        std::forward_list<THdlDesc> m_handlers;
};
