//*****************************************************************************
//
// @brief   Basic device interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Device prefixes (names)
    #define DEV_NAME_USART                  "com"
    #define DEV_NAME_SPI                    "spi"
    #define DEV_NAME_I2C                    "i2c"
    #define DEV_NAME_ADC                    "adc"
    #define DEV_NAME_DAC                    "dac"
    #define DEV_NAME_USB                    "usb"
    #define DEV_NAME_CDC                    "cdc"
    #define DEV_NAME_TIMER                  "tim"
    #define DEV_NAME_NET                    "net"
    #define DEV_NAME_GSM                    "gsm"
    #define DEV_NAME_BLOCK                  "blk"
    #define DEV_NAME_DISPLAY                "lcd"
    #define DEV_NAME_CAN                    "can"
    #define DEV_NAME_INPUT                  "inp"

    // Device ID for automatic index allocation
    #define DEV_AUTO_ID                     (0xFFFFFFFF)

    #define DEV_OK                          0
    // Error codes
    #define DEV_ERR                         (-1)
    #define DEV_IO_ERR                      (-2)

    // IOCTL code creation
    #define IOCTL_CODE(type, code)          ( ((uint32)(type) << 16) | (uint32)(code) )


    // Result codes for ioctl commands
    #define IOCTL_OK                        0
    #define IOCTL_NOT_SUPPORTED             1
    #define IOCTL_ERROR                     (-1)


    // Devices
    #define IOCTL_TIM_START                 IOCTL_CODE(0, 0)
    #define IOCTL_TIM_STOP                  IOCTL_CODE(0, 1)
    #define IOCTL_READ_DIAG                 IOCTL_CODE(0, 2)

    // DevChar
    #define IOCTL_SPI_CS_ACTIVATE           IOCTL_CODE(1, 0)
    #define IOCTL_SPI_CS_RELEASE            IOCTL_CODE(1, 1)
    #define IOCTL_USB_CONNECT_CTRL          IOCTL_CODE(1, 2)
    #define IOCTL_LCD_CLEAR                 IOCTL_CODE(1, 3)
    #define IOCTL_LCD_CURSOR_HOME           IOCTL_CODE(1, 4)

    // DevNet
    #define IOCTL_NET_IS_LINKED             IOCTL_CODE(3, 0)

    // RF
    #define IOCTL_RF_SET_FREQUENCY          IOCTL_CODE(4, 0)
    #define IOCTL_RF_RECV_INTERNAL          IOCTL_CODE(4, 1)
    #define IOCTL_RF_SET_MODE_RX            IOCTL_CODE(4, 2)    // args: timeout (ms). 0 - without timeout

    // Modem
    #define IOCTL_MODEM_GET_IMEI            IOCTL_CODE(5, 0)
    #define IOCTL_MODEM_GET_IMSI            IOCTL_CODE(5, 1)
    #define IOCTL_MODEM_GET_CSQ             IOCTL_CODE(5, 2)
    #define IOCTL_MODEM_GET_REG             IOCTL_CODE(5, 3)
    #define IOCTL_MODEM_GET_SIM_STATE       IOCTL_CODE(5, 4)
    #define IOCTL_MODEM_RESTART             IOCTL_CODE(5, 5)
    #define IOCTL_MODEM_PORT_ACTIVATE       IOCTL_CODE(5, 6)
    #define IOCTL_MODEM_PORT_DEACTIVATE     IOCTL_CODE(5, 7)
    #define IOCTL_MODEM_SET_LEVEL_MIC       IOCTL_CODE(5, 8)
    #define IOCTL_MODEM_GPRS_GET_ADDR       IOCTL_CODE(5, 10)
    #define IOCTL_MODEM_SMS_SET_INFO_IN     IOCTL_CODE(5, 20)
    #define IOCTL_MODEM_SMS_SET_INFO_OUT    IOCTL_CODE(5, 21)


    // Device ID creation
    #define DEV_ID(n, i)                    ( (uint32)n[0] | (uint32)n[1] << 8 | (uint32)n[2] << 16 | (uint32)i << 24 )

    // Device registration
    #define DEV_REGISTER(n, i, obj)         REGISTER_ITEM(xLib_devList, const TDevDesc, { DEV_ID(n, i), &(obj) })


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Device state
    typedef enum
    {
        DEV_UNKNOWN = 0,
        DEV_CLOSED,
        DEV_OPENING,
        DEV_OPENED,
        DEV_CLOSING
    } TDevState;

    // Device descriptor
    typedef struct
    {
        uint32      id;
        void*       pDev;
    } TDevDesc;


// ----------------------------------------------------------------------------
// CDevice
// ----------------------------------------------------------------------------

class CDevice
{
    public:
        /**
          * @brief  Default constructor for static devices (for backward compatibility, dynamic list isn't used)
          * @param  None
          */
        CDevice () {}

        /**
          * @brief  Constructor for dynamic created devices
          * @param  None
          */
        CDevice (const char* name, uint index);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CDevice ();

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        virtual int Open (void) = 0;

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        virtual int Close (void) = 0;

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        virtual int Configure (const void* params) = 0;

        /**
          * @brief  Advanced device control
          * @param  code: Command code
          * @param  arg: Pointer to command arguments
          * @return Operation result (see IOCTL_xx)
          */
        virtual int Ioctl (uint code, void* arg = NULL) { (void)code; (void)arg; return IOCTL_NOT_SUPPORTED; }

        /**
          * @brief  Read device state
          * @param  None
          * @return Device state
          */
        virtual TDevState GetState (void) const { return DEV_UNKNOWN; }
};


// ----------------------------------------------------------------------------
// Public functions
// ----------------------------------------------------------------------------

    /**
      * @brief  Find device object
      * @param  name: Device name
      * @param  index: Device index
      * @return Pointer to device object or NULL on error
      */
    const CDevice* GetDevice (const char* name, uint index);

    /**
      * @brief  Get list of registered devices
      * @param  pEnd: Pointer to the end of registered devices list (after last descriptor)
      * @return Pointer to first device descriptor in the list
      */
    const TDevDesc* GetDevList (const TDevDesc** pEnd);
