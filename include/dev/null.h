//*****************************************************************************
//
// @brief   Dummy character device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/char.h"


// ----------------------------------------------------------------------------
// CDevNull
// ----------------------------------------------------------------------------

class CDevNull : public CDevChar
{
    public:
        // Configuration parameters
        typedef struct
        {
            void*   pBuff;
            uint    size;
        } TOptions;

        CDevNull ();
        ~CDevNull ();

        int     Open (void) { return DEV_OK; }
        int     Close (void) { return DEV_OK; }

        int     Configure (const void* params);
        int     PutChar (const char c);
        int     GetChar (void);
        int     Write (const void* buff, uint size);
        int     Read (void* buff, uint size);

    private:
        char*   m_buff;
        uint    m_size;
        uint    m_index;
};
