//*****************************************************************************
//
// @brief   Interface of character device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/device.h"


// ----------------------------------------------------------------------------
// CDevChar
// ----------------------------------------------------------------------------

class CDevChar : public CDevice
{
    public:
        CDevChar () : CDevice () {}
        CDevChar (const char* name, uint index) : CDevice (name, index) {}
        
        /**
          * @brief  Send character to output stream
          * @param  c: character
          * @return 0 or negative error code
          */
        virtual int PutChar (const char c) = 0;

        /**
          * @brief  Get character from input stream
          * @return Character or negative error code
          */
        virtual int GetChar (void) = 0;

        /**
          * @brief  Send data to output stream
          * @param  data: Data
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        virtual int Write (const void* data, uint size) = 0;

        /**
          * @brief  Read data from input stream
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Written data size or negative error code
          */
        virtual int Read (void* buff, uint size) = 0;
};
