//*****************************************************************************
//
// @brief   Interface of network device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/device.h"
#include "net/socket.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Error codes
    #define NET_OK                          0
    #define NET_ERR                         (-1)


// ----------------------------------------------------------------------------
// CDevNet
// ----------------------------------------------------------------------------

class CDevNet : public CDevice
{
    public:
        /**
          * @brief  Constructor
          * @param  family: Network protocol family supported by the device
          *         (see NET_FAMILY_xx in appropriate socket family header file)
          */
        CDevNet (uint family);

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CDevNet ();

        /**
          * @brief  Send network packet through the device
          * @param  skb: Network packet descriptor
          * @return Processed data size or negative error code
          */
        virtual int Write (const TSkb* skb) = 0;

        /**
          * @brief  Set address of the network device
          * @param  pAddr: Network address descriptor
          * @return NET_OK or negative error code
          */
        virtual int SetAddr (const TNetAddr* pAddr);
        
        /**
          * @brief  Read address of the network device
          * @param  pAddr: Pointer to network address descriptor (to save address)
          */
        virtual void GetAddr (TNetAddr* pAddr) const;

    protected:
        TNetAddr    m_addr;         // Network address of the device
        TSkb        m_skb;          // Network packet of received data

        /**
          * @brief  Reset network packet descriptor
          * @param  skb: Network packet descriptor
          */
        virtual void resetSkb (TSkb* skb) = 0;

        /**
          * @brief  Received data processing
          * @param  data: Received data
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int onDataReceive (void* data, uint size);
};
