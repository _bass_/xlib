//*****************************************************************************
//
// @brief   Cached IO through block device
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/block.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Cache state flags
    typedef union
    {
        uint        val;
        struct
        {
            uint    lock    : 1;            // Flag of usage
            uint    valid   : 1;            // Flag of valid data in cache
            uint    dirty   : 1;            // Flag of changed data in cache
        };
    } TCacheFlags;

    #pragma pack(1)

    // Cache of block data
    typedef struct
    {
        uint        block;                  // Block number
        uint32      time;                   // Time of last block data change
        TCacheFlags flags;                  // Cache state flags
        char        buff[];                 // Start of buffer with block data
    } TBCache;

    #pragma pack()


// ----------------------------------------------------------------------------
// CBlockCache
// ----------------------------------------------------------------------------

class CBlockCache : public CDevBlock
{
    public:
        CBlockCache (CDevBlock* dev, uint count);
        ~CBlockCache ();

        int     Open (void);
        int     Close (void);
        int     Configure (const void* params) { return m_pDev->Configure(params); }
        int     Ioctl (uint code, void* arg = NULL) { return m_pDev->Ioctl(code, arg); }

        int     Read (void* buff, uint block, uint count);
        int     Write (const void* data, uint block, uint count);

        uint    BlockSize (void) { return m_pDev->BlockSize(); }
        uint    BlocksCount (void) { return m_pDev->BlocksCount(); }

        // Scheduler of cache data synchronization
        static void cacheScheduler (void);

    private:
        CBlockCache*    m_pNext;        // Pointer to next item in cache objects list
        CDevBlock*      m_pDev;         // Block device
        const uint      m_count;        // Number of cached blocks
        TBCache*        m_pCache;       // Pointer to start of cache descriptors list

        void        flush (void);
        TBCache*    getCache (uint index);
};
