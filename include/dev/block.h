//*****************************************************************************
//
// @brief   Interface of block device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/device.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// CDevBlock
// ----------------------------------------------------------------------------

class CDevBlock : public CDevice
{
    public:
        virtual ~CDevBlock () {}

        /**
          * @brief  Read data from block device
          * @param  buff: Buffer for read data
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        virtual int Read (void* buff, uint block, uint count = 1) = 0;

        /**
          * @brief  Write data to block device
          * @param  data: Pointer to data to write
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        virtual int Write (const void* data, uint block, uint count = 1) = 0;

        /**
          * @brief  Erase block on block device
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        virtual int Erase (uint block, uint count = 1) = 0;

        /**
          * @brief  Advanced block device control
          * @param  code: Command code
          * @param  arg: Pointer to command arguments
          * @return Operation result (see IOCTL_xx)
          */
        virtual int Ioctl (uint code, void* arg = NULL) { UNUSED_VAR(code); UNUSED_VAR(arg); return IOCTL_NOT_SUPPORTED; }

        /**
          * @brief  Get block size
          * @return Block size (bytes)
          */
        virtual uint BlockSize (void) = 0;

        /**
          * @brief  Get number of blocks on device
          * @return Numbers of blocks
          */
        virtual uint BlocksCount (void) = 0;
};
