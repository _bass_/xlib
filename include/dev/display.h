//*****************************************************************************
//
// @brief   Display device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/device.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Pixel data
    typedef union
    {
        uint8       data[3];
        struct
        {
            uint8   r;
            uint8   g;
            uint8   b;
        };
    } TPixel;


// ----------------------------------------------------------------------------
// CDevDisplay
// ----------------------------------------------------------------------------

class CDevDisplay : public CDevice
{
    public:
        /**
          * @brief  Constructor
          * @param  None
          */
        CDevDisplay ();

        /**
          * @brief  Destructor
          * @param  None
          */
        virtual ~CDevDisplay ();

        /**
          * @brief  Get screen width
          * @return Screen width (pixels)
          */
        virtual uint GetWidth (void) const = 0;

        /**
          * @brief  Get screen height
          * @return Screen height (pixels)
          */
        virtual uint GetHeight (void) const = 0;

        /**
          * @brief  Draw single pixel on the screen
          * @param  x: X coordinate of pixel's position
          * @param  y: Y coordinate of pixel's position
          * @param  color: Color of the pixel
          * @return 0 or negative error code
          */
        virtual int DrawPixel (uint x, uint y, TPixel color) = 0;

        /**
          * @brief  Draw rectangle with solod color
          * @param  x0: X coordinate of top-left corner
          * @param  y0: Y coordinate of top-left corner
          * @param  width: Rectangle width
          * @param  height: Rectangle height
          * @param  color: Color to fill the rectangle
          * @return 0 or negative error code
          */
        virtual int DrawRect (uint x0, uint y0, uint width, uint height, TPixel color);

        /**
         * @brief  Drawing callback
         * @param  x0: X coordinate of top-left corner
         * @param  y0: Y coordinate of top-left corner
         * @param  width: Area width
         * @param  height: Area height
         * @param  data: Pointer to pixel's data (size is width * height)
         * @param  cbParam: Callback function additional parameter
         */
        typedef void (TDrawCb) (uint x0, uint y0, uint width, uint height, const TPixel* data, void* param);

        /**
          * @brief  Draw area with specified pixels data
          * @param  x0: X coordinate of top-left corner
          * @param  y0: Y coordinate of top-left corner
          * @param  width: Area width
          * @param  height: Area height
          * @param  data: Pointer to pixel's data (size is width * height)
          * @param  endDrawCb: Callback function to notify about the end of data processing
          * @param  cbParam: Callback function additional parameter
          * @return 0 or negative error code
          */
        virtual int Draw (
                            uint x0, uint y0, uint width, uint height, const TPixel* data,
                            TDrawCb* endDrawCb = nullptr, void* cbParam = nullptr
        );
};
