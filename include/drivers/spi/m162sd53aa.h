//*****************************************************************************
//
// @brief   Driver for LCD display M162SD53AA (Futaba)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// CLcdM162sd53aa
// ----------------------------------------------------------------------------

class CLcdM162sd53aa : public CDevChar
{
    public:
        CLcdM162sd53aa (CDevChar* pSpi, TGpio pinCs);

        int Open (void);
        int Close (void);
        int Configure (const void* params);
        int Ioctl (uint code, void* arg = NULL);

        int PutChar (const uchar c);
        int GetChar (void);
        int Write (const void* buff, uint size);
        int Read (void* buff, uint size);

    private:
        CDevChar*   m_pSpi;
        TGpio       m_pinCs;
        char        m_addr;     // Current address in DDRAM

        void sendCmd (char cmd);
};
