//*****************************************************************************
//
// @brief   ENC28J60 driver (Ethernet)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/net.h"
#include "dev/char.h"
#include "kernel/kthread.h"
#include "kernel/kqueue.h"
#include "kernel/kmutex.h"
#include "lwip/netif.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        uint32  addr;
        uint32  mask;
        uint32  gw;
        char    mac[NETIF_MAX_HWADDR_LEN];
    } TEnc28j60Cfg;

    // Internal thread
    class CEnc28j60Reader;



// ----------------------------------------------------------------------------
// CDevEnc28j60
// ----------------------------------------------------------------------------

class CDevEnc28j60 : public CDevNet
{
    friend class CEnc28j60Reader;
    
    public:
        /**
          * @brief Constructor
          * @param pDev: IO interface (SPI)
          */
        CDevEnc28j60 (CDevChar* pDev);
        
        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (see TEnc28j60Cfg)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Advanced device control
          * @param  code: Command code
          * @param  arg: Pointer to command arguments
          * @return Operation result (see IOCTL_xx)
          */
        int Ioctl (uint code, void* arg = NULL);

        /**
          * @brief  Read device state
          * @param  None
          * @return Device state
          */
        TDevState GetState (void) const { return m_devState; }
        
        /**
          * @brief  Send network packet
          * @param  skb: Network paket descriptor
          * @return Processed data size or negative error code
          */
        int Write (const TSkb* skb);
        
        /**
          * @brief  Set address of the network device
          * @param  pAddr: Network address descriptor
          * @return NET_OK or negative error code
          */
        int SetAddr (const TNetAddr* pAddr);

        /**
          * @brief  Read address of the network device
          * @param  pAddr: Pointer to network address descriptor (to save address)
          */
        void GetAddr (TNetAddr* pAddr) const;
        
    private:
        CEnc28j60Reader m_reader;       // Thread for reading incoming data
        struct netif    m_netif;        // Network interface (lwip)
        CDevChar*       m_pSpi;         // IO interface
        KMutex          m_mutex;        // Mutex for locking external access
        char            m_revision;     // Chip revision
        char            m_bank;         // Chip active memory bank
        uint16          m_addrRx;       // Memory address of start of receiving packet
        TDevState       m_devState;     // Device state
        
        /**
          * @brief  Reset network packet descriptor
          * @param  skb: Network packet descriptor
          */
        void resetSkb (TSkb* skb);
        
        /**
          * @brief  Chip hardware initialization
          * @return DEV_OK or negative error code
          */
        int initHw (void);

        // Register-wise operations
        void writeReg (char addr, char value);
        char readReg (char addr);
        void writePhy (char addr, uint16 value);
        uint16 readPhy (char addr);
        void writeOpcode (char op, char addr, char value);
        void writeByte (char value);
        void setBank (char bank);
        void readBuffer (char* buff, uint size);
        int transmit (void);
        
        /**
          * @brief  Reset receive buffer
          * @param  None
          */
        void resetRxBuff (void);
        
        /**
          * @brief  Checking cable connection
          * @return true - cable is connected, otherwise - false
          */
        bool isLinked (void);
};
