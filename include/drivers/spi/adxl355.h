//*****************************************************************************
//
// @brief   Accelerometer ADXL355 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Axes
    typedef enum
    {
        ADXL_AXIS_X,
        ADXL_AXIS_Y,
        ADXL_AXIS_Z
    } TAdxlAxis;

    // Measurement range
    typedef enum
    {
        ADXL_RANGE_2 = 1,   // +/- 2g
        ADXL_RANGE_4,       // +/- 4g
        ADXL_RANGE_8        // +/- 8g
    } TAdxlRange;

    // Measurement frequency
    typedef enum
    {
        ADXL_ODR_4000 = 0,
        ADXL_ODR_2000,
        ADXL_ODR_1000,
        ADXL_ODR_500,
        ADXL_ODR_250,
        ADXL_ODR_125,
        ADXL_ODR_62_5,
        ADXL_ODR_31_25,
        ADXL_ODR_15_625,
        ADXL_ODR_7_813,
        ADXL_ODR_3_906
    } TAdxlOdr;

    // Configuration parameters
    typedef struct
    {
        TAdxlRange  range;
        TAdxlOdr    odr;
    } TAdxlCfg;


// ----------------------------------------------------------------------------
// CAdxl355
// ----------------------------------------------------------------------------

class CAdxl355 : public CDevice
{
    public:
        /**
          * @brief  Constructor
          * @param  pSpi: SPI device
          */
        CAdxl355 (CDevChar* pSpi, TGpio pinCs, TGpio pinDrdy);

        /**
          * @brief  Device opening
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Device closing
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set device configuration parameters
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Data reading
          * @param  axis: Axis
          * @param  pVal: Pointer to variable to save the value (g)
          * @return 0 or negative error code
          */
        int GetData (TAdxlAxis axis, float* pVal);

    private:
        CDevChar*   m_pSpi;
        TGpio       m_pinCs;
        TGpio       m_pinDrdy;
        TAdxlRange  m_range;

        /**
          * @brief  Register reading
          * @param  addr: Register address
          * @return Register value
          */
        char regRd (char addr);

        /**
          * @brief  Register writing
          * @param  addr: Register address
          * @param  value: Register value
          */
        void regWr (char addr, char value);
};
