//*****************************************************************************
//
// @brief   RS485 interface driver (network device)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/net.h"
#include "dev/char.h"
#include "kernel/kthread.h"
#include "arch/usart.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if !defined(RS485_READ_BUFF_SIZE)
        #define RS485_READ_BUFF_SIZE                    512
    #endif


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Device configuration parameters
    typedef struct
    {
        uint            devId;      // Device ID of used USART
        TUsartOptions   devCfg;     // USART device options
        uint            rdPeriod;   // Check incoming data period (ms)
        uint            quietTime;  // Minimum quiet time (ms)
    } TRs485Cfg;


    // Internal data reader thread
    class CDevRs485Reader;



// ----------------------------------------------------------------------------
// CDevRs485
// ----------------------------------------------------------------------------

class CDevRs485 : public CDevNet
{
    public:
        /**
          * @brief  Constructor
          * @param  family: Network protocol family supported by the device
          *         (see NET_FAMILY_xx in appropriate socket family header file)
          */
        CDevRs485 (uint family);
        
        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (see TRs485Cfg)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Send network packet through the device
          * @param  skb: Network packet descriptor
          * @return Processed data size or negative error code
          */
        int Write (const TSkb* skb);
        
    private:
        CDevRs485Reader m_reader;
        CDevChar*       m_pDev;
        TUsartOptions   m_devCfg;
        char            m_buffRx[RS485_READ_BUFF_SIZE];
        uint            m_quietTime;
        uint32          m_activeTime;
        
        /**
          * @brief  Reset network packet descriptor
          * @param  skb: Network packet descriptor
          */
        void resetSkb (TSkb* skb);

        /**
          * @brief  Incoming data processing
          * @param  None
          */
        void recv (void);
        
        friend CDevRs485Reader;
};
