//*****************************************************************************
//
// @brief   Input device (mouse + keyboard) driver based on SDL2 library (host)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/input.h"


// ----------------------------------------------------------------------------
// CDevInputSdl2
// ----------------------------------------------------------------------------

class CDevInputSdl2 : public CDevInput
{
    public:
        /**
         * @brief  Constructor
         * @param  devIndex: Device index
         */
        CDevInputSdl2 (uint devIndex);

        /**
         * @brief  Open device
         * @param  None
         * @return DEV_OK or negative error code
         */
        int Open (void);

        /**
         * @brief  Close device
         * @param  None
         * @return DEV_OK or negative error code
         */
        int Close (void);

        /**
         * @brief  Set configuration parameters of the device
         * @param  params: Configuration parameters
         * @return DEV_OK or negative error code
         */
        int Configure (const void* params);

    private:
        uint32 m_mouseButtons;
        uint16 m_x;
        uint16 m_y;

        void onMouseMove (const void* ev);
        void onMouseButton (const void* ev);
        void onKbdButton (const void* ev);

        friend class CSdlWorker;
};
