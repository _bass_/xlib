//*****************************************************************************
//
// @brief   Display device driver for virtual screen on a host
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/display.h"
#include "drivers/input/input_sdl2.h"


// ----------------------------------------------------------------------------
// CDevDisplaySdl2
// ----------------------------------------------------------------------------

class CDevDisplaySdl2 : public CDevDisplay
{
    public:
        /**
          * @brief  Constructor
          * @param  width: Screen width (pixels)
          * @param  height: Screen height (pixels)
          * @param  devInput: Input device to handle input events from the screen
          */
        CDevDisplaySdl2 (uint width, uint height, CDevInputSdl2* devInput = nullptr);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CDevDisplaySdl2 ();

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Set configuration parameters of the device (unused)
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Draw single pixel on the screen
          * @param  x: X coordinate of pixel's position
          * @param  y: Y coordinate of pixel's position
          * @param  color: Color of the pixel
          * @return 0 or negative error code
          */
        int DrawPixel (uint x, uint y, TPixel color);

        /**
          * @brief  Draw rectangle with solod color
          * @param  x0: X coordinate of top-left corner
          * @param  y0: Y coordinate of top-left corner
          * @param  width: Rectangle width
          * @param  height: Rectangle height
          * @param  color: Color to fill the rectangle
          * @return 0 or negative error code
          */
        int DrawRect (uint x0, uint y0, uint width, uint height, TPixel color);

        /**
          * @brief  Draw area with specified pixels data
          * @param  x0: X coordinate of top-left corner
          * @param  y0: Y coordinate of top-left corner
          * @param  width: Area width
          * @param  height: Area height
          * @param  data: Pointer to pixel's data (size is width * height)
          * @param  endDrawCb: Callback function to notify about the end of data processing
          * @param  cbParam: Callback function additional parameter
          * @return 0 or negative error code
          */
        int Draw (uint x0, uint y0, uint width, uint height, const TPixel* data, TDrawCb* endDrawCb, void* cbParam);

        /**
          * @brief  Get screen width
          * @return Screen width (pixels)
          */
        uint GetWidth (void) const;

        /**
          * @brief  Get screen height
          * @return Screen height (pixels)
          */
        uint GetHeight (void) const;

    private:
        /**
          * @brief  Checks if device was initialized (opened)
          * @return True for initialized device, otherwise - false
          */
        bool isInitialized (void);

        /**
          * @brief  Context (m_pContext) data deinitialization
          * @return None
          */
        void contextUninit (void);

        /**
          * @brief  Send update signal to screen drawing system
          * @return None
          */
        void updateScreen (void);

        struct TContext;
        TContext* m_pContext;       // SDL context data
        const uint  m_width;        // Screen width
        const uint  m_height;       // Screen height
        CDevInputSdl2* m_devInput;  // SDL input device driver

        friend class CSdlWorker;
};
