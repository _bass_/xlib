//*****************************************************************************
//
// @brief   Driver for SPI Flash W25Qxx
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/block.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Command codes
    #define W25Q_CMD_WRITE_ENABLE               0x06
    #define W25Q_CMD_WRITE_DISABLE              0x04

    #define W25Q_CMD_READ_STATUS_REG_1          0x05
    #define W25Q_CMD_READ_STATUS_REG_2          0x35
    #define W25Q_CMD_READ_STATUS_REG_3          0x15
    #define W25Q_CMD_WRITE_STATUS_REG_1         0x01
    #define W25Q_CMD_WRITE_STATUS_REG_2         0x31
    #define W25Q_CMD_WRITE_STATUS_REG_3         0x11

    #define W25Q_CMD_JEDEC_ID                   0x9F

    #define W25Q_CMD_READ                       0x03
    #define W25Q_CMD_PAGE_PGM                   0x02
    #define W25Q_CMD_SECTOR_E                   0x20
    #define W25Q_CMD_BLK_E_32K                  0x52
    #define W25Q_CMD_BLK_E_64K                  0xD8

    #define W25Q_CMD_PWR_DOWN                   0xB9
    #define W25Q_CMD_PWR_UP                     0xAB

    // Status register masks
    #define W25Q_STATUS_MASK_BUSY               0x01
    #define W25Q_STATUS_MASK_WEL                0x02


// ----------------------------------------------------------------------------
// CW25qxx
// ----------------------------------------------------------------------------

class CW25qxx : public CDevBlock
{
    public:
        // Configuration parameters
        typedef struct
        {
            TGpio   pinCs;
        } TCfg;

        // Internal chip descriptor
        typedef struct _TChipInfo TChipInfo;

        /**
          * @brief  Constructor
          * @param  pDev: Communication interface (SPI)
          */
        CW25qxx (CDevChar* pDev);
        
        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (see TCfg)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);
        
        /**
          * @brief  Read data from block device
          * @param  buff: Buffer for read data
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Read (void* buff, uint block, uint count = 1);

        /**
          * @brief  Write data to block device
          * @param  data: Pointer to data to write
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Write (const void* data, uint block, uint count = 1);

        /**
          * @brief  Erase block on block device
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Erase (uint block, uint count = 1);
        
        /**
          * @brief  Get block size
          * @return Block size (bytes)
          */
        uint BlockSize (void);

        /**
          * @brief  Get block size
          * @return Block size (bytes)
          */
        uint BlocksCount (void);
    
    private:
        CDevChar*   m_pDev;         // Communication interface (SPI)
        TGpio       m_pinCs;        // CS pin
        const TChipInfo* m_pDesc;   // Chip descriptor
        bool        m_writeActive;  // Flag of write process activation (to lock access to chip during writing)
        
        /**
          * @brief  Write access control
          * @param  state: Enable writing flag state (true - enabled)
          */
        void setWrite (bool state);

        /**
          * @brief  Waiting device ready state
          * @return Chip status register value
          */
        uint waitReady (void);
};
