//*****************************************************************************
//
// @brief   Block device driver with data in RAM
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/block.h"


// ----------------------------------------------------------------------------
// CBlockIram
// ----------------------------------------------------------------------------

class CBlockIram : public CDevBlock
{
    public:
        /**
          * @brief  Constructor
          * @param  blockSize: Block size
          * @param  count: Number of blocks
          */
        CBlockIram (uint blockSize, uint count);
        
        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Read data from block device
          * @param  buff: Buffer for read data
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Read (void* buff, uint sector, uint count = 1);

        /**
          * @brief  Write data to block device
          * @param  data: Pointer to data to write
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Write (const void* data, uint sector, uint count = 1);

        /**
          * @brief  Erase block on block device
          * @param  block: Block index
          * @param  count: Number of blocks
          * @return DEV_OK or negative error code
          */
        int Erase (uint block, uint count = 1);

        /**
          * @brief  Get block size
          * @return Block size (bytes)
          */
        uint BlockSize (void);

        /**
          * @brief  Get number of blocks on device
          * @return Numbers of blocks
          */
        uint BlocksCount (void);
    
    private:
        char*   m_buff;
        uint    m_blockSize;
        uint    m_blocksCount;
};
