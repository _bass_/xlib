//*****************************************************************************
//
// @brief   ATSHA204 driver (crypto-chip)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"
#include "kernel/kmutex.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define ATSHA204_I2C_DEFAULT_ADDR           0xc8

    // Packet buffer size (bytes)
    #define ATSHA204_BUFF_SIZE                  88

    // Memory zone sizes (bytes)
    #define ATSHA204_ZONE_SIZE_CONFIG           88
    #define ATSHA204_ZONE_SIZE_DATA             512
    #define ATSHA204_ZONE_SIZE_OTP              64

    // MAC generation modes
    #define ATSHA204_MAC_ADD_SN                 0x40
    #define ATSHA204_MAC_ADD_OTP_64             0x20
    #define ATSHA204_MAC_ADD_OTP_88             0x10
    #define ATSHA204_MAC_ADD_TEMPKEY_SRC_BIT    0x04
    #define ATSHA204_MAC_ADD_PART_1_DATA_SLOT   0x00
    #define ATSHA204_MAC_ADD_PART_1_TEMPKEY     0x02
    #define ATSHA204_MAC_ADD_PART_2_INPUT       0x00
    #define ATSHA204_MAC_ADD_PART_2_TEMPKEY     0x01

    // Lock state of data and configuration zones
    #define ATSHA204_ZONE_STATE_UNLOCKED        0x55
    #define ATSHA204_ZONE_STATE_LOCKED          0x00
    // OTP zone lock state
    #define ATSHA204_OTP_STATE_LOCKED           0x00
    #define ATSHA204_OTP_STATE_RO               0xaa
    #define ATSHA204_OTP_STATE_WR_ZERO          0x55



// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Memory zones
    typedef enum
    {
        ATSHA204_ZONE_CONFIG    = 0,
        ATSHA204_ZONE_OTP       = 1,
        ATSHA204_ZONE_DATA      = 2
    } ESha204Zone;

    // Slot access configuration
    typedef union
    {
        uint16      data;
        struct
        {
            uint16  readKey     : 4;
            uint16  checkOnly   : 1;
            uint16  singleUse   : 1;
            uint16  encryptRead : 1;
            uint16  isSecret    : 1;
            uint16  writeKey    : 4;
            uint16  writeConfig : 4;
        };
    } TSha204SlotCfg;


// ----------------------------------------------------------------------------
// CDrvSha204
// ----------------------------------------------------------------------------

class CDrvSha204 : public CDevice
{
    public:
        /**
          * @brief Constructor
          * @param pDev: IO interface (I2C)
          * @param addr: Chip address on the interface
          */
        CDrvSha204 (CDevChar* pDev, uint8 addr);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);
        
        /**
          * @brief  Random number generation
          * @param  buff: 32 bytes buffer to save the result
          * @return 0 or negative error code
          */
        int Rand (char* buff);

        /**
          * @brief  MAC calculation
          * @param  mode: MAC generation mode (see ATSHA204_MAC_ADD_xxx)
          * @param  slotId: Slot number for first part of the data (ATSHA204_MAC_ADD_PART_1_DATA_SLOT)
          * @param  buff: 32 bytes buffer to save the result
          * @param  pSalt: Slat (challenge) - second part of the data (32 bytes)
          * @return 0 or negative error code
          */
        int Mac (char mode, uint16 slotId, char* buff, const char* pSalt = NULL);

        /**
          * @brief  Reading data block (4 or 32 bytes)
          * @param  zone: Memory zone
          * @param  addr: Start address of read data
          * @param  buff: 32 or 4 bytes buffer to save the result
          * @param  size: Data size to read (4 or 32 bytes)
          * @return 0 or negative error code
          */
        int Read (ESha204Zone zone, uint16 addr, char* buff, uint size);

        /**
          * @brief  Writing data block (4 or 32 bytes)
          * @param  zone: Memory zone
          * @param  addr: Start address of write data
          * @param  data: 32 or 4 bytes data
          * @param  size: Data size (4 or 32 bytes)
          * @return 0 or negative error code
          */
        int Write (ESha204Zone zone, uint16 addr, const char* data, uint size);

        /**
          * @brief  Memory zone locking
          * @param  zone: Memory zone
          * @param  crc16: Data CRC16 value for integrity check or -1 to lock without checking
          * @return 0 or negative error code
          */
        int Lock (ESha204Zone zone, int crc16);
        
        /**
          * @brief  CRC value calculation
          * @param  data: Data
          * @param  size: Data size
          * @return 0 or negative error code
          */
        uint16 GetCrc (const char* data, uint size);

    private:
        CDevChar*   m_pDev;         // IO interface for chip communication (I2C)
        char        m_addr;         // Chip address on the interface
        KMutex      m_mutex;        // Mutex for external access locking
        char        m_buff[ATSHA204_BUFF_SIZE];
        
        /**
          * @brief  Send command to the chip
          * @param  cmd: Command code
          * @param  param1: First parameter of the command
          * @param  param2: Second parameter of the command
          * @param  data: Additional data for the command
          * @param  size: Data size
          * @return 0 or negative error code
          */
        int sendCmd (char cmd, char param1, uint16 param2, const char* data = NULL, uint size = 0);
        
        /**
          * @brief  Wakeup chip from sleep mode
          * @param  None
          * @return 0 or negative error code
          */
        int wakeup (void);

        /**
          * @brief  Switch chip to idle mode
          * @param  None
          * @return 0 or negative error code
          */
        int idle (void);

        /**
          * @brief  Switch chip to sleep mode
          * @param  None
          * @return 0 or negative error code
          */
        int sleep (void);
};
