//*****************************************************************************
//
// @brief   Pressure sensor BMP280 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Parameter IDs (channels)
    typedef enum
    {
        BMP280_CH_PRESS,    // Pressure
        BMP280_CH_TEMP      // Temperature
    } TBmp280Ch;


// ----------------------------------------------------------------------------
// CBmp280
// ----------------------------------------------------------------------------

class CBmp280 : public CDevice
{
    public:
        /**
          * @brief Constructor
          * @param pDev: IO interface (I2C)
          */
        CBmp280 (CDevChar* pDev);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (not used)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Read measurement values
          * @param  channel: Parameter ID (channel)
          * @param  pVal: Pointer to variable to save value
          * @return 0 or negative error code
          */
        int GetData (TBmp280Ch channel, float* pVal);

    private:
        CDevChar*   m_pDev;
        char        m_compData[24];
        sint32      m_fineT;
        float       m_cacheTemp;        // Cached temperature value
        uint32      m_cacheTime;        // Timestamp of read temperature value

        /**
          * @brief  Pressure value calculation with correction (more accurate)
          * @param  pRawData: Raw data from the chip
          * @return Pressure value
          */
        float getCompensatePress (const char* pRawData);

        /**
          * @brief  Temperature value calculation with correction (more accurate)
          * @param  pRawData: Raw data from the chip
          * @return Temperature value
          */
        float getCompensateTemp (const char* pRawData);
};
