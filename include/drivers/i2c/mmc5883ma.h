//*****************************************************************************
//
// @brief   Magnetometer MMC5883MA driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/char.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Parameter IDs (channels)
    typedef enum
    {
        MMC5883_CH_X,
        MMC5883_CH_Y,
        MMC5883_CH_Z,
        MMC5883_CH_TEMP
    } TMmc5883Ch;

    // Measurement frequency
    typedef enum
    {
        MMC5883_ODR_100 = 0,
        MMC5883_ODR_200,
        MMC5883_ODR_400,
        MMC5883_ODR_600
    } TMmc5883Odr;


// ----------------------------------------------------------------------------
// CMmc5883
// ----------------------------------------------------------------------------

class CMmc5883 : public CDevice
{
    public:
        /**
          * @brief Constructor
          * @param pDev: IO interface (I2C)
          * @param pinIrq: IRQ pin
          */
        CMmc5883 (CDevChar* pDev, TGpio pinIrq);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (not used)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Read measurement values
          * @param  channel: Parameter ID (channel)
          * @param  pVal: Pointer to variable to save value
          * @return 0 or negative error code
          */
        int GetData (TMmc5883Ch channel, float* pVal);

    private:
        CDevChar*   m_pDev;
        TGpio       m_pinIrq;
};
