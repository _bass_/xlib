//*****************************************************************************
//
// @brief   USB CDC device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/usbd.h"
#include "misc/fifo.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Used endpoints (number)
    #define CDC_EP_READ_NUM                     1
    #define CDC_EP_WRITE_NUM                    2
    #define CDC_EP_ACM_NUM                      3

    // Transfer buffer size for control endpoint (ACM)
    #define USB_DEV_EP_ACM_SIZE                 8

    // Polling interval for ACM endpoint (0..255)
    #define CDC_EP_ACM_INTERVAL                 250

    // CDC version 1.1 (BCD)
    #define CDC_1_1                             0x0110

    // Subtypes of CDC descriptors
    #define USB_CDC_SUBTYPE_HEADER              0x00
    #define USB_CDC_SUBTYPE_CALL_MANAGEMENT     0x01
    #define USB_CDC_SUBTYPE_ACM                 0x02
    #define USB_CDC_SUBTYPE_UNION               0x06

    #define USB_CDC_SUBCLASS_ACM                0x02
    #define USB_CDC_SUBCLASS_ETHERNET           0x06
    #define USB_CDC_SUBCLASS_WHCM               0x08
    #define USB_CDC_SUBCLASS_DMM                0x09
    #define USB_CDC_SUBCLASS_MDLM               0x0a
    #define USB_CDC_SUBCLASS_OBEX               0x0b
    #define USB_CDC_SUBCLASS_EEM                0x0c
    #define USB_CDC_SUBCLASS_NCM                0x0d
    #define USB_CDC_SUBCLASS_MBIM               0x0e

    // Common AT commands (protocol)
    #define USB_CDC_PROTO_V25                   0x01
    // Vendor-specific protocol code
    #define USB_CDC_PROTO_VENDOR_SPECIFIC       0xff

    // Class-specific request codes
    #define USB_CDC_REQ_SEND_ENCAPSULATED_COMMAND   0x00
    #define USB_CDC_REQ_GET_ENCAPSULATED_RESPONSE   0x01
    #define USB_CDC_REQ_SET_COMM_FEATURE            0x02
    #define USB_CDC_REQ_GET_COMM_FEATURE            0x03
    #define USB_CDC_REQ_CLEAR_COMM_FEATURE          0x04
    #define USB_CDC_REQ_SET_LINE_CODING             0x20
    #define USB_CDC_REQ_GET_LINE_CODING             0x21
    #define USB_CDC_REQ_SET_CONTROL_LINE_STATE      0x22
    #define USB_CDC_REQ_SEND_BREAK                  0x23

    // Serial port parameters
    #define USB_CDC_CFG_STOP_BITS_1                 0
    #define USB_CDC_CFG_STOP_BITS_1_5               1
    #define USB_CDC_CFG_STOP_BITS_2                 2
    #define USB_CDC_CFG_PARITY_NONE                 0
    #define USB_CDC_CFG_PARITY_ODD                  1
    #define USB_CDC_CFG_PARITY_EVEN                 2
    #define USB_CDC_CFG_PARITY_MARK                 3
    #define USB_CDC_CFG_PARITY_SPACE                4

    #define USB_CDC_DEFAULT_BAUDRATE                115200
    #define USB_CDC_DEFAULT_STOP_BITS               USB_CDC_CFG_STOP_BITS_1
    #define USB_CDC_DEFAULT_PARITY                  USB_CDC_CFG_PARITY_NONE
    #define USB_CDC_DEFAULT_DATA_BITS               8


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #pragma pack(1)

    // Functional Descriptor Header
    typedef struct
    {
        TUsbDescriptorHeader    header;                 // Standard descriptor header
        uchar                   bDescriptorSubtype;     // Descriptor subtype
        uint16                  bcdCDC;                 // CDC version (BCD format)
    } TCdcFuncDescHdr;

    // Call Management Functional Descriptor
    typedef struct
    {
        TUsbDescriptorHeader    header;                 // Standard descriptor header
        uchar                   bDescriptorSubtype;     // Descriptor subtype
        uchar                   bmCapabilities;         // Supported options (bitmap)
                                                        // D7..D2: RESERVED (Reset to zero)
                                                        // D1: 0 - Device sends/receives call management information only over the Communication Class interface.
                                                        // 1 - Device can send/receive call management information over a Data Class interface.
                                                        // D0: 0 - Device does not handle call management itself.
                                                        // 1 - Device handles call management itself.
                                                        // If D0 = 0, then D1 is ignored and reset to zero for future compatibility.
        uchar                   bDataInterface;         // Interface number of data class interface optionally used for call management
                                                        // Zero based index of the interface in this configuration.(bInterfaceNum)
    } TCdcFuncDescCallMgmt;

    // ACM Functional Descriptor
    typedef struct
    {
        TUsbDescriptorHeader    header;                 // Standard descriptor header
        uchar                   bDescriptorSubtype;     // Descriptor subtype
        uchar                   bmCapabilities;         // Supported options (bitmap. 0 - unsupported)
                                                        // D7..D4: RESERVED (Reset to zero)
                                                        // D3: supports notification Network_Connection.
                                                        // D2: supports request Send_Break
                                                        // D1: supports request combination of Set_Line_Coding, Set_Control_Line_State, Get_Line_Coding, and the notification Serial_State.
                                                        // D0: supports request combination of Set_Comm_Feature, Clear_Comm_Feature, and Get_Comm_Feature.
    } TCdcFuncDescAcm;

    // Union Functional Descriptor
    typedef struct
    {
        TUsbDescriptorHeader    header;                 // Standard descriptor header
        uchar                   bDescriptorSubtype;     // Descriptor subtype
        uchar                   bMasterInterface;       // Control interface index (bInterfaceNum)
        uchar                   bSlaveInterface0;       // Data interface index (bInterfaceNum)
    } TCdcFuncDescUnion;

    // Virtual COM-port parameters
    typedef struct
    {
        uint32      baudrate;
        uchar       format;
        uchar       parity;
        uchar       dataBits;
    } TCdcPortCfg;

    #pragma pack()


// ----------------------------------------------------------------------------
// CDevUsbCdc
// ----------------------------------------------------------------------------

class CDevUsbCdc : public CDevChar
{
    public:
        /**
          * @brief  Constructor
          * @param  pDev: Used USB-core device
          * @param  buffRx: Receive buffer (if NULL, will be allocated)
          * @param  sizeRx: Receive buffer size
          * @param  buffTx: Transmit buffer (if NULL, will be allocated)
          * @param  sizeTx: Transmit buffer size
          */
        CDevUsbCdc (CDevUsbd* pDev, void* buffRx = NULL, uint sizeRx = 0, void* buffTx = NULL, uint sizeTx = 0);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters (see TUsbdCdcOptions)
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Send character to output stream
          * @param  c: character
          * @return 0 or negative error code
          */
        int PutChar (const uchar c);

        /**
          * @brief  Get character from input stream
          * @return Character or negative error code
          */
        int GetChar (void);

        /**
          * @brief  Send data to output stream
          * @param  data: Data
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* buff, uint size);

        /**
          * @brief  Read data from input stream
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Written data size or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Class-specific requests handler
          * @param  request: Request descriptor
          * @param  arg: Additional arguments
          * @return DEV_OK or negative error code
          */
        static int OnRequest (const TUsbRequest* request, void* arg);

    private:
        CDevUsbd*   m_pDev;
        TFifo       m_buffRx;
        TFifo       m_buffTx;
        TFifo       m_buffTxAcm;
        uchar       m_dataAcm[USB_DEV_EP_ACM_SIZE];
        TCdcPortCfg m_portCfg;

        /**
          * @brief  Class-specific requests handler
          * @param  request: Request descriptor
          * @return DEV_OK or negative error code
          */
        int csRequestHandler (const TUsbRequest* request);

        /**
          * @brief  Send port state notification
          * @param  ifaceIndex: ACM interface number
          */
        void sendSerialState (uint ifaceIndex)
};
