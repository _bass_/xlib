//*****************************************************************************
//
// @brief   Simple buttons state processing
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "kernel/kthread.h"
#include "kernel/ktimer.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default button state fixing time (bounce protection, ms)
    #define BUTTON_DEFAULT_FIX_TIME         100


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration flags
    typedef struct
    {
        uchar       invert  : 1;    // State inversion
        uchar       irq     : 1;    // State is changed on IRQ (notification through OnStateChanged)
        uchar       reserved: 6;
    } TBtnFlags;

    // Button configuration parameters
    typedef struct
    {   
        const char* name;                       // Button name (for debug)
        bool        (*getHwState) (uint btnId); // Get button state handler
        TBtnFlags   flags;                      // Configuration flags
        uint16      timeFix;                    // State fixing time (ms)
        uint16      timeLongPress;              // Long press time (ms)
    } TButtonCfg;
    
    // Button state
    typedef enum
    {
        BTN_STATE_RELEASED = 0,
        BTN_STATE_PRESSED,
        BTN_STATE_LONGPRESS
    } TBtnState;

    // Internal button state edescriptor
    typedef struct _TButtonDesc TButtonDesc;


// ----------------------------------------------------------------------------
// CDrvButton
// ----------------------------------------------------------------------------

class CDrvButton : public KThreadWait
{
    public:
        /**
          * @brief  Constructor
          * @param  pCfg: Buttons configurations
          * @param  btnCount: Number of processing buttons 
          * @param  priority: Thread priority for buttons states processing
          */
        CDrvButton (const TButtonCfg* pCfg, uint btnCount, uint priority = THREAD_PRIORITY_DEFAULT);

        /**
          * @brief  Destructor
          * @param  None
          */
        ~CDrvButton ();
        
        /**
          * @brief  Read button current state
          * @param  btnId: ID of the button (index in list of descriptors)
          * @return Button state
          */
        static TBtnState GetState (uint btnId);

        /**
          * @brief  Button state changing handler (IRQ)
          * @param  btnId: ID of the button (index in list of descriptors)
          * @param  state: Button state (hardware)
          */
        static void OnStateChanged (uint btnId, bool state);
        
    private:
        static CDrvButton*  m_instance;
        const TButtonCfg*   m_pCfg;
        const uint          m_btnCount;
        TButtonDesc*        m_pDesc;
        
        // Lock access
        CDrvButton (const CDrvButton& obj);
        CDrvButton& operator= (const CDrvButton& rhs);
        
        /**
          * @brief  Thread initialization function
          * @param  None
          */
        void init (void);

        /**
          * @brief  Thread main function
          * @param  None
          */
        void run (void);

        /**
          * @brief  Button hardware state processing
          * @param  btnId: Button ID (index in descriptors table)
          * @param  hwState: Current hardware state
          */
        void processHwState (uint btnId, bool hwState);

        /**
          * @brief  Minimal inactivity time calculation
          * @return Delay value before next processing moment (ms)
          */
        uint getWaitTime (void);
};
