//*****************************************************************************
//
// @brief   GSM modem service driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem_port.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Modem service IDs
    typedef enum
    {
        E_MSERVICE_GPRS = 0,
        E_MSERVICE_SMS,
        E_MSERVICE_VOICE,
        E_MSERVICE_DATA,
        E_MSERVICE_USSD,

        E_MSERVICE_COUNT
    } TMServiceId;


// ----------------------------------------------------------------------------
// CMService
// ----------------------------------------------------------------------------

class CMService
{
    friend class CModem;
    friend class CModemPort;

    public:
        /**
          * @brief Constructor
          * @param id: Service ID
          * @param port: Used port
          */
        CMService (TMServiceId id, CModemPort* port) : m_id(id), m_pPort(port) {}
        virtual ~CMService () {};

        /**
          * @brief  Service initialization
          * @param  params: Service parameters
          * @return 0 or negative error code
          */
        virtual int Open (const void* params) = 0;

        /**
          * @brief  Service deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        virtual int Close (void) = 0;

        /**
          * @brief  Read data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Size of data written into the buffer or negative error code
          */
        virtual int Read (void* buff, uint size) = 0;

        /**
          * @brief  Write data
          * @param  data: Data for writing
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        virtual int Write (const void* data, uint size) = 0;

        /**
          * @brief  Extended command handling
          * @param  code: Command code
          * @param  arg: Command arguments
          * @return Processeing result (see IOCTL_NOT_xx)
          */
        virtual int Ioctl (uint code, void* arg = NULL)
        {
            UNUSED_VAR(code);
            UNUSED_VAR(arg);
            return IOCTL_NOT_SUPPORTED;
        }

    protected:
        TMServiceId     m_id;
        CModemPort*     m_pPort;

        /**
          * @brief  Incoming data processing
          * @param  data: Received data
          * @param  size: Data size
          * @return None
          */
        virtual void onDataReceive (const void* data, uint size)
        {
            UNUSED_VAR(data);
            UNUSED_VAR(size);
        }

        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        virtual int processUrc (const char* str)
        {
            UNUSED_VAR(str);
            return CMODEM_URC_RES_NONE;
        }
};
