//*****************************************************************************
//
// @brief   PDU base functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Phone number format
    typedef union
    {
        char    val;
        struct
        {
            char    set     : 4;        // Phone number format ID (Numbering-plan-identification, see TPduNumSet)
            char    type    : 3;        // Phone number type (Type-of-number, see TPduNumType)
            char    prefix  : 1;        // Always = 1
        };
    } TPduAddrType;
    
    // Phone number type (Type-of-number)
    typedef enum
    {
        E_PDU_NUMTYPE_UNKNOW        = 0,
        E_PDU_NUMTYPE_INTERNATIONAL = 1,
        E_PDU_NUMTYPE_NATIONAL      = 2,
        E_PDU_NUMTYPE_NETWORK       = 3,
        E_PDU_NUMTYPE_SUBCRIBER     = 4,
        E_PDU_NUMTYPE_ALPHA         = 5,
        E_PDU_NUMTYPE_ABBREV        = 6,
        E_PDU_NUMTYPE_RESERVED      = 7
    } TPduNumType;
    
    // Phone number format ID (Numbering-plan-identification). Only for UNKNOW, INTERNATIONAL and NATIONAL types.
    typedef enum
    {
        E_PDU_NUMSET_UNKNOW         = 0,
        E_PDU_NUMSET_ISDN           = 1,
        E_PDU_NUMSET_X121           = 3,
        E_PDU_NUMSET_TELEX          = 4,
        E_PDU_NUMSET_NATIONAL       = 8,
        E_PDU_NUMSET_PRIVATE        = 9,
        E_PDU_NUMSET_ERMES          = 10
    } TPduNumSet;


// ----------------------------------------------------------------------------
// PDU
// ----------------------------------------------------------------------------

class PDU
{
    public:
        /**
          * @brief  7-bit data decoding
          * @param  pdu: PDU data
          * @param  count: Number of packed symbols (7-bits symbols)
          * @param  buff: Buffer for decoded data. Size must be at least equal `count` bytes
          * @return 0 or negative error code
          */
        static int Decode7bit (const char* pdu, uint count, char* buff);

        /**
          * @brief  7-bit data encoding
          * @param  data: Data to encode
          * @param  size: Data size
          * @param  buff: Buffer for encoded data
          * @param  buffSize: Buffer size
          * @return Data size written into the buffer (encoded data) or negative error code
          */
        static int Encode7bit (const char* data, uint size, char* buff, uint buffSize);

        /**
          * @brief  Phone number in PDU format decoding
          * @param  pdu: PDU data
          * @param  pduSize: PDU data size
          * @param  buff: Buffer for decoded phone number (string)
          * @return 0 or negative error code
          */
        static int DecodePhone (const char* pdu, uint pduSize, char* buff);

        /**
          * @brief  Convert phone number to PDU format
          * @param  pdu: Start of PDU data block (starts with length)
          * @param  pduSize: PDU buffer size
          * @param  phone: Phone number
          * @return Size of written PDU data or negative error code
          */
        static int EncodePhone (char* pdu, uint pduSize, const char* phone);
};
