//*****************************************************************************
//
// @brief   GSM modem USSD service
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem_service.h"
#include "kernel/kthread.h"
#include "kernel/kqueue.h"


// ----------------------------------------------------------------------------
// CMServiceUssd
// ----------------------------------------------------------------------------

class CMServiceUssd : public CMService
{
    public:
        /**
          * @brief Constructor
          * @param port: Used port
          */
        CMServiceUssd (CModemPort* port);

        /**
          * @brief Destructor
          * @param None
          */
        ~CMServiceUssd ();
        
        /**
          * @brief  Service initialization
          * @param  params: Service parameters
          * @return 0 or negative error code
          */
        int Open (const void* params);

        /**
          * @brief  Service deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Size of data written into the buffer or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data
          * @param  data: Data for writing
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* data, uint size);
        
    private:
        KQueue  m_qResp;    // Queue of responses on sent USSD requests
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (const char* str);
        
        /**
          * @brief  USSD response decoding
          * @param  str: USSD response in PDU format
          * @param  dcs: Data Coding Scheme
          * @return 0 or negative error code
          */
        int decode (char* str, uint dcs);
};
