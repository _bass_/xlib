//*****************************************************************************
//
// @brief   GSM modem SMS service
// @author  Babkin Petr
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem_service.h"
#include "kernel/kthread.h"
#include "misc/time.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Data type
    typedef enum
    {
        SMS_DATA_TYPE_ASCII,    // Only ASCII symbols (0x00..0x7f)
        SMS_DATA_TYPE_8BIT,     // Single byte encoding (0x00..0xff)
        SMS_DATA_TYPE_UNICODE   // Unicode (1 symbol = 2 bytes)
    } TSmsDataType;

    // Additional information about incoming SMS
    typedef struct
    {
        char            phone[16];      // Sender phone number
        TTime           time;           // SMS-center timestamp
        int8            timeZone;       // Time zone (x0.25 hour)
        TSmsDataType    dataType;       // Data type
    } TSmsInfoIn;

    // Outgoing SMS parameters
    typedef struct
    {
        char            phone[16];      // Recipient phone number
        TSmsDataType    dataType;       // Data type
        char            validPeriod;    // SMS lifetime (2..30 days, 0 - prohibited)
    } TSmsInfoOut;

    // Configuration parameters
    typedef struct
    {
        void (*recvNotifyHdl) (void);
    } TSmsCfg;

    
// ----------------------------------------------------------------------------
// CMServiceSMS
// ----------------------------------------------------------------------------

class CMServiceSms : public CMService
{
    public:
        /**
          * @brief Constructor
          * @param port: Used port
          */
        CMServiceSms (CModemPort* port);

        /**
          * @brief Destructor
          * @param None
          */
        ~CMServiceSms ();
        
        /**
          * @brief  Service initialization
          * @param  params: Service parameters
          * @return 0 or negative error code
          */
        int Open (const void* params);

        /**
          * @brief  Service deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Size of data written into the buffer or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data
          * @param  data: Data for writing
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* data, uint size);

        /**
          * @brief  Extended command handling
          * @param  code: Command code
          * @param  arg: Command arguments
          * @return Processeing result (see IOCTL_NOT_xx)
          */
        int Ioctl (uint code, void* arg = NULL);
        
    private:
        const TSmsCfg*  m_pCfg;
        TSmsInfoIn*     m_optInfoInPtr;
        TSmsInfoOut*    m_optInfoOutPtr;
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (const char* str);
        
        /**
          * @brief  Reading index of first incoming SMS
          * @param  None
          * @return SMS index or negative error code
          */
        int smsGetIndex (void);

        /**
          * @brief  Reading SMS in PDU format
          * @param  smsIndex: SMS index
          * @param  buff: Buffer for PDU
          * @param  size: Buffer size
          * @return PDU data size written in buffer or negative error code
          */
        int smsReadPdu (uint smsIndex, char* buff, uint size);
};
