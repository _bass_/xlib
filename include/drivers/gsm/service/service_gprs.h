//*****************************************************************************
//
// @brief   Modem GPRS service driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem_service.h"
#include "lwip/netif.h"
#include "netif/ppp/ppp.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Set connection timeout (sec)
    #define CMODEM_GPRS_CONNECT_TIMEOUT                 30


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        const char* apn;
        const char* login;
        const char* password;
    } TGprsCfg;


// ----------------------------------------------------------------------------
// CMServiceGprs
// ----------------------------------------------------------------------------

class CMServiceGprs : public CMService
{
    public:
        /**
          * @brief Constructor
          * @param port: Used port
          */
        CMServiceGprs (CModemPort* port);
        
        /**
          * @brief  Service initialization
          * @param  params: Service parameters
          * @return 0 or negative error code
          */
        int Open (const void* params);

        /**
          * @brief  Service deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Size of data written into the buffer or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data
          * @param  data: Data for writing
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* data, uint size);

        /**
          * @brief  Extended command handling
          * @param  code: Command code
          * @param  arg: Command arguments
          * @return Processeing result (see IOCTL_NOT_xx)
          */
        int Ioctl (uint code, void* arg = NULL);
        
        /**
          * @brief  Incoming data processing
          * @param  data: Received data
          * @param  size: Data size
          * @return None
          */
        void onDataReceive (void* data, uint size);
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (const char* str);

        /**
          * @brief  PPP state changing handler
          * @param  code: Error code
          */
        void onStatusChanged (int code);
        
    private:
        struct netif    m_netif;                // Network interface (lwip)
        ppp_pcb*        m_pppDesc;              // PPP session descriptor
};
