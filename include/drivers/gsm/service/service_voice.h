//*****************************************************************************
//
// @brief   GSM modem voice calls service
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem_service.h"
#include "kernel/kthread.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // IOCTL codes
    #define IOCTL_VOICE_SEND_DTMF                   1
    #define IOCTL_VOICE_GET_PHONE                   2
    #define IOCTL_VOICE_GET_CALL_STATE              3
    #define IOCTL_VOICE_CALL_OUT                    4
    #define IOCTL_VOICE_CALL_ACCEPT                 5
    #define IOCTL_VOICE_CALL_TERMINATE              6
    #define IOCTL_VOICE_MODEM_DETECT_DTMF           100


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        void (*onCallIn) (CMService* pService);                     // Incomming call handler
        void (*onDetectDtmf) (CMService* pService, char code);      // Incomming DTMF codes handler
    } TVoiceCfg;

    // Call states
    typedef enum
    {
        E_VOICE_STATE_NOCALL = 0,           // No call
        E_VOICE_STATE_CALL_IN,              // Incoming call (before the call accepting)
        E_VOICE_STATE_CALL_OUT,             // Outgoing call (dialing)
        E_VOICE_STATE_ACTIVE_IN,            // Active incoming call (accepted)
        E_VOICE_STATE_ACTIVE_OUT            // Active outgoing call
    } TVoiceCallState;
    
    // Error codes
    typedef enum
    {
        E_VOICE_ERROR_NONE = 0,
        E_VOICE_ERROR_NO_DIALTONE,
        E_VOICE_ERROR_BUSY,
        E_VOICE_ERROR_NO_CARRIER,
        E_VOICE_ERROR_NO_ANSWER
    } TVoiceError;
    
    // Call state descriptor
    typedef struct
    {
        TVoiceCallState callState;
        TVoiceError     errCode;
    } TVoiceState;

    // Internal worker
    class CVoiceWorker;


// ----------------------------------------------------------------------------
// CMServiceVoice
// ----------------------------------------------------------------------------

class CMServiceVoice : public CMService
{
    friend class CVoiceWorker;
    
    public:
        /**
          * @brief Constructor
          * @param port: Used port
          */
        CMServiceVoice (CModemPort* port);

        /**
          * @brief Destructor
          * @param None
          */
        ~CMServiceVoice ();

        /**
          * @brief  Service initialization
          * @param  params: Service parameters
          * @return 0 or negative error code
          */
        int Open (const void* params);

        /**
          * @brief  Service deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        int Close (void);

        /**
          * @brief  Read data
          * @param  buff: Buffer for read data
          * @param  size: Buffer size
          * @return Size of data written into the buffer or negative error code
          */
        int Read (void* buff, uint size);

        /**
          * @brief  Write data
          * @param  data: Data for writing
          * @param  size: Data size
          * @return Processed data size or negative error code
          */
        int Write (const void* data, uint size);

        /**
          * @brief  Extended command handling
          * @param  code: Command code
          * @param  arg: Command arguments
          * @return Processeing result (see IOCTL_NOT_xx)
          */
        int Ioctl (uint code, void* arg = NULL);
        
    private:
        const TVoiceCfg*    m_pCfg;
        TVoiceState         m_state;
        char                m_phone[16];
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (const char* str);
        
        /**
          * @brief  Make outgoing call
          * @param  phone: Remote subscriber phone number
          * @return 0 or negative error code
          */
        int callOut (const char* phone);

        /**
          * @brief  Incoming call control
          * @param  isAccept: Action with a call (true - accept, false - hang up)
          * @return 0 or negative error code
          */
        int callCtrl (bool isAccept);
        
        /**
          * @brief  DTMF codes sending
          * @param  str: String of DTMF symbols
          * @return 0 or negative error code
          */
        int sendDtmf (const char* str);
        
        /**
          * @brief  Internal worker handler (incoming call processing)
          * @param  None
          */
        void workerHandler (void);
};
