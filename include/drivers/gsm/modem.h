//*****************************************************************************
//
// @brief   GSM modem base driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"
#include "dev/device.h"
#include "kernel/kthread.h"
#include "drivers/gsm/modem_port.h"
#include "drivers/gsm/modem_service.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // IO delay (ms)
    #if defined(CFG_CMODEM_IO_DELAY)
    #define CMODEM_IO_DELAY                     CFG_CMODEM_IO_DELAY
    #else
    #define CMODEM_IO_DELAY                     50
    #endif

    // Network registration timeout (sec)
    #define CMODEM_REG_TIMEOUT                  60

    // Error codes
    #define CMODEM_CME_ERR_NONE                 0
    #define CMODEM_CME_ERR_NOT_ALLOWED          3
    #define CMODEM_CME_ERR_NOT_SUPPORTED        4
    #define CMODEM_CME_ERR_SIM_NOT_INSERT       10
    #define CMODEM_CME_ERR_SIM_PIN_REQUIRED     11
    #define CMODEM_CME_ERR_SIM_PUK_REQUIRED     12
    #define CMODEM_CME_ERR_SIM_FAILURE          13
    #define CMODEM_CME_ERR_SIM_BUSY             14
    #define CMODEM_CME_ERR_SIM_WRONG            15
    #define CMODEM_CME_ERR_SIM_PIN2_REQUIRED    17
    #define CMODEM_CME_ERR_SIM_PUK2_REQUIRED    18

    // Network registration state codes
    #define CMODEM_REG_ERROR                    0
    #define CMODEM_REG_HOME                     1
    #define CMODEM_REG_SEARCH                   2
    #define CMODEM_REG_DENIED                   3
    #define CMODEM_REG_UNKNOWN                  4
    #define CMODEM_REG_ROAMING                  5

    // Debug module name
    #define CMODEM_DEBUG_NAME                  "Modem"

    // IMSI max length (number of digits)
    #define CMODEM_IMSI_MAX_LENGTH              15
    // IMEI length (number of digits)
    #define CMODEM_IMEI_LENGTH                  16


    // Service registration
    CC_SECTION_DECLARE(xLib_gsmModemServices)
    #define REGISTER_GSM_SERVICE(type, fn)      CC_VAR_USED_DECLARATION static const TMServiceDesc \
                                                gsmService_##type CC_VAR_SECTION(xLib_gsmModemServices) = {type, fn};
    
    // Modem state changing notification
    #define MODEM_NOTIFY_STATE(pModem, st)      do { \
                                                    TEvDevState params = {.pDev = pModem, .state = st}; \
                                                    Kernel::RiseEvent(EV_DEV_STATE_CHANGED, &params); \
                                                } while (0)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Modem configuration parameters
    typedef struct
    {
        const char* PIN;            // PIN-code
    } TModemCfg;

    // Modem service descriptor
    typedef struct
    {
        // Service type
        TMServiceId type;                                           
        // Service object creation function
        CMService*  (*creator) (TMServiceId id, CModemPort* const* portList, uint listSize);
    } TMServiceDesc;
    

    #pragma pack(1)

    // Modem diagnostics data
    typedef struct
    {
        char    regCode;            // Registration code
        char    rssi;               // Signal level
        char    imei[16];           // IMEI (string)
        char    imsi[16];           // IMSI (string)
        char    info[64];           // Modem information (string)
    } TModemDiag;
    
    #pragma pack()
    
    // Modem state codes (for EV_DEV_STATE_CHANGED)
    typedef enum
    {
        // TDevState
        MODEM_STATE_UNKNOWN = 0,
        MODEM_STATE_CLOSED,
        MODEM_STATE_OPENING,
        MODEM_STATE_OPENED,
        MODEM_STATE_CLOSING,
        // Extended state
        MODEM_STATE_WAIT_REG = 100
    } TModemState;


// ----------------------------------------------------------------------------
// CModem
// ----------------------------------------------------------------------------

class CModem : public CDevice, public KThread
{
    friend class CModemPort;
    
    public:
        /**
          * @brief  Constructor
          * @param  portList: Modem ports list 
          * @param  listSize: Number of ports
          */
        CModem (CModemPort* const* portList, uint listSize);
        
        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        virtual int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        virtual int Close (void);

        /**
          * @brief  Set configuration parameters of the device
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        virtual int Configure (const void* params);

        /**
          * @brief  Advanced device control
          * @param  code: Command code (see IOCTL_MODEM_xxx)
          * @param  arg: Pointer to command arguments
          * @return Operation result (see IOCTL_xx)
          */
        virtual int Ioctl (uint code, void* arg = NULL);
        
        /**
          * @brief  Get service object
          * @param  id: Service ID
          * @return Pointer to service object or NULL on error
          */
        CMService* GetService (TMServiceId id);

        /**
          * @brief  Service removing
          * @param  id: Service ID
          */
        void RemoveService (TMServiceId id);
        
        /**
          * @brief  Debug commands handler (kdebug)
          * @param  arg: Command arguments list
          * @param  argCount: Number of arguments
          */
        static void debugCmdHandler (char** arg, int argCount);
        
    protected:
        CModemPort* const*  m_portList;                         // Port list
        uint                m_portCount;                        // Number of ports in the list
        CMService*          m_serviceList[E_MSERVICE_COUNT];    // Modem services
        const char*         m_PIN;                              // PIN-code
        char                m_regCode;                          // Network registration code (see CMODEM_REG_xxx)
        
        /**
          * @brief  Thread main function
          * @param  None
          */
        void run (void);
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: Received URC string
          * @return Processing result (see CMODEM_URC_RES_xxx)
          */
        virtual int processUrc (const char* str);
        
        /**
          * @brief  Entering PIN-code
          * @param  code: PIN value
          * @return DEV_OK or negative error code
          */
        int enterPIN (const char* pin);

        /**
          * @brief  Waiting network registration
          * @return DEV_OK or negative error code
          */
        int waitReg (void);
        
        // Ioctl commands handlers
        int ioctlGetImei (char* buff);
        int ioctlGetImsi (char* buff);
        int ioctlGetCsq (int* pValue);
        int ioctlGetSimState (char* pError);
        int ioctlDoRestart (void);
};
