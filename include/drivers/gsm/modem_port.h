//*****************************************************************************
//
// @brief   GSM modem port driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/char.h"
#include "kernel/kqueue.h"
#include "kernel/kmutex.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // AT-command send timeout (ms)
    #define CMODEM_AT_TIMEOUT                   4000

    // Minimal size of valid AT-command
    #define CMODEM_AT_MIN_SIZE                  (sizeof("OK") - 1)

    // URC processing result codes
    #define CMODEM_URC_RES_NONE                 0
    #define CMODEM_URC_RES_OK                   1
    #define CMODEM_URC_RES_WARN                 (-1)
    #define CMODEM_URC_RES_ERR                  (-2)

    // Lock source IDs
    #define MPORT_SERVICE_ID_NONE               ( (uint)-1 )
    #define MPORT_SERVICE_ID_INTERNAL           ( (uint)-2 )
    #define MPORT_SERVICE_ID_IOCTL              ( (uint)-3 )

    // Parser timeout (ms)
    #define MPORT_PARSER_TIMEOUT                300
    // Adding to AT-commands queue timeout (ms)
    #define MPORT_AT_ADD_TIMEOUT                200


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Port working mode
    typedef enum
    {
        E_MODEM_PORT_MODE_CMD,
        E_MODEM_PORT_MODE_DATA
    } TMPortMode;

    // Modem driver interface declaration
    class CModem;
    
    // Port configuration parameters
    typedef struct
    {
        void*       devConfig;          // IO interface (USART) configuration parameters
        uint        buffSize;           // incoming AT-commands buffer size
        uint        qLen;               // AT-command queue size (number of commands)
        uint        modemDevNum;        // Modem device number
    } TMPortCfg;


// ----------------------------------------------------------------------------
// CModemPort
// ----------------------------------------------------------------------------

class CModemPort
{
    public:
        /**
          * @brief  Constructor
          * @param  pDev: IO interface (USART)
          * @param  pCfg: Port configuration parameters
          */
        CModemPort (CDevChar* pDev, const TMPortCfg* pCfg);

        /**
          * @brief  Open port
          * @param  None
          * @return None
          */
        void Open (void);

        /**
          * @brief  Close port
          * @param  None
          * @return None
          */
        void Close (void);

        /**
          * @brief  Read port state
          * @param  None
          * @return Current port state
          */
        TDevState GetState (void);

        /**
          * @brief  Read and processing incoming from IO interface data
          * @param  None
          * @return None
          */
        void Receive (void);

        /**
          * @brief  AT-command sending
          * @param  str: AT-command
          * @param  waitStr: Expected successfull response
          * @param  timeout: Answer receive timeout (ms)
          * @return 0 or negative error code
          */
        int SendAT (const char* str, const char* waitStr = "OK", uint timeout = CMODEM_AT_TIMEOUT);

        /**
          * @brief  AT-command sending
          * @param  str: AT-command
          * @param  waitStr: Expected successfull response
          * @param  buff: Buffer for saving incoming data (string) up to waitStr receiving
          * @param  size: Buffer size
          * @param  timeout: Answer receive timeout (ms)
          * @return 0 or negative error code
          */
        int SendAT (const char* str, const char* waitStr, char* buff, uint size, uint timeout = CMODEM_AT_TIMEOUT);

        /**
          * @brief  AT-command reading
          * @param  timeout: Reading timeout (ms)
          * @return Pointer to AT-command string or NULL
          */
        const char* ReadAT (uint timeout = CMODEM_AT_TIMEOUT);

        /**
          * @brief  Writing data
          * @param  data: Pointer to the data
          * @param  size: Data size
          * @return Transfered data size or negative error code
          */
        int Write (const void* data, uint size);

        /**
          * @brief  Lock port
          * @param  id: Locker ID (see MPORT_SERVICE_ID_xx)
          */
        void Lock (uint id);

        /**
          * @brief  Unlock port
          * @param  id: Unlocker ID (see MPORT_SERVICE_ID_xx)
          */
        void Unlock (uint id);

        /**
          * @brief  Switch port mode to "command"
          * @return true - on success otherwise - false
          */
        bool ToCmd (void);

        /**
          * @brief  Switch port mode to "data"
          * @return true - on success otherwise - false
          */
        bool ToData (void);

        /**
          * @brief  Set activated service ID
          * @param  id: Active service ID (see TMServiceId)
          */
        void SetService (uint id) { m_serviceId = id; }

        /**
          * @brief  Get pointer to modem object
          * @param  None
          * @return Pointer to modem object
          */
        CModem* GetModem (void) { return m_pModem; }

        /**
          * @brief  Get current error code
          * @param  None
          * @return Error code (see CMODEM_CME_ERR_xx)
          */
        uint GetError (void);
        
    private:
        const TMPortCfg* m_pCfg;                // Port configuration parameters
        CDevChar*       m_pDev;                 // IO device (USART)
        CModem*         m_pModem;               // Modem object
        KQueue          m_qAt;                  // Queue of incoming AT-commands
        KMutex          m_mutex;                // Mutex for licking external access to the port
        TMPortMode      m_mode;                 // Current port working mode
        char*           m_buff;                 // AT-command buffer
        char*           m_pAt;                  // Pointer to strat of AT-command (parser)
        uint            m_countRx;              // Counter of received data (parser)
        uint32          m_parserTime;           // Timestamp of last received symbol (parser)
        uint            m_lockId;               // ID of locker (service)
        uint            m_serviceId;            // ID of active modem service
        uint            m_error;                // Error code (CME ERROR)

        /**
          * @brief  Incoming AT-command processing
          * @param  None
          * @return None
          */
        void processAT (void);

        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (const char* str);

        /**
          * @brief  Adding AT-command to queue
          * @param  str: AT-command string
          */
        void addAT (const char* str);

        /**
          * @brief  AT-commands queue reset
          * @param  None
          */
        void flushAT (void);

        /**
          * @brief  Send data through IO interface (with timeout control)
          * @param  data: Data to send
          * @param  size: Data size
          * @return 0 or negative error code
          */
        int doWrite (const void* data, uint size);
};
