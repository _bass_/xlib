//*****************************************************************************
//
// @brief   GSM modem Cinterion BGS1/BGS2 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "drivers/gsm/modem.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Configuration parameters
    typedef struct
    {
        TModemCfg   baseCfg;                    // Base modem configuration
        void        (*initGpio) (void);         // Modem GPIO initialization handler
        void        (*setPwr) (bool state);     // Modem power control handler
        void        (*setPwrkey) (bool state);  // Power key input state changing handler
        void        (*setReset) (bool state);   // Reset line state changing handler
        void        (*setRts_0) (bool state);   // RTS of port 0 line state changing handler
        void        (*setRts_1) (bool state);   // RTS of port 1 line state changing handler
    } TBgsCfg;


// ----------------------------------------------------------------------------
// CGsmCinterionBgsx
// ----------------------------------------------------------------------------

class CGsmCinterionBgsx : public CModem
{
    public:
        /**
          * @brief Constructor
          * @param portList: Modem port list
          * @param listSize: Number of ports in the list
          */
        CGsmCinterionBgsx (CModemPort* const* portList, uint listSize);

        /**
          * @brief  Set configuration parameters
          * @param  params: Configuration parameters
          * @return DEV_OK or negative error code
          */
        int Configure (const void* params);

        /**
          * @brief  Open device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Open (void);

        /**
          * @brief  Close device
          * @param  None
          * @return DEV_OK or negative error code
          */
        int Close (void);

        /**
          * @brief  Advanced device control
          * @param  code: Command code
          * @param  arg: Pointer to command arguments
          * @return Operation result (see IOCTL_xx)
          */
        int Ioctl (uint code, void* arg = NULL);

        /**
          * @brief  Read device state
          * @param  None
          * @return Device state
          */
        TDevState GetState (void) const { return m_devState; }

    private:
        const TBgsCfg*  m_pCfg;         // Configuration parameters
        uint32          m_timeWakeup;   // Wakeup timestamp
        TDevState       m_devState;     // Device state
        
        /**
          * @brief  Asynchronous URC processing
          * @param  str: URC string
          * @return Processing result (see CMODEM_URC_RES_xx)
          */
        int processUrc (char* str);

        /**
          * @brief  Basic modem and ports initialization
          * @param  None
          * @return 0 or negative error code
          */
        int softInit (void);

        /**
          * @brief  IOCTL_MODEM_RESTART command handler
          * @return Operation result (see IOCTL_xx)
          */
        int ioctlDoRestart (void);

        /**
          * @brief  IOCTL_MODEM_PORT_ACTIVATE and IOCTL_MODEM_PORT_DEACTIVATE commands handler
          * @param  port: Pointer to port object (CModemPort)
          * @param  state: RTS pin logical state (see STATE_xx)
          * @return Operation result (see IOCTL_xx)
          */
        int ioctlCtrlPort (void* port, bool state);
};
