//*****************************************************************************
//
// @brief   NMEA protocol tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "parsers/nmea.h"
#include <new>
#include <string.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(nmea);

    CNmea* g_pObj = NULL;

    static const char strGga[] = "$GPGGA,004241.47,5532.8492,N,03729.0987,E,1,04,2.0,-0015,M,-1.25,M,0.5,1023*62\r\n";


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(nmea)
{
    RUN_TEST_CASE(nmea, parsePartial);
    RUN_TEST_CASE(nmea, parseIncomplete);
    RUN_TEST_CASE(nmea, parseWithEmptyFields);
    RUN_TEST_CASE(nmea, validReset);
    RUN_TEST_CASE(nmea, GGA);
    RUN_TEST_CASE(nmea, GLL);
    RUN_TEST_CASE(nmea, GSA);
    RUN_TEST_CASE(nmea, GSV);
    RUN_TEST_CASE(nmea, RMC);
    RUN_TEST_CASE(nmea, VTG_KMH);
    RUN_TEST_CASE(nmea, VTG_KNOTS);
    RUN_TEST_CASE(nmea, ZDA);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(nmea)
{
    g_pObj = new CNmea();
    CHECK(g_pObj);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(nmea)
{
    if (g_pObj)
    {
        delete g_pObj;
        g_pObj = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, parsePartial)
{
    const char* str = strGga;
    const uint strLen = strlen(str);
    const uint partSize = strLen / 8;

    for (uint i = 0; i < strLen; i += partSize)
    {
        CHECK( !g_pObj->IsValid(CNmea::GGA) );

        uint size = (strLen - i);
        if (size > partSize) size = partSize;

        g_pObj->Parse(str + i, size);
    }

    CHECK( g_pObj->IsValid(CNmea::GGA) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, parseIncomplete)
{
    const char* str = strGga;
    const uint strLen = strlen(str);

    CHECK( !g_pObj->IsValid(CNmea::GGA) );

    g_pObj->Parse(str + strLen / 2, strLen - strLen / 2);
    CHECK( !g_pObj->IsValid(CNmea::GGA) );

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::GGA) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, parseWithEmptyFields)
{
    const char* str = "$GPGGA,004241.47,,,,,,,,,,,,,*78\r\n";
    const uint strLen = strlen(str);

    CHECK( !g_pObj->IsValid(CNmea::GGA) );
    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::GGA) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, validReset)
{
    const uint strLen = strlen(strGga);

    CHECK( !g_pObj->IsValid(CNmea::GGA) );

    g_pObj->Parse(strGga, strLen);
    CHECK( g_pObj->IsValid(CNmea::GGA) );
    CHECK( g_pObj->IsValid(CNmea::GGA) );

    g_pObj->Invalidate(CNmea::GGA);
    CHECK( !g_pObj->IsValid(CNmea::GGA) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, GGA)
{
    const uint strLen = strlen(strGga);

    g_pObj->Parse(strGga, strLen);
    CHECK( g_pObj->IsValid(CNmea::GGA) );

    const CNmea::TMsgGga* pData = (const CNmea::TMsgGga*) g_pObj->GetMsg(CNmea::GGA);
    CHECK(pData);

    LONGS_EQUAL(0, pData->time.hour);
    LONGS_EQUAL(42, pData->time.min);
    LONGS_EQUAL(41, pData->time.sec);
    LONGS_EQUAL(470, pData->time.ms);

    FLOAT_EQUAL(55.547487, pData->coord.latitude, 0.000001);
    FLOAT_EQUAL(37.484978, pData->coord.longitude, 0.000001);

    CHECK(pData->quality == 1);
    CHECK(pData->satInView == 4);

    FLOAT_EQUAL(2.0, pData->hdop, 0.01);
    FLOAT_EQUAL(-15, pData->antAltitude, 0.01);
    FLOAT_EQUAL(-1.25, pData->geoSeparation, 0.01);
    FLOAT_EQUAL(0.5, pData->ageDiff, 0.01);

    LONGS_EQUAL(1023, pData->stationId);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, GLL)
{
    static const char str[] = "$GPGLL,5532.8492,N,03729.0987,E,004241.469,A*33\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::GLL) );

    const CNmea::TMsgGll* pData = (const CNmea::TMsgGll*) g_pObj->GetMsg(CNmea::GLL);
    CHECK(pData);

    FLOAT_EQUAL(55.547487, pData->coord.latitude, 0.000001);
    FLOAT_EQUAL(37.484978, pData->coord.longitude, 0.000001);

    LONGS_EQUAL(0, pData->time.hour);
    LONGS_EQUAL(42, pData->time.min);
    LONGS_EQUAL(41, pData->time.sec);
    LONGS_EQUAL(469, pData->time.ms);

    CHECK(pData->valid);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, GSA)
{
    static const char str[] = "$GPGSA,A,3,01,02,03,04,,,,,,,,,2.0,2.0,2.0*34\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::GSA) );

    const CNmea::TMsgGsa* pData = (const CNmea::TMsgGsa*) g_pObj->GetMsg(CNmea::GSA);
    CHECK(pData);

    CHECK(pData->modeSelection == 'A');

    LONGS_EQUAL(3, pData->mode);

    LONGS_EQUAL(1, pData->satId[0]);
    LONGS_EQUAL(2, pData->satId[1]);
    LONGS_EQUAL(3, pData->satId[2]);
    LONGS_EQUAL(4, pData->satId[3]);
    LONGS_EQUAL(0, pData->satId[4]);
    LONGS_EQUAL(0, pData->satId[11]);

    FLOAT_EQUAL(2.0, pData->pdop, 0.01);
    FLOAT_EQUAL(2.0, pData->hdop, 0.01);
    FLOAT_EQUAL(2.0, pData->vdop, 0.01);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, GSV)
{
    static const char str[] = "$GPGSV,3,1,12,02,86,172,99,09,62,237,,22,39,109,,27,37,301,*7A\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::GSV) );

    const CNmea::TMsgGsv* pData = (const CNmea::TMsgGsv*) g_pObj->GetMsg(CNmea::GSV);
    CHECK(pData);

    LONGS_EQUAL(3, pData->msgCount);
    LONGS_EQUAL(1, pData->msgNum);
    LONGS_EQUAL(12, pData->satCount);

    LONGS_EQUAL(2, pData->satelits[0].id);
    LONGS_EQUAL(86, pData->satelits[0].elevation);
    LONGS_EQUAL(172, pData->satelits[0].azimuth);
    LONGS_EQUAL(99, pData->satelits[0].snr);

    LONGS_EQUAL(9, pData->satelits[1].id);
    LONGS_EQUAL(62, pData->satelits[1].elevation);
    LONGS_EQUAL(237, pData->satelits[1].azimuth);
    LONGS_EQUAL(0, pData->satelits[1].snr);

    LONGS_EQUAL(22, pData->satelits[2].id);
    LONGS_EQUAL(39, pData->satelits[2].elevation);
    LONGS_EQUAL(109, pData->satelits[2].azimuth);
    LONGS_EQUAL(0, pData->satelits[2].snr);

    LONGS_EQUAL(27, pData->satelits[3].id);
    LONGS_EQUAL(37, pData->satelits[3].elevation);
    LONGS_EQUAL(301, pData->satelits[3].azimuth);
    LONGS_EQUAL(0, pData->satelits[3].snr);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, RMC)
{
    static const char str[] = "$GPRMC,225446,A,4916.45,N,12311.12,W,000.5,054.7,191194,020.3,E*68\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::RMC) );

    const CNmea::TMsgRmc* pData = (const CNmea::TMsgRmc*) g_pObj->GetMsg(CNmea::RMC);
    CHECK(pData);

    LONGS_EQUAL(22, pData->time.hour);
    LONGS_EQUAL(54, pData->time.min);
    LONGS_EQUAL(46, pData->time.sec);
    LONGS_EQUAL(0, pData->time.ms);

    LONGS_EQUAL(19, pData->date.day);
    LONGS_EQUAL(11, pData->date.month);
    LONGS_EQUAL(1994, pData->date.year);

    FLOAT_EQUAL(49.274167, pData->coord.latitude, 0.000001);
    FLOAT_EQUAL(-123.185333, pData->coord.longitude, 0.000001);

    FLOAT_EQUAL(0.926, pData->speed, 0.0001);
    FLOAT_EQUAL(54.7, pData->direction, 0.01);
    FLOAT_EQUAL(-20.3, pData->declination, 0.01);

    CHECK(pData->valid);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, VTG_KMH)
{
    static const char str[] = "$GPVTG,360.0,T,348.7,M,000.0,N,123.4,K*47\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::VTG) );

    const CNmea::TMsgVtg* pData = (const CNmea::TMsgVtg*) g_pObj->GetMsg(CNmea::VTG);
    CHECK(pData);

    FLOAT_EQUAL(360, pData->directionPole, 0.01);
    FLOAT_EQUAL(348.7, pData->directionMag, 0.01);
    FLOAT_EQUAL(123.4, pData->speed, 0.01);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, VTG_KNOTS)
{
    static const char str[] = "$GPVTG,360.0,T,348.7,M,12.3,N,000.0,K*73\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::VTG) );

    const CNmea::TMsgVtg* pData = (const CNmea::TMsgVtg*) g_pObj->GetMsg(CNmea::VTG);
    CHECK(pData);

    FLOAT_EQUAL(360, pData->directionPole, 0.01);
    FLOAT_EQUAL(348.7, pData->directionMag, 0.01);
    FLOAT_EQUAL(22.78, pData->speed, 0.001);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(nmea, ZDA)
{
    static const char str[] = "$GPZDA,201530.00,04,07,2002,00,00*60\r\n";
    const uint strLen = strlen(str);

    g_pObj->Parse(str, strLen);
    CHECK( g_pObj->IsValid(CNmea::ZDA) );

    const CNmea::TMsgZda* pData = (const CNmea::TMsgZda*) g_pObj->GetMsg(CNmea::ZDA);
    CHECK(pData);

    LONGS_EQUAL(20, pData->time.hour);
    LONGS_EQUAL(15, pData->time.min);
    LONGS_EQUAL(30, pData->time.sec);
    LONGS_EQUAL(0, pData->time.ms);

    LONGS_EQUAL(4, pData->date.day);
    LONGS_EQUAL(7, pData->date.month);
    LONGS_EQUAL(2002, pData->date.year);

    LONGS_EQUAL(0, pData->tzHours);
    LONGS_EQUAL(0, pData->tzMin);
}
