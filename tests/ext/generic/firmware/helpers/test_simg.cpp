//*****************************************************************************
//
// @brief   SIMG image helper tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "firmware/helpers/helper_simg.h"
#include "crypt/aes_cbc.h"
#include "crypt/sha256.h"
#include "misc/macros.h"
#include <string.h>
#include <new>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define SIMG_IMG_ID         5


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(ImgHelperSimg);

    // Testing object
    static IImgHelper* g_pObj = NULL;

    static const char g_dataDecoded[16] = "test image data";
    static const char g_dataEncoded[16] = {0x28,0xa2,0xa9,0xba,0x95,0x7d,0x24,0xb0,0xdc,0xf6,0xa5,0x40,0x45,0xd0,0x29,0x2b};
    static const char g_dataKey[AES_128_KEY_SIZE] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};

    static CImgHelperSimg::TMetadata g_meta =
    {
        SIMG_META_MAGIC,
        0,
        SIMG_IMG_ID,
        {},
        {
            1,  // CRYPT_TYPE_AES_128_CBC
            0,  // TAG_TYPE_SHA256
            {},
            sizeof(g_dataEncoded),
            sizeof(g_dataDecoded),
            1,
            {
                {0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f}
            },
            {0x30,0x61,0x45,0xb5,0x0b,0x50,0x2c,0x8d,0x6b,0xe6,0xe9,0x9b,0x8c,0xf5,0xa8,0x44,0xf7,0xc6,0xe7,0xad,0xc9,0x99,0x2b,0x74,0xe7,0x4c,0x83,0xab,0x35,0x0c,0xac,0x11},
            {0xe1,0x0e,0x14,0xc1,0x84,0x0d,0x5b,0x9f,0x3b,0x11,0x98,0x00,0xf6,0x27,0x30,0x75,0xe4,0x2c,0x8d,0x40,0xcd,0x3a,0x08,0x11,0xa7,0x2b,0x56,0x2d,0x73,0x1b,0x31,0x5c}
        },
        {
            0,  // SIGN_TYPE_SHA256_AES128_CBC
            {},
            {
                {0x10,0x0f,0x0e,0x0d,0x0c,0x0b,0x0a,0x09,0x08,0x07,0x06,0x05,0x04,0x03,0x02,0x01}
            }
        }
    };



/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(ImgHelperSimg)
{
    RUN_TEST_CASE(ImgHelperSimg, metaSize);
    RUN_TEST_CASE(ImgHelperSimg, metaParserInvalidMagic);
    RUN_TEST_CASE(ImgHelperSimg, metaParserInvalidSignType);
    RUN_TEST_CASE(ImgHelperSimg, metaParserInvalidSign);
    RUN_TEST_CASE(ImgHelperSimg, metaParserInvalidDataCryptType);
    RUN_TEST_CASE(ImgHelperSimg, metaParserInvalidDataTagType);
    RUN_TEST_CASE(ImgHelperSimg, metaImageSize);
    RUN_TEST_CASE(ImgHelperSimg, metaDataSize);
    RUN_TEST_CASE(ImgHelperSimg, metaVersion);
    RUN_TEST_CASE(ImgHelperSimg, metaOffset);
    RUN_TEST_CASE(ImgHelperSimg, dataOffset);
    RUN_TEST_CASE(ImgHelperSimg, imgId);
    RUN_TEST_CASE(ImgHelperSimg, imgParserValidData);
    RUN_TEST_CASE(ImgHelperSimg, imgParserInvalidData);
    RUN_TEST_CASE(ImgHelperSimg, checkValidData);
    RUN_TEST_CASE(ImgHelperSimg, checkInvalidData);
    RUN_TEST_CASE(ImgHelperSimg, imgDecode);
}


/**
  * @brief  Read encryption key
  * @param  buff: Buffer for the key
  * @param  size: Buffer size
  * @param  param: Additional parameters
  * @return Size of data written in the buffer or negative error code
  */
static int getKey (void* buff, uint size, void* param)
{
    UNUSED_VAR(param);

    if ( size < sizeof(g_dataKey) ) return -1;

    memcpy( buff, g_dataKey, sizeof(g_dataKey) );
    return sizeof(g_dataKey);
}


/**
  * @brief  Fill signature field in metadata
  * @param  pMeta: Pointer to metadata
  */
static void signMeta (CImgHelperSimg::TMetadata* pMeta)
{
    const int size = OFFSET(CImgHelperSimg::TMetadata, meta.sign.sha256aes.value);

    CSha256 hash;
    int res = hash.Calculate(pMeta, size, size);
    LONGS_EQUAL(size, res);

    res = hash.GetResult( pMeta->meta.sign.sha256aes.value, sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value) );
    LONGS_EQUAL(SHA256_DIGEST_LENGTH, res);

    CAesCbc crypt(AES_128_KEY_SIZE);
    CAesCbc::TParams params = {g_dataKey, pMeta->meta.sign.sha256aes.iv};
    res = crypt.SetParams(&params);
    LONGS_EQUAL(0, res);

    uint encodedSize = sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value);
    res = crypt.Encode(
        pMeta->meta.sign.sha256aes.value,
        sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value),
        sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value),
        pMeta->meta.sign.sha256aes.value,
        &encodedSize
    );

    LONGS_EQUAL(sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value), res);
    LONGS_EQUAL(sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value), encodedSize);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(ImgHelperSimg)
{
    g_pObj = new CImgHelperSimg(getKey);
    CHECK(g_pObj != NULL);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(ImgHelperSimg)
{
    if (g_pObj)
    {
        delete g_pObj;
        g_pObj = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaSize)
{
    int metaSize = g_pObj->CheckMetadata(NULL, 0);
    LONGS_EQUAL(sizeof(CImgHelperSimg::TMetadata), metaSize);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaParserInvalidMagic)
{
    uint32 magic = 0;

    for (uint i = 1; i <= sizeof(magic); ++i)
    {
        int result = g_pObj->CheckMetadata(&magic, i);
        CHECK(result < 0);
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaParserInvalidSignType)
{
    g_meta.meta.typeSign = ~0; // Invalid
    signMeta(&g_meta);
    int result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    CHECK(result < 0);

    g_meta.meta.typeSign = 0; // SIGN_TYPE_SHA256_AES128_CBC
    signMeta(&g_meta);
    result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaParserInvalidSign)
{
    g_meta.meta.sign.sha256aes.value[0] ^= 0xff;

    int result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    CHECK(result < 0);

    signMeta(&g_meta);
    result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaParserInvalidDataCryptType)
{
    g_meta.data.typeCrypt = ~0; // Invalid
    signMeta(&g_meta);
    int result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    CHECK(result < 0);

    g_meta.data.typeCrypt = 0; // CRYPT_TYPE_NONE
    signMeta(&g_meta);
    result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaParserInvalidDataTagType)
{
    g_meta.data.typeTag = ~0; // Invalid
    signMeta(&g_meta);
    int result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    CHECK(result < 0);

    g_meta.data.typeTag = 0; // TAG_TYPE_SHA256
    signMeta(&g_meta);
    result = g_pObj->CheckMetadata( &g_meta, sizeof(g_meta) );
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaImageSize)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    uint size = g_pObj->GetDataSizeImg(&g_meta);
    LONGS_EQUAL(g_meta.data.imgSize, size);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaDataSize)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    uint size = g_pObj->GetDataSize(&g_meta);
    LONGS_EQUAL(g_meta.data.dataSize, size);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaVersion)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    uint32 version = g_pObj->GetDataVersion(&g_meta);
    LONGS_EQUAL(g_meta.data.version, version);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, metaOffset)
{
    uint32 offset = g_pObj->GetBlockMetadataOffset();
    LONGS_EQUAL(0, offset);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, dataOffset)
{
    uint32 offset = g_pObj->GetBlockDataOffset();
    LONGS_EQUAL(256, offset);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, imgId)
{
    int id = g_pObj->GetImgId(NULL);
    CHECK(id < 0);

    id = g_pObj->GetImgId(&g_meta);
    LONGS_EQUAL(SIMG_IMG_ID, id);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, imgParserValidData)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    result = g_pObj->CheckDataImg(g_dataEncoded, sizeof(g_dataEncoded), &g_meta);
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, imgParserInvalidData)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    char invalidData[sizeof(g_dataEncoded)];
    memcpy( invalidData, g_dataEncoded, sizeof(g_dataEncoded) );
    invalidData[0] ^= 0xff;

    result = g_pObj->CheckDataImg(invalidData, sizeof(invalidData), &g_meta);
    CHECK(result < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, checkValidData)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    result = g_pObj->CheckData(g_dataDecoded, sizeof(g_dataDecoded), &g_meta);
    LONGS_EQUAL(0, result);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, checkInvalidData)
{
    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    char invalidData[sizeof(g_dataDecoded)];
    memcpy( invalidData, g_dataDecoded, sizeof(g_dataDecoded) );
    invalidData[0] ^= 0xff;

    result = g_pObj->CheckData(invalidData, sizeof(invalidData), &g_meta);
    CHECK(result < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ImgHelperSimg, imgDecode)
{
    g_meta.data.typeCrypt = 1; // CRYPT_TYPE_AES_128_CBC
    signMeta(&g_meta);

    int result = g_pObj->CheckMetadata(&g_meta, sizeof(g_meta));
    LONGS_EQUAL(0, result);

    char buff[sizeof(g_dataDecoded)];
    uint decodedSize = sizeof(buff);
    result = g_pObj->DecodeImg(g_dataEncoded, sizeof(g_dataEncoded), buff, &decodedSize, &g_meta);
    LONGS_EQUAL(0, result);
    LONGS_EQUAL(sizeof(g_dataDecoded), decodedSize);

    CHECK( !memcmp(buff, g_dataDecoded, sizeof(g_dataDecoded)) );
}
