//*****************************************************************************
//
// @brief   Firmware data loader mock
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "ext/generic/firmware/fwloader.h"


class CMockFwLoader: public CFwLoader
{
    public:
        CMockFwLoader ();
        ~CMockFwLoader ();

        /**
          * @brief  Loader initialization
          * @param  None
          * @return 0 or negative error code
          */
        int Open ();

        /**
          * @brief  Loader deinitialization
          * @param  None
          * @return 0 or negative error code
          */
        int Close ();

        /**
          * @brief  Image loading
          * @param  buff: buffer for save image data
          * @param  size: buffer size
          * @param  pDataOffset: pointer to variable to save received data offset in the image
          * @return < 0 - error code
          *         0 - no data
          *         > 0 - data written in the buffer
          */
        int Load (void* buff, uint size, uint* pDataOffset);

        /**
          * @brief  Set file to read image data
          * @param  fname: file name
          */
        void SetFname (const char* fname);

    private:
        const char* m_fname;
        uint        m_dataOffset;
};
