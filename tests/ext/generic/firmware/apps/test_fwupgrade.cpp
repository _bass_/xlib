//*****************************************************************************
//
// @brief   Firmware updates loader tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "firmware/apps/fwudateloader.h"
#include "firmware/helpers/helper_simg.h"
#include "mocks/devblock/mock_devblock.h"
#include "misc/macros.h"
#include "mock_loader.h"
#include <stdio.h>
#include <string.h>
#include <new>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Encryption key size
    #define EKEY_SIZE           16
    // File name with encryption key data 
    #define EKEY_FNAME          "../test_data/key.bin"
    // Update image file name
    #define FW_FNAME            "../test_data/fw_update.bin"


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static int getKey (void* buff, uint size, void* param);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(FwUpgrade);

    static char g_blockBuff[512];
    static CMockDevBlock g_blockDev(g_blockBuff, sizeof(g_blockBuff) / 4, 4);
    static const TFwSlot g_slotDesc = {&g_blockDev, 0, 4};

    static CImgHelperSimg g_imgHelper(getKey);
    static CMockFwLoader g_imgLoader;
    static CFwUpdateLoader g_fwUpdateLoader(&g_slotDesc, 1, (CFwLoader*)&g_imgLoader, 1, (IImgHelper*)&g_imgHelper);


/**
  * @brief  Read encryption key
  * @param  buff: Buffer for the key
  * @param  size: Buffer size
  * @param  param: Additional parameters
  * @return Size of data written in the buffer or negative error code
  */
int getKey (void* buff, uint size, void* param)
{
    UNUSED_VAR(param);

    if ( size < EKEY_SIZE ) return -1;

    FILE* fp = fopen(EKEY_FNAME, "rb");
    if (!fp) return -2;

    int result = -10;
    do
    {
        size_t len = fread(buff, sizeof(char), EKEY_SIZE, fp);
        if (!len || len != EKEY_SIZE) break;

        result = len;
    } while (0);

    fclose(fp);

    return result;
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(FwUpgrade)
{
    RUN_TEST_CASE(FwUpgrade, base);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(FwUpgrade)
{
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(FwUpgrade)
{
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(FwUpgrade, base)
{
    g_imgLoader.SetFname(FW_FNAME);
    uint map = g_fwUpdateLoader.Load(100);
    CHECK(map != 0);
}
