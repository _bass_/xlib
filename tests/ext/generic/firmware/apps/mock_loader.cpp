//*****************************************************************************
//
// @brief   Firmware data loader mock
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "mock_loader.h"
#include "ext/generic/firmware/fwloader.h"
#include <stdio.h>


/**
  * @brief  Constructor
  * @param  None
  * @return None
  */
CMockFwLoader::CMockFwLoader (void) :
    CFwLoader(),
    m_fname(NULL),
    m_dataOffset(0)
{
}


/**
  * @brief  Destructor
  * @param  None
  * @return None
  */
CMockFwLoader::~CMockFwLoader (void)
{
}


/**
  * @brief  Loader initialization
  * @param  None
  * @return 0 or negative error code
  */
int CMockFwLoader::Open (void)
{
    if (!m_fname) return -1;

    m_dataOffset = 0;

    return 0;
}


/**
  * @brief  Loader deinitialization
  * @param  None
  * @return 0 or negative error code
  */
int CMockFwLoader::Close (void)
{
    return 0;
}


/**
  * @brief  Image loading
  * @param  buff: buffer for save image data
  * @param  size: buffer size
  * @param  pDataOffset: pointer to variable to save received data offset in the image
  * @return < 0 - error code
  *         0 - no data
  *         > 0 - data written in the buffer
  */
int CMockFwLoader::Load (void* buff, uint size, uint* pDataOffset)
{
    if (!m_fname) return -1;

    FILE* fp = fopen(m_fname, "rb");
    if (!fp) return -2;

    int result = -10;
    do
    {
        if (m_dataOffset)
        {
            int res = fseek(fp, m_dataOffset, SEEK_SET);
            if (res) break;
        }

        size_t len = fread(buff, sizeof(char), size, fp);
        if (!len) break;

        if (pDataOffset) *pDataOffset = m_dataOffset;

        result = len;
        m_dataOffset += len;
    } while (0);

    fclose(fp);

    return result;
}


/**
  * @brief  Set file to read image data
  * @param  fname: file name
  */
void CMockFwLoader::SetFname (const char* fname)
{
    m_fname = fname;
}
