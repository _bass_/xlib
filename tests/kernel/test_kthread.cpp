//*****************************************************************************
//
// @brief   Thread tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kthread.h"
#include "misc/macros.h"
#include <new>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    class CWorkerThread : public KThread
    {
        public:
            CWorkerThread (int* pCounter) :
                KThread("CWorker", THREAD_DEFAULT_STACK_SIZE, THREAD_PRIORITY_DEFAULT),
                m_pCounter(pCounter)
            {}

        private:
            int* m_pCounter;

            void run();
    };


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(kthread);


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(kthread)
{
    RUN_TEST_CASE(kthread, base);
    RUN_TEST_CASE(kthread, sleep);
    RUN_TEST_CASE(kthread, suspendAndResume);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(kthread)
{
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(kthread)
{
}


/**
  * @brief  Thread run handler
  * @param  None
  * @return None
  */
void CWorkerThread::run (void)
{
    Kernel::EnterCritical();
    ++(*m_pCounter);
    Kernel::ExitCritical();
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kthread, base)
{
    int counter = 0;
    CWorkerThread worker(&counter);
    KThread::sleep(10);

    CHECK(counter == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kthread, sleep)
{
    uint32 startTime = Kernel::GetJiffies();
    KThread::sleep(10);
    uint32 now = Kernel::GetJiffies();

    CHECK( now >= (startTime + 10) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kthread, suspendAndResume)
{
    int counter = 0;

    Kernel::EnterCritical();
    CWorkerThread worker(&counter);
    worker.Suspend();
    Kernel::ExitCritical();

    KThread::sleep(10);
    CHECK(counter == 0);

    worker.Resume();
    KThread::sleep(10);
    CHECK(counter == 1);
}
