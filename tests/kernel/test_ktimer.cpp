//*****************************************************************************
//
// @brief   Timer functionality testing (kernel)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kthread.h"
#include "kernel/ktimer.h"
#include "misc/macros.h"
#include <new>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(ktimer);

    // Testing timer
    static KTimer* g_pTmr = NULL;
    // Variable changed in timer handler
    static int g_val = 0;


/**
  * @brief  Timer handler
  * @param  timer: Timer object
  * @param  params: Additional parameters
  */
static void timerHdl (KTimer* timer, void* params)
{
    UNUSED_VAR(timer);

    if (!params) return;

    int* ptr = (int*)params;
    ++(*ptr);
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(ktimer)
{
    RUN_TEST_CASE(ktimer, base);
    RUN_TEST_CASE(ktimer, startAndStop);
    RUN_TEST_CASE(ktimer, restart);
    RUN_TEST_CASE(ktimer, changeHandler);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(ktimer)
{
    g_val = 0;
    g_pTmr = new KTimer(timerHdl, &g_val);

    CHECK(g_pTmr != NULL);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(ktimer)
{
    g_val = -1;

    if (g_pTmr)
    {
        delete g_pTmr;
        g_pTmr = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ktimer, base)
{
    KThread::sleep(10);
    CHECK(g_val == 0);

    g_pTmr->Start(10);
    KThread::sleep(30);
    CHECK(g_val == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ktimer, startAndStop)
{
    g_pTmr->Start(40);
    KThread::sleep(20);
    g_pTmr->Stop();
    KThread::sleep(20);
    CHECK(g_val == 0);

    g_pTmr->Start(10);
    KThread::sleep(20);
    CHECK(g_val == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ktimer, restart)
{
    g_pTmr->Start(40);
    KThread::sleep(20);
    g_pTmr->Restart();
    KThread::sleep(20);
    CHECK(g_val == 0);

    KThread::sleep(20);
    CHECK(g_val == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(ktimer, changeHandler)
{
    g_pTmr->Start(10);
    KThread::sleep(20);
    CHECK(g_val == 1);

    int val = 0;
    g_pTmr->SetHandler(timerHdl, &val);
    g_pTmr->Start(10);
    KThread::sleep(20);
    CHECK(val == 1);
}
