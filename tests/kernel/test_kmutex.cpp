//*****************************************************************************
//
// @brief   Mutex functionality tests (kernel)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kthread.h"
#include "kernel/kmutex.h"
#include "misc/macros.h"
#include <new>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    class CWorkerMutex : public KThread
    {
        public:
            CWorkerMutex () :
                KThread("CWorker", THREAD_DEFAULT_STACK_SIZE, THREAD_PRIORITY_DEFAULT)
            {}

        private:
            void run ();
    };



// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(kmutex);

    // Testing mutex
    static KMutex* g_pMutex = NULL;
    // Target variable
    static int g_val = 0;


/**
  * @brief  Thread handler to change variable value
  * @param  None
  * @return None
  */
void CWorkerMutex::run (void)
{
    g_pMutex->Lock();
    ++g_val;
    g_pMutex->Unlock();
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(kmutex)
{
    RUN_TEST_CASE(kmutex, base);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(kmutex)
{
    g_pMutex = new KMutex();
    CHECK(g_pMutex != NULL);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(kmutex)
{
    if (g_pMutex)
    {
        delete g_pMutex;
        g_pMutex = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kmutex, base)
{
    g_pMutex->Lock();
    CHECK(g_val == 0);

    CWorkerMutex worker;
    KThread::sleep(10);
    CHECK(g_val == 0);

    g_pMutex->Unlock();
    KThread::sleep(10);
    CHECK(g_val == 1);
}
