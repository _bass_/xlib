//*****************************************************************************
//
// @brief   Kernel functionality tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kernel.h"
#include "kernel/kthread.h"
#include "misc/macros.h"
#include <new>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define KERNEL_TEST_EVENT           EVENT_CODE(0xaaaa, 0)


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    class CWorkerKernel : public KThread
    {
        public:
            CWorkerKernel () :
                KThread("CWorker", THREAD_DEFAULT_STACK_SIZE, THREAD_PRIORITY_DEFAULT)
            {}

        private:
            void run ();
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    // System events handler
    static void sysEventHandler (uint code, void* params);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(kernel);

    // System event handler registration
    REGISTER_EVENT_HANDLER(sysEventHandler);
    REGISTER_EVENT_CODE_HANDLER(KERNEL_TEST_EVENT, sysEventHandler);

    // Variable changed in event handler
    static uint g_val = 0;


/**
  * @brief  System events handler
  * @param  code: Event code
  * @param  params: Additional parameters
  */
void sysEventHandler (uint code, void* params)
{
    UNUSED_VAR(code);
    UNUSED_VAR(params);
    ++g_val;
}


/**
  * @brief  Thread handler to change variable value
  * @param  None
  * @return None
  */
void CWorkerKernel::run (void)
{
    Kernel::EnterCritical();
    ++g_val;
    Kernel::ExitCritical();
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(kernel)
{
    RUN_TEST_CASE(kernel, jiffies);
    RUN_TEST_CASE(kernel, criticalSection);
    RUN_TEST_CASE(kernel, nestedCriticalSection);
    RUN_TEST_CASE(kernel, riseEvent);
    RUN_TEST_CASE(kernel, postEvent);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(kernel)
{
    g_val = 0;
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(kernel)
{
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kernel, jiffies)
{
    uint32 time = Kernel::GetJiffies();
    KThread::sleep(10);

    CHECK( time <= (Kernel::GetJiffies() + 10) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kernel, criticalSection)
{
    Kernel::EnterCritical();
    {
        CWorkerKernel worker;

        KThread::sleep(10);
        CHECK(g_val == 0);
    }
    Kernel::ExitCritical();

    KThread::sleep(10);
    CHECK(g_val == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kernel, nestedCriticalSection)
{
    Kernel::EnterCritical();
    CWorkerKernel worker;

    KThread::sleep(10);
    CHECK(g_val == 0);

        Kernel::EnterCritical();
        KThread::sleep(10);
        CHECK(g_val == 0);
        Kernel::ExitCritical();

    KThread::sleep(10);
    CHECK(g_val == 0);

    Kernel::ExitCritical();

    KThread::sleep(10);
    CHECK(g_val == 1);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kernel, riseEvent)
{
    Kernel::RiseEvent(KERNEL_TEST_EVENT);
    // Event handler must be called twice because it registered
    // as handler for all system events anf for event KERNEL_TEST_EVENT
    CHECK(g_val == 2);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kernel, postEvent)
{
    Kernel::RiseEvent(KERNEL_TEST_EVENT);
    KThread::sleep(10);
    // Event handler must be called twice because it registered
    // as handler for all system events anf for event KERNEL_TEST_EVENT
    CHECK(g_val == 2);
}
