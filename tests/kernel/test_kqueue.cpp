//*****************************************************************************
//
// @brief   Queue functionality tests (kernel)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kthread.h"
#include "kernel/kqueue.h"
#include "misc/macros.h"
#include <new>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    class CWorkerQueue : public KThread
    {
        public:
            CWorkerQueue (int* pVal) :
                KThread("CWorker", THREAD_DEFAULT_STACK_SIZE, THREAD_PRIORITY_DEFAULT),
                m_pVal(pVal)
            {}

        private:
            int*    m_pVal;

            void run();
    };



// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(kqueue);

    // Testing queue
    static KQueue* g_pQueue = NULL;


/**
  * @brief  Thread handler to add value in queue
  * @param  None
  * @return None
  */
void CWorkerQueue::run (void)
{
    g_pQueue->Add(m_pVal);
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(kqueue)
{
    RUN_TEST_CASE(kqueue, base);
    RUN_TEST_CASE(kqueue, peek);
    RUN_TEST_CASE(kqueue, count);
    RUN_TEST_CASE(kqueue, isFull);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(kqueue)
{
    g_pQueue = new KQueue(2, sizeof(int));
    CHECK(g_pQueue != NULL);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(kqueue)
{
    if (g_pQueue)
    {
        delete g_pQueue;
        g_pQueue = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kqueue, base)
{
    int val = 1;
    CWorkerQueue worker(&val);

    KThread::sleep(10);
    CHECK(g_pQueue->Count() == 1);

    int testVal = 0;
    g_pQueue->Get(&testVal);

    CHECK(g_pQueue->Count() == 0);
    CHECK(val == testVal);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kqueue, peek)
{
    int val = 1;
    g_pQueue->Add(&val);
    CHECK(g_pQueue->Count() == 1);

    int testVal = 0;
    g_pQueue->Peek(&testVal);

    CHECK(g_pQueue->Count() == 1);
    CHECK(val == testVal);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kqueue, count)
{
    CHECK(g_pQueue->Count() == 0);

    int val = 1;
    g_pQueue->Add(&val);
    g_pQueue->Add(&val);

    CHECK(g_pQueue->Count() == 2);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kqueue, isFull)
{
    CHECK(g_pQueue->Count() == 0);
    CHECK( !g_pQueue->IsFull() );

    int val = 1;
    g_pQueue->Add(&val);
    g_pQueue->Add(&val);

    CHECK(g_pQueue->Count() == 2);
    CHECK( g_pQueue->IsFull() );
}
