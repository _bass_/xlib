//*****************************************************************************
//
// @brief   Kernel IO (kstdio) functionality tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "kernel/kstdio.h"
#include "dev/null.h"
#include "misc/macros.h"
#include <new>
#include <string.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(kstdio);

    // IO buffer
    static char g_buff[64];
    // Configuration parameters for devnull
    static const CDevNull::TOptions g_devCfg = {g_buff, sizeof(g_buff)};
    // IO device
    static CDevNull g_ioDev;
    // Local IO stream
    static KStream g_stream;


/**
  * @brief  Check results of fprintf testing
  * @param  expectedStr: Expected string
  * @param  resLen: String lengh which is built in g_buff
  */
static void checkResultFprintf (const char* expectedStr, int resLen)
{
    LONGS_EQUAL(strlen(expectedStr), resLen);
    CHECK( resLen > 0 && (uint)resLen < sizeof(g_buff) );

    g_buff[resLen] = '\0';
    STRCMP_EQUAL(expectedStr, g_buff);

    g_ioDev.Read(g_buff, (uint)resLen);
}


/**
  * @brief  Check results of sprintf testing
  * @param  expectedStr: Expected string
  * @param  resLen: String lengh which is built in g_buff
  */
static void checkResultSprintf (char* buff, uint buffSize, const char* expectedStr, int resLen)
{
    LONGS_EQUAL(strlen(expectedStr), resLen);
    CHECK(resLen > 0 && (uint)resLen < buffSize);

    buff[resLen] = '\0';
    STRCMP_EQUAL(expectedStr, buff);
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(kstdio)
{
    RUN_TEST_CASE(kstdio, fprintf);
    RUN_TEST_CASE(kstdio, sprintf);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(kstdio)
{
    int result = g_ioDev.Open();
    CHECK(result == DEV_OK);
    result = g_ioDev.Configure(&g_devCfg);
    CHECK(result == DEV_OK);

    memset( g_buff, 0xaa, sizeof(g_buff) );

    g_stream.pDev = &g_ioDev;
    g_stream.pMutex = NULL;
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(kstdio)
{
    int result = g_ioDev.Close();
    CHECK(result == DEV_OK);

    g_stream.pDev = NULL;
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kstdio, fprintf)
{
    // char
    int len = fprintf(&g_stream, "%c %c", 'A', 'b');
    checkResultFprintf("A b", len);

    // string
    len = fprintf(&g_stream, "%s %s", "test", "string");
    checkResultFprintf("test string", len);

    // decimal
    len = fprintf(&g_stream, "%d %u", -123, 456);
    checkResultFprintf("-123 456", len);

    // hex
    len = fprintf(&g_stream, "%x %.4x", 0x55, 0xbb);
    checkResultFprintf("55 00bb", len);

    // float
    len = fprintf(&g_stream, "%5.2f %.2f", 1.23, 4.567);
    checkResultFprintf(" 1.23 4.56", len);

    // dump
    const char data[] = {0xaa, 0xbb, 0xcc, 0xdd};
    len = fprintf(&g_stream, "%p", data, ARR_COUNT(data));
    checkResultFprintf("AABBCCDD", len);

    // bin
    len = fprintf(&g_stream, "%b %.16b", 123, 6789);
    checkResultFprintf("1111011 0001101010000101", len);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(kstdio, sprintf)
{
    char buff[64];

    // char
    int len = sprintf(buff, sizeof(buff), "%c %c", 'A', 'b');
    checkResultSprintf(buff, sizeof(buff), "A b", len);

    // string
    len = sprintf(buff, sizeof(buff), "%s %s", "test", "string");
    checkResultSprintf(buff, sizeof(buff), "test string", len);

    // decimal
    len = sprintf(buff, sizeof(buff), "%d %u", -123, 456);
    checkResultSprintf(buff, sizeof(buff), "-123 456", len);

    // hex
    len = sprintf(buff, sizeof(buff), "%x %.4x", 0x55, 0xbb);
    checkResultSprintf(buff, sizeof(buff), "55 00bb", len);

    // float
    len = sprintf(buff, sizeof(buff), "%5.2f %.2f", 1.23, 4.567);
    checkResultSprintf(buff, sizeof(buff), " 1.23 4.56", len);

    // dump
    const char data[] = {0xaa, 0xbb, 0xcc, 0xdd};
    len = sprintf(buff, sizeof(buff), "%p", data, ARR_COUNT(data));
    checkResultSprintf(buff, sizeof(buff), "AABBCCDD", len);

    // bin
    len = sprintf(buff, sizeof(buff), "%b %.16b", 123, 6789);
    checkResultSprintf(buff, sizeof(buff), "1111011 0001101010000101", len);
}
