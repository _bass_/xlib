//*****************************************************************************
//
// @brief   SLFS tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "mocks/devblock/mock_devblock.h"
#include "fs/slfs/slfs.h"
#include "misc/macros.h"
#include <string.h>
#include <new>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Block device number
    #define TEST_SLFS_BLOCK_DEV_NUM         10
    // Partition ID
    #define TEST_SLFS_PARTITION_ID          0
    // Block size (bytes)
    #define TEST_SLFS_BLOCK_SIZE            64
    // Number of blocks in partition
    #define TEST_SLFS_BLOCKS_COUNT          5


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(SLFS);

    // Testing object
    static CSlfs* g_pObj = NULL;
    // File system descriptor
    static const TFsDesc g_fsDesc =
    {
        TEST_SLFS_BLOCK_DEV_NUM,
        TEST_SLFS_PARTITION_ID,
        FS_TYPE_SLFS,
        0,
        TEST_SLFS_BLOCKS_COUNT
    };

    // Block device emulation
    static char g_buff[TEST_SLFS_BLOCK_SIZE * TEST_SLFS_BLOCKS_COUNT];
    static CMockDevBlock g_blockDev(g_buff, TEST_SLFS_BLOCK_SIZE, TEST_SLFS_BLOCKS_COUNT);
    // Device registration
    DEV_REGISTER(DEV_NAME_BLOCK, TEST_SLFS_BLOCK_DEV_NUM, g_blockDev);

    // File data
    uint32 g_fname = 1;
    const char g_fData[] = "test data";


/**
  * @brief  File creation
  * @param  id: File name (ID)
  * @param  data: File data
  * @param  size: Data size
  */
static void createFile (uint32 id, const void* data, uint size)
{
    int fd = g_pObj->Fopen( (const char*)&id, FILE_MODE_CREATE|FILE_MODE_WRITE );
    CHECK(fd >= 0);

    int res = g_pObj->Fwrite(fd, data, size);
    LONGS_EQUAL(size, res);

    res = g_pObj->Fclose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(SLFS)
{
    RUN_TEST_CASE(SLFS, FopenAndFclose);
    RUN_TEST_CASE(SLFS, FwriteAndFread);
    RUN_TEST_CASE(SLFS, Fstat);
    RUN_TEST_CASE(SLFS, dirOpenClose);
    RUN_TEST_CASE(SLFS, dirRead);
    RUN_TEST_CASE(SLFS, looping);
    RUN_TEST_CASE(SLFS, fileRewriteError);
    RUN_TEST_CASE(SLFS, VfsGetFs);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(SLFS)
{
    int res = g_blockDev.Open();
    CHECK(!res);

    g_pObj = new CSlfs(&g_fsDesc);
    CHECK(g_pObj != NULL);

    // Built-in check format -> mount
    res = g_pObj->Mount();
    CHECK(res < 0);

    res = g_pObj->Format();
    LONGS_EQUAL(0, res);

    res = g_pObj->Mount();
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(SLFS)
{
    int res = g_pObj->Umount();
    LONGS_EQUAL(0, res);

    if (g_pObj)
    {
        delete g_pObj;
        g_pObj = NULL;
    }

    res = g_blockDev.Close();
    CHECK(!res);

}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, FopenAndFclose)
{
    uint32 fname = 1;
    createFile( fname, g_fData, sizeof(g_fData) );

    int fd = g_pObj->Fopen( (const char*)&fname, FILE_MODE_READ );
    CHECK(fd >= 0);
    int res = g_pObj->Fclose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, FwriteAndFread)
{
    uint32 fname = 1;
    char buff[sizeof(g_fData)] = {};

    createFile( fname, g_fData, sizeof(g_fData) );

    int fd = g_pObj->Fopen( (const char*)&fname, FILE_MODE_READ );
    CHECK(fd >= 0);

    int res = g_pObj->Fread( fd, buff, sizeof(buff) );
    LONGS_EQUAL(sizeof(buff), res);

    res = g_pObj->Fclose(fd);
    LONGS_EQUAL(0, res);

    CHECK( !memcmp(g_fData, buff, sizeof(buff)) );
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, Fstat)
{
    uint32 fname = 1;
    TFileInfo info = {};

    createFile( fname, g_fData, sizeof(g_fData) );

    int fd = g_pObj->Fopen( (const char*)&fname, FILE_MODE_READ );
    CHECK(fd >= 0);

    int res = g_pObj->Fstat(fd, &info);
    LONGS_EQUAL(0, res);
    LONGS_EQUAL( fname, *((uint32*)info.name) );
    LONGS_EQUAL(sizeof(g_fData), info.size);

    res = g_pObj->Fclose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, dirOpenClose)
{
    int fd = g_pObj->DirOpen("?");
    CHECK(fd < 0);

    fd = g_pObj->DirOpen("/");
    CHECK(fd >= 0);

    int res = g_pObj->DirClose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, dirRead)
{
    uint32 fname = 1;
    TFileInfo info = {};
    char fData2[sizeof(g_fData) + 1];

    memcpy( fData2, g_fData, sizeof(g_fData) );
    fData2[sizeof(g_fData)] = 2;

    createFile( fname, g_fData, sizeof(g_fData) );
    createFile( fname + 1, fData2, sizeof(fData2) );

    int fd = g_pObj->DirOpen("/");
    CHECK(fd >= 0);

    int res = g_pObj->DirRead(fd, &info);
    LONGS_EQUAL(0, res);
    LONGS_EQUAL( fname + 1, *((uint32*)info.name) );
    LONGS_EQUAL(sizeof(fData2), info.size);

    res = g_pObj->DirRead(fd, &info);
    LONGS_EQUAL(0, res);
    LONGS_EQUAL( fname, *((uint32*)info.name) );
    LONGS_EQUAL(sizeof(g_fData), info.size);

    res = g_pObj->DirClose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, looping)
{
    const uint32 firstName = 1;
    char fData[sizeof(g_fData) + 1];
    char buff[sizeof(fData)];

    memcpy( fData, g_fData, sizeof(g_fData) );

    uint count = TEST_SLFS_BLOCK_SIZE * TEST_SLFS_BLOCKS_COUNT / sizeof(g_fData);
    for (uint i = 0; i <= count; ++i)
    {
        fData[sizeof(g_fData)] = firstName + i;
        createFile( firstName + i, fData, sizeof(fData) );
    }

    int fd = g_pObj->DirOpen("/");
    CHECK(fd >= 0);

    TFileInfo info = {};
    for (uint i = firstName + count; i >= firstName; --i)
    {
        int res = g_pObj->DirRead(fd, &info);
        if (res) break;

        uint32 fname = *((uint32*)info.name);
        LONGS_EQUAL(i, fname);
        LONGS_EQUAL(sizeof(fData), info.size);

        int f = g_pObj->Fopen( (const char*)&fname, FILE_MODE_READ );
        CHECK(f >= 0);

        res = g_pObj->Fread( f, buff, sizeof(buff) );
        LONGS_EQUAL(sizeof(buff), res);

        res = g_pObj->Fclose(f);
        CHECK(!res);

        fData[sizeof(g_fData)] = i;
        CHECK( !memcmp(fData, buff, sizeof(buff)) );
    }

    int res = g_pObj->DirClose(fd);
    LONGS_EQUAL(0, res);

    CHECK(*((uint32*)info.name) != firstName);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, fileRewriteError)
{
    uint32 fname = 1;
    createFile( fname, g_fData, sizeof(g_fData) );

    int fd = g_pObj->Fopen( (const char*)&fname, FILE_MODE_WRITE );
    CHECK(fd >= 0);

    int res = g_pObj->Fwrite( fd, g_fData, sizeof(g_fData) * 2 );
    CHECK(res < 0);

    res = g_pObj->Fclose(fd);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SLFS, VfsGetFs)
{
    VFS::Init(&g_fsDesc, 1);

    bool testPassed = false;
    do
    {
        int res = VFS::Mount(TEST_SLFS_PARTITION_ID);
        if (res) break;

        CFs* pObj = VFS::GetFs(TEST_SLFS_PARTITION_ID);
        if (!pObj) break;

        testPassed = true;
    } while (0);

    VFS::Uninit();

    CHECK(testPassed);
}
