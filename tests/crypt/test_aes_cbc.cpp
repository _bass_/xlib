//*****************************************************************************
//
// @brief   AES CBC encryption tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "crypt/aes_cbc.h"
#include "misc/macros.h"
#include <string.h>
#include <new>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(AES_CBC);

    // Testing object
    static ICrypt* g_pObj = NULL;

    // Test data
    static const char* g_dataDecoded[] =
    {
        "test",
        "0123456789abcdefghijklmnopqrstuvwxyz",
        "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789",
        "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
        "0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz"
    };

    // "test"
    static const char g_encodedData_128_0[] =
    {
        0xd4,0xc6,0x2d,0x90,0xcf,0xb0,0x26,0x80,0x9a,0x11,0x68,0xec,0xad,0xa0,0x16,0xce
    };
    static const char g_encodedData_256_0[] =
    {
        0xf5,0x6f,0x8b,0xae,0x19,0xc1,0xa1,0xd1,0x97,0xf9,0xf4,0x67,0xba,0xee,0x19,0xa7
    };
    // "0123456789abcdefghijklmnopqrstuvwxyz"
    static const char g_encodedData_128_1[] =
    {
        0xcd,0xc4,0xcf,0xa1,0x29,0x4b,0xee,0x52,0x36,0x72,0xc2,0xf6,0x33,0xc9,0x83,0xb3,
        0xc9,0x34,0x60,0xb1,0x1c,0x0b,0x1c,0xe8,0x0f,0xbd,0xcf,0x76,0x5a,0x7b,0xda,0xd2,
        0x92,0x69,0x3b,0x01,0x84,0x07,0x53,0x52,0x16,0x88,0x1b,0x56,0xb5,0xef,0xa5,0xb6
    };
    static const char g_encodedData_256_1[] =
    {
        0x60,0x63,0x3e,0x41,0x0c,0x0a,0xbe,0xa0,0x6a,0x7e,0xf3,0xf0,0x66,0x62,0xf7,0x25,
        0x33,0x7c,0xa3,0xdb,0x8f,0x18,0xee,0x2e,0xa5,0x5c,0xf0,0xc9,0x00,0x84,0xff,0x02,
        0xe4,0x73,0xe0,0x03,0x6d,0xcd,0x55,0xd2,0x9a,0x56,0x4b,0x3b,0x99,0x48,0x23,0x60
    };
    // "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
    static const char g_encodedData_128_2[] =
    {
        0xf3,0xab,0xb3,0x1b,0xcb,0x17,0xea,0xa3,0x9f,0xcd,0x06,0x3d,0x75,0x1e,0xec,0xed,
        0xbd,0x17,0x8c,0xec,0x72,0xeb,0xed,0x0d,0x9c,0xe8,0xa1,0xa6,0x7a,0x7e,0x18,0x04,
        0xb7,0xe5,0xe5,0xa0,0x1b,0x62,0x80,0x76,0xc3,0x76,0xe5,0x31,0x4c,0x2e,0xc1,0xd5,
        0x6d,0x99,0x64,0x62,0x38,0x6c,0x0f,0xf0,0xea,0xf9,0x48,0x17,0x5d,0xa8,0x76,0x34,
        0xfa,0x42,0x98,0xe3,0x0e,0x75,0x3e,0x33,0x01,0xb2,0x63,0xc7,0x56,0x26,0x72,0x77,
        0x23,0xa5,0x0f,0x07,0x87,0x25,0xc1,0x87,0x7e,0x42,0x13,0x6e,0x71,0x27,0x94,0x57,
        0x2e,0xe3,0x37,0x8e,0x9f,0xc3,0x8b,0x70,0x88,0xe3,0xd5,0x6c,0x42,0x73,0x48,0x60,
        0x10,0xe2,0x9b,0xdd,0xf5,0x1f,0xc3,0x87,0x14,0xa2,0x61,0xd3,0xf8,0x9b,0x79,0xc0,
        0x68,0xfc,0x0d,0x50,0x38,0xcf,0x13,0xb1,0xe9,0x15,0x9b,0xb6,0x2a,0x1a,0xca,0x4c
    };
    static const char g_encodedData_256_2[] =
    {
        0x26,0x4d,0x53,0x22,0xdb,0x8d,0x1c,0xa4,0x64,0x2d,0xa8,0x6b,0xe1,0x2a,0x68,0x53,
        0x5c,0x57,0xf3,0x0b,0x02,0xc5,0xb2,0xff,0x04,0x79,0x16,0x16,0xf7,0x31,0x79,0x59,
        0xdc,0x1c,0x56,0xbc,0xa1,0x58,0x46,0x13,0xc2,0x2b,0x2a,0x51,0x81,0xdd,0xfe,0x01,
        0x2b,0x55,0x3f,0x04,0xe6,0xf6,0xb8,0x7c,0xd6,0x71,0xa0,0xc9,0xc1,0xb6,0x47,0xb3,
        0x90,0x6a,0x9b,0xdd,0x81,0xb1,0xa9,0xde,0x34,0x32,0x07,0xa9,0x26,0x3e,0xd0,0x4b,
        0xe1,0xf6,0x82,0x8e,0x97,0xe2,0x1f,0x7c,0x0f,0xac,0x61,0x37,0xff,0x9b,0x7f,0xc3,
        0x4c,0x59,0x64,0x3a,0x25,0x93,0x07,0x1d,0x34,0x67,0x6e,0x70,0xd4,0x87,0x5c,0x6f,
        0x1e,0xec,0x2d,0x4d,0xc0,0xa3,0x96,0xfd,0x44,0x42,0xcf,0xd2,0xbf,0x20,0x99,0xe7,
        0x99,0x20,0xa0,0xc4,0x1b,0xae,0x9b,0x02,0xb3,0x8a,0xce,0x4a,0x10,0xf8,0x61,0x4c
    };
    // "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    static const char g_encodedData_128_3[] =
    {
        0xe6,0x55,0x65,0x9c,0x63,0xe3,0x49,0xc2,0x0a,0x73,0x3c,0x42,0x56,0xfd,0x74,0xcc,
        0x02,0xd6,0x7f,0x21,0xdb,0xa6,0x97,0xb6,0xac,0xda,0xe2,0x1b,0xf7,0x9c,0x1d,0xf6,
        0xb7,0x71,0x3b,0x39,0x91,0x0b,0x8b,0x91,0xf5,0x0d,0xda,0x15,0x0d,0x56,0xc1,0x2c,
        0xed,0xa4,0x61,0x42,0x86,0x4a,0x93,0x2d,0x92,0x73,0x47,0x37,0xda,0xd9,0x75,0xcd,
        0x51,0x61,0x02,0xee,0x46,0x97,0x55,0x4f,0x3f,0x80,0xeb,0x99,0xed,0xfb,0xc0,0xba,
        0xb7,0xd3,0xf0,0x05,0x56,0x64,0x03,0xe5,0xb0,0xb5,0x3c,0x8a,0xb1,0x9c,0xe8,0xae,
        0x3a,0x44,0x79,0xf5,0xe4,0x95,0x42,0x52,0xd9,0x1d,0x6e,0x87,0x45,0x7a,0xe3,0x16,
        0x61,0x5f,0x29,0xd6,0xb9,0x11,0xf2,0x9b,0xd6,0xeb,0x15,0xc1,0xf1,0x59,0x76,0xcf,
        0xa7,0x72,0x54,0x16,0x26,0xf5,0x7c,0x9b,0x7d,0x7b,0x09,0xe4,0x65,0xee,0x90,0xea
    };
    static const char g_encodedData_256_3[] =
    {
        0x25,0xfb,0x9d,0x26,0xdb,0xff,0x78,0x48,0x4a,0x52,0x9d,0x36,0x38,0xf8,0x44,0x1b,
        0x92,0x60,0x20,0x03,0xa9,0xdf,0x67,0x04,0x02,0xfa,0x13,0x23,0x95,0x78,0xd4,0x3b,
        0x53,0xe8,0xc8,0x88,0x60,0x91,0x97,0xff,0x74,0xc7,0xf6,0x8f,0xa0,0x7e,0xf2,0x52,
        0x65,0x4f,0xf7,0xf8,0x8c,0x82,0x48,0x51,0xf2,0x24,0xea,0xc8,0x0e,0xf5,0xab,0xa5,
        0x9f,0x5a,0x88,0xe3,0xb3,0x79,0x0e,0xec,0x66,0x7b,0x91,0xee,0x4e,0x1d,0x66,0x70,
        0x02,0x8c,0x06,0x12,0x75,0x8c,0x9d,0xc4,0x54,0xc1,0xee,0xd2,0xeb,0x6a,0xfb,0x22,
        0x8f,0x99,0x33,0x7f,0x4d,0x1d,0x71,0x5b,0x4e,0x99,0xf5,0x5b,0xf5,0x5b,0xf3,0x2c,
        0x2f,0xfd,0xa7,0x26,0xf5,0xd9,0xd2,0x4f,0x0d,0xf0,0x58,0x1c,0x9c,0x82,0xa7,0x67,
        0x81,0x99,0xc5,0xe3,0xe7,0xe6,0xc2,0x5c,0x43,0x4b,0xb5,0x6a,0x91,0x76,0xc5,0xfc
};
    // "0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz"
    static const char g_encodedData_128_4[] =
    {
        0xcd,0xc4,0xcf,0xa1,0x29,0x4b,0xee,0x52,0x36,0x72,0xc2,0xf6,0x33,0xc9,0x83,0xb3,
        0xc9,0x34,0x60,0xb1,0x1c,0x0b,0x1c,0xe8,0x0f,0xbd,0xcf,0x76,0x5a,0x7b,0xda,0xd2,
        0x25,0xd1,0x9b,0x6e,0x3d,0xf1,0xe5,0x55,0x5f,0xae,0x37,0xf6,0x6c,0x84,0x78,0x8b,
        0x48,0x2a,0x8b,0x73,0xe5,0xa1,0x6f,0x34,0x48,0x06,0x50,0x43,0x7e,0x9d,0x19,0x67,
        0x76,0x58,0x54,0x2e,0x9c,0x39,0xc8,0x70,0x28,0x7c,0xa7,0xdb,0xf5,0x24,0xd7,0x28,
        0xe5,0x48,0x28,0x99,0x2b,0x63,0x8e,0x9a,0x8f,0x5c,0xa2,0x39,0x93,0x84,0x62,0x46,
        0xa2,0xf0,0xbe,0x58,0x77,0x62,0x72,0x60,0xf1,0xe9,0x37,0x5f,0x28,0x69,0x15,0x70,
        0x58,0x05,0x06,0x01,0xc9,0x01,0x11,0xb4,0x1e,0x9d,0xd4,0x6a,0x23,0xc3,0x5a,0x07,
        0x09,0x00,0xe8,0x44,0x9c,0xd4,0x71,0x40,0x02,0xb8,0xc0,0x2d,0x66,0x8b,0xe0,0x8d
    };
    static const char g_encodedData_256_4[] =
    {
        0x60,0x63,0x3e,0x41,0x0c,0x0a,0xbe,0xa0,0x6a,0x7e,0xf3,0xf0,0x66,0x62,0xf7,0x25,
        0x33,0x7c,0xa3,0xdb,0x8f,0x18,0xee,0x2e,0xa5,0x5c,0xf0,0xc9,0x00,0x84,0xff,0x02,
        0x31,0x8f,0xd5,0xc4,0x01,0x8c,0x74,0x49,0x4e,0x22,0x8a,0xad,0xc9,0xc4,0xf9,0x00,
        0x8f,0x39,0xbb,0xdd,0x9f,0x8e,0xae,0x6b,0xaa,0xa2,0xb8,0xf1,0x21,0x2b,0xa9,0x01,
        0x78,0x3f,0x20,0xc0,0xae,0x6d,0xab,0x62,0xd8,0x99,0xd1,0xc0,0x83,0x93,0x17,0x25,
        0x68,0x76,0xf1,0xd4,0x5c,0xcd,0x7d,0x60,0x40,0x91,0x0f,0xf7,0xf3,0x0c,0xcb,0x81,
        0xa4,0xba,0x32,0xf6,0xd2,0x85,0xac,0x9d,0x37,0xb1,0x28,0x4a,0x55,0x8d,0xfb,0x9d,
        0xfc,0xa1,0x27,0x68,0x61,0x83,0xe6,0x91,0x13,0xf1,0x7f,0x51,0xd1,0x51,0xd7,0x8c,
        0x5f,0x33,0x95,0x90,0xcc,0x7a,0x7e,0x1c,0xcb,0xe1,0x5b,0xef,0x0a,0x65,0x4f,0xba
    };

    static const char* g_dataEncoded_128[ARR_COUNT(g_dataDecoded)] =
    {
        g_encodedData_128_0,
        g_encodedData_128_1,
        g_encodedData_128_2,
        g_encodedData_128_3,
        g_encodedData_128_4
    };
    static const char* g_dataEncoded_256[ARR_COUNT(g_dataDecoded)] =
    {
        g_encodedData_256_0,
        g_encodedData_256_1,
        g_encodedData_256_2,
        g_encodedData_256_3,
        g_encodedData_256_4
    };


    // Encryption parameters
    static uint g_keySize;
    static char g_iv[AES_BLOCK_SIZE];
    static char g_key[MAX(AES_128_KEY_SIZE, AES_256_KEY_SIZE)];


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(AES_CBC)
{
    for (uint i = 0; i < sizeof(g_iv); ++i)
    {
        g_iv[i] = sizeof(g_iv) - i;
    }

    uint testCount = 2;
    g_keySize = AES_128_KEY_SIZE;

    while (testCount--)
    {
        for (uint i = 0; i < g_keySize; ++i)
        {
            g_key[i] = i;
        }

        RUN_TEST_CASE(AES_CBC, wrongParamsSetParams);
        RUN_TEST_CASE(AES_CBC, wrongParamsEncode);
        RUN_TEST_CASE(AES_CBC, wrongParamsDecode);
        RUN_TEST_CASE(AES_CBC, wrongParamsAppend);
        RUN_TEST_CASE(AES_CBC, fullEncode);
        RUN_TEST_CASE(AES_CBC, fullDecode);
        RUN_TEST_CASE(AES_CBC, partEncode);
        RUN_TEST_CASE(AES_CBC, partDecode);

        g_keySize = AES_256_KEY_SIZE;
    }
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(AES_CBC)
{
    g_pObj = new CAesCbc(g_keySize);
    CHECK(g_pObj != NULL);

    CAesCbc::TParams params = {g_key, g_iv};
    int res = g_pObj->SetParams(&params);
    LONGS_EQUAL(0, res);
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(AES_CBC)
{
    if (g_pObj)
    {
        delete g_pObj;
        g_pObj = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, wrongParamsSetParams)
{
    int res = g_pObj->SetParams(NULL);
    CHECK(res < 0);

    CAesCbc::TParams params = {};
    res = g_pObj->SetParams(&params);
    CHECK(res < 0);

    params.pKey = g_key;
    params.pIv = NULL;
    res = g_pObj->SetParams(&params);
    CHECK(res < 0);

    params.pKey = NULL;
    params.pIv = g_iv;
    res = g_pObj->SetParams(&params);
    CHECK(res < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, wrongParamsEncode)
{
    char buff[AES_BLOCK_SIZE];

    int res = g_pObj->Encode(NULL, 0, 0, NULL, NULL);
    CHECK(res < 0);

    uint buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Encode(NULL, AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE, AES_BLOCK_SIZE, NULL, &buffSize);
    CHECK(res < 0);

    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, NULL);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Encode(g_dataDecoded[0], 0, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE, 0, buff, &buffSize);
    CHECK(res < 0);

    buffSize = 0;
    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE * 2, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE - 1;
    res = g_pObj->Encode(g_dataDecoded[0], AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, wrongParamsDecode)
{
    char buff[AES_BLOCK_SIZE];

    int res = g_pObj->Decode(NULL, 0, 0, NULL, NULL);
    CHECK(res < 0);

    uint buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Decode(NULL, AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Decode(buff, 0, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, 0, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, AES_BLOCK_SIZE, NULL, &buffSize);
    CHECK(res < 0);

    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, NULL);
    CHECK(res < 0);

    buffSize = 0;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE * 2, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE - 1;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, AES_BLOCK_SIZE, buff, &buffSize);
    CHECK(res < 0);

    buffSize = AES_BLOCK_SIZE + 1;
    res = g_pObj->Decode(buff, AES_BLOCK_SIZE, AES_BLOCK_SIZE + 1, buff, &buffSize);
    CHECK(res < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, wrongParamsAppend)
{
    char buff[AES_BLOCK_SIZE];

    for (uint i = 0; i < 3; ++i)
    {
        uint buffSize = AES_BLOCK_SIZE;
        int res;

        if (i == 1)
        {
            res = g_pObj->Encode(buff, sizeof(buff), sizeof(buff), buff, &buffSize);
            LONGS_EQUAL(sizeof(buff), res);
        }
        else if (i == 2)
        {
            res = g_pObj->Decode(buff, sizeof(buff), sizeof(buff), buff, &buffSize);
            LONGS_EQUAL(sizeof(buff), res);
        }

        res = g_pObj->Append(NULL, 0, NULL, 0);
        CHECK(res < 0);

        buffSize = AES_BLOCK_SIZE;
        res = g_pObj->Append(NULL, AES_BLOCK_SIZE, buff, &buffSize);
        CHECK(res < 0);

        buffSize = AES_BLOCK_SIZE;
        res = g_pObj->Append(buff, 0, buff, &buffSize);
        CHECK(res < 0);

        buffSize = AES_BLOCK_SIZE;
        res = g_pObj->Append(buff, AES_BLOCK_SIZE, NULL, &buffSize);
        CHECK(res < 0);

        res = g_pObj->Append(buff, AES_BLOCK_SIZE, buff, NULL);
        CHECK(res < 0);

        buffSize = 0;
        res = g_pObj->Append(buff, AES_BLOCK_SIZE, buff, &buffSize);
        CHECK(res < 0);

        buffSize = AES_BLOCK_SIZE - 1;
        res = g_pObj->Append(buff, AES_BLOCK_SIZE, buff, &buffSize);
        CHECK(res < 0);

        buffSize = AES_BLOCK_SIZE;
        res = g_pObj->Append(buff, AES_BLOCK_SIZE - 1, buff, &buffSize);
        CHECK(res < 0);
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, fullEncode)
{
    const char** encodedArray = (g_keySize == AES_128_KEY_SIZE)
                                ? g_dataEncoded_128
                                : g_dataEncoded_256;

    for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
    {
        const char* pDataDecoded = g_dataDecoded[i];
        const char* pDataEncoded = encodedArray[i];
        const uint len = strlen(pDataDecoded);
        const uint buffSize = (len + AES_BLOCK_SIZE - 1) & ~(AES_BLOCK_SIZE - 1);

        char* buff = (char*) malloc(buffSize);
        CHECK(buff);

        uint encodedSize = buffSize;
        int res = g_pObj->Encode(pDataDecoded, len, len, buff, &encodedSize);
        bool isPassed = ( res > 0 && encodedSize == buffSize && !memcmp(pDataEncoded, buff, encodedSize) );

        free(buff);

        CHECK(res > 0);
        LONGS_EQUAL(len, res);
        LONGS_EQUAL(encodedSize, buffSize);
        CHECK(isPassed);
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, fullDecode)
{
    const char** encodedArray = (g_keySize == AES_128_KEY_SIZE)
                                ? g_dataEncoded_128
                                : g_dataEncoded_256;

    for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
    {
        const char* pDataDecoded = g_dataDecoded[i];
        const char* pDataEncoded = encodedArray[i];
        const uint len = strlen(pDataDecoded);
        const uint buffSize = (len + AES_BLOCK_SIZE - 1) & ~(AES_BLOCK_SIZE - 1);

        char* buff = (char*) malloc(buffSize);
        CHECK(buff);

        uint decodedSize = buffSize;
        int res = g_pObj->Decode(pDataEncoded, buffSize, buffSize, buff, &decodedSize);
        bool dataCorrect = !memcmp(pDataDecoded, buff, len);

        free(buff);

        CHECK(res > 0);
        LONGS_EQUAL(res, decodedSize);
        LONGS_EQUAL(buffSize, decodedSize);
        CHECK(dataCorrect);
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, partEncode)
{
    const char** encodedArray = (g_keySize == AES_128_KEY_SIZE)
                                ? g_dataEncoded_128
                                : g_dataEncoded_256;
    char buff[AES_BLOCK_SIZE];

    for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
    {
        const char* pDataDecoded = g_dataDecoded[i];
        const char* pDataEncoded = encodedArray[i];
        const char* str = pDataDecoded;
        uint len = strlen(str);

        if (len <= AES_BLOCK_SIZE) continue;

        uint encodedSize = sizeof(buff);
        int res = g_pObj->Encode(str, 1, len, buff, &encodedSize);
        CHECK(!res);

        encodedSize = sizeof(buff);
        res = g_pObj->Encode(str, AES_BLOCK_SIZE - 1, len, buff, &encodedSize);
        CHECK(!res);

        encodedSize = sizeof(buff);
        res = g_pObj->Encode(str, AES_BLOCK_SIZE, len, buff, &encodedSize);
        LONGS_EQUAL(AES_BLOCK_SIZE, res);
        LONGS_EQUAL(AES_BLOCK_SIZE, encodedSize);

        len -= AES_BLOCK_SIZE;
        str += AES_BLOCK_SIZE;

        uint partLen = 0;
        while (len)
        {
            ++partLen;
            CHECK(partLen <= len);

            encodedSize = sizeof(buff);
            res = g_pObj->Append(str, partLen, buff, &encodedSize);
            if (!res)
            {
                CHECK(partLen < AES_BLOCK_SIZE);
                continue;
            }

            CHECK(res > 0);
            LONGS_EQUAL(partLen, res);
            LONGS_EQUAL(AES_BLOCK_SIZE, encodedSize);

            // Check encryption result
            uint offset = str - pDataDecoded;
            bool isCorrect = ( !memcmp(buff, &pDataEncoded[offset], encodedSize) );
            CHECK(isCorrect);

            len -= (partLen < len) ? partLen : len;
            str += partLen;
            partLen = 0;
        } // while (len)
    } // for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(AES_CBC, partDecode)
{
    const char** encodedArray = (g_keySize == AES_128_KEY_SIZE)
        ? g_dataEncoded_128
        : g_dataEncoded_256;
    char buff[AES_BLOCK_SIZE];

    for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
    {
        const char* pDataDecoded = g_dataDecoded[i];
        const char* pDataEncoded = encodedArray[i];
        uint len = strlen(pDataDecoded);
        uint encodedSize = (len + AES_BLOCK_SIZE - 1) & ~(AES_BLOCK_SIZE - 1);

        if (len <= AES_BLOCK_SIZE) continue;

        uint decodedSize = sizeof(buff);
        int res = g_pObj->Decode(pDataEncoded, 1, encodedSize, buff, &decodedSize);
        CHECK(!res);

        decodedSize = sizeof(buff);
        res = g_pObj->Decode(pDataEncoded, AES_BLOCK_SIZE - 1, encodedSize, buff, &decodedSize);
        CHECK(!res);

        decodedSize = sizeof(buff);
        res = g_pObj->Decode(pDataEncoded, AES_BLOCK_SIZE, encodedSize, buff, &decodedSize);
        LONGS_EQUAL(AES_BLOCK_SIZE, res);
        LONGS_EQUAL(AES_BLOCK_SIZE, decodedSize);
        CHECK( !memcmp(buff, pDataDecoded, decodedSize) );

        len -= AES_BLOCK_SIZE;
        pDataEncoded += AES_BLOCK_SIZE;
        pDataDecoded += AES_BLOCK_SIZE;

        uint partLen = 0;
        while (len)
        {
            ++partLen;
            CHECK(partLen <= AES_BLOCK_SIZE);

            decodedSize = sizeof(buff);
            res = g_pObj->Append(pDataEncoded, partLen, buff, &decodedSize);
            if (!res)
            {
                CHECK(partLen < AES_BLOCK_SIZE);
                continue;
            }

            CHECK(res > 0);
            LONGS_EQUAL(partLen, res);
            LONGS_EQUAL(AES_BLOCK_SIZE, decodedSize);

            // Check decryption result
            if (decodedSize > len) decodedSize = len;

            bool isCorrect = ( !memcmp(buff, pDataDecoded, decodedSize) );
            CHECK(isCorrect);

            len -= decodedSize;
            pDataEncoded += partLen;
            pDataDecoded += partLen;
            partLen = 0;
        } // while (len)
    } // for (uint i = 0; i < ARR_COUNT(g_dataDecoded); ++i)
}
