//*****************************************************************************
//
// @brief   SHA256 hash algorithm tests
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "unity.h"
#include "crypt/sha256.h"
#include "misc/macros.h"
#include <string.h>
#include <new>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    TEST_GROUP(SHA256);

    // Testing object
    static IHash* g_pObj = NULL;

    // Test data
    static const char* g_data[] =
    {
        "0123456789",
        "abcdefghijklmnopqrstuvwxyz",
        "0123456789abcdefghijklmnopqrstuvwxyz",
        "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789",
        "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
        "0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz"
    };

    // Test results
    static const char g_results[ARR_COUNT(g_data)][SHA256_DIGEST_LENGTH] =
    {
        {0x84,0xd8,0x98,0x77,0xf0,0xd4,0x04,0x1e,0xfb,0x6b,0xf9,0x1a,0x16,0xf0,0x24,0x8f,0x2f,0xd5,0x73,0xe6,0xaf,0x05,0xc1,0x9f,0x96,0xbe,0xdb,0x9f,0x88,0x2f,0x78,0x82},
        {0x71,0xc4,0x80,0xdf,0x93,0xd6,0xae,0x2f,0x1e,0xfa,0xd1,0x44,0x7c,0x66,0xc9,0x52,0x5e,0x31,0x62,0x18,0xcf,0x51,0xfc,0x8d,0x9e,0xd8,0x32,0xf2,0xda,0xf1,0x8b,0x73},
        {0x74,0xe7,0xe5,0xbb,0x9d,0x22,0xd6,0xdb,0x26,0xbf,0x76,0x94,0x6d,0x40,0xff,0xf3,0xea,0x9f,0x03,0x46,0xb8,0x84,0xfd,0x06,0x94,0x92,0x0f,0xcc,0xfa,0xd1,0x5e,0x33},
        {0x42,0x6f,0x26,0xda,0x92,0x89,0x27,0xa3,0x52,0x0b,0x61,0xad,0xe6,0x20,0xdc,0x7c,0x69,0xed,0x4d,0x31,0x54,0x25,0x92,0x9f,0xa0,0x4d,0x9a,0x99,0x3a,0x22,0xa0,0xf3},
        {0x06,0xf9,0xb1,0xa7,0xac,0x97,0xbc,0x8e,0x6a,0x83,0x5c,0x08,0x98,0x6f,0xe5,0x38,0xf0,0x47,0x8b,0x03,0x82,0x6e,0xfb,0x4e,0xed,0x35,0xdc,0x51,0x7b,0x43,0x3b,0x8a},
        {0x58,0x32,0x48,0x29,0x9a,0xe1,0xf3,0x02,0x3f,0x0c,0x31,0x24,0x1f,0xc2,0x92,0xee,0x3d,0x73,0xa6,0x3d,0xa3,0x03,0xca,0x77,0x95,0xfa,0x70,0xc8,0x9a,0xcb,0xa5,0xb5}
    };


/**
  * @brief  Test group runner
  * @param  Test group name
  */
TEST_GROUP_RUNNER(SHA256)
{
    RUN_TEST_CASE(SHA256, wrongParamsCalc);
    RUN_TEST_CASE(SHA256, wrongParamsAppend);
    RUN_TEST_CASE(SHA256, wrongParamsGetResult);
    RUN_TEST_CASE(SHA256, CalculateFull);
    RUN_TEST_CASE(SHA256, CalculatePartial);
}


/**
  * @brief  Test initialization (for each test)
  * @param  Test group name
  */
TEST_SETUP(SHA256)
{
    g_pObj = new CSha256();
    CHECK(g_pObj != NULL);
    g_pObj->Reset();
}


/**
  * @brief  Test deinitialization (for each test)
  * @param  Test group name
  */
TEST_TEAR_DOWN(SHA256)
{
    if (g_pObj)
    {
        delete g_pObj;
        g_pObj = NULL;
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SHA256, wrongParamsCalc)
{
    int result = g_pObj->Calculate(NULL, 1, 1);
    CHECK(result < 0);

    result = g_pObj->Calculate(g_data[0], 0, 0);
    CHECK(result < 0);

    result = g_pObj->Calculate(g_data[0], 1, 0);
    CHECK(result < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SHA256, wrongParamsAppend)
{
    int result = g_pObj->Append(NULL, 1);
    CHECK(result < 0);

    result = g_pObj->Append(g_data[0], 0);
    CHECK(result < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SHA256, wrongParamsGetResult)
{
    int result = g_pObj->GetResult(NULL, 1);
    CHECK(result < 0);

    char buff[1];
    result = g_pObj->GetResult(buff, 0);
    CHECK(result < 0);
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SHA256, CalculateFull)
{
    char hash[SHA256_DIGEST_LENGTH];

    for (uint i = 0; i < ARR_COUNT(g_data); ++i)
    {
        memset( hash, 0x00, sizeof(hash) );
        g_pObj->Reset();

        const char* str = g_data[i];
        uint len = strlen(str);
        int result = g_pObj->Calculate(str, len, len);
        LONGS_EQUAL(len, result);

        result = g_pObj->GetResult( hash, sizeof(hash) );
        LONGS_EQUAL(sizeof(hash), result);

        bool isCorrect = ( !memcmp(hash, g_results[i], result) );
        CHECK(isCorrect);
    }
}


/**
  * @brief  Test
  * @param  Test group name
  * @param  Test name
  */
TEST(SHA256, CalculatePartial)
{
    char hash[SHA256_DIGEST_LENGTH];

    for (uint i = 0; i < ARR_COUNT(g_data); ++i)
    {
        memset( hash, 0x00, sizeof(hash) );
        g_pObj->Reset();

        const char* str = g_data[i];
        uint totalLen = strlen(str);

        // Skip data less than one block (only partial calculation)
        if (totalLen <= SHA256_BLOCK_SIZE) continue;

        int result = g_pObj->Calculate(str, 1, totalLen);
        CHECK(!result);

        result = g_pObj->Calculate(str, SHA256_BLOCK_SIZE - 1, totalLen);
        CHECK(!result);

        result = g_pObj->Calculate(str, SHA256_BLOCK_SIZE, totalLen);
        LONGS_EQUAL(SHA256_BLOCK_SIZE, result);

        totalLen -= SHA256_BLOCK_SIZE;
        str += SHA256_BLOCK_SIZE;

        uint partLen = 0;
        while (totalLen)
        {
            ++partLen;
            CHECK(partLen <= totalLen);

            result = g_pObj->Append(str, partLen);
            if (!result)
            {
                CHECK(partLen < SHA256_BLOCK_SIZE);
                continue;
            }

            CHECK(result > 0);
            LONGS_EQUAL(partLen, result);

            totalLen -= partLen;
            str += partLen;
            partLen = 0;

            // Check calculation result
            if (!totalLen)
            {
                result = g_pObj->GetResult( hash, sizeof(hash) );
                LONGS_EQUAL(sizeof(hash), result);

                bool isCorrect = ( !memcmp(hash, g_results[i], result) );
                CHECK(isCorrect);
                break;
            }
        } // while (totalLen)
    } // for (uint i = 0; i < ARR_COUNT(g_data); ++i)
}
