//*****************************************************************************
//
// @brief   Block device mock
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "dev/block.h"


class CMockDevBlock : public CDevBlock
{
    public:
        CMockDevBlock (void* buff, uint blockSize, uint count);

        int     Open (void);
        int     Close (void);
        int     Configure (const void* params);

        int     Read (void* buff, uint blockIndex, uint count);
        int     Write (const void* data, uint blockIndex, uint count);
        int     Erase (uint blockIndex, uint count);

        uint    BlockSize (void);
        uint    BlocksCount (void);

    private:
        const uint  m_blockSize;
        const uint  m_blocksCount;
        char*       m_buff;
};
