//*****************************************************************************
//
// @brief   Block device mock
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "mocks/devblock/mock_devblock.h"
#include "misc/macros.h"
#include <string.h>


/**
  * @brief  Constructor
  * @param  buff: Memory is used for storage
  * @param  blockSize: Block size
  * @param  count: Number of blocks
  */
CMockDevBlock::CMockDevBlock (void* buff, uint blockSize, uint count) :
    CDevBlock(),
    m_blockSize(blockSize),
    m_blocksCount(count),
    m_buff( (char*)buff )
{
    assert(!m_buff);
    assert(!m_blockSize);
    assert(!m_blocksCount);
}


/**
  * @brief  Open device
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Open (void)
{
    return Erase(0, m_blocksCount);
}


/**
  * @brief  Close device
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Close (void)
{
    return DEV_OK;
}


/**
  * @brief  Set device configuration
  * @param  params: Configuration
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Configure (const void* params)
{
    UNUSED_VAR(params);

    return DEV_OK;
}


/**
  * @brief  Read block
  * @param  buff: Buffer for saved data
  * @param  blockIndex: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Read (void* buff, uint blockIndex, uint count)
{
    if ( (blockIndex + count) > m_blocksCount ) return DEV_ERR;

    uint32 addr = blockIndex * m_blockSize;
    uint size = count * m_blockSize;

    memcpy(buff, m_buff + addr, size);

    return DEV_OK;
}


/**
  * @brief  Write block
  * @param  data: Written data
  * @param  blockIndex: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Write (const void* data, uint blockIndex, uint count)
{
    if ( (blockIndex + count) > m_blocksCount ) return DEV_ERR;

    uint32 addr = blockIndex * m_blockSize;
    uint32 size = m_blockSize * count;

    memcpy(m_buff + addr, data, size);

    return DEV_OK;
}


/**
  * @brief  Block erase
  * @param  blockIndex: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CMockDevBlock::Erase (uint blockIndex, uint count)
{
    if ( (blockIndex + count) > m_blocksCount ) return DEV_ERR;

    uint32 addr = blockIndex * m_blockSize;
    uint32 size = m_blockSize * count;
    memset(m_buff + addr, 0xff, size);

    return DEV_OK;
}


/**
  * @brief  Get block size
  * @return Block size (bytes)
  */
uint CMockDevBlock::BlockSize (void)
{
    return m_blockSize;
}


/**
  * @brief  Get count of blocks
  * @return Count of blocks in storage
  */
uint CMockDevBlock::BlocksCount (void)
{
    return m_blocksCount;
}
