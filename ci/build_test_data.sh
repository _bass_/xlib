#!/usr/bin/env bash
#

SCRIPT=`realpath $0`
SCRIPT_PATH=`dirname $SCRIPT`
TOOLS_PATH="${SCRIPT_PATH}/../tools"


#
# Firmware update packets generation
#
DATA_PATH="${SCRIPT_PATH}/test_data"
mkdir -p $DATA_PATH

# Encryption key and firmware data generation
head -c 16 /dev/urandom > ${DATA_PATH}/key.bin
head -c 123 /dev/urandom > ${DATA_PATH}/fw.bin
# Firmware data into update package conversion
python3 ${TOOLS_PATH}/fwupgrade/fwupgrade.py pack \
--type_enc aes128-cbc --type_tag sha256 --type_sign sha256-aes128cbc \
-v 0.0.1 --id 0 \
--ekey "${DATA_PATH}/key.bin" --skey "${DATA_PATH}/key.bin" \
"${DATA_PATH}/fw.bin" "${DATA_PATH}/fw_update.bin"
