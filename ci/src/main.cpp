//*****************************************************************************
//
// @brief   Main file of project
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "kernel/kernel.h"


/**
  * @brief  Entry point
  * @param  None
  * @return Error code (not used)
  */
int main (void)
{
    Kernel::Start();
    return 0;
}
