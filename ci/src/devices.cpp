//*****************************************************************************
//
// @brief   Device objects
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "dev/device.h"
#include "misc/macros.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// DevStdIO
// ----------------------------------------------------------------------------

    #include "arch/cpu/x86/devstdio.h"

    static CDevStdio g_devStdio;
    DEV_REGISTER(DEV_NAME_USART, 0, g_devStdio);


// ----------------------------------------------------------------------------
// stdio
// ----------------------------------------------------------------------------

    #include "kernel/kstdio.h"
    
    static KStream stream =
    {
        &g_devStdio,
        NULL,
        {0}
    };
    
    KStream* kstdout = &stream;
    KStream* kstdin = &stream;
