//*****************************************************************************
//
// @brief   Configuration parameters
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "arch/cpu.h"


// ----------------------------------------------------------------------------
// System parameters
// ----------------------------------------------------------------------------

    // System timer frequency
    #define CFG_SYSTMR_FREQ                 100

    // CPU load functionality activation
//    #define CFG_KERNEL_CPU_LOAD_PRESENT

    // Debug thread stack size
//    #define CFG_KDEBUG_STACK_SIZE           768

    // Fill the rest of the data in encryption block by zeros (only for testing, to achieve determined result)
    #define CFG_AES_ENABLE_BLOCK_ZERO_FILL
