//*****************************************************************************
//
// @brief   Hashing algorithm SHA-512 implementation
//          Based on https://tools.ietf.org/html/rfc4634
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "crypt/sha512.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define STATE_DATA_LEN                      8
    #define INTER_DATA_LEN                      80

    // Macros of block convertions
    #define ROTLEFT(a,b)                        ( ((uint64)(a) << (b)) | ((uint64)(a) >> (64 - (b))) )
    #define ROTRIGHT(a,b)                       ( ((uint64)(a) >> (b)) | ((uint64)(a) << (64 - (b))) )
    #define CH(x,y,z)                           ( ((uint64)(x) & (uint64)(y)) ^ (~((uint64)(x)) & (uint64)(z)) )
    #define MAJ(x,y,z)                          ( ((uint64)(x) & (uint64)(y)) ^ ((uint64)(x) & (uint64)(z)) ^ ((uint64)(y) & (uint64)(z)) )
    #define BSIG0(x)                            ( ROTRIGHT(x,28) ^ ROTRIGHT(x,34) ^ ROTRIGHT(x,39) )
    #define BSIG1(x)                            ( ROTRIGHT(x,14) ^ ROTRIGHT(x,18) ^ ROTRIGHT(x,41) )
    #define SSIG0(x)                            ( ROTRIGHT(x,1) ^ ROTRIGHT(x,8) ^ ((uint64)(x) >> 7) )
    #define SSIG1(x)                            ( ROTRIGHT(x,19) ^ ROTRIGHT(x,61) ^ ((uint64)(x) >> 6) )


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // First 64 bits of fractional part of cubic roots for first 80 prime numbers
    static const uint64 K[] =
    {
        0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
        0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
        0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
        0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
        0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
        0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
        0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
        0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
        0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
        0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
        0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
        0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
        0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
        0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
        0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
        0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
        0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
        0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
        0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
        0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
    };


/**
  * @brief  Constructor
  * @param  None
  */
CSha512::CSha512 () :
    IHash(),
    m_totalSize(0),
    m_processedSize(0)
{
    m_pState = (uint64*) malloc(sizeof(uint64) * STATE_DATA_LEN);
    assert(!m_pState);

    m_pW = (uint64*) malloc(sizeof(uint64) * INTER_DATA_LEN);
    assert(!m_pW);
}


/**
  * @brief  Destructor
  * @param  None
  */
CSha512::~CSha512 ()
{
    if (m_pW)       free(m_pW);
    if (m_pState)   free(m_pState);
}


/**
  * @brief  Reset state
  * @param  None
  * @retval None
  */
void CSha512::Reset (void)
{
    if (m_pState)
    {
        memset( m_pState, 0x00, STATE_DATA_LEN * sizeof(uint64) );
    }

    if (m_pW)
    {
        memset( m_pW, 0x00, INTER_DATA_LEN * sizeof(uint64) );
    }

    m_totalSize = 0;
    m_processedSize = 0;
}


/**
  * @brief  Hash calculation or start of partial calculation
  * @param  data: Data for hashing
  * @param  size: Data size
  * @param  totalSize: Total data size
  * @return Size of processed data or negative error code
  */
int CSha512::Calculate (const void* data, uint size, uint totalSize)
{
    if (!data || !size) return -1;
    if (totalSize < size) return -2;
    if (!m_pState || !m_pW) return -3;

    // Service variables initialization
    m_pState[0] = 0x6a09e667f3bcc908;
    m_pState[1] = 0xbb67ae8584caa73b;
    m_pState[2] = 0x3c6ef372fe94f82b;
    m_pState[3] = 0xa54ff53a5f1d36f1;
    m_pState[4] = 0x510e527fade682d1;
    m_pState[5] = 0x9b05688c2b3e6c1f;
    m_pState[6] = 0x1f83d9abfb41bd6b;
    m_pState[7] = 0x5be0cd19137e2179;

    m_totalSize = totalSize;
    m_processedSize = 0;

    return Append(data, size);
}


/**
  * @brief  Apped data for hash calculation (continue calculation)
  * @param  data: Data for append
  * @param  size: Data size
  * @return Size of processed data or negative error code
  */
int CSha512::Append (const void* data, uint size)
{
    if (!data || !size) return -1;
    if ( m_totalSize <= m_processedSize || size > (m_totalSize - m_processedSize) ) return -2;
    if (!m_pState || !m_pW) return -3;

    // Full blocks processing
    uint blockCount = size / SHA512_BLOCK_SIZE;
    const char* ptr = (const char*) data;

    for (uint i = 0; i < blockCount; i++, ptr += SHA512_BLOCK_SIZE)
    {
        update(ptr);
    }

    int result;
    if ( size == (m_totalSize - m_processedSize) )
    {
        // Finishing the calculation
        final(ptr, m_totalSize);
        m_processedSize = m_totalSize;
        result = (int)size;
    }
    else
    {
        // Saving amount of remaining data for futher processing 
        result = ptr - (const char*)data;
        m_processedSize += (uint)result;
    }

    return result;
}


/**
  * @brief  Read hashing result
  * @param  buff: Buffer for result saving
  * @param  size: Buffer size
  * @return Data size written into the buffer or negative error code
  */
int CSha512::GetResult (void* buff, uint size)
{
    if (!buff || size < SHA512_DIGEST_LENGTH) return -1;
    if (!m_pState) return -2;
    if (!m_processedSize || m_processedSize != m_totalSize) return -3;

    // Fill the result, taking into account that calculations are made in big endian format
    char* hash = (char*) buff;

    for ( uint i = 0; i < SHA512_DIGEST_LENGTH / sizeof(uint64); ++i, hash += sizeof(uint64) )
    {
        const uint64 v = m_pState[i];
        hash[0] = (v >> 56) & 0xff;
        hash[1] = (v >> 48) & 0xff;
        hash[2] = (v >> 40) & 0xff;
        hash[3] = (v >> 32) & 0xff;
        hash[4] = (v >> 24) & 0xff;
        hash[5] = (v >> 16) & 0xff;
        hash[6] = (v >> 8) & 0xff;
        hash[7] = (v >> 0) & 0xff;
    }

    return SHA512_DIGEST_LENGTH;
}


/**
  * @brief  Get hash size
  * @return Hash size (bytes) or negative error code
  */
int CSha512::GetSize (void)
{
    return SHA512_DIGEST_LENGTH;
}


/**
  * @brief  Intermediate step of hash calculation
  * @param  data: Processed data block
  */
void CSha512::update (const char* data)
{
    // Conversion into big endian format
    for ( uint i = 0; i < SHA512_BLOCK_SIZE / sizeof(uint64); ++i, data += sizeof(uint64) )
    {
        m_pW[i] = ((uint64)data[0] << 56) | ((uint64)data[1] << 48) | ((uint64)data[2] << 40) | ((uint64)data[3] << 32)
                | ((uint64)data[4] << 24) | ((uint64)data[5] << 16) | ((uint64)data[6] << 8) | (data[7]);
    }
    // Additional data generation
    for (uint i = SHA512_BLOCK_SIZE / sizeof(uint64); i < 80; ++i)
    {
        m_pW[i] = SSIG1(m_pW[i - 2]) + m_pW[i - 7] + SSIG0(m_pW[i - 15]) + m_pW[i - 16];
    }

    // Intermediate hash value calculation (80 rounds)
    uint64 a = m_pState[0];
    uint64 b = m_pState[1];
    uint64 c = m_pState[2];
    uint64 d = m_pState[3];
    uint64 e = m_pState[4];
    uint64 f = m_pState[5];
    uint64 g = m_pState[6];
    uint64 h = m_pState[7];

    for (uint i = 0; i < 80; ++i)
    {
        uint64 t1 = h + BSIG1(e) + CH(e,f,g) + K[i] + m_pW[i];
        uint64 t2 = BSIG0(a) + MAJ(a,b,c);

        h = g;
        g = f;
        f = e;
        e = d + t1;
        d = c;
        c = b;
        b = a;
        a = t1 + t2;
    }

    m_pState[0] += a;
    m_pState[1] += b;
    m_pState[2] += c;
    m_pState[3] += d;
    m_pState[4] += e;
    m_pState[5] += f;
    m_pState[6] += g;
    m_pState[7] += h;
}


/**
  * @brief  Final step of hash calculation
  * @param  data: Processed data block (remaining data)
  * @param  totalSize: Total size of data
  */
void CSha512::final (const char* data, uint totalSize)
{
    uint restSize = totalSize % SHA512_BLOCK_SIZE;
    memcpy(m_pW, data, restSize);
    char* m = (char*) m_pW;

    // Append data into the rest of the block
    uint index = restSize;
    m[index++] = 0x80;

    const uint lenFieldSize = 16;
    if ( restSize < (SHA512_BLOCK_SIZE - lenFieldSize) )
    {
        while ( index < (SHA512_BLOCK_SIZE - lenFieldSize) ) m[index++] = 0x00;
    }
    else
    {
        while (index < SHA512_BLOCK_SIZE) m[index++] = 0x00;

        update(m);
        memset(m_pW, 0, SHA512_BLOCK_SIZE - lenFieldSize);
    }

    // Adding size of original data (in bits)
    uint64 bitlen = totalSize * 8;
    m[SHA512_BLOCK_SIZE - 1] = bitlen;
    m[SHA512_BLOCK_SIZE - 2] = bitlen >> 8;
    m[SHA512_BLOCK_SIZE - 3] = bitlen >> 16;
    m[SHA512_BLOCK_SIZE - 4] = bitlen >> 24;
    m[SHA512_BLOCK_SIZE - 5] = bitlen >> 32;
    m[SHA512_BLOCK_SIZE - 6] = bitlen >> 40;
    m[SHA512_BLOCK_SIZE - 7] = bitlen >> 48;
    m[SHA512_BLOCK_SIZE - 8] = bitlen >> 56;
    memset(&m[SHA512_BLOCK_SIZE - lenFieldSize], 0, lenFieldSize - 8);

    update(m);
}
