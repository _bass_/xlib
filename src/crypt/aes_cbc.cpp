//*****************************************************************************
//
// @brief   AES encryption (128/256, CBC mode)
//          Based on https://github.com/pfalcon/aes256_128
//          and http://avrcryptolib.das-labor.org/trac/wiki/AES
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "crypt/aes_cbc.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "def_board.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Number of convertion rounds
    #define AES_128_ROUND_COUNT         10
    #define AES_256_ROUND_COUNT         14

    #define AES_XTIME(x)                ( ((x) & 0x80) ? (((x) << 1) ^ 0x1b) : ((x) << 1) )
    #define AES_F(x)                    ( ((x) << 1) ^ ((((x) >> 7) & 1) * 0x1b) )
    #define AES_FD(x)                   ( ((x) >> 1) ^ (((x) & 1) ? 0x8d : 0) )

    #define AES_MODE_NONE               0
    #define AES_MODE_ENCODE             1
    #define AES_MODE_DECODE             (-1)


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Direct conversion table
    static const unsigned char sbox[256] =
    {
        0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5,
        0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
        0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0,
        0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
        0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC,
        0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
        0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A,
        0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
        0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0,
        0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
        0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B,
        0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
        0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85,
        0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
        0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5,
        0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
        0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17,
        0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
        0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88,
        0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
        0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C,
        0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
        0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9,
        0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
        0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6,
        0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
        0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E,
        0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
        0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94,
        0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
        0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68,
        0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
    };

    // Revers conversion table
    static const unsigned char sboxInv[256] =
    {
        0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38,
        0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
        0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87,
        0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
        0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D,
        0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
        0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2,
        0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
        0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16,
        0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
        0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA,
        0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
        0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A,
        0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
        0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02,
        0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
        0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA,
        0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
        0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85,
        0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
        0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89,
        0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
        0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20,
        0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
        0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31,
        0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
        0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D,
        0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
        0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0,
        0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
        0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26,
        0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
    };

    // Constants for round keys generation
    static const unsigned char rcon[] = 
    {
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
        0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------
    
    static void xorBlocks (void* data, const void* key);
    static void subBytes (char* data);
    static void shiftRows (char* data);
    static void mixColumns (char* data);
    static void addRoundKey (char* data, char* key);
    static void expandKey (char* key, uint keySize, uchar* rc);
    static void subBytesInv (char* data);
    static void shiftRowsInv (char* data);
    static void mixColumnsInv (char* data);
    static void expandKeyInv (char* key, uint keySize, uchar* rc);

    static uint fillBlock (const void* data, uint size, void* block);


/**
  * @brief  Constructor
  * @param  keySize: Encryption key size (bytes). AES128 = 16, AES256 = 32
  */
CAesCbc::CAesCbc (uint keySize) :
    ICrypt(),
    m_keySize(keySize),
    m_pMeta(NULL)
{
    assert(m_keySize != AES_128_KEY_SIZE && m_keySize != AES_256_KEY_SIZE);
    
    m_key = malloc(m_keySize);
    assert(!m_key);
    
    memset( m_key, 0, m_keySize );
    memset( m_iv, 0, sizeof(m_iv) );
}


/**
  * @brief  Destructor
  * @param  None
  */
CAesCbc::~CAesCbc ()
{
    if (m_key)
    {
        memset(m_key, 0, m_keySize);
        free(m_key);
    }

    if (m_pMeta)
    {
        memset( m_pMeta, 0x00, sizeof(TMeta) );
        free(m_pMeta);
    }
}


/**
  * @brief  Set encryption parameters (keys, auxiliary data...)
  * @param  params: Encryption parameters (see TParams)
  * @return 0 or negative error code
  */
int CAesCbc::SetParams (const void* params)
{
    const TParams* opt = (const TParams*) params;

    if (!opt) return -1;
    if (!opt->pKey || !opt->pIv) return -2;

    memcpy(m_key, opt->pKey, m_keySize);
    memcpy( m_iv, opt->pIv, sizeof(m_iv) );

    if (m_pMeta)
    {
        memset( m_pMeta, 0x00, sizeof(TMeta) );
        free(m_pMeta);
        m_pMeta = NULL;
    }

    return 0;
}


/**
  * @brief  Data encryption (start of partial encryption)
  * @param  data: Data (plaintext)
  * @param  size: Data size
  * @param  totalSize: Total data size
  * @param  buff: Buffer for encrypted data (ciphertext)
  * @param  pBuffSize: Buffer size (value will be replaced with written data size)
  * @return Size of processed data or negative error code
  */
int CAesCbc::Encode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize)
{
    if (!data || !size) return -1;
    if (totalSize < size) return -2;
    if ( !buff || !pBuffSize || *pBuffSize < ((size + AES_BLOCK_SIZE - 1) & ~(AES_BLOCK_SIZE - 1)) ) return -3;
    if (m_pMeta) return -4;

    // Minimum data size is one block
    if (totalSize > size && size < AES_BLOCK_SIZE) return 0;

    m_pMeta = (TMeta*) malloc( sizeof(TMeta) );
    if (!m_pMeta) return -10;

    m_pMeta->mode = AES_MODE_ENCODE;
    m_pMeta->totalSize = totalSize;
    m_pMeta->processedSize = 0;
    memcpy( m_pMeta->prevBlock[0], m_iv, sizeof(m_iv) );

    return Append(data, size, buff, pBuffSize);
}


/**
  * @brief  Data decryption (start of partial decryption)
  * @param  data: Encrypted data (ciphertext)
  * @param  size: Encrypted data size
  * @param  totalSize: Total encrypted data size
  * @param  buff: Buffer for decrypted data (plaintext)
  * @param  pBuffSize: Buffer size (value will be replaced with written data size)
  * @return Size of processed data or negative error code
  */
int CAesCbc::Decode (const void* data, uint size, uint totalSize, void* buff, uint* pBuffSize)
{
    if (!data || !size) return -1;
    if (totalSize < size) return -2;
    if ( totalSize & (AES_BLOCK_SIZE - 1) ) return -3;
    if (!buff || !pBuffSize || *pBuffSize < size) return -4;
    if (m_pMeta) return -5;

    // Minimum data size is one block
    if (totalSize > size && size < AES_BLOCK_SIZE) return 0;

    m_pMeta = (TMeta*) malloc( sizeof(TMeta) );
    if (!m_pMeta) return -10;

    m_pMeta->mode = AES_MODE_DECODE;
    m_pMeta->totalSize = totalSize;
    m_pMeta->processedSize = 0;
    memcpy( m_pMeta->prevBlock[0], m_iv, sizeof(m_iv) );

    return Append(data, size, buff, pBuffSize);
}


/**
  * @brief  Append data part for encryption/decryption (partial encryption/decryption)
  * @param  data: Source data
  * @param  size: Data size
  * @param  buff: Buffer for encrypted/decrypted data
  * @param  pBuffSize: Buffer size (value will be replaced with written data size)
  * @return Size of processed data or negative error code
  */
int CAesCbc::Append (const void* data, uint size, void* buff, uint* pBuffSize)
{
    if (!data || !size) return -1;
    if ( !buff || !pBuffSize || *pBuffSize < ((size + AES_BLOCK_SIZE - 1) & ~(AES_BLOCK_SIZE - 1)) ) return -2;
    if (!m_pMeta || m_pMeta->mode == AES_MODE_NONE) return -3;

    // Minimum data size is one block
    const uint restSize = m_pMeta->totalSize - m_pMeta->processedSize;
    if (size < restSize && size < AES_BLOCK_SIZE) return 0;

    const char* pData = (const char*) data;
    char* pBlock = (char*) buff;
    char* pPrevBlock = m_pMeta->prevBlock[0];

    uint processedSize = 0;
    for (; processedSize < size; pData += AES_BLOCK_SIZE, pBlock += AES_BLOCK_SIZE)
    {
        uint partSize = size - processedSize;
        if (partSize < AES_BLOCK_SIZE)
        {
            if (
                m_pMeta->mode == AES_MODE_ENCODE &&
                (partSize + processedSize + m_pMeta->processedSize) < m_pMeta->totalSize
                ) break;
            if (m_pMeta->mode == AES_MODE_DECODE) break;
        }

        partSize = fillBlock(pData, partSize, pBlock);

        if (m_pMeta->mode == AES_MODE_ENCODE)
        {
            encodeBlock(m_pMeta->keyBuff, pBlock, pPrevBlock);
            pPrevBlock = pBlock;
        }
        else
        {
            // Save data of current block
            char* ptr = (pPrevBlock == m_pMeta->prevBlock[0]) ? m_pMeta->prevBlock[1] : m_pMeta->prevBlock[0];
            memcpy(ptr, pBlock, AES_BLOCK_SIZE);

            decodeBlock(m_pMeta->keyBuff, pBlock, pPrevBlock);
            pPrevBlock = ptr;
        }

        processedSize += partSize;
    }

    *pBuffSize = pBlock - (char*) buff;

    m_pMeta->processedSize += processedSize;
    if (m_pMeta->processedSize == m_pMeta->totalSize)
    {
        memset( m_pMeta, 0x00, sizeof(TMeta) );
        free(m_pMeta);
        m_pMeta = NULL;
    }
    else
    {
        // Save previous block
        if (pPrevBlock != m_pMeta->prevBlock[0])
        {
            memcpy(m_pMeta->prevBlock[0], pPrevBlock, AES_BLOCK_SIZE);
        }
    }

    return (int)processedSize;
}


/**
  * @brief  Block encryption
  * @param  keyBuff: Encryption key buffer (is changing during encryption)
  * @param  pBlock: Plaintext data, will be replaced with encrypted data
  * @param  pPrevBlock: Previous data block
  */
void CAesCbc::encodeBlock (char* keyBuff, char* pBlock, const char* pPrevBlock)
{
    // XOR with previous block or with IV
    xorBlocks(pBlock, pPrevBlock);

    // Preparation for new block encryption
    memcpy(keyBuff, m_key, m_keySize);
    addRoundKey(pBlock, keyBuff);

    uchar rconVal = 1;
    const uint maxRoundCount = (m_keySize == AES_256_KEY_SIZE) ? AES_256_ROUND_COUNT : AES_128_ROUND_COUNT;

    for (uint round = 1; round <= maxRoundCount; ++round)
    {
        subBytes(pBlock);
        shiftRows(pBlock);
        // mixColumn isn't used in last round
        if (round != maxRoundCount)
        {
            mixColumns(pBlock);
        }

        if ( m_keySize != AES_128_KEY_SIZE && (round & 1) )
        {
            addRoundKey(pBlock, &keyBuff[16]);
        }
        else
        {
            expandKey(keyBuff, m_keySize, &rconVal);
            addRoundKey(pBlock, keyBuff);
        }
    } // for (uint round = 1; round <= maxRoundCount; round++)
}


/**
  * @brief  Block decryption
  * @param  keyBuff: Encryption key buffer (is changing during encryption)
  * @param  pBlock: Ciphertext data, will be replaced with decrypted data
  * @param  pPrevBlock: Previous data block
  */
void CAesCbc::decodeBlock (char* keyBuff, char* pBlock, const char* pPrevBlock)
{
    // Decryption key preparation
    uchar rconVal = 1;
    // Conversion rounds count
    const uint maxRoundCount = (m_keySize == AES_256_KEY_SIZE) ? AES_256_ROUND_COUNT : AES_128_ROUND_COUNT;
    // Number of decryption rounds (for making initial key value)
    const uint decodeKeyRounds = (maxRoundCount * AES_BLOCK_SIZE) / m_keySize;

    memcpy(keyBuff, m_key, m_keySize);

    for (uint i = decodeKeyRounds; i; i--)
    {
        expandKey(keyBuff, m_keySize, &rconVal);
    }

    addRoundKey(pBlock, keyBuff);
    rconVal = rcon[decodeKeyRounds];

    for (int round = maxRoundCount - 1; round >= 0; --round)
    {
        shiftRowsInv(pBlock);
        subBytesInv(pBlock);

        if (m_keySize == AES_128_KEY_SIZE)
        {
            expandKeyInv(keyBuff, m_keySize, &rconVal);
            addRoundKey(pBlock, keyBuff);
        }
        else
        {
            if (round & 1)
            {
                expandKeyInv(keyBuff, m_keySize, &rconVal);
                addRoundKey(pBlock, &keyBuff[16]);
            }
            else
            {
                addRoundKey(pBlock, keyBuff);
            }
        }

        // mixColumnInv isn't used in last round
        if (round)
        {
            mixColumnsInv(pBlock);
        }
    } // for (int round = maxRoundCount - 1; round >= 0; round--)

    // XOR with previous block or with IV
    xorBlocks(pBlock, pPrevBlock);
}


/**
  * @brief  Fill block with source data
  * @param  data: Source data
  * @param  size: Data size
  * @param  block: Block buffer (AES_BLOCK_SIZE)
  * @return Size of processed data
  */
uint fillBlock (const void* data, uint size, void* block)
{
    if (size >= AES_BLOCK_SIZE)
    {
        size = AES_BLOCK_SIZE;
        memcpy(block, data, size);
    }
    else
    {
        memcpy(block, data, size);

        #if defined(CFG_AES_ENABLE_BLOCK_ZERO_FILL)
        memset( (char*)block + size, 0x00, AES_BLOCK_SIZE - size );
        #else
        char* ptr = (char*) block + size;
        for (uint i = AES_BLOCK_SIZE - size; i; --i, ++ptr)
        {
            *ptr = rand();
        }
        #endif
    }

    return size;
}


/**
  * @brief  XOR of two blocks
  * @param  data: Source data, will be replaced with the result
  * @param  key: Key data
  */
void xorBlocks (void* data, const void* key)
{
    uint32* pData = (uint32*) data;
    const uint32* pKey = (const uint32*) key;

    for (uint i = 0; i < AES_BLOCK_SIZE / sizeof(uint32); ++i, ++pData, ++pKey)
    {
        *pData ^= *pKey;
    }
}


/**
  * @brief  Permutation stage
  * @param  data: Data block
  */
void subBytes (char* data)
{
    uint i = AES_BLOCK_SIZE;

    while (i--)
    {
        data[i] = sbox[ (uchar)data[i] ];
    }
}


/**
  * @brief  Permutation stage (inverse)
  * @param  data: Data block
  */
void subBytesInv (char* data)
{
    uint i = AES_BLOCK_SIZE;

    while (i--)
    {
        data[i] = sboxInv[ (uchar)data[i] ];
    }
}


/**
  * @brief  Stage of shifting rows in the block
  * @param  data: Data block
  */
void shiftRows (char* data)
{
    char tmp[4];
    
    // block[AES_BLOCK_SIZE] = 4 x 4
    // Byte-by-byte rows shifting on r bytes, where r is row number (0 row isn't shifted)
    for (uint r = 1; r < 4; r++)
    {
        tmp[0] = data[r + 0];
        tmp[1] = data[r + 4];
        tmp[2] = data[r + 8];
        tmp[3] = data[r + 12];
        data[r + 0] = tmp[(r + 0) & 3];
        data[r + 4] = tmp[(r + 1) & 3];
        data[r + 8] = tmp[(r + 2) & 3];
        data[r + 12] = tmp[(r + 3) & 3];
    }
}


/**
  * @brief  Stage of shifting rows in the block (inverse)
  * @param  data: Data block
  */
void shiftRowsInv (char* data)
{
    char tmp[4];
    
    // block[AES_BLOCK_SIZE] = 4 x 4
    // Byte-by-byte rows shifting on r bytes, where r is row number (0 row isn't shifted)
    for (uint r = 1; r < 4; r++)
    {
        tmp[0] = data[r + 0];
        tmp[1] = data[r + 4];
        tmp[2] = data[r + 8];
        tmp[3] = data[r + 12];
        data[r + 0] = tmp[(4 + 0 - r) & 3];
        data[r + 4] = tmp[(4 + 1 - r) & 3];
        data[r + 8] = tmp[(4 + 2 - r) & 3];
        data[r + 12] = tmp[(4 + 3 - r) & 3];
    }
}


/**
  * @brief  Stage of columns conversion in the block
  * @param  data: Data block
  */
void mixColumns (char* data)
{
    uint8 a, b, c, d, e;

    for (uint i = 0; i < AES_BLOCK_SIZE; i += 4)
    {
        a = data[i];
        b = data[i + 1];
        c = data[i + 2];
        d = data[i + 3];
        e = a ^ b ^ c ^ d;
        data[i] ^= e ^ AES_XTIME(a^b);
        data[i + 1] ^= e ^ AES_XTIME(b^c);
        data[i + 2] ^= e ^ AES_XTIME(c^d);
        data[i + 3] ^= e ^ AES_XTIME(d^a);
    }
}


/**
  * @brief  Stage of columns conversion in the block (inverse)
  * @param  data: Data block
  */
void mixColumnsInv (char* data)
{
    uint8 a, b, c, d, e, x, y, z;

    for (uint i = 0; i < AES_BLOCK_SIZE; i += 4)
    {
        a = data[i];
        b = data[i + 1];
        c = data[i + 2];
        d = data[i + 3];
        e = a ^ b ^ c ^ d;
        z = AES_XTIME(e);
        x = e ^ AES_XTIME( AES_XTIME(z^a^c) );
        y = e ^ AES_XTIME( AES_XTIME(z^b^d) );
        data[i] ^= x ^ AES_XTIME(a^b);
        data[i + 1] ^= y ^ AES_XTIME(b^c);
        data[i + 2] ^= x ^ AES_XTIME(c^d);
        data[i + 3] ^= y ^ AES_XTIME(d^a);
    }
}


/**
  * @brief  Key transformation for current round
  * @param  key: Key data
  * @param  keySize: Key size (AES_xxx_KEY_SIZE)
  * @param  rc: Value from table of key transformation
  */
void expandKey (char* key, uint keySize, uchar* rc)
{
    uchar* k = (uchar*) key;
    uint8 i;

    k[0] ^= sbox[ k[29 - (32 - keySize)] ] ^ (*rc);
    k[1] ^= sbox[ k[30 - (32 - keySize)] ];
    k[2] ^= sbox[ k[31 - (32 - keySize)] ];
    k[3] ^= sbox[ k[28 - (32 - keySize)] ];
    *rc = AES_F(*rc);

    for (i = 4; i < 16; i += 4)
    {
        k[i] ^= k[i - 4];
        k[i + 1] ^= k[i - 3];
        k[i + 2] ^= k[i - 2];
        k[i + 3] ^= k[i - 1];
    }
    
    if (keySize != AES_128_KEY_SIZE)
    {
        k[16] ^= sbox[ k[12] ];
        k[17] ^= sbox[ k[13] ];
        k[18] ^= sbox[ k[14] ];
        k[19] ^= sbox[ k[15] ];
        
        for (i = 20; i < 32; i += 4)
        {
            k[i] ^= k[i - 4];
            k[i + 1] ^= k[i - 3];
            k[i + 2] ^= k[i - 2];
            k[i + 3] ^= k[i - 1];
        }
    }
}


/**
  * @brief  Key transformation for current round (inverse)
  * @param  key: Key data
  * @param  keySize: Key size (AES_xxx_KEY_SIZE)
  * @param  rc: Value from table of key transformation
  */
void expandKeyInv (char* key, uint keySize, uchar* rc)
{
    uchar* k = (uchar*) key;

    if (keySize != AES_128_KEY_SIZE)
    {
        for (uint i = 28; i > 16; i -= 4)
        {
            k[i+0] ^= k[i-4];
            k[i+1] ^= k[i-3];
            k[i+2] ^= k[i-2];
            k[i+3] ^= k[i-1];
        }
        
        k[16] ^= sbox[ k[12] ];
        k[17] ^= sbox[ k[13] ];
        k[18] ^= sbox[ k[14] ];
        k[19] ^= sbox[ k[15] ];
    }

    for (uint i = 12; i > 0; i -= 4)
    {
        k[i+0] ^= k[i-4];
        k[i+1] ^= k[i-3];
        k[i+2] ^= k[i-2];
        k[i+3] ^= k[i-1];
    }

    *rc = AES_FD(*rc);
    k[0] ^= sbox[ k[29 - (32 - keySize)] ] ^ (*rc);
    k[1] ^= sbox[ k[30 - (32 - keySize)] ];
    k[2] ^= sbox[ k[31 - (32 - keySize)] ];
    k[3] ^= sbox[ k[28 - (32 - keySize)] ];
}


/**
  * @brief  Stage of XOR data with round key
  * @param  data: Data block
  * @param  key: Key data
  */
void addRoundKey (char* data, char* key)
{
    xorBlocks(data, key);
}
