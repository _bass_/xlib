//*****************************************************************************
//
// @brief   RSA encyption (lite)
//          Based on https://github.com/mborisov1/rsa_embedded
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "crypt/rsa.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>
#include <stdlib.h>


/**
  * @brief  Constructor
  * @param  keyLen: Key size (bytes)
  */
CRsa::CRsa (uint keyLen):
    m_wordCount( keyLen / sizeof(uint16) )
{
    // Key size must be power of 2
    assert( keyLen & (keyLen - 1) );
}


/**
  * @brief  Encryption
  *         ciphertext = (plaintext ^ e) % n, where e = 65537
  *         (plaintext and n size must be equal to encryption key size)
  * @param  plaintext: Source data, will be replaced with encrypted data
  * @param  size: Data size
  * @param  n: Modulus
  * @return 0 or negative error code
  */
int CRsa::Encode (void* plaintext, uint size, void* n)
{
    int res = prepareData( (char*)plaintext, size );
    if (res < 0) return res;
    
    uint pow = 65537;
    uint powSize = 17;
    return modExp( (uint16*)plaintext, (uint16*)n, (uint16*)&pow, powSize );
}


/**
  * @brief  Decryption
  *         plaintext = (ciphertext ^ d) % n
  *         (size of ciphertext, d and n must be equal to key size)
  * @param  ciphertext: Encrypted data, will be replaced with decrypted
  * @param  n: Modulus
  * @param  d: Private key
  * @return Decrypted data size or negative error code
  */
int CRsa::Decode (void* ciphertext, void* n, void* d)
{
    int res = modExp( (uint16*)ciphertext, (uint16*)n, (uint16*)d, m_wordCount * 2 * 8 );
    if (res < 0) return res;

    // FIXME: Check that there is PKCS #1 v1.5 header
    // FIXME: Find 0x00 after PS block and detect payload size
    // FIXME: Reverse payload and move to the start position

    return 0;
}


/**
  * @brief  Data preparatino for encoding process (according to PKCS #1 v1.5)
  * @param  data: Source data
  * @param  size: Data size
  * @return 0 or negative error code
  */
int CRsa::prepareData (char* data, uint size)
{
    uint keySize = m_wordCount * 2;
    // Maximum payload size = keySize - 11
    if (size > keySize - 11) return -11;
    
    // Encrypted data structure: EM = 0x00 || 0x02 || PS || 0x00 || M
    // M - payload, PS - random number (8 byte minimum)
    
    uint psSize = keySize - size - 3;
    char* ptr = data + size + 1;
    uint32 rnd = 0;
    
    for (uint i = 0; i < psSize; i++, ptr++)
    {
        char byte;
        do
        {
            if (!rnd) rnd = (uint32)rand();
            
            byte = rnd & 0xff;
            rnd >>= 8;
        } while (!byte);
        
        *ptr = byte;
    }
    
    // Make header (reverse order)
    data[keySize - 1] = 0x00;
    data[keySize - 2] = 0x02;
    data[size] = 0x00;
    
    // Reverse the data
    ptr = data + size - 1;
    size /= 2;
    
    for (uint i = 0; i < size; i++, data++, ptr--)
    {
        char byte = *data;
        *data = *ptr;
        *ptr = byte;
    }
    
    return 0;
}


/**
  * @brief  Calculating power of a number modulo
  *         m = (m ^ pow) % n
  * @param  m: Original number, will be replaced with the result
  * @param  n: Modulus
  * @param  pow: Value of power
  * @param  powSize: Power value size (in bits)
  * @return 0 or negative error code
  */
int CRsa::modExp (uint16* m, uint16* n, uint16* pow, uint powSize)
{
    const uint keySize = m_wordCount * 2;
    
    uint16* tmpPow = (uint16*) malloc(keySize);
    if (!tmpPow) return -1;
    
    // Double size buffer allocation for operation result
    uint16* mulRes = (uint16*) malloc(keySize * 2);
    if (!mulRes)
    {
        free(tmpPow);
        return -1;
    }
    
    // Multipliers initialization depends on parity of power number
    memcpy(tmpPow, m, keySize);
    
    if (*pow & 0x0001)
    {
        mul(mulRes, tmpPow, tmpPow);        // tmpPow = tmpPow ^ 2;
        mod(mulRes, n);                     // tmpPow %= n;
        memcpy(tmpPow, mulRes, keySize);
    }
    else
    {
        memset(m, 0, keySize);
        m[0] = 1;
    }

    uint powBit = 1;
    while (1)
    {
        uint powIndex = powBit / 16;        // uint16
        uint powOffset = powBit % 16;
        
        if ( pow[powIndex] & (1 << powOffset) )
        {
            mul(mulRes, m, tmpPow);         // M *= tmpPow;
            mod(mulRes, n);                 // M %= n;
            memcpy(m, mulRes, keySize);
        }
        
        powBit++;
        if (powBit >= powSize) break;
        
        mul(mulRes, tmpPow, tmpPow);        // tmpPow = tmpPow ^ 2;
        mod(mulRes, n);                     // tmpPow %= n;
        memcpy(tmpPow, mulRes, keySize);
    }
    
    free(mulRes);
    free(tmpPow);
    
    return 0;
}


/**
  * @brief  Big numbers multiplication (res = a * b)
  * @param  res: Result of multiplication (double size)
  * @param  a: Multiplier
  * @param  b: Multiplier
  */
void CRsa::mul (uint16* res, uint16* a, uint16* b)
{
    uint64 mcarry = 0;
    uint count = m_wordCount;
    uint8 niter2 = 1; // Inner loop starts with 1 iteration
    uint8 count2;
    const uint16* aa;
    const uint16* bb;
    
    // First outer loop - the LSWs
    do
    {
        count2 = niter2++;
        aa = a;
        bb = b++;
        do
        {
            mcarry += (unsigned)*aa++ * (unsigned)*bb--;
        } while(--count2);
        *res++ = (uint16)(mcarry & 0xFFFF);
        mcarry >>= 16;
    } while(--count);
    
    // Second outer loop - the MSWs
    count = m_wordCount - 1;
    b--;
    niter2--;
    do
    {
        count2 = --niter2;
        aa = ++a;
        bb = b;
        do
        {
            mcarry += (unsigned)*aa++ * (unsigned)*bb--;
        } while(--count2);
        *res++ = (uint16)(mcarry & 0xFFFF);
        mcarry >>= 16;
    } while(--count);
    
    // Store the last carry digit
    *res = (uint16)(mcarry & 0xFFFF);
}


/**
  * @brief  Remainder of the division calculation (a %= n)
  * @param  a: A number, will be replaced with operation result
  * @param  n: Modulus
  */
void CRsa::mod (uint16* a, uint16* n)
{
    uint16* pr = a + m_wordCount;   // Initial partial remainder is the dividend's upper half
    uint count = m_wordCount + 1;
    uint8 flag = 0;                 // If PR has N+1 significant words, this flag = 1
    uint32 pq;                      // Partial quotient
    uint32 carry;

    // Loop through the lower half of the dividend, bringing digits down to the PR
    do
    {
        if (flag)
        {
            // Number of words in PR is 1 more than in the divisor. Apply pq guessing
            pq = ( ((uint32) pr[m_wordCount]) << 16 ) | (uint32) pr[m_wordCount - 1];
            pq /= (uint32) n[m_wordCount - 1];
            if (pq > 0xFFFF) pq = 0xFFFF; // Divide overflow, use pq=0xFFFF
            
            // Multiply the divisor by pq, subtract the result from PR
            if ( !mulsub(pr, n, pq) )
            {
                // trial pq was too high (max 1 or 2 too high), add the divisor back and check
                carry = add(pr, n);
                carry += pr[m_wordCount];
                pr[m_wordCount] = carry & 0xFFFF;
                if ( !(carry & 0x10000) )
                {
                    // Trial pq was still too high, add the divisor back again
                    carry = add(pr, n);
                    carry += pr[m_wordCount];
                    pr[m_wordCount] = carry & 0xFFFF;
                }
            }
            // pr[wordCount] must be 0 here, but another digit will come at the next loop iteration
            flag = (pr[m_wordCount - 1] != 0);
        }
        else
        {
            // PR has N significant words or less
            if ( pr[m_wordCount - 1] ) // MSW of PR
            {
                // PR has N significant words, same as dividend
                if (cmp(pr, n) >= 0)
                {
                    // PR is >= divisor, pq=1, update PR
                    sub(pr, n);
                    // If MSW of PR after subtraction is >0, there will be N+1 words
                    flag = (pr[m_wordCount - 1] != 0);
                }
                else
                {
                    // PR is < divisor, pq=0, there will be N+1 words
                    flag = 1;
                }
            }
            else
            {
                // PR has less than N significant words. pq=0, leave the flag at 0
            }
        }
        pr--; // Bring in another digit from the dividend into PR
    } while(--count);
}


/**
  * @brief  Big numbers addition (res += a)
  * @param  res: A number, will be replaced with operation result
  * @param  a: Summand
  * @return Cary flag
  */
uint CRsa::add (uint16* res, uint16* a)
{
    uint32 c = 0;
    uint count = m_wordCount;
    
	do
	{
		c += *res + *a;
		*res = c & 0xFFFF;
		a++;
		res++;
		c >>= 16;
	} while(--count);
    
    return (c & 1);
}


/**
  * @brief  Big numbers subtraction (res -= a)
  * @param  res: A number, will be replaced with operation result
  * @param  a: Subtractable
  */
void CRsa::sub (uint16* res, uint16* a)
{
    uint32 c = 1;
    uint count = m_wordCount;
    
	do
	{
		c += 65535 + *res - *a;
		*res = c & 0xFFFF;
		a++;
		res++;
		c >>= 16;
		c &= 1;
	} while(--count);
}


/**
  * @brief  Big numbers multiplication with subtraction (res -= a * b)
  * @param  res: Base number, will be replaced with operation result
  * @param  a: Multiplier
  * @param  b: Multiplier
  * @return Borrowing flag
  */
uint CRsa::mulsub (uint16* res, uint16* a, uint16 b)
{
    uint32 nborrow = 1;
    uint32 mcarry = 0;
    uint count = m_wordCount;
    
    do
    {
        mcarry += *a++ * b;
        nborrow += 65535 + *res - (mcarry & 0xFFFF);
        *res++ = nborrow & 0xFFFF;
        mcarry >>= 16;
        nborrow >>= 16;
        nborrow &= 1;
    } while(--count);
    
    // Subtract from the N+1th digit
    nborrow += 65535 + *res - (mcarry & 0xFFFF);
    *res = nborrow & 0xFFFF;
    nborrow >>= 16;
    
    return (nborrow & 1);
}


/**
  * @brief  Big numbers comparision (a - b)
  * @param  a: First number
  * @param  b: Second number
  * @return Comparision result (a - b) 
  */
int CRsa::cmp (const uint16* a, const uint16* b)
{
    uint count = m_wordCount;
	int32 dif;
    
	a += m_wordCount - 1;
	b += m_wordCount - 1;
    
	do
	{
		dif = *a-- - *b--;
		if (dif) return (dif > 0) ? 1 : -1;
	} while(--count);
    
	return 0;
}
