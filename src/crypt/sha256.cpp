//*****************************************************************************
//
// @brief   Hashing algorithm SHA-256 implementation
//          Based on https://ru.wikipedia.org/wiki/SHA-2
//          and https://github.com/altk/sha256comparison/blob/master/CPP/sha256.c
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "crypt/sha256.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Macros of block convertions
    #define ROTLEFT(a,b)                        ( ((a) << (b)) | ((a) >> (32 - (b))) )
    #define ROTRIGHT(a,b)                       ( ((a) >> (b)) | ((a) << (32 - (b))) )
    #define CH(x,y,z)                           ( ((x) & (y)) ^ (~(x) & (z)) )
    #define MAJ(x,y,z)                          ( ((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)) )
    #define EP0(x)                              ( ROTRIGHT(x,2) ^ ROTRIGHT(x,13) ^ ROTRIGHT(x,22) )
    #define EP1(x)                              ( ROTRIGHT(x,6) ^ ROTRIGHT(x,11) ^ ROTRIGHT(x,25) )
    #define SIG0(x)                             ( ROTRIGHT(x,7) ^ ROTRIGHT(x,18) ^ ((x) >> 3) )
    #define SIG1(x)                             ( ROTRIGHT(x,17) ^ ROTRIGHT(x,19) ^ ((x) >> 10) )


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // First 32 bits of fractional part of cubic roots for first 64 prime numbers
    static const uint32 K[] =
    {
        0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
        0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
        0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
        0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
        0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
        0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
        0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
        0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
    };


/**
  * @brief  Constructor
  * @param  None
  */
CSha256::CSha256 () :
    IHash(),
    m_totalSize(0),
    m_processedSize(0)
{
    m_pState = (uint32*) malloc(8 * sizeof(uint32));
    assert(!m_pState);

    m_pW = (uint32*) malloc(sizeof(uint32) * 64);
    assert(!m_pW);
}


/**
  * @brief  Destructor
  * @param  None
  */
CSha256::~CSha256 ()
{
    if (m_pW)       free(m_pW);
    if (m_pState)   free(m_pState);
}


/**
  * @brief  Reset state
  * @param  None
  * @retval None
  */
void CSha256::Reset (void)
{
    if (m_pState)
    {
        memset( m_pState, 0x00, 8 * sizeof(uint32) );
    }

    if (m_pW)
    {
        memset( m_pW, 0x00, 64 * sizeof(uint32) );
    }

    m_totalSize = 0;
    m_processedSize = 0;
}


/**
  * @brief  Hash calculation or start of partial calculation
  * @param  data: Data for hashing
  * @param  size: Data size
  * @param  totalSize: Total data size
  * @return Size of processed data or negative error code
  */
int CSha256::Calculate (const void* data, uint size, uint totalSize)
{
    if (!data || !size) return -1;
    if (totalSize < size) return -2;
    if (!m_pState || !m_pW) return -3;

    // Variables initialization
    // First 32 bits of fractional part of cubic roots for first 8 prime numbers [from 2 to 19]
    m_pState[0] = 0x6a09e667;
    m_pState[1] = 0xbb67ae85;
    m_pState[2] = 0x3c6ef372;
    m_pState[3] = 0xa54ff53a;
    m_pState[4] = 0x510e527f;
    m_pState[5] = 0x9b05688c;
    m_pState[6] = 0x1f83d9ab;
    m_pState[7] = 0x5be0cd19;

    m_totalSize = totalSize;
    m_processedSize = 0;

    return Append(data, size);
}


/**
  * @brief  Apped data for hash calculation (continue calculation)
  * @param  data: Data for append
  * @param  size: Data size
  * @return Size of processed data or negative error code
  */
int CSha256::Append (const void* data, uint size)
{
    if (!data || !size) return -1;
    if ( m_totalSize <= m_processedSize || size > (m_totalSize - m_processedSize) ) return -2;
    if (!m_pState || !m_pW) return -3;

    // Full blocs processing
    uint blockCount = size / SHA256_BLOCK_SIZE;
    const char* ptr = (const char*) data;

    for (uint i = 0; i < blockCount; i++, ptr += SHA256_BLOCK_SIZE)
    {
        update(ptr);
    }

    int result;
    if ( size == (m_totalSize - m_processedSize) )
    {
        // Finishing the calculation
        final(ptr, m_totalSize);
        m_processedSize = m_totalSize;
        result = (int)size;
    }
    else
    {
        // Saving amount of remaining data for futher processing 
        result = ptr - (const char*)data;
        m_processedSize += (uint)result;
    }

    return result;
}


/**
  * @brief  Read hashing result
  * @param  buff: Buffer for result saving
  * @param  size: Buffer size
  * @return Data size written into the buffer or negative error code
  */
int CSha256::GetResult (void* buff, uint size)
{
    if (!buff || size < SHA256_DIGEST_LENGTH) return -1;
    if (!m_pState) return -2;
    if (!m_processedSize || m_processedSize != m_totalSize) return -3;

    // Fill the result, taking into account that calculations are made in big endian format
    char* hash = (char*) buff;
    for (uint i = 0; i < 4; ++i)
    {
        uint offset = 24 - i * 8;
        hash[i + 0] = (m_pState[0] >> offset) & 0x000000ff;
        hash[i + 4] = (m_pState[1] >> offset) & 0x000000ff;
        hash[i + 8] = (m_pState[2] >> offset) & 0x000000ff;
        hash[i + 12] = (m_pState[3] >> offset) & 0x000000ff;
        hash[i + 16] = (m_pState[4] >> offset) & 0x000000ff;
        hash[i + 20] = (m_pState[5] >> offset) & 0x000000ff;
        hash[i + 24] = (m_pState[6] >> offset) & 0x000000ff;
        hash[i + 28] = (m_pState[7] >> offset) & 0x000000ff;
    }

    return SHA256_DIGEST_LENGTH;
}


/**
  * @brief  Get hash size
  * @return Hash size (bytes) or negative error code
  */
int CSha256::GetSize (void)
{
    return SHA256_DIGEST_LENGTH;
}


/**
  * @brief  Intermediate step of hash calculation
  * @param  data: Processed data block
  */
void CSha256::update (const char* data)
{
    // Conversion into big endian format
    for (uint i = 0; i < SHA256_BLOCK_SIZE / sizeof(uint32); ++i, data += 4)
    {
        m_pW[i] = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3]);
    }
    // Additional data generation
    for (uint i = SHA256_BLOCK_SIZE / sizeof(uint32); i < 64; ++i)
    {
        m_pW[i] = m_pW[i - 16] + SIG0(m_pW[i - 15]) + m_pW[i - 7] + SIG1(m_pW[i - 2]);
    }

    // Intermediate hash value calculation (64 rounds)
    uint32 a = m_pState[0];
    uint32 b = m_pState[1];
    uint32 c = m_pState[2];
    uint32 d = m_pState[3];
    uint32 e = m_pState[4];
    uint32 f = m_pState[5];
    uint32 g = m_pState[6];
    uint32 h = m_pState[7];

    for (uint i = 0; i < 64; ++i)
    {
        uint32 t2 = EP0(a) + MAJ(a,b,c);
        uint32 t1 = h + EP1(e) + CH(e,f,g) + K[i] + m_pW[i];

        h = g;
        g = f;
        f = e;
        e = d + t1;
        d = c;
        c = b;
        b = a;
        a = t1 + t2;
    }

    m_pState[0] += a;
    m_pState[1] += b;
    m_pState[2] += c;
    m_pState[3] += d;
    m_pState[4] += e;
    m_pState[5] += f;
    m_pState[6] += g;
    m_pState[7] += h;
}


/**
  * @brief  Final step of hash calculation
  * @param  data: Processed data block (remaining data)
  * @param  totalSize: Total size of data
  */
void CSha256::final (const char* data, uint totalSize)
{
    uint restSize = totalSize % SHA256_BLOCK_SIZE;
    memcpy(m_pW, data, restSize);
    char* m = (char*) m_pW;

    // Append data into the rest of the block
    uint index = restSize;
    m[index++] = 0x80;

    if (restSize < 56)
    {
        while (index < 56) m[index++] = 0x00;
    }
    else
    {
        while (index < 64) m[index++] = 0x00;

        update(m);
        memset(m_pW, 0, 56);
    }

    // Adding size of original data (in bits)
    uint64 bitlen = totalSize * 8;
    m[63] = bitlen;
    m[62] = bitlen >> 8;
    m[61] = bitlen >> 16;
    m[60] = bitlen >> 24;
    m[59] = bitlen >> 32;
    m[58] = bitlen >> 40;
    m[57] = bitlen >> 48;
    m[56] = bitlen >> 56;

    update(m);
}
