//*****************************************************************************
//
// @brief   IRQ subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "arch/core/cm3/irq.h"
#include "arch/system.h"
#include "kernel/kstdio.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    extern "C" void __reset_handler (void);
    static void irqHdlLoop (void);
    static void irqHdlNop (void);
    static void irqHdlHardFault (void);
    static void irqHdlProxy (void);
    static void handlerHardFaultPrint (uint32* ptr);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Stack memory section
    CC_SECTION_DECLARE(CSTACK);

    // Initial IRQ table (ROM)
    static const TIsr g_IrqTableRom[IRQ_MAX_COUNT] CC_VAR_SECTION(.IrqTableRom) =
    {
        {.ptr = CC_SECTION_ADDR_END(CSTACK)},   // |  0 | 0x00000000  | Stack
        __reset_handler,                        // |  1 | 0x00000004  | Reset
        irqHdlProxy,                            // |  2 | 0x00000008  | Notmasked interrupt
        irqHdlProxy,                            // |  3 | 0x0000000C  | HardFault
        irqHdlProxy,                            // |  4 | 0x00000010  | Memory managment fault
        irqHdlProxy,                            // |  5 | 0x00000014  | Bus fault
        irqHdlProxy,                            // |  6 | 0x00000018  | Usage fault
        irqHdlProxy,                            // |  7 | 0x0000001C  | Reserved
        irqHdlProxy,                            // |  8 | 0x00000020  | Reserved
        irqHdlProxy,                            // |  9 | 0x00000024  | Reserved
        irqHdlProxy,                            // | 10 | 0x00000028  | Reserved
        irqHdlProxy,                            // | 11 | 0x0000002C  | Supervisor call
        irqHdlProxy,                            // | 12 | 0x00000030  | Reserved for debug
        irqHdlProxy,                            // | 13 | 0x00000034  | Reserved
        irqHdlProxy,                            // | 14 | 0x00000038  | Pending SVCall (for contex switch)
        irqHdlProxy                             // | 15 | 0x0000003C  | System timer tick
    };

    // IRQ table (RAM)
    static THandlerISR* volatile g_IrqTableRam[IRQ_MAX_COUNT] CC_VAR_SECTION(.IrqTableRam);

    uint g_irqLockCount = 0;
    uint g_irqLockState = 0;


/**
  * @brief  IRQ subsystem initialization
  * @param  None
  */
void IRQ::Init (void)
{
    for (uint i = 1; i < IRQ_PERIPH_START_INDEX; i++)
    {
        g_IrqTableRam[i] = irqHdlLoop;
    }

    g_IrqTableRam[0] = g_IrqTableRom[0].hdl;
    g_IrqTableRam[IRQ_ID_HARD_FAULT] = irqHdlHardFault;

    for (uint i = IRQ_PERIPH_START_INDEX; i < IRQ_MAX_COUNT; i++)
    {
        g_IrqTableRam[i] = irqHdlNop;
        // Set lowest priority
        CM3_NVIC->IP[i - IRQ_PERIPH_START_INDEX] = 0xff;
    }

    // Remap IRQ table to RAM
    CM3_SCB->VTOR = ( (uint32)g_IrqTableRam & SCB_VTOR_TBLOFF_MSK ) ;
    CM3_SCB->VTOR |= 1 << SCB_VTOR_TBLBASE_POS;

    // 0x05FA - write access unlocking key
    uint priGroup =  7 - CPU_IRQ_PRIORITY_BITS;
    CM3_SCB->AIRCR = 0x05FA0000 | (priGroup << SCB_AIRCR_PRIGROUP_POS);
}


/**
  * @brief  Set / remove IRQ handler
  * @param  irqId: IRQ vector ID
  * @param  handler: IRQ handler to set or NULL to delete
  * @param  priority: IRQ priority
  */
void IRQ::SetHandler (uint irqId, THandlerISR* handler, uint priority)
{
    assert(irqId == IRQ_NONE);
    assert(irqId >= IRQ_MAX_COUNT);
    
    Disable(irqId);

    // Reset pending IRQ status
    if (irqId >= IRQ_PERIPH_START_INDEX)
    {
        uint periphIndex = irqId - IRQ_PERIPH_START_INDEX;
        CM3_NVIC->ICPR[periphIndex >> 5] = ((uint32)1 << (periphIndex & (uint8)0x1F));
    }

    if (!handler)
    {
        handler = irqHdlNop;
    }

    g_IrqTableRam[irqId] = handler;

    SetPriority(irqId, priority);
}


/**
  * @brief  Enable specified IRQ
  * @param  irqId: IRQ vector ID
  */
void IRQ::Enable (uint irqId)
{
    if (irqId < IRQ_PERIPH_START_INDEX) return;
    
    uint periphIndex = irqId - IRQ_PERIPH_START_INDEX;
    CM3_NVIC->ISER[(periphIndex >> 5)] = ((uint)1 << (periphIndex & (uint8)0x1F));
}


/**
  * @brief  Disable specified IRQ
  * @param  irqId: IRQ vector ID
  */
void IRQ::Disable (uint irqId)
{
    if (irqId < IRQ_PERIPH_START_INDEX) return;
    
    uint periphIndex = irqId - IRQ_PERIPH_START_INDEX;
    CM3_NVIC->ICER[(periphIndex >> 5)] = ((uint)1 << (periphIndex & (uint8)0x1F));
}


/**
  * @brief  Set IRQ handler priority
  * @param  irqId: IRQ vector ID
  * @param  priority: IRQ priority
  */
void IRQ::SetPriority (uint irqId, uint priority)
{
    assert(irqId == IRQ_NONE);
    assert(irqId >= IRQ_MAX_COUNT);
    
    uint8_t pri;
    switch (priority)
    {
        case IRQ_PRIORITY_HIGHEST: pri = 0;  break;
        case IRQ_PRIORITY_HIGH:    pri = 1;  break;
        case IRQ_PRIORITY_MID:     pri = 2;  break;
        case IRQ_PRIORITY_LOW:     pri = 3;  break;
        case IRQ_PRIORITY_LOWEST:  pri = CPU_IRQ_PRIORITY_COUNT - 1; break;
        default:                   assert(1);
    }
    
    uint8_t priGroup = (CM3_SCB->AIRCR & SCB_AIRCR_PRIGROUP_MSK) >> SCB_AIRCR_PRIGROUP_POS;

    if (irqId >= IRQ_PERIPH_START_INDEX)
    {
        // IRQ of peripheral module
        uint8_t maxVal = 0xFF >> (priGroup + 1);
        assert(pri > maxVal);

        CM3_NVIC->IP[irqId - IRQ_PERIPH_START_INDEX] = (uint8)(pri << (priGroup + 1));
    }
    else
    {
        // Core IRQ
        CM3_SCB->SHP[(irqId & 0xF)-4UL] = (uint8)(pri << (priGroup + 1));
    }
}


/**
  * @brief  Enable all IRQ
  * @param  None
  */
void IRQ::Enable (void)
{
    asm volatile ("CPSIE   I  \n");
}


/**
  * @brief  Disable all IRQ
  * @param  None
  */
void IRQ::Disable (void)
{
    asm volatile ("CPSID   I  \n");
}


/**
  * @brief  Get active IRQ vector ID
  * @return Active IRQ vector ID or IRQ_NONE (there is no active IRQ)
  */
uint IRQ::GetActive (void)
{
    uint32 reg;

    asm volatile (
        "MRS     %0, IPSR   \n"
        : "=&r" (reg)
    );

    // 0x1ff - mask for active IRQ vector
    uint active = reg & 0x1ff;

    return (active) ? active : IRQ_NONE;
}


/**
  * @brief  Temporary IRQ disabling with saving current IRQ state
  * @param  None
  */
void IRQ::Lock (void)
{
    ++g_irqLockCount;

    // Save IRQ state on first locking
    if (g_irqLockCount == 1)
    {
        asm volatile (
            "MRS     %0, PRIMASK   \n"
            : "=&r" (g_irqLockState)
        );

        Disable();
    }
}


/**
  * @brief  Restore IRQ state saved in Lock()
  * @param  None
  */
void IRQ::Unlock (void)
{
    if (!g_irqLockCount) return;

    --g_irqLockCount;

    if (!g_irqLockCount)
    {
        // Restore previously saved IRQ state on last unlocking
        asm volatile (
            "MSR     PRIMASK, %0   \n"
            : "=&r" (g_irqLockState)
        );
    }
}


/**
  * @brief  IRQ handler stub
  * @param  None
  */
void irqHdlLoop (void)
{
    IRQ::Disable();
    assert(1);
}


/**
  * @brief  IRQ handler stub
  * @param  None
  */
void irqHdlNop (void)
{
    asm("nop");
}


/**
  * @brief  IRQ handler proxy. It's used for proper working of C-SPY (IAR) during debug
  * @param  None
  */
void irqHdlProxy (void)
{
    uint32 reg;
    asm volatile (
        "MRS     %0, IPSR   \n"
        : "=&r" (reg)
    );
    int active = reg & 0x1ff;

    assert( !g_IrqTableRam[active] );

    g_IrqTableRam[active]();
}


/**
  * @brief  Hardfault output handler (trace dump)
  * @param  ptr: Pointer to stack frame
  */
void handlerHardFaultPrint (uint32* ptr)
{
    uint32 regBFAR = *((volatile uint32*)0xE000ED38);
    uint32 regCFSR = *((volatile uint32*)0xE000ED28);
    uint32 regHFSR = *((volatile uint32*)0xE000ED2C);
    uint32 regDFSR = *((volatile uint32*)0xE000ED30);
    uint32 regAFSR = *((volatile uint32*)0xE000ED3C);

    printf(
            "[Hard fault]\r\n"
            "R0:   0x%.8x\r\n"
            "R1:   0x%.8x\r\n"
            "R2:   0x%.8x\r\n"
            "R3:   0x%.8x\r\n"
            "R12:  0x%.8x\r\n"
            "LR:   0x%.8x\r\n"
            "PC:   0x%.8x\r\n"
            "PSR:  0x%.8x\r\n"
            "BFAR: 0x%.8x\r\n"
            "CFSR: 0x%.8x\r\n"
            "HFSR: 0x%.8x\r\n"
            "DFSR: 0x%.8x\r\n"
            "AFSR: 0x%.8x",
            ptr[0],
            ptr[1],
            ptr[2],
            ptr[3],
            ptr[4],
            ptr[5],
            ptr[6],
            ptr[7],
            regBFAR,
            regCFSR,
            regHFSR,
            regDFSR,
            regAFSR
    );

    uint i = 100000;
    while (--i);
    IRQ::Disable();
    while(1);
}


/**
  * @brief  Hardfault handler
  * @param  None
  */
void irqHdlHardFault (void)
{
    asm volatile (
        " mov r2, %0         \n"
        " tst lr, #4         \n"
        " ite eq             \n"
        " mrseq r0, msp      \n"
        " mrsne r0, psp      \n"
        " ldr r1, [r0, #24]  \n"
        " bx r2              \n"
        :
        : "r" (handlerHardFaultPrint)
    );
}
