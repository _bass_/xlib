//*****************************************************************************
//
// @brief   System timer driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "arch/systimer.h"
#include "misc/macros.h"


/**
  * @brief  Timer initialization
  * @param  freq: Timer frequency (Hz)
  */
void SYSTMR::Init (uint freq)
{
    uint fcpu = CLOCK::GetCpuFreq();

    // Frequency divider calculation
    uint value = fcpu / freq;
    assert(value & ~SYSTICK_LOAD_RELOAD_MSK);

    if (value & ~SYSTICK_LOAD_RELOAD_MSK)
    {
        // Divider value exceeds maximum value. Input frequency divider should be activated.
        CM3_SYSTICK->CTRL &= ~SYSTICK_CTRL_CLKSOURCE_MSK;
        value /= 8;
    }
    else
    {
        CM3_SYSTICK->CTRL |= SYSTICK_CTRL_CLKSOURCE_MSK;
    }

    CM3_SYSTICK->LOAD = value & SYSTICK_LOAD_RELOAD_MSK;
}


/**
  * @brief  Start timer
  * @param  None
  */
void SYSTMR::Start (void)
{
    CM3_SYSTICK->CTRL |= SYSTICK_CTRL_ENABLE_MSK;
}


/**
  * @brief  Stop timer
  * @param  None
  */
void SYSTMR::Stop (void)
{
    CM3_SYSTICK->CTRL &= ~SYSTICK_CTRL_ENABLE_MSK;
}


/**
  * @brief  Set timer IRQ (tick) handler
  * @param  handler: IRQ handler
  */
void SYSTMR::SetHandler (THandlerISR* handler)
{
    IRQ::SetHandler(IRQ_ID_SYSTICK, handler);
    
    // Set lowest priority
    CM3_BASE_SCB->SHP[11] = 0xF0;

    if (handler)
    {
        // IRQ activation when counter value becomes 0
        CM3_SYSTICK->CTRL |= 1 << SYSTICK_CTRL_TICKINT_POS;
    }
    else
    {
        // Turn off IRQ generation
        CM3_SYSTICK->CTRL &= ~(1 << SYSTICK_CTRL_TICKINT_POS);
    }
}
