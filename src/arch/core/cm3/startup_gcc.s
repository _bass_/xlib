
    .syntax unified
    .arch   armv7-m
    .cpu    cortex-m3
    .fpu    softvfp
    .text
    .thumb
    .thumb_func
    .align	2
    .globl	__reset_handler
    .type   __reset_handler, %function

__reset_handler:
    ldr     r0, =__start_CSTACK
    mov     sp, r0

/*  Single section scheme.
 *
 *  The ranges of copy from/to are specified by following symbols
 *    __etext: LMA of start of the section to copy from. Usually end of text
 *    __data_start__: VMA of start of the section to copy to
 *    __data_end__: VMA of end of the section to copy to
 *
 *  All addresses must be aligned to 4 bytes boundary.
 */
    ldr	r1, =__etext
    ldr	r2, =__data_start__
    ldr	r3, =__data_end__

.L_loop1:
    cmp	r2, r3
    ittt	lt
    ldrlt	r0, [r1], #4
    strlt	r0, [r2], #4
    blt	.L_loop1


/*  Single BSS section scheme.
 *
 *  The BSS section is specified by following symbols
 *    __bss_start__: start of the BSS section.
 *    __bss_end__: end of the BSS section.
 *
 *  Both addresses must be aligned to 4 bytes boundary.
 */
    ldr	r1, =__bss_start__
    ldr	r2, =__bss_end__

    movs	r0, 0
.L_loop3:
    cmp	r1, r2
    itt	lt
    strlt	r0, [r1], #4
    blt	.L_loop3


    /* Call low level intitialization function.*/
    bl      LowLevelInit

    bl      _start

    /* Call static constructors */
/*    bl      __libc_init_array*/
    /* Call the application's entry point.*/
/*    bl      main*/
    bx      lr

    .pool
    .size   __reset_handler, .-__reset_handler

    .end
