//*****************************************************************************
//
// @brief   Startup for Cortex-M0 core (IAR)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
        MODULE  ?startup

        PUBLIC  __reset_handler

        EXTERN  __cmain
        EXTERN  LowLevelInit

        SECTION .text:CODE:REORDER(1)
        THUMB

__reset_handler:

        BL      LowLevelInit
        BL      __cmain

        END

