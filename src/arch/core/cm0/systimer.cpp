//*****************************************************************************
//
// @brief   System timer driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "arch/systimer.h"
#include "misc/macros.h"


/**
  * @brief  Timer initialization
  * @param  freq: Timer frequency (Hz)
  */
void SYSTMR::Init (uint freq)
{
    uint fcpu = CLOCK::GetCpuFreq();
    uint value = fcpu / freq - 1;
    
    assert(value & ~SYSTICK_LOAD_RELOAD_MSK);
    
    CM0_SYSTICK->LOAD = value;
    CM0_SYSTICK->VAL = 0;
    
    CM0_SYSTICK->CTRL |= SYSTICK_CTRL_CLKSOURCE_MSK;
}


/**
  * @brief  Start timer
  * @param  None
  */
void SYSTMR::Start (void)
{
    CM0_SYSTICK->CTRL |= SYSTICK_CTRL_ENABLE_MSK;
}


/**
  * @brief  Stop timer
  * @param  None
  */
void SYSTMR::Stop (void)
{
    CM0_SYSTICK->CTRL &= ~SYSTICK_CTRL_ENABLE_MSK;
}


/**
  * @brief  Set timer IRQ (tick) handler
  * @param  handler: IRQ handler
  */
void SYSTMR::SetHandler (THandlerISR* handler)
{
    IRQ::SetHandler(IRQ_ID_SYSTICK, handler, IRQ_PRIORITY_LOWEST);

    if (handler)
    {
        // IRQ activation when counter value becomes 0
        CM0_SYSTICK->CTRL |= 1 << SYSTICK_CTRL_TICKINT_POS;
    }
    else
    {
        // Turn off IRQ generation
        CM0_SYSTICK->CTRL &= ~(1 << SYSTICK_CTRL_TICKINT_POS);
    }
}
