//*****************************************************************************
//
// @brief   SWO driver
// @author  Petr Babkin, Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/core/cm4/swo.h"
#if defined(__ICCARM__)
#include <stdio.h>
#endif
#include "misc/macros.h"


/**
  * @brief  Constructor
  * @param  portIndex: Used SWO port index
  */
CDevSwo::CDevSwo (uint portIndex) :
    CDevChar(),
    m_portIndex(portIndex)
{
    assetr(m_portIndex > 31);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevSwo::Open (void)
{
    CM4_CORE_DBG->DEMCR |= CORE_DEBUG_DEMCR_TRACE_ENABLE;
    CM4_ITM->LAR = 0xC5ACCE55;
    CM4_ITM->TCR &= ~(ITM_TCR_TRACEBUSID_MSK | ITM_TCR_SYNCENA_MSK | ITM_TCR_SWOENA_MSK | ITM_TCR_ITMENA_MSK);
    CM4_ITM->TCR |= 1 << ITM_TCR_TRACEBUSID_POS
                        | 1 << ITM_TCR_SWOENA_POS
                        | 1 << ITM_TCR_DWTENA_MSK
                        | 1 << ITM_TCR_SYNCENA_POS
                        | 1 << ITM_TCR_ITMENA_POS; //| ITM_TCR_TSENA_MSK
    CM4_ITM->TPR = 0x0f;
    CM4_ITM->TER |= 1 << m_portIndex;

    #if defined(__ICCARM__)
    // Terminal IO output activation in IAR IDE (in project settings: output via Semihosting -> SWO)
    putchar('\n');
    #endif

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevSwo::Close (void)
{
    CM4_ITM->TER &= ~(1 << m_portIndex);

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CDevSwo::Configure (const void* params)
{
    UNUSED_VAR(params);
    
    return DEV_OK;
}


/**
  * @brief  Send character to output stream
  * @param  c: character
  * @return 0 or negative error code
  */
int CDevSwo::PutChar (const uchar c)
{
    if ( (CM4_ITM->TCR & ITM_TCR_ITMENA_MSK) == 0 ) return -1;     // ITM enabled
    if ( (CM4_ITM->TER & (1 << m_portIndex)) == 0 ) return -2;     // ITM Port enabled

    while (CM4_ITM->PORT[m_portIndex] == 0);
    
    volatile char* p = (volatile char*) &CM4_ITM->PORT[m_portIndex];
    *p = c;
    
    return 0;
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CDevSwo::GetChar (void)
{
    return -1;
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevSwo::Write (const void* data, uint size)
{
    if (size && !data) return -1;
    
    uint count = 0;
    const char* ptr = (const char*) data;
    
    for (count = 0; count < size; ++count)
    {
        int res = PutChar( *ptr++ );
        if (res < 0) break;
    }
    
    return (int)count;
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CDevSwo::Read (void* buff, uint size)
{
    UNUSED_VAR(buff);
    UNUSED_VAR(size);
    
    return -1;
}
