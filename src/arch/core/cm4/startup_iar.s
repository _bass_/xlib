//*****************************************************************************
//
// @brief   Startup for Cortex-M4 core (IAR)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

        MODULE  ?startup

        PUBLIC  __reset_handler

        EXTERN  __cmain
        EXTWEAK __iar_init_core
        EXTWEAK __iar_init_vfp
        EXTERN  LowLevelInit

        SECTION .text:CODE:REORDER(1)
        THUMB

__reset_handler:

        BL      LowLevelInit
        BL      __iar_init_vfp
        BL      __cmain

        END
