//*****************************************************************************
//
// @brief   Startup for Cortex-M4 core (GCC)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include <string.h>
#include <stdint.h>


/**
  * @brief  BSS data initialization
  * @param  None
  */
static inline void prepareBss (void)
{
    extern void* __bss_start__;
    extern void* __bss_end__;

    void* pStart = &__bss_start__;
    void* pEnd = &__bss_end__;
    size_t size = (char*)pEnd - (char*)pStart;

    memset(pStart, 0, size);
}


/**
  * @brief  Data section initialization
  * @param  None
  */
static inline void prepareData (void)
{
    extern void* __data_start__;
    extern void* __data_end__;
    extern void* _sidata;

    void* pSrc = &_sidata;
    void* pDst = &__data_start__;
    size_t size = (char*)&__data_end__ - (char*)&__data_start__;

    memcpy(pDst, pSrc, size);
}


/**
  * @brief  Call static constructors
  * @param  None
  */
static inline void callConstructors (void)
{
    extern void* __init_array_start;
    extern void* __init_array_end;

    uint32_t* pCtor;
    for (pCtor = (uint32_t*)&__init_array_start; pCtor < (uint32_t*)&__init_array_end; ++pCtor)
    {
        ((void(*)(void))(*pCtor))();
    }
}


/**
  * @brief  Reset handler (entry point)
  * @param  None
  */
void __reset_handler (void)
{
    extern void LowLevelInit (void);
    LowLevelInit();

    prepareBss();
    prepareData();
    callConstructors();

    extern int main (void);
    main();
}
