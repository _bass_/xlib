//*****************************************************************************
//
// @brief   RTC module driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "arch/rtc.h"
#include "misc/macros.h"
#include <time.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static time_t g_startTime;


/**
  * @brief  Module initialization
  * @param  None
  */
void RTC::Init (void)
{
    time(&g_startTime);
}


/**
  * @brief  Read Unix timestamp
  * @return Unix timestamp (seconds)
  */
uint32 RTC::GetTime (void)
{
    time_t currTime;
    time(&currTime);

    currTime += g_startTime;

    return (uint32)currTime;
}


/**
  * @brief  Read formatted datetime
  * @param  time: Pointer to variable to save current datetime value
  */
void RTC::GetTime (TTime* pTime)
{
    time_t currTime;
    time(&currTime);
    currTime += g_startTime;

    tm* pTm = gmtime(&currTime);

    pTime->year = (uint32) 1900 + pTm->tm_year - 2000;
    pTime->month = (uint32) pTm->tm_mon + 1;
    pTime->day = (uint32) pTm->tm_mday;
    pTime->hour = (uint32) pTm->tm_hour;
    pTime->min = (uint32) pTm->tm_min;
    pTime->sec = (uint32) (pTm->tm_sec > 59) ? 59 : pTm->tm_sec;
}


/**
  * @brief  Set time
  * @param  time: Time to set (Unix time)
  */
void RTC::SetTime (const TTime* pTime)
{
    uint32 utime = TimeToUnixTime(*pTime);
    SetTime(utime);
}


/**
  * @brief  Set time
  * @param  time: Time to set
  */
void RTC::SetTime (uint32 utime)
{
    time_t currTime;
    time(&currTime);

    g_startTime = utime - currTime;
}
