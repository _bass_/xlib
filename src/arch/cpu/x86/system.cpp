//*****************************************************************************
//
// @brief   Generic system functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"


/**
  * @brief  Delay (microseconds)
  * @param  time: delay value (microseconds)
  */
void delay_us (uint time)
{
    uint32 count = 1 * time + 1;
    while (--count);
}


/**
  * @brief  Delay (milliseconds)
  * @param  time: delay value (milliseconds)
  */
void delay_ms (uint time)
{
    uint32 count = 1000 * time + 1;
    while (--count);
}
