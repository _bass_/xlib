//*****************************************************************************
//
// @brief   IRQ subsystem (stub)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "misc/macros.h"


/**
  * @brief  IRQ subsystem initialization
  * @param  None
  */
void IRQ::Init (void)
{
}


/**
  * @brief  Set / remove IRQ handler
  * @param  irqId: IRQ vector ID
  * @param  handler: IRQ handler to set or NULL to delete
  * @param  priority: IRQ priority
  */
void IRQ::SetHandler (uint irqId, THandlerISR* handler, uint priority)
{
    UNUSED_VAR(irqId);
    UNUSED_VAR(handler);
    UNUSED_VAR(priority);
}


/**
  * @brief  Enable specified IRQ
  * @param  irqId: IRQ vector ID
  */
void IRQ::Enable (uint irqId)
{
    UNUSED_VAR(irqId);
}


/**
  * @brief  Disable specified IRQ
  * @param  irqId: IRQ vector ID
  */
void IRQ::Disable (uint irqId)
{
    UNUSED_VAR(irqId);
}


/**
  * @brief  Set IRQ handler priority
  * @param  irqId: IRQ vector ID
  * @param  priority: IRQ priority
  */
void IRQ::SetPriority (uint irqId, uint priority)
{
    UNUSED_VAR(irqId);
    UNUSED_VAR(priority);
}


/**
  * @brief  Enable all IRQ
  * @param  None
  */
void IRQ::Enable (void)
{
}


/**
  * @brief  Disable all IRQ
  * @param  None
  */
void IRQ::Disable (void)
{
}


/**
  * @brief  Get active IRQ vector ID
  * @return Active IRQ vector ID or IRQ_NONE (there is no active IRQ)
  */
uint IRQ::GetActive (void)
{
    return (uint)IRQ_NONE;
}


/**
  * @brief  Temporary IRQ disabling with saving current IRQ state
  * @param  None
  */
void IRQ::Lock (void)
{
}


/**
  * @brief  Restore IRQ state saved in Lock()
  * @param  None
  */
void IRQ::Unlock (void)
{
}
