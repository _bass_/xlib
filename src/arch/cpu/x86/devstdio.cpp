//*****************************************************************************
//
// @brief   stdio device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu/x86/devstdio.h"
#include "misc/macros.h"
#include <stdio.h>


/**
  * @brief  Constructor
  * @param  None
  */
CDevStdio::CDevStdio () :
    CDevChar()
{
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevStdio::Open (void)
{
    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevStdio::Close (void)
{
    return DEV_ERR;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CDevStdio::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}


/**
  * @brief  Read device state
  * @param  None
  * @return Device state
  */
TDevState CDevStdio::GetState (void) const
{
    return DEV_OPENED;
}


/**
  * @brief  Send character to output stream
  * @param  c: character
  * @return 0 or negative error code
  */
int CDevStdio::PutChar (const char c)
{
    return putchar(c);
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CDevStdio::GetChar (void)
{
    #warning "FIXME"
    // feof(stdin) always returns 0 if there is no data
    // if ( feof(stdin) ) return -1;

    // Current thread will be blocked until data is appeared
    // return getchar();
    
    return -1;
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevStdio::Write (const void* buff, uint size)
{
    if (!buff) return -1;
    if (!size) return 0;

    const char* ptr = (const char*) buff;
    for (uint i = size; i; ++ptr, --i)
    {
        int res = putchar(*ptr);
        if (res < 0) break;
    }

    return (int) (ptr - (const char*)buff);
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CDevStdio::Read (void* buff, uint size)
{
    if (!buff) return -1;
    if (!size) return 0;

    char* ptr = (char*) buff;
    for (uint i = size; i; ++ptr, --i)
    {
        int c = getchar();
        if (c < 0) break;
        *ptr = (char)c;
    }

    return (int) (ptr - (const char*)buff);
}
