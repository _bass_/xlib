//*****************************************************************************
//
// @brief   VFS subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "fs/vfs.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Partitions table
    static const TFsDesc* g_pFstab;
    uint g_fstabSize;

    // Table of mounted partitions (FS objects)
    static CFs** g_tblMount = NULL;


/**
  * @brief  Initialization
  * @param  pFstab: Pointer to partitions table
  * @param  size: Number of entries in the table
  */
void VFS::Init (const TFsDesc* pFstab, uint size)
{
    g_pFstab = pFstab;
    g_fstabSize = size;

    assert(g_tblMount);

    g_tblMount = (CFs**) malloc(sizeof(CFs*) * size);
    assert(!g_tblMount);

    memset(g_tblMount, 0, sizeof(CFs*) * size);
}


/**
  * @brief  Deinitialization
  * @param  None
  */
void VFS::Uninit (void)
{
    if (!g_tblMount) return;

    for (uint i = 0; i < g_fstabSize; ++i)
    {
        CFs* fs = g_tblMount[i];
        g_tblMount[i] = NULL;

        if (!fs) continue;

        fs->Umount();
        delete fs;
    }

    free(g_tblMount);
    g_tblMount = NULL;
}


/**
  * @brief  FS creation in partition (formatting)
  * @param  partId: Partition ID (partition index in partitions table)
  * @return 0 or negative error code
  */
int VFS::Mkfs (uint partId)
{
    if (partId >= g_fstabSize) return -1;

    CFs* fs;
    bool isMounted = false;

    if (g_tblMount[partId])
    {
        fs = g_tblMount[partId];
        isMounted = true;
    }
    else
    {
        fs = CFs::build(&g_pFstab[partId]);
    }

    if (!fs) return -2;

    int result = fs->Format();

    if (!isMounted)
    {
        delete fs;
    }

    return result;
}


/**
  * @brief  Partition mounting
  * @param  partId: Partition ID (partition index in partitions table)
  * @return 0 or negative error code
  */
int VFS::Mount (uint partId)
{
    if (partId >= g_fstabSize) return -1;

    int index = partId;

    // Mounting check
    if (g_tblMount[index]) return 1;

    CFs* fs = CFs::build(&g_pFstab[index]);
    if (!fs) return -3;

    g_tblMount[index] = fs;

    int result = fs->Mount();

    if (result < 0)
    {
        g_tblMount[index] = NULL;
        delete fs;
        return -5;
    }

    return 0;
}


/**
  * @brief  Partition unmounting
  * @param  partId: Partition ID (partition index in partitions table)
  * @return 0 or negative error code
  */
int VFS::Umount (uint partId)
{
    if (partId >= g_fstabSize) return -1;

    int index = partId;

    // Mounting check
    if (!g_tblMount[index]) return 1;

    CFs* fs = g_tblMount[index];

    int result = fs->Umount();
    if (result < 0) return result;

    g_tblMount[index] = NULL;
    delete fs;

    return 0;
}


/**
  * @brief  Get pointer to partition FS driver object
  * @param  partId: Partition ID (partition index in partitions table)
  * @return Pointer to driver object or NULL
  */
CFs* VFS::GetFs (uint partId)
{
    return ( (partId < g_fstabSize) && g_tblMount[partId] ) ? g_tblMount[partId] : NULL;
}
