//*****************************************************************************
//
// @brief   Class for working with files
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "fs/vfs.h"
#include "memory/heap.h"


/**
  * @brief  Constructor
  * @param  None
  */
CFile::CFile () :
    m_pFs(NULL)
{
}


/**
  * @brief  Open file
  * @param  partId: Partition ID (partition index in partitions table)
  * @param  name: File name (path)
  * @param  mode: Opening mode (see FILE_MODE_xx)
  * @return File descriptor index or negative error code
  */
int CFile::Open (uint partId, const char* name, uint mode)
{
    if (m_pFs) return -1;

    m_pFs = VFS::GetFs(partId);
    if (!m_pFs) return -2;

    int fd = m_pFs->Fopen(name, mode);
    if (fd < 0)
    {
        m_pFs = NULL;
        return fd;
    }

    m_fd = fd;
    m_offset = 0;

    return 0;
}


/**
  * @brief  Close file
  * @return 0 or negative error code
  */
int CFile::Close (void)
{
    if (!m_pFs) return -1;

    int result = m_pFs->Fclose(m_fd);

    m_pFs = NULL;
    m_fd = -1;
    m_offset = 0;

    return result;
}


/**
  * @brief  Read file data
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Number of bytes written in buff or negative error code
  */
int CFile::Read (void* buff, uint size)
{
    if (!m_pFs) return -1;

    return m_pFs->Fread(m_fd, buff, size, m_offset);
}


/**
  * @brief  Write data into file
  * @param  data: Data to write
  * @param  size: Data size
  * @return Number of written bytes or negative error code
  */
int CFile::Write (const void* data, uint size)
{
    if (!m_pFs) return -1;

    return m_pFs->Fwrite(m_fd, data, size, m_offset);
}


/**
  * @brief  Move opened file cursor
  * @param  offset: Number of bytes to move for
  * @param  origin: Base point for offset calculation
  * @return 0 or negative error code
  */
int CFile::Seek (uint offset, uint origin)
{
    if (!m_pFs) return -1;

    switch (origin)
    {
        case FILE_SEEK_CUR: offset += m_offset; break;
        case FILE_SEEK_END:
        {
            TFileInfo info;
            int result = m_pFs->Fstat(m_fd, &info);
            if (result < 0) return -1;
            if (info.size < offset) return -2;

            offset = info.size - offset; break;
        }
        break;

        default:
        case FILE_SEEK_SET: break;
    }

    m_offset = offset;

    return 0;
}


/**
  * @brief  Read information about opened file
  * @param  pInfo: Pointer to variable to save file info
  * @return 0 or negative error code
  */
int CFile::Stat (TFileInfo* pInfo)
{
    if (!m_pFs) return -1;

    return m_pFs->Fstat(m_fd, pInfo);
}
