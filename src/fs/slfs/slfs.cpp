//*****************************************************************************
//
// @brief   SLFS driver (Simple Log File System)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "fs/slfs/slfs.h"
#include "fs/vfs.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define SLFS_MAGIC_PARTITION                    ( (uint32)'S' | 'L' << 8 | 'F' << 16 | 'S' << 24 )
    #define SLFS_MAGIC_FILE                         ( (uint16)'F' | 'M' << 8 )

    // Number of service blocks at the begining of a partition
    #define SLFS_SERVICE_BLOCKS_COUNT               2

    // Address for unsaved file
    #define SLFS_ADDR_NOT_VALID                     0

    #define SLFS_BLOCK_NOT_VALID_INDEX              ( ~0u )


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static int  parseMap (const char* pMap, uint size);
    static void buildMap (uint lastZeroBit, char* buff, uint size);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    FS_REGISTER(FS_TYPE_SLFS, CSlfs::buildFsObject);

    static int g_fd = 0;


/**
  * @brief  Find last 0 position in map
  * @param  pMap: Pointer to map data
  * @param  size: Map size (bytes)
  * @return Index of last 0 or negative error code
  */
int parseMap (const char* pMap, uint size)
{
    if (!size) return -1;

    const int lastBitIndex = size * 8 - 1;

    for (int lastZeroBit = -1; lastZeroBit < lastBitIndex; lastZeroBit += 8, ++pMap)
    {
        if (*pMap == 0) continue;

        for (uint i = 0; i < 8; ++i)
        {
            if ( (*pMap & (1 << i)) == 0 ) continue;

            // -1 because searching proceed until bit with value 1 will be found
            // That means 0 position is previous one
            return (lastZeroBit + i);
        }
    }

    return -10;
}


/**
  * @brief  Create map
  * @param  lastZeroBit: PIndex of last 0 in the map
  * @param  buff: BUffer for map creation
  * @param  size: Buffer size (bytes)
  */
void buildMap (uint lastZeroBit, char* buff, uint size)
{
    for (uint i = lastZeroBit; i; i -= 8, ++buff)
    {
        if (i >= 8)
        {
            *buff = 0;
            continue;
        }

        *buff = (0xff << i);
        break;
    }

    const uint offset = (lastZeroBit + 7) / 8;
    if (offset >= size) return;

    memset(buff, 0xff, size - offset);
}


/**
  * @brief  Constructor
  * @param  pDesc: FS descriptor of partition
  */
CSlfs::CSlfs (const TFsDesc* pDesc) :
    CFs(pDesc),
    m_currBlock(SLFS_BLOCK_NOT_VALID_INDEX),
    m_offset(0),
    m_mutex(),
    m_cache(NULL),
    m_cachedBlock(SLFS_BLOCK_NOT_VALID_INDEX)
{
    assert(!pDesc || pDesc->size <= SLFS_SERVICE_BLOCKS_COUNT);

    memset( &m_partHdr, 0, sizeof(m_partHdr) );
}


/**
  * @brief  Destructor
  * @param  None
  */
CSlfs::~CSlfs ()
{
    if ( isMounted() ) Umount();
    if (m_cache) free(m_cache);
}


/**
  * @brief  Create FS driver object
  * @param  pDesc: Pointer to FS descriptor
  * @return Pointer to created FS object or NULL
  */
CFs* CSlfs::buildFsObject (const TFsDesc* pDesc)
{
    return (pDesc) ? new CSlfs(pDesc) : NULL;
}


/**
  * @brief  Partition mounting
  * @return 0 or negative error code
  */
int CSlfs::Mount (void)
{
    if ( !isPartSizeValid() ) return -1;

    m_mutex.Lock();

    int result = -10;
    do
    {
        // Check is performed only after mutex lock 
        // because there might be second function call during mounting process
        if ( isMounted() )
        {
            result = 0;
            break;
        }

        int res = partHeaderRead();
        if (res) break;

        res = syncLastFileData();
        if (res) break;

        result = 0;
    } while (0);

    m_mutex.Unlock();

    return result;
}


/**
  * @brief  Partition unmounting
  * @return 0 or negative error code
  */
int CSlfs::Umount (void)
{
    while ( m_fileList.size() )
    {
        auto i = m_fileList.begin();
        Fclose(i->fd);
    }

    m_mutex.Lock();
    memset( &m_partHdr, 0x00, sizeof(m_partHdr) );
    m_fileList.clear();
    m_currBlock = SLFS_BLOCK_NOT_VALID_INDEX;
    m_offset = 0;
    m_mutex.Unlock();

    return 0;
}


/**
  * @brief  Partition formatting
  * @return 0 or negative error code
  */
int CSlfs::Format (void)
{
    if (m_pDesc->size <= SLFS_SERVICE_BLOCKS_COUNT) return -1;

    if ( !isPartSizeValid() ) return -2;

    m_mutex.Lock();

    int result = -10;
    do
    {
        // First service block erasing
        int res = blockWrite(NULL, 1, true);
        if (res) break;

        // Create relevant partition header
        res = blockWrite(NULL, 0, true);
        if (res) break;
        void* buff = blockRead(0);
        if (!buff) break;

        THdrPart* pHdr = (THdrPart*) buff;
        pHdr->magic = SLFS_MAGIC_PARTITION;
        pHdr->size = (uint16)m_pDesc->size;
        pHdr->count = 0;
        pHdr->isLooped = 0;

        res = blockWrite(buff, 0);
        if (res) break;

        // Erase whole partition
        CDevBlock* dev = (CDevBlock*) GetDevice(DEV_NAME_BLOCK, m_pDesc->devNum);
        if (!dev) break;
        res = dev->Erase(m_pDesc->offset + SLFS_SERVICE_BLOCKS_COUNT, m_pDesc->size - SLFS_SERVICE_BLOCKS_COUNT);
        if (res) break;

        if ( isMounted() )
        {
            m_partHdr = *pHdr;
            m_currBlock = SLFS_SERVICE_BLOCKS_COUNT;
        }
        else
        {
            memset( &m_partHdr, 0x00, sizeof(m_partHdr) );
            m_currBlock = SLFS_BLOCK_NOT_VALID_INDEX;
        }

        m_offset = 0;

        result = 0;
    } while (0);

    blockFree(0);
    m_mutex.Unlock();

    return result;
}


/**
  * @brief  Open directory
  * @param  name: Directory name (path, only '/' is allowed)
  * @return Directory descriptor index or negative error code
  */
int CSlfs::DirOpen (const char* name)
{
    if ( !isMounted() ) return -1;

    if (name[0] != '/' || name[1]) return -2;

    TFileDesc* pFdesc;
    m_mutex.Lock();
    int fd = fdCreate(&pFdesc);
    m_mutex.Unlock();

    if (fd < 0) return -3;

    pFdesc->mode = FILE_MODE_READ;
    pFdesc->addr = SLFS_ADDR_NOT_VALID;

    return fd;
}


/**
  * @brief  Close directory
  * @param  fd: Descriptor index
  * @return 0 or negative error code
  */
int CSlfs::DirClose (uint fd)
{
    return Fclose(fd);
}


/**
  * @brief  Read file info from the directory
  * @param  fd: Descriptor index
  * @param  pInfo: Pointer to variable to save file info
  * @return 0 or negative error code
  */
int CSlfs::DirRead (uint fd, TFileInfo* pInfo)
{
    if ( !isMounted() ) return -1;

    TFileDesc* pFDesc = fdGet(fd);
    if (!pFDesc) return -2;

    m_mutex.Lock();

    int result = -10;
    uint blockIndex = SLFS_BLOCK_NOT_VALID_INDEX;

    do
    {
        uint blockSize = getBlockSize();
        blockIndex = (pFDesc->addr == SLFS_ADDR_NOT_VALID) ? m_currBlock : pFDesc->addr / blockSize;

        void* pData = blockRead(blockIndex);
        if (!pData) break;

        uint hdrOffset;
        if (pFDesc->addr == SLFS_ADDR_NOT_VALID)
        {
            // Load info about last file
            hdrOffset = m_offset;
        }
        else
        {
            // Jump to previous file
            hdrOffset = pFDesc->addr % blockSize;
            if ( hdrOffset >= sizeof(THdrFile) )
            {
                hdrOffset -= sizeof(THdrFile);
            }
            else
            {
                blockFree(blockIndex);

                blockIndex = prevDataBlock(blockIndex);
                if (blockIndex == SLFS_BLOCK_NOT_VALID_INDEX) break;
                // Stop searching on returning to current block
                if (blockIndex == m_currBlock)
                {
                    blockIndex = SLFS_BLOCK_NOT_VALID_INDEX;
                    break;
                }

                pData = blockRead(blockIndex);
                if (!pData) break;

                const THdrFile* pHdr = findLastFile(pData, blockSize);
                if (!pHdr) break;
                hdrOffset = (const char*)pHdr - (const char*)pData;
            }
        }

        const THdrFile* pHdr = (const THdrFile*) ((char*)pData + hdrOffset);
        if ( !isFileHeaderValid(pHdr) ) break;

        pFDesc->addr = blockIndex * blockSize + hdrOffset;
        pFDesc->hdr = *pHdr;

        pInfo->name = (const char*)&pFDesc->hdr.id;
        pInfo->size = pFDesc->hdr.size;
        pInfo->time = pFDesc->hdr.time;

        result = 0;
    } while (0);

    if (blockIndex != SLFS_BLOCK_NOT_VALID_INDEX) blockFree(blockIndex);
    m_mutex.Unlock();

    return result;
}


/**
  * @brief  Open file
  * @param  name: File name (path)
  * @param  mode: Opening mode (see FILE_MODE_xx in fs_file.h)
  * @return File descriptor index or negative error code
  */
int CSlfs::Fopen (const char* name, uint mode)
{
    if ( !isMounted() ) return -1;
    if (!mode) return -2;

    m_mutex.Lock();

    int result = -10;
    int fd;

    do
    {
        TFileDesc* pFdesc;
        fd = fdCreate(&pFdesc);
        if (fd < 0) break;

        pFdesc->mode = mode;

        if ( (mode & FILE_MODE_CREATE) )
        {
            // Create new file (id will be created automaticaly)
            pFdesc->hdr.magic = SLFS_MAGIC_FILE;
            pFdesc->hdr.size = 0;
            pFdesc->hdr.id = 0;
            pFdesc->hdr.dataOffset = 0;

            TTime timestamp;
            time(&timestamp);
            pFdesc->hdr.time = timestamp;

            pFdesc->addr = SLFS_ADDR_NOT_VALID;
        }
        else
        {
            // Open exesting file
            uint32 id;
            memcpy( &id, name, sizeof(id) );

            uint32 addr = findFile(id, &pFdesc->hdr);
            if ( addr == SLFS_ADDR_NOT_VALID || !isFileHeaderValid(&pFdesc->hdr) ) break;

            pFdesc->addr = (uint32)addr;
        }

        result = 0;
    } while (0);

    if (result < 0)
    {
        fdRemove(fd);
        fd = -11;
    }

    m_mutex.Unlock();

    return fd;
}


/**
  * @brief  Close file
  * @param  fd: Opened file descriptor
  * @return 0 or negative error code
  */
int CSlfs::Fclose (uint fd)
{
    TFileDesc* pFdesc = fdGet(fd);
    if (!pFdesc) return -1;

    m_mutex.Lock();
    fdRemove(fd);
    m_mutex.Unlock();

    return 0;
}


/**
  * @brief  Read file data
  * @param  fd: Opened file descriptor
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @param  offset: File data offset (from the beginning)
  * @return Number of bytes written in buff or negative error code
  */
int CSlfs::Fread (uint fd, void* buff, uint size, uint offset)
{
    if ( !isMounted() ) return -1;

    TFileDesc* pFdesc = fdGet(fd);
    if (!pFdesc) return -2;

    if ( !(pFdesc->mode & FILE_MODE_READ) ) return -3;
    if (pFdesc->addr == SLFS_ADDR_NOT_VALID || !isFileHeaderValid(&pFdesc->hdr) ) return -4;
    if (offset >= pFdesc->hdr.size) return -5;

    m_mutex.Lock();
    int result = -10;

    do
    {
        uint blockSize = getBlockSize();
        uint blockIndex = pFdesc->addr / blockSize;

        char* pData = (char*) blockRead(blockIndex);
        if (!pData) break;

        pData += pFdesc->addr % blockSize + sizeof(THdrFile) + pFdesc->hdr.dataOffset + offset;

        uint sizeToCopy = pFdesc->hdr.size - offset;
        if (sizeToCopy > size) sizeToCopy = size;

        memcpy(buff, pData, sizeToCopy);

        blockFree(blockIndex);
        result = (int)sizeToCopy;
    } while (0);

    m_mutex.Unlock();

    return result;
}


/**
  * @brief  Write data into file
  * @param  fd: Opened file descriptor
  * @param  data: Data to write
  * @param  size: Data size
  * @param  offset: Write offset (from the file beginning)
  * @return Number of written bytes or negative error code
  */
int CSlfs::Fwrite (uint fd, const void* data, uint size, uint offset)
{
    if ( !isMounted() ) return -1;

    TFileDesc* pFdesc = fdGet(fd);
    if (!pFdesc) return -2;
    if ( !(pFdesc->mode & FILE_MODE_WRITE) ) return -3;

    m_mutex.Lock();

    uint blockSize = getBlockSize();
    uint32 blockIndex = (pFdesc->addr == SLFS_ADDR_NOT_VALID) ? m_currBlock : pFdesc->addr / blockSize;
    int result = -10;

    do
    {
        void* pData = blockRead(blockIndex);
        if (!pData) break;

        if (pFdesc->mode & FILE_MODE_CREATE)
        {
            result = -20;

            // Detect input parameters for new file
            uint32 id;
            uint freeSize;
            char* buff;

            THdrFile* pHdr = (THdrFile*)((char*)pData + m_offset);
            if ( isFileHeaderValid(pHdr) )
            {
                id = pHdr->id + 1;
                freeSize = pHdr->dataOffset;
                buff = (char*)(pHdr + 1);
            }
            else
            {
                id = 1;
                freeSize = blockSize;
                buff = (char*)pData;
            }

            if (!id) ++id;

            // Check if new file data may be placed in current block
            if ( buff < pData || (buff + freeSize) > ((char*)pData + blockSize) ) break;

            const uint fileSize = offset + size;
            if (freeSize < sizeof(THdrFile) + fileSize)
            {
                // The is not enought space in current block - jump to next one.
                blockFree(blockIndex);
                pData = NULL;

                blockIndex = nextDataBlock(m_currBlock);
                if (blockIndex <= m_currBlock) m_partHdr.isLooped = 1;

                int res = blockWrite(NULL, blockIndex, true);
                if (res) break;

                res = partHeaderSave(blockIndex);
                if (res) break;

                m_currBlock = blockIndex;
                m_offset = 0;

                pData = blockRead(blockIndex);
                if (!pData) break;

                freeSize = blockSize;
                buff = (char*)pData;

                if (freeSize < sizeof(THdrFile) + fileSize) break;
            }

            pHdr = (THdrFile*)buff;
            pHdr->magic = SLFS_MAGIC_FILE;
            pHdr->size = fileSize;
            pHdr->time = pFdesc->hdr.time;
            pHdr->id = id;
            pHdr->dataOffset = freeSize - sizeof(THdrFile) - fileSize;

            int res = blockWrite(pData, blockIndex);
            if (res) break;

            m_offset = buff - (char*)pData;

            pFdesc->hdr = *pHdr;
            pFdesc->addr = blockIndex * blockSize + (char*)pHdr - (char*)pData;
        }

        result = -30;

        if ( pFdesc->addr == SLFS_ADDR_NOT_VALID || !isFileHeaderValid(&pFdesc->hdr) ) break;
        // Only editing without file size changing is allowed because header data was already saved
        if (offset + size > pFdesc->hdr.size) break;

         void* ptr = (void*) ((char*)pData + pFdesc->addr % blockSize + sizeof(THdrFile) + pFdesc->hdr.dataOffset + offset);
        memcpy(ptr, data, size);

        int res = blockWrite(pData, blockIndex);
        if (res) break;

        result = 0;
    } while (0);

    blockFree(blockIndex);

    m_mutex.Unlock();

    return (result) ? result : size;
}


/**
  * @brief  Read information about opened file
  * @param  fd: Opened file descriptor
  * @param  pInfo: Pointer to variable to save file info
  * @return 0 or negative error code
  */
int CSlfs::Fstat (uint fd, TFileInfo* pInfo)
{
    TFileDesc* pFdesc = fdGet(fd);
    if (!pFdesc) return -1;

    const THdrFile* pHdr = &pFdesc->hdr;
    if ( !isFileHeaderValid(pHdr) ) return -2;

    pInfo->name = (const char*) &pHdr->id;
    pInfo->time = pHdr->time;
    pInfo->size = pHdr->size;

    return 0;
}


/**
  * @brief  Partition header validation
  * @param  pHdr: Partition header
  * @return true - header is valid
  */
bool CSlfs::isPartHeaderValid (const THdrPart* pHdr)
{
    return (
        pHdr->magic == SLFS_MAGIC_PARTITION &&
        pHdr->size > SLFS_SERVICE_BLOCKS_COUNT &&
        (!m_pDesc->size || pHdr->size == m_pDesc->size)
    );
}


/**
  * @brief  Partition size validation
  * @return true - Partition size is valid
  */
bool CSlfs::isPartSizeValid (void)
{
    uint mapSize = (m_pDesc->size - SLFS_SERVICE_BLOCKS_COUNT + 7) / 8;
    uint blockSize = getBlockSize();

    return (blockSize >= sizeof(THdrPart) + mapSize);
}


/**
  * @brief  Reading current partition header
  * @return 0 or negative error code
  */
int CSlfs::partHeaderRead (void)
{
    THdrPart secondHdr;

    memset( &secondHdr, 0x00, sizeof(secondHdr) );
    memset( &m_partHdr, 0x00, sizeof(m_partHdr) );

    for (uint i = 0; i < SLFS_SERVICE_BLOCKS_COUNT; blockFree(i), ++i)
    {
        THdrPart* pHdr = (THdrPart*) blockRead(i);
        if (!pHdr) continue;

        if (!isPartHeaderValid(pHdr) || (pHdr->count & 0x01) != i) continue;

        if (i)  secondHdr = *pHdr;
        else    m_partHdr = *pHdr;
    }

    if ( isPartHeaderValid(&m_partHdr) && isPartHeaderValid(&secondHdr) )
    {
        if (secondHdr.count + 1 == m_partHdr.count)
        {
            // m_partHdr contains valid header
        }
        // If sequence was broken, header with higher counter value is selected
        else if (m_partHdr.count + 1 == secondHdr.count || secondHdr.count > m_partHdr.count)
        {
            m_partHdr = secondHdr;
        }
    }
    else if ( isPartHeaderValid(&secondHdr) )
    {
        m_partHdr = secondHdr;
    }

    return ( isPartHeaderValid(&m_partHdr) ) ? 0 : -1;
}


/**
  * @brief  Save (write) current partition header on disk
  * @param  currBlock: Current (edited) data block in partition
  * @return 0 or negative error code
  */
int CSlfs::partHeaderSave (uint currBlock)
{
    if (currBlock < SLFS_SERVICE_BLOCKS_COUNT) return -1;

    CDevBlock* dev = (CDevBlock*) GetDevice(DEV_NAME_BLOCK, m_pDesc->devNum);
    assert(!dev);

    uint blockIndex = m_partHdr.count & 0x01;
    if (currBlock == SLFS_SERVICE_BLOCKS_COUNT)
    {
        ++m_partHdr.count;
        blockIndex = m_partHdr.count & 0x01;
        dev->Erase(m_pDesc->offset + blockIndex);
    }

    void* pData = blockRead(blockIndex);
    if (!pData) return -2;

    int result = -10;
    do
    {
        THdrPart* pHdr = (THdrPart*) pData;
        memcpy( pHdr, &m_partHdr, sizeof(m_partHdr) );

        char* pMap = (char*) (pHdr + 1);
        const uint mapSize = (m_partHdr.size - SLFS_SERVICE_BLOCKS_COUNT + 7) / 8;
        buildMap(currBlock - SLFS_SERVICE_BLOCKS_COUNT, pMap, mapSize);

        result = blockWrite(pData, blockIndex);
        if (result < 0) break;

        result = 0;
    } while (0);

    blockFree(blockIndex);

    return result;
}


/**
  * @brief  Read block data (with caching)
  * @param  index: Block index in partition
  * @return Pointer to read data or NULL
  */
void* CSlfs::blockRead (uint index)
{
    if (index >= m_pDesc->size) return NULL;

    CDevBlock* dev = (CDevBlock*) GetDevice(DEV_NAME_BLOCK, m_pDesc->devNum);
    assert(!dev);
    assert(m_cache && m_cachedBlock != index);

    if (!m_cache)
    {
        uint tryCount = 5;
        while (tryCount--)
        {
            m_cache = malloc( dev->BlockSize() );
            if (m_cache) break;
            KThread::sleep(10);
        }

        if (!m_cache) return NULL;
    }

    if (m_cachedBlock != index)
    {
        int result = dev->Read(m_cache, m_pDesc->offset + index);
        if (result < 0) return NULL;
        m_cachedBlock = index;
    }

    return m_cache;
}


/**
  * @brief  Freeing cached block data
  * @param  index: Block index in partition
  */
void CSlfs::blockFree (uint index)
{
    if (m_cachedBlock != index) return;

    Kernel::EnterCritical();
    {
        free(m_cache);
        m_cache = NULL;
        m_cachedBlock = SLFS_BLOCK_NOT_VALID_INDEX;
    }
    Kernel::ExitCritical();
}


/**
  * @brief  Write block data
  * @param  data: Block data
  * @param  index: Block index in partition
  * @param  erase: Flag of required erasing before writing data
  * @return 0 or negative error code
  */
int CSlfs::blockWrite (void* data, uint index, bool erase)
{
    if (index >= m_pDesc->size) return -1;

    CDevBlock* dev = (CDevBlock*) GetDevice(DEV_NAME_BLOCK, m_pDesc->devNum);
    assert(!dev);

    index += m_pDesc->offset;
    int result = 0;

    if (erase)
    {
        result = dev->Erase(index);
        if (result) return -2;
    }

    if (data)
    {
        result = dev->Write(data, index);
    }

    // Synchronize data with cache
    if (m_cache && m_cachedBlock == (index - m_pDesc->offset) && data != m_cache)
    {
        Kernel::EnterCritical();
        if (data)
        {
            memcpy( m_cache, data, dev->BlockSize() );
        }
        else if (erase)
        {
            memset( m_cache, 0xff, dev->BlockSize() );
        }
        Kernel::ExitCritical();
    }

    return result;
}


/**
  * @brief  File descriptor creation
  * @param  pDescVar: Pointer to save created descriptor
  * @return Descriptor index or negative error code
  */
int CSlfs::fdCreate (TFileDesc** pDescVar)
{
    if (!pDescVar) return -1;

    TFileDesc desc;
    desc.addr = SLFS_ADDR_NOT_VALID;
    desc.mode = 0;
    desc.fd = g_fd++;
    memset( &desc.hdr, 0, sizeof(desc.hdr) );

    m_fileList.push_back(desc);

    *pDescVar = fdGet(desc.fd);

    return desc.fd;
}


/**
  * @brief  File descriptor removing
  * @param  fd: File descriptor index
  * @return 0 or negative error code
  */
int CSlfs::fdRemove (int fd)
{
    if ( fd < 0 || !m_fileList.size() ) return -1;

    for (auto i = m_fileList.begin(); i != m_fileList.end(); ++i)
    {
        if (i->fd != fd) continue;

        m_fileList.erase(i);
        return 0;
    }

    return -2;
}


/**
  * @brief  Get file descriptor data
  * @param  fd: File descriptor index
  * @return Pointer to descriptor data or NULL
  */
CSlfs::TFileDesc* CSlfs::fdGet (int fd)
{
    if ( fd < 0 || !m_fileList.size() ) return NULL;

    for (auto i = m_fileList.begin(); i != m_fileList.end(); ++i)
    {
        if (i->fd != fd) continue;

        TFileDesc* pDesc = &*i;
        return ( isFileDescValid(pDesc) ) ? pDesc : NULL;
    }

    return NULL;
}


/**
  * @brief  Find file with specified ID (name)
  * @param  id: File ID
  * @param  pHdr: Pointer to save file header
  * @return File offset in partition or SLFS_ADDR_NOT_VALID on error
  */
uint32 CSlfs::findFile (uint32 id, THdrFile* pHdr)
{
    // Searching in valid files descriptors
    if ( m_fileList.size() )
    {
        for (auto i = m_fileList.cbegin(); i != m_fileList.cend(); ++i)
        {
            if ( !isFileHeaderValid(&i->hdr) ) continue;
            if (i->hdr.id != id) continue;

            if (pHdr) *pHdr = i->hdr;

            return i->addr;
        }
    }

    // Search block with needed ID by first file ID in a block
    uint32 lastId = ~0u;
    uint blockIndex = m_currBlock;
    uint32 fileAddr = SLFS_ADDR_NOT_VALID;
    uint blockSize = getBlockSize();

    for (uint i = m_pDesc->size - SLFS_SERVICE_BLOCKS_COUNT; i; --i)
    {
        const THdrFile* pFirstHdr = (const THdrFile*) blockRead(blockIndex);
        if ( !pFirstHdr || !isFileHeaderValid(pFirstHdr) ) break;

        bool targetInCurrSector = (pFirstHdr->id < lastId && id >= pFirstHdr->id && id < lastId)
                                  || (pFirstHdr->id > lastId && (id >= pFirstHdr->id || id < lastId));

        if (!targetInCurrSector)
        {
            lastId = pFirstHdr->id;
            blockFree(blockIndex);

            blockIndex = prevDataBlock(blockIndex);
            if (blockIndex == SLFS_BLOCK_NOT_VALID_INDEX || blockIndex == m_currBlock) break;

            continue;
        }

        // Seacrh file in current block
        const THdrFile* ptr = pFirstHdr;
        const THdrFile* pEnd = (const THdrFile*) ((char*)pFirstHdr + blockSize - sizeof(THdrFile));
        for (; ptr < pEnd; ++ptr)
        {
            if ( !isFileHeaderValid(ptr) ) break;
            if (ptr->id != id) continue;

            if (pHdr) *pHdr = *ptr;
            fileAddr = blockIndex * blockSize + ((const char*)ptr - (const char*)pFirstHdr);
            break;
        }

        break;
    }

    blockFree(blockIndex);

    return fileAddr;
}


/**
  * @brief  Find last file in block
  * @param  blockData: Block data for searching
  * @param  size: Block data size
  * @return Pointer to last file in the block or NULL
  */
CSlfs::THdrFile* CSlfs::findLastFile (void* blockData, uint size)
{
    void* pEnd = (char*)blockData + size - sizeof(THdrFile);
    for (THdrFile* pHdr = (THdrFile*)blockData; pHdr < pEnd; ++pHdr)
    {
        if ( isFileHeaderValid(pHdr) ) continue;
        if (pHdr == blockData) break;

        return (pHdr - 1);
    }

    return NULL;
}


/**
  * @brief  File header validation
  * @param  pHdr: File header
  * @return true - the header is valid
  */
bool CSlfs::isFileHeaderValid (const THdrFile* pHdr)
{
    return (pHdr->magic == SLFS_MAGIC_FILE);
}


/**
  * @brief  File descriptor data validation
  * @param  pDesc: File descriptor
  * @return true - the descriptor is valid
  */
bool CSlfs::isFileDescValid (const TFileDesc* pDesc)
{
    return (pDesc->fd >= 0);
}


/**
  * @brief  Check partition mounting state
  * @return true - partition is mounted
  */
bool CSlfs::isMounted (void)
{
    return (
        isPartHeaderValid(&m_partHdr) &&
        m_currBlock != SLFS_BLOCK_NOT_VALID_INDEX &&
        m_currBlock >= SLFS_SERVICE_BLOCKS_COUNT
    );
}


/**
  * @brief  Read data about last file (block number and offset) from disc
  * @return 0 or negative error code
  */
int CSlfs::syncLastFileData (void)
{
    uint currBlock = SLFS_BLOCK_NOT_VALID_INDEX;
    uint offset = 0;

    do
    {
        uint blockIndex = m_partHdr.count & 0x01;

        void* data = blockRead(blockIndex);
        if (!data) break;

        const THdrPart* pPartHdr = (const THdrPart*) data;
        int lastBlockIndex = parseMap( (const char*)(pPartHdr + 1), (pPartHdr->size + 7) / 8 );
        blockFree(blockIndex);

        if (lastBlockIndex >= m_partHdr.size - SLFS_SERVICE_BLOCKS_COUNT) break;

        if (lastBlockIndex < 0) lastBlockIndex = 0;
        lastBlockIndex += SLFS_SERVICE_BLOCKS_COUNT;

        // Find last file offset
        data = blockRead(lastBlockIndex);
        if (!data) break;

        uint blockSize = getBlockSize();
        THdrFile* pFileHdr = findLastFile(data, blockSize);
        blockFree(lastBlockIndex);

        offset = (pFileHdr) ? ( (char*)pFileHdr - (char*)data ) : 0;
        currBlock = lastBlockIndex;

    } while (0);

    if (currBlock == SLFS_BLOCK_NOT_VALID_INDEX) return -1;

    m_currBlock = currBlock;
    m_offset = offset;

    return 0;
}


/**
  * @brief  Get block size
  * @return Block size (bytes)
  */
uint CSlfs::getBlockSize (void)
{
    CDevBlock* dev = (CDevBlock*) GetDevice(DEV_NAME_BLOCK, m_pDesc->devNum);
    assert(!dev);
    uint blockSize = dev->BlockSize();
    assert(!blockSize);

    return blockSize;
}


/**
  * @brief  Get previous block index (with looping)
  * @return Block index or SLFS_BLOCK_NOT_VALID_INDEX on error
  */
uint CSlfs::prevDataBlock (uint index)
{
    uint prevBlock;
    if (index > SLFS_SERVICE_BLOCKS_COUNT)
    {
        prevBlock = index - 1;
    }
    else
    {
        if (!m_partHdr.isLooped) return SLFS_BLOCK_NOT_VALID_INDEX;

        prevBlock = m_pDesc->size - 1;
    }

    return prevBlock;
}


/**
  * @brief  Get next block index (with looping)
  * @return Block index
  */
uint CSlfs::nextDataBlock (uint index)
{
    return (index < m_pDesc->size - 1) ? index + 1 : SLFS_SERVICE_BLOCKS_COUNT;
}
