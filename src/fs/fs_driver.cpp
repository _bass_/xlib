//*****************************************************************************
//
// @brief   FS driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "fs/vfs.h"
#include "memory/heap.h"
#include "misc/macros.h"


/**
  * @brief  Constructor
  * @param  pDesc: FS descriptor of partition
  */
CFs::CFs (const TFsDesc* pDesc) :
    m_pDesc(pDesc)
{
    assert(!m_pDesc);
}


/**
  * @brief  Destructor
  * @param  None
  */
CFs::~CFs ()
{
    // Umount???
}


/**
  * @brief  Create FS driver object
  * @param  pDesc: Pointer to FS descriptor
  * @return Pointer to created FS object or NULL
  */
CFs* CFs::build (const TFsDesc* pDesc)
{
    SECTION_FOREACH(xLib_fsList, const TFsRegDesc, itr)
    {
        if (itr->type != pDesc->fsType) continue;
        if (!itr->hdl) continue;

        return itr->hdl(pDesc);
    }

    return NULL;
}
