//*****************************************************************************
//
// @brief   Class for working with directories
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "fs/vfs.h"


/**
  * @brief  Constructor
  * @param  None
  */
CDir::CDir () :
    m_pFs(NULL)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CDir::~CDir ()
{
    if (m_pFs)
    {
        Close();
    }
}


/**
  * @brief  Open directory
  * @param  partId: Partition ID (partition index in partitions table)
  * @param  name: Directory name (path)
  * @return 0 or negative error code
  */
int CDir::Open (uint partId, const char* name)
{
    if (m_pFs) return -1;
    
    m_pFs = VFS::GetFs(partId);
    if (!m_pFs) return -2;
    
    int fd = m_pFs->DirOpen(name);
    if (fd < 0)
    {
        m_pFs = NULL;
        return fd;
    }
    
    m_fd = fd;

    return 0;
}


/**
  * @brief  Close directory
  * @return 0 or negative error code
  */
int CDir::Close (void)
{
    if (!m_pFs) return -1;

    int result = m_pFs->DirClose(m_fd);
    
    m_pFs = NULL;
    m_fd = -1;

    return result;
}


/**
  * @brief  Read file info from the directory
  * @param  pInfo: Pointer to variable to save file info
  * @return 0 or negative error code
  */
int CDir::Read (TFileInfo* pInfo)
{
    if (!m_pFs) return -1;
    
    return m_pFs->DirRead(m_fd, pInfo);
}
