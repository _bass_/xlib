//*****************************************************************************
//
// @brief   Input device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/input.h"


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #define IS_EV_SUPPORTED(type)   (m_supportedEvents & (type))


/**
 * @brief Constructor
 * @param devIndex: System index of the device
 * @param supportedEvents: Mask of supported event types
 */
CDevInput::CDevInput (uint devIndex, uint supportedEvents):
    CDevice(DEV_NAME_INPUT, devIndex),
    m_supportedEvents(supportedEvents)
{
}


/**
 * @brief Subscribe handler to get specified event notifications
 * @param msgType: Event type
 * @param hdl: Events handler
 * @param param: Events handler additional parameter to pass
 */
void CDevInput::Subscribe (EMsgType msgType, TMsgHdl hdl, void* param)
{
    if (!hdl || !IS_EV_SUPPORTED(msgType)) return;

    for (auto itr = m_handlers.cbegin(); itr != m_handlers.cend(); ++itr)
    {
        if (itr->type == msgType && itr->hdl == hdl) return;
    }

    m_handlers.emplace_front(msgType, hdl, param);
}


/**
 * @brief Unsubscribe handler to stop getting specified event notifications
 * @param msgType: Event type
 * @param hdl: Events handler
 * @param param: Events handler additional parameter to pass
 */
void CDevInput::Unsubscribe (EMsgType msgType, TMsgHdl hdl, void* param)
{
    if (!hdl || !IS_EV_SUPPORTED(msgType)) return;

    for (auto itr = m_handlers.cbegin(); itr != m_handlers.cend(); ++itr)
    {
        if (itr->type != msgType || itr->hdl != hdl || itr->param != param) continue;

        m_handlers.erase_after(itr);
        break;
    }
}


/**
 * @brief Send event notification
 * @param msg: Event message data
 */
void CDevInput::notify (const TMsg& msg)
{
    if (!IS_EV_SUPPORTED(msg.type)) return;

    for (auto itr = m_handlers.cbegin(); itr != m_handlers.cend(); ++itr)
    {
        if (itr->type != msg.type || !itr->hdl) continue;

        itr->hdl(this, msg, itr->param);
    }
}
