//*****************************************************************************
//
// @brief   Basic device interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "dev/device.h"
#include <list>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Borders of device object list
    CC_SECTION_DECLARE(xLib_devList)
    #define DEVLIST_ADDR_START          CC_SECTION_ADDR_START(xLib_devList)
    #define DEVLIST_ADDR_END            CC_SECTION_ADDR_END(xLib_devList)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #if !defined(__ICCARM__) || _HAS_NAMESPACE
    using std::list;
    #endif


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // List of dynamic created devices
    static list<TDevDesc> g_dynDevList;


/**
  * @brief  Constructor
  * @param  name: Pointer to device name (string with 3 symbols)
  * @param  index: Device number
  */
CDevice::CDevice (const char* name, uint index)
{
    const CDevice* pDev = GetDevice(name, index);
    // Adding the device to list of dynamic created devices
    if (!pDev)
    {
        TDevDesc desc = {.id = DEV_ID(name, index), .pDev = (void*)this};
        IRQ::Lock();
        g_dynDevList.push_back(desc);
        IRQ::Unlock();
    }
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevice::~CDevice ()
{
    IRQ::Lock();
    for (list<TDevDesc>::iterator i = g_dynDevList.begin(); i != g_dynDevList.end(); ++i)
    {
        if (i->pDev != this) continue;
        g_dynDevList.erase(i);
        break;
    }
    IRQ::Unlock();
}


/**
  * @brief  Find device object
  * @param  name: Device name
  * @param  index: Device index
  * @return Pointer to device object or NULL on error
  */
const CDevice* GetDevice (const char* name, uint index)
{
    uint32 id = DEV_ID(name, index);

    SECTION_FOREACH(xLib_devList, const TDevDesc, ptr)
    {
        if (ptr->id == id) return (const CDevice*)ptr->pDev;
    }

    // Searching in dynamic list
    for (list<TDevDesc>::const_iterator i = g_dynDevList.begin(); i != g_dynDevList.end(); ++i)
    {
        if (i->id == id) return (const CDevice*)i->pDev;
    }

    return NULL;
}


/**
  * @brief  Get list of registered devices
  * @param  pEnd: Pointer to the end of registered devices list (after last descriptor)
  * @return Pointer to first device descriptor in the list
  */
const TDevDesc* GetDevList (const TDevDesc** pEnd)
{
    if (pEnd) *pEnd = (TDevDesc*) DEVLIST_ADDR_END;

    return (TDevDesc*) DEVLIST_ADDR_START;
}
