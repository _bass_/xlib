//*****************************************************************************
//
// @brief   Dummy character device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/null.h"
#include <string.h>


/**
  * @brief  Constructor
  * @param  None
  */
CDevNull::CDevNull () :
    CDevChar()
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevNull::~CDevNull ()
{
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (TOptions)
  * @return DEV_OK or negative error code
  */
int CDevNull::Configure (const void* params)
{
    const TOptions* cfg = (const TOptions*) params;

    m_buff = (char*) cfg->pBuff;
    m_size = cfg->size;
    m_index = 0;

    return DEV_OK;
}


/**
  * @brief  Send character to output stream
  * @param  c: character
  * @return 0 or negative error code
  */
int CDevNull::PutChar (const char c)
{
    if (!m_buff || m_index >= m_size)
    {
        return DEV_IO_ERR;
    }

    m_buff[m_index++] = c;

    return 0;
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CDevNull::GetChar (void)
{
    if (!m_buff || !m_index || m_index > m_size)
    {
        return DEV_IO_ERR;
    }

    return m_buff[--m_index];
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevNull::Write (const void* buff, uint size)
{
    if (!m_buff || m_index >= m_size || !size)
    {
        return DEV_IO_ERR;
    }

    if (m_index + size > m_size)
    {
        size = m_size - m_index;
    }

    memcpy(&m_buff[m_index], buff, size);
    m_index += size;

    return size;
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CDevNull::Read (void* buff, uint size)
{
    if (!m_buff || !m_index || m_index > m_size || !size)
    {
       return DEV_IO_ERR;
    }

    if (size > m_index)
    {
        size = m_index;
    }

    m_index -= size;
    memcpy(&m_buff[m_index], buff, size);

    return size;
}
