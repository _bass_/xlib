//*****************************************************************************
//
// @brief   USB device base functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/usbd.h"


// ----------------------------------------------------------------------------
// Macros
// ----------------------------------------------------------------------------

    #if 0
        #include "kernel/kstdio.h"
        #define debug(str)                  printf(str);
    #else
        #define debug(str)
    #endif


/**
  * @brief  Constructor
  * @param  None
  */
CDevUsbd::CDevUsbd () :
    m_pDesc(NULL),
    m_cfgNum(0),
    m_devAddr(0)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevUsbd::~CDevUsbd ()
{
}


/**
  * @brief  Set class-specific requests handler
  * @param  pHdl: Handler
  * @param  arg: Pointer to additional arguments for handler
  */
void CDevUsbd::SetRequestHandler (TCsRequestHdl* pHdl, void* arg)
{
    m_pCsRequestHdl = pHdl;
    m_pCsRequestHdlArg = arg;
}


/**
  * @brief  Searching for configuration descriptor
  * @param  num: Configuration number (0 means current configuration)
  * @return Pointer to configuration descriptor or NULL on error
  */
const TUsbCfgDescriptor* CDevUsbd::getDescCfg (uint num)
{
    if (!num) num = m_cfgNum;

    // Error: there is no active configuration
    if (!num) return NULL;

    uint index = num - 1;
    const TUsbCfgDescriptor* pCfg = m_pDesc->pCfg[index];

    return pCfg;
}


/**
  * @brief  Searching for endpoint descriptor
  * @param  epNum: Endpoint number
  * @return Pointer to endpoint descriptor or NULL on error
  */
const TUsbEpDescriptor* CDevUsbd::getDescEp (uint epNum)
{
    // EP0 hasn't any descriptor
    if (!epNum) return NULL;

    const TUsbCfgDescriptor* pCfg = getDescCfg();
    if (!pCfg) return NULL;

    // Searching for EP descriptor in current configuration
    const TUsbDescriptorHeader* pDesc = (TUsbDescriptorHeader*) (pCfg + 1);
    for (
         uint len = sizeof(TUsbCfgDescriptor);
         len < pCfg->wTotalLength;
         len += pDesc->bLength, pDesc = (const TUsbDescriptorHeader*)(pDesc->bLength + (uchar*)pDesc)
    )
    {
        if (pDesc->bDescriptorType != USB_DESCRIPTOR_ENDPOINT) continue;

        const TUsbEpDescriptor* pDescEp = (const TUsbEpDescriptor*) pDesc;
        if ( (pDescEp->bEndpointAddress & USB_EP_NUM_MASK) == epNum)
        {
            return pDescEp;
        }
    }

    return NULL;
}


/**
  * @brief  Handler of standard requests 
  * @param  request: Request descriptor
  * @return 0 or negative error code (see DEV_xx)
  */
int CDevUsbd::onRequest (const TUsbRequest* request)
{
    uchar reqType = request->bmRequestType & USB_REQUEST_TYPE_MASK;
    if (reqType != USB_REQUEST_STANDARD)
    {
        return (m_pCsRequestHdl)
                ? m_pCsRequestHdl(request, m_pCsRequestHdlArg)
                : DEV_ERR;
    }

    switch (request->bRequest)
    {
        case USB_REQUEST_GET_STATUS:        hdlReqGetStatus(request);     break;
        case USB_REQUEST_CLEAR_FEATURE:
        case USB_REQUEST_SET_FEATURE:       hdlReqFeature(request);       break;
        case USB_REQUEST_SET_ADDRESS:       hdlReqSetAddress(request);    break;
        case USB_REQUEST_GET_DESCRIPTOR:    hdlReqDescriptor(request);    break;
        case USB_REQUEST_GET_CONFIGURATION:
        case USB_REQUEST_SET_CONFIGURATION: hdlReqConfig(request);        break;
        case USB_REQUEST_GET_INTERFACE:
        case USB_REQUEST_SET_INTERFACE:     hdlReqInterface(request);     break;

        default: return DEV_ERR;
    }

    return DEV_OK;
}


/**
  * @brief  Standard GET_STATUS requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqGetStatus (const TUsbRequest* request)
{
    debug("Get status ");
    // Get request destination (bits [0..4] of bmRequestType field)
    uchar recipient = request->bmRequestType & 0x1f;

    switch (recipient)
    {
        case USB_REQUEST_DEST_DEVICE:
        {
            debug("device\n");

            const TUsbCfgDescriptor* pCfg = getDescCfg();
            if (!pCfg)
            {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
                break;
            }

            // Set values of flags Self Powered and Remote Wakeup in response
            uint16 data = 0;
            if (pCfg->bmAttributes & USB_CFG_ATTR_SELF_POWERED_MASK)
            {
                data |= 0x01;
            }

            if (pCfg->bmAttributes & USB_CFG_ATTR_REMOTE_WKUP_MASK)
            {
                data |= 0x02;
            }

            Write(0, &data, sizeof(data) );
        }
        break;

        case USB_REQUEST_DEST_INTERFACE:
        {
            debug("interface\n");

            uint index = request->wIndex & 0xff;
            if ( index == (m_cfgNum - 1) )
            {
                uint16 data = 0;
                Write(0, &data, sizeof(data) );
            } else {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        case USB_REQUEST_DEST_ENDPOINT:
        {
            uint epNum = request->wIndex & 0x0f;
            if (epNum < USB_MAX_ENDPOINTS)
            {
                debug("endpiont\n");
                uint16 data = 0;
                TEpState state = epGetState(epNum);

                if (state == USB_EP_STATE_DISABLED)
                {
                    data |= 0x01;
                }

                Write(0, &data, sizeof(data) );

            } else {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        default:
            debug("UNKNOWN\n");
            epSetState(0, USB_EP_STATE_DISABLED);
        break;
    }
}


/**
  * @brief  Standard CLEAR_FEATURE / SET_FEATURE requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqFeature (const TUsbRequest* request)
{
    debug("Feature ");
    // Get request destination (bits [0..4] of bmRequestType field)
    uchar recipient = request->bmRequestType & 0x1f;

    switch (recipient)
    {
        case USB_REQUEST_DEST_DEVICE:
        {
            debug("device\n");
            if (request->wValue == USB_DEVICE_REMOTE_WAKEUP)
            {
                // FIXME: Wakeup support
                // Confirm receiving by zero length packet (ZLP)
                Write(0, NULL, 0);

            } else {
                debug("ERROR\n");
                // Unsupported option 
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        case USB_REQUEST_DEST_INTERFACE:
            debug("interface (ERROR)\n");
            // Unsupported
            epSetState(0, USB_EP_STATE_DISABLED);
        break;

        case USB_REQUEST_DEST_ENDPOINT:
        {
            debug("EP\n");
            uint epNum = request->wIndex & 0x0f;
            if (epNum < USB_MAX_ENDPOINTS && request->wValue == USB_ENDPOINT_HALT)
            {
                epSetState(epNum, USB_EP_STATE_DISABLED);

                // Confirm receiving by zero length packet (ZLP)
                Write(0, NULL, 0);

            } else {
                debug("ERROR\n");
                // Error: unknown EP or option
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        default:
            debug("UNKNOWN\n");
            epSetState(0, USB_EP_STATE_DISABLED);
        break;
    }
}


/**
  * @brief  Standard SET_ADDRESS requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqSetAddress (const TUsbRequest* request)
{
    debug("Set Address\n");
    m_devAddr = request->wValue & 0x7f;

    // Confirm receiving by zero length packet (ZLP)
    Write(0, NULL, 0);
}


/**
  * @brief  Standard Get descriptor requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqDescriptor (const TUsbRequest* request)
{
    debug("Descriptor ");
    // Get parameters of requested descriptor
    // (lower byte - index, high byte - type)
    uchar type = (uchar) (request->wValue >> 8);
    uchar index = (uchar) request->wValue;

    switch (type)
    {
        case USB_DESCRIPTOR_DEVICE:
        {
            debug("device\n");
            uint count = (request->wLength > m_pDesc->pDev->bLength)
                        ? m_pDesc->pDev->bLength
                        : request->wLength;
            Write(0, m_pDesc->pDev, count);
        }
        break;

        case USB_DESCRIPTOR_CONFIGURATION:
        {
            debug("config\n");
            // Get configuration descriptro (index is 1 less than number)
            const TUsbCfgDescriptor* pCfg = getDescCfg(index + 1);
            if (pCfg)
            {
                uint count = (request->wLength > pCfg->wTotalLength)
                            ? pCfg->wTotalLength
                            : request->wLength;
                Write(0, pCfg, count);
            } else {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        case USB_DESCRIPTOR_STRING:
        {
            debug("string\n");
            if (index > m_pDesc->pStringList->count)
            {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
                break;
            }

            const TUsbDescriptorHeader* pDesc = m_pDesc->pStringList->pStrings[index];

            uint count = (request->wLength > pDesc->bLength)
                        ? pDesc->bLength
                        : request->wLength;
            Write(0, pDesc, count);
        }
        break;

        default:
            debug("UNKNOWN\n");
            epSetState(0, USB_EP_STATE_DISABLED);
        break;
    }
}


/**
  * @brief  Standard GET_CONFIGURATION / SET_CONFIGURATION requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqConfig (const TUsbRequest* request)
{
    switch (request->bRequest)
    {
        case USB_REQUEST_GET_CONFIGURATION:
            debug("Get config\n");
            // Ack
            Write(0, &m_cfgNum, 1 );
        break;

        case USB_REQUEST_SET_CONFIGURATION:
        {
            debug("Set config\n");
            m_cfgNum = request->wValue & 0xff;
            // Ack
            Write(0, NULL, 0);

            // Endpoints activation after successfull configuration
            const TUsbCfgDescriptor* pCfg = getDescCfg();
            if (!pCfg) break;

            // Searching all EP descriptors in the configuration
            const TUsbDescriptorHeader* pDesc = (TUsbDescriptorHeader*) (pCfg + 1);
            for (
                uint len = sizeof(TUsbCfgDescriptor);
                len < pCfg->wTotalLength;
                len += pDesc->bLength, pDesc = (const TUsbDescriptorHeader*)(pDesc->bLength + (uchar*)pDesc)
            )
            {
                if (pDesc->bDescriptorType != USB_DESCRIPTOR_ENDPOINT) continue;

                const TUsbEpDescriptor* pDescEp = (const TUsbEpDescriptor*) pDesc;
                epInit(pDescEp);
            }
        }
        break;
    }
}


/**
  * @brief  Standard GET_INTERFACE / SET_INTERFACE requests processing
  * @param  request: Request descriptor
  */
void CDevUsbd::hdlReqInterface (const TUsbRequest* request)
{
    // Interface index
    uchar ifaceIdex = (uchar) request->wIndex;

    switch (request->bRequest)
    {
        case USB_REQUEST_GET_INTERFACE:
        {
            debug("Get iface\n");
            const TUsbCfgDescriptor* pCfg = getDescCfg();
            if (pCfg && ifaceIdex < pCfg->bNumInterfaces)
            {
                // Only one interface supported (Alternative Setting = 0)
                uchar data = 0;
                Write(0, &data, sizeof(data) );

            } else {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;

        case USB_REQUEST_SET_INTERFACE:
        {
            debug("Set iface\n");
            uchar alt = (uchar) request->wValue;
            // Only one interface supported (Alternative Setting = 0)
            if (ifaceIdex == 0 && alt == 0)
            {
                Write(0, &m_cfgNum, 1 );
            } else {
                debug("ERROR\n");
                epSetState(0, USB_EP_STATE_DISABLED);
            }
        }
        break;
    }
}
