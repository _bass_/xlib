//*****************************************************************************
//
// @brief   Interface of network device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/net.h"
#include "kernel/kdebug.h"
#include "misc/math.h"
#include <string.h>


/**
  * @brief  Constructor
  * @param  family: Network protocol family supported by the device
  *         (see NET_FAMILY_xx in appropriate socket family header file)
  */
CDevNet::CDevNet (uint family) :
    CDevice()
{
    m_addr.family = family;
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevNet::~CDevNet ()
{
}


/**
  * @brief  Set address of the network device
  * @param  pAddr: Network address descriptor
  * @return NET_OK or negative error code
  */
int CDevNet::SetAddr (const TNetAddr* pAddr)
{
    if (pAddr->family != m_addr.family) return NET_ERR;

    m_addr = *pAddr;

    return NET_OK;
}


/**
  * @brief  Read address of the network device
  * @param  pAddr: Pointer to network address descriptor (to save address)
  */
void CDevNet::GetAddr (TNetAddr* pAddr) const
{
    *pAddr = m_addr;
}


/**
  * @brief  Received data processing
  * @param  data: Received data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevNet::onDataReceive (void* data, uint size)
{
    if (!size) return -1;

    TNetParser* parser = CSocket::GetParser(m_addr.family);
    if (!parser) return -1;

    if (!m_skb.data)
    {
        resetSkb(&m_skb);
    }

    int count = size;
    int rest = 0;

    while (count || !rest)
    {
        // Detect expected packet data size
        rest = parser(&m_skb);
        if (rest < 0)
        {
            resetSkb(&m_skb);
            break;
        }

        // Check buffer overload
        if (m_skb.dataSize + rest > m_skb.buffSize)
        {
            resetSkb(&m_skb);
            count = size + 1;
            break;
        }

        if (!rest)
        {
            // Valid packet is built. Transfer it to linked socket
            if (m_skb.socket)
            {
                m_skb.socket->onReceivePacket(&m_skb);
            }

            resetSkb(&m_skb);
            continue;
        }

        // Fill buffer by proper data
        uint sizeToCopy = (uint) __MIN(rest, count);

        if (sizeToCopy)
        {
            memcpy( (char*)m_skb.data + m_skb.dataSize, data, sizeToCopy );

            m_skb.dataSize += sizeToCopy;
            rest -= sizeToCopy;
            data = (char*)data + sizeToCopy;
            count -= sizeToCopy;
        }
    } // while ( m_skb.data && (count || !rest) )

    return (int)size - count;
}
