//*****************************************************************************
//
// @brief   Display device driver interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/display.h"


/**
  * @brief  Constructor
  * @param  None
  */
CDevDisplay::CDevDisplay () :
    CDevice()
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevDisplay::~CDevDisplay ()
{
}


/**
  * @brief  Draw rectangle with solod color
  * @param  x0: X coordinate of top-left corner
  * @param  y0: Y coordinate of top-left corner
  * @param  width: Rectangle width
  * @param  height: Rectangle height
  * @param  color: Color to fill the rectangle
  * @return 0 or negative error code
  */
int CDevDisplay::DrawRect (uint x0, uint y0, uint width, uint height, TPixel color)
{
    for (uint row = 0; row < height; ++row)
    {
        uint x = x0;

        for (uint i = 0; i < width; ++i, ++x)
        {
            if (x >= GetWidth()) break;

            int res = DrawPixel(x, y0, color);
            if (res < 0) return -1;
        }

        ++y0;
        if (y0 >= GetHeight()) break;
    }

    return 0;
}


/**
  * @brief  Draw area with specified pixels data
  * @param  x0: X coordinate of top-left corner
  * @param  y0: Y coordinate of top-left corner
  * @param  width: Area width
  * @param  height: Area height
  * @param  data: Pointer to pixel's data (size is width * height)
  * @param  endDrawCb: Callback function to notify about the end of data processing
  * @param  cbParam: Callback function additional parameter
  * @return 0 or negative error code
  */
int CDevDisplay::Draw (uint x0, uint y0, uint width, uint height, const TPixel* data, TDrawCb* endDrawCb, void* cbParam)
{
    if (!data) return -1;

    for (uint row = 0; row < height; ++row)
    {
        const TPixel* ptr = &data[row * width];
        uint x = x0;

        for (uint i = 0; i < width; ++i, ++x)
        {
            if (x >= GetWidth()) break;

            int res = DrawPixel(x, y0, *ptr);
            if (res < 0) return -2;
        }

        ++y0;
        if (y0 >= GetHeight()) break;
    }

    if (endDrawCb)
    {
        endDrawCb(x0, y0, width, height, data, cbParam);
    }

    return 0;
}
