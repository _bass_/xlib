//*****************************************************************************
//
// @brief   Cached IO through block device
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "dev/bcache.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Timeout to save changed data on disk (ms)
    #define BCACHE_WR_TIMEOUT                       250

    // Cache lock control
    #define BCACHE_CACHE_LOCK(ptr)                  do { \
                                                        Kernel::EnterCritical(); \
                                                        ptr->flags.lock = 1; \
                                                        Kernel::ExitCritical(); \
                                                    } while (0)
    #define BCACHE_CACHE_UNLOCK(ptr)                do { \
                                                        Kernel::EnterCritical(); \
                                                        ptr->flags.lock = 0; \
                                                        Kernel::ExitCritical(); \
                                                    } while (0)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Thread for cache dispatcher
    class CCacheDisp : public KThread
    {
        public:
            CCacheDisp ();

        private:
            void run (void);
    };


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("bcache", "Bcache", NULL, NULL);

    // Chache objects list
    static CBlockCache* pCacheList = NULL;

    // Cache dispatcher
    CCacheDisp  cacheDisp;


/**
  * @brief  Constructor
  * @param  dev: Block device
  * @param  count: Number of cached blocks
  */
CBlockCache::CBlockCache (CDevBlock* dev, uint count) :
    m_pNext(NULL),
    m_pDev(dev),
    m_count(count),
    m_pCache(NULL)
{
    assert(!m_pDev || !m_count);

    // Append object to global list
    if (pCacheList)
    {
        // Searching last object in the list
        CBlockCache* ptr = pCacheList;
        while (ptr->m_pNext)
        {
            ptr = ptr->m_pNext;
        }
        ptr->m_pNext = this;
    }
    else
    {
        pCacheList = this;
    }
}


/**
  * @brief  Destructor
  * @param  None
  */
CBlockCache::~CBlockCache ()
{
    flush();

    if (m_pCache) free(m_pCache);

    // Removing object from global list
    if (pCacheList == this)
    {
        pCacheList = m_pNext;
    }
    else
    {
        CBlockCache* prev = pCacheList;
        while (prev->m_pNext != this)
        {
            prev = prev->m_pNext;
        }
        prev->m_pNext = m_pNext;
    }
}


/**
  * @brief  Device initialization
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBlockCache::Open (void)
{
    if (m_pCache) return DEV_OK;

    int res = m_pDev->Open();
    if (res != DEV_OK) return res;

    // Memory allocation for block buffer
    uint blockSize = m_pDev->BlockSize();
    if (!blockSize) return -10;

    uint memSize = (sizeof(TBCache) + blockSize) * m_count;
    m_pCache = (TBCache*) malloc(memSize);
    if (!m_pCache) return -11;

    memset(m_pCache, 0x00, memSize);

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBlockCache::Close (void)
{
    flush();

    m_pDev->Close();

    if (m_pCache) free(m_pCache);

    return DEV_OK;
}


/**
  * @brief  Read data from block device
  * @param  buff: Buffer for read data
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CBlockCache::Read (void* buff, uint block, uint count)
{
    int result = DEV_OK;
    uint blockSize = m_pDev->BlockSize();

    while (count--)
    {
        TBCache* ptr = getCache(block);

        if (!ptr->flags.valid)
        {
            int res = m_pDev->Read(ptr->buff, block);
            if (res != DEV_OK)
            {
                result = res;
                BCACHE_CACHE_UNLOCK(ptr);
                break;
            }

            Kernel::EnterCritical();
            ptr->flags.valid = 1;
            ptr->flags.dirty = 0;
            Kernel::ExitCritical();
        }

        // Update access time to synchronized cache
        if (!ptr->flags.dirty)
        {
            ptr->time = Kernel::GetJiffies();
        }

        memcpy(buff, ptr->buff, blockSize);

        BCACHE_CACHE_UNLOCK(ptr);

        ++block;
        buff = (char*)buff + blockSize;
    }

    return result;
}


/**
  * @brief  Write data to block device
  * @param  data: Pointer to data to write
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CBlockCache::Write (const void* data, uint block, uint count)
{
    uint blockSize = m_pDev->BlockSize();

    while (count--)
    {
        TBCache* ptr = getCache(block);

        Kernel::EnterCritical();
        {
            ptr->time = Kernel::GetJiffies();
            ptr->flags.valid = 1;
            ptr->flags.dirty = 1;

            memcpy(ptr->buff, data, blockSize);
            // Unlock cache after end of usage
            ptr->flags.lock = 0;
        }
        Kernel::ExitCritical();

        ++block;
        data = (char*)data + blockSize;
    }

    return DEV_OK;
}


/**
  * @brief  Save changed data on block device
  * @param  None
  * @return None
  */
void CBlockCache::flush (void)
{
    TBCache* ptr = m_pCache;
    uint blockSize = m_pDev->BlockSize();

    for ( uint i = 0; i < m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
    {
        if (ptr->flags.lock || !ptr->flags.valid || !ptr->flags.dirty) continue;

        BCACHE_CACHE_LOCK(ptr);
        int result = m_pDev->Write(ptr->buff, ptr->block);
        BCACHE_CACHE_UNLOCK(ptr);

        if (result != DEV_OK)
        {
            kerror("flush cache ERROR (%d)", result);
        }

        Kernel::EnterCritical();
        ptr->flags.dirty = 0;
        Kernel::ExitCritical();
    }
}


/**
  * @brief  Cache allocation
  * @param  index: Block index
  * @return Pointer to allocated cache or NULL on error
  */
TBCache* CBlockCache::getCache (uint index)
{
    uint blockSize = m_pDev->BlockSize();
    TBCache* ptr;

    while (1)
    {
        Kernel::EnterCritical();

        // Check if requested cache already exists
        bool foundLocked = false;
        ptr = m_pCache;
        for ( uint i = 0; i < m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
        {
            if (!ptr->flags.valid || ptr->block != index) continue;

            if (ptr->flags.lock)
            {
                foundLocked = true;
                break;
            }

            ptr->flags.lock = 1;

            Kernel::ExitCritical();
            return ptr;
        }
        // Waiting of cache unlock and run searching one more time after that
        if (foundLocked)
        {
            Kernel::ExitCritical();
            KThread::sleep(10);
            continue;
        }

        // Searching of invalid cache
        ptr = m_pCache;
        for ( uint i = 0; i < m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
        {
            if (ptr->flags.lock || ptr->flags.valid) continue;

            ptr->flags.lock = 1;
            ptr->flags.dirty = 0;
            ptr->block = index;

            Kernel::ExitCritical();
            return ptr;
        }

        // Searching the oldest synchronized cache
        uint32 now = Kernel::GetJiffies();
        TBCache* target = NULL;
        ptr = m_pCache;

        for ( uint i = 0; i < m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
        {
            if (ptr->flags.lock) continue;

            if (!ptr->flags.valid)
            {
                target = ptr;
                break;
            }

            if (ptr->flags.dirty) continue;

            if ( !target || (now - ptr->time > now - target->time) )
            {
                target = ptr;
            }
        }

        if (target)
        {
            target->flags.lock = 1;
            target->flags.valid = 0;
            target->block = index;

            Kernel::ExitCritical();
            return target;
        }

        // Save data of the oldest synchronized cache
        now = Kernel::GetJiffies();
        target = NULL;
        ptr = m_pCache;

        for ( uint i = 0; i < m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
        {
            // Double check on cache usage and filter by valid and dirty caches
            // (All caches at this stage should be valid and with changes)
            if (ptr->flags.lock || !ptr->flags.valid || !ptr->flags.dirty) continue;

            if ( !target || (now - ptr->time > now - target->time) )
            {
                target = ptr;
            }
        }

        if (!target)
        {
            klog("get oldest cache ERROR");
            Kernel::ExitCritical();
            KThread::sleep(1);
            continue;
        }

        target->flags.val = 0;
        target->flags.lock = 1;
        uint flushBlock = target->block;
        target->block = index;
        Kernel::ExitCritical();

        int result = m_pDev->Write(target->buff, flushBlock);
        if (result != DEV_OK)
        {
            kerror("write oldest cache ERROR (%d)", result);
        }

        return target;
    } // while (1)
}


/**
  * @brief  Caches synchronization scheduler
  * @param  None
  * @return None
  */
void CBlockCache::cacheScheduler (void)
{
    for (CBlockCache* obj = pCacheList; obj; obj = obj->m_pNext)
    {
        uint blockSize = obj->m_pDev->BlockSize();
        TBCache* ptr = obj->m_pCache;

        for ( uint i = 0; i < obj->m_count; ++i, ptr = (TBCache*)(ptr->buff + blockSize) )
        {
            bool doFlush = false;
            Kernel::EnterCritical();
            do
            {
                if (ptr->flags.lock || !ptr->flags.valid || !ptr->flags.dirty) break;
                if ( !IsTimeout(ptr->time, BCACHE_WR_TIMEOUT) ) break;
                BCACHE_CACHE_LOCK(ptr);
                doFlush = true;
            } while (0);
            Kernel::ExitCritical();

            if (!doFlush) continue;

            int result = obj->m_pDev->Write(ptr->buff, ptr->block);

            if (result == DEV_OK)
            {
                Kernel::EnterCritical();
                ptr->flags.dirty = 0;
                Kernel::ExitCritical();
            }
            else
            {
                kerror("flush cache ERROR (timeout, %d)", result);
            }

            BCACHE_CACHE_UNLOCK(ptr);
        }
    } // for (CBlockCache* obj = pCacheList; obj; obj = ptr->m_pNext)
}


/**
  * @brief  Cache dispatcher constructor
  * @param  None
  */
CCacheDisp::CCacheDisp () :
    KThread("CacheDisp", 512, THREAD_PRIORITY_DEFAULT)
{
}


/**
  * @brief  Cache dispatcher main cycle
  * @param  None
  * @return None
  */
void CCacheDisp::run (void)
{
    for (;;)
    {
        CBlockCache::cacheScheduler();
        sleep(BCACHE_WR_TIMEOUT);
    }
}
