//*****************************************************************************
//
// @brief   Basic memory control functionality translator to 
//          appropriate functions from xLib 
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "memory/new"
#include "memory/heap.h"


void* operator new (size_t size)
{
    return malloc(size);
}


void* operator new (size_t size, const std::nothrow_t& n)
{
    (void)n;
    return malloc(size);
}


void* operator new[] (size_t size)
{
    return malloc(size);
}


void* operator new (size_t size, void* p)
{
    (void)size;
    return p;
}


void* operator new (size_t size, size_t itemSize)
{
    (void)itemSize;
    return malloc(size);
}


void operator delete (void* ptr)
{
    free(ptr);
}


void operator delete (void* ptr, const std::nothrow_t& n)
{
    (void)n;
    return free(ptr);
}


void operator delete (void*, void*)
{
}


void operator delete (void* ptr, size_t size)
{
    (void)size;
    free(ptr);
}


void operator delete[] (void* ptr)
{
    free(ptr);
}


void operator delete[] (void* ptr, size_t size)
{
    (void)size;
    free(ptr);
}
