//*****************************************************************************
//
// @brief   Dynamic memory manger (heap). Based on heap_4 from FreeRTOS (v7.4.2)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "memory/heap.h"
#include "kernel/kernel.h"
#include "kernel/events.h"
#include "misc/macros.h"
#include "def_board.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Memory block align (must be power of 2!!!)
    #if defined(CFG_HEAP_BLOCK_ALIGN)
        #define HEAP_BLOCK_ALIGN                    CFG_HEAP_BLOCK_ALIGN
    #else
        #define HEAP_BLOCK_ALIGN                    8
    #endif
    #define HEAP_BLOCK_ALIGN_MASK                   ( (size_t)(HEAP_BLOCK_ALIGN - 1) )

    #define HEAP_STRUCT_SIZE                        ( (sizeof(TBlockDesc) + HEAP_BLOCK_ALIGN - 1) & ~HEAP_BLOCK_ALIGN_MASK )
    #define HEAP_MINIMUM_BLOCK_SIZE                 ( HEAP_STRUCT_SIZE * 2 )

    #if defined(CFG_HEAP_DECLARE_RANGE)
        CC_SECTION_DECLARE(HEAP_start)
        CC_SECTION_DECLARE(HEAP_end)
        #define HEAP_START_ADDR_START               ( (size_t)((char*)CC_SECTION_ADDR_START(HEAP_start) + HEAP_BLOCK_ALIGN - 1) & ~HEAP_BLOCK_ALIGN_MASK )
        #define HEAP_START_ADDR_END                 ( (size_t)CC_SECTION_ADDR_END(HEAP_end) & ~HEAP_BLOCK_ALIGN_MASK )
    #else
        CC_SECTION_DECLARE(HEAP)
        #define HEAP_START_ADDR_START               ( (size_t)((char*)CC_SECTION_ADDR_START(HEAP) + HEAP_BLOCK_ALIGN - 1) & ~HEAP_BLOCK_ALIGN_MASK )
        #define HEAP_START_ADDR_END                 ( (size_t)CC_SECTION_ADDR_END(HEAP) & ~HEAP_BLOCK_ALIGN_MASK )
    #endif

    #define HEAP_ADJUSTED_SIZE                      ( (size_t)(HEAP_START_ADDR_END - HEAP_START_ADDR_START) )


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Free memory block descriptor
    typedef struct _TBlockDesc
    {
        struct _TBlockDesc*     pNextFreeBlock;     // The next free block in the list
        size_t                  blockSize;          // The size of the free block
    } TBlockDesc;


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static void heapInit (void);
    static void insertBlockIntoFreeList (TBlockDesc* pBlockToInsert);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Amount of free memory in the heap (bytes)
    static size_t g_freeBytesRemaining = 0;

    // First and last blocks descriptors
    static TBlockDesc g_Start = {};
    static TBlockDesc* g_pEnd = NULL;


/**
  * @brief  Dynamic memory manager initialization
  * @param  None
  * @return None
  */
void heapInit (void)
{
    char* pHeap = (char*) HEAP_START_ADDR_START;

    g_Start.pNextFreeBlock = (TBlockDesc*)pHeap;
    g_Start.blockSize = 0;

    char* pHeapEnd = pHeap + HEAP_ADJUSTED_SIZE;
    pHeapEnd -= HEAP_STRUCT_SIZE;

    g_pEnd = (TBlockDesc*) pHeapEnd;
    g_pEnd->pNextFreeBlock = NULL;
    g_pEnd->blockSize = 0;

    // First free block contains all available memory in the heap
    TBlockDesc* pFirstFreeBlock = (TBlockDesc*)pHeap;
    pFirstFreeBlock->blockSize = HEAP_ADJUSTED_SIZE - HEAP_STRUCT_SIZE;
    pFirstFreeBlock->pNextFreeBlock = g_pEnd;

    g_freeBytesRemaining = pFirstFreeBlock->blockSize;
}


/**
  * @brief  Append block to free block list
  * @param  pBlockToInsert: Block to append
  */
void insertBlockIntoFreeList (TBlockDesc* pBlockToInsert)
{
    TBlockDesc* pPrevBlock;

    // Searching for a block before insterting one
    for (pPrevBlock = &g_Start; pPrevBlock->pNextFreeBlock < pBlockToInsert; pPrevBlock = pPrevBlock->pNextFreeBlock);

    // Joining inserting and previous blocks if they are located one by one
    // (joining with g_Start is prohibited)
    if (
        pPrevBlock != &g_Start &&
        ((char*)pPrevBlock + pPrevBlock->blockSize) == (char*)pBlockToInsert
    ) {
        pPrevBlock->blockSize += pBlockToInsert->blockSize;
        pBlockToInsert = pPrevBlock;
    }

    // Joining inserting and next blocks if they are located one by one 
    // (joining with g_pEnd is prohibited)
    if (
        pPrevBlock->pNextFreeBlock != g_pEnd &&
        ((char*)pBlockToInsert + pBlockToInsert->blockSize) == (char*)pPrevBlock->pNextFreeBlock
    ) {
        pBlockToInsert->blockSize += pPrevBlock->pNextFreeBlock->blockSize;
        pBlockToInsert->pNextFreeBlock = pPrevBlock->pNextFreeBlock->pNextFreeBlock;
    }
    else
    {
        pBlockToInsert->pNextFreeBlock = pPrevBlock->pNextFreeBlock;
    }

    // Updating pointer to next free block if it's necessary
    if (pPrevBlock != pBlockToInsert)
    {
        pPrevBlock->pNextFreeBlock = pBlockToInsert;
    }
}


/**
  * @brief  Memory allocation
  * @param  size: Allocating memory size
  * @return Pointer to allocated memory or NULL on error
  */
void* malloc (size_t size)
{
    void* ptr = NULL;

    Kernel::EnterCritical();
    do
    {
        // Lazy initialization (on first call)
        if (g_pEnd == NULL)
        {
            heapInit();
        }

        if (!size) break;

        // Increase allocated size to fit memory block service descriptor
        size += HEAP_STRUCT_SIZE;

        // Adjusting the size to achieve required memory block align
        if (size & HEAP_BLOCK_ALIGN_MASK)
        {
            size += ( HEAP_BLOCK_ALIGN - (size & HEAP_BLOCK_ALIGN_MASK) );
        }

        if (size >= g_freeBytesRemaining) break;

        // Searching for memory block with proper size
        // Searching through previous block to be able to update next pointer after allocation
        TBlockDesc* pBlockPrev;
        
        #if defined(CFG_HEAP_MODE_BESTFIT)
        pBlockPrev = NULL;
        for (TBlockDesc* itr = &g_Start; itr->pNextFreeBlock; itr = itr->pNextFreeBlock)
        {
            TBlockDesc* b = itr->pNextFreeBlock;
            if (b->blockSize < size) continue;
            
            if (!pBlockPrev || b->blockSize < pBlockPrev->pNextFreeBlock->blockSize)
            {
                pBlockPrev = itr;
                if (b->blockSize == size) break;
            }
        }
        
        #else
        pBlockPrev = &g_Start;
        while (pBlockPrev->pNextFreeBlock && pBlockPrev->pNextFreeBlock->blockSize < size)
        {
            pBlockPrev = pBlockPrev->pNextFreeBlock;
        }
        
        #endif

        if (!pBlockPrev || !pBlockPrev->pNextFreeBlock) break;
        
        // Excluding founf block from free memory list
        TBlockDesc* pBlock = pBlockPrev->pNextFreeBlock;
        pBlockPrev->pNextFreeBlock = pBlock->pNextFreeBlock;

        ptr = (void*) (pBlock + 1);

        // Split found block on a parts if its size more that requested
        if ( (pBlock->blockSize - size) > HEAP_MINIMUM_BLOCK_SIZE )
        {
            TBlockDesc* pNewBlock = (TBlockDesc*) ( ((char*)pBlock) + size );
            pNewBlock->blockSize = pBlock->blockSize - size;
            pBlock->blockSize = size;

            insertBlockIntoFreeList(pNewBlock);
        }

        g_freeBytesRemaining -= pBlock->blockSize;

    } while (0);
    Kernel::ExitCritical();


    if (!ptr)
    {
        Kernel::RiseEvent(EV_SYS_MALLOC_FILED);
    }

    return ptr;
}


/**
  * @brief  Memory allocation with zeroing
  * @param  num: Number of allocating items
  * @param  size: Item size
  * @return Pointer to allocated memory or NULL on error
  */
void* calloc (size_t num, size_t size)
{
    size_t totalSize = num * size;
    void* p = malloc(totalSize);
    if (!p) return p;

    memset(p, 0x00, totalSize);

    return p;
}


/**
  * @brief  Releasing previously allocated memory
  * @param  ptr: Pointer to the beginning of allocated memory
  */
void free (void* ptr)
{
    if ( !ptr || ptr < (void*)HEAP_START_ADDR_START || ptr > (void*)HEAP_START_ADDR_END ) return;

    // Block descriptor is located right before the block memory
    TBlockDesc* pBlock = (TBlockDesc*) ((char*)ptr - HEAP_STRUCT_SIZE);

    Kernel::EnterCritical();
    {
        g_freeBytesRemaining += pBlock->blockSize;
        insertBlockIntoFreeList(pBlock);
    }
    Kernel::ExitCritical();
}


/**
  * @brief  Reading amount of free memory in the heap
  * @return Amount of free memory in the heap (bytes)
  */
size_t heapFreeSize (void)
{
    return g_freeBytesRemaining;
}
