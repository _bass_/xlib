//*****************************************************************************
//
// @brief   RS485 interface driver (network device)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/usart/rs485.h"
#include "arch/cpu.h"
#include "arch/usart.h"
#include "kernel/kernel.h"
#include "misc/time.h"
#include "threads.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define RS485_DEFAULT_READ_PERIOD                   50

    // Internal worker for data reading
    class CDevRs485Reader : public KThread
    {
        public:
            CDevRs485Reader (CDevRs485* drv);
            
            void SetPeriod (uint ms) { m_pollPeriod = ms; }
            
        private:
            CDevRs485*  m_drv;
            uint        m_pollPeriod;
            
            void run (void);
    };


/**
  * @brief  Constructor
  * @param  family: Network protocol family supported by the device
  *         (see NET_FAMILY_xx in appropriate socket family header file)
  */
CDevRs485::CDevRs485 (uint family) :
    CDevNet(family),
    m_pDev(NULL),
    m_quietTime(0),
    m_reader(this)
{
    m_reader.Suspend();
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (see TRs485Cfg)
  * @return DEV_OK or negative error code
  */
int CDevRs485::Configure (const void* params)
{
    do
    {
        if (!params) break;
        TRs485Cfg* pCfg = (TRs485Cfg*)params;
        
        m_pDev = (CDevChar*) GetDevice(DEV_NAME_USART, pCfg->devId);
        if (!m_pDev) break;
        
        m_devCfg = pCfg->devCfg;
        int result = m_pDev->Configure(&m_devCfg);
        if (result != DEV_OK) break;
        
        if (!pCfg->rdPeriod) break;
        m_reader.SetPeriod(pCfg->rdPeriod);
        
        m_quietTime = pCfg->quietTime;
        
        return DEV_OK;
    } while (0);
    
    return DEV_ERR;
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevRs485::Open (void)
{
    do
    {
        if (!m_pDev) break;
        
        int result = m_pDev->Open();
        if (result != DEV_OK) break;
        
        result = m_pDev->Configure(&m_devCfg);
        if (result != DEV_OK) break;
        
        m_activeTime = 0;
        m_reader.Resume();
        
        return DEV_OK;
    } while (0);
    
    return DEV_ERR;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevRs485::Close (void)
{
    if (!m_pDev) return DEV_ERR;
    
    m_reader.Suspend();
    m_pDev->Close();
    
    return DEV_OK;
}


/**
  * @brief  Send network packet through the device
  * @param  skb: Network packet descriptor
  * @return Processed data size or negative error code
  */
int CDevRs485::Write (const TSkb* skb)
{
    if (!m_pDev || !skb->data || !skb->dataSize) return NET_ERR;

    // Waiting for minimal quiet timeout
    if ( m_activeTime && m_quietTime && !IsTimeout(m_activeTime, m_quietTime) )
    {
        uint timeout = m_quietTime + rand() & 0xff;
        uint32 delay = m_activeTime + timeout - Kernel::GetJiffies();

        KThread::sleep(delay);
    }
    
    int result = m_pDev->Write(skb->data, skb->dataSize);
    m_activeTime = Kernel::GetJiffies();
    
    return result;
}


/**
  * @brief  Reset network packet descriptor
  * @param  skb: Network packet descriptor
  */
void CDevRs485::resetSkb (TSkb* skb)
{
    skb->data = m_buffRx;
    skb->buffSize = sizeof(m_buffRx);
    skb->dataSize = 0;
    skb->pDev = this;
    skb->socket = NULL;
}


/**
  * @brief  Incoming data processing
  * @param  None
  */
void CDevRs485::recv (void)
{
    if (!m_pDev) return;
    
    int len = m_pDev->Read(m_buffRx, sizeof(m_buffRx));
    if (len > 0)
    {
        onDataReceive(m_buffRx, len);
    }
}


/**
  * @brief  Internal reading worker constructor
  * @param  drv: Pointer to driver object
  */
CDevRs485Reader::CDevRs485Reader (CDevRs485* drv) :
    KThread("RS485 rd", 512, THREAD_PRIORITY_RS485RD),
    m_drv(drv),
    m_pollPeriod(RS485_DEFAULT_READ_PERIOD)
{
}


/**
  * @brief  Reading worker thread main function
  * @param  None
  */
void CDevRs485Reader::run (void)
{
    for (;;)
    {
        m_drv->recv();
        sleep(m_pollPeriod);
    }
}
