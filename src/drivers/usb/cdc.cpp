//*****************************************************************************
//
// @brief   USB CDC device driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/usb/cdc.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if 0
        #include "kernel/kdebug.h"
        #define debug                           kprint
    #else
        #define debug(...)
    #endif


/**
  * @brief  Class-specific requests handler
  * @param  request: Request descriptor
  * @param  arg: Additional arguments
  * @return DEV_OK or negative error code
  */
int CDevUsbCdc::OnRequest (const TUsbRequest* request, void* arg)
{
    CDevUsbCdc* obj = (CDevUsbCdc*) arg;
    return (obj)
            ? obj->csRequestHandler(request)
            : DEV_ERR;
}


/**
  * @brief  Constructor
  * @param  pDev: Used USB-core device
  * @param  buffRx: Receive buffer (if NULL, will be allocated)
  * @param  sizeRx: Receive buffer size
  * @param  buffTx: Transmit buffer (if NULL, will be allocated)
  * @param  sizeTx: Transmit buffer size
  */
CDevUsbCdc::CDevUsbCdc (CDevUsbd* pDev, void* buffRx, uint sizeRx, void* buffTx, uint sizeTx) :
    CDevChar(),
    m_buffRx(sizeRx, buffRx),
    m_buffTx(sizeTx, buffTx),
    m_buffTxAcm(sizeof(m_dataAcm), m_dataAcm)
{
    m_pDev = pDev;

    // Link buffers with endpoints
    m_pDev->EpSetBuffRx(CDC_EP_READ_NUM, &m_buffRx);
    m_pDev->EpSetBuffTx(CDC_EP_WRITE_NUM, &m_buffTx);
    m_pDev->EpSetBuffTx(CDC_EP_ACM_NUM, &m_buffTxAcm);

    m_pDev->SetRequestHandler(OnRequest, this);

    m_portCfg.baudrate = USB_CDC_DEFAULT_BAUDRATE;
    m_portCfg.format = USB_CDC_DEFAULT_STOP_BITS;
    m_portCfg.parity = USB_CDC_DEFAULT_PARITY;
    m_portCfg.dataBits = USB_CDC_DEFAULT_DATA_BITS;
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevUsbCdc::Open (void)
{
    int result = m_pDev->Open();

    if (result == DEV_OK)
    {
        if (m_pDev->Ioctl(IOCTL_USB_CONNECT_CTRL, (void*) true) != IOCTL_OK)
        {
            result = DEV_ERR;
        }
    }

    return result;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevUsbCdc::Close (void)
{
    int result = m_pDev->Close();

    return result;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (see TUsbdCdcOptions)
  * @return DEV_OK or negative error code
  */
int CDevUsbCdc::Configure (const void* params)
{
    int result = m_pDev->Configure(params);

    return result;
}


/**
  * @brief  Send character to output stream
  * @param  c: character
  * @return 0 or negative error code
  */
int CDevUsbCdc::PutChar (const uchar c)
{
    return m_pDev->Write(CDC_EP_WRITE_NUM, &c, 1);
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CDevUsbCdc::GetChar (void)
{
    uchar data;
    int result = m_pDev->Read(CDC_EP_READ_NUM, &data, 1);
    return (result <= 0) ? result : data;
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevUsbCdc::Write (const void* buff, uint size)
{
    return m_pDev->Write(CDC_EP_WRITE_NUM, buff, size);
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CDevUsbCdc::Read (void* buff, uint size)
{
    return m_pDev->Read(CDC_EP_READ_NUM, buff, size);
}


/**
  * @brief  Class-specific requests handler
  * @param  request: Request descriptor
  * @return DEV_OK or negative error code
  */
int CDevUsbCdc::csRequestHandler (const TUsbRequest* request)
{
    // Request type verification
    uchar reqType = request->bmRequestType & (USB_REQUEST_TYPE_MASK | USB_REQUEST_DEST_MASK);
    if ( reqType != (USB_REQUEST_CLASS | USB_REQUEST_DEST_INTERFACE) )
    {
        return DEV_ERR;
    }

    switch (request->bRequest)
    {
        // Set COM-port parameters
        case USB_CDC_REQ_SET_LINE_CODING:
        {
            TCdcPortCfg cfg;
            uint len = m_pDev->Read( 0, (void*)&cfg, sizeof(cfg) );
            debug("SET_LINE_CODING [%u]", len);
            if (len != sizeof(cfg)) break;
            
            if (
                cfg.format > 2 ||
                cfg.parity > 4 ||
                cfg.dataBits < 5 ||
                (cfg.dataBits > 8 && cfg.dataBits != 16) ||
                cfg.baudrate > 256000
            ) {
                return DEV_ERR;
            }
            
            m_portCfg = cfg;
            debug("SET_LINE_CODING (%u%c%u s%u)", m_portCfg.baudrate, (m_portCfg.parity) ? 'y' : 'n', m_portCfg.dataBits, m_portCfg.format);
            
            // Acknowledge of packet receive
            m_pDev->Write(0, NULL, 0);
            
            sendSerialState(request->wIndex);
        }
        break;

        // Reading COM-port parameters
        case USB_CDC_REQ_GET_LINE_CODING:
        {
            debug("GET_LINE_CODING");
            uint len = ( request->wLength > sizeof(m_portCfg) ) ? sizeof(m_portCfg) : request->wLength;
            m_pDev->Write(0, (void*) &m_portCfg, len);
        }
        break;

        case USB_CDC_REQ_SET_CONTROL_LINE_STATE:
        {
            // Bit 0 request->wValue - DTR signal state (notify about connected host)
            // Bit 1 request->wValue - RTS signal state
            debug("SET_CONTROL_LINE_STATE (%.2x)", request->wValue & 0x03);
            // RTS activation isn't controlled because in Windows and Linux is used different values
            
            m_pDev->Write(0, NULL, 0);
            
            m_buffTxAcm.Flush();
            m_buffRx.Flush();
            m_buffTx.Flush();
            sendSerialState(request->wIndex);
        }
        break;

        case USB_CDC_REQ_SEND_BREAK:
            // Success execution emulation
            m_pDev->Write(0, NULL, 0);
        break;

        default: 
            debug("unknown req (%.2x)", request->bRequest);
        return DEV_ERR;
    }

    return DEV_OK;
}


/**
  * @brief  Send port state notification
  * @param  ifaceIndex: ACM interface number
  */
void CDevUsbCdc::sendSerialState (uint ifaceIndex)
{
    // Not required
    return;
#if 0
    debug("send SERIAL_STATE");
    
    char buff[sizeof(TUsbRequest) + 2];
    TUsbRequest* notify = (TUsbRequest*)buff;
    notify->bmRequestType = USB_REQUEST_RESPONSE_FLAG | USB_REQUEST_CLASS | USB_REQUEST_DEST_INTERFACE;
    notify->bRequest = USB_CDC_NOTIFY_SERIAL_STATE;
    notify->wValue = 0;
    notify->wIndex = ifaceIndex;
    notify->wLength = 2;
    // Port is always ready to work
    *( (uint16*)&buff[sizeof(TUsbRequest)] ) = 0x0003;
    
    m_buffTxAcm.Flush();
    m_pDev->Write(CDC_EP_ACM_NUM, buff, sizeof(buff));
#endif
}
