//*****************************************************************************
//
// @brief   USB CDC descriptors
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/usb/cdc.h"
#include "misc/macros.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Endpoints packets size
    #if defined(CFG_USB_CDC_EP0_PACKET_SIZE)
        #define USB_CDC_EP0_PACKET_SIZE         CFG_USB_CDC_EP0_PACKET_SIZE
    #else
        #define USB_CDC_EP0_PACKET_SIZE         64
    #endif
    #if defined(CFG_USB_DEV_EP_IN_SIZE)
        #define USB_DEV_EP_IN_SIZE              CFG_USB_DEV_EP_IN_SIZE
    #else
        #define USB_DEV_EP_IN_SIZE              64
    #endif
    #if defined(CFG_USB_DEV_EP_OUT_SIZE)
        #define USB_DEV_EP_OUT_SIZE             CFG_USB_DEV_EP_OUT_SIZE
    #else
        #define USB_DEV_EP_OUT_SIZE             64
    #endif


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #pragma pack(1)

    // Manufacturer string descriptor
    typedef struct
    {
        TUsbDescriptorHeader    header;
        const wchar_t           data[sizeof(CFG_USB_STR_MANUFACTURER) / sizeof(wchar_t)];
    } TCdcStringManufacturer;

    // Product  string descriptor
    typedef struct
    {
        TUsbDescriptorHeader    header;
        const wchar_t           data[sizeof(CFG_USB_STR_PRODUCT) / sizeof(wchar_t)];
    } TCdcStringProduct;

    // Supported languages string descriptor (usually only English)
    typedef struct
    {
        TUsbDescriptorHeader    header;
        uint16                  wLangId;
    } TCdcStringLanguages;


    // Configuration descriptor
    typedef struct
    {
        TUsbCfgDescriptor       cfg;
        TUsbIfaceDescriptor     ifaceAcm;
        TCdcFuncDescHdr         funcDescHdr;
        TCdcFuncDescCallMgmt    funcDescCallMgmt;
        TCdcFuncDescAcm         funcDescAcm;
        TCdcFuncDescUnion       funcDescUnion;
        TUsbEpDescriptor        epAcmIN;
        TUsbIfaceDescriptor     ifaceData;
        TUsbEpDescriptor        epDataIN;
        TUsbEpDescriptor        epDataOUT;
    } TCdcCfgDescriptor;

    #pragma pack()


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Device descriptor
    static const TUsbDevDescriptor g_CdcDevDesc =
    {
        sizeof(TUsbDevDescriptor),
        USB_DESCRIPTOR_DEVICE,
        USB_2_0,
        USB_CLASS_CDC,
        USB_SUBCLASS_DEFAULT,
        USB_PROTOCOL_DEFAULT,
        USB_CDC_EP0_PACKET_SIZE,
        CFG_USB_DEV_VID,
        CFG_USB_DEV_PID,
        CFG_USB_DEV_VERSION_BCD,
        1,                                      // Manufactorer string descriptor index
        2,                                      // Product (device name) string descriptor index
        0,                                      // Serial number string descriptor index
        1                                       // Number of configurations
    };


    // Device configuration descriptor
    static const TCdcCfgDescriptor g_CdcCfgDesc =
    {
        // Configuration descriptor
        {
            sizeof(TUsbCfgDescriptor),
            USB_DESCRIPTOR_CONFIGURATION,
            sizeof(TCdcCfgDescriptor),
            2,                                  // Number of interfaces in current configuration
            1,                                  // Curent configuration number
            0,                                  // Configuration string descriptor
            USB_POWER_BUS,                      // Configuration attributes
            USB_POWER(500)                      // Bus current consumption (mA)
        },

        // Interface descriptor (ACM)
        {
            sizeof(TUsbIfaceDescriptor),
            USB_DESCRIPTOR_INTERFACE,
            0,                                  // Interface number
            0,                                  // Alternate interface number
            1,                                  // Number of endpoints in the interface
            USB_CLASS_CDC,                      // Interface class code
            USB_CDC_SUBCLASS_ACM,               // Interface subclass code
            USB_CDC_PROTO_V25,                  // Interface protocol code
            0                                   // Interface string descriptor index
        },
        // Header Functional Descriptor
        {
            sizeof(TCdcFuncDescHdr),
            USB_DESCRIPTOR_CS_INTERFACE,
            USB_CDC_SUBTYPE_HEADER,             // Descriptor subtype
            CDC_VERSION                         // CDC version (BCD format)
        },
        // Call Management Functional Descriptor
        {
            sizeof(TCdcFuncDescCallMgmt),
            USB_DESCRIPTOR_CS_INTERFACE,
            USB_CDC_SUBTYPE_CALL_MANAGEMENT,    // Descriptor subtype
            0,                                  // Supported options:
                                                // D1: 0 - Device sends/receives call management information only over the Communication Class interface.
                                                //     1 - Device can send/receive call management information over a Data Class interface.
                                                // D0: 0 - Device does not handle call management itself.
                                                //     1 - Device handles call management itself.
            1                                   // Data interface number
        },
        // ACM Functional Descriptor
        {
            sizeof(TCdcFuncDescAcm),
            USB_DESCRIPTOR_CS_INTERFACE,
            USB_CDC_SUBTYPE_ACM,                // Descriptor subtype
            (1 << 1)                            // Supported options:
                                                // D3: 1 - Device supports the notification Network_Connection.
                                                // D2: 1 - Device supports the request Send_Break
                                                // D1: 1 - Device supports the request combination of Set_Line_Coding, Set_Control_Line_State, Get_Line_Coding, and the notification Serial_State.
                                                // D0: 1 - Device supports the request combination of Set_Comm_Feature, Clear_Comm_Feature, and Get_Comm_Feature.
        },
        // Union Functional Descriptor
        {
            sizeof(TCdcFuncDescUnion),
            USB_DESCRIPTOR_CS_INTERFACE,
            USB_CDC_SUBTYPE_UNION,              // Descriptor subtype
            0,                                  // Control interface index
            1                                   // Data interface index
        },
        // Endpoint descriptor (ACM IN)
        {
            sizeof(TUsbEpDescriptor),
            USB_DESCRIPTOR_ENDPOINT,
            USB_EP_ADDR(USB_EP_DIR_IN, CDC_EP_ACM_NUM),
            USB_EP_TYPE_INT,
            USB_DEV_EP_ACM_SIZE,
            CDC_EP_ACM_INTERVAL                 // Polling interval
        },

        // Interface descriptor (DATA)
        {
            sizeof(TUsbIfaceDescriptor),
            USB_DESCRIPTOR_INTERFACE,
            1,                                  // Interface number
            0,                                  // Alternate setting nuber
            2,                                  // Number of endpoints in the interface
            USB_CLASS_CDC_DATA,                 // Interface class code
            USB_SUBCLASS_DEFAULT,               // Interface subclass code
            USB_PROTOCOL_DEFAULT,               // Interface protocol code
            0                                   // Interface string descriptor index
        },
        // Endpoint descriptor (DATA OUT)
        {
            sizeof(TUsbEpDescriptor),
            USB_DESCRIPTOR_ENDPOINT,
            USB_EP_ADDR(USB_EP_DIR_OUT, CDC_EP_READ_NUM),
            USB_EP_TYPE_BULK,
            USB_DEV_EP_OUT_SIZE,
            0                                   // Polling interval
        },
        // Endpoint descriptor (DATA IN)
        {
            sizeof(TUsbEpDescriptor),
            USB_DESCRIPTOR_ENDPOINT,
            USB_EP_ADDR(USB_EP_DIR_IN, CDC_EP_WRITE_NUM),
            USB_EP_TYPE_BULK,
            USB_DEV_EP_IN_SIZE,
            0                                   // Polling interval
        }
    };

    // Configuration list (Number of items corresponds to g_CdcDevDesc.bNumConfigurations!!!)
    static const TUsbCfgDescriptor* g_CdcConfigs[] =
    {
        (const TUsbCfgDescriptor*) &g_CdcCfgDesc
    };


    // Language string descriptor
    static const TCdcStringLanguages g_CdcStringLanguages =
    {
        {
            sizeof(TCdcStringLanguages),
            USB_DESCRIPTOR_STRING
        },
        USB_LANG_ID_EN
    };

    // Manufacturer string descriptor
    static const TCdcStringManufacturer g_CdcStringManufacturer =
    {
        {
            sizeof(TCdcStringManufacturer) - 2, // 2 bytes string null-terminator in unicode
            USB_DESCRIPTOR_STRING
        },
        CFG_USB_STR_MANUFACTURER
    };

    // Product string descriptor
    static const TCdcStringProduct g_CdcStringProduct =
    {
        {
            sizeof(TCdcStringProduct) - 2,      // 2 bytes string null-terminator in unicode
            USB_DESCRIPTOR_STRING
        },
        CFG_USB_STR_PRODUCT
    };

    // Pointers to used string descriptors
    static const TUsbDescriptorHeader* g_CdcStrings[] =
    {
        (const TUsbDescriptorHeader*) &g_CdcStringLanguages,
        (const TUsbDescriptorHeader*) &g_CdcStringManufacturer,
        (const TUsbDescriptorHeader*) &g_CdcStringProduct,
    };

    // String descriptors list
    static const TUsbdStringList g_CdcStringList = {
        (const TUsbDescriptorHeader**) g_CdcStrings,
        ARR_COUNT(g_CdcStrings)
    };


    // CDC descriptors set
    extern const TUsbdDescriptors g_CdcDescriptors =
    {
        (TUsbDevDescriptor*)    &g_CdcDevDesc,
        (TUsbCfgDescriptor**)   &g_CdcConfigs,
        (TUsbdStringList*)      &g_CdcStringList
    };
