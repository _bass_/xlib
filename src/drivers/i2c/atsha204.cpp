//*****************************************************************************
//
// @brief   ATSHA204 driver (crypto-chip)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/i2c/atsha204.h"
#include "arch/system.h"
#include "arch/i2c.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define ATSHA204_ERR_OK                         0x00
    #define ATSHA204_ERR_CHECKMAC                   0x01
    #define ATSHA204_ERR_PARSE                      0x03
    #define ATSHA204_ERR_EXEC                       0x0F
    #define ATSHA204_ERR_WAKEUP                     0x11
    #define ATSHA204_ERR_CRC                        0xFF

    #define ATSHA204_PACKET_TYPE_RESET              0x00
    #define ATSHA204_PACKET_TYPE_SLEEP              0x01
    #define ATSHA204_PACKET_TYPE_IDLE               0x02
    #define ATSHA204_PACKET_TYPE_CMD                0x03

    #define ATSHA204_CMD_DERIVE_KEY                 0x1C
    #define ATSHA204_CMD_DEV_REV                    0x30
    #define ATSHA204_CMD_GENDIG                     0x15
    #define ATSHA204_CMD_HMAC                       0x11
    #define ATSHA204_CMD_CHECK_MAC                  0x28
    #define ATSHA204_CMD_LOCK                       0x17
    #define ATSHA204_CMD_MAC                        0x08
    #define ATSHA204_CMD_NONCE                      0x16
    #define ATSHA204_CMD_PAUSE                      0x01
    #define ATSHA204_CMD_RANDOM                     0x1B
    #define ATSHA204_CMD_READ                       0x02
    #define ATSHA204_CMD_SHA                        0x47
    #define ATSHA204_CMD_UPDATE_EXTRA               0x20
    #define ATSHA204_CMD_WRITE                      0x12

    // Read/write modes
    #define ATSHA204_ZONE_RW_SIZE_4                 0x00
    #define ATSHA204_ZONE_RW_SIZE_32                0x80
    #define ATSHA204_ZONE_RW_ENCRYPT                0x40


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    #pragma pack(1)

    // Command packet
    typedef struct
    {
        char    addr;           // Chip address
        char    type;           // Packet type (ATSHA204_PACKET_TYPE_CMD)
        char    len;            // Packet size
        char    cmd;            // Operation code (see ATSHA204_CMD_xxx)
        char    param1;         // First command parameter (required)
        uint16  param2;         // Second command parameter (required)
    } TSha204Cmd;

    #pragma pack()


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // I2C configuration parameters
    static const TI2cOptions g_devOpt =
    {
        .speed = 100000,
        .addr = 0x00,
        .addrFormat = I2C_ADDR_FORMAT_7BIT
    };


/**
  * @brief Constructor
  * @param pDev: IO interface (I2C)
  * @param addr: Chip address on the interface
  */
CDrvSha204::CDrvSha204 (CDevChar* pDev, uint8 addr) :
    m_pDev(pDev),
    m_addr(addr),
    m_mutex()
{
    assert(!pDev);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDrvSha204::Open (void)
{
    int result;
    
    m_mutex.Lock();
    
    do
    {
        delay_us(100);
        
        result = m_pDev->Open();
        if (result < 0) break;
        
        result = m_pDev->Configure(&g_devOpt);
        if (result < 0) break;
        
        result = wakeup();
        if (result < 0) break;
        
        // Configuration reading (response in m_buff)
        result = sendCmd(ATSHA204_CMD_READ, ATSHA204_ZONE_CONFIG | ATSHA204_ZONE_RW_SIZE_32, 0);
        if (result < 0) break;
        
        // Validation by known parts of serial number
        if (m_buff[1] != 0x01 || m_buff[2] != 0x23 || m_buff[13] != 0xee)
        {
            result = -10;
            break;
        }
        
        result = sendCmd(ATSHA204_CMD_READ, ATSHA204_ZONE_CONFIG | ATSHA204_ZONE_RW_SIZE_4, 0x15);
        if (result < 0) break;
        
        result = DEV_OK;
    } while (0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDrvSha204::Close (void)
{
    sleep();
    m_pDev->Close();
    
    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CDrvSha204::Configure (const void* params)
{
    return DEV_OK;
}


/**
  * @brief  Random number generation
  * @param  buff: 32 bytes buffer to save the result
  * @return 0 or negative error code
  */
int CDrvSha204::Rand (char* buff)
{
    if (!buff) return -1;
    
    m_mutex.Lock();
    int result;
    
    do
    {
        result = wakeup();
        if (result < 0) break;
        
        char mode = 0;  // Random number is always generating with EEPROM seed update
        result = sendCmd(ATSHA204_CMD_RANDOM, mode, 0x0000, NULL, 0);
        if (result < 0) break;
        
        if (m_buff[0] != 35)
        {
            result = -2;
            break;
        }
    
        memcpy(buff, &m_buff[1], 32);
        
        result = 0;
    } while(0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  MAC calculation
  * @param  mode: MAC generation mode (see ATSHA204_MAC_ADD_xxx)
  * @param  slotId: Slot number for first part of the data (ATSHA204_MAC_ADD_PART_1_DATA_SLOT)
  * @param  buff: 32 bytes buffer to save the result
  * @param  pSalt: Slat (challenge) - second part of the data (32 bytes)
  * @return 0 or negative error code
  */
int CDrvSha204::Mac (char mode, uint16 slotId, char* buff, const char* pSalt)
{
    if ( (mode & ATSHA204_MAC_ADD_PART_2_INPUT) && !pSalt ) return -1;
    
    m_mutex.Lock();
    int result;
    
    do
    {
        result = wakeup();
        if (result < 0) break;
        
        result = sendCmd(ATSHA204_CMD_MAC, mode, slotId, pSalt, (pSalt) ? 32 : 0);
        if (result < 0) break;
        
        if (m_buff[0] != 35)
        {
            result = -2;
            break;
        }
    
        memcpy(buff, &m_buff[1], 32);
        
        result = 0;
    } while(0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  Reading data block (4 or 32 bytes)
  * @param  zone: Memory zone
  * @param  addr: Start address of read data
  * @param  buff: 32 or 4 bytes buffer to save the result
  * @param  size: Data size to read (4 or 32 bytes)
  * @return 0 or negative error code
  */
int CDrvSha204::Read (ESha204Zone zone, uint16 addr, char* buff, uint size)
{
    if (size > 32) size = 32;
    if (size != 4 && size != 32) return -1;
    
    m_mutex.Lock();
    int result;
    
    do
    {
        result = wakeup();
        if (result < 0) break;
        
        char param1 = zone;
        if (size == 32)
        {
            param1 |= ATSHA204_ZONE_RW_SIZE_32;
        }
        
        // TODO: encrypted exchange support
        result = sendCmd(ATSHA204_CMD_READ, param1, addr, NULL, 0);
        if (result < 0) break;
        
        //Checking packet size (length + CRC16)
        if (m_buff[0] != size + 3)
        {
            result = -2;
            break;
        }
    
        memcpy(buff, &m_buff[1], size);
        
        result = 0;
    } while(0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  Writing data block (4 or 32 bytes)
  * @param  zone: Memory zone
  * @param  addr: Start address of write data
  * @param  data: 32 or 4 bytes data
  * @param  size: Data size (4 or 32 bytes)
  * @return 0 or negative error code
  */
int CDrvSha204::Write (ESha204Zone zone, uint16 addr, const char* data, uint size)
{
    if ( size != 4 && size != 32) return -1;
    
    m_mutex.Lock();
    int result;
    
    do
    {
        result = wakeup();
        if (result < 0) break;
        
        char param1 = zone;
        if (size == 32)
        {
            param1 |= ATSHA204_ZONE_RW_SIZE_32;
        }
        
        // TODO: encrypted exchange support
        result = sendCmd(ATSHA204_CMD_WRITE, param1, addr, data, size);
        if (result < 0) break;
        
        if (m_buff[0] != 4)
        {
            result = -2;
            break;
        }
    
        if (m_buff[1] != ATSHA204_ERR_OK)
        {
            result = -3;
            break;
        }
        
        result = 0;
    } while(0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  Memory zone locking
  * @param  zone: Memory zone
  * @param  crc16: Data CRC16 value for integrity check or -1 to lock without checking
  * @return 0 or negative error code
  */
int CDrvSha204::Lock (ESha204Zone zone, int crc16)
{
    m_mutex.Lock();
    int result;
    
    do
    {
        result = wakeup();
        if (result < 0) break;
        
        char param1 = (zone == ATSHA204_ZONE_CONFIG) ? 0 : 1;
        uint16 param2 = 0;
        
        // Zone content validation by CRC
        if (crc16 >= 0)
        {
            param2 = crc16;
        }
        else
        {
            param1 |= 1 << 7;
        }

        result = sendCmd(ATSHA204_CMD_LOCK, param1, param2, NULL, 0);
        if (result < 0) break;
        
        if (m_buff[0] != 4)
        {
            result = -2;
            break;
        }
    
        if (m_buff[1] != ATSHA204_ERR_OK)
        {
            result = -3;
            break;
        }
        
        result = 0;
    } while(0);
    
    sleep();
    m_mutex.Unlock();
    
    return result;
}


/**
  * @brief  Send command to the chip (with waiting for response)
  * @param  cmd: Command code
  * @param  param1: First parameter of the command
  * @param  param2: Second parameter of the command
  * @param  data: Additional data for the command
  * @param  size: Data size
  * @return 0 or negative error code
  */
int CDrvSha204::sendCmd (char cmd, char param1, uint16 param2, const char* data, uint size)
{
    TSha204Cmd* pHdr = (TSha204Cmd*) m_buff;
    int result;
    
    if ( size + sizeof(TSha204Cmd) + sizeof(uint16) > sizeof(m_buff) ) return -1;
    
    do
    {
        pHdr->addr = m_addr;
        pHdr->type = ATSHA204_PACKET_TYPE_CMD;
        pHdr->len = sizeof(TSha204Cmd) - 2 + size + sizeof(uint16);    // First 2 bytes are unused + CRC
        pHdr->cmd = cmd;
        pHdr->param1 = param1;
        pHdr->param2 = param2;
        
        if (size)
        {
            memcpy( (void*)(pHdr + 1), data, size );
        }
        
        uint16* pCrc = (uint16*)&m_buff[pHdr->len];
        *pCrc = GetCrc( &m_buff[2], pHdr->len - sizeof(uint16) );
        
        result = m_pDev->Write(m_buff, pHdr->len + 2);
        if (result < 0) break;
        
        // Waiting for command execution
        uint timeout = 70;
        m_buff[0] = m_addr;
        m_buff[1] = ATSHA204_PACKET_TYPE_RESET;
        
        do
        {
            delay_ms(1);
        } while ( timeout-- && m_pDev->Write(m_buff, 2) );
        
        if (!timeout)
        {
            result = -4;
            break;
        }
        
        m_buff[0] = m_addr;
        result = m_pDev->Read(m_buff, 35);
        if (result < 0) break;
        
        // Packed verification
        if ( m_buff[0] == 0xff || m_buff[0] > sizeof(m_buff) )
        {
            result = -2;
            break;
        }
        
        pCrc = (uint16*)(m_buff + m_buff[0] - sizeof(uint16));
        uint16 crc = GetCrc( m_buff, m_buff[0] - sizeof(uint16) );
        
        if (crc != *pCrc)
        {
            result = -3;
            break;
        }
    } while(0);
    
    return result;
}


/**
  * @brief  Wakeup chip from sleep mode
  * @param  None
  * @return 0 or negative error code
  */
int CDrvSha204::wakeup (void)
{
    m_pDev->PutChar(0x00);
    delay_ms(3);

    char buff[4];
    buff[0] = m_addr;

    int res = m_pDev->Read(buff, 4);
    if (res != 4) return -10;
    
    uint16 crc = GetCrc(buff, 2);
    if (crc != *((uint16*)&buff[2]) || buff[1] != ATSHA204_ERR_WAKEUP) return -11;
    
    return 0;
}


/**
  * @brief  Switch chip to idle mode
  * @param  None
  * @return 0 or negative error code
  */
int CDrvSha204::idle (void)
{
    char buff[2];
    buff[0] = m_addr;
    buff[1] = ATSHA204_PACKET_TYPE_IDLE;

    int res =  m_pDev->Write(buff, 2);
    
    return (res == 2) ? 0 : -1;
}


/**
  * @brief  Switch chip to sleep mode
  * @param  None
  * @return 0 or negative error code
  */
int CDrvSha204::sleep (void)
{
    char buff[2];
    buff[0] = m_addr;
    buff[1] = ATSHA204_PACKET_TYPE_SLEEP;

    int res =  m_pDev->Write(buff, 2);
    
    return (res == 2) ? 0 : -1;
}


/**
  * @brief  CRC value calculation
  * @param  data: Data
  * @param  size: Data size
  * @return 0 or negative error code
  */
uint16 CDrvSha204::GetCrc (const char* data, uint size)
{
    uint16 crc = 0;
    
    while (size--)
    {
        for (uint8 i = 0x01; i; i <<= 1)
        {
            uint8 bitVal = (*data & i) ? 1 : 0;
            
            if ( bitVal == (crc >> 15) )
            {
                crc <<= 1;
            }
            else
            {
                crc = (crc << 1) ^ 0x8005;
            }
        }
        
        data++;
    }
    
    return crc;
}
