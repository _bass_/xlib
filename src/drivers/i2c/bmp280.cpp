//*****************************************************************************
//
// @brief   Pressure sensor BMP280 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/i2c/bmp280.h"
#include "kernel/kthread.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define BMP280_I2C_ADDR                         0xec

    // Registers addresses
    #define BMP280_REG_TEMP_XLSB                    0xfc
    #define BMP280_REG_TEMP_LSB                     0xfb
    #define BMP280_REG_TEMP_MSB                     0xfa
    #define BMP280_REG_PRESS_XLSB                   0xf9
    #define BMP280_REG_PRESS_LSB                    0xf8
    #define BMP280_REG_PRESS_MSB                    0xf7
    #define BMP280_REG_CONFIG                       0xf5
    #define BMP280_REG_CTRL_MEAS                    0xf4
    #define BMP280_REG_STATUS                       0xf3
    #define BMP280_REG_RESET                        0xe0
    #define BMP280_REG_ID                           0xd0
    #define BMP280_REG_COMP_DATA                    0x88

    // Status masks
    #define BMP280_STATUS_MEASURING                 (1 << 3)
    #define BMP280_STATUS_UPDATE                    (1 << 0)

    #define BMP280_CTRL_OSRS_T_POS                  5
    #define BMP280_CTRL_OSRS_T_MASK                 0xe0
    #define BMP280_CTRL_OSRS_P_POS                  2
    #define BMP280_CTRL_OSRS_P_MASK                 0x1c
    #define BMP280_CTRL_OSRS_SKIP                   0x00
    #define BMP280_CTRL_OSRS_X1                     0x01
    #define BMP280_CTRL_OSRS_X2                     0x02
    #define BMP280_CTRL_OSRS_X4                     0x03
    #define BMP280_CTRL_OSRS_X8                     0x04
    #define BMP280_CTRL_OSRS_X16                    0x05
    #define BMP280_CTRL_MODE_MASK                   0x03
    #define BMP280_CTRL_MODE_SLEEP                  0x00
    #define BMP280_CTRL_MODE_FORCED                 0x01
    #define BMP280_CTRL_MODE_NORMAL                 0x03

    #define BMP280_CONFIG_T_SB_POS                  5
    #define BMP280_CONFIG_T_SB_MASK                 0xe0
    #define BMP280_CONFIG_T_SB_0_5                  (0 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_62_5                 (1 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_125                  (2 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_250                  (3 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_500                  (4 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_1000                 (5 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_2000                 (6 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_T_SB_4000                 (7 << BMP280_CONFIG_T_SB_POS)
    #define BMP280_CONFIG_FILTER_POS                2
    #define BMP280_CONFIG_FILTER_MASK               0x1c

    #define BMP280_ID                               0x58
    #define BMP280_RESET_CODE                       0xb6

    // Temperature value caching time (ms)
    #define BMP280_TEMP_CACHE_TIME                  10


/**
  * @brief Constructor
  * @param pDev: IO interface (I2C)
  */
CBmp280::CBmp280 (CDevChar* pDev) :
    CDevice(),
    m_pDev(pDev),
    m_fineT(0),
    m_cacheTemp(0),
    m_cacheTime(0)
{
    assert(!pDev);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBmp280::Open (void)
{
    // Startup time 2ms
    KThread::sleep(3);

    if (m_pDev->GetState() != DEV_OPENED) return -1;

    // Software reset
    char buff[8] = {};
    buff[0] = BMP280_I2C_ADDR;
    buff[1] = BMP280_REG_RESET;
    buff[2] = BMP280_RESET_CODE;

    int result = m_pDev->Write(buff, 3);
    KThread::sleep(3);

    // ID reading
    buff[1] = BMP280_REG_ID;
    result = m_pDev->Write(buff, 2);
    if (result < 0) return -3;

    result = m_pDev->Read(buff, 1);
    if (result < 0) return -4;
    if (!result || buff[0] != BMP280_ID) return -5;

    // Reading of compensation parameters
    buff[0] = BMP280_I2C_ADDR;
    buff[1] = BMP280_REG_COMP_DATA;
    result = m_pDev->Write(buff, 2);

    memset( m_compData, 0x00, sizeof(m_compData) );
    m_compData[0] = BMP280_I2C_ADDR;

    Kernel::EnterCritical();
    result = m_pDev->Read( m_compData, sizeof(m_compData) );
    Kernel::ExitCritical();
    if ( result != sizeof(m_compData) ) return -6;

    // Setting working mode and measurement accuracy
    buff[0] = BMP280_I2C_ADDR;
    buff[1] = BMP280_REG_CONFIG;
    buff[2] = BMP280_CONFIG_T_SB_0_5;
    buff[3] = BMP280_REG_CTRL_MEAS;
    buff[4] = BMP280_CTRL_OSRS_X1 << BMP280_CTRL_OSRS_T_POS | BMP280_CTRL_OSRS_X4 << BMP280_CTRL_OSRS_P_POS | BMP280_CTRL_MODE_NORMAL;
    Kernel::EnterCritical();
    result = m_pDev->Write(buff, 5);
    Kernel::ExitCritical();
    if (result < 0) return -7;

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBmp280::Close (void)
{
    m_pDev->Close();

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (not used)
  * @return DEV_OK or negative error code
  */
int CBmp280::Configure (const void* params)
{
    return DEV_OK;
}


/**
  * @brief  Read measurement values
  * @param  channel: Parameter ID (channel)
  * @param  pVal: Pointer to variable to save value
  * @return 0 or negative error code
  */
int CBmp280::GetData (TBmp280Ch channel, float* pVal)
{
    if (!pVal) return -1;

    // Get temperature value from cache if it's still valid
    if (
        channel == BMP280_CH_TEMP &&
        m_cacheTime &&
        !IsTimeout(m_cacheTime, BMP280_TEMP_CACHE_TIME)
    ) {
        *pVal = m_cacheTemp;
        return 0;
    }

    char buff[8] = {};
    buff[0] = BMP280_I2C_ADDR;
    buff[1] = BMP280_REG_PRESS_MSB;

    int result = m_pDev->Write(buff, 2);
    if (result <= 0) return -20 + result;

    // Pressure value is calculated with temperatire compensation
    // That's why temperature is always reading and saving for BMP280_TEMP_CACHE_TIME
    Kernel::EnterCritical();
    result = m_pDev->Read(buff, 6);
    Kernel::ExitCritical();
    if (result != 6) return -30 - result;

    m_cacheTemp = getCompensateTemp(&buff[3]);
    m_cacheTime = Kernel::GetJiffies();

    if (channel == BMP280_CH_TEMP)
    {
        *pVal = m_cacheTemp;
        return 0;
    }

    if (channel != BMP280_CH_PRESS) return -4;

    *pVal = getCompensatePress(&buff[0]);

    return 0;
}


/**
  * @brief  Pressure value calculation with correction (more accurate)
  * @param  pRawData: Raw data from the chip
  * @return Pressure value
  */
float CBmp280::getCompensatePress (const char* pRawData)
{
    sint32 val = pRawData[0] << 12 | pRawData[1] << 4 | pRawData[2] >> 4;
    uint16* pDigP1 = (uint16*) &m_compData[6];
    sint16* pDigP2 = (sint16*) &m_compData[8];
    sint16* pDigP3 = (sint16*) &m_compData[10];
    sint16* pDigP4 = (sint16*) &m_compData[12];
    sint16* pDigP5 = (sint16*) &m_compData[14];
    sint16* pDigP6 = (sint16*) &m_compData[16];
    sint16* pDigP7 = (sint16*) &m_compData[18];
    sint16* pDigP8 = (sint16*) &m_compData[20];
    sint16* pDigP9 = (sint16*) &m_compData[22];

    sint64 var1, var2, p;
    var1 = ((sint64)m_fineT) - 128000;
    var2 = var1 * var1 * (sint64)(*pDigP6);
    var2 = var2 + ((var1 * (sint64)(*pDigP5)) << 17);
    var2 = var2 + (((sint64)(*pDigP4)) << 35);
    var1 = ((var1 * var1 * (sint64)(*pDigP3)) >> 8) + ((var1 * (sint64)(*pDigP2)) << 12);
    var1 = (((((sint64)1) << 47) + var1)) * ((sint64)(*pDigP1)) >> 33;

    if (var1 == 0) return 0; // Avoid exception caused by division by zero

    p = 1048576 - val;
    p = (((p << 31) - var2) * 3125) / var1;
    var1 = (((sint64)(*pDigP9)) * (p >> 13) * (p >> 13)) >> 25;
    var2 = (((sint64)(*pDigP8)) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + (((sint64)(*pDigP7)) << 4);

    return (float)p / 256 / 100;

}


/**
  * @brief  Temperature value calculation with correction (more accurate)
  * @param  pRawData: Raw data from the chip
  * @return Temperature value
  */
float CBmp280::getCompensateTemp (char* pRawData)
{
    sint32 val = pRawData[0] << 12 | pRawData[1] << 4 | pRawData[2] >> 4;
    uint16* pDigT1 = (uint16*) &m_compData[0];
    sint16* pDigT2 = (sint16*) &m_compData[2];
    sint16* pDigT3 = (sint16*) &m_compData[4];

    sint32 var1 = ( (((val >> 3) - ((sint32)(*pDigT1) << 1))) * ((sint32)(*pDigT2)) ) >> 11;
    sint32 var2 = ( ((((val >> 4) - ((sint32)(*pDigT1))) * ((val >> 4) - ((sint32)(*pDigT1)))) >> 12) * ((sint32)(*pDigT3)) ) >> 14;

    m_fineT = var1 + var2;

    return (float)((m_fineT * 5 + 128) >> 8) / 100;
}
