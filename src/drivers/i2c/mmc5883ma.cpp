//*****************************************************************************
//
// @brief   Magnetometer MMC5883MA driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/i2c/mmc5883ma.h"
#include "kernel/kthread.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define MMC5883_I2C_ADDR                    0x60

    // Registers addresses
    #define MMC5883_REG_XL                      0x00
    #define MMC5883_REG_XH                      0x01
    #define MMC5883_REG_YL                      0x02
    #define MMC5883_REG_YH                      0x03
    #define MMC5883_REG_ZL                      0x04
    #define MMC5883_REG_ZH                      0x05
    #define MMC5883_REG_TEMP                    0x06
    #define MMC5883_REG_STATUS                  0x07
    #define MMC5883_REG_CTRL_0                  0x08
    #define MMC5883_REG_CTRL_1                  0x09
    #define MMC5883_REG_CTRL_2                  0x0a
    #define MMC5883_REG_THR_X                   0x0b
    #define MMC5883_REG_THR_Y                   0x0c
    #define MMC5883_REG_THR_Z                   0x0d
    #define MMC5883_REG_PID                     0x2f

    // Registers bit masks
    #define MMC5883_STATUS_MEAS_M_DONE          (1 << 0)
    #define MMC5883_STATUS_MEAS_T_DONE          (1 << 1)
    #define MMC5883_STATUS_MOTION               (1 << 2)
    #define MMC5883_STATUS_PUMP_ON              (1 << 3)
    #define MMC5883_STATUS_OTP_RD_DONE          (1 << 4)

    #define MMC5883_CTRL_0_TM_M                 (1 << 0)
    #define MMC5883_CTRL_0_TM_T                 (1 << 1)
    #define MMC5883_CTRL_0_START_MD             (1 << 2)
    #define MMC5883_CTRL_0_SET                  (1 << 3)
    #define MMC5883_CTRL_0_RESET                (1 << 4)
    #define MMC5883_CTRL_0_OTP_RD               (1 << 6)
    #define MMC5883_CTRL_0_TEST_PIN_SEL         (1 << 7)

    #define MMC5883_CTRL_1_BW_MASK              (3 << 0)
    #define MMC5883_CTRL_1_BW_100HZ             (0 << 0)
    #define MMC5883_CTRL_1_BW_200HZ             (1 << 0)
    #define MMC5883_CTRL_1_BW_400HZ             (2 << 0)
    #define MMC5883_CTRL_1_BW_600HZ             (3 << 0)
    #define MMC5883_CTRL_1_DISABLE_X            (1 << 2)
    #define MMC5883_CTRL_1_DISABLE_Y            (1 << 3)
    #define MMC5883_CTRL_1_DISABLE_Z            (1 << 4)
    #define MMC5883_CTRL_1_SW_RST               (1 << 7)

    #define MMC5883_CTRL_2_CM_FREQ_MASK         0x0f
    #define MMC5883_CTRL_2_CM_FREQ_OFF          0x00
    #define MMC5883_CTRL_2_CM_FREQ_14HZ         0x01
    #define MMC5883_CTRL_2_CM_FREQ_5HZ          0x02
    #define MMC5883_CTRL_2_CM_FREQ_2P2HZ        0x03
    #define MMC5883_CTRL_2_CM_FREQ_1HZ          0x04
    #define MMC5883_CTRL_2_CM_FREQ_1D2HZ        0x05
    #define MMC5883_CTRL_2_CM_FREQ_1D4HZ        0x06
    #define MMC5883_CTRL_2_CM_FREQ_1D8HZ        0x07
    #define MMC5883_CTRL_2_CM_FREQ_1D16HZ       0x08
    #define MMC5883_CTRL_2_CM_FREQ_1D32HZ       0x09
    #define MMC5883_CTRL_2_CM_FREQ_1D64HZ       0x0a
    #define MMC5883_CTRL_2_CM_INT_MDT_EN        (1 << 5)
    #define MMC5883_CTRL_2_CM_INT_MEAS_DONE_EN  (1 << 6)


/**
  * @brief Constructor
  * @param pDev: IO interface (I2C)
  * @param pinIrq: IRQ pin
  */
CMmc5883::CMmc5883 (CDevChar* pDev, TGpio pinIrq) :
    CDevice(),
    m_pDev(pDev),
    m_pinIrq(pinIrq)
{
    assert(!pDev);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CMmc5883::Open (void)
{
    KThread::sleep(5);

    if (m_pDev->GetState() != DEV_OPENED) return -1;

    // Software reset
    char buff[4] = {};
    buff[0] = MMC5883_I2C_ADDR;
    buff[1] = MMC5883_REG_CTRL_1;
    buff[2] = MMC5883_CTRL_1_SW_RST;

    int result = m_pDev->Write(buff, 3);
    KThread::sleep(10);

    // Product ID verification
    buff[1] = MMC5883_REG_PID;
    result = m_pDev->Write(buff, 2);
    if (result < 0) return -3;

    result = m_pDev->Read(buff, 1);
    if (result < 0) return -4;
    if (buff[0] != 0x0c) return -5;

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CMmc5883::Close (void)
{
    m_pDev->Close();

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (not used)
  * @return DEV_OK or negative error code
  */
int CMmc5883::Configure (const void* params)
{
    const TMmc5883Odr odr = *( (const TMmc5883Odr*)params );

    char buff[4] = {};
    buff[0] = MMC5883_I2C_ADDR;
    buff[1] = MMC5883_REG_CTRL_1;
    buff[2] = odr;
    int result = m_pDev->Write(buff, 3);

    return (result < 0) ? DEV_ERR : DEV_OK;
}


/**
  * @brief  Read measurement values
  * @param  channel: Parameter ID (channel)
  * @param  pVal: Pointer to variable to save value
  * @return 0 or negative error code
  */
int CMmc5883::GetData (TMmc5883Ch channel, float* pVal)
{
    if (!pVal) return -1;

    char buff[12] = {};

    buff[0] = MMC5883_I2C_ADDR;
    buff[1] = MMC5883_REG_CTRL_0;
    buff[2] = MMC5883_CTRL_0_TM_M | MMC5883_CTRL_0_TM_T;

    int result = m_pDev->Write(buff, 3);
    if (result <= 0) return -2;

    KThread::sleep(50);

    do
    {
        buff[0] = MMC5883_I2C_ADDR;
        buff[1] = MMC5883_REG_STATUS;
        result = m_pDev->Write(buff, 2);
        if (result <= 0) return -3;

        result = m_pDev->Read(buff, 1);
        if (result <= 0) return -3;
    } while ( (buff[1] & (MMC5883_STATUS_MEAS_M_DONE|MMC5883_STATUS_MEAS_T_DONE)) != (MMC5883_STATUS_MEAS_M_DONE|MMC5883_STATUS_MEAS_T_DONE) );

    buff[0] = MMC5883_I2C_ADDR;
    buff[1] = MMC5883_REG_XL;
    result = m_pDev->Write(buff, 2);
    if (result <= 0) return -4;

    result = m_pDev->Read(buff, 8);
    if (result <= 0) return -5;

    uint16* pRawVal;
    switch (channel)
    {
        case MMC5883_CH_X: pRawVal = (uint16*)&buff[0]; break;
        case MMC5883_CH_Y: pRawVal = (uint16*)&buff[2]; break;
        case MMC5883_CH_Z: pRawVal = (uint16*)&buff[4]; break;
        default: return -6;
    }

    *pVal = (float)(*pRawVal);

    return 0;
}
