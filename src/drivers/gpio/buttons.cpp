//*****************************************************************************
//
// @brief   Simple buttons state processing
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gpio/buttons.h"
#include "kernel/kdebug.h"
#include "kernel/kernel.h"
#include "kernel/events.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include "def_board.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define BUTTON_THREAD_STACK_SIZE        512
    #define BUTTON_WAIT_TIME_FOREVER        ( ~0u )


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    struct _TButtonDesc
    {   
        uint32      jiff;           // Timestamp of last button state changing
        TBtnState   state;          // Last button state
        TBtnState   stateFixed;     // Fixed state of the button
    };

// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static int getFixTime (const TButtonDesc* pDesc, const TButtonCfg* pCfg);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("btn", "Buttons", NULL, NULL);

    CDrvButton* CDrvButton::m_instance = NULL;


/**
  * @brief  Constructor
  * @param  pCfg: Buttons configurations
  * @param  btnCount: Number of processing buttons 
  * @param  priority: Thread priority for buttons states processing
  */
CDrvButton::CDrvButton (const TButtonCfg* pCfg, uint btnCount, uint priority) :
    KThreadWait("Buttons", BUTTON_THREAD_STACK_SIZE, priority),
    m_pCfg(pCfg),
    m_btnCount(btnCount)
{
    m_instance = this;

    assert(!pCfg || !btnCount);

    uint size = sizeof(TButtonDesc) * btnCount;
    m_pDesc = (TButtonDesc*) malloc(size);
    assert(!m_pDesc);

    memset(m_pDesc, 0, size);
}


/**
  * @brief  Destructor
  * @param  None
  */
CDrvButton::~CDrvButton ()
{
    m_instance = NULL;
    if (m_pDesc) free(m_pDesc);
}


/**
  * @brief  Thread initialization function
  * @param  None
  */
void CDrvButton::init (void)
{
    const TButtonCfg* pCfg = m_pCfg;
    TButtonDesc* pDesc = m_pDesc;
    uint32 now = Kernel::GetJiffies();
    if (!now) --now;

    for (uint i = 0; i < m_btnCount; ++i, ++pCfg, ++pDesc)
    {
        bool currState = pCfg->getHwState(i);
        if (pCfg->flags.invert) currState = !currState;
        // State inversion for for correct first time state fixation and appropriate event generation
        pDesc->stateFixed = (currState) ? BTN_STATE_RELEASED : BTN_STATE_PRESSED;
        pDesc->state = (currState) ? BTN_STATE_PRESSED : BTN_STATE_RELEASED;
        pDesc->jiff = now;
    }

    uint sleepTime = getWaitTime();
    if (sleepTime)
    {
        if (sleepTime == BUTTON_WAIT_TIME_FOREVER)  Suspend();
        else                                        wait(sleepTime);
    }

}


/**
  * @brief  Thread main function
  * @param  None
  */
void CDrvButton::run(void)
{
    for (;;)
    {
        const TButtonCfg* pCfg = m_pCfg;
        TButtonDesc* pDesc = m_pDesc;

        for (uint i = 0; i < m_btnCount; ++i, ++pCfg, ++pDesc)
        {
            // Current state processing
            if (!pCfg->flags.irq)
            {
                if (!pCfg->getHwState) continue;

                bool currState = pCfg->getHwState(i);
                processHwState(i, currState);
            }

            if (!pDesc->jiff) continue;

            // State fixation timeout calculation
            int fixTime = getFixTime(pDesc, pCfg);
            if (fixTime < 0)
            {
                pDesc->jiff = 0;
                continue;
            }
            if ( !IsTimeout(pDesc->jiff, fixTime) ) continue;


            if (pDesc->state == pDesc->stateFixed)
            {
                pDesc->jiff = 0;
            }
            else
            {
                // State fixation
                pDesc->stateFixed = pDesc->state;

                uint ev;
                switch (pDesc->stateFixed)
                {
                    case BTN_STATE_RELEASED:
                        kdebug("%s released", pCfg->name);
                        pDesc->jiff = 0;
                        ev = EV_KEY_RELEASED;
                    break;

                    case BTN_STATE_PRESSED:
                        kdebug("%s pressed",  pCfg->name);
                        if (pCfg->timeLongPress)    pDesc->state = BTN_STATE_LONGPRESS;
                        else                        pDesc->jiff = 0;
                        ev = EV_KEY_PRESSED;
                    break;

                    case BTN_STATE_LONGPRESS:
                        kdebug("%s pressed (long)",  pCfg->name);
                        pDesc->jiff = 0;
                        ev = EV_KEY_LONGPRESS;
                    break;

                    default: assert(1);
                }

                Kernel::PostEvent(ev, (void*)i);
            }
        } // for (uint i = 0; i < m_btnCount; ++i, ++pCfg, ++pDesc)

        // Suspend thread until next state processing
        uint sleepTime = getWaitTime();
        if (sleepTime)
        {
            if (sleepTime == BUTTON_WAIT_TIME_FOREVER)  Suspend();
            else                                        wait(sleepTime);
        }
    } // for (;;)
}


/**
  * @brief  Button hardware state processing
  * @param  btnId: Button ID (index in descriptors table)
  * @param  hwState: Current hardware state
  */
void CDrvButton::processHwState (uint btnId, bool hwState)
{
    if (btnId >= m_instance->m_btnCount) return;

    const TButtonCfg* pCfg = &m_pCfg[btnId];
    TButtonDesc* pDesc = &m_pDesc[btnId];

    // Convertation of hardware state into logical
    if (pCfg->flags.invert) hwState = !hwState;

    TBtnState state;
    if (hwState)
    {
        state = (pDesc->stateFixed == BTN_STATE_RELEASED || !pCfg->timeLongPress)
                ? BTN_STATE_PRESSED
                : BTN_STATE_LONGPRESS;
    }
    else
    {
        state = BTN_STATE_RELEASED;
    }

    // Run fixation timer on state changing
    if (state != pDesc->state)
    {
        pDesc->state = state;
        pDesc->jiff = Kernel::GetJiffies();
        if (!pDesc->jiff) --pDesc->jiff;
    }
}


/**
  * @brief  Minimal inactivity time calculation
  * @return Delay value before next processing moment (ms)
  */
uint CDrvButton::getWaitTime (void)
{
    const TButtonCfg* pCfg = m_pCfg;
    TButtonDesc* pDesc = m_pDesc;
    uint minTime = ( ~0u );

    for (uint i = 0; i < m_btnCount; ++i, ++pCfg, ++pDesc)
    {
        uint waitTime;

        if (pCfg->flags.irq)
        {
            if (!pDesc->jiff) continue;
            if (pDesc->stateFixed == BTN_STATE_LONGPRESS && pDesc->state == BTN_STATE_LONGPRESS) continue;

            int fixTime = getFixTime(pDesc, pCfg);
            if (fixTime < 0) continue;

            waitTime = ( IsTimeout(pDesc->jiff, fixTime) ) ? 0 : fixTime - (Kernel::GetJiffies() - pDesc->jiff);
        }
        else
        {
            // For buttons with periodical state check,
            // processing period is 2 times less than time of state fixation
            waitTime = pCfg->timeFix >> 1;
        }

        if (waitTime < minTime)
        {
            minTime = waitTime;
        }
    }

    return minTime;
}


/**
  * @brief  Read button current state
  * @param  btnId: ID of the button (index in list of descriptors)
  * @return Button state
  */
TBtnState CDrvButton::GetState (uint btnId)
{
    return (!m_instance || btnId >= m_instance->m_btnCount)
            ? BTN_STATE_RELEASED
            : m_instance->m_pDesc[btnId].stateFixed;
}


/**
  * @brief  Button state changing handler (IRQ)
  * @param  btnId: ID of the button (index in list of descriptors)
  * @param  state: Button state (hardware)
  */
void CDrvButton::OnStateChanged (uint btnId, bool state)
{
    if (!m_instance || btnId >= m_instance->m_btnCount) return;

    uint currDelay = m_instance->getWaitTime();

    m_instance->processHwState(btnId, state);

    // Chech button state changing
    TButtonDesc* pDesc = &m_instance->m_pDesc[btnId];
    if (pDesc->state == pDesc->stateFixed) return;

    uint sleepTime = m_instance->getWaitTime();
    if (sleepTime == BUTTON_WAIT_TIME_FOREVER) return;

    if (sleepTime)
    {
        // Restart fixation timer
        if (sleepTime < currDelay) m_instance->m_threadTimer.Start(sleepTime + 1);
    }
    else
    {
        m_instance->Resume();
    }
}


/**
  * @brief  Button state fixation time calculation
  * @param  pDesc: Button state descriptor
  * @param  pCfg: Button configuration
  * @return State fixation time or negative error code
  */
int getFixTime (const TButtonDesc* pDesc, const TButtonCfg* pCfg)
{
    if (pDesc->stateFixed == BTN_STATE_LONGPRESS && pDesc->state == BTN_STATE_LONGPRESS) return -1;

    if (pDesc->state == BTN_STATE_LONGPRESS)
    {
        if (!pCfg->timeLongPress) return -2;
        return pCfg->timeLongPress;
    }

    return pCfg->timeFix;
}
