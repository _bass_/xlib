//*****************************************************************************
//
// @brief   Character device driver for SEGGER RTT
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/debug/segger_rtt.h"
#include "misc/math.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // IO buffers size
    #if defined(CFG_RTT_BUFF_SIZE_UP)
        #define RTT_BUFF_SIZE_UP            CFG_RTT_BUFF_SIZE_UP
    #else
        #define RTT_BUFF_SIZE_UP            256
    #endif
    #if defined(CFG_RTT_BUFF_SIZE_DOWN)
        #define RTT_BUFF_SIZE_DOWN          CFG_RTT_BUFF_SIZE_DOWN
    #else
        #define RTT_BUFF_SIZE_DOWN          64
    #endif

    // Operating modes. Define behavior if buffer is full (not enough space for entire message)
    #define SEGGER_RTT_MODE_NO_BLOCK_SKIP         (0U)     // Skip. Do not block, output nothing. (Default)
    #define SEGGER_RTT_MODE_NO_BLOCK_TRIM         (1U)     // Trim: Do not block, output as much as fits.
    #define SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL    (2U)     // Block: Wait until there is space in the buffer.
    #define SEGGER_RTT_MODE_MASK                  (3U)


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Description for a circular buffer (also called "ring buffer")
    // which is used as up-buffer (T->H)
    typedef struct
    {
        const char*     sName;         // Optional name. Standard names so far are: "Terminal", "SysView", "J-Scope_t4i4"
        char*           pBuffer;       // Pointer to start of buffer
        uint            SizeOfBuffer;  // Buffer size in bytes. Note that one byte is lost, as this implementation does not fill up the buffer in order to avoid the problem of being unable to distinguish between full and empty.
        uint            WrOff;         // Position of next item to be written by either target.
        volatile uint   RdOff;         // Position of next item to be read by host. Must be volatile since it may be modified by host.
        uint            Flags;         // Contains configuration flags
    } SEGGER_RTT_BUFFER_UP;

    // Description for a circular buffer (also called "ring buffer")
    // which is used as down-buffer (H->T)
    typedef struct
    {
        const char*     sName;         // Optional name. Standard names so far are: "Terminal", "SysView", "J-Scope_t4i4"
        char*           pBuffer;       // Pointer to start of buffer
        uint            SizeOfBuffer;  // Buffer size in bytes. Note that one byte is lost, as this implementation does not fill up the buffer in order to avoid the problem of being unable to distinguish between full and empty.
        volatile uint   WrOff;         // Position of next item to be written by host. Must be volatile since it may be modified by host.
        uint            RdOff;         // Position of next item to be read by target (down-buffer).
        uint            Flags;         // Contains configuration flags
    } SEGGER_RTT_BUFFER_DOWN;

    // RTT control block which describes the number of buffers available
    // as well as the configuration for each buffer
    typedef struct
    {
        char                    acID[16];             // Initialized to "SEGGER RTT"
        int                     MaxNumUpBuffers;      // Initialized to SEGGER_RTT_MAX_NUM_UP_BUFFERS (type. 2)
        int                     MaxNumDownBuffers;    // Initialized to SEGGER_RTT_MAX_NUM_DOWN_BUFFERS (type. 2)
        SEGGER_RTT_BUFFER_UP    aUp;                  // Up buffers, transferring information up from target via debug probe to host
        SEGGER_RTT_BUFFER_DOWN  aDown;                // Down buffers, transferring information down from host via debug probe to target
    } SEGGER_RTT_CB;


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // RTT control block
    static SEGGER_RTT_CB g_rttDesc;
    // IO buffers
    static char g_buffUp[RTT_BUFF_SIZE_UP];
    static char g_buffDown[RTT_BUFF_SIZE_DOWN];


/**
  * @brief  Constructor
  * @param  None
  */
CDevSeggerRtt::CDevSeggerRtt () :
    CDevChar()
{
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevSeggerRtt::Open (void)
{
    // Initialize control block
    // (x1 up buffer, x1 down buffer)
    g_rttDesc.MaxNumUpBuffers    = 1;
    g_rttDesc.MaxNumDownBuffers  = 1;

    g_rttDesc.aUp.sName         = "Terminal";
    g_rttDesc.aUp.pBuffer       = g_buffUp;
    g_rttDesc.aUp.SizeOfBuffer  = sizeof(g_buffUp);
    g_rttDesc.aUp.RdOff         = 0u;
    g_rttDesc.aUp.WrOff         = 0u;
    g_rttDesc.aUp.Flags         = SEGGER_RTT_MODE_NO_BLOCK_SKIP;

    g_rttDesc.aDown.sName         = "Terminal";
    g_rttDesc.aDown.pBuffer       = g_buffDown;
    g_rttDesc.aDown.SizeOfBuffer  = sizeof(g_buffDown);
    g_rttDesc.aDown.RdOff         = 0u;
    g_rttDesc.aDown.WrOff         = 0u;
    g_rttDesc.aDown.Flags         = SEGGER_RTT_MODE_NO_BLOCK_SKIP;

    // Finish initialization of the control block.
    // Copy Id string in three steps to make sure "SEGGER RTT" is not found
    // in initializer memory (usually flash) by J-Link
    strcpy(&g_rttDesc.acID[7], "RTT");
    strcpy(&g_rttDesc.acID[0], "SEGGER");
    g_rttDesc.acID[6] = ' ';

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevSeggerRtt::Close (void)
{
    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (unused)
  * @return DEV_OK or negative error code
  */
int CDevSeggerRtt::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}


/**
  * @brief  Send character to output stream
  * @param  c: character
  * @return 0 or negative error code
  */
int CDevSeggerRtt::PutChar (const char c)
{
    SEGGER_RTT_BUFFER_UP* pRing = &g_rttDesc.aUp;

    // Get write position and handle wrap-around if necessary
    uint WrOff = pRing->WrOff + 1;
    if (WrOff == pRing->SizeOfBuffer)
    {
        WrOff = 0;
    }

    // Wait for free space if mode is set to blocking
    if (pRing->Flags == SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL)
    {
        while (WrOff == pRing->RdOff);
    }

    // Output byte if free space is available
    if (WrOff == pRing->RdOff) return -1;

    pRing->pBuffer[pRing->WrOff] = c;
    pRing->WrOff = WrOff;

    return 0;
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CDevSeggerRtt::GetChar (void)
{
    SEGGER_RTT_BUFFER_DOWN* pRing = &g_rttDesc.aDown;
    if (pRing->RdOff == pRing->WrOff) return -1;

    char c = pRing->pBuffer[pRing->RdOff];

    // Handle wrap-around of buffer
    ++pRing->RdOff;
    if (pRing->RdOff >= pRing->SizeOfBuffer) pRing->RdOff = 0;

    return (int)c;
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CDevSeggerRtt::Write (const void* buff, uint size)
{
    if (size && !buff) return -1;

    uint count = 0;
    const char* ptr = (const char*) buff;

    for (; count < size; ++count)
    {
        int res = PutChar( *ptr++ );
        if (res < 0) break;
    }

    return (int)count;
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CDevSeggerRtt::Read (void* buff, uint size)
{
    if (size && !buff) return -1;

    uint count = 0;
    char* ptr = (char*) buff;

    for (; count < size; ++count)
    {
        int c = GetChar();
        if (c < 0) break;
        *ptr++ = (char)c;
    }

    return (int)count;
}
