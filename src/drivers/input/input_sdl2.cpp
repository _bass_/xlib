//*****************************************************************************
//
// @brief   Input device (mouse + keyboard) driver based on SDL2 library (host)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/input/input_sdl2.h"
#include "misc/macros.h"
#include <SDL2/SDL_events.h>


CDevInputSdl2::CDevInputSdl2 (uint devIndex):
    CDevInput(devIndex, E_MSG_POINTER | E_MSG_KBD),
    m_mouseButtons(0),
    m_x(0),
    m_y(0)
{
}

int CDevInputSdl2::Open (void)
{
    return DEV_OK;
}

int CDevInputSdl2::Close (void)
{
    return DEV_OK;
}

int CDevInputSdl2::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}

void CDevInputSdl2::onMouseMove (const void* ev)
{
    const SDL_MouseMotionEvent* ptr = (const SDL_MouseMotionEvent*) ev;
    if (m_x == ptr->x && m_y == ptr->y) return;

    int16 dx = ptr->x - m_x;
    int16 dy = ptr->y - m_y;
    m_x = ptr->x;
    m_y = ptr->y;

    TMsg msg =
    {
        .type = E_MSG_POINTER,
        .pointer =
        {
            .type = E_EV_POINTER_MOVE,
            .x = m_x, .y = m_y,
            .move = {.dx = dx, .dy = dy},
        },
    };
    notify(msg);
}

void CDevInputSdl2::onMouseButton (const void* ev)
{
    const SDL_MouseButtonEvent* ptr = (const SDL_MouseButtonEvent*) ev;
    uint32 changeMask = m_mouseButtons ^ ptr->state;
    bool isPressed = ptr->state & changeMask;

    if (m_mouseButtons == ptr->state) return;

    m_mouseButtons = ptr->state;
    m_x = ptr->x;
    m_y = ptr->y;

    TMsg msg =
    {
        .type = E_MSG_POINTER,
        .pointer =
        {
            .type = E_EV_POINTER_PRESS,
            .x = m_x, .y = m_y,
            .press = {.isPressed = isPressed},
        },
    };
    notify(msg);
}

void CDevInputSdl2::onKbdButton(const void* ev)
{
    const SDL_KeyboardEvent* ptr = (const SDL_KeyboardEvent*) ev;


    TMsg msg =
    {
        .type = E_MSG_KBD,
        .kbd =
        {
            .state = (ptr->state == SDL_PRESSED),
            .scanCode = (uint16)ptr->keysym.scancode,
            .symbol = (uint)ptr->keysym.sym,
            .modifiers = {.val = 0},
        }
    };

    if (ptr->keysym.mod & (KMOD_LCTRL | KMOD_RCTRL))    msg.kbd.modifiers.ctrl = 1;
    if (ptr->keysym.mod & (KMOD_LSHIFT | KMOD_RSHIFT))  msg.kbd.modifiers.shift = 1;

    notify(msg);
}
