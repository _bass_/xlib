//*****************************************************************************
//
// @brief   Display device driver for virtual screen on a host
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/display/host_sdl2.h"
#include "kernel/kernel.h"
#include "kernel/kthread.h"
#include "kernel/kdebug.h"
#include "misc/macros.h"
#include "misc/time.h"

#include <SDL2/SDL.h>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Forward declaration of worker class
    class CSdlWorker;

    struct CDevDisplaySdl2::TContext
    {
        uint            width;
        uint            height;
        SDL_Window*     window;
        CSdlWorker*     pollThread;
        bool            workerIsActive;
        CDevInputSdl2*  devInput;
    };


    class CSdlWorker: public KThread
    {
        public:
            /**
              * @brief  Constructor
              * @param  pContext: Pointer to SDL context data
              */
            CSdlWorker (CDevDisplaySdl2::TContext* pContext);

            /**
              * @brief  Destructor
              * @param  None
              */
            ~CSdlWorker ();

            /**
              * @brief  Update screen request
              * @param  None
              */
            void update (void);

            /**
              * @brief  Stop processing thread request.
              *         When thread stoped, 'workerIsActive' will be set to false
              * @param  None
              */
            void stop (void);

        protected:
            CDevDisplaySdl2::TContext* const m_pContext;
            uint32  m_updateTime;
            bool    m_flagStop;

            /**
              * @brief  Thread initialization
              * @param  None
              */
            void init (void);

            /**
              * @brief  Thread main function
              * @param  None
              */
            void run (void);

            /**
             * @brief  Thread deinitialization
             * @param  None
             */
            void uninit (void);
    };


/**
  * @brief  Constructor
  * @param  width: Screen width (pixels)
  * @param  height: Screen height (pixels)
  * @param  devInput: Input device to handle input events from the screen
  */
CDevDisplaySdl2::CDevDisplaySdl2 (uint width, uint height, CDevInputSdl2* devInput) :
    CDevDisplay(),
    m_pContext(nullptr),
    m_width(width),
    m_height(height),
    m_devInput(devInput)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CDevDisplaySdl2::~CDevDisplaySdl2 ()
{
    if (m_pContext)
    {
        contextUninit();
    }
}


/**
  * @brief  Get screen width
  * @return Screen width (pixels)
  */
uint CDevDisplaySdl2::GetWidth () const
{
    return m_width;
}


/**
  * @brief  Get screen height
  * @return Screen height (pixels)
  */
uint CDevDisplaySdl2::GetHeight () const
{
    return m_height;
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevDisplaySdl2::Open (void)
{
    int result = DEV_ERR;
    do
    {
        if (m_pContext) break;

        m_pContext = (TContext*) malloc(sizeof(TContext));
        if (!m_pContext)
        {
            kprint("Memory allocation error");
            break;
        }

        m_pContext->width = m_width;
        m_pContext->height = m_height;
        m_pContext->window = nullptr;
        m_pContext->workerIsActive = false;
        m_pContext->devInput = m_devInput;

        m_pContext->pollThread = new CSdlWorker(m_pContext);
        if (!m_pContext->pollThread)
        {
            kprint("Polling thread creation failed");
            break;
        }

        uint32 timeStart = Kernel::GetJiffies();
        while (!m_pContext->workerIsActive && !IsTimeout(timeStart, 1000))
        {
            KThread::sleep(10);
        }
        if (!m_pContext->workerIsActive) break;

        result = DEV_OK;
    } while(0);

    if (result != DEV_OK)
    {
        Close();
    }

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device (unused)
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CDevDisplaySdl2::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevDisplaySdl2::Close (void)
{
    contextUninit();

    return DEV_OK;
}


/**
  * @brief  Draw single pixel on the screen
  * @param  x: X coordinate of pixel's position
  * @param  y: Y coordinate of pixel's position
  * @param  color: Color of the pixel
  * @return 0 or negative error code
  */
int CDevDisplaySdl2::DrawPixel (uint x, uint y, TPixel color)
{
    if (!isInitialized()) return -1;
    if (x >= GetWidth() || y >= GetHeight()) return -2;

    SDL_Surface* surface = SDL_GetWindowSurface(m_pContext->window);
    if (!surface) return -3;

    SDL_LockSurface(surface);
    {
        uint32* ptr = (uint32*) ((uint8*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
        *ptr = SDL_MapRGB(surface->format, color.r, color.g, color.b);
    }
    SDL_UnlockSurface(surface);

    updateScreen();

    return 0;
}


/**
  * @brief  Draw rectangle with solod color
  * @param  x0: X coordinate of top-left corner
  * @param  y0: Y coordinate of top-left corner
  * @param  width: Rectangle width
  * @param  height: Rectangle height
  * @param  color: Color to fill the rectangle
  * @return 0 or negative error code
  */
int CDevDisplaySdl2::DrawRect (uint x0, uint y0, uint width, uint height, TPixel color)
{
    if (!isInitialized()) return -1;

    SDL_Surface* surface = SDL_GetWindowSurface(m_pContext->window);
    if (!surface) return -2;

    SDL_Rect rect = {.x = (int)x0, .y = (int)y0, .w = (int)width, .h = (int)height};
    int res = SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, color.r, color.g, color.b));
    if (res < 0)
    {
        kprint("Rectangle rendering failed: %s", SDL_GetError());
        return -3;
    }

    updateScreen();

    return 0;
}


/**
  * @brief  Draw area with specified pixels data
  * @param  x0: X coordinate of top-left corner
  * @param  y0: Y coordinate of top-left corner
  * @param  width: Area width
  * @param  height: Area height
  * @param  data: Pointer to pixel's data (size is width * height)
  * @param  endDrawCb: Callback function to notify about the end of data processing
  * @param  cbParam: Callback function additional parameter
  * @return 0 or negative error code
  */
int CDevDisplaySdl2::Draw (uint x0, uint y0, uint width, uint height, const TPixel* data, TDrawCb* endDrawCb, void* cbParam)
{
    if (!isInitialized()) return -1;
    if (!data) return -2;

    SDL_Surface* surface = SDL_GetWindowSurface(m_pContext->window);
    if (!surface) return -3;

    SDL_LockSurface(surface);
    {

        for (uint row = 0; row < height; ++row)
        {
            uint x = x0;
            uint y = y0 + row;
            if (y >= GetHeight()) break;

            uint32* pPixelDst = (uint32*) ((uint8*)surface->pixels
                                              + y * surface->pitch
                                              + x * surface->format->BytesPerPixel);
            const TPixel* pPixelSrc = &data[row * width];

            for (uint i = 0; i < width; ++i, ++x)
            {
                if (x >= GetWidth()) break;

                *pPixelDst = SDL_MapRGB(surface->format, pPixelSrc->r, pPixelSrc->g, pPixelSrc->b);
                ++pPixelDst;
                ++pPixelSrc;
            }
        }
    }
    SDL_UnlockSurface(surface);

    updateScreen();

    if (endDrawCb)
    {
        endDrawCb(x0, y0, width, height, data, cbParam);
    }

    return 0;
}


/**
  * @brief  Checks if device was initialized (opened)
  * @return True for initialized device, otherwise - false
  */
bool CDevDisplaySdl2::isInitialized (void)
{
    return (m_pContext && m_pContext->window && m_pContext->workerIsActive);
}


/**
  * @brief  Context (m_pContext) data deinitialization
  * @return None
  */
void CDevDisplaySdl2::contextUninit (void)
{
    if (!m_pContext) return;

    if (m_pContext->pollThread)
    {
        m_pContext->pollThread->stop();

        uint32 timeStart = Kernel::GetJiffies();
        while (m_pContext->workerIsActive && IsTimeout(timeStart, 1000))
        {
            KThread::sleep(10);
        }

        delete m_pContext->pollThread;
        m_pContext->pollThread = nullptr;
    }

    free(m_pContext);
    m_pContext = nullptr;
}


/**
 * @brief  Send update signal to screen drawing system
 * @return None
 */
void CDevDisplaySdl2::updateScreen ()
{
    if (!isInitialized()) return;
    if (!m_pContext->pollThread) return;

    m_pContext->pollThread->update();
}


/**
  * @brief  Constructor
  * @param  pContext: Pointer to SDL context data
  */
CSdlWorker::CSdlWorker(CDevDisplaySdl2::TContext* pContext) :
    KThread("SdlWorker", 0, 0),
    m_pContext(pContext),
    m_updateTime(0),
    m_flagStop(false)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CSdlWorker::~CSdlWorker ()
{
    if (m_pContext && m_pContext->workerIsActive)
    {
        uninit();
    }
}


/**
  * @brief  Update screen request
  * @param  None
  */
void CSdlWorker::update (void)
{
    m_updateTime = Kernel::GetJiffies();
    if (!m_updateTime) --m_updateTime;
}


/**
  * @brief  Stop processing thread request.
  *         When thread stoped, 'workerIsActive' will be set to false
  * @param  None
  */
void CSdlWorker::stop (void)
{
    m_flagStop = true;
}


/**
  * @brief  Thread initialization
  * @param  None
  */
void CSdlWorker::init (void)
{
    if (!m_pContext) return;

    do
    {
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            kprint("Initiallization error: %s", SDL_GetError());
            break;
        }

        m_pContext->window = SDL_CreateWindow(
            "Virtual screen",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            m_pContext->width, m_pContext->height,
            SDL_WINDOW_MINIMIZED
        );
        if (!m_pContext->window)
        {
            kprint("Window creation error: %s", SDL_GetError());
            break;
        }

        SDL_Surface* screen = SDL_GetWindowSurface(m_pContext->window);
        if (!screen) break;
        SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
        SDL_UpdateWindowSurface(m_pContext->window);

        m_pContext->workerIsActive = true;
        return;
    } while (0);

    uninit();
}


/**
  * @brief  Thread main function
  * @param  None
  */
void CSdlWorker::run (void)
{
    do
    {
        if (!m_pContext || !m_pContext->workerIsActive) break;

        while (!m_flagStop)
        {
            if (m_updateTime && IsTimeout(m_updateTime, 10))
            {
                m_updateTime = 0;
                SDL_UpdateWindowSurface(m_pContext->window);
            }

            SDL_Event event;
            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                    case SDL_QUIT:
                        // Stop the application
                        exit(0);
                    break;

                    case SDL_WINDOWEVENT:
                        if (event.window.event == SDL_WINDOWEVENT_EXPOSED)
                        {
                            SDL_RestoreWindow(m_pContext->window);
                            SDL_UpdateWindowSurface(m_pContext->window);
                        }
                    break;

                    case SDL_MOUSEMOTION:
                        if (m_pContext->devInput)
                        {
                            m_pContext->devInput->onMouseMove(&event.motion);
                        }
                    break;

                    case SDL_MOUSEBUTTONDOWN:
                    case SDL_MOUSEBUTTONUP:
                        if (m_pContext->devInput)
                        {
                            m_pContext->devInput->onMouseButton(&event.button);
                        }
                    break;

                    case SDL_KEYDOWN:
                    case SDL_KEYUP:
                        if (m_pContext->devInput)
                        {
                            m_pContext->devInput->onKbdButton(&event.key);
                        }
                    break;

                    default: break;
                }
            }

            sleep(10);
        }
    } while (0);

    uninit();
}


/**
  * @brief  Thread deinitialization
  * @param  None
  */
void CSdlWorker::uninit (void)
{
    if (m_pContext && m_pContext->window)
    {
        SDL_DestroyWindow(m_pContext->window);
        m_pContext->window = nullptr;
    }

    SDL_Quit();

    if (m_pContext) m_pContext->workerIsActive = false;
}
