//*****************************************************************************
//
// @brief   Block device driver with data in RAM
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/block/iram.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>


/**
  * @brief  Constructor
  * @param  blockSize: Block size
  * @param  count: Number of blocks
  */
CBlockIram::CBlockIram (uint blockSize, uint count)
{
    m_blockSize = blockSize;
    m_blocksCount = count;
    
    uint size = m_blockSize * m_blocksCount;
    m_buff = (char*) malloc(size);
    assert(!m_buff);
    
    memset(m_buff, 0xff, size);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBlockIram::Open (void)
{
    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CBlockIram::Close (void)
{
    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CBlockIram::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}


/**
  * @brief  Read data from block device
  * @param  buff: Buffer for read data
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CBlockIram::Read (void* buff, uint block, uint count)
{
    if ( (block + count) > m_blocksCount) return DEV_ERR;
    
    uint32 addr = block * m_blockSize;
    uint size = count * m_blockSize;
    
    memcpy(buff, m_buff + addr, size);
    
    return DEV_OK;
}


/**
  * @brief  Write data to block device
  * @param  data: Pointer to data to write
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CBlockIram::Write (const void* data, uint block, uint count)
{
    if ( (block + count) > m_blocksCount) return DEV_ERR;
    
    uint32 addr = block * m_blockSize;
    uint32 size = m_blockSize * count;
    
    memcpy(m_buff + addr, data, size);
    
    return DEV_OK;
}


/**
  * @brief  Erase block on block device
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CBlockIram::Erase (uint block, uint count)
{
    if ( (block + count) > m_blocksCount) return DEV_ERR;

    uint32 addr = block * m_blockSize;
    uint32 size = m_blockSize * count;

    memset(m_buff + addr, 0xff, size);

    return DEV_OK;
}


/**
  * @brief  Get block size
  * @return Block size (bytes)
  */
uint CBlockIram::BlockSize (void)
{
    return m_blockSize;
}


/**
  * @brief  Get number of blocks on device
  * @return Numbers of blocks
  */
uint CBlockIram::BlocksCount (void)
{
    return m_blocksCount;
}

