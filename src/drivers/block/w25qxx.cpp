//*****************************************************************************
//
// @brief   Driver for SPI Flash W25Qxx
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/block/w25qxx.h"
#include "arch/spi.h"
#include "kernel/kernel.h"
#include "misc/macros.h"
#include "misc/time.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Chip descriptor
    struct _TChipInfo
    {
        uint16 id;            // Chip ID (defined by manufacturer)
        uint16 sectors;       // Numbers of sectors in the chip
        uint16 sectorSize;    // Sector size
        uint16 pageSize;      // Page size
        const char* name;     // Chip name
    };


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static const CW25qxx::TChipInfo w25qDevices[] =
    {
        { 0x4014, 256,  4096, 256, "W25Q80"  },
        { 0x4015, 512,  4096, 256, "W25Q16"  },
        { 0x4016, 1024, 4096, 256, "W25Q32"  },
        { 0x4017, 2048, 4096, 256, "W25Q64"  },
        { 0x4018, 4096, 4096, 256, "W25Q128" }
    };

    static const TSpiOptions w25qSpiOpt =
    {
        #if defined(CFG_FLASH_SPI_SPEED)
        .speed = CFG_FLASH_SPI_SPEED,
        #else
        .speed = 30000000,
        #endif
        .format = SPI_FORMAT_MSB,
        .cpol = SPI_CPOL_0,
        .cpha = SPI_CPHA_RISE
    };


/**
  * @brief  Constructor
  * @param  pDev: Communication interface (SPI)
  */
CW25qxx::CW25qxx (CDevChar* pDev) :
    m_pDev(pDev),
    m_pinCs(NULL),
    m_pDesc(NULL),
    m_writeActive(false)
{
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CW25qxx::Open (void)
{
    assert(!m_pinCs);
    
    if (m_pDesc) return DEV_OK;
    
    m_writeActive = false;
    
    int result = m_pDev->Open();
    if (result < 0) return result;
    
    result = m_pDev->Configure(&w25qSpiOpt);
    if (result < 0) return result;
    
    // Detecting chip type and parameters
    m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    m_pDev->PutChar(W25Q_CMD_JEDEC_ID);
    m_pDev->PutChar(0x00);
    uint16 id = (m_pDev->GetChar() & 0xff) << 8;
    id |= m_pDev->GetChar() & 0xff;
    m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);

    for (uint i = 0; i < ARR_COUNT(w25qDevices); i++)
    {
        if (w25qDevices[i].id != id) continue;
        
        m_pDesc = &w25qDevices[i];
        break;
    }
    
    return (m_pDesc) ? DEV_OK : DEV_ERR;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CW25qxx::Close (void)
{
    m_pDev->Close();
    
    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (see TCfg)
  * @return DEV_OK or negative error code
  */
int CW25qxx::Configure (const void* params)
{
    const TCfg* cfg = (const TCfg*) params;
    m_pinCs = cfg->pinCs;
    
    return DEV_OK;
}


/**
  * @brief  Read data from block device
  * @param  buff: Buffer for read data
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CW25qxx::Read (void* buff, uint block, uint count)
{
    if (!m_pDesc || (block + count) > m_pDesc->sectors) return DEV_ERR;
    
    uint32 addr = block * m_pDesc->sectorSize;
    uint size = count * m_pDesc->sectorSize;
    
    // Wiating end of block writing operation
    while (waitReady() || m_writeActive)
    {
        KThread::sleep(10);
    }
    
    m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    
    m_pDev->PutChar(W25Q_CMD_READ);
    m_pDev->PutChar(addr >> 16);
    m_pDev->PutChar(addr >> 8);
    m_pDev->PutChar(addr);
    int result = m_pDev->Read(buff, size);
    
    m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
    
    return (result == size) ? DEV_OK : DEV_IO_ERR;
}


/**
  * @brief  Write data to block device
  * @param  data: Pointer to data to write
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CW25qxx::Write (const void* data, uint block, uint count)
{
    if (!m_pDesc || (block + count) > m_pDesc->sectors) return DEV_ERR;
    
    waitReady();
    
    // Checking device state and writing process activation
    uint32 timeStart = Kernel::GetJiffies();
    while (1)
    {
        Kernel::EnterCritical();
        if (!m_writeActive)
        {
            m_writeActive = true;
            Kernel::ExitCritical();
            break;
        }
        Kernel::ExitCritical();
        
        if ( IsTimeout(timeStart, 5000) ) return DEV_IO_ERR;
        
        KThread::sleep(10);
    }
    
    // Blocks writing
    char* ptr = (char*) data;
    uint32 addr = block * m_pDesc->sectorSize;
    uint32 addrEnd = addr + m_pDesc->sectorSize * count;
    
    for (; addr < addrEnd; addr += m_pDesc->pageSize, ptr += m_pDesc->pageSize)
    {
        setWrite(ENABLE);
        
        m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
        m_pDev->PutChar(W25Q_CMD_PAGE_PGM);
        m_pDev->PutChar(addr >> 16);
        m_pDev->PutChar(addr >> 8);
        m_pDev->PutChar(0x00);
        m_pDev->Write(ptr, m_pDesc->pageSize);
        m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
        
        waitReady();
    }
    
    setWrite(DISABLE);
    
    Kernel::EnterCritical();
    m_writeActive = false;
    Kernel::ExitCritical();
    
    return DEV_OK;
}


/**
  * @brief  Erase block on block device
  * @param  block: Block index
  * @param  count: Number of blocks
  * @return DEV_OK or negative error code
  */
int CW25qxx::Erase (uint block, uint count)
{
    if (!m_pDesc) return DEV_ERR;

    while (count--)
    {
        if (block >= m_pDesc->sectors) return DEV_ERR;

        while (waitReady() & W25Q_STATUS_MASK_WEL)
        {
            KThread::sleep(10);
        }
        setWrite(ENABLE);
    
        uint addr = block * m_pDesc->sectorSize;
        m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
        m_pDev->PutChar(W25Q_CMD_SECTOR_E);
        m_pDev->PutChar(addr >> 16);
        m_pDev->PutChar(addr >> 8);
        m_pDev->PutChar(addr);
        m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
    
        waitReady();
        setWrite(DISABLE);

        ++block;
    }
    
    return DEV_OK;
}


/**
  * @brief  Get block size
  * @return Block size (bytes)
  */
uint CW25qxx::BlockSize (void)
{
    return (m_pDesc) ? m_pDesc->sectorSize : 0;
}


/**
  * @brief  Get block size
  * @return Block size (bytes)
  */
uint CW25qxx::BlocksCount (void)
{
    return (m_pDesc) ? m_pDesc->sectors : 0;
}


/**
  * @brief  Waiting device ready state
  * @return Chip status register value
  */
uint CW25qxx::waitReady (void)
{
    uchar status;
    uint32 timeStart = Kernel::GetJiffies();
    
    do
    {
        m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
        m_pDev->PutChar(W25Q_CMD_READ_STATUS_REG_1);
        status = m_pDev->GetChar();
        m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);

        // Check for timeout
        if ( IsTimeout(timeStart, 1000) ) break;
    } while ( (status & (W25Q_STATUS_MASK_BUSY | W25Q_STATUS_MASK_WEL) ) && (status != 0xFF) && (status != 0x00) );
    
    return status;
}


/**
  * @brief  Write access control
  * @param  state: Enable writing flag state (true - enabled)
  */
void CW25qxx::setWrite (bool state)
{
    m_pDev->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    m_pDev->PutChar( (state) ? W25Q_CMD_WRITE_ENABLE : W25Q_CMD_WRITE_DISABLE );
    m_pDev->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
}
