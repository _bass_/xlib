//*****************************************************************************
//
// @brief   Driver for LCD display M162SD53AA (Futaba)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/spi/m162sd53aa.h"
#include "arch/system.h"
#include "arch/spi.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // SPI frequency (Hz)
    #define LCD_M162SD_SPI_DEFAULT_FREQ             1000000

    #define LCD_M162SD_START_BYTE_PREFIX            0xf8
    #define LCD_M162SD_START_BYTE_RD                0x04
    #define LCD_M162SD_START_BYTE_WR                0x00
    #define LCD_M162SD_START_BYTE_INSTR             0x00
    #define LCD_M162SD_START_BYTE_DATA              0x02

    #define LCD_M162SD_CMD_CLEAR                    0x01
    #define LCD_M162SD_CMD_HOME                     0x02
    #define LCD_M162SD_CMD_DISP_CTRL                0x08
    #define LCD_M162SD_CMD_DISP_CTRL_ON             (1 << 2)
    #define LCD_M162SD_CMD_DISP_CTRL_CURSOR         (1 << 1)
    #define LCD_M162SD_CMD_DISP_CTRL_BLINK          (1 << 0)
    #define LCD_M162SD_CMD_SHIFT                    0x10
    #define LCD_M162SD_CMD_SHIFT_CURSOR             (0 << 3)
    #define LCD_M162SD_CMD_SHIFT_DISPLAY            (1 << 3)
    #define LCD_M162SD_CMD_SHIFT_DIR_DEC            (0 << 2)
    #define LCD_M162SD_CMD_SHIFT_DIR_INC            (1 << 2)
    #define LCD_M162SD_CMD_FUNC                     0x20
    #define LCD_M162SD_CMD_FUNC_BUS_8BIT            (1 << 4)
    #define LCD_M162SD_CMD_FUNC_2LINES              (1 << 3)
    #define LCD_M162SD_CMD_FUNC_LVL_100             (0 << 0)
    #define LCD_M162SD_CMD_FUNC_LVL_75              (1 << 0)
    #define LCD_M162SD_CMD_FUNC_LVL_50              (2 << 0)
    #define LCD_M162SD_CMD_FUNC_LVL_25              (3 << 0)
    #define LCD_M162SD_CMD_CGRAM_ADDR_SET           0x40
    #define LCD_M162SD_CMD_DDRAM_ADDR_SET           0x80

    #define LCD_M162SD_ADDR_ROW_1                   0x00
    #define LCD_M162SD_ADDR_ROW_2                   0x40
    #define LCD_M162SD_ROW_WIDTH                    16


/**
  * @brief  Constructor
  * @param  pSpi: SPI device
  * @param  pinCs: CS pin
  */
CLcdM162sd53aa::CLcdM162sd53aa (CDevChar* pSpi, TGpio pinCs) :
    CDevChar(),
    m_pSpi(pSpi),
    m_pinCs(pinCs),
    m_addr(LCD_M162SD_ADDR_ROW_1)
{
    assert(!pSpi || !pinCs);
}


/**
  * @brief  Device opening
  * @return DEV_OK or negative error code
  */
int CLcdM162sd53aa::Open (void)
{
    int result = m_pSpi->Open();
    if (result < 0) return -1;

    TSpiOptions optSpi = {LCD_M162SD_SPI_DEFAULT_FREQ, SPI_FORMAT_MSB, SPI_CPOL_1, SPI_CPHA_RISE};
    result = m_pSpi->Configure(&optSpi);
    if (result < 0) return -2;

    delay_ms(100);

    // Initialization and turning on
    m_addr = LCD_M162SD_ADDR_ROW_1;
    sendCmd(LCD_M162SD_CMD_DISP_CTRL);
    sendCmd(LCD_M162SD_CMD_CLEAR);
    sendCmd(LCD_M162SD_CMD_HOME);
    sendCmd(LCD_M162SD_CMD_FUNC | LCD_M162SD_CMD_FUNC_BUS_8BIT | LCD_M162SD_CMD_FUNC_2LINES | LCD_M162SD_CMD_FUNC_LVL_100);
    sendCmd(LCD_M162SD_CMD_SHIFT | LCD_M162SD_CMD_SHIFT_CURSOR | LCD_M162SD_CMD_SHIFT_DIR_INC);
    sendCmd(LCD_M162SD_CMD_DISP_CTRL | LCD_M162SD_CMD_DISP_CTRL_ON);

    return DEV_OK;
}


/**
  * @brief  Device closing
  * @return DEV_OK or negative error code
  */
int CLcdM162sd53aa::Close (void)
{
    sendCmd(LCD_M162SD_CMD_DISP_CTRL);
    m_pSpi->Close();

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters
  * @param  params: Configuration parameters (unused)
  * @return DEV_OK or negative error code
  */
int CLcdM162sd53aa::Configure (const void* params)
{
    UNUSED_VAR(params);
    return DEV_OK;
}


/**
  * @brief  Extended control
  * @param  code: Command code
  * @param  arg: Command parameter
  * @return Operation result (see IOCTL_xx)
  */
int CLcdM162sd53aa::Ioctl (uint code, void* arg)
{
    switch (code)
    {
        // Clear screen
        case IOCTL_LCD_CLEAR:
            sendCmd(LCD_M162SD_CMD_CLEAR);
            m_addr = LCD_M162SD_ADDR_ROW_1;
        break;

        // Move cursor to position (0, 0)
        case IOCTL_LCD_CURSOR_HOME:
            sendCmd(LCD_M162SD_CMD_HOME);
            m_addr = LCD_M162SD_ADDR_ROW_1;
        break;

        default: return IOCTL_NOT_SUPPORTED;
    }

    return IOCTL_OK;
}


/**
  * @brief  Send character to output stream
  * @param  c: Character
  * @return DEV_OK or negative error code
  */
int CLcdM162sd53aa::PutChar (const uchar c)
{
    if (c == '\r')
    {
        // Move to the begining of current line
        m_addr = (m_addr >= LCD_M162SD_ADDR_ROW_1 && m_addr <= LCD_M162SD_ADDR_ROW_1 + LCD_M162SD_ROW_WIDTH)
                   ? LCD_M162SD_ADDR_ROW_1
                   : LCD_M162SD_ADDR_ROW_2;
        sendCmd(m_addr | LCD_M162SD_CMD_DDRAM_ADDR_SET);
    }
    else if (c == '\n')
    {
        // Move to next line
        m_addr = (m_addr >= LCD_M162SD_ADDR_ROW_1 && m_addr <= LCD_M162SD_ADDR_ROW_1 + LCD_M162SD_ROW_WIDTH)
                   ? LCD_M162SD_ADDR_ROW_2
                   : LCD_M162SD_ADDR_ROW_1;
        sendCmd(m_addr | LCD_M162SD_CMD_DDRAM_ADDR_SET);
    }
    else
    {
        m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
        m_pSpi->PutChar(LCD_M162SD_START_BYTE_PREFIX | LCD_M162SD_START_BYTE_WR | LCD_M162SD_START_BYTE_DATA);
        delay_us(1);
        m_pSpi->PutChar(c);
        m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
        ++m_addr;
    }

    return DEV_OK;
}


/**
  * @brief  Get character from input stream
  * @return Character or negative error code
  */
int CLcdM162sd53aa::GetChar (void)
{
    // Reading isn't supported
    return -1;
}


/**
  * @brief  Send data to output stream
  * @param  data: Data
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CLcdM162sd53aa::Write (const void* buff, uint size)
{
    if (!buff || !size) return -1;

    const char* ptr = (const char*) buff;

    for (uint i = 0; i < size; ++i)
    {
        int result = PutChar(*ptr++);

        if (result < 0)
        {
            size = i;
            break;
        }
    }

    return size;
}


/**
  * @brief  Read data from input stream
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Written data size or negative error code
  */
int CLcdM162sd53aa::Read (void* buff, uint size)
{
    // Reading isn't supported
    return -1;
}


/**
  * @brief  Send command
  * @param  cmd: Command code
  */
void CLcdM162sd53aa::sendCmd (char cmd)
{
    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    m_pSpi->PutChar(LCD_M162SD_START_BYTE_PREFIX | LCD_M162SD_START_BYTE_WR | LCD_M162SD_START_BYTE_INSTR);
    delay_us(1);
    m_pSpi->PutChar(cmd);
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
}
