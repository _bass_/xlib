//*****************************************************************************
//
// @brief   Accelerometer ADXL355 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/spi/adxl355.h"
#include "arch/system.h"
#include "arch/spi.h"
#include "misc/macros.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // SPI default frequency (Hz)
    #define ADXL355_SPI_DEFAULT_FREQ        8000000

    // Read/write operation marker
    #define ADXL355_ADDR_RW_MASK            0x01
    #define ADXL355_ADDR_RD                 0x01
    #define ADXL355_ADDR_WR                 0x00

    // Register addresses
    #define ADXL355_ADDR_DEVID_AD           0x00
    #define ADXL355_ADDR_DEVID_MST          0x01
    #define ADXL355_ADDR_STATUS             0x04
    #define ADXL355_ADDR_XDATA3             0x08
    #define ADXL355_ADDR_XDATA2             0x09
    #define ADXL355_ADDR_XDATA1             0x0a
    #define ADXL355_ADDR_YDATA3             0x0b
    #define ADXL355_ADDR_YDATA2             0x0c
    #define ADXL355_ADDR_YDATA1             0x0d
    #define ADXL355_ADDR_ZDATA3             0x0e
    #define ADXL355_ADDR_ZDATA2             0x0f
    #define ADXL355_ADDR_ZDATA1             0x10
    #define ADXL355_ADDR_FILTER             0x28
    #define ADXL355_ADDR_INTMAP             0x2a
    #define ADXL355_ADDR_RANGE              0x2c
    #define ADXL355_ADDR_POWER_CTRL         0x2d
    #define ADXL355_ADDR_RESET              0x2f

    // Registers values
    #define ADXL355_RESET_CODE              0x52

    #define ADXL355_REG_RANGE_MASK          0x03

    #define ADXL355_PWR_MODE_MASK           0x01
    #define ADXL355_PWR_MODE_STANDBY        0x01
    #define ADXL355_PWR_MODE_MEAS           0x00


/**
  * @brief  Constructor
  * @param  pSpi: SPI device
  */
CAdxl355::CAdxl355 (CDevChar* pSpi, TGpio pinCs, TGpio pinDrdy) :
    CDevice(),
    m_pSpi(pSpi),
    m_pinCs(pinCs),
    m_pinDrdy(pinDrdy)
{
    assert(!pSpi || !pinCs);

    m_range = ADXL_RANGE_2;
}


/**
  * @brief  Device opening
  * @return DEV_OK or negative error code
  */
int CAdxl355::Open (void)
{
    int result = m_pSpi->Open();
    if (result < 0) return -1;

    TSpiOptions optSpi = {ADXL355_SPI_DEFAULT_FREQ, SPI_FORMAT_MSB, SPI_CPOL_0, SPI_CPHA_RISE};
    result = m_pSpi->Configure(&optSpi);
    if (result < 0) return -2;


    regWr(ADXL355_ADDR_RESET, ADXL355_RESET_CODE);
    delay_ms(10);

    char val = regRd(ADXL355_ADDR_DEVID_AD);
    if (val != 0xad) return -3;

    val = regRd(ADXL355_ADDR_DEVID_MST);
    if (val != 0x1d) return -4;

    val = regRd(ADXL355_ADDR_RANGE);
    val &= ADXL355_REG_RANGE_MASK;
    m_range = (val) ? (TAdxlRange)val : ADXL_RANGE_2;

    // Measurement activation
    regWr(ADXL355_ADDR_POWER_CTRL, ADXL355_PWR_MODE_MEAS);

    return DEV_OK;
}


/**
  * @brief  Device closing
  * @return DEV_OK or negative error code
  */
int CAdxl355::Close (void)
{
    m_pSpi->Close();

    return DEV_OK;
}


/**
  * @brief  Set device configuration parameters
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CAdxl355::Configure (const void* params)
{
    TAdxlCfg* pCfg = (TAdxlCfg*) params;
    m_range = pCfg->range;

    char regVal = regRd(ADXL355_ADDR_RANGE);
    regVal &= ~ADXL355_REG_RANGE_MASK;
    regVal |= m_range;
    regWr(ADXL355_ADDR_RANGE, regVal);

    regVal = pCfg->odr;
    regWr(ADXL355_ADDR_FILTER, regVal);

    return DEV_OK;
}


/**
  * @brief  Data reading
  * @param  axis: Axis
  * @param  pVal: Pointer to variable to save the value (g)
  * @return 0 or negative error code
  */
int CAdxl355::GetData (TAdxlAxis axis, float* pVal)
{
    if (!pVal) return -1;

    char addr;
    switch (axis)
    {
        case ADXL_AXIS_X: addr = ADXL355_ADDR_XDATA1; break;
        case ADXL_AXIS_Y: addr = ADXL355_ADDR_YDATA1; break;
        case ADXL_AXIS_Z: addr = ADXL355_ADDR_ZDATA1; break;
        default: return -2;
    }

    int32 rawVal;
    char res = regRd(addr--);
    rawVal = res >> 4;
    res = regRd(addr--);
    rawVal |= (uint32)res << 4;
    res = regRd(addr);
    rawVal |= (uint32)res << 12;

    // Conversion to signed value
    if (rawVal & (1 << 19)) rawVal -= (1 << 20);

    float resolution;
    switch (m_range)
    {
        case ADXL_RANGE_2: resolution = 2.048 / 0x7ffff; break;
        case ADXL_RANGE_4: resolution = 4.096 / 0x7ffff; break;
        case ADXL_RANGE_8: resolution = 8.192 / 0x7ffff; break;
        default: return -3;
    }

    *pVal = resolution * rawVal;

    return 0;
}


/**
  * @brief  Register reading
  * @param  addr: Register address
  * @return Register value
  */
char CAdxl355::regRd (char addr)
{
    // First byte: 7 bit address + read/write bit
    addr <<= 1;
    addr |= ADXL355_ADDR_RD;

    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    m_pSpi->PutChar(addr);
    int data = m_pSpi->GetChar();
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);

    return (data < 0) ? 0 : data;
}


/**
  * @brief  Register writing
  * @param  addr: Register address
  * @param  value: Register value
  */
void CAdxl355::regWr (char addr, char value)
{
    // First byte: 7 bit address + read/write bit
    addr <<= 1;
    addr |= ADXL355_ADDR_WR;

    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)m_pinCs);
    m_pSpi->PutChar(addr);
    m_pSpi->PutChar(value);
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)m_pinCs);
}
