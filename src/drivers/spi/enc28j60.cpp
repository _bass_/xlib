//*****************************************************************************
//
// @brief   ENC28J60 driver (Ethernet)
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/spi/enc28j60.h"
#include "arch/cpu.h"
#include "arch/system.h"
#include "arch/spi.h"
#include "kernel/kdebug.h"
#include "kernel/kernel.h"
#include "kernel/kthread.h"
#include "net/socket/socktcp.h"
#include "lwip/tcpip.h"
#include "lwip/ip_addr.h"
#include "lwip/dhcp.h"
#include "netif/etharp.h"
#include "misc/macros.h"
#include "misc/math.h"
#include "misc/time.h"
#include "threads.h"
#include "def_board.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // SPI clock frequency (Hz)
    #if defined(CFG_ETH_SPI_SPEED)
        #define ENC28J60_SPI_SPEED          CFG_ETH_SPI_SPEED
    #else
        #define ENC28J60_SPI_SPEED          20000000
    #endif

    // Maximum frame size = 1518 (according to IEEE 802.3)
    #define  ETHERNET_MAX_FRAMELEN      (6 + 6 + 2 + 1500 + 4)

    // Memory map constants
    #define  RAMSIZE                    (8 * 1024ul)    
    #define  TXSIZE                     (ETHERNET_MAX_FRAMELEN + sizeof(TEncTxStatus) + 1)
    #define  RXSTART                    (0ul)                                 // Must be 0 (errata)
    #define  TXSTART                    (RXSTART + RAMSIZE - TXSIZE)
    #define  RXSTOP                     ((TXSTART - 2ul) | 0x0001ul)	      // Must be odd (errata)
    #define  RXSIZE                     (RXSTOP - RXSTART + 1ul)

    // Total chip's internal memory size is 8 * 1024 bytes.
    // It can be splitted to create receive and transmit buffers there.
    // For example:
    //   -------------  0x0000 
    //   |            |
    //   |            |
    //   |     RX     |
    //   |            |
    //   |            | 0x19FF
    //   -------------  
    //   |            | 0x1A00
    //   |     TX     |
    //   |            |    
    //   -------------  0x1FFF

    // Chip response timeout (counter)
    #define ENC28J60_DEFAULT_TIMEOUT            50000

    // Write Control Register command
    #define  ENC28J60_WCR               ((0x2 << 5))
    // Bit Field Set command
    #define  ENC28J60_BFS               ((0x4 << 5))
    // Bit Field Clear command
    #define  ENC28J60_BFC               ((0x5 << 5))
    // Read Control Register command
    #define  ENC28J60_RCR               ((0x0 << 5))
    // Read Buffer Memory command
    #define  ENC28J60_RBM               ((0x1 << 5) | 0x1A) 
    // Write Buffer Memory command
    #define  ENC28J60_WBM               ((0x3 << 5) | 0x1A)
    // System Reset command
    #define  ENC28J60_SR                ((0x7 << 5) | 0x1F)

    // Control registers
    #define  ADDR_MASK                  0x1F
    #define  BANK_MASK	                0x60
    #define  SPRD_MASK                  0x80
    #define  EIE                        0x1B
    #define  EIR                        0x1C
    #define  ESTAT                      0x1D
    #define  ECON2                      0x1E
    #define  ECON1                      0x1F

    // Bank 0 registers
    #define  ERDPTL                     (0x00 | 0x00)
    #define  ERDPTH                     (0x01 | 0x00)
    #define  EWRPTL                     (0x02 | 0x00)
    #define  EWRPTH                     (0x03 | 0x00)
    #define  ETXSTL                     (0x04 | 0x00)
    #define  ETXSTH                     (0x05 | 0x00)
    #define  ETXNDL                     (0x06 | 0x00)
    #define  ETXNDH                     (0x07 | 0x00)
    #define  ERXSTL                     (0x08 | 0x00)
    #define  ERXSTH                     (0x09 | 0x00)
    #define  ERXNDL                     (0x0A | 0x00)
    #define  ERXNDH                     (0x0B | 0x00)
    #define  ERXRDPTL                   (0x0C | 0x00)
    #define  ERXRDPTH                   (0x0D | 0x00)
    #define  ERXWRPTL                   (0x0E | 0x00)
    #define  ERXWRPTH                   (0x0F | 0x00)
    #define  EDMASTL                    (0x10 | 0x00)
    #define  EDMASTH                    (0x11 | 0x00)
    #define  EDMANDL                    (0x12 | 0x00)
    #define  EDMANDH                    (0x13 | 0x00)
    #define  EDMADSTL                   (0x14 | 0x00)
    #define  EDMADSTH                   (0x15 | 0x00)
    #define  EDMACSL                    (0x16 | 0x00)
    #define  EDMACSH                    (0x17 | 0x00)

    // Bank 1 registers
    #define  EHT0                       (0x00 | 0x20)
    #define  EHT1                       (0x01 | 0x20)
    #define  EHT2                       (0x02 | 0x20)
    #define  EHT3                       (0x03 | 0x20)
    #define  EHT4                       (0x04 | 0x20)
    #define  EHT5                       (0x05 | 0x20)
    #define  EHT6                       (0x06 | 0x20)
    #define  EHT7                       (0x07 | 0x20)
    #define  EPMM0                      (0x08 | 0x20)
    #define  EPMM1                      (0x09 | 0x20)
    #define  EPMM2                      (0x0A | 0x20)
    #define  EPMM3                      (0x0B | 0x20)
    #define  EPMM4                      (0x0C | 0x20)
    #define  EPMM5                      (0x0D | 0x20)
    #define  EPMM6                      (0x0E | 0x20)
    #define  EPMM7                      (0x0F | 0x20)
    #define  EPMCSL                     (0x10 | 0x20)
    #define  EPMCSH                     (0x11 | 0x20)
    #define  EPMOL                      (0x14 | 0x20)
    #define  EPMOH                      (0x15 | 0x20)
    #define  EWOLIE                     (0x16 | 0x20)
    #define  EWOLIR                     (0x17 | 0x20)
    #define  ERXFCON                    (0x18 | 0x20)
    #define  EPKTCNT                    (0x19 | 0x20)

    // Bank 2 registers
    #define  MACON1                     (0x00 | 0x40 | 0x80)
    #define  MACON2                     (0x01 | 0x40 | 0x80)
    #define  MACON3                     (0x02 | 0x40 | 0x80)
    #define  MACON4                     (0x03 | 0x40 | 0x80)
    #define  MABBIPG                    (0x04 | 0x40 | 0x80)
    #define  MAIPGL                     (0x06 | 0x40 | 0x80)
    #define  MAIPGH                     (0x07 | 0x40 | 0x80)
    #define  MACLCON1                   (0x08 | 0x40 | 0x80)
    #define  MACLCON2                   (0x09 | 0x40 | 0x80)
    #define  MAMXFLL                    (0x0A | 0x40 | 0x80)
    #define  MAMXFLH                    (0x0B | 0x40 | 0x80)
    #define  MAPHSUP                    (0x0D | 0x40 | 0x80)
    #define  MICON                      (0x11 | 0x40 | 0x80)
    #define  MICMD                      (0x12 | 0x40 | 0x80)
    #define  MIREGADR                   (0x14 | 0x40 | 0x80)
    #define  MIWRL                      (0x16 | 0x40 | 0x80)
    #define  MIWRH                      (0x17 | 0x40 | 0x80)
    #define  MIRDL                      (0x18 | 0x40 | 0x80)
    #define  MIRDH                      (0x19 | 0x40 | 0x80)
    
    // Bank 3 registers
    #define  MAADR1                     (0x00 | 0x60 | 0x80)
    #define  MAADR0                     (0x01 | 0x60 | 0x80)
    #define  MAADR3                     (0x02 | 0x60 | 0x80)
    #define  MAADR2                     (0x03 | 0x60 | 0x80)
    #define  MAADR5                     (0x04 | 0x60 | 0x80)
    #define  MAADR4                     (0x05 | 0x60 | 0x80)
    #define  EBSTSD                     (0x06 | 0x60)
    #define  EBSTCON                    (0x07 | 0x60)
    #define  EBSTCSL                    (0x08 | 0x60)
    #define  EBSTCSH                    (0x09 | 0x60)
    #define  MISTAT                     (0x0A | 0x60 | 0x80)
    #define  EREVID                     (0x12 | 0x60)
    #define  ECOCON                     (0x15 | 0x60)
    #define  EFLOCON                    (0x17 | 0x60)
    #define  EPAUSL                     (0x18 | 0x60)
    #define  EPAUSH                     (0x19 | 0x60)

    // PHY registers
    #define  PHCON1                     0x00
    #define  PHSTAT1                    0x01
    #define  PHHID1                     0x02
    #define  PHHID2                     0x03
    #define  PHCON2                     0x10
    #define  PHSTAT2                    0x11
    #define  PHIE                       0x12
    #define  PHIR                       0x13
    #define  PHLCON                     0x14

    // Register bits
    #define  EIE_INTIE                  (1 << 7)
    #define  EIE_PKTIE                  (1 << 6)
    #define  EIE_DMAIE                  (1 << 5)
    #define  EIE_LINKIE                 (1 << 4)
    #define  EIE_TXIE                   (1 << 3)
    #define  EIE_WOLIE                  (1 << 2)
    #define  EIE_TXERIE                 (1 << 1)
    #define  EIE_RXERIE                 (1 << 0)

    #define  EIR_PKTIF                  (1 << 6)
    #define  EIR_DMAIF                  (1 << 5)
    #define  EIR_LINKIF                 (1 << 4)
    #define  EIR_TXIF                   (1 << 3)
    #define  EIR_WOLIF                  (1 << 2)
    #define  EIR_TXERIF                 (1 << 1)
    #define  EIR_RXERIF                 (1 << 0)
    
    #define  ESTAT_INT                  (1 << 7)
    #define  ESTAT_LATECOL              (1 << 4)
    #define  ESTAT_RXBUSY               (1 << 2)
    #define  ESTAT_TXABRT               (1 << 1)
    #define  ESTAT_CLKRDY               (1 << 0)
                                                    
    #define  ECON2_AUTOINC              (1 << 7)
    #define  ECON2_PKTDEC               (1 << 6)
    #define  ECON2_PWRSV                (1 << 5)
    #define  ECON2_VRPS                 (1 << 3)

    #define  ECON1_TXRST                (1 << 7)
    #define  ECON1_RXRST                (1 << 6)
    #define  ECON1_DMAST                (1 << 5)
    #define  ECON1_CSUMEN               (1 << 4)
    #define  ECON1_TXRTS                (1 << 3)
    #define  ECON1_RXEN                 (1 << 2)
    #define  ECON1_BSEL1                (1 << 1)
    #define  ECON1_BSEL0                (1 << 0)

    #define  MACON1_LOOPBK              (1 << 4)
    #define  MACON1_TXPAUS              (1 << 3)
    #define  MACON1_RXPAUS              (1 << 2)
    #define  MACON1_PASSALL             (1 << 1)
    #define  MACON1_MARXEN              (1 << 0)
    
    #define  MACON2_MARST               (1 << 7)
    #define  MACON2_RNDRST              (1 << 6)
    #define  MACON2_MARXRST             (1 << 3)
    #define  MACON2_RFUNRST             (1 << 2)
    #define  MACON2_MATXRST             (1 << 1)
    #define  MACON2_TFUNRST             (1 << 0)
    
    #define  MACON3_PADCFG2             (1 << 7)
    #define  MACON3_PADCFG1             (1 << 6)
    #define  MACON3_PADCFG0             (1 << 5)
    #define  MACON3_TXCRCEN             (1 << 4)
    #define  MACON3_PHDRLEN             (1 << 3)
    #define  MACON3_HFRMLEN             (1 << 2)
    #define  MACON3_FRMLNEN             (1 << 1)
    #define  MACON3_FULDPX              (1 << 0)

    #define  MACON4_DEFER               (1 << 6)
    #define  MACON4_BPEN                (1 << 5)
    #define  MACON4_NOBKOFF             (1 << 4)

    #define  MICMD_MIISCAN              (1 << 1)
    #define  MICMD_MIIRD                (1 << 0)
    
    #define  MISTAT_NVALID              (1 << 2)
    #define  MISTAT_SCAN                (1 << 1)
    #define  MISTAT_BUSY                (1 << 0)

    #define  PHCON1_PRST                0x8000
    #define  PHCON1_PLOOPBK             0x4000
    #define  PHCON1_PPWRSV              0x0800
    #define  PHCON1_PDPXMD              0x0100

    #define  PHCON2_FRCLINK             0x4000
    #define  PHCON2_TXDIS               0x2000
    #define  PHCON2_JABBER              0x0400
    #define  PHCON2_HDLDIS              0x0100

    #define  PHSTAT1_PFDPX              0x1000
    #define  PHSTAT1_PHDPX              0x0800
    #define  PHSTAT1_LLSTAT             0x0004
    #define  PHSTAT1_JBSTAT             0x0002

    #define  PHSTAT2_LSTAT              0x0400
   
    #define  PKTCTRL_PHUGEEN            (1 << 3)
    #define  PKTCTRL_PPADEN             (1 << 2)
    #define  PKTCTRL_PCRCEN             (1 << 1)
    #define  PKTCTRL_POVERRIDE          (1 << 0)


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Reading data worker
    class CEnc28j60Reader : public KThread
    {
        public:
            CEnc28j60Reader (CDevEnc28j60* drv);
            
        private:
            CDevEnc28j60*  m_drv;
            
            void run (void);
    };

    #pragma pack(1)
    typedef union 
    {
        uchar v[7];
        struct {
            uint16  ByteCount;
            uchar   CollisionCount          : 4;
            uchar   CRCError                : 1;
            uchar   LengthCheckError        : 1;
            uchar   LengthOutOfRange        : 1;
            uchar   Done                    : 1;
            uchar   Multicast               : 1;
            uchar   Broadcast               : 1;
            uchar   PacketDefer             : 1;
            uchar   ExcessiveDefer          : 1;
            uchar   MaximumCollisions       : 1;
            uchar   LateCollision           : 1;
            uchar   Giant                   : 1;
            uchar   Underrun                : 1;
            uint16  BytesTransmittedOnWire;
            uchar   ControlFrame            : 1;
            uchar   PAUSEControlFrame       : 1;
            uchar   BackpressureApplied     : 1;
            uchar   VLANTaggedFrame         : 1;
            uchar   Zeros                   : 4;
        } bits;
    } TEncTxStatus;
    
    typedef union 
    {
        uchar v[4];
        struct {
            uint16  ByteCount;
            uchar   PreviouslyIgnored       : 1;
            uchar   RXDCPreviouslySeen      : 1;
            uchar   CarrierPreviouslySeen   : 1;
            uchar   CodeViolation           : 1;
            uchar   CRCError                : 1;
            uchar   LengthCheckError        : 1;
            uchar   LengthOutOfRange        : 1;
            uchar   ReceiveOk               : 1;
            uchar   Multicast               : 1;
            uchar   Broadcast               : 1;
            uchar   DribbleNibble           : 1;
            uchar   ControlFrame            : 1;
            uchar   PauseControlFrame       : 1;
            uchar   UnsupportedOpcode       : 1;
            uchar   VLANType                : 1;
            uchar   Zero                    : 1;
        } bits;
    } TEncRxStatus;
    
    typedef union 
    {
        uchar Val;
    
        struct {
            uchar RXERIE        : 1;
            uchar TXERIE        : 1;
            uchar               : 1;
            uchar TXIE          : 1;
            uchar LINKIE        : 1;
            uchar DMAIE         : 1;
            uchar PKTIE         : 1;
            uchar INTIE         : 1;
        } EIEbits;
    
        struct {
            uchar RXERIF        : 1;
            uchar TXERIF        : 1;
            uchar               : 1;
            uchar TXIF          : 1;
            uchar LINKIF        : 1;
            uchar DMAIF         : 1;
            uchar PKTIF         : 1;
            uchar               : 1;
        } EIRbits;
    
        struct {
            uchar CLKRDY        : 1;
            uchar TXABRT        : 1;
            uchar RXBUSY        : 1;
            uchar               : 1;
            uchar LATECOL       : 1;
            uchar               : 1;
            uchar BUFER         : 1;
            uchar INT           : 1;
        } ESTATbits;
    
        struct {
            uchar               : 3;
            uchar VRPS          : 1;
            uchar               : 1;
            uchar PWRSV         : 1;
            uchar PKTDEC        : 1;
            uchar AUTOINC       : 1;
        } ECON2bits;
            
        struct {
            uchar BSEL0         : 1;
            uchar BSEL1         : 1;
            uchar RXEN          : 1;
            uchar TXRTS         : 1;
            uchar CSUMEN        : 1;
            uchar DMAST         : 1;
            uchar RXRST         : 1;
            uchar TXRST         : 1;
        } ECON1bits;
            
        struct {
            uchar BCEN          : 1;
            uchar MCEN          : 1;
            uchar HTEN          : 1;
            uchar MPEN          : 1;
            uchar PMEN          : 1;
            uchar CRCEN         : 1;
            uchar ANDOR         : 1;
            uchar UCEN          : 1;
        } ERXFCONbits;
            
        struct {
            uchar MARXEN        : 1;
            uchar PASSALL       : 1;
            uchar RXPAUS        : 1;
            uchar TXPAUS        : 1;
            uchar               : 4;
        } MACON1bits;
            
        struct {
            uchar FULDPX        : 1;
            uchar FRMLNEN       : 1;
            uchar HFRMEN        : 1;
            uchar PHDREN        : 1;
            uchar TXCRCEN       : 1;
            uchar PADCFG0       : 1;
            uchar PADCFG1       : 1;
            uchar PADCFG2       : 1;
        } MACON3bits;
        
        struct {
            uchar FULDPX        : 1;
            uchar FRMLNEN       : 1;
            uchar HFRMEN        : 1;
            uchar PHDREN        : 1;
            uchar TXCRCEN       : 1;
            uchar PADCFG        : 3;
        } MACON3bits2;
            
        struct {
            uchar               : 4;
            uchar NOBKOFF       : 1;
            uchar BPEN          : 1;
            uchar DEFER         : 1;
            uchar               : 1;
        } MACON4bits;
            
        struct {
            uchar MIIRD         : 1;
            uchar MIISCAN       : 1;
            uchar               : 6;
        } MICMDbits;
    
        struct {
            uchar BISTST        : 1;
            uchar TME           : 1;
            uchar TMSEL0        : 1;
            uchar TMSEL1        : 1;
            uchar PSEL          : 1;
            uchar PSV0          : 1;
            uchar PSV1          : 1;
            uchar PSV2          : 1;
        } EBSTCONbits;
        
        struct {
            uchar BISTST        : 1;
            uchar TME           : 1;
            uchar TMSEL         : 2;
            uchar PSEL          : 1;
            uchar PSV           : 3;
        } EBSTCONbits2;
            
        struct {
            uchar BUSY          : 1;
            uchar SCAN          : 1;
            uchar NVALID        : 1;
            uchar               : 5;
        } MISTATbits;
            
        struct {
            uchar COCON0        : 1;
            uchar COCON1        : 1;
            uchar COCON2        : 1;
            uchar               : 5;
        } ECOCONbits;
        
        struct {
            uchar COCON         : 3;
            uchar               : 5;
        } ECOCONbits2;
            
        struct {
            uchar FCEN0         : 1;
            uchar FCEN1         : 1;
            uchar FULDPXS       : 1;
            uchar               : 5;
        } EFLOCONbits;
        
        struct {
            uchar FCEN          : 2;
            uchar FULDPXS       : 1;
            uchar               : 5;
        } EFLOCONbits2;
    } TEncReg;
    
    typedef union 
    {
        uint16 Val;
    
        struct {
            uint16              : 8;
            uint16 PDPXMD       : 1;
            uint16              : 2;
            uint16 PPWRSV       : 1;
            uint16              : 2;
            uint16 PLOOPBK      : 1;
            uint16 PRST         : 1;
        } PHCON1bits;
    
        struct {
            uint16              : 1;
            uint16 JBSTAT       : 1;
            uint16 LLSTAT       : 1;
            uint16              : 5;
            uint16              : 3;
            uint16 PHDPX        : 1;
            uint16 PFDPX        : 1;
            uint16              : 3;
        } PHSTAT1bits;
    
        struct {
            uint16 PREV0        : 1;
            uint16 PREV1        : 1;
            uint16 PREV2        : 1;
            uint16 PREV3        : 1;
            uint16 PPN0         : 1;
            uint16 PPN1         : 1;
            uint16 PPN2         : 1;
            uint16 PPN3         : 1;
            uint16 PPN4         : 1;
            uint16 PPN5         : 1;
            uint16 PID19        : 1;
            uint16 PID20        : 1;
            uint16 PID21        : 1;
            uint16 PID22        : 1;
            uint16 PID23        : 1;
            uint16 PID24        : 1;
        } PHID2bits;
        
        struct {
            uint16 PREV         : 4;
            uint16 PPNL         : 4;
            uint16 PPNH         : 2;
            uint16 PID          : 6;
        } PHID2bits2;
    
        struct {
            uint16              : 8;
            uint16 HDLDIS       : 1;
            uint16              : 1;
            uint16 JABBER       : 1;
            uint16              : 2;
            uint16 TXDIS        : 1;
            uint16 FRCLNK       : 1;
            uint16              : 1;
        } PHCON2bits;
    
        struct {
            uint16              : 5;
            uint16 PLRITY       : 1;
            uint16              : 2;
            uint16              : 1;
            uint16 DPXSTAT      : 1;
            uint16 LSTAT        : 1;
            uint16 COLSTAT      : 1;
            uint16 RXSTAT       : 1;
            uint16 TXSTAT       : 1;
            uint16              : 2;
        } PHSTAT2bits;
    
        struct {
            uint16              : 1;
            uint16  PGEIE       : 1;
            uint16              : 2;
            uint16  PLNKIE      : 1;
            uint16              : 3;
            uint16              : 8;
        } PHIEbits;
    
        struct {
            uint16              : 2;
            uint16  PGIF        : 1;
            uint16              : 1;
            uint16  PLNKIF      : 1;
            uint16              : 3;
            uint16              : 8;
        } PHIRbits;
    
        struct {
            uint16              : 1;
            uint16  STRCH       : 1;
            uint16  LFRQ0       : 1;
            uint16  LFRQ1       : 1;
            uint16  LBCFG0      : 1;
            uint16  LBCFG1      : 1;
            uint16  LBCFG2      : 1;
            uint16  LBCFG3      : 1;
            uint16  LACFG0      : 1;
            uint16  LACFG1      : 1;
            uint16  LACFG2      : 1;
            uint16  LACFG3      : 1;
            uint16              : 4;
        } PHLCONbits;
        
        struct {
            uint16              : 1;
            uint16  STRCH       : 1;
            uint16  LFRQ        : 2;
            uint16  LBCFG       : 4;
            uint16  LACFG       : 4;
            uint16              : 4;
        } PHLCONbits2;
    } TEncPHYReg;
    
    typedef struct  
    {
        uint16           NextPacketPtr;
        TEncRxStatus     Status;
    } TEncPreamble;
    #pragma pack()


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static err_t netifInit (struct netif* netif);
    static err_t netifSendPacket (struct netif* netif, struct pbuf* p);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------
    
    REGISTER_DEBUG("eth", "ENC28j60", NULL, NULL);


/**
  * @brief  Callback function of network interface (lwip) initialization
  * @param  netif: Pointer to network interface object (lwip)
  * @return Operation result
  */
err_t netifInit (struct netif *netif)
{
    LWIP_ASSERT("netif != NULL", (netif != NULL));
    LWIP_ASSERT("netif->state != NULL", (netif->state != NULL));

#if LWIP_NETIF_HOSTNAME
	// Initialize interface hostname
	netif->hostname 	= "lwip";
#endif
	netif->name[0] 		= 'e';
	netif->name[1] 		= 't';
	netif->output 		= etharp_output;    // LwIP function
	netif->linkoutput 	= netifSendPacket;

	return ERR_OK;
}


/**
  * @brief  Callback function of packet sending
  * @param  netif: Pointer to network interface object (lwip)
  * @param  p: Network packet
  * @return Operation result
  */
err_t netifSendPacket (struct netif *netif, struct pbuf *p)
{
    LWIP_ASSERT("netif != NULL", (netif != NULL));
    LWIP_ASSERT("netif->state != NULL", (netif->state != NULL));
    
    CDevEnc28j60* pDev = (CDevEnc28j60*) netif->state;
    TSkb skb = 
    {
        .data = (void*)p
    };
    int res = pDev->Write(&skb);
    
	return (res < 0) ? ERR_IF : ERR_OK;
}


/**
  * @brief Constructor
  * @param pDev: IO interface (SPI)
  */
CDevEnc28j60::CDevEnc28j60 (CDevChar* pDev) :
    CDevNet(NET_FAMILY_INET),
    m_pSpi(pDev),
    m_devState(DEV_CLOSED),
    m_reader(this),
    m_mutex()
{
    // Parameter 'state' is used for driver internal data - pointer to current class object
    ip_addr_t dummyAddr = {.addr = 0};
    struct netif* pNetIf = netif_add(&m_netif, &dummyAddr, &dummyAddr, &dummyAddr, this, netifInit, tcpip_input); 
    assert(pNetIf == NULL);
    
    m_revision = 0;
    m_bank = 0;
    m_addrRx = 0;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (see TEnc28j60Cfg)
  * @return DEV_OK or negative error code
  */
int CDevEnc28j60::Configure (const void* params)
{
    m_mutex.Lock();
    {
        const TEnc28j60Cfg* cfg = (const TEnc28j60Cfg*) params;
        
        ip_addr_t addr;
        ip_addr_t mask;
        ip_addr_t gw;
        
        m_netif.hwaddr_len = ETHARP_HWADDR_LEN;
        memcpy(m_netif.hwaddr, cfg->mac, ETHARP_HWADDR_LEN);
        
        addr.addr = cfg->addr;
        mask.addr = cfg->mask;
        gw.addr = cfg->gw;
        netif_set_addr(&m_netif, &addr, &mask, &gw);
        
        TAddrIp* pIp = (TAddrIp*)&m_addr;
        pIp->addr = cfg->addr;
    }
    m_mutex.Unlock();

    return DEV_OK;
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevEnc28j60::Open (void)
{
    if (m_devState == DEV_OPENED) return DEV_OK;
    
    int result;
    
    do
    {
        m_mutex.Lock();
        
        // Additional check after mutex locking
        if (m_devState == DEV_OPENED)
        {
            m_mutex.Unlock();
            return DEV_OK;
        }
        
        m_devState = DEV_OPENING;
        
        result = m_pSpi->Open();
        if (result != DEV_OK) break;
        
        TSpiOptions spiCfg = 
        {
            .speed = ENC28J60_SPI_SPEED,
            .format = SPI_FORMAT_MSB,
            .cpol = SPI_CPOL_0,
            .cpha = SPI_CPHA_0
        };
        result = m_pSpi->Configure(&spiCfg);
        if (result != DEV_OK) break;
        
        result = initHw();
        if (result != DEV_OK) break;
        
        m_netif.mtu = 1500 + IP_HLEN + 20; // 20 == TCP_HLEN
        m_netif.flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP;
        
        // Release the mutex before netif_set_up() because during setup packets might be sent
        m_mutex.Unlock();
        
        netif_set_up(&m_netif);
    } while (0);
    
    if (result != DEV_OK)
    {
        m_devState = DEV_CLOSED;
        m_mutex.Unlock();
        return result;
    }
    
    // DHCP
    if (!m_netif.ip_addr.addr)
    {
    #if LWIP_DHCP
        if (dhcp_start(&m_netif) != ERR_OK)
        {
            kdebug("DHCP start ERROR");
            m_devState = DEV_CLOSED;
            return DEV_ERR;
        }
        
        // Waiting for getting network address
        uint32 timeStart = Kernel::GetJiffies();
        while ( !IsTimeout(timeStart, CFG_ETH_DHCP_TIMEOUT * 1000) && !m_netif.ip_addr.addr )
        {
            KThread::sleep(100);
            // Prevent waiting of address if network cable isn't connected
            // (DHCP service will keep working in background)
            if ( IsTimeout(timeStart, 1 * 1000) && !isLinked() ) break;
        }
        
        if (m_netif.ip_addr.addr)
        {
            kdebug("DHCP OK");
        }
        else
        {
            kdebug("DHCP ERROR");
            m_devState = DEV_CLOSED;
            return DEV_ERR;
        }
    #else
        kdebug("address ERROR");
        assert(1);
    #endif
    }
    
    m_devState = DEV_OPENED;

    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CDevEnc28j60::Close (void)
{
    m_mutex.Lock();
    {
        m_devState = DEV_CLOSING;
        
        #if LWIP_DHCP
        dhcp_stop(&m_netif);
        #endif
        
        netif_set_down(&m_netif);
        
        // Switch the chip in to power-down mode
        // Turning off packets receiving, waiting for current sent and receive processes termination
        setBank(ECON1);
        writeOpcode(ENC28J60_BFS, ECON1, 0);
        
        uint timeout = 5;
        while ( (readReg(ESTAT) & ESTAT_RXBUSY) == 0 && timeout-- )
        {
            KThread::sleep(10);
        }
        
        timeout = 5;
        while ( (readReg(ECON1) & ECON1_TXRTS) && timeout-- )
        {
            KThread::sleep(10);
        }
        
        // Energy saving mode activation
        writeOpcode(ENC28J60_BFS, ECON2, ECON2_VRPS);
        // Turn on power-down mode
        writeOpcode(ENC28J60_BFS, ECON2, ECON2_PWRSV);
        
        m_devState = DEV_CLOSED;
    }
    m_mutex.Unlock();
    
    return DEV_OK;
}


/**
  * @brief  Advanced device control
  * @param  code: Command code
  * @param  arg: Pointer to command arguments
  * @return Operation result (see IOCTL_xx)
  */
int CDevEnc28j60::Ioctl (uint code, void* arg)
{
    switch (code)
    {
        case IOCTL_NET_IS_LINKED:
            if ( !arg || !netif_is_up(&m_netif) ) return IOCTL_ERROR;
            *((bool*)arg) = isLinked();
        break;
        
        default: return CDevNet::Ioctl(code, arg);
    }
    
    return IOCTL_OK;
}


/**
  * @brief  Send network packet
  * @param  skb: Network paket descriptor
  * @return Processed data size or negative error code
  */
int CDevEnc28j60::Write (const TSkb* skb)
{
    m_mutex.Lock();
    
    // skb->data is a pointer to 'pbuff'
    struct pbuf* p = (struct pbuf*) skb->data;
    
    #if ETH_PAD_SIZE
    // reclaim the padding word
    pbuf_header(p, ETH_PAD_SIZE);
    #endif
    
    // Waiting for ready to send state
    uint timeout = ENC28J60_DEFAULT_TIMEOUT;
    while ( (readReg(ECON1) & ECON1_TXRTS) && timeout-- );
    
    // Writing data into the buffer (control byte is skipped)
    // Setting pointers to start and end of transfering data (from TXSTART + 1 to TXSTART + size)
    uint16 addrStart = TXSTART + 1;
    uint16 addrEnd = TXSTART;
    
    for (; p != NULL; p = p->next)
    {
        writeReg(EWRPTL, LOW (addrStart));
        writeReg(EWRPTH, HIGH(addrStart));
            
        m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)ETH_PIN_CS);
        m_pSpi->PutChar(ENC28J60_WBM);
        m_pSpi->Write(p->payload, p->len);
        m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)ETH_PIN_CS);
        
        addrStart += p->len;
        addrEnd += p->len;
        
        if (addrEnd > TXSTART + TXSIZE)
        {
            m_mutex.Unlock();
            return NET_ERR;
        }
    }
    
    writeReg(ETXNDL, LOW (addrEnd));
    writeReg(ETXNDH, HIGH(addrEnd));
    
    int res = transmit();

    m_mutex.Unlock();
    
    return (res < 0) ? res : DEV_OK;
}


/**
  * @brief  Set address of the network device
  * @param  pAddr: Network address descriptor
  * @return NET_OK or negative error code
  */
int CDevEnc28j60::SetAddr (const TNetAddr* pAddr)
{
    if (pAddr->family != m_addr.family) return NET_ERR;
    
    m_addr = *pAddr;
    
    TAddrIp* pIp = (TAddrIp*)&m_addr;
    ip_addr_t ipAddr = { .addr = pIp->addr };
    netif_set_ipaddr(&m_netif, &ipAddr);
    
    return NET_OK;
}


/**
  * @brief  Read address of the network device
  * @param  pAddr: Pointer to network address descriptor (to save address)
  */
void CDevEnc28j60::GetAddr (TNetAddr* pAddr) const 
{
    #if LWIP_DHCP
    TAddrIp* pIp = (TAddrIp*)&m_addr;
    pIp->addr = m_netif.ip_addr.addr;
    #endif
    
    *pAddr = m_addr;
}


/**
  * @brief  Reset network packet descriptor
  * @param  skb: Network packet descriptor
  */
void CDevEnc28j60::resetSkb (TSkb* skb)
{
    // FIXME: driver uses lwip directly
//    skb->data = m_dataBuff;
//    skb->buffSize = sizeof(m_dataBuff);
    skb->data = NULL;
    skb->buffSize = 0;
    skb->dataSize = 0;
    skb->pDev = this;
    skb->socket = NULL;
}


/**
  * @brief  Chip hardware initialization
  * @return DEV_OK or negative error code
  */
int CDevEnc28j60::initHw (void)
{
    // Quit from power-down mode
    writeOpcode(ENC28J60_BFC, ECON2, ECON2_VRPS);
    writeOpcode(ENC28J60_BFC, ECON2, ECON2_PWRSV);
    
    // Software reset
    writeOpcode(ENC28J60_SR, 0, ENC28J60_SR);
    // Waiting for reset completion to start internal generator
    delay_ms(2); 
    
    uint timeout = ENC28J60_DEFAULT_TIMEOUT;
    while ( (readReg(ESTAT) & ESTAT_CLKRDY) == 0 && timeout-- );
    
    // Receive and transmit buffers initialization
    m_addrRx = RXSTART;
    writeReg(ERXSTL,   LOW(RXSTART));  
    writeReg(ERXSTH,   HIGH(RXSTART));
    writeReg(ERXRDPTL, LOW(RXSTOP));
    writeReg(ERXRDPTH, HIGH(RXSTOP));
    writeReg(ERXNDL,   LOW(RXSTOP));
    writeReg(ERXNDH,   HIGH(RXSTOP));
    writeReg(ETXSTL,   LOW(TXSTART));
    writeReg(ETXSTH,   HIGH(TXSTART));

    // Control byte for sending packets
    writeReg(EWRPTL,   LOW(TXSTART));
    writeReg(EWRPTH,   HIGH(TXSTART));
    writeByte(0x00);

    // Allow MAC-packet receiving
    writeReg(MACON1, MACON1_MARXEN | MACON1_TXPAUS | MACON1_RXPAUS);
    
    // MACON3_FULDPX option removed to use half-duplex mode
    // Auto-padding is turned on, CRC enabled, half-duplex
    writeReg(MACON3, MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN);
    // According to datasheet (set inter-frame gap (back-to-back))
    writeReg(MABBIPG, 0x15);
    // 
    writeReg(MACON4, MACON4_DEFER);
    writeReg(MACLCON2, 63);
    
    // According to Datasheet (set inter-frame gap (non-back-to-back))
    writeReg(MAIPGL, 0x12);
    writeReg(MAIPGH, 0x0C);
    
    // Set maximum packet size
    writeReg(MAMXFLL, LOW(ETHERNET_MAX_FRAMELEN));	
    writeReg(MAMXFLH, HIGH(ETHERNET_MAX_FRAMELEN));
  
    // Set MAC-address
    writeReg(MAADR5, m_netif.hwaddr[0]);
    writeReg(MAADR4, m_netif.hwaddr[1]);
    writeReg(MAADR3, m_netif.hwaddr[2]);
    writeReg(MAADR2, m_netif.hwaddr[3]);
    writeReg(MAADR1, m_netif.hwaddr[4]);
    writeReg(MAADR0, m_netif.hwaddr[5]);
    
    // Turn off CLKOUT to reduce EMI
    writeReg(ECOCON, 0x00);

    // Reading chip revision
    m_revision = readReg(EREVID);
    if (m_revision == 0xff || m_revision == 0x00) return DEV_ERR;
    
    // Disable half-duplex loopback in PHY
    writePhy(PHCON2, PHCON2_HDLDIS);    
    // LEDA and LEDB configuration
    // writePhy(PHLCON, 0x3472); // From Microchip

    // Full-fuplex usage
    // writePhy(PHCON1, PHCON1_PDPXMD);    
    
    // Switch to bank 0
    setBank(ECON1);
    // IRQ activation
    writeOpcode(ENC28J60_BFS, EIE, EIE_INTIE | EIE_PKTIE);
    // Allow packets reception
    writeOpcode(ENC28J60_BFS, ECON1, ECON1_RXEN);
    
    return DEV_OK;
}


/**
  * @brief  Write data in to register
  * @param  addr: Register address
  * @param  value: Register value
  */
void CDevEnc28j60::writeReg (char addr, char value)
{
    setBank(addr);
    writeOpcode(ENC28J60_WCR, addr, value);
}


/**
  * @brief  Read data from register
  * @param  addr: Register address
  * @return Register value
  */
char CDevEnc28j60::readReg (char addr)
{
    setBank(addr);
    
    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)ETH_PIN_CS);
    
    m_pSpi->PutChar( ENC28J60_RCR | (addr & ADDR_MASK) );
    char data = m_pSpi->GetChar();
    if (addr & 0x80) data = m_pSpi->GetChar();
    
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)ETH_PIN_CS);
    
    return data;
}


/**
  * @brief  Write data in to PHY register
  * @param  addr: Register address
  * @param  value: Register value
  */
void CDevEnc28j60::writePhy (char addr, uint16 value)
{
    writeReg(MIREGADR, addr);
    writeReg(MIWRL, value);	
    writeReg(MIWRH, value >> 8);
    
    char reg;
    do
    {
        reg = readReg(MISTAT);
    } while ((reg & MISTAT_BUSY) && (reg != 0xFF));
}


/**
  * @brief  Read data from PHY register
  * @param  addr: Register address
  * @return Register value
  */
uint16 CDevEnc28j60::readPhy (char addr)
{
    writeReg(MIREGADR, addr);
    writeReg(MICMD, MICMD_MIIRD);
    delay_ms(10);
    
    char regVal;
    do
    {
        regVal = readReg(MISTAT);
    } while ( (regVal & MISTAT_BUSY) && (regVal != 0xFF) );
    
    writeReg(MICMD, 0x00);
    
    uint16 value = readReg(MIRDL);
    value |= readReg(MIRDH) << 8;

    return value;
}


/**
  * @brief  Send command
  * @param  op: Operation code
  * @param  addr: Register address
  * @param  value: Register value
  */
void CDevEnc28j60::writeOpcode (char op, char addr, char value)
{
    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)ETH_PIN_CS);
    
    m_pSpi->PutChar( op | (addr & ADDR_MASK) );
    m_pSpi->PutChar(value);
    
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)ETH_PIN_CS);
}


/**
  * @brief  Write data byte
  * @param  value: Byte value
  */
void CDevEnc28j60::writeByte (char value)
{
    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)ETH_PIN_CS);
    
    m_pSpi->PutChar(ENC28J60_WBM);
    m_pSpi->PutChar(value);
    
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)ETH_PIN_CS);
}


/**
  * @brief  Set current memory bank
  * @param  bank: Bank number
  */
void CDevEnc28j60::setBank (char bank)
{
    bank = bank & BANK_MASK;
    
    if (bank != m_bank)
    {
        writeOpcode(ENC28J60_BFC, ECON1, (ECON1_BSEL1 | ECON1_BSEL0));
        writeOpcode(ENC28J60_BFS, ECON1, bank >> 5);
        m_bank = bank;
    }
}


/**
  * @brief  Read data
  * @param  buff: Buffer to save read data
  * @param  size: Buffer size (how many data to read)
  */
void CDevEnc28j60::readBuffer (char* buff, uint size)
{
    m_pSpi->Ioctl(IOCTL_SPI_CS_ACTIVATE, (void*)ETH_PIN_CS);
    
    m_pSpi->PutChar(ENC28J60_RBM);
    m_pSpi->Read(buff, size);
    
    m_pSpi->Ioctl(IOCTL_SPI_CS_RELEASE, (void*)ETH_PIN_CS);
}


/**
  * @brief  Transmission starting
  * @return 0 or negative error code
  */
int CDevEnc28j60::transmit (void)
{
    int result = 0;
    // Reset transmission state if there was an TX Error previously (see silicon errata)
    writeOpcode(ENC28J60_BFS, ECON1, ECON1_TXRST);
    writeOpcode(ENC28J60_BFC, ECON1, ECON1_TXRST);
    writeOpcode(ENC28J60_BFC, EIR,   EIR_TXERIF | EIR_TXIF);
    // Transmitter enable
    writeOpcode(ENC28J60_BFS, ECON1, ECON1_TXRTS);
    
    // Waiting for trasmission completion
    uint timeout = ENC28J60_DEFAULT_TIMEOUT;
    char reg;   
    
    do
    {
        reg = readReg(EIR);
    } while ( !(reg & (EIR_TXERIF | EIR_TXIF)) && --timeout );

    // Fixes for revisions B5 and B7 (errata)
    if (m_revision == 0x05 || m_revision == 0x06)
    {
        // Transmit errors processing
        if ( (reg & EIR_TXERIF) || !timeout )
        {
            // Reset transmission
            writeOpcode(ENC28J60_BFC, ECON1, ECON1_TXRTS);
            
            // Save current read address
            uint16 addrRead = readReg(ERDPTL);
            addrRead |= readReg(ERDPTH) << 8;
            
            // Read current transmit buffer address
            uint16 addrTx = readReg(ETXNDL);
            addrTx |= readReg(ETXNDH) << 8;
            addrTx++;
            
            // Re-enable transmission in case of collision occured
            for (uint i = 0; i < 16; i++)
            {
                // Reading transmision state
                TEncTxStatus statusTx;
                writeReg( ERDPTL, LOW(addrTx) );
                writeReg( ERDPTH, HIGH(addrTx) );
                readBuffer((char*)&statusTx, sizeof(statusTx));
                
                if ( !(reg & EIR_TXERIF) && !statusTx.bits.LateCollision ) break;
                
                // Reset transmission
                writeOpcode(ENC28J60_BFS, ECON1, ECON1_TXRST);
                writeOpcode(ENC28J60_BFC, ECON1, ECON1_TXRST);
                writeOpcode(ENC28J60_BFC, EIR,   EIR_TXERIF | EIR_TXIF);
                
                // Re-enable transmitter
                writeOpcode(ENC28J60_BFS, ECON1, ECON1_TXRTS);
                
                timeout = ENC28J60_DEFAULT_TIMEOUT;
                do reg = readReg(EIR);
                while ( (reg & (EIR_TXERIF | EIR_TXIF)) == 0 && timeout-- );
                
                // Disable transmitter
                writeOpcode(ENC28J60_BFC, ECON1, ECON1_TXRST);
            } // for (uint i = 0; i < 16; i++)
            
            // Current pointer restoration
            writeReg(ERDPTL, LOW(addrRead));
            writeReg(ERDPTH, HIGH(addrRead));
        }
    } // if (m_revision == 0x05 || m_revision == 0x06)
    
    return result;
}


/**
  * @brief  Reset reception buffer
  * @return None
  */
void CDevEnc28j60::resetRxBuff (void)
{
    writeOpcode(ENC28J60_BFC, ECON1, ECON1_RXEN);
    writeOpcode(ENC28J60_BFC, EIR, EIR_RXERIF);
    writeReg(ERXSTL,   LOW(RXSTART));
    writeReg(ERXSTH,   HIGH(RXSTART));
    writeReg(ERXRDPTL, LOW(RXSTOP));
    writeReg(ERXRDPTH, HIGH(RXSTOP));
    writeReg(ERXNDL,   LOW(RXSTOP));
    writeReg(ERXNDH,   HIGH(RXSTOP));
    
    // Reset incoming packets counter
    char packetCount = readReg(EPKTCNT);
    while (packetCount--)
    {
        writeOpcode(ENC28J60_BFS, ECON2, ECON2_PKTDEC);
    }
    
    writeOpcode(ENC28J60_BFS, ECON1, ECON1_RXEN);
    m_addrRx = RXSTART;
}


/**
  * @brief  Get network cable connection state
  * @return true - cable connected, otherwise - false
  */
bool CDevEnc28j60::isLinked (void)
{
    return (readPhy(PHSTAT2) & PHSTAT2_LSTAT);
}


/**
  * @brief  Reading worker constructor
  * @param  drv: Pointer to driver object
  */
CEnc28j60Reader::CEnc28j60Reader (CDevEnc28j60* drv) :
KThread("Eth rd", 384, THREAD_PRIORITY_ETHRD),
    m_drv(drv)
{
}


/**
  * @brief  Reading thread main function
  * @param  None
  */
void CEnc28j60Reader::run (void)
{
    TEncPreamble encHeader;
    char packetCount = 0;
    
    for (;;)
    {
        m_drv->m_mutex.Lock();
        
        do
        {
            // Enable processing only after driver initialization
            if ( !netif_is_up(&m_drv->m_netif) ) break;
            
            // Reception errors processing
            char reg = m_drv->readReg(EIR);
            if (reg & EIR_RXERIF)
            {
                m_drv->resetRxBuff();
                packetCount = 0;
                break;
            }
        
            packetCount = m_drv->readReg(EPKTCNT);
            if (!packetCount) break;
            
            // Get packet metadata
            m_drv->writeReg(ERDPTL, LOW(m_drv->m_addrRx));
            m_drv->writeReg(ERDPTH, HIGH(m_drv->m_addrRx));
            m_drv->readBuffer((char*)&encHeader, sizeof(TEncPreamble));
            
            // Received data verification
            //   - NextPointer must be less than RXSTOP
            //   - NextPointer must be even
            //   - Packet data size must be less than ETHERNET_MAX_FRAMELEN
            //   - Checksum verification
            //   - Zero bit must be reset
            //   - ReciveOK bit must be set
            
            uint addr = encHeader.NextPacketPtr;
            uint count = encHeader.Status.bits.ByteCount;
            
            if (
                encHeader.NextPacketPtr > RXSTOP ||
                (addr & 0x01) ||
                count > ETHERNET_MAX_FRAMELEN ||
                encHeader.Status.bits.Zero ||
                encHeader.Status.bits.ReceiveOk == 0 ||
                encHeader.Status.bits.CRCError
            ) {
                // Reception error: reset settings, stop packet reception
                m_drv->resetRxBuff();
                packetCount = 0;
                break;
            }
            
            // Packet reading. Network buffer allocation.
            struct pbuf* netBuff = pbuf_alloc(PBUF_RAW, count, PBUF_POOL);
            if (!netBuff)
            {
                packetCount = 0;
                break;
            }
            
            // Fill network buffer by data from frame
            for (struct pbuf* ptr = netBuff; count && ptr; ptr = ptr->next)
            {
                uint size = __MIN(ptr->len, count);
                m_drv->readBuffer( (char*)ptr->payload, size );
                count -= size;
            }
            
            // Save packet address
            m_drv->m_addrRx = addr;
            
            // Decrease pointer value before writing into ERXRDPT register (errata). 
            // RX buffer wrapping must be taken into account if the NextPacketLocation is precisely RXSTART.
            if (--addr > RXSTOP) addr = RXSTOP;
            
            // Update receiving buffer pointer. Writing order is important (low -> high)) 
            m_drv->writeReg(ERXRDPTL, LOW (addr));
            m_drv->writeReg(ERXRDPTH, HIGH(addr));
            
            // Decrease receiving packets counter
            m_drv->writeOpcode(ENC28J60_BFS, ECON2, ECON2_PKTDEC);
            
            // Pass the packet to the handler
            if ( ERR_OK != m_drv->m_netif.input(netBuff, &m_drv->m_netif) )
            {
                pbuf_free(netBuff);
            }
        } while (0);
            
        m_drv->m_mutex.Unlock();

        if (packetCount > 1) continue;
        
        sleep(50);
    }
}
