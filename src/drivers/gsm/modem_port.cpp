//*****************************************************************************
//
// @brief   GSM modem port driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/modem_port.h"
#include "drivers/gsm/modem.h"
#include "kernel/kernel.h"
#include "kernel/kthread.h"
#include "kernel/kdebug.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <ctype.h>
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Port activation control (data transfering)
    #define PORT_RTS_ON()                   do { \
                                                m_pModem->Ioctl(IOCTL_MODEM_PORT_ACTIVATE, this); \
                                            } while (0)
    #define PORT_RTS_OFF()                  do { \
                                                m_pModem->Ioctl(IOCTL_MODEM_PORT_DEACTIVATE, this); \
                                            } while (0)


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Attach debug output to modem debug output
    ATTACH_DEBUG(CMODEM_DEBUG_NAME);


/**
  * @brief  Constructor
  * @param  pDev: IO interface (USART)
  * @param  pCfg: Port configuration parameters
  */
CModemPort::CModemPort (CDevChar* pDev, const TMPortCfg* pCfg) :
    m_pCfg(pCfg),
    m_pDev(pDev),
    m_qAt( pCfg->qLen, sizeof(char*) ),
    m_mutex()
{
    m_buff = (char*) malloc(m_pCfg->buffSize);
    assert(!m_buff);
    
    m_pModem = (CModem*) GetDevice(DEV_NAME_GSM, m_pCfg->modemDevNum);
    assert(!m_pModem);
    
    m_mode = E_MODEM_PORT_MODE_CMD;
    m_pAt = m_buff;
    m_countRx = 0;
    m_serviceId = MPORT_SERVICE_ID_NONE;
    m_lockId = MPORT_SERVICE_ID_NONE;
    m_error = CMODEM_CME_ERR_NONE;
}


/**
  * @brief  Open port
  * @param  None
  * @return None
  */
void CModemPort::Open (void)
{
    flushAT();
    
    m_pDev->Open();
    m_pDev->Configure(m_pCfg->devConfig);
    
    // Wakeup reading thread (modem)
    m_pModem->Resume();
}


/**
  * @brief  Close port
  * @param  None
  * @return None
  */
void CModemPort::Close (void)
{
    m_pDev->Close();
    m_lockId = MPORT_SERVICE_ID_NONE;
    m_mutex.Unlock();
}


/**
  * @brief  Read port state
  * @param  None
  * @return Current port state
  */
TDevState CModemPort::GetState (void)
{
    return m_pDev->GetState();
}


/**
  * @brief  Read port state
  * @param  None
  * @return Current port state
  */
void CModemPort::Receive (void)
{
    while (1)
    {
        int byte = m_pDev->GetChar();
        
        if (byte < 0)
        {
            if ( m_countRx && IsTimeout(m_parserTime, MPORT_PARSER_TIMEOUT) )
            {
                m_pAt[m_countRx] = '\0';
                processAT();
            }
            break;
        }
        
        // Updating timestamp of last received character
        m_parserTime = Kernel::GetJiffies();
        
        // Overflow check
        char* pBuffEnd = m_buff + m_pCfg->buffSize;
        if ( (m_pAt + m_countRx) >= pBuffEnd || (!m_countRx && (m_pAt + CMODEM_AT_MIN_SIZE) >= pBuffEnd) )
        {
            // Move data to start of the buffer
            if (m_countRx < m_pCfg->buffSize)
            {
                // Waiting previous AT-commands processing
                // (to prevent unprocessed data damage on moving)
                uint32 startTime = Kernel::GetJiffies();
                while ( m_qAt.Count() && !IsTimeout(startTime, MPORT_AT_ADD_TIMEOUT) )
                {
                    KThread::sleep(5);
                }
                
                memcpy(m_buff, m_pAt, m_countRx);
            }
            else
            {
                m_countRx = 0;
            }
            
            m_pAt = m_buff;
        }
        
        // Data receiving
        if (m_mode == E_MODEM_PORT_MODE_DATA)
        {
            char buff[32];
            buff[0] = byte;
            uint len = 1;
            
            len += m_pDev->Read(&buff[1], sizeof(buff) - len);
            
            // Pass data to active modem service
            CMService* service = m_pModem->GetService( (TMServiceId)m_serviceId );
            
            if (service)
            {
                service->onDataReceive(buff, len);
            }
                
            continue;
        }
        
        // AT-command receiving (string)
        // Only printable symbols is used + \r\n
        if ( !isprint(byte) && byte != '\r' && byte != '\n' ) continue;
        
        if (byte != '\r' && byte != '\n')
        {
            m_pAt[m_countRx++] = byte;
            continue;
        }
        
        m_pAt[m_countRx] = '\0';
        
        processAT();
    } // while (1)
}


/**
  * @brief  AT-command sending
  * @param  str: AT-command
  * @param  waitStr: Expected successfull response
  * @param  timeout: Answer receive timeout (ms)
  * @return 0 or negative error code
  */
int CModemPort::SendAT (const char* str, const char* waitStr, uint timeout)
{
    return SendAT(str, waitStr, NULL, 0, timeout);
}


/**
  * @brief  AT-command sending
  * @param  str: AT-command
  * @param  waitStr: Expected successfull response
  * @param  buff: Buffer for saving incoming data (string) up to waitStr receiving
  * @param  size: Buffer size
  * @param  timeout: Answer receive timeout (ms)
  * @return 0 or negative error code
  */
int CModemPort::SendAT (const char* str, const char* waitStr, char* buff, uint size, uint timeout)
{
    int result = 0;
    
    ToCmd();
    
    if (str)
    {
        m_error = CMODEM_CME_ERR_NONE;
        flushAT();
        
        PORT_RTS_ON();
        result = -2;
        
        do
        {
            int res = doWrite( (void*)str, strlen(str) );
            if (res < 0) break;
            doWrite( (void*)"\r\n", sizeof("\r\n") - 1 );
            if (res < 0) break;
            result = 0;
        } while (0);
        
        PORT_RTS_OFF();
        kdebug("> %s", str);
    }
    
    if (waitStr)
    {
        result = -1;
        // Timeout control
        uint32 timeStart = Kernel::GetJiffies();
        
        if (buff && size) *buff = '\0';
        
        while (1)
        {
            const char* pAt = ReadAT(timeout + 1);
            
            if ( IsTimeout(timeStart, timeout) )
            {
                result = -3;
                break;
            }

            // Command execution error
            if (!pAt) break;
            
            // Response was received. Timeout extending.
            timeStart = Kernel::GetJiffies();
            
            if ( !strcmp(pAt, waitStr) )
            {
                result = 0;
                break;
            }
            
            // Execution is done
            if ( !strcmp(pAt, "OK") || !strcmp(pAt, "ERROR") ) break;
            
            if (buff && size && *pAt)
            {
                uint len = strlen(pAt);
                if (len > size - 1) len = size - 1;
                memcpy(buff, pAt, len);
                buff[len] = '\0';
                buff += len + 1;
                size -= len + 1;
                if (size) *buff = '\0';
            }
        } // while (1)
    } // if (waitStr)
    
    return result;
}


/**
  * @brief  AT-command reading
  * @param  timeout: Reading timeout (ms)
  * @return Pointer to AT-command string or NULL
  */
const char* CModemPort::ReadAT (uint timeout)
{
    const char* pAt;
    int result = m_qAt.Get(&pAt, timeout);
    
    return (result) ? NULL : pAt;
}


/**
  * @brief  Writing data
  * @param  data: Pointer to the data
  * @param  size: Data size
  * @return Transfered data size or negative error code
  */
int CModemPort::Write (const void* data, uint size)
{
    PORT_RTS_ON();
    int result = doWrite(data, size);
    PORT_RTS_OFF();
    
    return (result < 0) ? result : size;
}


/**
  * @brief  Lock port
  * @param  id: Locker ID (see MPORT_SERVICE_ID_xx)
  */
void CModemPort::Lock (uint id)
{
    if (id == m_lockId) return;
    m_mutex.Lock();
    m_lockId = id;
}


/**
  * @brief  Unlock port
  * @param  id: Unlocker ID (see MPORT_SERVICE_ID_xx)
  */
void CModemPort::Unlock (uint id)
{
    if (id != m_lockId) return;
    m_lockId = MPORT_SERVICE_ID_NONE;
    m_mutex.Unlock();
}


/**
  * @brief  Switch port mode to "command"
  * @return true - on success otherwise - false
  */
bool CModemPort::ToCmd (void)
{
    if (m_mode == E_MODEM_PORT_MODE_CMD) return true;
    
    uint tryCount = 3;
    PORT_RTS_ON();

    while (tryCount--)
    {
        kdebug("> +++");
        
        KThread::sleep(1000);
        doWrite( (void*)"+++", sizeof("+++") - 1 );
        KThread::sleep(200);
        m_mode = E_MODEM_PORT_MODE_CMD;
        
        const char* str = ReadAT();
        if (!str || !(*str)) continue;
        
        if ( !strcmp(str, "OK") )
        {
            PORT_RTS_OFF();
            return true;
        }
    }
    
    PORT_RTS_OFF();
    
    return false;
}


/**
  * @brief  Switch port mode to "data"
  * @return true - on success otherwise - false
  */
bool CModemPort::ToData (void)
{
    if (m_mode == E_MODEM_PORT_MODE_DATA) return true;

    int result = SendAT("ATO", NULL);
    if (result < 0) return false;
    
    uint32 timeStart = Kernel::GetJiffies();
    while ( !IsTimeout(timeStart, CMODEM_AT_TIMEOUT) )
    {
        const char* str = ReadAT();
        if (!str || !(*str)) continue;
        
        // State changing is done in processUrc()
        if ( strstr(str, "CONNECT") || !strcmp(str, "NO CARRIER") )
        {
            break;
        }
    }
    
    return (m_mode == E_MODEM_PORT_MODE_DATA);
}


/**
  * @brief  Get current error code
  * @param  None
  * @return Error code (see CMODEM_CME_ERR_xx)
  */
uint CModemPort::GetError (void)
{
    uint error = m_error;
    m_error = CMODEM_CME_ERR_NONE;
    return error;
}


/**
  * @brief  Adding AT-command to queue
  * @param  str: AT-command string
  */
void CModemPort::addAT (const char* str)
{
    // Wating for data processing if queue id full
    uint32 startTime = Kernel::GetJiffies();
    while ( m_qAt.IsFull() && !IsTimeout(startTime, MPORT_AT_ADD_TIMEOUT) )
    {
        KThread::sleep(10);
    }
    
    if ( m_qAt.IsFull() )
    {
        kwarn("AT queue OVERFLOW");
        return;
    }
    
    m_qAt.Add(&str);
}


/**
  * @brief  AT-commands queue reset
  * @param  None
  */
void CModemPort::flushAT (void)
{
    while ( m_qAt.Count() )
    {
        char* ptr;
        m_qAt.Get(&ptr);
    }
}


/**
  * @brief  Send data through IO interface (with timeout control)
  * @param  data: Data to send
  * @param  size: Data size
  * @return 0 or negative error code
  */
int CModemPort::doWrite (const void* data, uint size)
{
    uint tryCount = 0;
    
    while (size)
    {
        int len = m_pDev->Write(data, size);
        if (len < 0)                        return -1;
        else if (!len && ++tryCount > 5)    return -2;
        else                                tryCount = 0;
        
        size -= len;
        
        if (size)
        {
            data = (char*)data + len;
            KThread::sleep(5);
        }
    }
    
    return 0;
}


/**
  * @brief  Incoming AT-command processing (m_pAt)
  * @param  None
  * @return None
  */
void CModemPort::processAT (void)
{
    // Minimal length checking
    if (m_countRx < CMODEM_AT_MIN_SIZE)
    {
        m_countRx = 0;
        return;
    }
    
    if (m_countRx > 64) kdebug("< ...");
    else                kdebug("< %s", m_pAt);
    
    // Asynchronous URC processing
    int result = processUrc(m_pAt);
    if (result)
    {
        m_countRx = 0;
        
        if (result < 0)
        {
            addAT(NULL);
        }
        return;
    }
    
    char* str = m_pAt;
    m_pAt += m_countRx + 1;
    m_countRx = 0;
    
    // Adding received string into AT-command queue
    addAT(str);
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CModemPort::processUrc (const char* str)
{
    if ( !str || !strcmp(str, "OK") ) return CMODEM_URC_RES_NONE;
    
    if (*str != '+')
    {
        int result = CMODEM_URC_RES_NONE;
        
        // Port mode changing tracking
        if ( strstr(str, "CONNECT") )
        {
            m_mode = E_MODEM_PORT_MODE_DATA;
            result = CMODEM_URC_RES_OK;
        }
        
        if ( !strcmp(str, "NO CARIER") )
        {
            m_mode = E_MODEM_PORT_MODE_CMD;
            result = CMODEM_URC_RES_OK;
        }
        
        int res = m_pModem->processUrc(str);
        
        if (res == CMODEM_URC_RES_NONE)
        {
            CMService* service = m_pModem->GetService( (TMServiceId)m_serviceId );
            
            if (service)
            {
                res = service->processUrc(str);
            }
        }
        
        return (result == CMODEM_URC_RES_NONE) ? res : result;
    }
    
    // All strings below strat with '+'
    
    // Error codes
    if ( strstr(str, "+CME ERROR: ") || strstr(str, "+CMS ERROR: ") )
    {
        str += sizeof("+CME ERROR: ") - 1;
        m_error = atoi(str);
        return CMODEM_URC_RES_ERR;
    }
    
    return m_pModem->processUrc(str);
}
