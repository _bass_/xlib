//*****************************************************************************
//
// @brief   GSM modem base driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/modem.h"
#include "drivers/gsm/service/service_voice.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "kernel/kstdio.h"
#include "kernel/events.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include "threads.h"
#include <string.h>
#include <ctype.h>
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Services section borders
    #define CMODEM_SERVICELIST_ADDR_START       CC_SECTION_ADDR_START(xLib_gsmModemServices)
    #define CMODEM_SERVICELIST_ADDR_END         CC_SECTION_ADDR_END(xLib_gsmModemServices)


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("modem", CMODEM_DEBUG_NAME, NULL, CModem::debugCmdHandler);


/**
  * @brief  Constructor
  * @param  portList: Modem ports list 
  * @param  listSize: Number of ports
  */
CModem::CModem (CModemPort* const* portList, uint listSize) :
    m_portList(portList),
    m_portCount(listSize),
    KThread("Modem", CFG_GSM_STACK_SIZE, THREAD_PRIORITY_MODEMRD)
{
    assert(listSize == 0);
    
    m_PIN = NULL;
    m_regCode = CMODEM_REG_UNKNOWN;
    
    memset( m_serviceList, 0, sizeof(m_serviceList) );
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CModem::Open (void)
{
    if (!m_portList) return DEV_IO_ERR;
    
    int result = DEV_OK;
    m_regCode = CMODEM_REG_UNKNOWN;

    for (uint i = 0; i < m_portCount; i++)
    {
        CModemPort* port = m_portList[i];
        if (!port) continue;
        port->Lock(MPORT_SERVICE_ID_INTERNAL);
        
        do
        {
            port->Open();
            
            int res = port->SendAT("ATE0V1");
            if (res < 0)
            {
                result = res;
                break;
            }
            
            res = port->SendAT("AT+CMEE=1;+CREG=1;");
            if (res < 0)
            {
                result = res;
                break;
            }
            
            if (i == 0)
            {
                res = enterPIN(m_PIN);
                if (res < 0)
                {
                    result = res;
                    continue;
                }
                
                MODEM_NOTIFY_STATE(this, MODEM_STATE_WAIT_REG);
                
                res = waitReg();
                if (res < 0)
                {
                    result = res;
                    continue;
                }
            } // if (i == 0)
            
            if (!result)
            {
                res = port->SendAT("AT+CRC=1;+COLP=1;+CUSD=1;");
                if (res < 0)
                {
                    result = res;
                    break;
                }
            }
        } while(0);
        
        port->Unlock(MPORT_SERVICE_ID_INTERNAL);
        if (result) return result;
    }
    
    return result;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CModem::Close (void)
{
    // Closing all services
    for (uint i = 0; i < ARR_COUNT(m_serviceList); ++i)
    {
        CMService* service = m_serviceList[i];
        if (!service) continue;
        service->Close();
        RemoveService( (TMServiceId)i );
    }
    
    // Closing ports
    for (uint i = 0; m_portList && i < m_portCount; i++)
    {
        CModemPort* port = m_portList[i];
        if (port) port->Close();
    }
    
    m_regCode = CMODEM_REG_UNKNOWN;

    return DEV_OK;
}


/**
  * @brief  Set configuration parameters of the device
  * @param  params: Configuration parameters (see TModemCfg)
  * @return DEV_OK or negative error code
  */
int CModem::Configure (const void* params)
{
    const TModemCfg* cfg = (const TModemCfg*) params;
    
    m_PIN = cfg->PIN;
    
    return DEV_OK;
}


/**
  * @brief  Advanced device control
  * @param  code: Command code (see IOCTL_MODEM_xxx)
  * @param  arg: Pointer to command arguments
  * @return Operation result (see IOCTL_xx)
  */
int CModem::Ioctl (uint code, void* arg)
{
    int result = IOCTL_OK;
    
    switch (code)
    {
        case IOCTL_MODEM_GET_IMEI:  result = ioctlGetImei( (char*)arg ); break;
        case IOCTL_MODEM_GET_IMSI:  result = ioctlGetImsi( (char*)arg ); break;
        case IOCTL_MODEM_GET_CSQ:   result = ioctlGetCsq( (int*)arg ); break;
        case IOCTL_MODEM_GET_REG:   *((char*)arg) = m_regCode; break;
        case IOCTL_MODEM_GET_SIM_STATE: result = ioctlGetSimState( (char*)arg ); break;
        case IOCTL_MODEM_RESTART:   result = ioctlDoRestart(); break;
        default: return IOCTL_NOT_SUPPORTED;
    }
    
    return result;
}


/**
  * @brief  Get service object
  * @param  id: Service ID
  * @return Pointer to service object or NULL on error
  */
CMService* CModem::GetService (TMServiceId id)
{
    if (id >= E_MSERVICE_COUNT) return NULL;
    if (m_serviceList[id]) return m_serviceList[id];
    
    // Object creation
    TMServiceDesc* pDesc = (TMServiceDesc*) CMODEM_SERVICELIST_ADDR_START;
    for (; (void*)pDesc < CMODEM_SERVICELIST_ADDR_END; ++pDesc)
    {
        if (pDesc->type != id || !pDesc->creator) continue;
        m_serviceList[pDesc->type] = pDesc->creator(pDesc->type, m_portList, m_portCount);
    }

    return m_serviceList[id];
}


/**
  * @brief  Service removing
  * @param  id: Service ID
  */
void CModem::RemoveService (TMServiceId id)
{
    if (id >= E_MSERVICE_COUNT || !m_serviceList[id]) return;
    
    Kernel::EnterCritical();
    CMService* service = m_serviceList[id];
    m_serviceList[id] = NULL;
    Kernel::ExitCritical();
    delete service;
}


/**
  * @brief  Entering PIN-code
  * @param  code: PIN value
  * @return DEV_OK or negative error code
  */
int CModem::enterPIN (const char* code)
{
    const uint maxTry = 3;
    uint tryCount = 0;
    CModemPort* port = m_portList[0];
    if (!m_portList || !port) return DEV_IO_ERR;
    
    while (tryCount < maxTry)
    {
        // First iteration without delay
        if (tryCount) KThread::sleep(1000);
        ++tryCount;
        
        // Check if PIn is needed
        char buff[24];
        port->GetError();
        int result = port->SendAT( "AT+CPIN?", "OK", buff, sizeof(buff) );
        if (result < 0) continue;
        
        uint error = port->GetError();
        if (error == CMODEM_CME_ERR_SIM_BUSY || error == CMODEM_CME_ERR_SIM_NOT_INSERT) continue;
        
        char* str = buff;
        if ( !strstr(str, "+CPIN") ) continue;
        str += sizeof("+CPIN:") - 1;
        if (*str == ' ') ++str;
        
        if ( !strcmp(str, "READY") )
        {
            return DEV_OK;
        }
        else if ( !strcmp(str, "SIM PIN") )
        {
            if (!code || !*code) return -10;
            // PIN-code entering
            uint len = sprintf(buff, sizeof(buff), "AT+CPIN=%u", code);
            buff[len] = '\0';
            result = port->SendAT(buff);
            return (result < 0) ? result : DEV_OK;
        }
        else
        {
            continue;
        }
    } // while (tryCount--)
    
    return DEV_ERR;
}


/**
  * @brief  Waiting network registration
  * @return DEV_OK or negative error code
  */
int CModem::waitReg (void)
{
    CModemPort* port = m_portList[0];
    if (!m_portList || !port) return DEV_IO_ERR;
    
    uint32 timeStart = Kernel::GetJiffies();
    m_regCode = CMODEM_REG_SEARCH;
    
    uint tryCount = 5;
    while (tryCount--)
    {
        int result = port->SendAT("AT+CREG?");
        if (!result) break;
    }
    
    while ( !IsTimeout(timeStart, CMODEM_REG_TIMEOUT * 1000) )
    {
        if (m_regCode == CMODEM_REG_ERROR)
        {
            // Waiting start of registration process
            sleep(1000);
            continue;
        }
        
        if (m_regCode == CMODEM_REG_HOME || m_regCode == CMODEM_REG_ROAMING) return DEV_OK;
        
        sleep(CMODEM_IO_DELAY);
    }

    // Registration timeout
    return DEV_ERR;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: Received URC string
  * @return Processing result (see CMODEM_URC_RES_xxx)
  */
int CModem::processUrc (const char* str)
{
    if (!str) return CMODEM_URC_RES_NONE; 
    
    // Network registration
    if ( strstr(str, "+CREG: ") )
    {
        // Generic response: +CREG: <n>,<stat> or +CREG: <n>, <stat>
        // URC: +CREG: <stat>
        // URC: +CREG: <stat>,"<lac>","<ci>"
        str += sizeof("+CREG: ") - 1;
        
        if (
            str[1] == ',' &&
            (
                isdigit(str[2]) ||
                (str[2] == ' ' && isdigit(str[3]))
            )
        ) {
            // Skip symbols "<n>,"
            str += 2;
            // Skip space if it's present
            if ( !isdigit(*str) ) str++; 
        }
        
        m_regCode = *str - '0';
        return CMODEM_URC_RES_OK;
    }
    
    // Passing URC to services
    for (uint i = 0; i < ARR_COUNT(m_serviceList); ++i)
    {
        if (!m_serviceList[i]) continue;
        
        int result = m_serviceList[i]->processUrc(str);
        if (result != CMODEM_URC_RES_NONE) return result;
    }
    
    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  Ports processing thread
  * @param  None
  */
void CModem::run (void)
{
    for (;;)
    {
        bool allPortsClosed = true;
        
        for (uint i = 0; m_portList && i < m_portCount; i++)
        {
            CModemPort* port = m_portList[i];
            if (!port) continue;
            
            port->Receive();
            if (port->GetState() != DEV_CLOSED) allPortsClosed = false;
        }
        
        if (allPortsClosed)
        {
            // Stop thread until first port will be opened
            Suspend();
        }
        else
        {
            sleep(CMODEM_IO_DELAY);
        }
    }
}


/**
  * @brief  IOCTL_MODEM_GET_IMEI command handler
  * @param  buff: Buffer to save result (CMODEM_IMEI_LENGTH)
  * @return Operation result (see IOCTL_xx)
  */
int CModem::ioctlGetImei (char* buff)
{
    if (GetState() != DEV_OPENED || !m_portList) return IOCTL_ERROR;
    
    CModemPort* port = (m_portCount > 1) ? m_portList[1] : m_portList[0];
    if (!port || port->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    port->Lock(MPORT_SERVICE_ID_IOCTL);
    char imeiStr[CMODEM_IMEI_LENGTH + 1];
    int result = port->SendAT("AT+CGSN", "OK", imeiStr, sizeof(imeiStr));
    port->Unlock(MPORT_SERVICE_ID_IOCTL);
    
    if (result)
    {
        return IOCTL_ERROR;
    }
    else
    {
        memcpy(buff, imeiStr, CMODEM_IMEI_LENGTH);
        return IOCTL_OK;
    }
}


/**
  * @brief  IOCTL_MODEM_GET_IMSI command handler
  * @param  buff: Buffer to save result (CMODEM_IMSI_MAX_LENGTH + 1)
  * @return Operation result (see IOCTL_xx)
  */
int CModem::ioctlGetImsi (char* buff)
{
    if (GetState() != DEV_OPENED || !m_portList) return IOCTL_ERROR;
    
    CModemPort* port = (m_portCount > 1) ? m_portList[1] : m_portList[0];
    if (!port || port->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    port->Lock(MPORT_SERVICE_ID_IOCTL);
    port->GetError();
    int result = port->SendAT("AT+CIMI", "OK", buff, CMODEM_IMSI_MAX_LENGTH + 1);
    uint portError = port->GetError();
    port->Unlock(MPORT_SERVICE_ID_IOCTL);
    
    return (result || portError != CMODEM_CME_ERR_NONE) ? IOCTL_ERROR : IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_GET_CSQ command handler
  * @param  pValue: Pointer to variable to save result
  * @return Operation result (see IOCTL_xx)
  */
int CModem::ioctlGetCsq (int* pValue)
{
    if (!pValue) return IOCTL_ERROR;
    if (GetState() != DEV_OPENED || !m_portList) return IOCTL_ERROR;
    
    CModemPort* port = (m_portCount > 1) ? m_portList[1] : m_portList[0];
    if (!port || port->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    port->Lock(MPORT_SERVICE_ID_IOCTL);
    char buff[12];
    int result = port->SendAT( "AT+CSQ", "OK", buff, sizeof(buff) );
    port->Unlock(MPORT_SERVICE_ID_IOCTL);
    
    if (result < 0) return IOCTL_ERROR;
    
    // Response format "+CSQ x,y"
    char* str = buff;
    if ( !strstr(str, "+CSQ") ) return IOCTL_ERROR;
    
    str += sizeof("+CSQ");
    *pValue = atoi(str);
    
    return IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_GET_SIM_STATE command handler
  * @param  pError: Pointer to variable to save result (0 - OK, or error code)
  * @return Operation result (see IOCTL_xx)
  */
int CModem::ioctlGetSimState (char* pError)
{
    if (!pError) return IOCTL_ERROR;
    if (!m_portList || !m_portList[0] || m_portList[0]->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    *pError = (m_regCode == CMODEM_REG_SEARCH || m_regCode == CMODEM_REG_HOME || m_regCode == CMODEM_REG_ROAMING)
            ? 0
            : CMODEM_CME_ERR_SIM_NOT_INSERT;
    
    return IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_RESTART command handler
  * @return Operation result (see IOCTL_xx)
  */
int CModem::ioctlDoRestart (void)
{
    if (!m_portList) return IOCTL_ERROR;
    
    CModemPort* port = (m_portCount > 1) ? m_portList[1] : m_portList[0];
    if (!port || port->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    // Software reset
    port->Lock(MPORT_SERVICE_ID_IOCTL);
    int result = port->SendAT("AT+CFUN=1,1");
    port->Unlock(MPORT_SERVICE_ID_IOCTL);
    
    return (result < 0) ? IOCTL_ERROR : IOCTL_OK;
}


/**
  * @brief  kdebug commands handler
  * @param  arg: Command arguments
  * @param  argCount: Number of arguments
  */
void CModem::debugCmdHandler (char** arg, int argCount)
{
    if (argCount < 1) return;
    // Format: modem [devNum]>[>]AT
    char* str = arg[0];
    if (!str) return;
    
    // Get device number
    uint devNum = 0;
    if ( isdigit(*str) )
    {
        devNum = *str - '0';
        ++str;
    }
    
    CModem* pDev = (CModem*) GetDevice(DEV_NAME_GSM, devNum);
    if (!pDev) return;
    
    // Port number calculation
    uint portNum = 0;
    while (*str == '>')
    {
        ++str;
        ++portNum;
    }
    
    if (!portNum || portNum > pDev->m_portCount || !*str) return;
    
    CModemPort* port = pDev->m_portList[portNum - 1];
    if (!pDev->m_portList || !port) return; 
    
    port->SendAT(str);
}
