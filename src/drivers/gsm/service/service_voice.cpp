//*****************************************************************************
//
// @brief   GSM modem voice calls service
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/service/service_voice.h"
#include "drivers/gsm/modem.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "kernel/kstdio.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/math.h"
#include "misc/time.h"
#include "def_board.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if defined(CFG_VOICE_WORKER_STACK_SIZE)
        #define VOICE_WORKER_STACK_SIZE             CFG_VOICE_WORKER_STACK_SIZE
    #else
        #define VOICE_WORKER_STACK_SIZE             512
    #endif

    // Dialing timeout (sec)
    #define VOICE_DIALING_TIMEOUT                   10


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Internal worker
    class CVoiceWorker : public KThread
    {
        public:
            CVoiceWorker (CMServiceVoice* obj);
        
        private:
            CMServiceVoice* m_obj;
        
            void run (void);
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("voice", "VOICE", NULL, NULL);
    REGISTER_GSM_SERVICE(E_MSERVICE_VOICE, createService);


/**
  * @brief  Object service creation
  * @param  id: GSM modem service ID
  * @param  portList: Modem port list
  * @param  listSize: Number of ports in the list
  * @return Pointer to object of service or NULL on error
  */
CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize)
{
    CModemPort* port = (listSize > 1) ? portList[1] : portList[0];
    return (id == E_MSERVICE_VOICE && listSize && portList && port)
            ? new CMServiceVoice(port)
            : NULL;
}


/**
  * @brief Constructor
  * @param port: Used port
  */
CMServiceVoice::CMServiceVoice (CModemPort* port) :
    CMService(E_MSERVICE_VOICE, port),
    m_pCfg(NULL)
{
    assert(!port);

    m_state.callState = E_VOICE_STATE_NOCALL;
    m_state.errCode = E_VOICE_ERROR_NONE;
    m_phone[0] = '\0';
}


/**
  * @brief Destructor
  * @param None
  */
CMServiceVoice::~CMServiceVoice ()
{
}


/**
  * @brief  Service initialization
  * @param  params: Service parameters
  * @return 0 or negative error code
  */
int CMServiceVoice::Open (const void* params)
{
    if (!params) return -1;
    
    m_pCfg = (const TVoiceCfg*) params;
    
    m_pPort->Lock(m_id);
    int res = m_pPort->SendAT("AT+CLIP=1");
    m_pPort->Unlock(m_id);
    
    if (res < 0) return -2;
    
    return 0;
}


/**
  * @brief  Service deinitialization
  * @param  None
  * @return 0 or negative error code
  */
int CMServiceVoice::Close (void)
{
    m_pCfg = NULL;
    return 0;
}


/**
  * @brief  Read data
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Size of data written into the buffer or negative error code
  */
int CMServiceVoice::Read (void* buff, uint size)
{
    return -1;
}


/**
  * @brief  Write data
  * @param  data: Data for writing
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CMServiceVoice::Write (const void* data, uint size)
{
    // FIXME: send data to audio channel?
    return -1;
}


/**
  * @brief  Extended command handling
  * @param  code: Command code
  * @param  arg: Command arguments
  * @return Processeing result (see IOCTL_NOT_xx)
  */
int CMServiceVoice::Ioctl (uint code, void* arg)
{
    switch (code)
    {
        case IOCTL_VOICE_SEND_DTMF:
        {
            int res = sendDtmf( (const char*) arg );
            if (res) return IOCTL_ERROR;
        }
        break;
        
        case IOCTL_VOICE_GET_PHONE:
            if (!arg) return IOCTL_ERROR;
            strcpy( (char*)arg, m_phone );
        break;
        
        case IOCTL_VOICE_GET_CALL_STATE:
            if (!arg) return IOCTL_ERROR;
            *(TVoiceState*)arg = m_state;
        break;
        
        case IOCTL_VOICE_CALL_OUT:
        {
            int res = callOut( (const char*) arg );
            if (res) return IOCTL_ERROR;
        }
        break;
        
        case IOCTL_VOICE_CALL_ACCEPT:
        case IOCTL_VOICE_CALL_TERMINATE:
        {
            int res = callCtrl( (code == IOCTL_VOICE_CALL_ACCEPT) );
            if (res) return IOCTL_ERROR;
        }
        break;
        
        case IOCTL_VOICE_MODEM_DETECT_DTMF:
            if (m_pCfg && m_pCfg->onDetectDtmf)
            {
                uint c = (uint) ((size_t)arg);
                m_pCfg->onDetectDtmf(this, c);
            }
        break;
        
        default: return CMService::Ioctl(code, arg);
    }
    
    return IOCTL_OK;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CMServiceVoice::processUrc (const char* str)
{
    // Incoming call: +CRING: VOICE
    if ( strstr(str, "+CRING: VOICE") )
    {
        if (m_state.callState == E_VOICE_STATE_NOCALL)
        {
            m_state.callState = E_VOICE_STATE_CALL_IN;
            m_state.errCode = E_VOICE_ERROR_NONE;
            new CVoiceWorker(this);
        }
        return CMODEM_URC_RES_OK;
    }
    
    // Phone number (incuming call) +CLIP: "02151082965",129,"",,"",0
    // Phone number (outgoing call) +COLP: "+79231234567",145,"",0
    if (
        m_state.callState != E_VOICE_STATE_NOCALL &&
        ( strstr(str, "+CLIP") || strstr(str, "+COLP") )
    ) {
        if ( strstr(str, "+COLP") )
        {
            m_state.callState = E_VOICE_STATE_ACTIVE_OUT;
            m_state.errCode = E_VOICE_ERROR_NONE;
        }
        
        do
        {
            str = strchr(str, '"');
            if (!str) break;
            
            ++str;
            if (!*str) break;
            
            const char* pEnd = strchr(str, '"');
            if (!pEnd) break;
            
            uint len = __MIN(sizeof(m_phone) - 1, pEnd - str);
            memcpy(m_phone, str, len);
            m_phone[len] = '\0';
        } while (0);
        
        return CMODEM_URC_RES_OK;
    }

    
    if (m_state.callState != E_VOICE_STATE_NOCALL)
    {
        if ( !strcmp(str, "NO DIALTONE") )
        {
            m_state.callState = E_VOICE_STATE_NOCALL;
            m_state.errCode = E_VOICE_ERROR_NO_DIALTONE;
        }
        else if ( !strcmp(str, "BUSY") )
        {
            m_state.callState = E_VOICE_STATE_NOCALL;
            m_state.errCode = E_VOICE_ERROR_BUSY;
        }
        else if ( !strcmp(str, "NO CARRIER") )
        {
            m_state.callState = E_VOICE_STATE_NOCALL;
            m_state.errCode = E_VOICE_ERROR_NO_CARRIER;
        }
        else if ( !strcmp(str, "NO ANSWER") )
        {
            m_state.callState = E_VOICE_STATE_NOCALL;
            m_state.errCode = E_VOICE_ERROR_NO_ANSWER;
        }
        
        if (m_state.callState == E_VOICE_STATE_NOCALL) return CMODEM_URC_RES_OK;
    }
    
    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  Make outgoing call
  * @param  phone: Remote subscriber phone number
  * @return 0 or negative error code
  */
int CMServiceVoice::callOut (const char* phone)
{
    if (!phone) return -1;
    
    int len = strlen(phone);
    if (len > 16) return -2;
    
    const uint buffSize = 32;
    char* buff = (char*) malloc(buffSize);
    if (!buff) return -3;
    
    m_pPort->Lock(m_id);
    
    int result;
    do
    {
        // Option turning off in order to get OK on ATD command immediately after dialing
        result = m_pPort->SendAT("AT+COLP=0");
        if (result < 0) break;
        
        len = sprintf(buff, buffSize, "ATD%s;", phone);
        buff[len] = '\0';
        result = m_pPort->SendAT(buff, "OK", VOICE_DIALING_TIMEOUT * 1000);
        if (result < 0)
        {
            m_state.errCode = E_VOICE_ERROR_NO_DIALTONE;
            break;
        }
        
        m_state.callState = E_VOICE_STATE_CALL_OUT;
        m_state.errCode = E_VOICE_ERROR_NONE;
        
        // Turn back on the option to be able to determine the moment of picking up the handset
        result = m_pPort->SendAT("AT+COLP=1");
        if (result < 0) break;
    } while (0);
    
    m_pPort->Unlock(m_id);
    free(buff);
    
    return result;
}


/**
  * @brief  Incoming call control
  * @param  isAccept: Action with a call (true - accept, false - hang up)
  * @return 0 or negative error code
  */
int CMServiceVoice::callCtrl (bool isAccept)
{
    if (isAccept)
    {
        if (m_state.callState != E_VOICE_STATE_CALL_IN) return -1;
        
        m_pPort->Lock(m_id);
        int res = m_pPort->SendAT("ATA");
        m_pPort->Unlock(m_id);
        if (res < 0) return -2;
        
        m_state.callState = E_VOICE_STATE_ACTIVE_IN;
        m_state.errCode = E_VOICE_ERROR_NONE;
    }
    else
    {
        if (m_state.callState == E_VOICE_STATE_NOCALL) return 0;
        
        m_pPort->Lock(m_id);
        int res = m_pPort->SendAT("ATH");
        m_pPort->Unlock(m_id);
        if (res < 0) return -3;
        
        m_state.callState = E_VOICE_STATE_NOCALL;
        m_state.errCode = E_VOICE_ERROR_NONE;
    }
    
    return 0;
}


/**
  * @brief  DTMF codes sending
  * @param  str: String of DTMF symbols
  * @return 0 or negative error code
  */
int CMServiceVoice::sendDtmf (const char* str)
{
    if (!str || !*str) return -1;
    
    int len = strlen(str);
    const uint buffSize = sizeof("AT+VTS=\"%s\"") + len;
    char* buff = (char*) malloc(buffSize);
    if (!buff) return -2;
    
    m_pPort->Lock(m_id);
    int res;
    
    do
    {
        res = m_pPort->SendAT("AT+VTD=1,0");
        if (res < 0)
        {
            res = -3;
            break;
        }
        
        len = sprintf(buff, buffSize, "AT+VTS=\"%s\"", str);
        buff[len] = '\0';
        
        res = m_pPort->SendAT(buff, "OK", ((len + 1) / 2) * 1000);
        if (res < 0)
        {
            res = -4;
            break;
        }
    } while (0);
    
    m_pPort->Unlock(m_id);
    free(buff);
    
    return (res < 0) ? res : 0;
}


/**
  * @brief  Internal worker handler (incoming call processing)
  * @param  None
  */
void CMServiceVoice::workerHandler (void)
{
    if (m_state.callState != E_VOICE_STATE_CALL_IN) return;
    
    if (m_pCfg && m_pCfg->onCallIn)
    {
        m_pCfg->onCallIn(this);
    }
    else
    {
        int result;
        uint tryCount = 3;
        do
        {
            result = callCtrl(false);
        } while (result < 0 && tryCount--);
        
        m_state.callState = E_VOICE_STATE_NOCALL;
        m_state.errCode = E_VOICE_ERROR_NONE;
    }
}


/**
  * @brief Internal worker constructor
  * @param obj: Voice service object
  */
CVoiceWorker::CVoiceWorker (CMServiceVoice* obj) :
    KThread("VoiceWorker", VOICE_WORKER_STACK_SIZE, THREAD_PRIORITY_DEFAULT),
    m_obj(obj)
{
}


/**
  * @brief Internal worker main thread function
  * @param None
  */
void CVoiceWorker::run (void)
{
    m_obj->workerHandler();
}
