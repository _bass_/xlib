//*****************************************************************************
//
// @brief   GSM modem SMS service
// @author  Babkin Petr
//
//*****************************************************************************

#include "drivers/gsm/service/service_sms.h"
#include "drivers/gsm/service/pdu.h"
#include "drivers/gsm/modem.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "kernel/kstdio.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/math.h"
#include "misc/string.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Max header size of outgoing message (SCA = 0, DA - 12 bytes, VP - 1 byte, UDL)
    #define PDU_OUT_MAX_HEADER_SIZE                 19
    // Max header size of incoming message (SCA = 12 bytes, OA - 12 bytes, + UDL)
    #define PDU_IN_MAX_HEADER_SIZE                  32

    // Max user data size
    #define PDU_MAX_USERDATA_SIZE                   140
    #define PDU_MAX_7BIT_USERDATA_SIZE              (PDU_MAX_USERDATA_SIZE * 8 / 7)

    //  End of PDU input character (ctrl+z)
    #define PDU_END_MARKER                          0x1a

    // Data coding scheme (DCS) fields
    #define PDU_DCS_GROUP_GENERAL                   0x00
    #define PDU_DCS_COMPRESS                        0x20
    #define PDU_DCS_MSG_CLASS_PRESENT               0x10
    #define PDU_DCS_MSG_CLASS_0                     0x00
    #define PDU_DCS_MSG_CLASS_1                     0x01
    #define PDU_DCS_MSG_CLASS_2                     0x02
    #define PDU_DCS_MSG_CLASS_3                     0x03
    #define PDU_DCS_CHARSET_7BIT                    0x00
    #define PDU_DCS_CHARSET_8BIT                    0x04
    #define PDU_DCS_CHARSET_UNICODE                 0x08

    // PDU message type
    #define PDU_TYPE_DELIVER                        0x00
    #define PDU_TYPE_SUBMIT                         0x01

    // Data format for Validity Period
    #define PDU_VP_FORMAT_NONE                      0x00
    #define PDU_VP_FORMAT_RELATIVE                  0x02
    #define PDU_VP_FORMAT_ABSOLUTE                  0x03

    // AT-command format for message sending
    #define SMS_AT_FORMAT_SEND                      "AT+CMGS=%u"

    // SMS send timeout (sec)
    #define SMS_SEND_TIMEOUT                        30


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Outgoing message descriptor
    typedef struct
    {
        const char*     data;           // Data to send
        uint            size;           // Data size
        char*           buff;           // Buffer for PDU data creation (maximum size)
        uint            buffSize;       // PDU buffer size
        const char*     phone;          // Recipient phone number
        TSmsDataType    dataType;       // Data type
        char            validPeriod;    // Message lifetime (days). 2..30 days, 0 - not set
        uint16          refId;          // Message ID (common for all message parts)
        char            msgRef;         // Outgoing message ID (unique for each message / part of message)
        char            partNum;        // Number of sending message part
    } TSmsOutDesc;

    // PDU type
    typedef union
    {
        char    val;
        struct
        {
            char    msgTypeInd  : 2;    // Message Type Indicator
            char    rejectDup   : 1;    // Reject Duplicates
            char    vp          : 2;    // Validity Period
            char    statusRepReq: 1;    // Status Report Request
            char    userDataHdr : 1;    // Flag that determines whether header in UD exists
            char    replyPath   : 1;    // Request of response from message recipient
        } out;
        struct
        {
            char    msgTypeInd  : 2;    // Message Type Indicator
            char    moreMsg     : 1;    // More Message to Send
            char    reserved    : 2;
            char    statusRepReq: 1;    // Status Report Request
            char    userDataHdr : 1;    // Flag that determines whether header in UD exists
            char    replyPath   : 1;    // Request of response from message recipient
        } in;
    } TPduType;


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize);
    static int pduDecode (char* pdu, TSmsInfoIn* pInfo);
    static int pduEncode (TSmsOutDesc* pDesc);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("sms", "SMS", NULL, NULL);
    REGISTER_GSM_SERVICE(E_MSERVICE_SMS, createService);

    // Sending message ID (MR)
    static char g_msgRef = 0;
    // Message ID (comman for each message part)
    static char g_msgId = 0;


/**
  * @brief  Object service creation
  * @param  id: GSM modem service ID
  * @param  portList: Modem port list
  * @param  listSize: Number of ports in the list
  * @return Pointer to object of service or NULL on error
  */
CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize)
{
    CModemPort* port = (listSize > 1) ? portList[1] : portList[0];
    return (id == E_MSERVICE_SMS && listSize && portList && port)
            ? new CMServiceSms(port)
            : NULL;
}


/**
  * @brief Constructor
  * @param port: Used port
  */
CMServiceSms::CMServiceSms (CModemPort* port) :
    CMService(E_MSERVICE_SMS, port),
    m_pCfg(NULL),
    m_optInfoInPtr(NULL),
    m_optInfoOutPtr(NULL)
{
    assert(!port);
}


/**
  * @brief Destructor
  * @param None
  */
CMServiceSms::~CMServiceSms ()
{
}


/**
  * @brief  Service initialization
  * @param  params: Service parameters
  * @return 0 or negative error code
  */
int CMServiceSms::Open (const void* params)
{
    if (!params) return -1;
   
    m_pCfg = (const TSmsCfg*) params;
    
    m_pPort->Lock(m_id);
    int result = 0;
    
    do
    {
        // Set SMS PDU mode
        result = m_pPort->SendAT("AT+CMGF=0");
        if (result < 0) break;
        
        // Turn on notifications about new messages
        result = m_pPort->SendAT("AT+CNMI=2,1,0,0,0");
        if (result < 0) break;
        
        result = 0;
    } while (0);

    m_pPort->Unlock(m_id);
    
    return result;
}


/**
  * @brief  Service deinitialization
  * @param  None
  * @return 0 or negative error code
  */
int CMServiceSms::Close (void)
{
    m_pCfg = 0;
    return 0;
}


/**
  * @brief  Read data
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Size of data written into the buffer or negative error code
  */
int CMServiceSms::Read (void* buff, uint size)
{
    if (!buff || !size) return -1;
    
    m_pPort->Lock(m_id);
    
    char* pduBuff = NULL;
    int result = 0;

    do
    {
        if ( !m_pPort->ToCmd() )
        {
            result = -2;
            break;
        }
        
        int smsIndex = smsGetIndex();
        if (smsIndex < 0) break;
        
        const int buffSize = (PDU_IN_MAX_HEADER_SIZE + PDU_MAX_USERDATA_SIZE) * 2;
        pduBuff = (char*) malloc(buffSize);
        if (!pduBuff)
        {
            result = -4;
            break;
        }
        
        int pduLen = smsReadPdu(smsIndex, pduBuff, buffSize);
        if (pduLen < 0)
        {
            result = -5;
            break;
        }
        
        pduBuff[pduLen] = '\0';
        
        // Remove received message
        {
            static const char atFormat[] = "AT+CMGD=%u";
            char buffAt[sizeof(atFormat)];
            int res = sprintf(buffAt, sizeof(buffAt), atFormat, smsIndex);
            if (res <= 0)
            {
                result = -6;
                break;
            }
            buffAt[res] = '\0';
            
            res = m_pPort->SendAT(buffAt);
            if (res < 0)
            {
                result = -7;
                break;
            }
        }
        
        result = pduDecode(pduBuff, m_optInfoInPtr);
        if (result <= 0)
        {
            result = -8;
            break;
        }
        
        if (result > size) result = size;
        memcpy(buff, pduBuff, result);
    } while (0);

    m_optInfoInPtr = NULL;
    m_pPort->Unlock(m_id);
    
    if (pduBuff) free(pduBuff);

    // FIXME: partial message reading???
    // FIXME: multipart messages

    return result;
}


/**
  * @brief  Write data
  * @param  data: Data for writing
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CMServiceSms::Write (const void* data, uint size)
{
    if (!data || !size) return -1;
    
    m_pPort->Lock(m_id);
    
    // Header + data + \0
    const uint buffSize = (PDU_OUT_MAX_HEADER_SIZE + __MIN(size, PDU_MAX_USERDATA_SIZE)) * 2 + 1;
    char* pduBuff = (char*) malloc(buffSize);
    int result = 0;

    TSmsOutDesc smsDesc = 
    {
        .data = (const char*)data,
        .size = size,
        .buff = pduBuff,
        .buffSize = buffSize,
        .phone = m_optInfoOutPtr->phone,
        .dataType = m_optInfoOutPtr->dataType,
        .validPeriod = m_optInfoOutPtr->validPeriod,
        .refId = ++g_msgId,
        .partNum = 1
    };

    while (1)
    {
        if (!pduBuff)           { result = -2; break; }
        if (!m_optInfoOutPtr)   { result = -3; break; }
        if ( !m_pPort->ToCmd() ) { result = -4; break; }
        
        smsDesc.msgRef = g_msgRef;
        
        int pduLen = pduEncode(&smsDesc);
        if (!pduLen) break;
        if (pduLen <= 2)
        {
            result = -5;
            break;
        }
        
        // Data size calculation for passing to AT-command (bytes, without SCA)
        int lenForAt = pduLen / 2 - (smsDesc.buff[0] - '0') * 10 - (smsDesc.buff[1] - '0') - 1;
        if (lenForAt <= 0 || lenForAt >= pduLen)
        {
            result = -6;
            break;
        }
        
        // Sending transfer request and waiting for input prompt
        char atBuff[sizeof(SMS_AT_FORMAT_SEND) + 3];
        int res = sprintf(atBuff, sizeof(atBuff), SMS_AT_FORMAT_SEND, lenForAt);
        atBuff[res] = '\0';
        res = m_pPort->SendAT(atBuff, "> ");
        if (res < 0)
        {
            // Try to wait input prompt after OK receiving
            res = m_pPort->SendAT(NULL, "> ");
            if (res < 0)
            {
                result = -7;
                break;
            }
        }
        
        // Sending PDU
        res = m_pPort->Write(pduBuff, pduLen);
        if (res <= 0)
        {
            result = -8;
            break;
        }
        
        const char eol = PDU_END_MARKER;
        res = m_pPort->Write( (void*)&eol, sizeof(eol) );
        if (res <= 0)
        {
            result = -9;
            break;
        }
        
        // Waiting the end of transfering "+CMGS: xx" -> "OK"
        uint32 timeStart = Kernel::GetJiffies();
        while ( !IsTimeout(timeStart, SMS_SEND_TIMEOUT * 1000) )
        {
            res = m_pPort->SendAT(NULL);
            if (!res || m_pPort->GetError() != CMODEM_CME_ERR_NONE) break;
        }
        
        if (res < 0)
        {
            result = -10;
            break;
        }
        
        ++g_msgRef;
    } // while (1)

    m_optInfoOutPtr = NULL;
    m_pPort->Unlock(m_id);
    
    if (pduBuff) free(pduBuff);

    return (result < 0) ? result : size;
}


/**
  * @brief  Extended command handling
  * @param  code: Command code
  * @param  arg: Command arguments
  * @return Processeing result (see IOCTL_NOT_xx)
  */
int CMServiceSms::Ioctl (uint code, void* arg)
{
    switch (code)
    {
        case IOCTL_MODEM_SMS_SET_INFO_IN:
            m_optInfoInPtr = (TSmsInfoIn*) arg;
        break;
        
        case IOCTL_MODEM_SMS_SET_INFO_OUT:
            m_optInfoOutPtr = (TSmsInfoOut*) arg;
        break;
        
        default:
            return CMService::Ioctl(code, arg);
        break;
    }

    return IOCTL_OK;
}


/**
  * @brief  Reading index of first incoming SMS
  * @param  None
  * @return SMS index or negative error code
  */
int CMServiceSms::smsGetIndex (void)
{
    char buff[12];
    
    m_pPort->Lock(m_id);
    // Print list of unread messages
    int res = m_pPort->SendAT( "AT+CMGL=0,1", "OK", buff, sizeof(buff) );
    m_pPort->Unlock(m_id);
    
    if (res < 0) return -1;
    
    char* str = buff;
    if ( !strstr(str, "+CMGL:") ) return -2;
    
    str += sizeof("+CMGL:") - 1;
    if (*str == ' ') ++str;
    
    uint smsIndex = atoi(str);
    return smsIndex;
}


/**
  * @brief  Reading SMS in PDU format
  * @param  smsIndex: SMS index
  * @param  buff: Buffer for PDU
  * @param  size: Buffer size
  * @return PDU data size written in buffer or negative error code
  */
int CMServiceSms::smsReadPdu (uint smsIndex, char* buff, uint size)
{
    if (!buff || !size) return -1;
    
    static const char format[] = "AT+CMGR=%u,1";
    char at[sizeof(format)];
    int res = sprintf(at, sizeof(at), format, smsIndex);
    if (res < 0) return -2;
    at[res] = '\0';
    
    int offset = 0;
    m_pPort->Lock(m_id);
    {
        res = m_pPort->SendAT(at, NULL);
        if (res < 0)
        {
            m_pPort->Unlock(m_id);
            return -3;
        }
        
        // PDU reading
        uint pduLen = 0;
        uint32 timeStart = Kernel::GetJiffies();
        
        while ( !IsTimeout(timeStart, CMODEM_AT_TIMEOUT) )
        {
            const char* pAt = m_pPort->ReadAT();
            // AT-command execution error
            if (!pAt) break;
            
            // Execution complete
            if ( !strcmp(pAt, "OK") || !strcmp(pAt, "ERROR") ) break;
            
            // Response received - extending the timeout.
            timeStart = Kernel::GetJiffies();
            
            // Format: +CMGR: <stat>,[<alpha>],<length>
            if ( !pduLen && strstr(pAt, "+CMGR:") )
            {
                const char* pLen = strchr(pAt, ',');
                if (!pLen || !*pLen) continue;
                pLen = strchr(pLen + 1, ',');
                
                if ( pLen && *(pLen + 1) )
                {
                    pduLen = atoi(pLen + 1);
                    if (pduLen)
                    {
                        // Raw -> ASCII
                        pduLen *= 2;

                        if (pduLen > size)
                        {
                            offset = -4;
                        }
                    }
                }
                continue;
            }
            
            if (offset < 0 || offset >= pduLen) continue;
            
            // Copy PDU into the buffer
            uint len = strlen(pAt);
            if (!len) continue;
            if ( (len + offset) > size ) len = size - offset;
            
            memcpy(&buff[offset], pAt, len);
            
            // Expected PDU size adjusting
            if (offset < 2 && (offset + len) >= 2)
            {
                uint lenSca = (1 + (buff[0] - '0') * 10 + (buff[1] - '0')) * 2;
                pduLen += lenSca;
                
                if (pduLen > size)
                {
                    offset = -5;
                    continue;
                }
            }
            
            offset += len;
        } // while ( IsTimeout(timeStart, CMODEM_AT_TIMEOUT * 1000) )
        
        // PDU string length validation
        if (offset > 0 && offset != pduLen)
        {
            offset = -6;
        }
    }
    m_pPort->Unlock(m_id);
    
    return offset;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CMServiceSms::processUrc (const char* str)
{
    // New SMS received: +CMTI: "SM",3
    if ( strstr(str, "+CMTI:") )
    {
        if (m_pCfg && m_pCfg->recvNotifyHdl)
        {
            // New message notification
            m_pCfg->recvNotifyHdl();
        }
        
        return CMODEM_URC_RES_OK;
    }

    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  PDU string decoding
  * @param  pdu: PDU string (will pe replaced by decoded data)
  * @param  pInfo: Pointer to variable to save message metadata
  * @return SMS text length or negative error code
  */
int pduDecode (char* pdu, TSmsInfoIn* pInfo)
{
    if (!pdu) return -1;
    
    // 07919731899699F0040B919731080893F50004701092215220180774657374706475
    // 07919731899699F0 - SMS-center phone number length (bytes), phone number type and the number. ADDRESS OF DELIVERING SMSC NUMBER IS:+79139869990
    // 04  - MESSAGE HEADER FLAGS
    // 0B 91 9731080893F5 - recipient phone number length (symbols), phone number type, phone numner (12 symbols). ORIGINATING ADDRESS NUMBER IS:+79138080395
    // 00 04 - Protocol type and coding scheme
    // 701092 215220 18 - date and time from SMS-center. SMSC TIMESTAMP: 29.01.07 12:25:02 GMT-0,25
    // 07 74657374706475 - length and data
    
    int size = HexToRaw(pdu);
    if (size <= 0) return -2;
    char* ptr = pdu;
    
    // SCA - SMS-center phone number (unused)
    uint fieldSize = (uint8)(*ptr);
    if (size <= fieldSize || fieldSize > 12) return -3;
    
    ptr += fieldSize + 1;
    
    // PDU type - flags
    if (size < ptr - pdu) return -4;
    TPduType pduType = { .val = *ptr++ };
    
    // OA - Sender phone number
    fieldSize = ((uint8)(*ptr + 1) >> 1) + 1;
    if (size <= ptr - pdu + fieldSize) return -5;
    
    if (pInfo)
    {
        int res = PDU::DecodePhone(ptr, fieldSize + 1, pInfo->phone);
        if (res < 0) return -50;
    }
    ptr += fieldSize + 1;
    
    // PID – protocol ID (0x00)
    if (size < ptr - pdu) return -6;
    if (*ptr) return -60;
    ++ptr;
    
    // DCS – data coding scheme
    if (size < ptr - pdu) return -7;
    char dcs = *ptr;
    ++ptr;
    
    // SCTS - Date and time
    if (size < ptr - pdu + 7) return -8;
    
    if (pInfo)
    {
        pInfo->time.year = (ptr[0] & 0x0f) * 10 + (ptr[0] >> 4);
        pInfo->time.month = (ptr[1] & 0x0f) * 10 + (ptr[1] >> 4);
        pInfo->time.day = (ptr[2] & 0x0f) * 10 + (ptr[2] >> 4);
        pInfo->time.hour = (ptr[3] & 0x0f) * 10 + (ptr[3] >> 4);
        pInfo->time.min = (ptr[4] & 0x0f) * 10 + (ptr[4] >> 4);
        pInfo->time.sec = (ptr[5] & 0x0f) * 10 + (ptr[5] >> 4);
        pInfo->timeZone = (ptr[6] & 0x07) * 10 + (ptr[6] >> 4);
        if (ptr[6] & 0x08) pInfo->timeZone = -pInfo->timeZone;
    }
    ptr += 7;
    
    // UDL - user data length
    if (size < ptr - pdu) return -8;
    uint dataSize = (uint8)*ptr;
    ++ptr;
    
    // UD - user data
    uint restSize = size - (ptr - pdu);
    if (!restSize || restSize > PDU_MAX_USERDATA_SIZE) return -9;
    
    // coding scheme detection
    dcs &= 0x0c;
    switch (dcs)
    {
        // 7-bit scheme: data length is number of septets (7-bit symbols)
        case PDU_DCS_CHARSET_7BIT:
        {
            if (pInfo) pInfo->dataType = SMS_DATA_TYPE_ASCII;
            int res = PDU::Decode7bit(ptr, dataSize, pdu);
            if (res < 0) return -10;
        }
        break;
        // 8-bit charset: data length is bytes
        // Unicode: data length is bytes
        case PDU_DCS_CHARSET_8BIT:
        case PDU_DCS_CHARSET_UNICODE:
            if (pInfo)
            {
                pInfo->dataType = (dcs == PDU_DCS_CHARSET_8BIT) ? SMS_DATA_TYPE_8BIT : SMS_DATA_TYPE_UNICODE;
            }
            memcpy(pdu, ptr, dataSize);
        break;
        
        default: return -11;
    }
    
    // Removing data header
    if (pduType.in.userDataHdr)
    {
        // Hedaer size calculation, including after 7-bit decoding
        uint udhl = ((uint8)*pdu) & 0x7f;
        if (udhl >= dataSize) return -100;
        dataSize -= udhl + 1;
        memcpy(pdu, pdu + udhl + 1, dataSize);
    }
    
    if (dcs == PDU_DCS_CHARSET_UNICODE)
    {
        dataSize &= ~0x01;          // Unicode - 2-bytes charset, size is a multiple of 2
        BeToLeWord(pdu, dataSize);  // UCS2 - data in Big Endian format
    }
    
    return dataSize;
}


/**
  * @brief  PDU string encoding
  * @param  pDesc: outgoing message descriptor
  * @return PDU encoded message size (written in pDesc->buff) or negative error code
  */
int pduEncode (TSmsOutDesc* pDesc)
{
    if (!pDesc || !pDesc->data || !pDesc->size || !pDesc->phone || !pDesc->buff) return -1;
    if ( pDesc->buffSize < ((PDU_OUT_MAX_HEADER_SIZE + __MIN(pDesc->size, PDU_MAX_USERDATA_SIZE)) * 2) ) return -2;
    
    if (!pDesc->partNum) pDesc->partNum = 1;
    
    // Calculation of processing data part size and total number of SMS (parts) 
    uint udhl = 0;      // User data header size. 0 - there is no splitting on parts
    uint maxPartSize;
    uint totalPartsCount;
    {
        switch (pDesc->dataType)
        {
            case SMS_DATA_TYPE_ASCII:   maxPartSize = PDU_MAX_7BIT_USERDATA_SIZE; break;
            case SMS_DATA_TYPE_8BIT:
            case SMS_DATA_TYPE_UNICODE: maxPartSize = PDU_MAX_USERDATA_SIZE; break;
            default: return -3;
        }
        
        // Maximum block size adjustment considering UDH header size
        // For 7-bit scheme 7-byte header (UDHL + UDH) is used to prevent aligning to 7-bit border
        // For UCS2 and 8-bit schemes 6-byte header (UDHL + UDH) is used.
        if (pDesc->size > maxPartSize)
        {
            // Data header size (only attributes, without UDHL field)
            udhl = (pDesc->dataType == SMS_DATA_TYPE_ASCII) ? 7 - 1 : 6 - 1;
            
            uint udhEquivalentSize;
            if (pDesc->dataType == SMS_DATA_TYPE_ASCII) udhEquivalentSize = 8;      // (udhl + 1) * 8bit / 7bit
            else                                        udhEquivalentSize = udhl + 1;
            
            maxPartSize -= udhEquivalentSize;
        }
        
        totalPartsCount = (pDesc->size + maxPartSize - 1) / maxPartSize;
    }
    
    if (pDesc->partNum > totalPartsCount) return 0;
    if (totalPartsCount > 255) return -4;
    
    
    char header[PDU_OUT_MAX_HEADER_SIZE];
    
    // SCA - getting from SIM (0x00)
    header[0] = 0x00;
    
    // PDU type
    TPduType pduType = {};
    pduType.out.msgTypeInd = PDU_TYPE_SUBMIT;
    pduType.out.vp = (pDesc->validPeriod) ? PDU_VP_FORMAT_RELATIVE : PDU_VP_FORMAT_NONE;
    pduType.out.userDataHdr = (udhl > 0);
    header[1] = pduType.val;
    
    // Message Reference
    header[2] = pDesc->msgRef;
    
    // DA - Recipient phone number
    char* ptr = &header[3];
    int daLen = PDU::EncodePhone(ptr, sizeof(header) - (ptr - header), pDesc->phone);
    if (daLen <= 0) return -5;
    ptr += daLen;
    
    // PID - protocol ID
    *ptr++ = 0x00;
    
    // DCS - Coding scheme
    *ptr = PDU_DCS_GROUP_GENERAL;
    switch (pDesc->dataType)
    {
        case SMS_DATA_TYPE_ASCII:   *ptr |= PDU_DCS_CHARSET_7BIT; break;
        case SMS_DATA_TYPE_8BIT:    *ptr |= PDU_DCS_CHARSET_8BIT; break;
        case SMS_DATA_TYPE_UNICODE: *ptr |= PDU_DCS_CHARSET_UNICODE; break;
    }
    ptr++;
    
    // VP - message lifetime (days)
    if (pDesc->validPeriod)
    {
        if (pDesc->validPeriod < 2 || pDesc->validPeriod > 30) return -4;
        *ptr++ = 166 + pDesc->validPeriod;
    }
    
    // Header creation (ASCII)
    int writeSize = sprintf(pDesc->buff, pDesc->buffSize, "%p", header, ptr - header);
    if (writeSize < 0) return -6;
    ptr = pDesc->buff + writeSize;
    
    
    // UDL - Data block length
    const char* data = pDesc->data;
    uint size = pDesc->size;
    
    if (udhl)
    {
        uint sendedData = (pDesc->partNum - 1) * maxPartSize;
        data += sendedData;
        size -= sendedData;
        if (size > maxPartSize) size = maxPartSize;
        
        // UDL in 7-bit coding scheme is number of septets in data and in header
        // In other cases, UDL is number of bytes in data and header
        // 1 byte - UDHL field size
        uint udl;
        if (pDesc->dataType == SMS_DATA_TYPE_ASCII) udl = size + (((1 + udhl) * 8 + 6) / 7);
        else                                        udl = size + (1 + udhl);
        
        int res = sprintf(ptr, 2, "%.2x", udl);
        if (res < 0) return -7;
        writeSize += res;
        ptr += res;
    }
    else
    {
        int res = sprintf(ptr, 2, "%.2x", size);
        if (res < 0) return -8;
        writeSize += res;
        ptr += res;
    }
    
    // UDH - data header
    if (udhl)
    {
        char udh[8];
        char* p = udh;
        
        *p++ = udhl;
        if (pDesc->dataType == SMS_DATA_TYPE_ASCII)
        {
            *p++ = 0x08;  // Concatenated short message, 16-bit reference number
            *p++ = 4;     // Information field size
            *((uint16*)p) = pDesc->refId;
            p += 2;
        }
        else
        {
            *p++ = 0x00;  // Concatenated short messages, 8-bit reference number
            *p++ = 3;     // Information field size
            *p++ = pDesc->refId;
        }
        
        *p++ = totalPartsCount;
        *p++ = pDesc->partNum;
        
        int res = sprintf(ptr, pDesc->buffSize - writeSize, "%p", udh, p - udh);
        if (res < 0) return -9;
        writeSize += res;
        ptr += res;
    }
    
    // Data block
    switch (pDesc->dataType)
    {
        case SMS_DATA_TYPE_ASCII:
        {
            // Saving encoded data in the middle of free space in pDesc->buff,
            // That alaws to do correct converting from raw data to hex format
            const int freeSize = (pDesc->buffSize - writeSize) / 2;
            char* pEncodedData = ptr + freeSize;
            
            int res = PDU::Encode7bit(data, size, pEncodedData, freeSize);
            if (res <= 0) return -10;
            
            // Raw -> hex conversion
            res = sprintf(ptr, pDesc->buffSize - writeSize, "%p", pEncodedData, res);
            if (res <= 0) return -11;
            writeSize += res;
        }
        break;
        
        case SMS_DATA_TYPE_8BIT:
        {
            int res = sprintf(ptr, pDesc->buffSize - writeSize, "%p", data, size);
            if (res <= 0) return -12;
            writeSize += res;
        }
        break;
        
        case SMS_DATA_TYPE_UNICODE:
        {
            // Byte order changing (LE -> BE) with placing in the middle of free space of buffer
            const int freeSize = (pDesc->buffSize - writeSize) / 2;
            char* pRaw = ptr + freeSize;
            
            for (uint i = 0; i < size; i += 2)
            {
                pRaw[i] = data[i + 1];
                pRaw[i + 1]= data[i];
            }
            
            int res = sprintf(ptr, pDesc->buffSize - writeSize, "%p", pRaw, size);
            if (res <= 0) return -13;
            writeSize += res;
        }
        break;
    } // switch (pDesc->dataType)
    
    ++pDesc->partNum;
    
    return writeSize;
}
