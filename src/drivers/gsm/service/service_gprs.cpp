//*****************************************************************************
//
// @brief   Modem GPRS service driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/service/service_gprs.h"
#include "drivers/gsm/modem.h"
#include "lwip/api.h"
#include "lwip/pppapi.h"
#include "netif/ppp/pppos.h"
#include "net/socket.h"
#include "kernel/kernel.h"
#include "kernel/kstdio.h"
#include "kernel/kdebug.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize);
    static u32_t gprsSendPacket (ppp_pcb* pcb, u8_t* data, u32_t len, void* ctx);
    static void gprsLinkStatusChanged (ppp_pcb* pcb, int err_code, void* ctx);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("gprs", "GPRS", NULL, NULL);
    REGISTER_GSM_SERVICE(E_MSERVICE_GPRS, createService);


/**
  * @brief  Object service creation
  * @param  id: GSM modem service ID
  * @param  portList: Modem port list
  * @param  listSize: Number of ports in the list
  * @return Pointer to object of service or NULL on error
  */
CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize)
{
    return (id == E_MSERVICE_GPRS && listSize && portList && portList[0])
            ? new CMServiceGprs(portList[0])
            : NULL;
}


/**
  * @brief  Callback function for send packet
  * @param  pcb: PPP session descriptor (lwip)
  * @param  data: Data for send
  * @param  len: Data size
  * @param  ctx: Callback context (pointer to GPRS service object)
  * @return Sent data size
  */
u32_t gprsSendPacket (ppp_pcb* pcb, u8_t* data, u32_t len, void* ctx)
{
    CMServiceGprs* service = (CMServiceGprs*) ctx;
    
    int res = service->Write(data, len);
    
	return (res < 0) ? 0 : res;
}


/**
  * @brief  Callback function of link status changing
  * @param  pcb: PPP session descriptor (lwip)
  * @param  err_code: Error code
  * @param  ctx: Callback context (pointer to GPRS service object)
  */
void gprsLinkStatusChanged (ppp_pcb* pcb, int err_code, void* ctx)
{
    CMServiceGprs* service = (CMServiceGprs*) ctx;
    service->onStatusChanged(err_code);
}


/**
  * @brief Constructor
  * @param port: Used port
  */
CMServiceGprs::CMServiceGprs (CModemPort* port) :
    CMService(E_MSERVICE_GPRS, port),
    m_pppDesc(NULL)
{
}


/**
  * @brief  Service initialization
  * @param  params: Service parameters
  * @return 0 or negative error code
  */
int CMServiceGprs::Open (const void* params)
{
    if (!params) return -1;
    const TGprsCfg* cfg = (const TGprsCfg*) params;
    
    if (!m_pppDesc)
    {
        m_pppDesc = pppos_create(&m_netif, gprsSendPacket, gprsLinkStatusChanged, this);
        if (!m_pppDesc) return -5;
        m_netif.flags |= NETIF_FLAG_LINK_UP;
    }
    
    if (m_pppDesc->phase == PPP_PHASE_RUNNING)
    {
        m_pPort->ToData();
        return 0;
    }
    
    int result;
    m_pPort->Lock(m_id);
    
    do
    {
        // Set APN
        static const char format[] = "AT+CGDCONT=1,\"IP\",\"%s\"";
        uint buffSize = sizeof(format) + strlen(cfg->apn);
        
        char* buff = (char*) malloc(buffSize);
        if (!buff)
        {
            result = -2;
            break;
        }
        
        uint len = sprintf(buff, buffSize, format, cfg->apn);
        buff[len] = '\0';
        
        result = m_pPort->SendAT(buff);
        free(buff);
        if (result < 0)
        {
            uint error = m_pPort->GetError();
            // Restart the modem if there is a problem with SIM
            if (error >= CMODEM_CME_ERR_SIM_NOT_INSERT && error <= CMODEM_CME_ERR_SIM_PUK2_REQUIRED)
            {
                CModem* pModem = m_pPort->GetModem();
                m_pPort->Unlock(m_id);
                pModem->Ioctl(IOCTL_MODEM_RESTART);
                KThread::sleep(2000);
            }
            break;
        }
        
        // APN activation
        {
            char status[12];
            result = m_pPort->SendAT("ATD*99***1#", "OK", status, sizeof(status), CMODEM_GPRS_CONNECT_TIMEOUT * 1000);
            if (result < 0) break;
            
            // State changing is handled in processUrc()
            if ( !strstr(status, "CONNECT") )
            {
                result = -1;
                break;
            }
        }
        
        m_pPort->SetService(E_MSERVICE_GPRS);
        
        // PPP setup
        pppapi_set_auth(m_pppDesc, PPPAUTHTYPE_PAP | PPPAUTHTYPE_CHAP, cfg->login, cfg->password);
        err_t error = pppapi_connect(m_pppDesc, 0);
        if (error != ERR_OK)
        {
            result = error;
            break;
        }
        
        // Waiting for PPP session setup
        timeStart = Kernel::GetJiffies();
        result = -3;
        
        while ( !IsTimeout(timeStart, CMODEM_GPRS_CONNECT_TIMEOUT * 1000) )
        {
            if (m_pppDesc->phase == PPP_PHASE_RUNNING)
            {
                result = 0;
                break;
            }
            
            KThread::sleep(CMODEM_IO_DELAY);
        }
        if (result) break;
        
        // Set current network interface as default to be able to sent DNS requests
        netif_set_default(&m_netif);
        
    } while (0);
    
    if (result)
    {
        m_pPort->ToCmd();
    }
    
    m_pPort->Unlock(m_id);
    
    return result;
}


/**
  * @brief  Service deinitialization
  * @param  None
  * @return 0 or negative error code
  */
int CMServiceGprs::Close (void)
{
    if (!m_pppDesc) return 0;
    
    int result;
    
    m_pPort->Lock(m_id);
    
    do
    {
        if (m_pppDesc->phase != PPP_PHASE_DEAD)
        {
            if (pppapi_close(m_pppDesc, 0) != ERR_OK)
            {
                result = -1;
                break;
            }
            
            // Delay for stopping PPP session
            KThread::sleep(1 * 1000);
        }
        
        m_pPort->ToCmd();
    
        result = m_pPort->SendAT("ATH");
        if (result < 0) break;
        
        result = m_pPort->SendAT("AT+CGACT=0");
        if (result < 0) break;
        
        if (netif_default == &m_netif) netif_set_default(NULL);
    } while (0);
    
    m_pPort->Unlock(m_id);
    
    return result;
}


/**
  * @brief  Read data
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Size of data written into the buffer or negative error code
  */
int CMServiceGprs::Read (void* buff, uint size)
{
    // Direct reading isn't supported
    return -1;
}


/**
  * @brief  Write data
  * @param  data: Data for writing
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CMServiceGprs::Write (void* data, uint size)
{
    int result;
    
    m_pPort->Lock(m_id);
    m_pPort->ToData();
    
    result = m_pPort->Write(data, size);
        
    m_pPort->Unlock(m_id);
    
    return (result < 0) ? result : size;
}


/**
  * @brief  Extended command handling
  * @param  code: Command code
  * @param  arg: Command arguments
  * @return Processeing result (see IOCTL_NOT_xx)
  */
int CMServiceGprs::Ioctl (uint code, void* arg)
{
    switch (code)
    {
        case IOCTL_MODEM_GPRS_GET_ADDR:
        if (arg)
        {
            TNetAddr* pAddr = (TNetAddr*) arg;
            pAddr->family = NET_FAMILY_INET;
            pAddr->ip.addr = ( netif_is_up(&m_netif) ) ? m_netif.ip_addr.addr : 0;
        }
        else
        {
            return IOCTL_ERROR;
        }
        break;
        
        default: return CMService::Ioctl(code, arg);
    }
    
    return IOCTL_OK;
}


/**
  * @brief  Incoming data processing
  * @param  data: Received data
  * @param  size: Data size
  * @return None
  */
void CMServiceGprs::onDataReceive (void* data, uint size)
{
    pppos_input(m_pppDesc, (u8_t*)data, size);
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CMServiceGprs::processUrc (const char* str)
{
    // FIXME: abort processing
    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  PPP state changing handler
  * @param  code: Error code
  */
void CMServiceGprs::onStatusChanged (int code)
{
    switch(code)
    {
        case PPPERR_NONE:           kdebug("PPP: connected"); break;
        case PPPERR_PARAM:          kdebug("PPP: Invalid parameter"); break;
        case PPPERR_OPEN:           kdebug("PPP: Unable to open PPP session"); break;
        case PPPERR_DEVICE:         kdebug("PPP: Invalid I/O device for PPP"); break;
        case PPPERR_ALLOC:          kdebug("PPP: Unable to allocate resources"); break;
        case PPPERR_USER:           kdebug("PPP: User interrupt"); break;
        case PPPERR_CONNECT:        kdebug("PPP: Connection lost"); break;
        case PPPERR_AUTHFAIL:       kdebug("PPP: AUTH failed"); break;
        case PPPERR_PROTOCOL:       kdebug("PPP: Failed to meet protocol"); break;
        case PPPERR_PEERDEAD:       kdebug("PPP: Connection timeout"); break;
        case PPPERR_IDLETIMEOUT:    kdebug("PPP: Idle Timeout"); break;
        case PPPERR_CONNECTTIME:    kdebug("PPP: Max connect time reached"); break;
        case PPPERR_LOOPBACK:       kdebug("PPP: Loopback detected"); break;
        default:                    kdebug("PPP: Unknown error"); break;
    }
}
