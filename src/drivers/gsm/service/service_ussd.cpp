//*****************************************************************************
//
// @brief   GSM modem USSD service
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/service/service_ussd.h"
#include "drivers/gsm/service/pdu.h"
#include "drivers/gsm/modem.h"
#include "kernel/kdebug.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/string.h"
#include <string.h>
#include <ctype.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------
    
    #if defined(CFG_MSERVICE_USSD_QUEUE_SIZE)
        #define MSERVICE_USSD_QUEUE_SIZE                    CFG_MSERVICE_USSD_QUEUE_SIZE
    #else
        #define MSERVICE_USSD_QUEUE_SIZE                    2
    #endif

    // Response timeout (ms)
    #define MSERVICE_USSD_TIMEOUT                           15000


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("ussd", "USSD", NULL, NULL);
    REGISTER_GSM_SERVICE(E_MSERVICE_USSD, createService);


/**
  * @brief  Object service creation
  * @param  id: GSM modem service ID
  * @param  portList: Modem port list
  * @param  listSize: Number of ports in the list
  * @return Pointer to object of service or NULL on error
  */
CMService* createService (TMServiceId id, CModemPort* const* portList, uint listSize)
{
    CModemPort* port = (listSize > 1) ? portList[1] : portList[0];
    return (id == E_MSERVICE_USSD && listSize && portList && port)
            ? new CMServiceUssd(port)
            : NULL;
}


/**
  * @brief Constructor
  * @param port: Used port
  */
CMServiceUssd::CMServiceUssd (CModemPort* port) :
    CMService(E_MSERVICE_USSD, port),
    m_qResp( MSERVICE_USSD_QUEUE_SIZE, sizeof(char*) )
{
    assert(!port);
}


/**
  * @brief Destructor
  * @param None
  */
CMServiceUssd::~CMServiceUssd ()
{
    Close();
}


/**
  * @brief  Service initialization
  * @param  params: Service parameters
  * @return 0 or negative error code
  */
int CMServiceUssd::Open (const void* params)
{
    return 0;
}


/**
  * @brief  Service deinitialization
  * @param  None
  * @return 0 or negative error code
  */
int CMServiceUssd::Close (void)
{
    m_pPort->Lock(m_id);
    {
        if ( m_pPort->ToCmd() )
        {
            m_pPort->SendAT("AT+CUSD=2");
        }
    }
    m_pPort->Unlock(m_id);
    
    // Release allocated memory
    while ( m_qResp.Count() )
    {
        char* ptr = NULL;
        int res = m_qResp.Get(&ptr, MSERVICE_USSD_TIMEOUT);
        if (res < 0 || !ptr) break;
        free(ptr);
    }
    
    return 0;
}


/**
  * @brief  Read data
  * @param  buff: Buffer for read data
  * @param  size: Buffer size
  * @return Size of data written into the buffer or negative error code
  */
int CMServiceUssd::Read (void* buff, uint size)
{
    if ( !m_qResp.Count() ) return 0;
    
    char* ptr = NULL;
    int len = 0;
    
    do
    {
        int res = m_qResp.Get(&ptr, MSERVICE_USSD_TIMEOUT);
        if (res < 0 || !ptr) break;
        
        len = strlen(ptr);
        if (len >= size) len = size - 1;
        
        memcpy(buff, ptr, len);
        *((char*)buff + len) = '\0';
    } while (0);
    
    if (ptr) free(ptr);

    return (len <= 0) ? -1 : len + 1;
}


/**
  * @brief  Write data
  * @param  data: Data for writing
  * @param  size: Data size
  * @return Processed data size or negative error code
  */
int CMServiceUssd::Write (const void* data, uint size)
{
    if (!data || !size) return -1;
    
    m_pPort->Lock(m_id);
    int result = -2;
    
    do
    {
        if ( !m_pPort->ToCmd() ) break;
        
        const char ussdReqFormat[] = "AT+CUSD=1,\"\"";
        const uint buffSize = sizeof(ussdReqFormat) + size;
        
        char* buff = (char*) malloc(buffSize);
        if (!buff) break;
        
        memcpy(buff, ussdReqFormat, sizeof(ussdReqFormat) - 2);
        memcpy(&buff[sizeof(ussdReqFormat) - 2], data, size);
        buff[sizeof(ussdReqFormat) - 2 + size] = '"';
        buff[sizeof(ussdReqFormat) - 2 + size + 1] = '\0';
        
        result = m_pPort->SendAT(buff, "OK", MSERVICE_USSD_TIMEOUT);
        free(buff);
        if (result < 0) break;
        
        result = (int)size;
    } while (0);
    
    m_pPort->Unlock(m_id);

    return result;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CMServiceUssd::processUrc (const char* str)
{
    // USSD response: +CUSD: 0,"_response_",72
    const char* ptr = strstr(str, "+CUSD:");
    if (!ptr) return CMODEM_URC_RES_NONE;
    
    if (*ptr == ' ') ++ptr;
    
    do
    {
        // Start of string in quotes detection and length calculation
        const char* pData = strchr(ptr, '"');
        if (!pData) break;
        ++pData;
        
        ptr = strchr(pData, '"');
        if (!ptr || ptr == pData) break;
        
        uint sLen = ptr - pData;
        if (!sLen) break;
        
        // Text coding scheme detection
        ++ptr;  // Move to ','
        if (*ptr == '\0') break;
        ++ptr;
        if (*ptr == '\0') break;
        if (*ptr == ' ') ++ptr;
        uint dcs = atoi(ptr);
        
        // Buffer for response allocation
        char* buff = (char*) malloc(sLen + 1);
        if (!buff)
        {
            kdebug("buff alloc ERROR");
            break;
        }
        
        memcpy(buff, pData, sLen);
        buff[sLen] = '\0';
        
        int res = decode(buff, dcs);
        if (res < 0)
        {
            free(buff);
            kdebug("decode ERROR");
            break;
        }
        
        if ( m_qResp.IsFull() )
        {
            free(buff);
            kdebug("queue OVERFLOW");
        }
        else
        {
            m_qResp.Add(&buff);
        }
    } while (0);
    
    return CMODEM_URC_RES_OK;
}


/**
  * @brief  USSD response decoding
  * @param  str: USSD response in PDU format
  * @param  dcs: Data Coding Scheme
  * @return 0 or negative error code
  */
int CMServiceUssd::decode (char* str, uint dcs)
{
    if (!str || !*str) return -1;
    
    // See https://en.wikipedia.org/wiki/Data_Coding_Scheme
    uint dataType = (dcs >> 2) & 0x03;
    if (dataType == 3) return -2;
    
    // Hex to binary conversion
    int size = HexToRaw(str);
    if (size <= 0) return -3;
    
    switch (dataType)
    {
        // 7 bit
        case 0:
        {
            uint count = (size * 8) / 7;
            char* tmpBuff = (char*) malloc(count);
            if (!tmpBuff) return -4;
            
            int res = PDU::Decode7bit(str, count, tmpBuff);
            if (!res)
            {
                memcpy(str, tmpBuff, count);
                str[count] = '\0';
            }
            
            free(tmpBuff);
            
            if (res < 0) return -5;
        }
        break;
        
        // 8 bit
        case 1:
            // Data already converted
            str[size] = '\0';
        break;
        
        // UCS2 (UNICODE)
        case 2:
            // Big endian -> Little endian
            BeToLeWord(str, size);
            // Unicode to single byte charset (windows-1251) conversion
            size = UnicodeToWin1251(str, size, str);
            if (size <= 0) return -6;
            str[size] = '\0';
        break;
    }
    
    return 0;
}
