//*****************************************************************************
//
// @brief   PDU base functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/service/pdu.h"
#include "kernel/kdebug.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("pdu", "PDU", NULL, NULL);


/**
  * @brief  7-bit data decoding
  * @param  pdu: PDU data
  * @param  count: Number of packed symbols (7-bits symbols)
  * @param  buff: Buffer for decoded data. Size must be at least equal `count` bytes
  * @return 0 or negative error code
  */
int PDU::Decode7bit (const char* pdu, uint count, char* buff)
{
    if (!pdu || !buff) return -1;
    
    uint16 container = 0;
    uint shiftCount = 0;
    
    while (count)
    {
        shiftCount &= 0x07;
        container |= (uint16)(*pdu) << shiftCount;
        ++pdu;
        
        do
        {
            *buff = container & 0x7f;
            ++buff;
            ++shiftCount;
            --count;
            container >>= 7;
        } while (shiftCount == 7 && count);
    }
    
    return 0;
}



/**
  * @brief  7-bit data encoding
  * @param  data: Data to encode
  * @param  size: Data size
  * @param  buff: Buffer for encoded data
  * @param  buffSize: Buffer size
  * @return Data size written into the buffer (encoded data) or negative error code
  */
int PDU::Encode7bit (const char* data, uint size, char* buff, uint buffSize)
{
    if (!data || !size || !buff) return -1;
    
    // Encoded data size calculation
    uint encodedSize = (size * 7 + 7) / 8;
    if (buffSize < encodedSize) return -2;
    
    uint16 container = 0;
    uint shiftCount = 0;
    
    while (size--)
    {
        char c = *data++;
        if (c & 0x80) return -3;
        
        container |= (uint16)c << shiftCount;
        
        if (shiftCount)
        {
            *buff++ = container & 0xff;
            container >>= 8;
        }
        
        --shiftCount;
        shiftCount &= 0x07;
    }
    
    // Save last byte
    *buff = container & 0xff;
    
    return encodedSize;
}


/**
  * @brief  Phone number in PDU format decoding
  * @param  pdu: PDU data
  * @param  pduSize: PDU data size
  * @param  buff: Buffer for decoded phone number (string)
  * @return 0 or negative error code
  */
int PDU::DecodePhone (const char* pdu, uint pduSize, char* buff)
{
    // Data format: 0B 91 9731080893F5 
    // recepiend phone number length (symbols), phone number type, phone number (12 symbols): +79138080395
    
    if (!pdu || !buff) return -1;
    
    buff[0] = '\0';
    
    char numLen = pdu[0];
    if (!numLen) return 0;
    
    char numDataSize = (numLen + 1) >> 1;
    if (numDataSize >= pduSize || numDataSize > 12) return -2;
    
    TPduAddrType numType = { .val = pdu[1] };
    if (!numType.prefix) return -3;
    
    if (
        numType.set != E_PDU_NUMSET_ISDN &&
        (numType.type != E_PDU_NUMTYPE_ALPHA)
    ) {
        kdebug("unknown phone type 0x%.2x", numType.val);
        return -4;
    }
    
    if (numType.type == E_PDU_NUMTYPE_ALPHA)
    {
        // Conversion half octets length to number of packed symbols
        numLen = numLen * 4 / 7;
        int res = Decode7bit(&pdu[2], numLen, buff);
        if (res < 0) return -5;
        
        // Operator might add 0x0d at the end of the string
        if (buff[numLen - 1] == 0x0d)   buff[numLen - 1] = '\0';
        else                            buff[numLen] = '\0';
    }
    else
    {
        if (numLen > 15) return -4;
        
        char tmp[16];
        memcpy(tmp, pdu + 2, numDataSize);
        
        // Convert phone number to string
        for (uint i = 0; i < numLen; ++i)
        {
            char c = (i & 0x01) ? tmp[i >> 1] >> 4 : tmp[i >> 1] & 0x0f;
            
            if (c < 10)         c += '0';
            else if (c == 0x0a) c = '*';
            else if (c == 0x0b) c = '#';
            else                c = c - 0x0c + 'a';
            
            buff[i] = c;
        }
        
        buff[numLen] = '\0';
    }
    
    return 0;
}


/**
  * @brief  Convert phone number to PDU format
  * @param  pdu: Start of PDU data block (starts with length)
  * @param  pduSize: PDU buffer size
  * @param  phone: Phone number
  * @return Size of written PDU data or negative error code
  */
int PDU::EncodePhone (char* pdu, uint pduSize, const char* phone)
{
    if (!pdu || pduSize < 3 || !phone || !*phone) return -1;
    
    // Phone number string to binary format conversion (with nibbles swapping)
    const char* p = phone;
    char* wrPtr = &pdu[2];
    uint phoneLen = 0;
    
    if (*p == '+')
    {
        ++p;
    }
    else if (*p == '8')
    {
        wrPtr[0] = 7;
        ++phoneLen;
    }
    
    // Max phone number length is 15 symbols
    while (*p && (2 + ((phoneLen + 1) >> 1)) < pduSize && phoneLen < 16)
    {
        char c = *p - '0';
        if (c > 9) return -3;
        if (phoneLen & 0x01)    wrPtr[phoneLen >> 1] |= c << 4;
        else                    wrPtr[phoneLen >> 1] = c;
        ++phoneLen;
        ++p;
    }
    
    // Mark unused nibbles
    if (phoneLen & 0x01) wrPtr[phoneLen >> 1] |= 0xf0;
    
    pdu[0] = phoneLen;
    TPduAddrType* pType = (TPduAddrType*) &pdu[1];
    pType->prefix = 1;
    pType->type = E_PDU_NUMTYPE_INTERNATIONAL;
    pType->set = E_PDU_NUMSET_ISDN;
    
    // Length + type + data
    return 2 + ((phoneLen + 1) >> 1);
}
