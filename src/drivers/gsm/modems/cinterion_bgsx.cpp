//*****************************************************************************
//
// @brief   GSM modem Cinterion BGS1/BGS2 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/modems/cinterion_bgsx.h"
#include "drivers/gsm/modem_port.h"
#include "arch/usart.h"
#include "kernel/kdebug.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <ctype.h>
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Power off process timeout (sec)
    #define BGSX_POWEROFF_TIMEOUT               10

    // Active mode timeout (sec)
    #define BGSX_WAKEUP_TIMEOUT                 2

    // Pins logic state
    #define STATE_ON                            true
    #define STATE_OFF                           false


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Attach debug output to modem debug output
    ATTACH_DEBUG(CMODEM_DEBUG_NAME);


/**
  * @brief Constructor
  * @param portList: Modem port list
  * @param listSize: Number of ports in the list
  */
CGsmCinterionBgsx::CGsmCinterionBgsx (CModemPort* const* portList, uint listSize) :
    CModem(portList, listSize)
{
    m_devState = DEV_CLOSED;
    m_timeWakeup = 0;
}


/**
  * @brief  Set configuration parameters
  * @param  params: Configuration parameters
  * @return DEV_OK or negative error code
  */
int CGsmCinterionBgsx::Configure (const void* params)
{
    m_pCfg = (const TBgsCfg*) params;
    
    return CModem::Configure(&m_pCfg->baseCfg);
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CGsmCinterionBgsx::Open (void)
{
    if (m_devState == DEV_OPENED) return DEV_OK;
    
    m_devState = DEV_OPENING;
    
    m_pCfg->initGpio();
    
    m_pCfg->setPwr(STATE_OFF);
    m_pCfg->setReset(STATE_ON);
    m_pCfg->setPwrkey(STATE_OFF);
    if (m_pCfg->setRts_0) m_pCfg->setRts_0(STATE_OFF);
    if (m_pCfg->setRts_1) m_pCfg->setRts_1(STATE_OFF);
    
    // Powering on
    sleep(500);
    m_pCfg->setPwr(STATE_ON);
    m_pCfg->setReset(STATE_OFF);
    sleep(200);
    m_pCfg->setPwrkey(STATE_ON);
    sleep(4000);
    m_pCfg->setPwrkey(STATE_OFF);
    sleep(2000);
    
    // Software initialization
    int result = softInit();
    if (result)
    {
        m_devState = DEV_CLOSED;
        return DEV_ERR;
    }
    m_devState = DEV_OPENED;
    
    // Showing signal level
    int csq = 0;
    result = CModem::ioctlGetCsq(&csq);
    if (result == IOCTL_OK)
    {
        kinfo("CSQ: %d", csq);
    }
    
    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CGsmCinterionBgsx::Close (void)
{
    m_devState = DEV_CLOSING;
    
    // Modem power off (AT^SMSO ->  URC ^SHUTDOWN)
    CModemPort* port = m_portList[0];
    port->Lock(MPORT_SERVICE_ID_INTERNAL);
    
    port->SendAT("AT^SMSO");
    
    uint32 timeStart = Kernel::GetJiffies();
    while ( m_devState != DEV_CLOSED && IsTimeout(timeStart, BGSX_POWEROFF_TIMEOUT * 1000) )
    {
        sleep(250);
    }
    
    port->Unlock(MPORT_SERVICE_ID_INTERNAL);
    
    CModem::Close();
    
    if (m_pCfg->setRts_0) m_pCfg->setRts_0(STATE_OFF);
    if (m_pCfg->setRts_1) m_pCfg->setRts_1(STATE_OFF);
    m_pCfg->setReset(STATE_ON);
    m_pCfg->setPwr(STATE_OFF);
    
    m_devState = DEV_CLOSED;
    
    return DEV_OK;
}


/**
  * @brief  Advanced device control
  * @param  code: Command code
  * @param  arg: Pointer to command arguments
  * @return Operation result (see IOCTL_xx)
  */
int CGsmCinterionBgsx::Ioctl (uint code, void* arg)
{
    int result = IOCTL_OK;
    
    switch (code)
    {
        case IOCTL_MODEM_RESTART:           result = ioctlDoRestart(); break;
        case IOCTL_MODEM_PORT_ACTIVATE:     result = ioctlCtrlPort(arg, true); break;
        case IOCTL_MODEM_PORT_DEACTIVATE:   result = ioctlCtrlPort(arg, false); break;
        default: return CModem::Ioctl(code, arg);
    }
    
    return result;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CGsmCinterionBgsx::processUrc (char* str)
{
    int result = CModem::processUrc(str);
    if (result != CMODEM_URC_RES_NONE) return result;
    
    // Power off
    if ( strstr(str, "^SHUTDOWN") )
    {
        m_devState = DEV_CLOSED;
        return CMODEM_URC_RES_OK;
    }
    
    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  Basic modem and ports initialization
  * @param  None
  * @return 0 or negative error code
  */
int CGsmCinterionBgsx::softInit (void)
{
    int result = DEV_ERR;
    
    // Ports initialization
    for (uint i = 0; i < m_portCount; ++i)
    {
        CModemPort* port = m_portList[i];
        
        port->Lock(MPORT_SERVICE_ID_INTERNAL);
        port->Open();
        sleep(50);
        
        do
        {
            uint tryCount = 5;
            while (tryCount--)
            {
                result = port->SendAT("AT");
                if (!result) break;
                sleep(CMODEM_IO_DELAY);
            }
            if (result < 0) break;
            
            // Turned off because some modems (rev2) stop answering after this command
//            result = port->SendAT("AT+IPR=115200");
//            if (result < 0) break;
//            sleep(250);

            result = port->SendAT("AT&D0E0V1");
            if (result < 0) break;
            
            // If there is at least one RTS pin, turning on sleep mode
            if (m_pCfg->setRts_0 || m_pCfg->setRts_1)
            {
                result = port->SendAT("AT+CFUN=9");
                if (result < 0) break;
            }
        } while (0);
        
        port->Unlock(MPORT_SERVICE_ID_INTERNAL);
        
        if (result < 0) return result;
    } // for (uint i = 0; i < m_portCount; i++)
    
    // Basic initialization
    result = CModem::Open();
    
    return result;
}


/**
  * @brief  IOCTL_MODEM_RESTART command handler
  * @return Operation result (see IOCTL_xx)
  */
int CGsmCinterionBgsx::ioctlDoRestart (void)
{
    m_devState = DEV_OPENING;
    
    do
    {
        // Software reset
        int result = CModem::ioctlDoRestart();
        if (result == IOCTL_OK) break;
        
        // Hardware reset
        m_pCfg->setReset(STATE_ON);
        sleep(500);
        m_pCfg->setReset(STATE_OFF);
    } while (0);
    
    // Waiting for the end of rebooting process
    sleep(4000);
    
    // First time initialization
    int result = softInit();
    m_devState = (result == DEV_OK) ? DEV_OPENED : DEV_CLOSED;
    
    return IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_PORT_ACTIVATE and IOCTL_MODEM_PORT_DEACTIVATE commands handler
  * @param  port: Pointer to port object (CModemPort)
  * @param  state: RTS pin logical state (see STATE_xx)
  * @return Operation result (see IOCTL_xx)
  */
int CGsmCinterionBgsx::ioctlCtrlPort (void* port, bool state)
{
    int result = IOCTL_ERROR;
    
    do
    {
        if (m_portList[0] == port)
        {
            if (!m_pCfg->setRts_0) break;
            m_pCfg->setRts_0(state);
        }
        else if (m_portCount > 1 && m_portList[1] == port)
        {
            if (!m_pCfg->setRts_1) break;
            m_pCfg->setRts_1(state);
        }
        else
        {
            break;
        }
        
        // Waiting for modem's quit from sleep mode if it was activated after the end of active mode timeout
        if ( state && IsTimeout(m_timeWakeup, BGSX_WAKEUP_TIMEOUT * 1000) )
        {
            // Exit from sleep mode migh take up to 60 ms (see Hardware Interface Description)
            uint32 startTime = Kernel::GetJiffies();
            bool tmpState = state;
            
            // Multiple RTS pin activation to increase chance on wake up
            while ( !IsTimeout(startTime, 500) )
            {
                tmpState = !tmpState;
                if (m_portList[0] == port)  m_pCfg->setRts_0(tmpState);
                else                        m_pCfg->setRts_1(tmpState);
                KThread::sleep(20);
            }
            
            if (tmpState != state)
            {
                if (m_portList[0] == port)  m_pCfg->setRts_0(state);
                else                        m_pCfg->setRts_1(state);
            }
        }
        
        m_timeWakeup = Kernel::GetJiffies();
        
        result = IOCTL_OK;
    } while (0);
    
    return result;
}
