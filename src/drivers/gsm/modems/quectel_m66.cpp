//*****************************************************************************
//
// @brief   GSM modem Quectel M66 driver
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "drivers/gsm/modems/quectel_m66.h"
#include "drivers/gsm/service/service_voice.h"
#include "arch/usart.h"
#include "kernel/kdebug.h"
#include "kernel/kstdio.h"
#include "kernel/events.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>
#include <stdlib.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Power off process timeout (sec)
    #define M66_POWEROFF_TIMEOUT                12

    // Pin's logic states
    #define STATE_ON                            true
    #define STATE_OFF                           false


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Attach debug output to modem debug output
    ATTACH_DEBUG(CMODEM_DEBUG_NAME);


/**
  * @brief Constructor
  * @param portList: Modem port list
  * @param listSize: Number of ports in the list
  */
CGsmQuectelM66::CGsmQuectelM66 (CModemPort* const* portList, uint listSize) :
    CModem(portList, listSize),
    m_pCfg(NULL),
    m_devState(DEV_CLOSED)
{
}


/**
  * @brief  Open device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CGsmQuectelM66::Open (void)
{
    if (m_devState == DEV_OPENED) return DEV_OK;
    
    assert(!m_pCfg);
    
    m_devState = DEV_OPENING;
    m_flags.val = 0;
    MODEM_NOTIFY_STATE(this, m_devState);
    
    m_pCfg->initGpio();
    
    m_pCfg->setPwr(STATE_OFF);
    m_pCfg->setPwrkey(STATE_OFF);
    if (m_pCfg->setDtr) m_pCfg->setDtr(STATE_OFF);

    // Powering on
    sleep(200);
    m_pCfg->setPwr(STATE_ON);
    sleep(200);
    m_pCfg->setPwrkey(STATE_ON);
    sleep(1200);
    m_pCfg->setPwrkey(STATE_OFF);
    sleep(2000);
    
    // Software initialization
    int result = softInit();
    if (result)
    {
        m_devState = DEV_CLOSED;
        MODEM_NOTIFY_STATE(this, m_devState);
        return DEV_ERR;
    }
    
    m_devState = DEV_OPENED;
    MODEM_NOTIFY_STATE(this, m_devState);
    
    // Showing signal level
    int csq = 0;
    result = CModem::ioctlGetCsq(&csq);
    if (result == IOCTL_OK)
    {
        kinfo("CSQ: %d", csq);
    }
    
    return DEV_OK;
}


/**
  * @brief  Close device
  * @param  None
  * @return DEV_OK or negative error code
  */
int CGsmQuectelM66::Close (void)
{
    m_devState = DEV_CLOSING;
    MODEM_NOTIFY_STATE(this, m_devState);
    
    // Power off (AT+QPOWD=1 -> URC: NORMAL POWER DOWN)
    CModemPort* port = m_portList[0];
    port->Lock(MPORT_SERVICE_ID_INTERNAL);
    
    port->SendAT("AT+QPOWD=1");
    
    uint32 timeStart = Kernel::GetJiffies();
    while ( m_devState != DEV_CLOSED && IsTimeout(timeStart, M66_POWEROFF_TIMEOUT * 1000) )
    {
        sleep(250);
    }
    
    port->Unlock(MPORT_SERVICE_ID_INTERNAL);
    
    CModem::Close();
    
    if (m_pCfg->setDtr) m_pCfg->setDtr(STATE_OFF);
    m_pCfg->setPwr(STATE_OFF);
    
    m_flags.val = 0;
    m_devState = DEV_CLOSED;
    MODEM_NOTIFY_STATE(this, m_devState);
    
    return DEV_OK;
}


/**
  * @brief  Set configuration parameters
  * @param  params: Configuration parameters (see TM66Cfg)
  * @return DEV_OK or negative error code
  */
int CGsmQuectelM66::Configure (const void* params)
{
    m_pCfg = (const TM66Cfg*) params;
    if (!m_pCfg->initGpio || !m_pCfg->setPwr || !m_pCfg->setPwrkey) return -1;
    
    return CModem::Configure(&m_pCfg->baseCfg);
}


/**
  * @brief  Advanced device control
  * @param  code: Command code
  * @param  arg: Pointer to command arguments
  * @return Operation result (see IOCTL_xx)
  */
int CGsmQuectelM66::Ioctl (uint code, void* arg)
{
    int result = IOCTL_OK;
    
    switch (code)
    {
        case IOCTL_MODEM_RESTART:           result = ioctlDoRestart(); break;
        case IOCTL_MODEM_PORT_ACTIVATE:     result = ioctlCtrlPort(arg, true); break;
        case IOCTL_MODEM_PORT_DEACTIVATE:   result = ioctlCtrlPort(arg, false); break;
        case IOCTL_MODEM_SET_LEVEL_MIC:     result = ioctlSetLevelMic( (size_t)arg ); break;
        default: return CModem::Ioctl(code, arg);
    }
    
    return result;
}


/**
  * @brief  Asynchronous URC processing
  * @param  str: URC string
  * @return Processing result (see CMODEM_URC_RES_xx)
  */
int CGsmQuectelM66::processUrc (char* str)
{
    int result = CModem::processUrc(str);
    if (result != CMODEM_URC_RES_NONE) return result;
    
    // Power off
    if ( strstr(str, "POWER DOWN") )
    {
        m_devState = DEV_CLOSED;
        return CMODEM_URC_RES_OK;
    }
    
    if ( !strcmp(str, "Call Ready") )
    {
        m_flags.callReady = 1;
        return CMODEM_URC_RES_OK;
    }
    
    if ( !strcmp(str, "SMS Ready") )
    {
        m_flags.smsReady = 1;
        return CMODEM_URC_RES_OK;
    }
    
    // Build-in DTMF detector
    if ( strstr(str, "+QTONEDET") )
    {
        CMService* pService = m_serviceList[E_MSERVICE_VOICE];
        if (pService)
        {
            str += sizeof("+QTONEDET");
            if (*str == ' ') ++str;
            uint code = atoi(str);
            pService->Ioctl(IOCTL_VOICE_MODEM_DETECT_DTMF, (void*)code);
        }
        return CMODEM_URC_RES_OK;
    }
    
    return CMODEM_URC_RES_NONE;
}


/**
  * @brief  Basic modem and ports initialization
  * @param  None
  * @return 0 or negative error code
  */
int CGsmQuectelM66::softInit (void)
{
    int result = DEV_ERR;
    
    // Ports initialization
    for (uint i = 0; i < m_portCount; ++i)
    {
        CModemPort* port = m_portList[i];
        
        port->Lock(MPORT_SERVICE_ID_INTERNAL);
        port->Open();
        sleep(50);
        
        do
        {
            uint tryCount = 5;
            while (tryCount--)
            {
                result = port->SendAT("AT");
                if (!result) break;
                sleep(CMODEM_IO_DELAY);
            }
            if (result < 0) break;
            
            result = port->SendAT("AT+IPR=115200;+IFC=2,2;");
            if (result < 0) break;
            
            sleep(250);

            result = port->SendAT("AT&D0E0Q0V1;+QGPCLASS=8;");
            if (result < 0) break;
            
            result = port->SendAT("AT+QTONEDET=1");
            if (result < 0) break;
            
            // If there is DTR pin, sleep mode activation
            if (m_pCfg->setDtr)
            {
                result = port->SendAT("AT+QSCLK=1");
                if (result < 0) break;
            }
            
            // Pin RFTX activation (transmitter activity)
            result = port->SendAT("AT+QCFG=\"RFTXburst\",1");
        } while (0);
        
        port->Unlock(MPORT_SERVICE_ID_INTERNAL);
        
        if (result < 0) return result;
    } // for (uint i = 0; i < m_portCount; i++)
    
    result = CModem::Open();
    
    // Waiting for Call Ready / SMS Ready after successful network registration
    if (result == DEV_OK)
    {
        uint32 timeStart = Kernel::GetJiffies();
        while ( !IsTimeout(timeStart, CMODEM_REG_TIMEOUT * 1000) )
        {
            // SMS Ready is present not in all modems revisions
            if (m_flags.callReady || m_flags.smsReady) break;
            sleep(500);
        }
    }
    
    return result;
}


/**
  * @brief  IOCTL_MODEM_RESTART command handler
  * @return Operation result (see IOCTL_xx)
  */
int CGsmQuectelM66::ioctlDoRestart (void)
{
    m_devState = DEV_OPENING;
    m_flags.val = 0;
    
    do
    {
        // Software reset
        int result = CModem::ioctlDoRestart();
        if (result == IOCTL_OK) break;
        
        // Hardware reset
        m_pCfg->setPwrkey(STATE_ON);
        sleep(800);
        m_pCfg->setPwrkey(STATE_OFF);

        uint32 timeStart = Kernel::GetJiffies();
        while ( m_devState != DEV_CLOSED && IsTimeout(timeStart, M66_POWEROFF_TIMEOUT * 1000) )
        {
            sleep(250);
        }
        
        sleep(500);
        
         m_pCfg->setPwrkey(STATE_ON);
        sleep(1200);
        m_pCfg->setPwrkey(STATE_OFF);
    } while (0);
    
    // Waiting for the end of rebooting process
    sleep(4000);
    
    int result = softInit();
    m_devState = (result == DEV_OK) ? DEV_OPENED : DEV_CLOSED;
    
    return IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_PORT_ACTIVATE and IOCTL_MODEM_PORT_DEACTIVATE commands handler
  * @param  port: Pointer to port object (CModemPort)
  * @param  state: RTS pin logical state (see STATE_xx)
  * @return Operation result (see IOCTL_xx)
  */
int CGsmQuectelM66::ioctlCtrlPort (void* port, bool state)
{
    m_pCfg->setDtr(state);
    
    if (state)
    {
        if (m_pCfg->getCts)
        {
            uint32 timeStart = Kernel::GetJiffies();
            while ( m_pCfg->getCts() && !IsTimeout(timeStart, 20) )
            {
                KThread::sleep(5);
            }
        }
        else
        {
            KThread::sleep(20);
        }
    }
    
    return IOCTL_OK;
}


/**
  * @brief  IOCTL_MODEM_SET_LEVEL_MIC command handler
  * @param  level: Microphone volume (0..100%)
  * @return Operation result (see IOCTL_xx)
  */
int CGsmQuectelM66::ioctlSetLevelMic (uint level)
{
    if (level > 100) level = 100;
    
    CModemPort* port = (m_portCount > 1) ? m_portList[1] : m_portList[0];
    if (port->GetState() != DEV_OPENED) return IOCTL_ERROR;
    
    char buff[16];
    uint gain = level * 15 / 100;   // Gain level value [0..15]
    int len = sprintf(buff, sizeof(buff), "AT+QMIC=0,%u", gain);
    assert( len <= 0 || len >= sizeof(buff) );
    buff[len] = '\0';
    
    port->Lock(MPORT_SERVICE_ID_IOCTL);
    int result = port->SendAT(buff);
    port->Unlock(MPORT_SERVICE_ID_IOCTL);
    
    return (result) ? IOCTL_ERROR : IOCTL_OK;
}
