//*****************************************************************************
//
// @brief   Simple discrete indication
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "apps/indication.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if !defined(CFG_IND_TICK_MAX_COUNT)
        #define CFG_IND_TICK_MAX_COUNT          30
    #endif

    #if (CFG_IND_TICK_MAX_COUNT > 32)
        #error "CFG_IND_TICK_MAX_COUNT must be less than 32 (mask type is uint32)"
    #endif

    #if !defined(CFG_IND_TICK_PERIOD)
        #error "Indication tick duration isn't set (CFG_IND_TICK_PERIOD)"
    #endif


/**
  * @brief  Constructor
  * @param  pDesc: Pointer to indicator descriptors
  * @param  count: Number of indicators
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
CIndication::CIndication (const TIndDesc* pDesc, uint count, const char* name, uint stackSize, uint priority) :
    KThreadWait(name, stackSize, priority),
    m_pDesc(pDesc),
    m_indCount(count),
    m_step(0),
    m_jiff(0)
{
    m_state = (TIndState*) malloc(sizeof(TIndState) * m_indCount);
    assert(!m_state);

    // Indicators initialization
    for (uint i = 0; i < m_indCount; ++i)
    {
        GPIO::Cfg(m_pDesc[i].pin, m_pDesc[i].pinCfg);
        m_state[i].mask = IND_MASK_OFF;
        m_state[i].savedMask = IND_MASK_OFF;
        m_state[i].startPos = 0;
        m_state[i].count = 0;

        // Set initial indicators state
        GPIO::Set(m_pDesc[i].pin, m_pDesc[i].flags.invert);
    }
}


/**
  * @brief  Destructor
  * @param  None
  * @return None
  */
CIndication::~CIndication ()
{
    free(m_state);
    
    for (uint i = 0; i < m_indCount; ++i)
    {
        GPIO::Cfg(m_pDesc[i].pin, GPIO_MODE_IN);
    }
}


/**
  * @brief  Thread initialization
  * @param  None
  * @return None
  */
void CIndication::run (void)
{
    for (;;)
    {
        uint32 now = Kernel::GetJiffies();
        uint stepsToSleep = (uint)-1;
        m_step = getCurrentStep();

        for (uint i = 0; i < m_indCount; ++i)
        {
            TIndState* pState = &m_state[i];
            const TIndDesc* pDesc = &m_pDesc[i];

            // State updating
            bool state = pState->mask & (1 << m_step);
            GPIO::Set(pDesc->pin, (pDesc->flags.invert) ? !state : state);

            // Indication counter processing
            uint stepUpdateCount = (pState->startPos) ? pState->startPos - 1 : CFG_IND_TICK_MAX_COUNT - 1;

            if (m_step == stepUpdateCount && pState->count && --pState->count == 0)
            {
                pState->mask = pState->savedMask;
                onBlinkEnd(i);
            }

            // Minimal sleep time calculation (detect tick on which will be changing of indicator state)
            // Start from 1 to analyze from next step
            for (uint count = 1; count <= CFG_IND_TICK_MAX_COUNT; count++)
            {
                uint step = m_step + count;
                if (step >= CFG_IND_TICK_MAX_COUNT) step -= CFG_IND_TICK_MAX_COUNT;

                if ( state != (bool)(pState->mask & (1 << step)) || (pState->count && step == stepUpdateCount) )
                {
                    if (stepsToSleep > count)
                    {
                        stepsToSleep = count;
                    }
                    break;
                }
            } // for (uint count = 1; count <= CFG_IND_TICK_MAX_COUNT; count++)
        } // for (uint i = 0; i < ARR_COUNT(m_state); i++)

        // Waiting of next update step
        if (stepsToSleep == (uint)-1)
        {
            m_step = 0;
            m_jiff = 0;
            Suspend();
        }
        else
        {
            // Save current time for proper m_step correction on wake up
            m_jiff = now;
            wait(stepsToSleep * CFG_IND_TICK_PERIOD);
        }
    } // for (;;)
}


/**
  * @brief  Set indication mask
  * @param  channel: Index of indication channel
  * @param  mask: mask value
  * @param  count: Number of replays (0 - set permanent indication)
  */
void CIndication::setMask (uint channel, uint32 mask, uint count)
{
    if (channel >= m_indCount) return;
    TIndState* pState = &m_state[channel];

    Kernel::EnterCritical();
    {
        // Make mask value relative to current position
        uint step = getCurrentStep();
        uint32 globalMask = (uint32)-1 >> (sizeof(TIndState::mask) * 8 - CFG_IND_TICK_MAX_COUNT);
        uint32 value = ((mask << step) & globalMask) | (mask >> (CFG_IND_TICK_MAX_COUNT - step));

        if (count)
        {
            // Save background mask on set temporary indication mask
            if (!pState->count) pState->savedMask = pState->mask;

            pState->mask = value;
            pState->count = count;
            pState->startPos = step;
        }
        else if (pState->count)
        {
            // Set base mask if temporary one is active
            pState->savedMask = value;
        }
        else
        {
            // Set base mask when another one is active
            pState->mask = value;
            pState->startPos = step;
        }
    }
    Kernel::ExitCritical();
}


/**
  * @brief  Read current indication step
  * @return Current indication step value
  */
uint CIndication::getCurrentStep (void)
{
    uint step = m_step;

    if (m_jiff)
    {
        uint32 now = Kernel::GetJiffies();
        step += (now - m_jiff) * KERNEL_TICK_PERIOD / CFG_IND_TICK_PERIOD;

        if (step >= CFG_IND_TICK_MAX_COUNT) step %= CFG_IND_TICK_MAX_COUNT;
    }

    return step;
}
