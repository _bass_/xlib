//*****************************************************************************
//
// @brief   Basic beeper control
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "apps/beep.h"
#include "arch/pwm.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define BEEP_ON                 true
    #define BEEP_OFF                false


/**
  * @brief  Constructor
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
CBeep::CBeep (const TBeepCfg* pCfg, uint stackSize, uint priority) :
    KThreadWait("Beep", stackSize, priority),
    m_pCfg(pCfg),
    m_pMelody(NULL),
    m_melodyIndex(0),
    m_loop(0)
{
}


/**
  * @brief  Destructor
  * @param  None
  * @return None
  */
CBeep::~CBeep ()
{
}


/**
  * @brief  Thread initialization
  * @param  None
  * @return None
  */
void CBeep::init (void)
{
    hwCtrl(BEEP_OFF);
}


/**
  * @brief  Thread main cycle 
  * @param  None
  * @return None
  */
void CBeep::run (void)
{
    for (;;)
    {
        processMelody();
        Suspend();
    }
}


/**
  * @brief  Current melody processing
  * @param  None
  * @return None
  */
void CBeep::processMelody (void)
{
    while (m_pMelody)
    {
        // Check the end of the melody
        if (!m_pMelody[m_melodyIndex].time)
        {
            if (m_loop)
            {
                // Repeat playback
                Kernel::EnterCritical();
                if (m_loop != BEEP_PLAY_ALWAYS) --m_loop;
                m_melodyIndex = 0;
                Kernel::ExitCritical();
                continue;
            }
            break;
        }
        
        if (m_pMelody[m_melodyIndex].freq)
        {
            hwCtrl(BEEP_ON);
            PWM::Configure(m_pCfg->pwmChannel, m_pMelody[m_melodyIndex].freq);
            PWM::SetDuty(m_pCfg->pwmChannel, 50);
            PWM::Start(m_pCfg->pwmChannel);
        }
        else
        {
            hwCtrl(BEEP_OFF);
            PWM::Stop(m_pCfg->pwmChannel);
        }
        
        Kernel::EnterCritical();
        uint delay = m_pMelody[m_melodyIndex].time;
        ++m_melodyIndex;
        Kernel::ExitCritical();
        
        wait(delay);
    } // while (m_pMelody)
    
    // Turn off sound at the end of playback
    hwCtrl(BEEP_OFF);
    PWM::Stop(m_pCfg->pwmChannel);
    Kernel::EnterCritical();
    m_pMelody = NULL;
    m_melodyIndex = 0;
    Kernel::ExitCritical();
}


/**
  * @brief  Beeper activation state control
  * @param  state: Set value of beeper (on/off)
  */
void CBeep::hwCtrl (bool state)
{
    if (m_pCfg->hwCtrl) m_pCfg->hwCtrl(state);
}


/**
  * @brief  Melody playback
  * @param  pMelody: Pointer to melody descriptor (the end marked by descriptor with freq = 0)
  * @param  replay: Number of replays
  */
void CBeep::Play (const TMelody* pMelody, uint replay)
{
    hwCtrl(BEEP_OFF);

    Kernel::EnterCritical();
    {
        m_pMelody = pMelody;
        m_melodyIndex = 0;
        m_loop = (replay && replay != BEEP_PLAY_ALWAYS) ? replay - 1 : replay;
    }
    Kernel::ExitCritical();
    
    Resume();
}


/**
  * @brief  Get current melody descriptor
  * @return Pointer to current melody descriptor
  */
const TMelody* CBeep::GetMelody (void) const
{
    return m_pMelody;
}
