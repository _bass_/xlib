//*****************************************************************************
//
// @brief   Mutexes implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "FreeRTOS.h"
#include "kernel/kmutex.h"
#include "kernel/kernel.h"
#include "misc/macros.h"
#include "semphr.h"


/**
  * @brief  Constructor
  * @param  None
  */
KMutex::KMutex (void)
{
    m_mutex = xSemaphoreCreateMutex();
    assert(!m_mutex);
}


/**
  * @brief  Destructor
  * @param  None
  */
KMutex::~KMutex ()
{
    if (m_mutex)
    {
        vSemaphoreDelete(m_mutex);
    }
}


/**
  * @brief  Lock mutex
  * @param  None
  */
void KMutex::Lock (void)
{
    while(!m_mutex);
    
    if (IRQ::GetActive() != IRQ_NONE)
    {
        // Mutexes mustn't be locked in IRQ
        assert(1);
    }
    else
    {
        // Lock in thread
        xSemaphoreTake(m_mutex, portMAX_DELAY);
    }
}


/**
  * @brief  Release mutex
  * @param  None
  */
void KMutex::Unlock (void)
{
    while (!m_mutex);
    
    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;

        xSemaphoreGiveFromISR(m_mutex, &needSchedule);
        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        xSemaphoreGive(m_mutex);
    }
}
