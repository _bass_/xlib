//*****************************************************************************
//
// @brief   Queue implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kqueue.h"
#include "kernel/kernel.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "misc/macros.h"


/**
  * @brief  Constructor
  * @param  len: Number of items in the queue
  * @param  itemSize: Single item size
  */
KQueue::KQueue (uint len, uint itemSize)
{
    m_handler = xQueueCreate(len, itemSize);
    assert(!m_handler);
}


/**
  * @brief  Destructor
  * @param  None
  */
KQueue::~KQueue ()
{
    if (m_handler)
    {
        vQueueDelete(m_handler);
    }
}


/**
  * @brief  Add item in the queue
  * @param  pItem: Item to add
  */
void KQueue::Add (const void* pItem)
{
    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;

        xQueueSendToBackFromISR(m_handler, pItem, &needSchedule);

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        xQueueSendToBack(m_handler, pItem, portMAX_DELAY);
    }
}


/**
  * @brief  Read item from the queue. The read value will be deleted
  * @param  pItem: Pointer to variable to save item data
  * @param  timeout: Maximum timeout of waiting data in case of empty queue
  * @return 0 or negative error code
  */
int KQueue::Get (void* pItem, uint timeout)
{
    portBASE_TYPE res;

    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;

        res = xQueueReceiveFromISR(m_handler, pItem, &needSchedule);

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        res = xQueueReceive(m_handler, pItem, (timeout == (uint)-1) ? portMAX_DELAY : timeout / KERNEL_TICK_PERIOD);
    }

    return (res == pdPASS) ? 0 : -1;
}


/**
  * @brief  Read item from the queue (without deleting item from the queue)
  * @param  pItem: Pointer to variable to save item data
  * @return 0 or negative error code
  */
int KQueue::Peek (void* pItem)
{
    portBASE_TYPE res = xQueuePeek(m_handler, pItem, 0);

    return (res == pdPASS) ? 0 : -1;
}


/**
  * @brief  Read number of items in the queue
  * @param  None
  * @return Number of items in the queue
  */
uint KQueue::Count (void)
{
    if (IRQ::GetActive() != IRQ_NONE)
    {
        return uxQueueMessagesWaitingFromISR(m_handler);
    }
    else
    {
        return uxQueueMessagesWaiting(m_handler);
    }
}


/**
  * @brief  Check if the queue is full
  * @param  None
  * @return true - the queue is full, otherwise false
  */
bool KQueue::IsFull (void)
{
    return xQueueIsQueueFullFromISR(m_handler);
}
