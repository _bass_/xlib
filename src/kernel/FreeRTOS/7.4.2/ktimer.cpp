//*****************************************************************************
//
// @brief   Kernel software timer
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/ktimer.h"
#include "kernel/kernel.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "arch/system.h"
#include "misc/macros.h"
#include "misc/math.h"


/**
  * @brief  Constructor
  * @param  hdl: Timer (timeout completion) handler
  * @param  params: Additional handler parameters
  */
KTimer::KTimer (TTimerHdl* hdl, void* params)
{
    m_handle = xTimerCreate( (const char*)"TMR", 1, 0, this, genericTimerHandler );
    assert(!m_handle);

    m_params = params;
    m_cb = hdl;
}


/**
  * @brief  Destructor
  * @param  None
  */
KTimer::~KTimer ()
{
    xTimerDelete(m_handle, portMAX_DELAY);
}


/**
  * @brief  Destructor
  * @param  None
  */
void KTimer::SetHandler (TTimerHdl* hdl, void* params)
{
    m_params = params;
    m_cb = hdl;
}


/**
  * @brief  Generic timer service handler for timers processing
  * @param  timer: Timer object (unused)
  */
void KTimer::genericTimerHandler (void* timer)
{
    KTimer* obj = (KTimer*) pvTimerGetTimerID(timer);

    if (obj && obj->m_cb)
    {
        obj->m_cb(obj, obj->m_params);
    }
}


/**
  * @brief  Start timer
  * @param  time: Timer timeout
  * @return 0 or negative error code
  */
int KTimer::Start (uint time)
{
    // Convert time to ticks with rounding up
    time = (time + KERNEL_TICK_PERIOD - 1) / KERNEL_TICK_PERIOD;
    int result = 0;

    // Note: xTimerStart() call was removed because the timer is restarted on period changing
    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;
        if ( !result && xTimerChangePeriodFromISR(m_handle, time, &needSchedule) == pdFAIL ) result = -1;

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        if ( !result && xTimerChangePeriod(m_handle, time, time) == pdFAIL ) result = -1;
    }

    return result;
}


/**
  * @brief  Stop timer
  * @param  None
  * @return 0 or negative error code
  */
int KTimer::Stop (void)
{
    if ( !xTimerIsTimerActive(m_handle) ) return -1;

    int result = 0;

    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;
        if (!result && xTimerStopFromISR(m_handle, &needSchedule) == pdFAIL)  result = -2;

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        if (!result && xTimerStop(m_handle, portMAX_DELAY) == pdFAIL)  result = -2;
    }

    return result;
}


/**
  * @brief  Restart timer with last timeout
  * @param  None
  * @return 0 or negative error code
  */
int KTimer::Restart (void)
{
    int result = 0;

    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = pdFALSE;
        if (!result && xTimerResetFromISR(m_handle, &needSchedule) == pdFAIL)  result = -1;

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        if (!result && xTimerReset(m_handle, portMAX_DELAY) == pdFAIL)  result = -1;
    }

    return result;
}


extern "C"
{

#include "kernel/kdebug.h"
#include "task.h"
#include "queue.h"
#include <string.h>

/**
  * @brief  Check timer queue overflow
  * @param  queue: Timer queue
  */
void checkTimerQueue(xQueueHandle queue)
{
    #if !defined(CFG_SYS_NO_DEBUG)
    extern void* pxCurrentTCB;

    char* name = pcTaskGetTaskName(pxCurrentTCB);
    uint len = strlen( (const char*)name );
    if ( memcmp(name, "Tmr Svc", __MIN(len, configMAX_TASK_NAME_LEN)) ) return;

    if ( xQueueIsQueueFullFromISR(queue) )
    {
        assert(1);
    }
    #endif
}

} // extern "C"
