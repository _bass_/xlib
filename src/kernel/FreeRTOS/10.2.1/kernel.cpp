//*****************************************************************************
//
// @brief   RTOS core functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kernel.h"
#include "kernel/knotifier.h"
#include "kernel/events.h"
#include "FreeRTOS.h"
#include "task.h"
#include "arch/cpu.h"
#include "arch/systimer.h"
#include "arch/system.h"
#include "misc/macros.h"
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Event handlers list section
    #define EV_HANDLERS_ADDR_START      CC_SECTION_ADDR_START(xLib_kernel_evDesc)
    #define EV_HANDLERS_ADDR_END        CC_SECTION_ADDR_END(xLib_kernel_evDesc)

    // Background services list section
    #define BGSRV_ADDR_START            CC_SECTION_ADDR_START(xLib_kernel_bgHdl)
    #define BGSRV_ADDR_END              CC_SECTION_ADDR_END(xLib_kernel_bgHdl)


// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

    extern "C" void xPortSysTickHandler (void);
    extern "C" void xPortPendSVHandler (void);
    extern "C" void vPortSVCHandler (void);

    static void     markIdleBegin (void);
    static void     markIdleEnd (void);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // CPU load measurment variables
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    static bool     fIdleBegin = false;
    static uint32   idleBegin = 0;
    static uint32   countClock = 0;
    static uint32   measBegin = 0;
    #endif


/**
  * @brief  Start kernel (scheduler)
  * @param  None
  */
void Kernel::Start (void)
{
    SYSTMR::Init(CFG_SYSTMR_FREQ);
    SYSTMR::SetHandler(xPortSysTickHandler);
    SYSTMR::Start();

    IRQ::SetHandler(IRQ_ID_PEND_SV, xPortPendSVHandler);
    IRQ::SetHandler(IRQ_ID_SVC, vPortSVCHandler, IRQ_PRIORITY_HIGHEST);

    IRQ::Enable();

    vTaskStartScheduler();
    // There is no return from the function 
    // because scheduler will switch to selected thread
    assert(1);
}


/**
  * @brief  Stop kernel (scheduler)
  * @param  None
  */
void Kernel::Stop (void)
{
    vTaskEndScheduler();
}


/**
  * @brief  Activate scheduler with current thread blocking (switch to another thread)
  * @param  None
  */
void Kernel::Schedule (void)
{
    portYIELD();
}


/**
  * @brief  Critical section activation
  * @param  None
  */
void Kernel::EnterCritical (void)
{
    vTaskSuspendAll();
}


/**
  * @brief  Critical section deactivation
  * @param  None
  */
void Kernel::ExitCritical (void)
{
    xTaskResumeAll();
}


/**
  * @brief  System time reading (time from system start)
  * @param  None
  * @return Number of system ticks
  */
uint32 Kernel::GetJiffies (void)
{
    uint32 ticks;

    if (IRQ::GetActive() != IRQ_NONE)
    {
        ticks = xTaskGetTickCountFromISR();
    }
    else
    {
        ticks = xTaskGetTickCount();
    }

    return ticks * KERNEL_TICK_PERIOD;
}


/**
  * @brief  Event generation (in-place)
  * @param  code: Event code
  * @param  params: Event parameters
  */
void Kernel::RiseEvent (uint code, void* params)
{
    if (code == EV_SYS_CLOCK_CHANGED)
    {
        SYSTMR::Init(CFG_SYSTMR_FREQ);
        SYSTMR::Start();
    }
    
    SECTION_FOREACH(xLib_kernel_evDesc, const TEvHdlDesc, pDesc)
    {
        if (!pDesc->handler) continue;
        if (pDesc->evCode != KERNEL_EV_CODE_ALL && pDesc->evCode != code) continue;

        (pDesc->handler)(code, params);
    }
}


/**
  * @brief  Event generation (delayed, processing from separate thread)
  * @param  code: Event code
  * @param  params: Event parameters
  * @param  size: Parameters data size
  */
void Kernel::PostEvent (uint code, void* params, uint size)
{
    KNotifier::AddEvent(code, params, size);
}


/**
  * @brief  CPU load reading
  * @param  None
  * @return CPU load (%)
  */
uint Kernel::GetCpuLoad (void)
{
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    // Updating counters only in idle mode
    if (fIdleBegin)
    {
        markIdleBegin();
        markIdleEnd();
    }

    uint32 nowJiff = GetJiffies();
    uint32 clockPerJiff = (CLOCK::GetCpuFreq() * KERNEL_TICK_PERIOD) / 1000;
    uint32 idleTime = countClock / clockPerJiff;
    uint32 period = nowJiff - measBegin;

    uint load = (period) ? 100 - (idleTime * 100) / period : 100;

    measBegin = nowJiff;
    countClock = 0;

    return load;

    #else
    return 0;
    #endif
}


/**
  * @brief  Mark the begining of IDLE task
  * @param  None
  */
inline void markIdleBegin (void)
{
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    idleBegin = clock();
    fIdleBegin = true;
    #endif
}


/**
  * @brief  Mark the ending of IDLE task
  * @param  None
  */
inline void markIdleEnd (void)
{
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    if (!fIdleBegin) return;

    countClock += clock() - idleBegin;
    fIdleBegin = false;
    #endif
}


extern "C"
{

/**
  * @brief  Stack overflow hook
  * @param  pxCurrentTCB: Current task descriptor
  * @param  name: Task name
  */
void vApplicationStackOverflowHook (void* pxCurrentTCB, char* name)
{
    UNUSED_VAR(pxCurrentTCB);

    Kernel::RiseEvent(EV_SYS_STACK_OVERFLOW, (void*)name);
    // Stop working because data was damaged
    IRQ::Disable();
    while(1);
}


/**
  * @brief  Task enter hook
  * @param  None
  */
void kernelOnTaskIn (void)
{
    void* currTask = xTaskGetCurrentTaskHandle();
    assert(!currTask);

    if (currTask != xTaskGetIdleTaskHandle() ) return;

    markIdleBegin();
}


/**
  * @brief  Task out hook
  * @param  None
  */
void kernelOnTaskOut (void)
{
    if ( xTaskGetCurrentTaskHandle() != xTaskGetIdleTaskHandle() ) return;

    markIdleEnd();
}


/**
  * @brief  Scheduler was started hook
  * @param  None
  */
void kernelOnStartScheduler (void)
{
    Kernel::RiseEvent(EV_SYS_SCHEDULER_STARTED);
}


/**
  * @brief  System timer setup hook
  * @param  None
  */
void vPortSetupTimerInterrupt (void)
{
    // Function is empty because all needed work is made in Kernel::Start() directly
}


/**
  * @brief  IDLE task hook
  * @param  None
  */
void vApplicationIdleHook (void)
{
    SECTION_FOREACH(xLib_kernel_bgHdl, const TBgHdl, pHdl)
    {
        if (!pHdl || !*pHdl) continue;
        (*pHdl)();
    }
}

} // extern "C"
