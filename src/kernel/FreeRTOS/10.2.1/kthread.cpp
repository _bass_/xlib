//*****************************************************************************
//
// @brief   Threads
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kthread.h"
#include "kernel/kdebug.h"
#include "FreeRTOS.h"
#include "task.h"
#include "misc/macros.h"
#include "def_board.h"
#include <new>


/**
  * @brief  Constructor
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
KThread::KThread (const char* name, uint stackSize, uint priority) :
    m_handle(NULL)
{
    assert(priority >= configMAX_PRIORITIES);

    uint stackDepth = (stackSize + sizeof(uint32) - 1) / sizeof(uint32);
    void* handler;
    portBASE_TYPE result = xTaskCreate(
                                        KThread::EnterPoint,
                                        ((const char*)name),
                                        stackDepth,
                                        this,                       // Parameter for EnterPoint()
                                        priority,
                                        (TaskHandle_t*)&handler
                                      );
    if (result != pdTRUE)
    {
        kprint("[KThread] create thread %s ERROR", name);
        assert(1);
    }

    m_handle = handler;
}


/**
  * @brief  Destructor
  * @param  None
  */
KThread::~KThread ()
{
}


/**
  * @brief  Suspend thread
  * @param  None
  */
void KThread::Suspend (void)
{
    vTaskSuspend((TaskHandle_t)m_handle);
}


/**
  * @brief  Resume thread
  * @param  None
  */
void KThread::Resume (void)
{
    // Checking was removed because it exists in vTaskResume() implementation
    // if (eTaskGetState((TaskHandle_t)m_handle) != eSuspended) return;

    if (IRQ::GetActive() != IRQ_NONE)
    {
        portBASE_TYPE needSchedule = xTaskResumeFromISR((TaskHandle_t)m_handle);

        if (needSchedule)
        {
            Kernel::Schedule();
        }
    }
    else
    {
        vTaskResume((TaskHandle_t)m_handle);
    }
}


/**
  * @brief  Thread entry point
  * @param  params: Thread parameters (pointer to KThread object)
  */
void KThread::EnterPoint (void* params)
{
    KThread* obj = (KThread*) params;
    
    if (!obj->m_handle)
    {
        sleep(1);
    }
    assert(!obj->m_handle);

    obj->init();
    obj->run();

    // Remove thread object on exit
    // Handler zeroing to protect from static objects
    obj->m_handle = NULL;
    
    delete obj;
    vTaskDelete(NULL);
}


/**
  * @brief  Switch thread into sleep mode for specified time
  * @param  time: Minimum sleep state duration (ms)
  */
void KThread::sleep (uint time)
{
    uint ticks = time / KERNEL_TICK_PERIOD;
    if (!ticks) return;

    vTaskDelay(ticks);
}


/**
  * @brief  Constructor
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
KThreadWait::KThreadWait (const char* name, uint stackSize, uint priority) :
    KThread(name, stackSize, priority),
    m_threadTimer(threadTimerHandler, (void*)this)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
KThreadWait::~KThreadWait ()
{
}


/**
  * @brief  Stop thread for specified time with ability to 
  *         wake it up early by Resume()
  * @param  time: Minimum sleep state duration if thread won't be resumed manually (ms)
  */
void KThreadWait::wait (uint time)
{
    if (!time)
    {
        Resume();
        return;
    }

    vTaskSuspendAll();
    {
        int result = m_threadTimer.Start(time);
        if (!result) Suspend();
    }
    xTaskResumeAll();
}


/**
  * @brief  Timer handler (internal)
  * @param  timer: Timer object
  * @param  params: Additianal timer parameters
  */
void KThreadWait::threadTimerHandler (KTimer* timer, void* params)
{
    UNUSED_VAR(timer);

    KThreadWait* thread = (KThreadWait*) params;
    thread->Resume();
}
