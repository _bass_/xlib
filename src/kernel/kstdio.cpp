//*****************************************************************************
//
// @brief   Input/output functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kstdio.h"
#include "arch/system.h"
#include "dev/null.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Defining Type of formatting argument list to avoid issues with compilation on different platforms.
    // va_list in MinGW w64 is not a pointer so you can't get access to variables (except for first one)
    // in formatting handlers because the list isn't modified.
    #if defined(__MINGW64__)
    typedef va_list& TArgList;
    #else
    typedef va_list TArgList;
    #endif

    // Formatting handler descriptor
    typedef struct
    {
        // Formatting code
        char    code;
        /**
          * @brief  Formatting hadnler
          * @param  code: Formatting code
          * @param  args: List of arguments
          * @param  precision: Value precision
          * @param  pBuff: Pointer to output buffer (might be changes)
          * @param  size: Buffer size
          * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
          */
        int     (*pHandler) (char code, TArgList args, uint precision, char** pBuff, uint size);
    } TFormatDesc;


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    // Formatted string output into the stream
    static inline int outData (CDevChar* pDev, const char* buff, uint size, uint width, char fillChar);

    // Formatting hadnlers
    static int  hdlChar (char code, TArgList args, uint precision, char** pBuff, uint size);
    static int  hdlString (char code, TArgList args, uint precision, char** pBuff, uint size);
    static int  hdlDecimal (char code, TArgList args, uint precision, char** pBuff, uint size);
    static int  hdlHex (char code, TArgList args, uint precision, char** pBuff, uint size);
    static int  hdlFloat (char code, TArgList args, uint precision, char** pBuff, uint size);
    static int  hdlDump (char code, TArgList args, uint precision, char** pBuff, uint size);
    #if !defined(CFG_SYS_PRINTF_NO_IP)
    static int  hdlIp (char code, TArgList args, uint precision, char** pBuff, uint size);
    #endif
    #if !defined(CFG_SYS_PRINTF_NO_BIN)
    static int  hdlBin (char code, TArgList args, uint precision, char** pBuff, uint size);
    #endif


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Default values for IO streams
    CC_VAR_WEAK_DECLARATION KStream* kstdout = NULL;
    CC_VAR_WEAK_DECLARATION KStream* kstdin = NULL;

    // Formatting descriptors list
    static const TFormatDesc tableFormat[] =
    {
        { 'd', hdlDecimal },
        { 'u', hdlDecimal },
        { 'x', hdlHex },
        { 'f', hdlFloat },
        { 'c', hdlChar },
        { 's', hdlString },
        #if !defined(CFG_SYS_PRINTF_NO_IP)
        { 'i', hdlIp },
        #endif
        #if !defined(CFG_SYS_PRINTF_NO_BIN)
        { 'b', hdlBin },
        #endif
        { 'p', hdlDump }
    };

    //  IO device replacement for data formatting into a buffer
    static CDevNull m_devNull;

    static KMutex m_mutexDevNull;
    static KStream m_dumyStream =
    {
        &m_devNull,
        &m_mutexDevNull,
        {0}
    };


/**
  * @brief  Formatted output
  * @param  stream: Stream object
  * @param  format: Output string format
  * @param  ...: Additional format arguments
  * @return Number of bytes written into the stream or negative error code
  */
int fprintf (KStream* stream, const char* format, ...)
{
    if (!stream) return DEV_IO_ERR;

    bool isInIrq = (IRQ::GetActive() != IRQ_NONE);
    if (stream->pMutex && !isInIrq) stream->pMutex->Lock();

    va_list args;
    va_start(args, format);
    int len = vfprintf(stream, format, args);
    va_end(args);

    if (stream->pMutex && !isInIrq) stream->pMutex->Unlock();

    return len;
}


/**
  * @brief  Formatted output into specified buffer
  * @param  buff: Output buffer
  * @param  size: Output buffer size
  * @param  format: Output string format
  * @param  ...: Additional format arguments
  * @return Number of bytes written into the buffer or negative error code
  */
int sprintf (void* buff, uint size, const char* format, ...)
{
    m_mutexDevNull.Lock();

    // m_devNull configuring to work with specified buffer
    CDevNull::TOptions opt = {buff, size};
    m_devNull.Configure(&opt);

    va_list args;
    va_start(args, format);
    int len = vfprintf(&m_dumyStream, format, args);
    va_end(args);

    m_mutexDevNull.Unlock();

    return len;
}


/**
  * @brief  Formatted output into specified buffer
  * @param  buff: Output buffer
  * @param  size: Output buffer size
  * @param  format: Output string format
  * @param  args: Additional format arguments
  * @return Number of bytes written into the buffer or negative error code
  */
int vsprintf (void* buff, uint size, const char* format, va_list args)
{
    m_mutexDevNull.Lock();

    // m_devNull configuring to work with specified buffer
    CDevNull::TOptions opt = {buff, size};
    m_devNull.Configure(&opt);

    int len = vfprintf(&m_dumyStream, format, args);

    m_mutexDevNull.Unlock();

    return len;
}


/**
  * @brief  Formatted output
  * @param  stream: Stream object
  * @param  format: Output string format
  * @param  args: Additional format arguments
  * @return Number of bytes written into the stream or negative error code
  */
int vfprintf (KStream* stream, const char* format, va_list args)
{
    if (!stream) return DEV_IO_ERR;

    CDevChar* pDev = stream->pDev;
    if (!pDev) return DEV_IO_ERR;

    int count = 0;
    while (*format)
    {
        char c = *format++;

        // Output into stream unformatted symbol (format code always starts with '%')
        // Format "%%" as '%'
        if (c != '%' || *format == '%')
        {
            for (uint tryCount = 10; tryCount; --tryCount)
            {
                int res = pDev->PutChar(c);
                if (res >= 0) break;
                if (tryCount == 1) return -10;
                delay_us(10);
            }

            ++count;
            continue;
        }

        // Format data analyzing
        c = *format++;

        // Value width
        uint width = 0;
        while (c >= '0' && c <= '9')
        {
            width = width * 10 + (uint)c - '0';
            c = *format++;
        }

        // Value precision
        uint precision = 0;
        if (c == '.')
        {
            c = *format++;
            for (; c >= '0' && c <= '9'; c = *format++)
            {
                precision = (precision * 10) + (uint)c - '0';
            }
        }

        // Searching for specified format handler
        for (uint i = 0; i < ARR_COUNT(tableFormat); ++i)
        {
            if (c != tableFormat[i].code) continue;

            char* buff = stream->buff;
            int size = (tableFormat[i].pHandler)( c, args, precision, &buff, sizeof(KStream::buff) );

            if (size && buff)
            {
                bool dynMem = false;
                if (size < 0)
                {
                    dynMem = true;
                    size = -size;
                }

                int len = outData(stream->pDev, buff, (uint)size, width, ' ');
                if (dynMem) free(buff);

                if (len < 0) return -20;
                count += len;
            }

            break;
        } // for (uint i = 0; i < ARR_COUNT(tableFormat); ++i)
    } // while (c = *format)

    return count;
}


/**
  * @brief  Formatted data output
  * @param  pDev: IO device
  * @param  buff: Buffer with output data
  * @param  size: Amount of data in the buffer
  * @param  width: Value width
  * @param  fillChar: Fill-in symbol
  * @return Number of bytes sent to device for output or negative error code
  */
int outData (CDevChar* pDev, const char* buff, uint size, uint width, char fillChar)
{
    int count = 0;

    if (!width)
    {
        // Whole data output without filling-in and limits
        width = size;
    }

    // Fill-in free space
    while (width > size)
    {
        for (uint tryCount = 10; tryCount; --tryCount)
        {
            int res = pDev->PutChar(fillChar);
            if (res >= 0) break;
            if (tryCount == 1) return -1;
            delay_us(10);
        }

        ++count;
        --width;
    }

    // Data output taking into account the remaining width
    for (uint tryCount = 10; width && tryCount; --tryCount)
    {
        int len = pDev->Write(buff, width);
        if (len > 0)
        {
            count += len;
            buff += len;
            width -= (uint)len;
            ++tryCount;
        }

        if (width) delay_us(10);
    }

    return count;
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlChar (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);
    UNUSED_VAR(precision);

    char* buff = *pBuff;
    if ( !buff || size < sizeof(char) ) return 0;

    char c = (char) va_arg(args, int);
    *buff = c;

    return sizeof(char);
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlString (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);
    UNUSED_VAR(precision);
    UNUSED_VAR(size);

    char* str = va_arg(args, char*);
    if (!str) return 0;

    size_t len = strlen(str);
    if (!len) return 0;

    *pBuff = str;

    return (int)len;
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlDecimal (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    bool putMinus = false;
    uint uNumber;

    switch (code)
    {
        case 'u':
            uNumber = va_arg(args, uint);
        break;

        case 'd':
        {
            int val = va_arg(args, int);
            if (val < 0)
            {
                putMinus = true;
                val = -val;
            }

            uNumber = (uint)val;
        }
        break;

        default: return 0;
    }

    char* buff = *pBuff;
    if (!buff || !size) return 0;

    // Fill the buffer in reverse order
    buff += size - 1;
    uint rest = size;

    do
    {
        *buff = uNumber % 10 + '0';
        uNumber /= 10;
        --buff;
        --rest;
        if (precision) --precision;
    } while ( rest && (uNumber || precision) );

    if (uNumber) return 0;

    if (putMinus)
    {
        if (!rest) return 0;

        *buff = '-';
        --buff;
        --rest;
    }

    *pBuff = buff + 1;
    return (int)(size - rest);
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
#if !defined(CFG_SYS_PRINTF_NO_BIN)
int hdlBin (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);

    char* buff = *pBuff;
    if (!buff || !size) return 0;

    uint val = va_arg(args, uint);

    // Fill the buffer in reverse order
    buff += size - 1;
    uint rest = size;

    do
    {
        *buff = (val & 0x01) + '0';
        val >>= 1;
        --buff;
        --rest;
        if (precision) --precision;
    } while ( rest && (val || precision) );

    if (val) return 0;

    *pBuff = buff + 1;
    return (int)(size - rest);
}
#endif


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlHex (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);

    char* buff = *pBuff;
    if (!buff || !size) return 0;

    uint val = va_arg(args, uint);

    // Fill the buffer in reverse order
    buff += size - 1;
    uint rest = size;

    do
    {
        char nibble = val & 0x0f;
        *buff = (nibble < 10) ? nibble + '0' : nibble - 10 + 'a';
        val >>= 4;
        --buff;
        --rest;
        if (precision) --precision;
    } while ( rest && (val || precision) );

    if (val) return 0;

    *pBuff = buff + 1;
    return (int)(size - rest);
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlFloat (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);

    char* buff = *pBuff;
    if (!buff || !size) return 0;

    if (!precision || precision > 10)
    {
        precision = 1;
    }

    float val = (float) va_arg(args, double);

    bool putMinus = false;
    if (val < 0)
    {
        val = -val;
        putMinus = true;
    }

    // Split the value on integer and fractional parts
    uint intVal = (uint) val;
    val -= intVal;

    buff += size - 1;
    uint rest = size;

    // Make vlue of fractional part
    if ( rest <= (precision + 1) ) return 0;
    rest -= precision + 1;
    buff -= precision;
    *buff = '.';

    for (uint i = 1; i <= precision; ++i)
    {
        val *= 10.0f;
        uint c = (uint) val;
        val -= c;

        buff[i] = '0' + (char)c;
    }

    // Make vlue of integer part
    if (!rest) return 0;

    --buff;
    do
    {
        *buff = intVal % 10 + '0';
        intVal /= 10;
        --buff;
        --rest;
    } while (rest && intVal);

    if (intVal) return 0;

    if (putMinus)
    {
        if (!rest) return 0;
        *buff = '-';
        --buff;
        --rest;
    }

    *pBuff = buff + 1;
    return (int)(size - rest);
}


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
#if !defined(CFG_SYS_PRINTF_NO_IP)
int hdlIp (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);
    UNUSED_VAR(precision);

    char* buff = *pBuff;
    if (!buff || !size) return 0;

    uint32 ip = va_arg(args, uint32);
    // Each byte is 3 symbols + '.'
    if (size < sizeof(ip) * 3 + 3) return 0;

    for (uint i = 0; i < sizeof(ip); ++i)
    {
        // IP value is in network byte order
        uint val = (ip >> (8 * i)) & 0xff;

        // Make value string (3 digits + dot) in reverse order
        char tmp[4];
        uint j = 0;
        do
        {
            tmp[j++] = '0' + val % 10;
            val /= 10;
        } while(val);

        if (i) *buff++ = '.';

        // Copy the value into output buffer in correct order
        while (j--)
        {
            *buff++ = tmp[j];
        }
    } // for (uint i = 0; i < sizeof(ip); ++i)

    return (int)(buff - *pBuff);
}
#endif


/**
  * @brief  Formatting hadnler
  * @param  code: Formatting code
  * @param  args: List of arguments
  * @param  precision: Value precision
  * @param  pBuff: Pointer to output buffer (might be changes)
  * @param  size: Buffer size
  * @return Number of bytes for output. If value is negative - buffer is dynamic memory and must be freed
  */
int hdlDump (char code, TArgList args, uint precision, char** pBuff, uint size)
{
    UNUSED_VAR(code);
    UNUSED_VAR(precision);
    UNUSED_VAR(size);

    char* pData = va_arg(args, char*);
    uint dataLen = va_arg(args, uint);

    if (!dataLen) return 0;
    dataLen *= 2; // Each byte is 2 symbols in hex

    char* buff = (char*) malloc(dataLen);
    if (!buff) return 0;

    *pBuff = buff;

    for (uint i = dataLen; i; --i, ++buff)
    {
        char nibble = (i & 0x01) ? *pData : *pData >> 4;
        nibble &= 0x0f;

        *buff = (nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A');

        if (i & 0x01) ++pData;
    }

    return -( (int)dataLen );
}


/**
  * @brief  Read data from standard input stream (kstdin)
  * @param  None
  * @return Byte from the stream or negative error code
  */
int getchar (void)
{
    return (kstdin && kstdin->pDev) ? kstdin->pDev->GetChar() : -1;
}
