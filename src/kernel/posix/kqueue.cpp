//*****************************************************************************
//
// @brief   Queue implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kqueue.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>
#include <queue>
#include <mutex>
#include <condition_variable>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Internal queue descriptor
    typedef struct
    {
        uint                    itemSize;
        uint                    queueLen;
        std::queue<void*>*      pQueue;
        std::mutex              mutex;
        std::condition_variable sigItemAdded;
        std::condition_variable sigItemRemoved;
    } TQueueDesc;


/**
  * @brief  Constructor
  * @param  len: Number of items in the queue
  * @param  itemSize: Single item size
  */
KQueue::KQueue (uint len, uint itemSize)
{
    assert(!len);

    TQueueDesc* pDesc = new TQueueDesc;
    assert(!pDesc);

    pDesc->itemSize = itemSize;
    pDesc->queueLen = len;

    pDesc->pQueue = new std::queue<void*>;
    assert(!pDesc->pQueue);

    m_handler = pDesc;
}


/**
  * @brief  Destructor
  * @param  None
  */
KQueue::~KQueue ()
{
    if (!m_handler) return;

    TQueueDesc* pDesc = (TQueueDesc*)m_handler;

    pDesc->mutex.lock();

    // Remove all data in the queue and queue itself
    std::queue<void*>* pQueue = pDesc->pQueue;

    while ( pQueue && pQueue->size() )
    {
        void* pData = pQueue->front();
        pQueue->pop();
        if (pData) free(pData);
    }
    delete pQueue;

    pDesc->mutex.unlock();
    delete pDesc;
}


/**
  * @brief  Add item in the queue
  * @param  pItem: Item to add
  */
void KQueue::Add (const void* pItem)
{
    TQueueDesc* pDesc = (TQueueDesc*)m_handler;
    std::queue<void*>* pQueue = pDesc->pQueue;

    void* pData = malloc(pDesc->itemSize);
    assert(!pData);
    memcpy(pData, pItem, pDesc->itemSize);

    std::unique_lock<std::mutex> locker(pDesc->mutex);
    while (pQueue->size() >= pDesc->queueLen)
    {
        pDesc->sigItemRemoved.wait(locker);
    }

    pQueue->push(pData);
    pDesc->sigItemAdded.notify_one();
}


/**
  * @brief  Read item from the queue. The read value will be deleted
  * @param  pItem: Pointer to variable to save item data
  * @param  timeout: Maximum timeout of waiting data in case of empty queue
  * @return 0 or negative error code
  */
int KQueue::Get (void* pItem, uint timeout)
{
    if (!pItem) return -1;

    TQueueDesc* pDesc = (TQueueDesc*)m_handler;
    std::queue<void*>* pQueue = pDesc->pQueue;

    std::unique_lock<std::mutex> locker(pDesc->mutex);

    // Stop the thread until data in queue will appear
    uint32 startTime = Kernel::GetJiffies();
    while ( pQueue->empty() && IsTimeout(startTime, timeout) )
    {
        pDesc->sigItemAdded.wait(locker);
    }

    if ( pQueue->empty() ) return -2;

    void* pData = pQueue->front();
    pQueue->pop();
    pDesc->sigItemRemoved.notify_one();

    if (!pData) return -3;

    memcpy(pItem, pData, pDesc->itemSize);
    free(pData);

    return 0;
}


/**
  * @brief  Read item from the queue (without deleting item from the queue)
  * @param  pItem: Pointer to variable to save item data
  * @return 0 or negative error code
  */
int KQueue::Peek (void* pItem)
{
    if (!pItem) return -1;

    TQueueDesc* pDesc = (TQueueDesc*)m_handler;
    std::queue<void*>* pQueue = pDesc->pQueue;

    std::lock_guard<std::mutex> locker(pDesc->mutex);
    if ( pQueue->empty() ) return -2;

    void* pData = pQueue->front();
    memcpy(pItem, pData, pDesc->itemSize);

    return 0;
}


/**
  * @brief  Read number of items in the queue
  * @param  None
  * @return Number of items in the queue
  */
uint KQueue::Count (void)
{
    TQueueDesc* pDesc = (TQueueDesc*)m_handler;
    std::queue<void*>* pQueue = pDesc->pQueue;

    std::lock_guard<std::mutex> locker(pDesc->mutex);
    return (uint) pQueue->size();
}


/**
  * @brief  Check if the queue is full
  * @param  None
  * @return true - the queue is full, otherwise false
  */
bool KQueue::IsFull (void)
{
    TQueueDesc* pDesc = (TQueueDesc*)m_handler;
    std::queue<void*>* pQueue = pDesc->pQueue;

    return (pQueue->size() >= pDesc->queueLen);
}
