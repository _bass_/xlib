//*****************************************************************************
//
// @brief   Mutexes implementation
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "kernel/kmutex.h"
#include "kernel/kernel.h"
#include "misc/macros.h"
#include <mutex>


/**
  * @brief  Constructor
  * @param  None
  */
KMutex::KMutex (void)
{
    m_mutex = new std::mutex;
    assert(!m_mutex);
}


/**
  * @brief  Destructor
  * @param  None
  */
KMutex::~KMutex ()
{
    if (!m_mutex) return;

    std::mutex* pMutex = (std::mutex*) m_mutex;
    delete pMutex;
}


/**
  * @brief  Lock mutex
  * @param  None
  */
void KMutex::Lock (void)
{
    if (!m_mutex) return;

    std::mutex* pMutex = (std::mutex*) m_mutex;
    pMutex->lock();
}


/**
  * @brief  Release mutex
  * @param  None
  */
void KMutex::Unlock (void)
{
    if (!m_mutex) return;

    // Note: if the mutex is not currently locked by the calling thread, it causes undefined behavior!
    std::mutex* pMutex = (std::mutex*) m_mutex;
    pMutex->unlock();
}
