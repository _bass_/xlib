//*****************************************************************************
//
// @brief   Kernel software timer
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/ktimer.h"
#include "kernel/kernel.h"
#include "misc/macros.h"
#include <thread>
#include <mutex>
#include <chrono>
#include <list>
#include <algorithm>
#include <stdlib.h>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Internal timer descriptor
    typedef struct
    {
        uint        time;       // Timeout value
        uint        counter;    // Let time counter
        std::mutex  mutex;      // Lock mutext for safe descriptor modification
    } TTmrDesc;


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // List of activated timers
    static std::list<KTimer*> g_activeTimers;
    // Mutex for safe timer list modification
    static std::mutex m_listMutex;


/**
  * @brief  Constructor
  * @param  hdl: Timer (timeout completion) handler
  * @param  params: Additional handler parameters
  */
KTimer::KTimer (TTimerHdl* hdl, void* params)
{
    assert(!hdl);
    m_cb = hdl;
    m_params = params;

    m_handle = new TTmrDesc;
    assert(!m_handle);
}


/**
  * @brief  Destructor
  * @param  None
  */
KTimer::~KTimer ()
{
    Stop();

    if (m_handle)
    {
        TTmrDesc* pDesc = (TTmrDesc*) m_handle;
        delete pDesc;
    }
}


/**
  * @brief  Set / change timer timeout handler
  * @param  hdl: Timer handler
  * @param  params: Additional handler parameters
  */
void KTimer::SetHandler (TTimerHdl* hdl, void* params)
{
    m_params = params;
    m_cb = hdl;
}


/**
  * @brief  Generic timer service handler for timers processing
  * @param  timer: Timer object (unused)
  */
void KTimer::genericTimerHandler (void* timer)
{
    UNUSED_VAR(timer);

    // Timers tick processing
    for (auto i = g_activeTimers.begin(); i != g_activeTimers.end(); ++i)
    {
        KTimer* obj = *i;
        TTmrDesc* pDesc = (TTmrDesc*) obj->m_handle;

        if (!pDesc || !pDesc->counter) continue;

        pDesc->mutex.lock();
        if (pDesc->counter > KERNEL_TICK_PERIOD)    pDesc->counter -= KERNEL_TICK_PERIOD;
        else                                        pDesc->counter = 0;
        pDesc->mutex.unlock();

        if (!pDesc->counter && obj->m_cb)
        {
            obj->m_cb(obj, obj->m_params);
        }
    }

    // Stopped timers removing
    m_listMutex.lock();
    for (auto i = g_activeTimers.begin(); i != g_activeTimers.end(); )
    {
        KTimer* obj = *i;
        TTmrDesc* pDesc = (TTmrDesc*) obj->m_handle;

        if (pDesc && !pDesc->counter)
        {
            i = g_activeTimers.erase(i);
        }
        else
        {
            ++i;
        }
    }
    m_listMutex.unlock();
}


/**
  * @brief  Start timer
  * @param  time: Timer timeout
  * @return 0 or negative error code
  */
int KTimer::Start (uint time)
{
    if (!time) return -1;

    TTmrDesc* pDesc = (TTmrDesc*) m_handle;

    pDesc->mutex.lock();
    pDesc->time = time;
    pDesc->counter = time;
    pDesc->mutex.unlock();

    auto itr = std::find(g_activeTimers.begin(), g_activeTimers.end(), this);
    if ( itr == g_activeTimers.end() )
    {
        g_activeTimers.push_back(this);
    }

    return 0;
}


/**
  * @brief  Stop timer
  * @param  None
  * @return 0 or negative error code
  */
int KTimer::Stop (void)
{
    // Remove from activated timers list
    auto itr = std::find(g_activeTimers.begin(), g_activeTimers.end(), this);
    if ( itr == g_activeTimers.end() ) return -1;

    m_listMutex.lock();
    g_activeTimers.erase(itr);
    m_listMutex.unlock();

    return 0;
}


/**
  * @brief  Restart timer with last timeout
  * @param  None
  * @return 0 or negative error code
  */
int KTimer::Restart (void)
{
    TTmrDesc* pDesc = (TTmrDesc*) m_handle;
    return Start(pDesc->time);
}
