//*****************************************************************************
//
// @brief   RTOS core functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kernel.h"
#include "kernel/knotifier.h"
#include "kernel/events.h"
#include "arch/cpu.h"
#include "misc/macros.h"
#include "def_board.h"
#include <stdlib.h>
#include <new>
#include <thread>
#include <chrono>
#include <mutex>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static volatile uint32 g_ticksCounter = 0;
    // Mutex for locking critical section
    static std::recursive_mutex g_critSectionMutex;

    // CPU load measurment variables
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    static bool     fIdleBegin = false;
    static uint32   idleBegin = 0;
    static uint32   countClock = 0;
    static uint32   measBegin = 0;
    #endif


/**
  * @brief  System timer handler
  * @param  None
  */
static void sysTimerHandler (void)
{
    for (;;)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(KERNEL_TICK_PERIOD));
        ++g_ticksCounter;

        // Call base timer handler to provide synchronous work of timers with system time
        KTimer::genericTimerHandler(NULL);
    }
}


/**
  * @brief  Start kernel (scheduler)
  * @param  None
  */
void Kernel::Start (void)
{
    std::thread* sysTmrThread = new std::thread(sysTimerHandler);
    // Thread priority increasing (IRQ emulation)
    {
        std::thread::native_handle_type nativeHdl = sysTmrThread->native_handle();

        int policy;
        sched_param thrParams;
        int res = pthread_getschedparam(nativeHdl, &policy, &thrParams);
        assert(res);
        thrParams.sched_priority = sched_get_priority_max(policy);
        res = pthread_setschedparam(nativeHdl, policy, &thrParams);
    }
    sysTmrThread->detach();

    Kernel::RiseEvent(EV_SYS_SCHEDULER_STARTED);

    while (1)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));

        // Background services processing
        SECTION_FOREACH(xLib_kernel_bgHdl, const TBgHdl, pHdl)
        {
            if (!pHdl) continue;
            (*pHdl)();
        }
    }
}


/**
  * @brief  Stop kernel (scheduler)
  * @param  None
  */
void Kernel::Stop (void)
{
    exit(EXIT_SUCCESS);
}


/**
  * @brief  Activate scheduler with current thread blocking (switch to another thread)
  * @param  None
  */
void Kernel::Schedule (void)
{
    std::this_thread::yield();
}


/**
  * @brief  Critical section activation
  * @param  None
  */
void Kernel::EnterCritical (void)
{
    g_critSectionMutex.lock();
}


/**
  * @brief  Critical section deactivation
  * @param  None
  */
void Kernel::ExitCritical (void)
{
    g_critSectionMutex.unlock();
}


/**
  * @brief  System time reading (time from system start)
  * @param  None
  * @return Number of system ticks
  */
uint32 Kernel::GetJiffies (void)
{
    return g_ticksCounter * KERNEL_TICK_PERIOD;
}


/**
  * @brief  Event generation (in-place)
  * @param  code: Event code
  * @param  params: Event parameters
  */
void Kernel::RiseEvent (uint code, void* params)
{
    SECTION_FOREACH(xLib_kernel_evDesc, const TEvHdlDesc, pDesc)
    {
        if (!pDesc->handler) continue;
        if (pDesc->evCode != KERNEL_EV_CODE_ALL && pDesc->evCode != code) continue;

        (pDesc->handler)(code, params);
    }
}


/**
  * @brief  Event generation (delayed, processing from separate thread)
  * @param  code: Event code
  * @param  params: Event parameters
  * @param  size: Parameters data size
  */
void Kernel::PostEvent (uint code, void* params, uint size)
{
    KNotifier::AddEvent(code, params, size);
}


/**
  * @brief  CPU load reading
  * @param  None
  * @return CPU load (%)
  */
uint Kernel::GetCpuLoad (void)
{
    #if defined(CFG_KERNEL_CPU_LOAD_PRESENT)
    // Updating counters only in idle mode
    if (fIdleBegin)
    {
        markIdleBegin();
        markIdleEnd();
    }

    uint32 nowJiff = GetJiffies();
    uint32 clockPerJiff = (CLOCK::GetCpuFreq() * KERNEL_TICK_PERIOD) / 1000;
    uint32 idleTime = countClock / clockPerJiff;
    uint32 period = nowJiff - measBegin;

    uint load = (period) ? 100 - (idleTime * 100) / period : 100;

    measBegin = nowJiff;
    countClock = 0;

    return load;

    #else
    return 0;
    #endif
}
