//*****************************************************************************
//
// @brief   Threads
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kthread.h"
#include "kernel/kdebug.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <thread>
#include <mutex>
#include <stddef.h>
#include <new>

#if defined(_WIN32) || defined(__CYGWIN__)
    #include <windows.h>
    #define IS_WINDOWS
#elif defined(__linux__)
    #include <signal.h>
    #include <errno.h>
    #define IS_LINUX
#else
    #error "Unsupported platform!"
#endif


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if defined (IS_LINUX)
        // Signals for suspend and resume thread
        #define SIGNAL_SUSPEND      SIGUSR1
        #define SIGNAL_RESUME       SIGUSR2
    #endif


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    #if defined (IS_LINUX)
        static int initSigHandlers (void);
        static void sigHdlSuspend (int signum, siginfo_t* info, void* context);
        static void sigHdlResume (int signum, siginfo_t* info, void* context);
    #endif


/**
  * @brief  Thread signals setup
  * @param  None
  * @return 0 on success or negative error code
  */
#if defined (IS_LINUX)
int initSigHandlers (void)
{
    struct sigaction act;

    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGNAL_SUSPEND);
    sigaddset(&act.sa_mask, SIGNAL_RESUME);
    act.sa_flags = SA_RESTART | SA_SIGINFO;

    act.sa_sigaction = sigHdlSuspend;
    if ( sigaction(SIGNAL_SUSPEND, &act, NULL) ) return -1;

    act.sa_sigaction = sigHdlResume;
    if ( sigaction(SIGNAL_RESUME, &act, NULL) ) return -2;

    return 0;
}
#endif


/**
  * @brief  Suspend thread signal handler
  * @param  signum: Activated signal ID
  * @param  info: Signal additional information
  * @param  context: Signal context (see struct ucontext_t)
  */
#if defined (IS_LINUX)
void sigHdlSuspend (int signum, siginfo_t* info, void* context)
{
    UNUSED_VAR(info);
    UNUSED_VAR(context);

    if (signum != SIGNAL_SUSPEND) return;

    int errnoSaved = errno;

    // Lock thread until resume signal receiving
    sigset_t resumeset;
    sigfillset(&resumeset);
    sigdelset(&resumeset, SIGNAL_SUSPEND);
    sigdelset(&resumeset, SIGNAL_RESUME);
    sigsuspend(&resumeset);

    // errno value restoration
    errno = errnoSaved;
}
#endif


/**
  * @brief  Resume thread signal handler
  * @param  signum: Activated signal ID
  * @param  info: Signal additional information
  * @param  context: Signal context (see struct ucontext_t)
  */
#if defined (IS_LINUX)
void sigHdlResume (int signum, siginfo_t* info, void* context)
{
    UNUSED_VAR(signum);
    UNUSED_VAR(info);
    UNUSED_VAR(context);
}
#endif


/**
  * @brief  Constructor
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
KThread::KThread (const char* name, uint stackSize, uint priority) :
    m_handle(NULL)
{
    UNUSED_VAR(name);
    UNUSED_VAR(stackSize);
    UNUSED_VAR(priority);

    std::thread* hdl = new std::thread(EnterPoint, this);

    if (!hdl)
    {
        kprint("[KThread] create thread %s ERROR", name);
        assert(1);
    }

    m_handle = hdl;
}


/**
  * @brief  Destructor
  * @param  None
  */
KThread::~KThread ()
{
    if (!m_handle) return;

    std::thread* pThread = (std::thread*) m_handle;

    if ( pThread->joinable() )
    {
        pThread->detach();
    }

    delete pThread;
}


/**
  * @brief  Suspend thread
  * @param  None
  */
void KThread::Suspend (void)
{
    std::thread* pThread = (std::thread*)m_handle;
    assert(!pThread);
    std::thread::native_handle_type nativeHdl = pThread->native_handle();

    #if defined(IS_WINDOWS)
    void* winHdl = pthread_gethandle(nativeHdl);
    DWORD result = ::SuspendThread(winHdl);
    assert(result == (DWORD)-1);

    #elif defined(IS_LINUX)
    int result = pthread_kill(nativeHdl, SIGNAL_SUSPEND);
    assert(result != 0);
    #endif
}


/**
  * @brief  Resume thread
  * @param  None
  */
void KThread::Resume (void)
{
    std::thread* pThread = (std::thread*)m_handle;
    assert(!pThread);
    std::thread::native_handle_type nativeHdl = pThread->native_handle();

    #if defined(IS_WINDOWS)
    void* winHdl = pthread_gethandle(nativeHdl);
    DWORD result = ::ResumeThread(winHdl);
    assert(result == (DWORD)-1);

    #elif defined(IS_LINUX)
    int result = pthread_kill(nativeHdl, SIGNAL_RESUME);
    assert(result != 0);
    #endif
}


/**
  * @brief  Thread entry point
  * @param  params: Thread parameters (pointer to KThread object)
  */
void KThread::EnterPoint (void* params)
{
    KThread* obj = (KThread*) params;

    if (!obj->m_handle)
    {
        sleep(1);
    }
    assert(!obj->m_handle);

    #if defined(IS_LINUX)
    {
        int res = initSigHandlers();
        assert(res);
    }
    #endif

    obj->init();
    obj->run();
}


/**
  * @brief  Switch thread into sleep mode for specified time
  * @param  time: Minimum sleep state duration (ms)
  */
void KThread::sleep (uint time)
{
    if (!time) return;

    uint32 startTime = Kernel::GetJiffies();

    std::this_thread::sleep_for(std::chrono::milliseconds(time));
    while ( !IsTimeout(startTime, time) )
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}



/**
  * @brief  Constructor
  * @param  name: Thread name
  * @param  stackSize: Thread stack size
  * @param  priority: Thread priority
  */
KThreadWait::KThreadWait (const char* name, uint stackSize, uint priority) :
    KThread(name, stackSize, priority),
    m_threadTimer(threadTimerHandler, (void*)this)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
KThreadWait::~KThreadWait ()
{
}


/**
  * @brief  Stop thread for specified time with ability to 
  *         wake it up early by Resume()
  * @param  time: Minimum sleep state duration if thread won't be resumed manually (ms)
  */
void KThreadWait::wait (uint time)
{
    if (!time)
    {
        Resume();
        return;
    }

    int result = m_threadTimer.Start(time);
    if (!result) Suspend();
}


/**
  * @brief  Timer handler (internal)
  * @param  timer: Timer object
  * @param  params: Additianal timer parameters
  */
void KThreadWait::threadTimerHandler (KTimer* timer, void* params)
{
    UNUSED_VAR(timer);

    KThreadWait* thread = (KThreadWait*) params;
    thread->Resume();
}
