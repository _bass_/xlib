//*****************************************************************************
//
// @brief   Platform-independent delayed events processing (see Kernel::PostEvent())
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/knotifier.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>
#include <new>
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Events processing thread stack size (bytes)
    #if defined(CFG_KEVENT_THREAD_STACK_SIZE)
        #define KEVENT_THREAD_STACK_SIZE            CFG_KEVENT_THREAD_STACK_SIZE
    #else
        #define KEVENT_THREAD_STACK_SIZE            0
    #endif

    // Waiting timeout in notification thread of appearing new events (ms)
    #if defined(CFG_KEVENT_THREAD_TIMEOUT)
        #define KEVENT_THREAD_TIMEOUT               CFG_KEVENT_THREAD_TIMEOUT
    #else
        #define KEVENT_THREAD_TIMEOUT               200
    #endif

    // Notification queue length
    #if defined(CFG_KEVENT_THREAD_QUEUE_LEN)
        #define KEVENT_THREAD_QUEUE_LEN             CFG_KEVENT_THREAD_QUEUE_LEN
    #else
        #define KEVENT_THREAD_QUEUE_LEN             2
    #endif


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Event descriptor
    typedef struct
    {
        uint    code;
        void*   params;
        uint    size;
    } TEvDesc;


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Notification thread
    #if (KEVENT_THREAD_STACK_SIZE > 0)
    KNotifier*  KNotifier::m_pThread = NULL;
    uint        KNotifier::m_lockThreadDestroy = 0;
    #endif


/**
  * @brief  Constructor
  * @param  None
  */
#if (KEVENT_THREAD_STACK_SIZE > 0)
KNotifier::KNotifier ():
    KThread("KNotifier", KEVENT_THREAD_STACK_SIZE, THREAD_PRIORITY_MAX),
    m_evQueue( KEVENT_THREAD_QUEUE_LEN, sizeof(TEvDesc) )
{
}
#endif


/**
  * @brief  Add event to processing queue
  * @param  code: Event code
  * @param  params: Event parameters
  * @param  size: Parameters data size
  */
void KNotifier::AddEvent (uint code, void* params, uint size)
{
    #if (KEVENT_THREAD_STACK_SIZE > 0)
    Kernel::EnterCritical();
    {
        ++m_lockThreadDestroy;
        do
        {
            if (!m_pThread)
            {
                m_pThread = new KNotifier();
                if (!m_pThread) break;
            }
            
            TEvDesc desc = { .code = code, .params = params, .size = size };
            if (size)
            {
                void* ptr = malloc(size);
                if (!ptr) break;
                memcpy(ptr, params, size);
                desc.params = ptr;
            }
            
            m_pThread->m_evQueue.Add(&desc);
            
        } while (0);
        
        --m_lockThreadDestroy;
    }
    Kernel::ExitCritical();
    
    #else
    UNUSED_VAR(size);
    Kernel::RiseEvent(code, params);
    #endif
}


/**
  * @brief  Thread mein function
  * @param  None
  */
#if (KEVENT_THREAD_STACK_SIZE > 0)
void KNotifier::run (void)
{
    for (;;)
    {
        TEvDesc desc;
        int res = m_evQueue.Get(&desc, KEVENT_THREAD_TIMEOUT);
        if (!res)
        {
            Kernel::RiseEvent(desc.code, desc.params);
            
            if (desc.size && desc.params) free(desc.params);
            continue;
        }
        
        bool stopProcess = false;
        Kernel::EnterCritical();
        if ( !m_lockThreadDestroy && !m_evQueue.Count() )
        {
            m_pThread = NULL;
            stopProcess = true;
        }
        Kernel::ExitCritical();
        
        // Notification queue is empty, quitting
        if (stopProcess) break;
    } // for (;;)
}
#endif
