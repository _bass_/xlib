//*****************************************************************************
//
// @brief   Debug IO subsystem
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "arch/cpu.h"
#include "kernel/kdebug.h"
#include "kernel/kernel.h"
#include "kernel/kstdio.h"
#include "kernel/kthread.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <string.h>
#include <ctype.h>


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    // Stack size of reading thread (bytes)
    // If CFG_KDEBUG_STACK_SIZE = 0 reading thread won't be created
    #if defined(CFG_KDEBUG_STACK_SIZE)
    #define KDEBUG_STACK_SIZE                           CFG_KDEBUG_STACK_SIZE
    #else
    #define KDEBUG_STACK_SIZE                           0
    #endif

    // Buffer size for incomming commands (bytes)
    #define KDEBUG_CMD_BUFF_SIZE                        32

    // Maximum number of commands arguments
    #define KDEBUG_CMD_MAX_ARG_COUNT                    4

    // Check symbol on quotes (single or double)
    #define KDEBUG_IS_QUOTE(c)                          ( (c) == '"' || (c) == '\'' )


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Debug thread for commands reading
    #if (KDEBUG_STACK_SIZE > 0)
    class KDebug : public KThread
    {
        public:
            /**
              * @brief  Constructor (reading thread)
              * @param  stackSize: Reading thread stack size
              */
            KDebug (uint stackSize);

        private:
            char m_buff[KDEBUG_CMD_BUFF_SIZE];
            uint m_index;

            /**
              * @brief  Thread main function
              * @param  None
              */
            void run (void);

            /**
              * @brief  Parse command input to create argument list
              * @param  str: Command input string
              * @param  argList: Pointer to arguments array (maximum size KDEBUG_CMD_MAX_ARG_COUNT)
              * @return Number of arguments in the list or negative error code
              */
            int buildArgList (char* str, char** argList);

            /**
              * @brief  Find command handler descriptor
              * @param  str: Command name
              * @return Pointer to command descriptor or NULL on error
              */
            static const TDbgDesc* findDesc (const char* str);
    };
    #endif


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Default value of system log handler
    extern CC_VAR_WEAK_DECLARATION TLogHdl* const pKdebugSyslogHandler = NULL;

#if (KDEBUG_STACK_SIZE > 0)
    KDebug debugThread(KDEBUG_STACK_SIZE);
#endif


#if (KDEBUG_STACK_SIZE > 0)
/**
  * @brief  Constructor (reading thread)
  * @param  stackSize: Reading thread stack size
  */
KDebug::KDebug (uint stackSize) :
    KThread("KDebug", stackSize, THREAD_PRIORITY_DEFAULT)
{
}


/**
  * @brief  Thread main function
  * @param  None
  */
void KDebug::run (void)
{
    m_index = 0;

    for (;;)
    {
        do
        {
            int byte = getchar();
            if (byte < 0) break;

            if ( m_index >= sizeof(m_buff) ) m_index = 0;

            m_buff[m_index] = (char)byte;
            ++m_index;

            if (byte != '\n') continue;

            m_buff[m_index - 1] = '\0';
            if (m_buff[m_index - 2] == '\r') m_buff[m_index - 2] = '\0';
            m_index = 0;

            kprint(m_buff);

            // Build argument list for received command
            char* argList[KDEBUG_CMD_MAX_ARG_COUNT] = {};
            int count = buildArgList(m_buff, argList);

            // First word is command name
            const TDbgDesc* pDesc = findDesc(m_buff);
            if (!pDesc || !pDesc->hdl)
            {
                kprint("Unknown command");
                continue;
            }

            pDesc->hdl(argList, count);
        } while (0);

        sleep(100);
    }
}


/**
  * @brief  Parse command input to create argument list
  * @param  str: Command input string
  * @param  argList: Pointer to arguments array (maximum size KDEBUG_CMD_MAX_ARG_COUNT)
  * @return Number of arguments in the list or negative error code
  */
int KDebug::buildArgList (char* str, char** argList)
{
    if (!str || *str == '\0') return -1;

    // Skipping space symbols at the beginning
    while ( *str && isspace(*str) ) ++str;

    char* pArg = NULL;
    char* ptr = str;
    int argCount = -1;
    bool inQuote = false;
    size_t len = strlen(str) + 1; // +1 - null-terminator

    while (len--)
    {
        if (
            !*ptr ||
            ( !inQuote && isspace(*ptr) ) ||
            ( inQuote && KDEBUG_IS_QUOTE(*ptr) )
        ) {
            // Quoted string is precessed as single argument
            if (inQuote)
            {
                inQuote = false;
                // Adjusting quoted argument start position
                if ( pArg && KDEBUG_IS_QUOTE(*pArg) ) ++pArg;
            }

            *ptr = '\0';

            if (*(ptr - 1) != '\0')
            {
                // Save arguments in the list except for first one - it's a command name
                if (pArg && argCount >= 0)
                {
                    argList[argCount] = pArg;
                }

                pArg = NULL;
                ++argCount;
                if (argCount > KDEBUG_CMD_MAX_ARG_COUNT) break;
            }
        }
        else if (!pArg)
        {
            pArg = ptr;
            // Quoted arguments detection
            if ( !inQuote && KDEBUG_IS_QUOTE(*ptr) ) inQuote = true;
        }

        ++ptr;
    } // while (len--)

    return argCount;
}


/**
  * @brief  Find command handler descriptor
  * @param  str: Command name
  * @return Pointer to command descriptor or NULL on error
  */
const TDbgDesc* KDebug::findDesc (const char* cmd)
{
    SECTION_FOREACH(xLib_kernel_dbgDesc, const TDbgDesc, pDesc)
    {
        if ( !strcmp(cmd, pDesc->cmd) ) return pDesc;
    }

    return NULL;
}
#endif // #if (KDEBUG_STACK_SIZE > 0)


/**
  * @brief  Formatted output of debug messages
  * @param  pDesc: Debug module descriptor
  * @param  level: Debug level of the message
  * @param  format: Message format
  */
void kDebugPrint (const TDbgDesc* pDesc, TDbgLvl level, const char* format, ...)
{
    va_list args;
    va_start(args, format);

    if (pKdebugSyslogHandler)
    {
        va_list argsCopy;
        va_copy(argsCopy, args);
        pKdebugSyslogHandler(level, (pDesc) ? pDesc->name : NULL, format, &argsCopy);
        va_end(argsCopy);
    }

    do
    {
        if (pDesc && pDesc->pDebugLevel && level > *pDesc->pDebugLevel) break;

        // Lock output to prevent splitting message on parts by another output
        static uint lock = 0;
        uint wait = 1;

        while (wait)
        {
            IRQ::Lock();
            if (!lock)
            {
                lock = 1;
                wait = 0;
            }
            IRQ::Unlock();

            if (wait)
            {
                if (IRQ::GetActive() == IRQ_NONE)   KThread::sleep(1);  // Waiting for unlock inside a thread
                else                                break;              // We are in IRQ handler so ignore any locks (can't sleep here)
            }
        }

        if (pDesc && *pDesc->name) fprintf(kstdout, "[%s] ", pDesc->name);
        vfprintf(kstdout, format, args);
        fprintf(kstdout, "\r\n");

        IRQ::Lock();
        lock = 0;
        IRQ::Unlock();
    } while (0);

    va_end(args);
}


/**
  * @brief  Find debug module descriptor
  * @param  name: Debug module name
  * @return Debug module descriptor or NULL
  */
const TDbgDesc* kDebugFindDesc (const char* name)
{
    if (!name || *name == '\0') return NULL;

    SECTION_FOREACH(xLib_kernel_dbgDesc, const TDbgDesc, pDesc)
    {
        if ( !strcmp(name, pDesc->name) ) return pDesc;
    }

    return NULL;
}
