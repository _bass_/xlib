//*****************************************************************************
//
// @brief   Time functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "misc/time.h"
#include "misc/macros.h"
#include "arch/rtc.h"
#include "kernel/kernel.h"
#include "kernel/events.h"


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Number of days in a month
    static const char g_tableDays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


/**
  * @brief  Get current time in Unixtime format
  * @param  None
  * @return Timestamp
  */
uint32 time (void)
{
    return RTC::GetTime();
    // FIXME: Use system time if there is no RTC
}


/**
  * @brief  Get current time in structured format
  * @param  pTm: Variable to save time data
  */
void time (TTime* pTm)
{
    RTC::GetTime(pTm);
    // FIXME: Use system time if there is no RTC
}


/**
  * @brief  Set system time
  * @param  pTm: Pointer to time data
  */
void SetTime (TTime* pTm)
{
    RTC::SetTime(pTm);
    Kernel::RiseEvent(EV_SYS_TIME_UPDATED);
}


/**
  * @brief  Set system time
  * @param  time: Time value
  */
void SetTime (uint32 time)
{
    RTC::SetTime(time);
    Kernel::RiseEvent(EV_SYS_TIME_UPDATED);
}


/**
  * @brief  Day of week calculation
  * @param  day: Day of month [1 - 31]
  * @param  month: Month [1 - 12]
  * @param  year: Year (20xx)
  * @return Day of week (1 - Mon, ... 7 - Sun)
  */
uint WhatDayTime (uint day, uint month, uint year)
{
    uint a = (14 - month) / 12;
    uint y = year - a;
    uint m = month + 12 * a - 2;

    day = (7000 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7;

    return (day == 0) ? 7 : day;
}


/**
  * @brief  Structured time to Unixtime conversion
  * @param  time: Structured time data
  * @return Timestamp in Unixtime format
  */
uint32 TimeToUnixTime (const TTime& time)
{
    uint16 tmp   = 2000 + time.year - 1970;
    uint32 uTime = tmp * 365 * 24 * 60 * 60;

    // Leap years
    tmp    = (tmp + 1) / 4;
    uTime += (tmp * 24 * 60 * 60);
    tmp    = ((time.year % 4 == 0 && time.year % 100 != 0) || time.year % 400 == 0);

    // Number of days up to current month (without correction)
    uint16 days;
    days   = (((time.month - 1) * 367) + 5) / 12;
    days  -= (time.month > 2) ? ((tmp) ? 1 : 2) : 0;
    uTime += (days * 24 * 60 * 60);

    uTime += (24 * 60 * 60 * (time.day - 1));
    uTime += (60 * 60 * time.hour);
    uTime += (60 * time.min);
    uTime += time.sec;

    return uTime;
}


/**
  * @brief  Unixtime to structured time conversion
  * @param  time: Unixtime value
  * @param  pTm: Pointer to variable to save structured time data
  */
void UnixTimeToTime (uint32 time, TTime* pTm)
{
    UNUSED_VAR(time);
    UNUSED_VAR(pTm);

    // FIXME: complete implementation
}


/**
  * @brief  Structured time value addition
  * @param  pTm: Structured time data
  * @param  field: Field type of structured time which should be changed
  * @param  value: Field value to add
  */
void TimeAdd (TTime* pTm, TTimeType field, uint value)
{
    if (!pTm) return;

    if (field == TIME_TYPE_SEC)
    {
        value += pTm->sec;
        pTm->sec = value % 60;
        value /= 60;
        if (value) field = TIME_TYPE_MIN;
    }

    if (field == TIME_TYPE_MIN)
    {
        value += pTm->min;
        pTm->min = value % 60;
        value /= 60;
        if (value) field = TIME_TYPE_HOUR;
    }

    if (field == TIME_TYPE_HOUR)
    {
        value += pTm->hour;
        pTm->hour = value % 24;
        value /= 24;
        if (value) field = TIME_TYPE_DAY;
    }

    if (field == TIME_TYPE_DAY)
    {
        uint monthCount = 0;

        value += pTm->day;
        do
        {
            uint monthOffset = pTm->month - 1 + monthCount;
            uint year = 2000 + pTm->year + monthOffset / 12;
            bool isLeap = ( (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) );
            uint index = monthOffset % 12;
            uint maxDays = g_tableDays[index];
            if (index == 1 && isLeap) ++maxDays;

            if (value < maxDays) break;

            value -= maxDays;
            ++monthCount;

        } while (value);

        pTm->day = value;
        value = monthCount;

        if (value) field = TIME_TYPE_MONTH;
    }

    if (field == TIME_TYPE_MONTH)
    {
        value += pTm->month;
        pTm->month = value % 12;
        value /= 12;
        if (value) field = TIME_TYPE_YEAR;
    }

    if (field == TIME_TYPE_YEAR)
    {
        pTm->year += value;
    }

    // Month value validation
    uint index = pTm->month - 1;
    uint maxDays = g_tableDays[index];

    if (pTm->day > maxDays)
    {
        pTm->day -= maxDays;
        ++pTm->month;

        if (pTm->month > 12) ++pTm->year;
    }
}
