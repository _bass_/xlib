//*****************************************************************************
//
// @brief   Maths functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "misc/math.h"


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static const uchar table_crc8[256] = 
    {
        0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
        0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
        0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
        0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
        0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
        0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
        0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
        0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
        0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
        0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
        0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
        0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
        0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
        0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
        0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
        0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35
    };

    const uint16 table_crc16[256] = 
    {
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
    };


/**
  * @brief  CRC8 calculation
  * @param  data: Pointer to data
  * @param  size: Data size
  * @param  crc: Initial CRC value
  * @return Calculated CRC value
  */
uchar crc8 (const void* data, uint size, uchar crc)
{
    const uchar* ptr = (const uchar*) data;
    
    for(; size; --size)
    {
        crc = table_crc8[crc ^ *ptr++]; 
    }
    
    return crc;
}


/**
  * @brief  CRC16 CCITT calculation (x^16 + x^12 + x^5 + 1, X.25, HDLC, XMODEM, Bluetooth, SD...) 
  * @param  data: Pointer to data
  * @param  size: Data size
  * @param  crc: Initial CRC value
  * @return Calculated CRC value
  */
uint16 crc16_ccitt (const void* data, uint size, uint crc) 
{
    const uchar* buff = (const uchar*) data;
    
    for (; size; --size)
    {
        crc = crc ^ (uint)*buff++ << 8;
        
        for (uint i = 0; i < 8; i++)
        {
            if (crc & 0x8000) crc = crc << 1 ^ 0x1021;
            else              crc = crc << 1;
        } 
    }
    
    return (uint16)crc;
}


/**
  * @brief  CRC16 CCITT calculation (x^16 + x^12 + x^5 + 1, X.25, HDLC, XMODEM, Bluetooth, SD...) 
  * @param  data: Pointer to data
  * @param  size: Data size
  * @param  crc: Initial CRC value
  * @return Calculated CRC value
  */
uint16 crc16_ccitt_tab (const void* data, uint size, uint crc)
{
    const char* ptr = (const char*) data;
    
    for(; size; --size)
    {
        crc = table_crc16[(crc >> 8) & 0xFF] ^ (crc << 8) ^ *ptr++;
    }
    
    return (uint16)crc;
}


/**
 * @brief  CRC32 calculation (x32 + x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x + 1)
 *         Is used in: Ethernet, V.42, MPEG-2, PNG, POSIX cksum, Arj, Lha32, Rar, Zip...
 * @param  data: Pointer to data
 * @param  size: Data size
 * @param  crc: Initial CRC value
 * @return Calculated CRC value
 */
uint32 crc32 (const void* data, uint size, uint crc)
{
    static const uint32 table[256] =
    {
        0u,          1996959894u, 3993919788u, 2567524794u, 124634137u,  1886057615u, 3915621685u, 2657392035u,
        249268274u,  2044508324u, 3772115230u, 2547177864u, 162941995u,  2125561021u, 3887607047u, 2428444049u,
        498536548u,  1789927666u, 4089016648u, 2227061214u, 450548861u,  1843258603u, 4107580753u, 2211677639u,
        325883990u,  1684777152u, 4251122042u, 2321926636u, 335633487u,  1661365465u, 4195302755u, 2366115317u,
        997073096u,  1281953886u, 3579855332u, 2724688242u, 1006888145u, 1258607687u, 3524101629u, 2768942443u,
        901097722u,  1119000684u, 3686517206u, 2898065728u, 853044451u,  1172266101u, 3705015759u, 2882616665u,
        651767980u,  1373503546u, 3369554304u, 3218104598u, 565507253u,  1454621731u, 3485111705u, 3099436303u,
        671266974u,  1594198024u, 3322730930u, 2970347812u, 795835527u,  1483230225u, 3244367275u, 3060149565u,
        1994146192u, 31158534u,   2563907772u, 4023717930u, 1907459465u, 112637215u,  2680153253u, 3904427059u,
        2013776290u, 251722036u,  2517215374u, 3775830040u, 2137656763u, 141376813u,  2439277719u, 3865271297u,
        1802195444u, 476864866u,  2238001368u, 4066508878u, 1812370925u, 453092731u,  2181625025u, 4111451223u,
        1706088902u, 314042704u,  2344532202u, 4240017532u, 1658658271u, 366619977u,  2362670323u, 4224994405u,
        1303535960u, 984961486u,  2747007092u, 3569037538u, 1256170817u, 1037604311u, 2765210733u, 3554079995u,
        1131014506u, 879679996u,  2909243462u, 3663771856u, 1141124467u, 855842277u,  2852801631u, 3708648649u,
        1342533948u, 654459306u,  3188396048u, 3373015174u, 1466479909u, 544179635u,  3110523913u, 3462522015u,
        1591671054u, 702138776u,  2966460450u, 3352799412u, 1504918807u, 783551873u,  3082640443u, 3233442989u,
        3988292384u, 2596254646u, 62317068u,   1957810842u, 3939845945u, 2647816111u, 81470997u,   1943803523u,
        3814918930u, 2489596804u, 225274430u,  2053790376u, 3826175755u, 2466906013u, 167816743u,  2097651377u,
        4027552580u, 2265490386u, 503444072u,  1762050814u, 4150417245u, 2154129355u, 426522225u,  1852507879u,
        4275313526u, 2312317920u, 282753626u,  1742555852u, 4189708143u, 2394877945u, 397917763u,  1622183637u,
        3604390888u, 2714866558u, 953729732u,  1340076626u, 3518719985u, 2797360999u, 1068828381u, 1219638859u,
        3624741850u, 2936675148u, 906185462u,  1090812512u, 3747672003u, 2825379669u, 829329135u,  1181335161u,
        3412177804u, 3160834842u, 628085408u,  1382605366u, 3423369109u, 3138078467u, 570562233u,  1426400815u,
        3317316542u, 2998733608u, 733239954u,  1555261956u, 3268935591u, 3050360625u, 752459403u,  1541320221u,
        2607071920u, 3965973030u, 1969922972u, 40735498u,   2617837225u, 3943577151u, 1913087877u, 83908371u,
        2512341634u, 3803740692u, 2075208622u, 213261112u,  2463272603u, 3855990285u, 2094854071u, 198958881u,
        2262029012u, 4057260610u, 1759359992u, 534414190u,  2176718541u, 4139329115u, 1873836001u, 414664567u,
        2282248934u, 4279200368u, 1711684554u, 285281116u,  2405801727u, 4167216745u, 1634467795u, 376229701u,
        2685067896u, 3608007406u, 1308918612u, 956543938u,  2808555105u, 3495958263u, 1231636301u, 1047427035u,
        2932959818u, 3654703836u, 1088359270u, 936918000u,  2847714899u, 3736837829u, 1202900863u, 817233897u,
        3183342108u, 3401237130u, 1404277552u, 615818150u,  3134207493u, 3453421203u, 1423857449u, 601450431u,
        3009837614u, 3294710456u, 1567103746u, 711928724u,  3020668471u, 3272380065u, 1510334235u, 755167117u,
     };

    if (!data || !size) return crc;

    const uint8* ptr = (const uint8*)data;

    while (size--)
    {
        crc = table[(*ptr ^ crc) & 0xff] ^ (crc >> 8);
        ++ptr;
    }

    return (crc ^ 0xffffffff);
}
