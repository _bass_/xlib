//*****************************************************************************
//
// @brief   FIFO buffer
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "misc/fifo.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"


/**
  * @brief  Constructor
  * @param  size: Buffer size
  * @param  pBuff: [Optional] Pointer to allocated memory for the buffer
  */
TFifo::TFifo (uint size, void* pBuff)
{
    assert(size == 0);
    
    if (pBuff)
    {
        m_pBuff = (char*) pBuff;
        m_isBuffAlloc = false;
    }
    else
    {
        m_pBuff = (char*) malloc(size);
        assert(!m_pBuff);
        m_isBuffAlloc = true;
    }

    m_size = size;
    m_rd = 0;
    m_wr = 0;
}


/**
  * @brief  Destructor
  * @param  None
  */
TFifo::~TFifo ()
{
    if (m_isBuffAlloc)
    {
        free(m_pBuff);
    }
}


/**
  * @brief  Save byte in the buffer
  * @param  c: Data byte
  * @return 0 on success or negative error code
  */
int TFifo::Push (const uchar c)
{
    Kernel::EnterCritical();
    
    if ( !GetFree() )
    {
        Kernel::ExitCritical();
        return -1;
    }

    m_pBuff[m_wr] = c;
    if (++m_wr >= m_size) m_wr = 0;
    
    Kernel::ExitCritical();

    return 0;
}


/**
  * @brief  Reading byte from the buffer
  * @param  None
  * @return Byte value or negative error code
  */
int TFifo::Pop (void)
{
    Kernel::EnterCritical();
    
    if ( !GetCount() )
    {
        Kernel::ExitCritical();
        return -1;
    }

    char c = m_pBuff[m_rd];
    if (++m_rd >= m_size) m_rd = 0;
    
    Kernel::ExitCritical();

    return c;
}


/**
  * @brief  Reading data from the buffer
  * @param  buff: Pointer to buffer to save read data
  * @param  size: Buffer size
  * @return Amount of read data
  */
uint TFifo::Read (void* buff, uint size)
{
    Kernel::EnterCritical();

    uint count = GetCount();
    if (count > size) count = size;
    size = count;
    
    char* pBuff = (char*) buff;
    while (count)
    {
        int c = Pop();
        if (c < 0) break;
        
        *pBuff++ = (uchar) c;
        count--;
    }
    
    Kernel::ExitCritical();

    return (size - count);
}


/**
  * @brief  Writing data to the buffer
  * @param  data: Pointer to data for save
  * @param  size: Data size
  * @return Amount of saved in the buffer data
  */
uint TFifo::Write (const void* data, uint size)
{
    Kernel::EnterCritical();
    
    uint freeSize = GetFree();
    uint count = (freeSize > size) ? size : freeSize;
    size = count;
    
    const char* pBuff = (const char*) data;
    while (count && Push(*pBuff) >= 0)
    {
        count--;
        pBuff++;
    }
    
    Kernel::ExitCritical();

    return (size - count);
}


/**
  * @brief  Reading data without removing from the buffer
  * @param  buff: Pointer to buffer to save read data
  * @param  size: Buffer size
  * @return Amount of read data
  */
uint TFifo::Peek (void* buff, uint size)
{
    uint rd = m_rd;
    uint res = Read(buff, size);

    // Read index restoration
    m_rd = rd;

    return res;
}


/**
  * @brief  Buffer size calculation available for linear write
  * @param  pLen: Pointer to cariable to save linear buffer size
  * @return Pointer to the begining of the linear buffer
  */
void* TFifo::GetLineBuffWr (uint* pLen)
{
    uint len = (m_rd > m_wr) ? m_rd - m_wr : m_size - m_wr;
    *pLen = len;

    return &m_pBuff[m_wr];
}


/**
  * @brief  Buffer size calculation available for linear read
  * @param  pLen: Pointer to cariable to save linear buffer size
  * @return Pointer to the begining of the linear buffer
  */
void* TFifo::GetLineBuffRd (uint* pLen)
{
    uint len = (m_rd > m_wr) ? m_size - m_rd : m_wr - m_rd;
    *pLen = len;

    return &m_pBuff[m_rd];
}


/**
  * @brief  Reading pointer modification. Emuation of reading data.
  * @param  len: Number of read data
  */
void TFifo::SetRead (uint len)
{
    Kernel::EnterCritical();
    
    len %= m_size;
    m_rd += len;

    if (m_rd >= m_size)
    {
        m_rd -= m_size;
    }
    
    Kernel::ExitCritical();
}


/**
  * @brief  Writing pointer modification. Emuation of writing data.
  * @param  len: Number of written data
  */
void TFifo::SetWrite (uint len)
{
    Kernel::EnterCritical();
    
    len %= m_size;
    m_wr += len;

    if (m_wr >= m_size)
    {
        m_wr -= m_size;
    }
    
    Kernel::ExitCritical();
}
