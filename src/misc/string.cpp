//*****************************************************************************
//
// @brief   String functions
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "misc/string.h"
#include <ctype.h>
#include <string.h>


/**
  * @brief  Hex-string to binary conversion
  * @param  pHexStr: Hex-string. Will be replaced with the result.
  * @return Number of bytes in result or negative error code
  */
int HexToRaw (char* pHexStr)
{
    if (!pHexStr) return -1;

    char* pDst = pHexStr;
    int len = 0;

    while (1)
    {
        char a = *pHexStr;
        if (!a) break;

        char b = *(pHexStr + 1);
        // Must be even number of symbols (1 byte == 2 symbols)
        if (!b) return -1;

        if ( isdigit(a) )       a &= 0x0f;
        else if ( isupper(a) )  a -= 0x37;
        else                    a -= 0x57;

        if ( isdigit(b) )       b &= 0x0f;
        else if ( isupper(b) )  b -= 0x37;
        else                    b -= 0x57;

        *pDst = (a << 4) | b;

        ++len;
        ++pDst;
        pHexStr += 2;
    }

    *pDst = '\0';

    return len;
}


/**
  * @brief  Unicode to Windows-1251 string conversion
  * @param  data: Unicode string data
  * @param  size: Data size
  * @param  buff: Buffer for result string
  * @return Result string length or negative error code
  */
int UnicodeToWin1251 (const char* data, uint size, char* buff)
{
    if (!data || !size) return -1;
    // One unicode symbol = 2 bytes
    if (size & 0x01) return -2;

    int len = 0;
    while (size >= 2)
    {
        // Little endian
        uint16 c = *data | *(data + 1) << 8;

        // Windows-1251:
        //     Latin symbols    : symbol code
        //     Russian symbols  : 0xC0  - 0xFF  (ё - 0xB8 , Ё - 0xA8)
        // Unicode:
        //     Latin symbols    : 0x00 + symbol code
        //     Russian symbols  : 0x410 - 0x44F (ё - 0x451, Ё - 0x401)
        if (c == 0x401)                     c = 0xA8;   // Ё
        else if (c == 0x451)                c = 0xB8;   // ё
        else if (c >= 0x410 && c <= 0x44F)  c = c - 0x410 + 0xC0;

        *buff = (char)(c & 0x00ff);

        ++buff;
        ++len;
        data += 2;
        size -= 2;
    }

    *buff = '\0';

    return len;
}


/**
  * @brief  Windows-1251 to Unicode string conversion
  * @param  data: Windows-1251 string data
  * @param  size: Data size
  * @param  buff: Buffer for result string
  * @param  buffSize: Buffer size
  * @return Result string size or negative error code
  */
int Win1251ToUnicode (const char* data, uint size, char* buff, uint buffSize)
{
    if (!data || !size || !buff || buffSize < size * 2) return -1;

    int len = 0;
    while (size--)
    {
        uint16 c = *data++;

        // Unicode:
        //     Latin symbols    : 0x00 + symbol code
        //     Russian symbols  : 0x410 - 0x44F (ё - 0x451, Ё - 0x401)
        // Windows-1251:
        //     Latin symbols    : symbol code
        //     Russian symbols  : 0xC0  - 0xFF  (ё - 0xB8 , Ё - 0xA8)
        if (c == 0xA8)      c = 0x401;  // Ё
        else if (c == 0xB8) c = 0x451;  // ё
        else if (c >= 0xC0) c = c - 0xC0 + 0x410;

        // Little endian
        *buff++ = (char)(c & 0xff);
        *buff++ = (char)( (c >> 8) & 0xff );
        
        len += sizeof(c);
    }

    return len;
}


/**
  * @brief  Big-endian to little-endian conversion for 16-bit words
  * @param  data: Pointer to original data, will be replaced by the result
  * @param  size: Data size
  * @return 0 on success or negative error code
  */
int BeToLeWord (void* data, uint size)
{
    if (size & 0x01) return -1;
    
    char* ptr = (char*)data;
    while ( size >= sizeof(uint16) )
    {
        char c = *ptr;
        *ptr = *(ptr + 1);
        *(ptr + 1) = c;
        ptr += 2;
        size -= sizeof(uint16);
    }
    
    return 0;
}


/**
  * @brief  Little-endian to big-endian conversion for 16-bit words
  * @param  data: Pointer to original data, will be replaced by the result
  * @param  size: Data size
  * @return 0 on success or negative error code
  */
int LeToBeWord (void* data, uint size)
{
    // For uint16 that's just byte swapping
    return BeToLeWord(data, size);
}


/**
  * @brief  String with phone number format (international) verification
  * @param  phone: String with phone number
  * @return true - phone is valid
  */
bool IsValidPhone (const char* phone)
{
    if (!phone || !*phone) return false;
    
    uint len = strlen(phone);
    if (len <= 3 || len >= 16) return false;
    
    if (phone[0] != '+') return false;
    
    for (uint i = 1; i < len; ++i)
    {
        if ( !isdigit(phone[i]) ) return false;
    }

    return true;
}
