//*****************************************************************************
//
// @brief   NMEA 0183 protocol parser
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "parsers/nmea.h"
#include <stdlib.h>
#include <string.h>


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Message descriptor
    typedef struct
    {
        CNmea::TMsgType type;
        const char      name[4];
        int             (*handler) (uint fieldNum, const char* data, void* pMsg);
    } TMsgDesc;

    // Coordanate field type
    typedef enum
    {
        COORD_TYPE_LATITUDE,
        COORD_TYPE_LATITUDE_DIR,
        COORD_TYPE_LONGTITUDE,
        COORD_TYPE_LONGTITUDE_DIR
    } TCoordType;


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------


    static const TMsgDesc* getDesc (CNmea::TMsgType type);
    static const TMsgDesc* getDesc (const char* name);
    static float getCoordInDeg (const char* str);
    static float getSpeedInKmh (const char* str);
    static bool parseTime (const char* str, CNmea::TTime* pValue);
    static bool parseDate (const char* str, CNmea::TDate* pValue);
    static bool parseCoordinates (TCoordType type, const char* str, CNmea::TCoord* pValue);

    static int parserGga (uint fieldNum, const char* data, void* pMsg);
    static int parserGll (uint fieldNum, const char* data, void* pMsg);
    static int parserGsa (uint fieldNum, const char* data, void* pMsg);
    static int parserGsv (uint fieldNum, const char* data, void* pMsg);
    static int parserRmc (uint fieldNum, const char* data, void* pMsg);
    static int parserVtg (uint fieldNum, const char* data, void* pMsg);
    static int parserZda (uint fieldNum, const char* data, void* pMsg);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Messages descriptors
    static const TMsgDesc g_descList[] =
    {
        { CNmea::GGA, "GGA", parserGga },
        { CNmea::GLL, "GLL", parserGll },
        { CNmea::GSA, "GSA", parserGsa },
        { CNmea::GSV, "GSV", parserGsv },
        { CNmea::RMC, "RMC", parserRmc },
        { CNmea::VTG, "VTG", parserVtg },
        { CNmea::ZDA, "ZDA", parserZda }
    };


/**
  * @brief  Constructor
  * @param  None
  */
CNmea::CNmea ():
    m_buffIndex(0),
    m_msgType(MSG_TYPE_COUNT),
    m_validMask(0)
{
    memset( &m_storage, 0, sizeof(m_storage) );
}


/**
  * @brief  Transfer data to the parser
  * @param  data: Raw data
  * @param  size: Data size
  */
void CNmea::Parse (const char* data, uint size)
{
    if (!data || !size) return;

    while (size)
    {
        uint processedLen = 0;

        // Message type searching and processing
        if (m_msgType == MSG_TYPE_COUNT)
        {
            processedLen = processType(data, size);

            if (m_msgType != MSG_TYPE_COUNT)
            {
                // Message data initialization
                m_crc = 0;
                addToCrc(m_buff, m_buffIndex);

                Invalidate(m_msgType);
                resetData(m_msgType);

                m_buffIndex = 0;
                m_fieldIndex = 0;
            }
        }
        else
        {
            processedLen = processData(data, size);
        }

        if (!processedLen || processedLen > size)
        {
            resetParser();
            break;
        }

        size -= processedLen;
        data += processedLen;
    }
}


/**
  * @brief  Message data validation
  * @param  type: Message type
  * @return true - message was parsed and contains valid data
  */
bool CNmea::IsValid (CNmea::TMsgType type) const
{
    return (type < MSG_TYPE_COUNT) ? ( m_validMask & (1u << type) ) : false;
}


/**
  * @brief  Reset flag of valid message
  * @param  type: Message type
  */
void CNmea::Invalidate (CNmea::TMsgType type)
{
    if (type < MSG_TYPE_COUNT)
    {
        m_validMask &= ~(1u << type);
    }
}


/**
  * @brief  Message data reading
  * @param  type: Message type
  * @return Pointer to message data
  */
const void* CNmea::GetMsg (CNmea::TMsgType type)
{
    if ( !IsValid(type) ) return NULL;

    return getMsgStorage(type);
}


/**
  * @brief  Find message in storage
  * @param  type: Message type
  * @param  pMsgSize: Pointe to variable to save message size
  * @return Pointer to message data
  */
void* CNmea::getMsgStorage (CNmea::TMsgType type, uint* pMsgSize)
{
    switch (type)
    {
        case GGA:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.gga);
        return &m_storage.gga;

        case GLL:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.gll);
        return &m_storage.gll;

        case GSA:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.gsa);
        return &m_storage.gsa;

        case GSV:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.gsv);
        return &m_storage.gsv;

        case RMC:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.rmc);
        return &m_storage.rmc;

        case VTG:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.vtg);
        return &m_storage.vtg;

        case ZDA:
            if (pMsgSize) *pMsgSize = sizeof(&m_storage.zda);
        return &m_storage.zda;

        default: break;
    }

    return NULL;
}


/**
  * @brief  Reset message data in storage
  * @param  type: Message type
  */
void CNmea::resetData (CNmea::TMsgType type)
{
    const TMsgDesc* pDesc = getDesc(type);
    if (!pDesc) return;

    uint msgSize = 0;
    void* pData = getMsgStorage(type, &msgSize);
    if (!pData) return;

    memset(pData, 0, msgSize);
}


/**
  * @brief  Reset parser state
  * @param  None
  */
void CNmea::resetParser (void)
{
    m_buffIndex = 0;
    m_fieldIndex = 0;
    m_msgType = MSG_TYPE_COUNT;
}


/**
  * @brief  Find start of message and its type detection
  * @param  data: NMEA data block
  * @param  size: Data size
  * @return Number of processed data bytes
  */
uint CNmea::processType (const char* data, uint size)
{
    uint count = 0;

    while (count < size)
    {
        if ( m_buffIndex >= sizeof(m_buff) ) m_buffIndex = 0;

        m_buff[m_buffIndex++] = *data++;
        ++count;

        if (m_buff[0] != '$')
        {
            m_buffIndex = 0;
            continue;
        }

        if (m_buffIndex < sizeof("$GPABC") - 1) continue;

        const TMsgDesc* pDesc = getDesc(&m_buff[3]);
        if (pDesc) m_msgType = pDesc->type;

        break;
    }

    return count;
}


/**
  * @brief  Message data processing
  * @param  data: NMEA data block
  * @param  size: Data size
  * @return Number of processed data bytes
  */
uint CNmea::processData (const char* data, uint size)
{
    uint count = 0;

    for (; count < size; ++count, ++data)
    {
        if ( m_buffIndex >= sizeof(m_buff) ) break;

        char byte = *data;

        if ( !m_buffIndex || !(byte == ',' || byte == '*' || byte == '\n') )
        {
            m_buff[m_buffIndex++] = byte;
            continue;
        }

        m_buff[m_buffIndex] = '\0';

        if (m_buff[0] == ',')
        {
            // Field data processing
            const TMsgDesc* pDesc = getDesc(m_msgType);
            if (!pDesc || !pDesc->handler)
            {
                resetParser();
                break;
            }

            void* pData = getMsgStorage(m_msgType);
            if (!pData)
            {
                resetParser();
                break;
            }

            int result = pDesc->handler(m_fieldIndex, &m_buff[1], pData);
            if (result)
            {
                resetParser();
                break;
            }

            ++m_fieldIndex;
            addToCrc(m_buff, m_buffIndex);
        }
        else if (m_buff[0] == '*')
        {
            // CRC verification
            uint crc = strtoul(&m_buff[1], NULL, 16);

            if (crc <= 0xff && (char)crc == m_crc)
            {
                m_validMask |= 1u << m_msgType;
            }
        }

        m_buff[0] = byte;
        m_buffIndex = 1;
    }

    return count;
}


/**
  * @brief  On the fly message CRC calculation
  * @param  data: NMEA data block
  * @param  size: Data size
  */
void CNmea::addToCrc (const char* data, uint size)
{
    while (size--)
    {
        char byte = *data++;

        if (byte == '$') continue;
        if (byte == '*') break;

        m_crc ^= byte;
    }
}


/**
  * @brief  Get message descriptor
  * @param  type: Message type
  * @return Message descriptor or NULL on error
  */
const TMsgDesc* getDesc (CNmea::TMsgType type)
{
    for (uint i = 0; i < ARR_COUNT(g_descList); ++i)
    {
        const TMsgDesc* pDesc = &g_descList[i];
        if (pDesc->type != type) continue;

        return pDesc;
    }

    return NULL;
}


/**
  * @brief  Get message descriptor
  * @param  name: Message name
  * @return Message descriptor or NULL on error
  */
const TMsgDesc* getDesc (const char* name)
{
    for (uint i = 0; i < ARR_COUNT(g_descList); ++i)
    {
        const TMsgDesc* pDesc = &g_descList[i];
        if ( memcmp(name, pDesc->name, sizeof(TMsgDesc::name) - 1) ) continue;

        return pDesc;
    }

    return NULL;
}


/**
  * @brief  Coordinates converting from "DDMM.MMMM" format into "DD.DDDDDD"
  * @param  str: Coordinate value
  * @return Converted coordinate value or negative error code
  */
float getCoordInDeg (const char* str)
{
    do
    {
        if (!str) break;

        uint len = (uint) strlen(str);
        if (!len) return 0;

        double val = atof(str);
        if (val < 0) break;

        int deg = (int) (val / 100);
        double min = val - deg * 100;

        return (float) (deg + min / 60);
    } while (0);

    return -1;
}


/**
  * @brief  Speed converting into km/h
  * @param  str: Speed value (text)
  * @return Converted speed value or negative error code
  */
float getSpeedInKmh (const char* str)
{
    do
    {
        if (!str) break;

        uint len = (uint) strlen(str);
        if (!len) return 0;


        float val = (float) atof(str);
        if (val < 0) break;

        return val * 1.852f;
    } while (0);

    return -1.0;
}


/**
  * @brief  Time parsing in format "hhmmss.ms[s]"
  * @param  str: Time string
  * @param  pValue: Variable to save parsed value
  * @return True on success, otherwise - false
  */
bool parseTime (const char* str, CNmea::TTime* pValue)
{
    if (!str || !pValue) return false;

    uint len = (uint) strlen(str);

    do
    {
        if (len < sizeof("hhmmss") - 1) break;

        char data[4] = {};
        int val;

        data[0] = *str++;
        data[1] = *str++;
        val = atoi(data);
        if (val < 0 || val > 24) break;
        pValue->hour = (uint8) val;

        data[0] = *str++;
        data[1] = *str++;
        val = atoi(data);
        if (val < 0 || val > 59) break;
        pValue->min = (uint8) val;

        data[0] = *str++;
        data[1] = *str++;
        val = atoi(data);
        if (val < 0 || val > 59) break;
        pValue->sec = (uint8) val;

        if (*str++ == '.')
        {
            data[0] = (len > sizeof("hhmmss.") - 1) ? *str++ : 0;
            data[1] = (len > sizeof("hhmmss.m") - 1) ? *str++ : 0;
            data[2] = (len > sizeof("hhmmss.ms") - 1) ? *str++ : 0;
            val = atoi(data);

            if (data[2])
            {
                if (val < 0 || val > 999) break;
                pValue->ms = (uint16) val;
            }
            else
            {
                if (val < 0 || val > 99) break;
                pValue->ms = (uint16) val * 10;
            }
        }

        return true;

    } while (0);

    return false;
}


/**
  * @brief  Date parsing in format "ddmmyy"
  * @param  str: Date string
  * @param  pValue: Variable to save parsed value
  * @return True on success, otherwise - false
  */
bool parseDate (const char* str, CNmea::TDate* pValue)
{
    if (!str || !pValue) return false;

    uint len = (uint) strlen(str);

    do
    {
        if (len < sizeof("ddmmyy") - 1) break;

        uint val = (str[0] - '0') * 10 + (str[1] - '0');
        if (!val || val > 31) break;
        pValue->day = (uint8) val;
        str += 2;

        val = (str[0] - '0') * 10 + (str[1] - '0');
        if (!val || val > 12) break;
        pValue->month = (uint8) val;
        str += 2;

        val = (str[0] - '0') * 10 + (str[1] - '0');
        if (val > 99) break;
        val += (val >= 70) ? 1900 : 2000;
        pValue->year = (uint16) val;

        return true;
    } while (0);

    return false;
}


/**
  * @brief  Coordinates parsing
  * @param  type: Type of coordinate to parse
  * @param  str: Text value
  * @param  pValue: Variable to save parsed value
  * @return True on success, otherwise - false
  */
bool parseCoordinates (TCoordType type, const char* str, CNmea::TCoord* pValue)
{
    if (!str || !pValue) return false;

    uint len = (uint) strlen(str);

    switch (type)
    {
        case COORD_TYPE_LATITUDE:
            pValue->latitude = getCoordInDeg(str);
            if (pValue->latitude < 0) break;
        return true;

        // N (+) or S (-) latitude
        case COORD_TYPE_LATITUDE_DIR:
            if (len != 1) break;

            if (*str == 'N')
            {
                if (pValue->latitude >= 0) return true;
            }
            else if (*str == 'S')
            {
                if (pValue->latitude > 0) pValue->latitude = -pValue->latitude;
                return true;
            }
        break;

        case COORD_TYPE_LONGTITUDE:
            pValue->longitude = getCoordInDeg(str);
            if (pValue->longitude < 0) break;
        return true;

        // E (+) or W (-) longitude
        case COORD_TYPE_LONGTITUDE_DIR:
            if (len != 1) break;

            if (*str == 'E')
            {
                if (pValue->longitude >= 0) return true;
            }
            else if (*str == 'W')
            {
                if (pValue->longitude > 0) pValue->longitude = -pValue->longitude;
                return true;
            }
        break;
    }

    return false;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserGga (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgGga* ptr = (CNmea::TMsgGga*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    switch (fieldNum)
    {
        // Time
        case 0:
            if ( !parseTime(data, &ptr->time) ) return -1;
        break;

        // Latitude
        case 1:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE, data, &ptr->coord) ) return -10;
        break;

        // N (+) or S (-) latitude
        case 2:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE_DIR, data, &ptr->coord) ) return -20;
        break;

        // Longitude
        case 3:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE, data, &ptr->coord) ) return -30;
        break;

        // E (+) or W (-) longitude
        case 4:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE_DIR, data, &ptr->coord) ) return -40;
        break;

        // GPS Quality Indicator
        case 5:
            ptr->quality = *data - '0';
        break;

        // Number of satellites in view
        case 6:
        {
            int val = atoi(data);
            if (val < 0) return -60;
            ptr->satInView = (uint8) val;
        }
        break;

        // Horizontal Dilution of precision
        case 7:
            ptr->hdop = (float) atof(data);
        break;

        // Antenna Altitude above/below mean-sea-level
        case 8:
            ptr->antAltitude = (float) atof(data);
        break;

        // Units of antenna altitude
        case 9:
            if (*data != 'M') return -90;
        break;

        // Geoidal separation
        case 10:
            ptr->geoSeparation = (float) atof(data);
        break;

        // Units of geoidal separation
        case 11:
            if (*data != 'M') return -110;
        break;

        // Age of differential GPS data
        case 12:
            ptr->ageDiff = (float) atof(data);
            if (ptr->ageDiff < 0) return -120;
        break;

        // Differential reference station ID, 0000-1023
        case 13:
        {
            int val = atoi(data);
            if (val < 0) return -130;
            ptr->stationId = (uint16) val;
        }
        break;

        default: return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserGll (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgGll* ptr = (CNmea::TMsgGll*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    switch (fieldNum)
    {
        // Latitude
        case 0:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE, data, &ptr->coord) ) return -1;
        break;

        // N (+) or S (-) latitude
        case 1:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE_DIR, data, &ptr->coord) ) return -10;
        break;

        // Longitude
        case 2:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE, data, &ptr->coord) ) return -20;
        break;

        // E (+) or W (-) longitude
        case 3:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE_DIR, data, &ptr->coord) ) return -30;
        break;

        // Time
        case 4:
            if ( !parseTime(data, &ptr->time) ) return -40;
        break;

        // Valid
        case 5:
            ptr->valid = (*data == 'A');
        break;

        default: return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserGsa (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgGsa* ptr = (CNmea::TMsgGsa*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    // Selection mode
    if (fieldNum == 0)
    {
        if (len != 1) return -1;
        ptr->modeSelection = *data;
    }
    // Mode
    else if (fieldNum == 1)
    {
        if (len != 1) return -10;
        ptr->mode = *data - '0';
    }
    // ID of satellite used for fix (1..12)
    else if (fieldNum >= 2 && fieldNum <= 13)
    {
        int id = atoi(data);
        if (id < 0 || id > 99) return -20;
        ptr->satId[fieldNum - 2] = (uint8) id;
    }
    // PDOP in meters
    else if (fieldNum == 14)
    {
        if (len) ptr->pdop = (float) atof(data);
    }
    // HDOP in meters
    else if (fieldNum == 15)
    {
        ptr->hdop = (float) atof(data);
    }
    // VDOP in meters
    else if (fieldNum == 16)
    {
        ptr->vdop = (float) atof(data);
    }
    else
    {
        return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserGsv (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgGsv* ptr = (CNmea::TMsgGsv*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    // Total number of messages
    // Message number
    // Satellites in view
    if (fieldNum <= 2)
    {
        int count = atoi(data);
        if (count < 0) return -1;

        if (fieldNum == 0)      ptr->msgCount = (uint8) count;
        else if (fieldNum == 1) ptr->msgNum = (uint8) count;
        else                    ptr->satCount = (uint8) count;
    }
    // Satellite number
    // Elevation in degrees [0..90]
    // Azimuth in degrees to true [0..360]
    // SNR in dB [0..99], 0 - no signal
    else if (fieldNum >= 3 && fieldNum <= 18)
    {
        int val = atoi(data);
        if (val < 0) return -30;

        uint satIndex = ((fieldNum - 3) >> 2) & 0x03;
        uint valIndex = (fieldNum - 3) & 0x03;

        if (valIndex == 0)
        {
            if (val > 99) return -31;
            ptr->satelits[satIndex].id = (uint8) val;
        }
        else if (valIndex == 1)
        {
            if (val > 90) return -32;
            ptr->satelits[satIndex].elevation = (uint8) val;
        }
        else if (valIndex == 2)
        {
            if (val > 360) return -33;
            ptr->satelits[satIndex].azimuth = (uint16) val;
        }
        else
        {
            if (val > 99) return -34;
            ptr->satelits[satIndex].snr = (uint8) val;
        }
    }
    else
    {
        return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserRmc (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgRmc* ptr = (CNmea::TMsgRmc*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    switch (fieldNum)
    {
        // Time
        case 0:
            if ( !parseTime(data, &ptr->time) ) return -1;
        break;

        // Valid
        case 1:
            ptr->valid = (*data == 'A');
        break;

        // Latitude
        case 2:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE, data, &ptr->coord) ) return -20;
        break;

        // N (+) or S (-) latitude
        case 3:
            if ( !parseCoordinates(COORD_TYPE_LATITUDE_DIR, data, &ptr->coord) ) return -30;
        break;

        // Longitude
        case 4:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE, data, &ptr->coord) ) return -40;
        break;

        // E (+) or W (-) longitude
        case 5:
            if ( !parseCoordinates(COORD_TYPE_LONGTITUDE_DIR, data, &ptr->coord) ) return -50;
        break;

        // Speed
        case 6:
            ptr->speed = getSpeedInKmh(data);
            if (ptr->speed < 0) return -60;
        break;

        // Direction
        case 7:
            ptr->direction = (float) atof(data);
            if (ptr->direction < 0 || ptr->direction > 360) return -70;
        break;

        // Date
        case 8:
            if ( !parseDate(data, &ptr->date) ) return -80;
        break;

        // Magnetic Variation
        case 9:
            ptr->declination = (float) atof(data);
            if (ptr->declination < 0) return -90;
        break;

        // Magnetic Variation (E = -, W = +)
        case 10:
            if (len != 1) return -100;

            if (*data == 'E')
            {
                if (ptr->declination > 0) ptr->declination = -ptr->declination;
            }
            else if (*data == 'W')
            {
                if (ptr->declination < 0) return -101;
            }
            else
            {
                return -102;
            }
        break;

        default: return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserVtg (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgVtg* ptr = (CNmea::TMsgVtg*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    switch (fieldNum)
    {
        // Track Degrees (True)
        case 0:
            ptr->directionPole = (float) atof(data);
            if (ptr->directionPole < 0 || ptr->directionPole > 360) return -1;
        break;

        // True
        case 1:
            if (len != 1 || *data != 'T') return -10;
        break;

        // Track Degrees (Magnetic)
        case 2:
            ptr->directionMag = (float) atof(data);
            if (ptr->directionMag < 0 || ptr->directionMag > 360) return -20;
        break;

        // Magnetic
        case 3:
            if (len != 1 || *data != 'M') return -30;
        break;

        // Speed Knots
        case 4:
            ptr->speed = getSpeedInKmh(data);
            if (ptr->speed < 0) return -40;
        break;

        // Knots
        case 5:
            if (len != 1 || *data != 'N') return -50;
        break;

        // Speed KMH
        case 6:
            if (ptr->speed <= 0.001f)
            {
                ptr->speed = (float) atof(data);
                if (ptr->speed < 0) return -60;
            }
        break;

        // Knots
        case 7:
            if (len != 1 || *data != 'K') return -70;
        break;

        default: return -1000;
    }

    return 0;
}


/**
  * @brief  Message parsing
  * @param  fieldNum: Number of field in the message
  * @param  data: Message data (text)
  * @param  pMsg: Variable to save parsed data
  * @return 0 or negative error code
  */
int parserZda (uint fieldNum, const char* data, void* pMsg)
{
    CNmea::TMsgZda* ptr = (CNmea::TMsgZda*) pMsg;
    uint len = (uint) strlen(data);

    if (!len) return 0;

    switch (fieldNum)
    {
        // Time
        case 0:
            if ( !parseTime(data, &ptr->time) ) return -1;
        break;

        // Day
        case 1:
        {
            int val = atoi(data);
            if (val <= 0 || val > 31) return -10;
            ptr->date.day = (uint8) val;
        }
        break;

        // Month
        case 2:
        {
            int val = atoi(data);
            if (val <= 0 || val > 12) return -20;
            ptr->date.month = (uint8) val;
        }
        break;

        // Year
        case 3:
        {
            int val = atoi(data);
            if (val < 0 || val > 2099) return -30;
            ptr->date.year = (uint16) val;
        }
        break;

        // TZ hours
        case 4:
        {
            int val = atoi(data);
            if (val < -13 || val > 13) return -40;
            ptr->tzHours = (int8) val;
        }
        break;

        // TZ minutes
        case 5:
        {
            int val = atoi(data);
            if (val < 0 || val > 59) return -50;
            ptr->tzMin = (uint8) val;
        }
        break;

        default: return -1000;
    }

    return 0;
}
