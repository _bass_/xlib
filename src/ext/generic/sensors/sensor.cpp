//*****************************************************************************
//
// @brief   Generic sensors functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "sensors/sensor.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "misc/macros.h"
#include "misc/time.h"


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_DEBUG("sens", SENSOR_DEBUG_NAME, NULL, NULL);

    static list<CSensor*> g_sensorList;


/**
  * @brief  Constructor
  * @param  id: Sensor ID
  * @param  type: Sensor type
  * @param  pCb: List of sensor's callback functions
  */
CSensor::CSensor (uint32 id, uint16 type, const TSensCb* pCb) :
    m_id(id),
    m_type(type),
    m_pCb(pCb)
{
    setUpdateDelay(1);
    g_sensorList.push_back(this);
}


/**
  * @brief  Destructor
  * @param  None
  */
CSensor::~CSensor ()
{
    g_sensorList.remove(this);
}


/**
  * @brief  Get delay value before next update
  * @return Remaining time before update (ms)
  */
uint CSensor::GetUpdateDelay (void) const
{
    if (!m_updateTime) return (uint)-1;
    return ( IsTimeout(m_updateTime, 0) ) ? 0 : m_updateTime - Kernel::GetJiffies();
}


/**
  * @brief  Set configuration parameters
  * @param  params: Configuration parameters
  * @return 0 or negative error code
  */
int CSensor::Configure (const void* params)
{
    UNUSED_VAR(params);
    return 0;
}


/**
  * @brief  Sensor descriptor reading
  * @return Sensor descriptor or NULL on error
  */
const TSensChDesc* CSensor::GetChDesc (uint channel) const
{
    const TSensDesc* pSensDesc = GetDesc();
    if (!pSensDesc) return NULL;
    
    for (uint i = 0; i < pSensDesc->chCount; ++i)
    {
        if (channel == pSensDesc->channels[i].channel) return &pSensDesc->channels[i];
    }
    
    return NULL;
}


/**
  * @brief  Channel value reading (generic function)
  * @param  channel: Channel ID
  * @param  pType: Variable where channel value type will be saved
  * @param  buff: Channel value buffer
  * @param  size: Buffer size
  * @return Amount of data written in the buffer or negative error code
  */
int CSensor::GetValue (uint channel, TSensDType* pType, void* buff, uint size) const
{
    return getValue(channel, pType, buff, size);
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, bool* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(bool) );

    return (result == sizeof(bool) && type == SENSOR_DATA_TYPE_BOOL) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, int8* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(int8) );

    return (result == sizeof(int8) && type == SENSOR_DATA_TYPE_INT8) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, uint8* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(uint8) );

    return (result == sizeof(uint8) && type == SENSOR_DATA_TYPE_UINT8) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, int* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(int) );

    return (result == sizeof(int) && type == SENSOR_DATA_TYPE_INT) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, uint* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(uint) );

    return (result == sizeof(uint) && type == SENSOR_DATA_TYPE_UINT) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, int64* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(int64) );

    return (result == sizeof(int64) && type == SENSOR_DATA_TYPE_INT64) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, uint64* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(uint64) );

    return (result == sizeof(uint64) && type == SENSOR_DATA_TYPE_UINT64) ? 0 : -1;
}


/**
  * @brief  Channel value reading
  * @param  channel: Channel ID
  * @param  pVal: Variable where channel value will be saved
  * @return 0 or negative error code
  */
int CSensor::GetValue (uint channel, float* pVal) const
{
    TSensDType type;
    int result = getValue( channel, &type, pVal, sizeof(float) );

    return (result == sizeof(float) && type == SENSOR_DATA_TYPE_FLOAT) ? 0 : -1;
}


/**
  * @brief  Set channel value (generic function)
  * @param  channel: Channel ID
  * @param  type: Channel value type
  * @param  data: Pointer to channel value
  * @param  size: Value data size
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, TSensDType type, const void* data, uint size)
{
    return setValue(channel, type, data, size);
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, int8 val)
{
    return setValue( channel, SENSOR_DATA_TYPE_INT8, &val, sizeof(int8) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, bool val)
{
    return setValue( channel, SENSOR_DATA_TYPE_BOOL, &val, sizeof(bool) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, uint8 val)
{
    return setValue( channel, SENSOR_DATA_TYPE_UINT8, &val, sizeof(uint8) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, int val)
{
    return setValue( channel, SENSOR_DATA_TYPE_INT, &val, sizeof(int) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, uint val)
{
    return setValue( channel, SENSOR_DATA_TYPE_UINT, &val, sizeof(uint) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, int64 val)
{
    return setValue( channel, SENSOR_DATA_TYPE_INT64, &val, sizeof(int64) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, uint64 val)
{
    return setValue( channel, SENSOR_DATA_TYPE_UINT64, &val, sizeof(uint64) );
}


/**
  * @brief  Set channel value
  * @param  channel: Channel ID
  * @param  val: Channel value to set
  * @return 0 or negative error code
  */
int CSensor::SetValue (uint channel, float val)
{
    return setValue( channel, SENSOR_DATA_TYPE_FLOAT, &val, sizeof(float) );
}


/**
  * @brief  Sensor object creation
  * @param  id: Sensor ID
  * @param  type: Sensor type
  * @param  pCb: List of sensor's callback functions
  * @return Sensor object or NULL on error
  */
CSensor* CSensor::CreateSensor (uint32 id, uint16 type, const TSensCb* pCb)
{
    SECTION_FOREACH(xLib_sensors, const TSensDrvDesc, pDesc)
    {
        if (pDesc->type != type || !pDesc->creator) continue;
        return pDesc->creator(id, pCb);
    }

    return NULL;
}


/**
  * @brief  Sensor object searching
  * @param  id: Sensor ID
  * @return Sensor object or NULL on error
  */
CSensor* CSensor::GetSensor (uint32 id)
{
    for (list<CSensor*>::const_iterator i = g_sensorList.begin(); i != g_sensorList.end(); ++i)
    {
        CSensor* pSensor = *i;
        if (pSensor->GetId() == id) return pSensor;
    }

    return NULL;
}


/**
  * @brief  List of sensors objects
  * @return Pointer to sensor objects list
  */
const list<CSensor*>* CSensor::GetSensors (void)
{
    return &g_sensorList;
}


/**
  * @brief  Run sensor's callback funtion
  * @param  cbType: Type of callback function
  * @param  arg: Additional callback function arguments
  */
void CSensor::notify (uint cbType, void* arg)
{
    if (!m_pCb) return;

    const TSensCbDesc* pDesc = m_pCb->list;
    for (uint i = m_pCb->count; i; --i, ++pDesc)
    {
        if (pDesc->type != cbType) continue;
        if (pDesc->fn)
        {
            pDesc->fn(this, arg);
        }
        break;
    }
}


/**
  * @brief  Set delay before next sensor update
  * @param  delay: Delay value (ms)
  */
void CSensor::setUpdateDelay (uint delay)
{
    if (delay == SENSOR_UPDATE_DELAY_NEVER)
    {
        m_updateTime = 0;
    }
    else
    {
        m_updateTime = Kernel::GetJiffies() + delay;
        if (!m_updateTime) ++m_updateTime;
    }
    
    notify(SENSOR_CB_UPDATE_CHANGED, &delay);
}


/**
  * @brief  Get channel index in sensor descriptor
  * @param  channel: Channel ID
  * @return Channel index or negative error code
  */
int CSensor::getChIndex (uint channel) const
{
    const TSensDesc* pSensDesc = GetDesc();
    const TSensChDesc* pDesc = pSensDesc->channels;
    
    for (uint i = 0; i < pSensDesc->chCount; ++i, ++pDesc)
    {
        if (channel == pDesc->channel) return (int)i;
    }
    
    return -1;
}
