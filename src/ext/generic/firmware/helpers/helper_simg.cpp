//*****************************************************************************
//
// @brief   SIMG image helper
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "firmware/helpers/helper_simg.h"
#include "crypt/aes_cbc.h"
#include "crypt/sha256.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include <new>
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Image data block offset in a slot (bytes)
    #define SIMG_DATA_OFFSET            256

    STATIC_ASSERT(sizeof(CImgHelperSimg::TMetadata) <= SIMG_DATA_OFFSET);

    // Current version of SIMG image structure
    #define SIMG_META_VERSION           0


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Image encryption type
    typedef enum
    {
        CRYPT_TYPE_NONE,
        CRYPT_TYPE_AES_128_CBC,
        CRYPT_TYPE_COUNT
    } TCryptType;

    // Image data tag type
    typedef enum
    {
        TAG_TYPE_SHA256,
        TAG_TYPE_COUNT
    } TTagType;

    // Type of metadata signature
    typedef enum
    {
        SIGN_TYPE_SHA256_AES128_CBC,
        SIGN_TYPE_COUNT
    } TSignType;


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static IHash* createHasher (TTagType type);
    static IHash* createHasher (TSignType type);
    static ICrypt* createCoder (TCryptType type, const void* params, CImgHelperSimg::TCbGetKey cbGetKey);
    static ICrypt* createCoder (TSignType type, const void* params, CImgHelperSimg::TCbGetKey cbGetKey);
    static ICrypt* createAesCoder (uint keySize, const void* pIv, CImgHelperSimg::TCbGetKey cbGetKey);
    static uint getSignLen (TSignType type);


/**
  * @brief  Hasher object creation for data tag calculation
  * @param  type: Tag type
  * @return Hasher object or NULL on error
  */
IHash* createHasher (TTagType type)
{
    switch (type)
    {
        case TAG_TYPE_SHA256: return new CSha256();
        default: break;
    }

    return NULL;
}


/**
  * @brief  Hasher object creation for signature calculation
  * @param  type: Signature type
  * @return Hasher object or NULL on error
  */
IHash* createHasher (TSignType type)
{
    switch (type)
    {
        case SIGN_TYPE_SHA256_AES128_CBC: return new CSha256();
        default: break;
    }

    return NULL;
}


/**
  * @brief  Coder object creation for data decryption
  * @param  type: Encryption type
  * @param  params: Additional coder parameters
  * @param  cbGetKey: Function for encryption key reading
  * @return Coder object or NULL on error
  */
ICrypt* createCoder (TCryptType type, const void* params, CImgHelperSimg::TCbGetKey cbGetKey)
{
    if (!cbGetKey || !params) return NULL;

    ICrypt* coder = NULL;

    switch (type)
    {
        case CRYPT_TYPE_AES_128_CBC:
            coder = createAesCoder(AES_128_KEY_SIZE, params, cbGetKey);
        break;

        default: break;
    }

    return coder;
}


/**
  * @brief  Coder object creation for data signing
  * @param  type: Signature type
  * @param  params: Additional coder parameters
  * @param  cbGetKey: Function for encryption key reading
  * @return Coder object or NULL on error
  */
ICrypt* createCoder (TSignType type, const void* params, CImgHelperSimg::TCbGetKey cbGetKey)
{
    if (!cbGetKey || !params) return NULL;

    ICrypt* coder = NULL;

    switch (type)
    {
        case SIGN_TYPE_SHA256_AES128_CBC:
            coder = createAesCoder(AES_128_KEY_SIZE, params, cbGetKey);
        break;

        default: break;
    }

    return coder;
}


/**
  * @brief  AES coder object creation
  * @param  keySize: Encryption key size
  * @param  pIv: Pointer to IV data
  * @param  cbGetKey: Function for encryption key reading
  * @return Coder object or NULL on error
  */
ICrypt* createAesCoder (uint keySize, const void* pIv, CImgHelperSimg::TCbGetKey cbGetKey)
{
    if (!pIv || !cbGetKey) return NULL;

    ICrypt* coder = NULL;
    void* key = NULL;
    bool success = false;

    do
    {
        coder = new CAesCbc(keySize);
        if (!coder) break;

        key = malloc(keySize);
        if (!key) break;

        int res = cbGetKey(key, keySize, NULL);
        if ( (uint)res != keySize ) break;

        CAesCbc::TParams params = {key, pIv};
        res = coder->SetParams(&params);
        if (res) break;

        success = true;
    } while (0);

    if (key)
    {
        memset(key, 0x00, keySize);
        free(key);
    }

    if (coder && !success)
    {
        delete coder;
        coder = NULL;
    };

    return coder;
}


/**
  * @brief  Get signature size
  * @param  type: Signature size
  * @return Signature size or 0 on error
  */
uint getSignLen (TSignType type)
{
    switch (type)
    {
        case SIGN_TYPE_SHA256_AES128_CBC: return sizeof(CImgHelperSimg::TMetadata::meta.sign.sha256aes.value);
        default: break;
    }

    return 0;
}


/**
  * @brief  Constructor
  * @param  getKeyCallback: Function to read encryption key data
  */
CImgHelperSimg::CImgHelperSimg (TCbGetKey getKeyCallback) :
    IImgHelper(),
    m_cbGetKey(getKeyCallback),
    m_hasher(NULL),
    m_coder(NULL)
{
}


/**
  * @brief  Destructor
  * @param  None
  */
CImgHelperSimg::~CImgHelperSimg ()
{
    Reset();
}


/**
  * @brief  Reset helper's state
  * @param  None
  */
void CImgHelperSimg::Reset ()
{
    m_processedSize = 0;

    if (m_hasher)
    {
        delete m_hasher;
        m_hasher = NULL;
    }

    if (m_coder)
    {
        delete m_coder;
        m_coder = NULL;
    }
}


/**
  * @brief  Metadata validation
  * @param  data: Pointer to metadata
  * @param  size: Metadata size
  * @return      > 0 - missing amout of the metadata
  *              0 - metadata is valid
  *              < 0 - corrupted metadata
  */
int CImgHelperSimg::CheckMetadata (const void* data, uint size)
{
    if (!size) return sizeof(TMetadata);
    if ( !data || size > sizeof(TMetadata) ) return -1;

    const TMetadata* ptr = (const TMetadata*) data;

    // TMetadata::magic validation
    {
        uint maskSize = (size > sizeof(TMetadata::magic)) ? sizeof(TMetadata::magic) : size;
        uint32 mask = ( maskSize < sizeof(TMetadata::magic) ) ? ~((~0u) << (maskSize * 8)) : (~0u);

        if ( (ptr->magic & mask) != (SIMG_META_MAGIC & mask) ) return -2;
    }

    if ( size != sizeof(TMetadata) ) return sizeof(TMetadata) - size;

    int res = verifyMetaSign(ptr);
    if (res) return -3;

    if (ptr->metaVersion != SIMG_META_VERSION) return -4;

    if (ptr->data.typeCrypt >= CRYPT_TYPE_COUNT) return -10;
    if (ptr->data.typeTag >= TAG_TYPE_COUNT) return -11;
    if (!ptr->data.imgSize) return -12;
    if (!ptr->data.dataSize) return -13;

    return 0;
}


/**
  * @brief  Image data validation
  * @param  data: Pointer to image data / part of data
  * @param  size: Data size
  * @param  pMetadata: Pointer to valid image metadata for validation
  * @return          > 0 - missing amount of image data
  *                  0 - data block is valid
  *                  < 0 - corrupted data block
  */
int CImgHelperSimg::CheckDataImg (const void* data, uint size, const void* pMetadata)
{
    const TMetadata* pMeta = (const TMetadata*) pMetadata;
    if (!pMeta) return -1;

    if (!m_processedSize)
    {
        // FIXME: m_hasher->Reset instead of error
        if (m_hasher) return -10;

        m_hasher = createHasher( (TTagType)pMeta->data.typeTag );
    }

    if (!m_hasher) return -11;

    int res = verifyDataTag( data, size, pMeta->data.imgSize, pMeta->data.tagImg, sizeof(TMetadata::data.tagImg) );
    if (res <= 0)
    {
        Reset();
    }

    return res;
}


/**
  * @brief  Decoded image data validation
  * @param  data: Pointer to decoded image data / part of data
  * @param  size: Data size
  * @param  pMetadata: Pointer to valid image metadata
  * @return          > 0 - missing amount of data for validation
  *                  0 - data block is valid
  *                  < 0 - corrupted data block
  */
int CImgHelperSimg::CheckData (const void* data, uint size, const void* pMetadata)
{
    const TMetadata* pMeta = (const TMetadata*) pMetadata;
    if (!pMeta) return -1;

    if (!m_processedSize)
    {
        if (m_hasher) return -10;

        m_hasher = createHasher( (TTagType)pMeta->data.typeTag );
    }

    if (!m_hasher) return -11;

    int res = verifyDataTag( data, size, pMeta->data.dataSize, pMeta->data.tagData, sizeof(TMetadata::data.tagData) );
    if (res <= 0)
    {
        Reset();
    }

    return res;
}


/**
  * @brief  Image data decoding
  * @param  src: Pointer to encoded image data / part of data
  * @param  srcSize: Source data size
  * @param  dst: Buffer for decoded data
  * @param  pDstSize: Pointer to buffer size. Actual data size will be stored there.
  * @param  pMetadata: Pointer to valid image metadata
  * @return          > 0 - Required amount of data to complete image decoding
  *                  0 - Successful decoding
  *                  < 0 - Error code
  */
int CImgHelperSimg::DecodeImg (const void* src, uint srcSize, void* dst, uint* pDstSize, const void* pMetadata)
{
    const TMetadata* pMeta = (const TMetadata*) pMetadata;
    if (!pMeta) return -1;

    if (!pDstSize || *pDstSize < srcSize) return -2;

    if (pMeta->data.typeCrypt == CRYPT_TYPE_NONE)
    {
        memcpy(dst, src, srcSize);
        return (int)srcSize;
    }

    if (!m_processedSize)
    {
        if (m_coder) return -10;

        m_coder = createCoder( (TCryptType)pMeta->data.typeCrypt, pMeta->data.crypt.params, m_cbGetKey );
    }

    if (!m_coder) return -11;

    int processed = 0;
    if (m_processedSize)
    {
        processed = m_coder->Append(src, srcSize, dst, pDstSize);
    }
    else
    {
        processed = m_coder->Decode(src, srcSize, pMeta->data.imgSize, dst, pDstSize);
    }

    if (processed < 0) return -12;

    m_processedSize += processed;

    int result;
    if (m_processedSize >= pMeta->data.imgSize)
    {
        if (m_coder) delete m_coder;
        m_coder = NULL;
        m_processedSize = 0;
        result = 0;
    }
    else
    {
        result = pMeta->data.imgSize - m_processedSize;
    }

    return result;
}


/**
  * @brief  Get image ID
  * @param  pMetadata: Pointer to valid image metadata
  * @return Image ID or negative error code
  */
int CImgHelperSimg::GetImgId (const void* pMetadata)
{
    const TMetadata* ptr = (const TMetadata*) pMetadata;
    return (ptr) ? ptr->imgId : -1;
}


/**
  * @brief  Get image size (encoded)
  * @param  pMetadata: Pointer to valid image metadata
  * @return Image size or 0 on error
  */
uint CImgHelperSimg::GetDataSizeImg (const void* pMetadata)
{
    const TMetadata* ptr = (const TMetadata*) pMetadata;
    return (ptr) ? ptr->data.imgSize : 0;
}


/**
  * @brief  Get decoded image size
  * @param  pMetadata: Pointer to valid image metadata
  * @return Decoded image size or 0 on error
  */
uint CImgHelperSimg::GetDataSize (const void* pMetadata)
{
    const TMetadata* ptr = (const TMetadata*) pMetadata;
    return (ptr) ? ptr->data.dataSize : 0;
}


/**
  * @brief  Get image version
  * @param  pMetadata: Pointer to valid image metadata
  * @return Version number or 0 on error
  */
uint32 CImgHelperSimg::GetDataVersion (const void* pMetadata)
{
    const TMetadata* ptr = (const TMetadata*) pMetadata;
    return (ptr) ? ptr->data.version : 0;
}


/**
  * @brief  Get metadata offset in a slot
  * @return Metadata offset (bytes) from slot begining
  */
uint32 CImgHelperSimg::GetBlockMetadataOffset (void)
{
    return 0;
}


/**
  * @brief  Get data block offset in a slot
  * @return Data offset (bytes) from slot begining
  */
uint32 CImgHelperSimg::GetBlockDataOffset (void)
{
    return SIMG_DATA_OFFSET;
}


/**
  * @brief  Metadata integrity check by signature verification
  * @param  pMeta: Pointer to image metadata
  * @return 0 or negative error code
  */
int CImgHelperSimg::verifyMetaSign (const TMetadata* pMeta)
{
    if (!pMeta) return -1;
    if (pMeta->meta.typeSign >= SIGN_TYPE_COUNT) return -2;

    const uint buffSize = sizeof(TMetadata::meta.sign.data);
    void* buff = malloc(buffSize);
    if (!buff) return -3;

    int result = -10;
    do
    {
        int res = calcMetaTag(pMeta, buff, buffSize);
        if (res <= 0) break;

        res = checkMetaTag(pMeta, buff, res);
        if (res) break;

        result = 0;
    } while (0);

    free(buff);

    return result;
}


/**
  * @brief  Metadata tag calculation
  * @param  pMeta: Pointer to image metadata
  * @param  buff: Buffer to save tag value
  * @param  size: Buffer size
  * @return Tag data written in the buffer or negative error code
  */
int CImgHelperSimg::calcMetaTag (const TMetadata* pMeta, void* buff, uint size)
{
    if (!pMeta || !buff) return -1;

    const uint signLen = getSignLen( (TSignType)pMeta->meta.typeSign );
    if ( !signLen || signLen > sizeof(TMetadata::meta.sign.data) ) return -10;
    const uint dataLen = sizeof(TMetadata) - signLen;

    IHash* pHasher = createHasher( (TSignType)pMeta->meta.typeSign );
    if (!pHasher) return -20;

    int result = -21;
    do
    {
        int hashLen = pHasher->GetSize();
        if (hashLen <= 0 || size < (uint)hashLen) break;

        int res = pHasher->Calculate(pMeta, dataLen, dataLen);
        if (res < 0 || (uint)res != dataLen) break;

        res = pHasher->GetResult(buff, size);
        if (res <= 0 || (uint)res > size) break;

        result = res;
    } while (0);

    delete pHasher;

    return result;
}


/**
  * @brief  Metadata tag verification
  * @param  pMeta: Pointer to image metadata
  * @param  tag: Tag data
  * @param  size: Tag data size
  * @return 0 (tag is valid) or negative error code
  */
int CImgHelperSimg::checkMetaTag (const TMetadata* pMeta, const void* tag, uint tagLen)
{
    if (!pMeta || !tag) return -1;

    const uint signLen = getSignLen( (TSignType)pMeta->meta.typeSign );
    if ( !signLen || signLen > sizeof(TMetadata::meta.sign.data) ) return -2;

    ICrypt* coder = NULL;
    void* buff = NULL;
    int result = -10;

    do
    {
        coder = createCoder( (TSignType)pMeta->meta.typeSign, pMeta->meta.sign.data, m_cbGetKey );
        if (!coder) break;

        const uint buffSize = sizeof(TMetadata::meta.sign.data);
        buff = malloc(buffSize);
        if (!buff) break;

        uint encodedSize = buffSize;
        int res = coder->Encode(tag, tagLen, tagLen, buff, &encodedSize);
        if (res < 0 || (uint)res != tagLen || encodedSize != signLen) break;

        const void* pSign = pMeta->meta.sign.data + sizeof(TMetadata::meta.sign.data) - signLen;
        if ( memcmp(buff, pSign, signLen) ) break;

        result = 0;
    } while (0);

    if (buff) free(buff);
    if (coder) delete coder;

    return result;
}


/**
  * @brief  Data tag verification
  * @param  data: Pointer to decoded image data (part of data)
  * @param  size: Data size
  * @param  totalSize: Total image size
  * @param  tag: Data tag
  * @param  tagDataSize: Data tag size
  * @return 0 - Tag is valid
  *         > 0 - Number of processed bytes
  *         < 0 - Error code
  */
int CImgHelperSimg::verifyDataTag (const void* data, uint size, uint totalSize, const void* tag, uint tagDataSize)
{
    if (!m_hasher) return -1;
    if (m_processedSize > totalSize) return -2;
    if (m_processedSize + size > totalSize) return -3;

    int processed = 0;
    if (m_processedSize)
    {
        processed = m_hasher->Append(data, size);
    }
    else
    {
        processed = m_hasher->Calculate(data, size, totalSize);
    }

    if (processed < 0) return -10;
    m_processedSize += processed;

    if (m_processedSize < totalSize) return processed;


    const int tagSize = m_hasher->GetSize();
    if (tagSize <= 0 || (uint)tagSize > tagDataSize) return -11;

    void* buff = malloc(tagSize);
    bool isValid = false;
    do
    {
        if (!buff) break;

        int res = m_hasher->GetResult(buff, tagSize);
        if (res < 0 || res != tagSize) break;

        isValid = ( !memcmp(buff, tag, tagSize) );
    } while (0);

    if (buff) free(buff);

    return (isValid) ? 0 : -20;
}
