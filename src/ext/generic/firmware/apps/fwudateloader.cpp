//*****************************************************************************
//
// @brief   Firmware loader base functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "firmware/apps/fwudateloader.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #define IMG_MARK_LOADED(map, imgId)         ( (map) |= 1 << (imgId) )
    #define IMG_MARK_NOT_LOADED(map, imgId)     ( (map) &= ~(1 << (imgId)) )
    #define IS_ALL_IMG_LOADED(map, count)       ( (map) == ~(~0u << (count)) )


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static int saveDataInSlot (const TFwSlot* pSlot, uint dataOffset, const void* data, uint dataSize);
    static int readDataFromSlot (const TFwSlot* pSlot, uint dataOffset, void* buff, uint dataSize);
    static int verifyImgData (const TFwSlot* pSlot, IImgHelper* pHelper, bool isEncoded);
    static int decode (const TFwSlot* pSlot, IImgHelper* pHelper);


/**
  * @brief  Constructor
  * @param  slotList: Slot descriptors list
  * @param  slotsLen: Number of items in the list
  * @param  loaders: Loaders list
  * @param  loadersLen: Number of items in the list
  * @param  pHelper: Pointer to image helper object
  */
CFwUpdateLoader::CFwUpdateLoader (const TFwSlot* slotList, uint slotsLen, CFwLoader* loaders, uint loadersLen, IImgHelper* pHelper) :
    m_pSlotList(slotList),
    m_slotListLen(slotsLen),
    m_pLoadersList(loaders),
    m_loadersListLen(loadersLen),
    m_pHelper(pHelper)
{
    assert(!m_pHelper);
}


/**
  * @brief  Destructor
  * @param  None
  */
CFwUpdateLoader::~CFwUpdateLoader ()
{
}


/**
  * @brief  Load image with timeout
  * @param  timeout: Loading timeout (ms)
  * @return Loaded images map (according to position in slot list)
  */
uint CFwUpdateLoader::Load (uint timeout)
{
    if (!m_slotListLen || !m_loadersListLen) return 0;

    uint loadedImages = 0;
    if (sizeof(loadedImages) * 8 < m_slotListLen) return 0;


    uint32 timeStart = Kernel::GetJiffies();
    uint loaderIndex = 0;

    while ( !IsTimeout(timeStart, timeout) && !IS_ALL_IMG_LOADED(loadedImages, m_slotListLen) )
    {
        if (loaderIndex >= m_loadersListLen) loaderIndex = 0;

        int res = -1;
        do
        {
            CFwLoader* pLoader = m_pLoadersList + loaderIndex;
            if (!pLoader) break;

            res = pLoader->Open();
            if (res < 0) break;

            res = load(pLoader, timeout / m_loadersListLen + 1);
            pLoader->Close();
        } while (0);

        if (res < 0)
        {
            // Change loader on error
            ++loaderIndex;
            continue;
        }

        const int imgId = res;
        if ((uint)imgId >= m_slotListLen) continue;

        const TFwSlot* pSlot = &m_pSlotList[imgId];
        if (!pSlot || !pSlot->pDev) break;

        IMG_MARK_LOADED(loadedImages, imgId);

        res = verifyImgData(pSlot, m_pHelper, true);

        if (!res)
        {
            res = decode(pSlot, m_pHelper);
        }

        if (!res)
        {
            res = verifyImgData(pSlot, m_pHelper, false);
        }

        if (res)
        {
            pSlot->pDev->Erase(pSlot->blockStart, pSlot->blocksCount);
            IMG_MARK_NOT_LOADED(loadedImages, imgId);
        }
    }

    return loadedImages;
}


/**
  * @brief  Image loading
  * @param  pLoader: Loader
  * @param  timeout: Loading timeout (ms)
  * @return ID of loaded image or negative error code
  */
int CFwUpdateLoader::load (CFwLoader* pLoader, uint32 timeout)
{
    uint32 timeStart = Kernel::GetJiffies();
    void* pMeta = NULL;
    void* pData = NULL;
    int result;

    do
    {
        const uint metaSize = m_pHelper->CheckMetadata(NULL, 0);
        int res;

        result = -1;
        pMeta = malloc(metaSize);
        if (!pMeta) break;

        // Image loading and metadata verification
        for (uint offset = 0; offset < metaSize && !IsTimeout(timeStart, timeout); )
        {
            res = pLoader->Load((char*)pMeta + offset, metaSize - offset, &offset);
            if (res <= 0) continue;

            offset += res;
        }

        if (IsTimeout(timeStart, timeout)) break;

        res = m_pHelper->CheckMetadata(pMeta, metaSize);
        if (res) break;

        // Getting image parameters and verification
        result = -10;

        const int imgId = m_pHelper->GetImgId(pMeta);
        if (imgId < 0 || (uint)imgId >= m_slotListLen) break;

        const TFwSlot* pSlot = &m_pSlotList[imgId];
        if (!pSlot || !pSlot->pDev) break;

        const uint blockSize = pSlot->pDev->BlockSize();
        if (!blockSize) break;

        const uint imgSize = m_pHelper->GetDataSizeImg(pMeta);
        if (!imgSize) break;

        const uint32 metaOffset = m_pHelper->GetBlockMetadataOffset();
        const uint32 imgDataOffset = m_pHelper->GetBlockDataOffset();
        const uint32 slotSize = blockSize * pSlot->blocksCount;
        if ((metaSize + imgSize) > slotSize) break;
        if ((metaOffset + metaSize) > slotSize) break;
        if ((imgDataOffset + imgSize) > slotSize) break;

        // Slot earasing before writing new data
        result = -20;
        res = pSlot->pDev->Erase(pSlot->blockStart, pSlot->blocksCount);
        if (res != DEV_OK) break;

        // Save metadata
        result = -30;
        res = saveDataInSlot(pSlot, metaOffset, pMeta, metaSize);
        if (res < 0) break;

        // Image data loading
        const uint buffSize = 1024;
        pData = malloc(buffSize);
        if (!pData) break;

        uint size = 0;
        while (size < imgSize && !IsTimeout(timeStart, timeout))
        {
            uint dataOffset;
            res = pLoader->Load(pData, blockSize, &dataOffset);
            if (res <= 0) continue;
            if (dataOffset != (metaSize + size)) break;

            uint loadedSize = res;
            res = saveDataInSlot(pSlot, imgDataOffset + size, pData, loadedSize);
            if (res < 0) break;

            size += loadedSize;
        }

        result = (size == imgSize) ? imgId : -40;
    } while (0);

    if (pMeta) free(pMeta);
    if (pData) free(pData);

    return result;
}


/**
  * @brief  Save data in the slot
  * @param  pSlot: Slot descriptor
  * @param  dataOffset: Data offset in the slot
  * @param  data: Data to write
  * @param  dataSize: Data size
  * @return 0 or negative error code
  */
int saveDataInSlot (const TFwSlot* pSlot, uint dataOffset, const void* data, uint dataSize)
{
    if (!pSlot || !pSlot->pDev || !pSlot->blocksCount) return -1;
    if (!dataSize) return -2;

    const uint blockSize = pSlot->pDev->BlockSize();
    if (!blockSize) return -3;

    uint32 blockIndex = pSlot->blockStart + dataOffset / blockSize;
    if (blockIndex >= pSlot->blockStart + pSlot->blocksCount) return -10;
    if (blockIndex + (dataSize + blockSize - 1) / blockSize > pSlot->blockStart + pSlot->blocksCount) return -11;

    void* blockBuff = malloc(blockSize);
    if (!blockBuff) return -20;

    uint32 offsetInBlock = dataOffset % blockSize;
    while (dataSize)
    {
        int res;
        uint size = (dataSize > (blockSize - offsetInBlock)) ? (blockSize - offsetInBlock) : dataSize;

        if (offsetInBlock || size != blockSize)
        {
            res = pSlot->pDev->Read(blockBuff, blockIndex);
            if (res != DEV_OK) break;
        }

        memcpy((char*)blockBuff + offsetInBlock, data, size);

        res = pSlot->pDev->Write(blockBuff, blockIndex);
        if (res != DEV_OK) break;

        offsetInBlock = 0;
        ++blockIndex;
        dataSize -= size;
        data = (const char*)data + size;
    }

    free(blockBuff);

    return (dataSize) ? -30 : 0;
}


/**
  * @brief  Read data from the slot
  * @param  pSlot: Slot descriptor
  * @param  dataOffset: Data offset in the slot
  * @param  buff: Buffer to save data
  * @param  dataSize: Data size to read
  * @return 0 or negative error code
  */
int readDataFromSlot (const TFwSlot* pSlot, uint dataOffset, void* buff, uint dataSize)
{
    if (!pSlot || !pSlot->pDev || !pSlot->blocksCount) return -1;
    if (!dataSize) return -2;

    const uint blockSize = pSlot->pDev->BlockSize();
    if (!blockSize) return -3;

    uint32 blockIndex = pSlot->blockStart + dataOffset / blockSize;
    if (blockIndex >= pSlot->blockStart + pSlot->blocksCount) return -10;
    if (blockIndex + (dataSize + blockSize - 1) / blockSize > pSlot->blockStart + pSlot->blocksCount) return -11;

    void* blockBuff = malloc(blockSize);
    if (!blockBuff) return -20;

    uint32 offsetInBlock = dataOffset % blockSize;
    while (dataSize)
    {
        int res = pSlot->pDev->Read(blockBuff, blockIndex);
        if (res != DEV_OK) break;

        uint sizeToCopy = (dataSize > (blockSize - offsetInBlock)) ? (blockSize - offsetInBlock) : dataSize;
        memcpy(buff, (char*)blockBuff + offsetInBlock, sizeToCopy);

        offsetInBlock = 0;
        ++blockIndex;
        dataSize -= sizeToCopy;
        buff = (char*)buff + sizeToCopy;
    }

    free(blockBuff);

    return (dataSize) ? -30 : 0;
}


/**
  * @brief  Slot data verification
  * @param  pSlot: Slot descriptor
  * @param  pHelper: Image helper object
  * @param  isEncoded: Slot data type (plaintext or encoded)
  * @return 0 (image is valid) or negative error code
  */
int verifyImgData (const TFwSlot* pSlot, IImgHelper* pHelper, bool isEncoded)
{
    if (!pSlot || !pHelper) return -1;
    if (!pSlot->pDev || !pSlot->blocksCount) return -2;

    CDevBlock* pDev = pSlot->pDev;
    const uint blockSize = pDev->BlockSize();
    const uint slotSize = blockSize * pSlot->blocksCount;

    const int metaSize = pHelper->CheckMetadata(NULL, 0);
    if (metaSize <= 0) return -3;

    const uint dataBuffSize = blockSize;
    void* buff = malloc(dataBuffSize + metaSize);
    if (!buff) return -10;

    int result = -20;
    do
    {
        // Metadata verification
        void* pMeta = (char*)buff + dataBuffSize;
        const uint32 metaOffset = pHelper->GetBlockMetadataOffset();
        int res = readDataFromSlot(pSlot, metaOffset, pMeta, metaSize);
        if (res < 0) break;

        res = pHelper->CheckMetadata(pMeta, metaSize);
        if (res) break;

        // Image data verification
        uint32 imgDataOffset = pHelper->GetBlockDataOffset();
        uint dataSize = (isEncoded) ? pHelper->GetDataSizeImg(pMeta) : pHelper->GetDataSize(pMeta);
        if (!dataSize || (imgDataOffset + dataSize) > slotSize) break;

        while (dataSize)
        {
            uint size = (dataSize > dataBuffSize) ? dataBuffSize : dataSize;
            res = readDataFromSlot(pSlot, imgDataOffset, buff, size);
            if (res < 0) break;

            if (isEncoded)  res = pHelper->CheckDataImg(buff, size, pMeta);
            else            res = pHelper->CheckData(buff, size, pMeta);
            if (res < 0) break;

            dataSize -= size;
            imgDataOffset += size;
        }

        if (dataSize) break;

        result = 0;
    } while (0);

    free(buff);

    return result;
}


/**
  * @brief  Image data decoding
  * @param  pSlot: Slot descriptor
  * @param  pHelper: Image helper object
  * @return 0 or negative error code
  */
int decode (const TFwSlot* pSlot, IImgHelper* pHelper)
{
    if (!pSlot || !pHelper) return -1;
    if (!pSlot->pDev || !pSlot->blocksCount) return -2;

    CDevBlock* pDev = pSlot->pDev;
    const uint blockSize = pDev->BlockSize();

    const int metaSize = pHelper->CheckMetadata(NULL, 0);
    if (metaSize <= 0) return -3;

    void* buff = malloc(metaSize + blockSize);
    if (!buff) return -10;

    int result = -20;
    do
    {
        void* pMeta = buff;
        void* blockBuff = (char*)buff + metaSize;
        int res;

        // Metadata loading
        const uint32 metaOffset = pHelper->GetBlockMetadataOffset();
        res = readDataFromSlot(pSlot, metaOffset, pMeta, metaSize);
        if (res < 0) break;

        // Block-by-block image data decoding
        const uint32 imgDataOffset = pHelper->GetBlockDataOffset();
        const uint imgSize = pHelper->GetDataSizeImg(pMeta);
        if (!imgSize || (imgDataOffset + imgSize + blockSize - 1) / blockSize > pSlot->blocksCount) break;

        const uint startBlockNum = imgDataOffset / blockSize;
        const uint endBlockNum = startBlockNum + (imgSize + blockSize - 1) / blockSize;
        uint32 offset = imgDataOffset % blockSize;
        uint size = 0;

        for (uint blockNum = startBlockNum; blockNum < endBlockNum; ++blockNum)
        {
            res = pDev->Read(blockBuff, blockNum);
            if (res != DEV_OK) break;

            void* ptr = (char*)blockBuff + offset;
            uint dataSize = blockSize - offset;
            if (dataSize > imgSize - size) dataSize = imgSize - size;

            res = pHelper->DecodeImg(ptr, dataSize, ptr, &dataSize, pMeta);
            if (res < 0) break;
            if (res && dataSize != blockSize) break;

            res = pDev->Erase(blockNum);
            if (res != DEV_OK) break;

            res = pDev->Write(blockBuff, blockNum);
            if (res != DEV_OK) break;

            size += dataSize;
            offset = 0;
        }

        if (size < imgSize) break;

        result = 0;
    } while (0);


    free(buff);

    return result;
}
