//*****************************************************************************
//
// @brief   Tests launcher
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "kernel/kernel.h"
#include "kernel/kthread.h"
#include "kernel/kstdio.h"
#include "unity_fixture.h"


// ----------------------------------------------------------------------------
// Types
// ----------------------------------------------------------------------------

    // Thread for tests launcher
    class TestThr : public KThread
    {
        public:
            TestThr ();

        private:
            void    run (void);
    };


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static TestThr testRunerThread;


/**
  * @brief  C-wrap of stdout for Unity. 
  *         Used from UNITY_OUTPUT_CHAR() in unity_config.h
  * @param  c: Output character
  * @return 0 or negative error code
  */
extern "C" int unityPutchar (char c)
{
    if (!kstdout || !kstdout->pDev) return -1;

    return kstdout->pDev->PutChar(c);
}


/**
  * @brief  Tests group launcher
  * @param  None
  * @return None
  */
static void RunAllTests (void)
{
    RUN_ALL_TEST_GROUPS();
}



/**
  * @brief  Handler of completing tests
  * @param  resultCode: Tests result (0 - success, otherwise number of errors)
  * @return None
  */
static void OnTestsDone (int resultCode)
{
    exit(resultCode);
}


/**
  * @brief  Constructor
  * @param  None
  * @return None
  */
TestThr::TestThr () :
    KThread("Test", 32 * 1024, THREAD_PRIORITY_INIT)
{
}


/**
  * @brief  Thread main function
  * @param  None
  * @return None
  */
void TestThr::run (void)
{
    static const char* av_override[] = {"exe", "-v"};
    int result = UnityMain(2, av_override, RunAllTests);

    OnTestsDone( (result) ? EXIT_FAILURE : EXIT_SUCCESS );
}
