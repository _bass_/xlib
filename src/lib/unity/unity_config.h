//*****************************************************************************
//
// @brief   Unity configaration for integrations with xLib
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

#define UNITY_OUTPUT_CHAR(c)     (void)unityPutchar(c)

int unityPutchar (char c);

#ifdef __cplusplus
}
#endif
