//*****************************************************************************
//
// @brief   Base widget functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/widget.h"
#include <algorithm>
#include <string.h>


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static std::forward_list<CWidget*> g_rootList;
    static const CWidget::TCbDesc* g_pCbDesc = nullptr;


/**
 * @brief Get list of root objects (parents)
 * @return Pointer to the list or null
 */
const std::forward_list<CWidget*>* CWidget::GetRootList (void)
{
    return &g_rootList;
}


/**
 * @brief Set callbacks
 * @param Callback descriptor
 */
void CWidget::SetCallbacks (const TCbDesc* desc)
{
    g_pCbDesc = desc;
}


/**
 * @brief Constructor
 * @param parent: Parent widget (limits size and position of current one)
 */
CWidget::CWidget (CWidget* parent):
    m_pParent(nullptr),
    m_pChildList(nullptr)
{
    memset(&m_area, 0x00, sizeof(m_area));
    setParent(parent);
}


/**
 * @brief Destructor
 */
CWidget::~CWidget ()
{
    if (!m_pParent)
    {
        g_rootList.remove(this);
    }
}


/**
 * @brief Get parent widget
 * @return Pointer to parent widget object or null
 */
CWidget* CWidget::GetParent (void) const
{
    return m_pParent;
}


/**
 * @brief Get list of childrens
 * @return Pointer to list of children or null
 */
const std::forward_list<CWidget*>* CWidget::GetChildren (void) const
{
    return m_pChildList;
}


/**
 * @brief Get widget width
 * @return Widget width
 */
uint16 CWidget::GetWidth (void) const
{
    uint16 w = m_area.width();

    CWidget* parent = GetParent();
    if (parent)
    {
        uint16 parentWidth = parent->GetWidth();
        if (m_area.x0() + w > parentWidth)
        {
            w = (parentWidth > m_area.x0()) ? parentWidth - m_area.x0() : 0;
        }
    }

    return w;
}


/**
 * @brief Get widget height
 * @return Widget height
 */
uint16 CWidget::GetHeight (void) const
{
    uint16 h = m_area.height();

    CWidget* parent = GetParent();
    if (parent)
    {
        uint16 parentHeight = parent->GetHeight();
        if (m_area.y0() + h > parentHeight)
        {
            h = (parentHeight > m_area.y0()) ? parentHeight - m_area.y0() : 0;
        }
    }

    return h;
}


/**
 * @brief Set widget width
 * @param width: Widget width
 */
void CWidget::SetWidth (uint16 width)
{
    CArea changedArea = m_area;
    m_area.SetWidth(width);

    if (width > changedArea.width())
    {
        changedArea.SetWidth(width);
    }

    setChangedArea(changedArea);
}


/**
 * @brief Set widget height
 * @param height: Widget height
 */
void CWidget::SetHeight (uint16 height)
{
    CArea changedArea = m_area;
    m_area.SetHeight(height);

    if (height > changedArea.height())
    {
        changedArea.SetWidth(height);
    }

    setChangedArea(changedArea);
}


/**
 * @brief Get widget offset
 * @param type: Offset type
 * @return Offset value
 */
TPoint CWidget::GetOffset (TOffset type) const
{
    TPoint offset = {m_area.x0(), m_area.y0()};

    if (type == E_OFFSET_ABS)
    {
        CWidget* parent = GetParent();
        while (parent)
        {
            offset.x += parent->m_area.x0();
            offset.y += parent->m_area.y0();
            parent = parent->GetParent();
        }
    }

    return offset;
}


/**
 * @brief Set widget offset
 * @param x: X coordinate offset
 * @param y: Y coordinate offset
 * @param type: Offset type
 */
void CWidget::SetOffset (int16 x, int16 y, TOffset type)
{
    if (type == E_OFFSET_ABS)
    {
        CWidget* parent = GetParent();
        if (parent)
        {
            const TPoint startPoint = parent->GetOffset(E_OFFSET_ABS);
            x -= startPoint.x;
            y -= startPoint.y;
        }
    }

    const int16 dx = x - m_area.x0();
    const int16 dy = y - m_area.y0();
    CArea changedArea;

    if (dx > 0)
    {
        changedArea.SetX0(-dx);
        changedArea.SetWidth(m_area.width() + dx);
    }
    else
    {
        changedArea.SetX0(0);
        if (dx) changedArea.SetWidth(m_area.width() + (uint16)(-dx));
        else    changedArea.SetWidth(m_area.width());
    }

    if (dy > 0)
    {
        changedArea.SetY0(-dy);
        changedArea.SetHeight(m_area.height() + dy);
    }
    else
    {
        changedArea.SetY0(0);
        if (dy) changedArea.SetHeight(m_area.height() + (uint16)(-dy));
        else    changedArea.SetHeight(m_area.height());
    }

    m_area.SetX0(x);
    m_area.SetY0(y);

    setChangedArea(changedArea);
}


/**
 * @brief Get widget area
 * @param offsetType: Base point offset type
 * @return Widget area
 */
CArea CWidget::GetArea (TOffset offsetType) const
{
    TPoint startPoint = GetOffset(offsetType);
    CArea area(startPoint.x, startPoint.y, GetWidth(), GetHeight());

    return area;
}


/**
 * @brief Set widget area
 * @param offsetType: Base point offset type
 */
void CWidget::SetArea (const CArea& area, TOffset offsetType)
{
    SetOffset(area.x0(), area.y0(), offsetType);
    SetWidth(area.width());
    SetHeight(area.height());
}


/**
 * @brief Notify about changes in the widget (need to redraw)
 * @param area: Changed area (relative to the base point of the widget)
 */
void CWidget::setChangedArea (const CArea& area)
{
    if (!g_pCbDesc || !g_pCbDesc->onChange) return;
    if (!area.width() || !area.height()) return;


    // Convert area base point to absolute position
    TPoint widgetOffset = GetOffset(E_OFFSET_ABS);
    CArea changedArea(
        widgetOffset.x + area.x0(), widgetOffset.y + area.y0(),
        area.width(), area.height()
    );

    CWidget* parent = GetParent();
    if (parent)
    {
        const CArea& parentArea = parent->GetArea(E_OFFSET_ABS);
        // Limit changes by parent widget area
        changedArea = changedArea.Intersection(parentArea);
    }

    g_pCbDesc->onChange(changedArea);
}


/**
 * @brief Set parent widget
 * @param parent: Pointer to parent widget object
 */
void CWidget::setParent (CWidget* parent)
{
    if (m_pParent)
    {
        m_pParent->childRemove(this);
    }

    m_pParent = parent;

    if (m_pParent)
    {
        m_pParent->childAdd(this);
    }
    else
    {
        g_rootList.reverse();
        g_rootList.push_front(this);
        g_rootList.reverse();
    }
}


/**
 * @brief Add child widget
 * @param pChild: Pointer to child widget
 */
void CWidget::childAdd (CWidget* pChild)
{
    if (!pChild) return;

    if (!m_pChildList)
    {
        m_pChildList = new std::forward_list<CWidget*>;
        if (!m_pChildList) return;
    }

    // Add new child to the end of the list
    m_pChildList->reverse();
    m_pChildList->push_front(pChild);
    m_pChildList->reverse();
}


/**
 * @brief Remove parent widget
 * @param pChild: Pointer to child widget to remove
 */
void CWidget::childRemove (CWidget* pChild)
{
    if (!pChild || !m_pChildList) return;

    m_pChildList->remove(pChild);
}
