//*****************************************************************************
//
// @brief   Base UI functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/ui.h"
#include "ui/area.h"
#include "ui/renderer.h"
#include "ui/widget.h"
#include "kernel/kmutex.h"
#include "dev/input.h"
#include "misc/macros.h"
#include <forward_list>
#include <string.h>
#include "def_board.h"


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if defined(CFG_UI_DRAW_BUFF_SIZE)
        #define UI_DRAW_BUFF_SIZE           CFG_UI_DRAW_BUFF_SIZE
    #else
        #define UI_DRAW_BUFF_SIZE           0
    #endif
    static_assert(CFG_UI_DRAW_BUFF_SIZE > sizeof(TColor), "UI drawing buffer must be at least size of few display widths");

    #if defined(CFG_UI_THREAD_STACK_SIZE)
        #define UI_THREAD_STACK_SIZE        CFG_UI_THREAD_STACK_SIZE
    #else
        #define UI_THREAD_STACK_SIZE        THREAD_DEFAULT_STACK_SIZE
    #endif

    #if defined(CFG_UI_THREAD_PRIORITY)
        #define UI_THREAD_PRIORITY          CFG_UI_THREAD_PRIORITY
    #else
        #define UI_THREAD_PRIORITY          THREAD_PRIORITY_DEFAULT
    #endif

    // Timeout of sending drawing buffer to display (ms)
    #if defined(CFG_UI_DRAWING_BUFF_TIMEOUT)
        #define UI_DRAWING_BUFF_TIMEOUT     CFG_UI_DRAWING_BUFF_TIMEOUT
    #else
        #define UI_DRAWING_BUFF_TIMEOUT     20
    #endif


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

    // Draw buffer descriptor
    struct TDrawBuffDesc
    {
        bool    isUsed;
        bool    isReady;
        uint16  x;
        uint16  y;
        uint16  width;
        uint16  height;
        uint8   buff[CFG_UI_DRAW_BUFF_SIZE];
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static void onWidgetChanged (const CArea& area);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    static const CWidget::TCbDesc g_uiDesc =
    {
        onWidgetChanged
    };

    static std::forward_list<CArea> g_changedAreas;
    static std::forward_list<CWidget::TEvent> g_inputEvents;
    // Mutex to block access to g_changedAreas and g_inputEvents during their modification
    static KMutex g_mutexLists;
    // Semaphore to notify UI thread about changes and necessity of their processing
    static KMutex g_processingSemaphore;

    #if defined(CFG_UI_DOUBLE_BUFFERING)
    static TDrawBuffDesc g_drawBuffers[2] = {};
    #else
    static TDrawBuffDesc g_drawBuffers[1] = {};
    #endif
    static int8 g_activeBuffIndex = -1;


/**
 * @brief Callback to notify about changes in a widget (need to redraw)
 * @param area: Changed area (absolute coordinates)
 */
void onWidgetChanged (const CArea& area)
{
    g_mutexLists.Lock();
    {
        bool joined = false;
        for (auto itr = g_changedAreas.begin(); itr != g_changedAreas.end(); ++itr)
        {
            if (!itr->HasIntersection(area)) continue;

            itr->Join(area);
            joined = true;
        }

        if (!joined)
        {
            g_changedAreas.push_front(area);
        };
    }
    g_mutexLists.Unlock();

    g_processingSemaphore.Unlock();
}


/**
 * @brief Color data (TColor) into pixels (TPixel) conversion (in place)
 * @param data: Pointer to the beginig of the data
 * @param count: Number of pixels to convert
 */
static void toPixels (TColor* data, uint count)
{
    TColor* pSrc = data;
    TPixel* pDst = (TPixel*)data;

    for (; count--; ++pSrc, ++pDst)
    {
        pDst->r = pSrc->r;
        pDst->g = pSrc->g;
        pDst->b = pSrc->b;
    }
}


/**
 * @brief Get pointer to unused drawing buffer
 * @return Pointer to the buffer
 */
static TDrawBuffDesc* getDrawBuff ()
{
    uint tryCounter = 3;
    TDrawBuffDesc* ptr = nullptr;

    do
    {
#if defined(CFG_UI_DOUBLE_BUFFERING)
        uint index = (g_activeBuffIndex < 0 || (uint)g_activeBuffIndex >= ARR_COUNT(g_drawBuffers))
                         ? 0
                         : (g_activeBuffIndex ^ 1);
#else
        uint index = 0;
#endif
        TDrawBuffDesc* pDesc = &g_drawBuffers[index];

        if (pDesc->isUsed && --tryCounter)
        {
            KThread::sleep(UI_DRAWING_BUFF_TIMEOUT);
            continue;
        }

        pDesc->isUsed = true;
        pDesc->isReady = false;
        ptr = pDesc;
    } while (!ptr);

    return ptr;
}


/**
 * @brief Drawing completion handler (release active drawing buffer)
 * @param x0: X coordinate of top-left corner
 * @param y0: Y coordinate of top-left corner
 * @param width: Area width
 * @param height: Area height
 * @param data: Pointer to pixel's data (size is width * height)
 * @param cbParam: Additional parameter (pointer to buffer descriptor)
 */
static void onDrawingComplete (uint x0, uint y0, uint width, uint height, const TPixel* data, void* cbParam)
{
    if (!cbParam) return;

    TDrawBuffDesc* pDesc = (TDrawBuffDesc*)cbParam;
    if (!pDesc->isUsed) return;

    pDesc->isReady = false;
    pDesc->isUsed = false;

    if (pDesc != &g_drawBuffers[g_activeBuffIndex]) return;

#if defined(CFG_UI_DOUBLE_BUFFERING)
    uint nextIndex = (g_activeBuffIndex ^ 1) & 0x01;
    pDesc = &g_drawBuffers[nextIndex];

    if (pDesc->isReady)
    {
        CDevDisplay* pDisplay = (CDevDisplay*)GetDevice(DEV_NAME_DISPLAY, 0);
        if (!pDisplay) return;

        g_activeBuffIndex = nextIndex;
        pDesc->isUsed = true;

        pDisplay->Draw(
            pDesc->x, pDesc->y,
            pDesc->width, pDesc->height,
            (const TPixel*)pDesc->buff
        );
    }
    else
#endif
    {
        g_activeBuffIndex = -1;
    }
}


/**
 * @brief Send buffer data to display
 * @param pDisplay: Pointer to display
 * @param pDesc: Drawing buffer descriptor
 */
static void doDraw (CDevDisplay* pDisplay, TDrawBuffDesc* pDesc)
{
    if (!pDisplay) return;

    uint buffIndex;
    if (pDesc == &g_drawBuffers[0])         buffIndex = 0;
#if defined(CFG_UI_DOUBLE_BUFFERING)
    else if (pDesc == &g_drawBuffers[1])    buffIndex = 1;
#endif
    else                                    return;

    if (g_activeBuffIndex >= 0 || !pDesc->isUsed)
    {
        g_activeBuffIndex = -1;
        pDesc->isReady = false;
        pDesc->isUsed = false;
        return;
    }

    pDesc->isReady = true;
    g_activeBuffIndex = buffIndex;

    const uint count = pDesc->width * pDesc->height;
    toPixels((TColor*)pDesc->buff, count);

    pDisplay->Draw(
        pDesc->x, pDesc->y,
        pDesc->width, pDesc->height,
        (const TPixel*)pDesc->buff,
        onDrawingComplete,
        pDesc
    );
}


/**
 * @brief Get widgets in speceified area
 * @param area: Area to select widgets
 * @return List of widgets
 */
static std::forward_list<CWidget*> getWidgets (const CArea& area)
{
    std::forward_list<CWidget*> changedWidgets;

    auto roots = CWidget::GetRootList();
    if (!roots || roots->empty()) return changedWidgets;

    auto widgetsToProcess = *roots;

    for (; !widgetsToProcess.empty(); widgetsToProcess.pop_front())
    {
        CWidget* w = widgetsToProcess.front();
        if (!w) continue;

        CArea widgetArea = w->GetArea(CWidget::E_OFFSET_ABS);
        if (!area.HasIntersection(widgetArea)) continue;

        changedWidgets.push_front(w);

        auto children = w->GetChildren();
        if (children && !children->empty())
        {
            widgetsToProcess.insert_after(widgetsToProcess.cbegin(), children->cbegin(), children->cend());
        }
    }

    return changedWidgets;
}


/**
 * @brief Find widget (at top layer) which contains specified point
 * @param x: X-coordinate of the point
 * @param y: Y-coordinate of the point
 * @return Pinter to found widget or null
 */
static CWidget* getWidget (uint16 x, uint16 y)
{
    auto roots = CWidget::GetRootList();
    if (!roots || roots->empty()) return nullptr;

    CWidget* target = nullptr;

    const std::forward_list<CWidget*>* pList = roots;
    for (auto itr = pList->cbegin(); itr != pList->cend();)
    {
        CWidget* w = *itr++;
        CArea area = w->GetArea(CWidget::E_OFFSET_ABS);
        if (!area.Contains(x, y)) continue;

        target = w;
        pList = w->GetChildren();
        if (!pList) break;

        itr = pList->cbegin();
    }

    return target;
}


/**
 * @brief Input events handler
 * @param dev: Input device which generated the event
 * @param msg: Input event message
 * @param param: Handler additional parameter
 */
static void hdlInputEvents (CDevInput* dev, const CDevInput::TMsg& msg, void* param)
{
    UNUSED_VAR(dev);

    CWidget::TEvent ev;
    switch (msg.type)
    {
        case CDevInput::E_MSG_POINTER:
            if (msg.pointer.type == CDevInput::E_EV_POINTER_PRESS)
            {
                ev.type = CWidget::E_EV_POINTER_PRESS;
                ev.pointer.press.isPressed = msg.pointer.press.isPressed;
            }
            else if (msg.pointer.type == CDevInput::E_EV_POINTER_MOVE)
            {
                ev.type = CWidget::E_EV_POINTER_MOVE;
                ev.pointer.move.dx = msg.pointer.move.dx;
                ev.pointer.move.dy = msg.pointer.move.dy;
            }
            else return;

            ev.pointer.x = msg.pointer.x;
            ev.pointer.y = msg.pointer.y;
        break;

        case CDevInput::E_MSG_KBD:
            ev.type = CWidget::E_EV_KBD;
            ev.kbd.state = msg.kbd.state;
            ev.kbd.scanCode = msg.kbd.scanCode;
            ev.kbd.symbol = msg.kbd.symbol;
            ev.kbd.modifiers.val = msg.kbd.modifiers.val;
        break;

        default: return;
    }

    g_mutexLists.Lock();
    {
        g_inputEvents.push_front(ev);
    }
    g_mutexLists.Unlock();

    g_processingSemaphore.Unlock();
}


/**
 * @brief Constructor
 * @param pDisplay: Pointer to display device object
 */
UI::UI (CDevDisplay* pDisplay):
    KThread("UI", UI_THREAD_STACK_SIZE, UI_THREAD_PRIORITY)
{
}


/**
 * @brief Constructor
 * @param displayIndex: Index of registered display device
 */
UI::UI (uint displayIndex):
    KThread("UI", UI_THREAD_STACK_SIZE, UI_THREAD_PRIORITY)
{
    m_pDisplay = (CDevDisplay*) GetDevice(DEV_NAME_DISPLAY, displayIndex);
}


/**
 * @brief Add input device to process events from it
 * @param devIndex: Input device system index
 */
void UI::AddInputDevice (uint devIndex)
{
    CDevInput* dev = (CDevInput*) GetDevice(DEV_NAME_INPUT, devIndex);
    if (!dev) return;

    dev->Subscribe(CDevInput::E_MSG_POINTER, hdlInputEvents);
    dev->Subscribe(CDevInput::E_MSG_KBD, hdlInputEvents);
}


/**
 * @brief Set focus on specified widget
 * @param widget: Widget to set focus on or nullptr to clear current focus
 */
void UI::SetFocus (CWidget* widget)
{
    if (widget == m_widgetInFocus) return;

    CWidget::TEvent focusEv = { .type = CWidget::E_EV_FOCUS_CHANGED };

    if (m_widgetInFocus)
    {
        // Focus lost
        focusEv.focus.state = false;
        m_widgetInFocus->ProcessEvent(focusEv);
    }

    m_widgetInFocus = widget;

    if (m_widgetInFocus)
    {
        // Focus acquired
        focusEv.focus.state = true;
        m_widgetInFocus->ProcessEvent(focusEv);
    }
}


/**
 * @brief Thread initialization
 */
void UI::init()
{
    if (!m_pDisplay)
    {
        kprint("Display device wasn't found");
        assert(1);
    }

    int res = m_pDisplay->Open();
    if (res != DEV_OK)
    {
        kprint("Open display device error");
        assert(1);
    }

    CWidget::SetCallbacks(&g_uiDesc);
}


/**
 * @brief Thread main function
 */
void UI::run()
{
    for (;;)
    {
        // Wait for updates
        g_processingSemaphore.Lock();

        processEvents();
        update();
    }

    CWidget::SetCallbacks(nullptr);
    m_pDisplay->Close();
}


/**
 * @brief Update handler (redraw changed areas)
 */
void UI::update()
{
    while (!g_changedAreas.empty())
    {
        g_mutexLists.Lock();
        const CArea changedArea = g_changedAreas.front();
        g_changedAreas.pop_front();
        g_mutexLists.Unlock();

        if (!changedArea.width() || !changedArea.height()) continue;

        const uint blockHeightMax = CFG_UI_DRAW_BUFF_SIZE / sizeof(TColor) / changedArea.width();
        if (!blockHeightMax)
        {
            kprint("Drawing buffer is to small");
            continue;
        }
        const uint blocksCount = (changedArea.height() + blockHeightMax - 1) / blockHeightMax;
        if (!blocksCount)
        {
            kprint("Drawing buffer is to small");
            continue;
        }
        CArea block = changedArea;
        if (block.height() > blockHeightMax) block.SetHeight(blockHeightMax);

        // Draw changed area block-by-block limited by drawing buffer size
        for (uint i = 0; i < blocksCount; ++i)
        {
            block.SetY0(changedArea.y0() + i * block.height());
            if (i == blocksCount - 1)
            {
                const uint16 rest = changedArea.height() % block.height();
                if (rest) block.SetHeight(rest);
            }

            std::forward_list<CWidget*> widgets = getWidgets(block);
            if (widgets.empty()) continue;

            TDrawBuffDesc* pBuffDesc = getDrawBuff();
            pBuffDesc->x = block.x0();
            pBuffDesc->y = block.y0();
            pBuffDesc->width = block.width();
            pBuffDesc->height = block.height();

            CRenderer renderer((TColor*)pBuffDesc->buff, block.width(), block.height());
            renderer.SetBlendMode(CRenderer::E_BLEND_ADD_BG);

            for (auto itr = widgets.cbegin(); itr != widgets.end(); ++itr)
            {
                CWidget* widget = *itr;
                if (!widget) continue;

                const CArea wAreaAbs = widget->GetArea(CWidget::E_OFFSET_ABS);
                renderer.SetOffsetX(wAreaAbs.x0() - block.x0());
                renderer.SetOffsetY(wAreaAbs.y0() - block.y0());

                CArea area = block.Intersection(wAreaAbs);
                if (!area.width() || !area.height()) continue;

                area.SetX0(area.x0() - wAreaAbs.x0());
                area.SetY0(area.y0() - wAreaAbs.y0());

                widget->Render(renderer, area);
            }

            doDraw(m_pDisplay, pBuffDesc);
        }
    }
}


/**
 * @brief Widgets input events processing
 */
void UI::processEvents (void)
{
    while (!g_inputEvents.empty())
    {
        g_mutexLists.Lock();
        const CWidget::TEvent ev = g_inputEvents.front();
        g_inputEvents.pop_front();
        g_mutexLists.Unlock();

        CWidget* target = nullptr;

        if (ev.type == CWidget::E_EV_POINTER_PRESS || ev.type == CWidget::E_EV_POINTER_MOVE)
        {
            target = getWidget(ev.pointer.x, ev.pointer.y);
        }

        if (!target) target = m_widgetInFocus;
        if (!target) continue;

        bool processed = false;
        while (target)
        {
            processed = target->ProcessEvent(ev);
            if (processed) break;

            target = target->GetParent();
        }
    }
}
