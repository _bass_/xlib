//*****************************************************************************
//
// @brief   Image widget
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/widgets/image.h"
#include "memory/heap.h"
#include <string.h>


/**
 * @brief Constructor
 * @param parent: Parent widget (limits size and position of current one)
 */
CWidgetImage::CWidgetImage (CWidget* parent):
    CWidget(parent)
{
}


/**
 * @brief Destructor
 */
CWidgetImage::~CWidgetImage ()
{
    freeResources();
}


/**
 * @brief Widget content drawing
 * @param renderer: Renderer
 * @param area: Area of the content to draw (coordinates are relative to start point of the widget)
 * @return 0 or negative error code
 */
int CWidgetImage::Render (CRenderer& renderer, const CArea& area)
{
    if (!m_img) return -1;

    // Original renderer offset for current widget
    const int16 rendererOffsX = renderer.GetOffsetX();
    const int16 rendererOffsY = renderer.GetOffsetY();

    do
    {
        if (!m_img->IsAnimated()) break;

        const uint currFrame = m_img->GetFrame();
        CImage::TFrameInfo info;
        int res = m_img->GetFrameInfo(currFrame, &info);
        if (res) break;

        // FIXME: проверить необходивость отрисовки предыдущих фреймов (area != frameArea)

        renderer.SetOffsetX(rendererOffsX + info.left);
        renderer.SetOffsetY(rendererOffsY + info.top);
    } while (0);

    m_img->Render(renderer);

    // Original renderer offset restoration
    renderer.SetOffsetX(rendererOffsX);
    renderer.SetOffsetY(rendererOffsY);

    return 0;
}


/**
 * @brief Set image data to use
 * @param data: Image data
 * @param size: Image data size
 */
void CWidgetImage::SetImage (const void* data, uint size)
{
    freeResources();

    bool success = false;
    do
    {
        if (!data || !size) break;

        m_data = malloc(size);
        if (!m_data) break;

        memcpy(m_data, data, size);
        m_size = size;

        m_img = CImage::CreateObject(m_data, m_size);
        if (!m_img) break;

        if (m_img->IsAnimated())
        {
            m_timer = new KTimer(onTimer, this);
            if (!m_timer) break;

            CImage::TFrameInfo info;
            int res = m_img->GetFrameInfo(0, &info);
            if (!res)
            {
                m_timer->Start(info.duration);
            }
        }

        success = true;
    } while (0);

    const CArea changedArea(0, 0, GetWidth(), GetHeight());
    setChangedArea(changedArea);

    if (!success)
    {
        freeResources();
    }
}


/**
 * @brief Release all allocated resources
 */
void CWidgetImage::freeResources (void)
{
    if (m_timer)
    {
        m_timer->Stop();
        delete m_timer;
        m_timer = nullptr;
    }

    if (m_img)
    {
        delete m_img;
        m_img = nullptr;
    }

    if (m_data)
    {
        free(m_data);
        m_data = nullptr;
        m_size = 0;
    }
}


/**
 * @brief Timer handler (anumaned images updating)
 * @param timer: Timer object
 * @param params: Additional handler parameters
 */
void CWidgetImage::onTimer(KTimer* timer, void* params)
{
    if (!params) return;

    CWidgetImage* obj = (CWidgetImage*) params;
    int res;

    uint currFrame = obj->m_img->GetFrame();
    uint totalFrames = obj->m_img->GetFrames();
    uint nextFrame = currFrame + 1;
    if (nextFrame >= totalFrames)
    {
        if (!obj->m_img->IsLooped())
        {
            timer->Stop();
            return;
        }

        nextFrame = 0;
    }

    CImage::TFrameInfo info;
    res = obj->m_img->GetFrameInfo(nextFrame, &info);
    if (res) return;
    obj->m_img->SetFrame(nextFrame);
    timer->Start(info.duration);

    CArea area(info.left, info.top, info.width, info.height);
    obj->setChangedArea(area);
}
