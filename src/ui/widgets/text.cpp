//*****************************************************************************
//
// @brief   Text widget
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/widgets/text.h"
#include "memory/heap.h"
#include <string.h>


/**
 * @brief Constructor
 * @param parent: Parent widget (limits size and position of current one)
 */
CWidgetText::CWidgetText (CWidget* parent):
    CWidget(parent)
{
}


/**
 * @brief Destructor
 */
CWidgetText::~CWidgetText ()
{
    if (m_text)
    {
        free(m_text);
        m_text = nullptr;
    }
}


/**
 * @brief Widget content drawing
 * @param renderer: Renderer
 * @param area: Area of the content to draw (coordinates are relative to start point of the widget)
 * @return 0 or negative error code
 */
int CWidgetText::Render (CRenderer& renderer, const CArea& area)
{
    if (!m_text || !m_font) return -1;

    uint len = strlen(m_text);
    if (!len) return -2;

    // Original renderer offset for current widget
    const int16 rendererOffsX = renderer.GetOffsetX();
    const int16 rendererOffsY = renderer.GetOffsetY();

    CArea charArea;
    for (uint i = 0; i < len; ++i)
    {
        int res = getSymbolArea(i, charArea);
        if (res < 0) break;

        if (!area.HasIntersection(charArea)) continue;

        renderer.SetOffsetX(rendererOffsX + charArea.x0());
        renderer.SetOffsetY(rendererOffsY + charArea.y0());

        m_font->Render(renderer, m_text[i], m_color);
    }

    // Original renderer offset restoration
    renderer.SetOffsetX(rendererOffsX);
    renderer.SetOffsetY(rendererOffsY);

    return 0;
}


/**
 * @brief Set widget text
 * @param text: Text string
 */
void CWidgetText::SetText (const char* text)
{
    if (m_text)
    {
        free(m_text);
        m_text = nullptr;
    }

    do
    {
        if (!text) break;

        uint len = strlen(text);
        m_text = (char*) malloc(len + 1);
        if (!m_text) break;

        memcpy(m_text, text, len + 1);
    } while (0);

    const CArea changedArea(0, 0, GetWidth(), GetHeight());
    setChangedArea(changedArea);
}


/**
 * @brief Set widget font
 * @param font: Font to use
 */
void CWidgetText::SetFont (IFont* font)
{
    if (m_font && m_text)
    {
        const CArea changedArea(0, 0, GetWidth(), GetHeight());
        setChangedArea(changedArea);
    }

    m_font = font;
}


/**
 * @brief Set text color
 * @param color: Color value
 */
void CWidgetText::SetColor (TColor color)
{
    if (m_font && m_text)
    {
        const CArea changedArea(0, 0, GetWidth(), GetHeight());
        setChangedArea(changedArea);
    }

    m_color = color;
}


/**
 * @brief Set horizontal text align
 * @param align: Align value
 */
void CWidgetText::SetAlign (EAlign align)
{
    if (m_font && m_text)
    {
        const CArea changedArea(0, 0, GetWidth(), GetHeight());
        setChangedArea(changedArea);
    }

    m_align = align;
}


/**
 * @brief Get symbol area
 * @param index: Symbol index in the string
 * @param prevArea: Area of previous symbol. Data will be replaced with values for specefiead symbol.
 * @return 0 or negative error code
 */
int CWidgetText::getSymbolArea (uint index, CArea& prevArea)
{
    if (!m_text || !m_font) return -1;

    int result = -2;
    do
    {
        if (index)
        {
            prevArea.SetX0(prevArea.x0() + prevArea.width());
        }

        const char c = m_text[index];
        const uint charWidth = m_font->GetWidth(c);

        prevArea.SetWidth(charWidth);
        prevArea.SetHeight(m_font->GetHeight());

        if (c != '\n' && index)
        {
            result = 0;
            break;
        }

        int x = -1;
        switch (m_align)
        {
            case E_ALIGN_LEFT:
                x = 0;
            break;

            case E_ALIGN_RIGHT:
            case E_ALIGN_CENTER:
            {
                uint lineLen = m_font->GetWidth( (c == '\n') ? &m_text[index + 1] : &m_text[index]);

                const uint16 widgetWidth = GetWidth();
                if (m_align == E_ALIGN_RIGHT)
                {
                    x = (int)widgetWidth - (int)lineLen;
                }
                else
                {
                    x = ((int)widgetWidth - (int)lineLen) / 2;
                }
            }
            break;

            default: break;
        }

        if (x < 0) break;

        prevArea.SetX0(x);

        // FIXME: check vertical align

        if (c == '\n')
        {
            // FIXME: line height
            prevArea.SetY0(prevArea.y0() + m_font->GetHeight());
        }

        result = 0;
    } while (0);

    return result;
}
