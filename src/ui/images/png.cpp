#include "ui/images/png.h"
#include "ui/ui.h"
#include "memory/heap.h"
#include "misc/math.h"
#include <stdlib.h>
#include <string.h>


//// CRC32 hardware acceleration
//#include "crc.h"


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    #define PNG_CHUNK_IHDR              ( (uint32)'I' | 'H' << 8 | 'D' << 16 | 'R' << 24 )
    #define PNG_CHUNK_IDAT              ( (uint32)'I' | 'D' << 8 | 'A' << 16 | 'T' << 24 )
    #define PNG_CHUNK_PLTE              ( (uint32)'P' | 'L' << 8 | 'T' << 16 | 'E' << 24 )
    #define PNG_CHUNK_TRNS              ( (uint32)'t' | 'R' << 8 | 'N' << 16 | 'S' << 24 )
    #define PNG_CHUNK_IEND              ( (uint32)'I' | 'E' << 8 | 'N' << 16 | 'D' << 24 )

    #define PNG_CRC_SIZE                sizeof(uint32)
    #define PNG_CODE_MAX_BITS           15

    #define WORD_TO_LE(w)               ( \
                                            ((w) & 0xff) << 24 | \
                                            (((w) >> 8) & 0xff) << 16 | \
                                            (((w) >> 16) & 0xff) << 8 | \
                                            (((w) >> 24) & 0xff) \
                                        )

    // Markers to replace pointers to dynamic tree to be able to identefy fixed trees
    #define PNG_FIXED_CODE_ID_LENGTHS   ( (TTree*) ~((size_t)0) )
    #define PNG_FIXED_CODE_ID_DIST      ( (TTree*) ((size_t)PNG_FIXED_CODE_ID_LENGTHS - 1) )


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

#pragma pack(1)

    // Chunk header
    struct TChunkHdr
    {
        uint32  len;
        uint32  type;
        uint8   data[];
    };

    // PNG image header data (IHDR)
    struct CImagePng::TIhdr
    {
        uint32  width;
        uint32  height;
        uint8   bitDepth;
        union
        {
            uint8 value;
            struct
            {
                uint8 palette   : 1;
                uint8 color     : 1;
                uint8 alpha     : 1;
                uint8 reserved  : 5;
            };
        } colorType;
        uint8   compression;
        uint8   filter;
        uint8   interlace;
    };

    // Deflated (zlib) data header
    struct TZlibHdr
    {
        union
        {
            uint8 value;
            struct
            {
                uint8   method  : 4;
                uint8   info    : 4;
            };
        } cmf;
        union
        {
            uint8 value;
            struct
            {
                uint8   check   : 5;
                uint8   dict    : 1;
                uint8   level   : 2;
            };
        } flags;
    };

#pragma pack()

    // Huffman tree descriptor
    struct TTree
    {
        uint16  startCodes[PNG_CODE_MAX_BITS];  // Values of first codes in a group (grouped by code width)
        uint16  counters[PNG_CODE_MAX_BITS];    // Number of codes in each code group
        uint16  count;                          // Number of symbols
        uint16  symbols[];                      // Array of symbols for each code
    };

    // Descriptor for Huffman fixed literals and lengths codes
    struct TFixedTreeItem
    {
        uint16  startCode;                  // First code of codes range
        uint8   bitLen;                     // Codes bit-length
        uint8   codeCount;                  // Number of sequential codes in the range
        uint16  startSymbol;                // First symbol (for startCode) for the same range
    } ;

    // IDAT data processing descriptor
    struct TIdatDesc
    {
        const uint8*    data;               // Pointer to source compressed (deflated) data in IDAT chunk
        uint            totalBits;          // Total amount of data (bits)
        uint            offset;             // Index of next bit to process
    };

    // Rendering context descriptor
    struct CImagePng::TContext
    {
        CRenderer*          renderer;           // Renderer object
        uint                pixelsCounter;      // Number of rendered pixels
        TIdatDesc           idatDesc;           // IDAT chunk data processig descriptor
        uint8*              outBuff;            // Output ring buffer for inflated (decompressed) data
        uint16              outBuffSize;        // Output buffer size (bytes)
        uint16              outOffsetWr;        // Index of next byte to write into output buffer
        TTree*              treeLengths;        // Code tree of literal and length values
        TTree*              treeDist;           // Code tree of distance values
        uint16              rowIndex;           // Current index of image row
        uint16              lineDataCounter;    // Number of data bytes in current row
        uint8               linesData[];        // Buffer for data of 2 image rows for restoring filtered data
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static bool probe (const void* data, uint size);
    static CImage* createObject (void);

    static const TChunkHdr* findChunk (uint32 type, const void* data, uint size);
    static bool isChunkValid (const TChunkHdr* pHdr);
    static uint32 calcCrc32 (const void* data, uint size);
    static uint getSamplesInPixel (uint8 colorType);
    static int getCode (TIdatDesc* pDesc, TTree* pTree);
    static int getByte (TIdatDesc* pDesc);
    static int getBits (TIdatDesc* pDesc, uint len);
    static bool loadNextIdatChunk (TIdatDesc* pDesc);
    static TTree* buildTree (uint16* codeLengths, uint count);
    static void destroyTree (TTree* pTree);
    static int unfilter (uint8* pLine, const uint8* pPrevLine, uint size, uint bytesInPixel);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_IMG_FORMAT(CImage::E_IMG_FORMAT_PNG, probe, createObject);

    // PNG file signature
    static const uint8 g_sign[8] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};

    // Length codes for dynamic tree Huffman codes
    static const uint8 g_codeLengths[] = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

    // Base values for length codes
    static const uint16 g_lengthBase[] =
    {
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258
    };
    // Array of numbers of extra-bits for length codes
    static const uint8 g_lengthExtraBits[] =
    {
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2,
        3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0
    };

    // Base values for distance codes
    static const uint16 g_distanceBase[] =
    {
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193,
        257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145,
        8193, 12289, 16385, 24577
    };
    // Array of numbers of extra-bits for distance codes
    static const uint8 g_distanceExtraBits[] =
    {
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10, 11, 11,
        12, 12, 13, 13
    };

    // Literals and lengths for fixed tree of Huffman codes
    //
    // Lit Value Bits Codes
    // --------- ---- -----
    // 0 - 143      8 00110000 - 10111111
    // 144 - 255    9 110010000 - 111111111
    // 256 - 279    7 0000000 - 0010111
    // 280 - 287    8 11000000 - 11000111
    static const TFixedTreeItem g_fixedTreeLengths[] =
    {
        {0x0030, 8, 144, 0},
        {0x0190, 9, 112, 144},
        {0x0000, 7, 24, 256},
        {0x00C0, 8, 7, 280},
    };


/**
 * @brief   Base check if image data is valid GIF image
 * @param   data: Image data
 * @param   size: Image data size
 * @return  true - data is valid GIF image
 */
bool probe (const void* data, uint size)
{
    if (!data || size < sizeof(g_sign)) return false;
    if ( memcmp(data, g_sign, sizeof(g_sign)) ) return false;

    const TChunkHdr* pChunkHdr = findChunk(PNG_CHUNK_IHDR, data, size);
    if (!pChunkHdr || !isChunkValid(pChunkHdr)) return false;

    uint32 len = WORD_TO_LE(pChunkHdr->len);
    if (len != sizeof(CImagePng::TIhdr)) return false;

    const CImagePng::TIhdr* pHdr = (const CImagePng::TIhdr*)pChunkHdr->data;
    if (!pHdr->width || !pHdr->height) return false;

    if (
        pHdr->bitDepth != 1 &&
        pHdr->bitDepth != 2 &&
        pHdr->bitDepth != 4 &&
        pHdr->bitDepth != 8 &&
        pHdr->bitDepth != 16
    ) return false;

    switch (pHdr->colorType.value)
    {
        case 0:
            // All valid bit depths are alowed
        break;

        case 2:
        case 4:
        case 6:
            if (pHdr->bitDepth != 8 && pHdr->bitDepth != 16) return false;
        break;

        case 3:
            if (pHdr->bitDepth > 8) return false;
        break;

        default: return false;
    }

    // If indexed colors are used, then PLTE chunk is required
    if (pHdr->colorType.palette && !findChunk(PNG_CHUNK_PLTE, data, size)) return false;

    //  Only method 0 (deflate) is defined
    if (pHdr->compression) return false;

    //  Only method 0 (adaptive filtering with five basic filter types) is defined
    if (pHdr->filter) return false;

    // Interlace is not supported
    if (pHdr->interlace) return false;

    // Check all chunks data integrity
    pChunkHdr = (const TChunkHdr*) ((const uint8*)pChunkHdr + sizeof(TChunkHdr) + len + PNG_CRC_SIZE);
    size -= (const uint8*)pChunkHdr - (const uint8*)data;

    while (size)
    {
        if (!isChunkValid(pChunkHdr)) return false;

        len = WORD_TO_LE(pChunkHdr->len);
        const uint chunkSize = sizeof(TChunkHdr) + len + PNG_CRC_SIZE;
        if (size < chunkSize) return false;

        pChunkHdr = (const TChunkHdr*) ((const uint8*)pChunkHdr + chunkSize);
        size -= chunkSize;
    }

    return true;
}


/**
 * @brief  Image object creation
 * @param  None
 * @return Pointer to created object or null on error
 */
CImage* createObject (void)
{
    return new CImagePng();
}


/**
 * @brief Constructor
 * @param None
 */
CImagePng::CImagePng () :
    CImage(),
    m_data(nullptr),
    m_dataSize(0),
    m_pIhdr(nullptr),
    m_bytesInPixel(0),
    m_bytesInLine(0),
    m_palette(nullptr),
    m_alphaTable(nullptr),
    m_alphaLen(0)
{
}


/**
 * @brief Constructor
 * @param None
 */
CImagePng::~CImagePng ()
{
}


/**
 * @brief  Load image data into the object
 * @param  data: Pointer to image data
 * @param  size: Image data size
 * @return 0 or negative error code
 */
int CImagePng::Load (const void* data, uint size)
{
    if (!probe(data, size)) return -1;

    const TChunkHdr* pChunk = findChunk(PNG_CHUNK_IHDR, data, size);
    if (!pChunk) return -2;
    const TIhdr* pHdr = (const TIhdr*)pChunk->data;

    const uint samplesInPixel = getSamplesInPixel(pHdr->colorType.value);
    if (!samplesInPixel) return -3;

    const uint width = WORD_TO_LE(pHdr->width);

    m_data = data;
    m_dataSize = size;
    m_pIhdr = pHdr;
    m_bytesInPixel = (samplesInPixel * pHdr->bitDepth + 7) / 8; // Round up to byte
    m_bytesInLine = (width * samplesInPixel * pHdr->bitDepth + 7) / 8 + 1; // +1 - filter type for each line
    m_palette = nullptr;
    m_alphaTable = nullptr;
    m_alphaLen = 0;

    if (pHdr->colorType.palette)
    {
        pChunk = findChunk(PNG_CHUNK_PLTE, data, size);
        if (!pChunk) return -3;
        m_palette = pChunk->data;

        // Checking availability of alpha-channel table for palette entries
        pChunk = findChunk(PNG_CHUNK_TRNS, data, size);
        if (pChunk)
        {
            m_alphaTable = pChunk->data;
            m_alphaLen = WORD_TO_LE(pChunk->len);
        }
    }

    return 0;
}


/**
 * @brief  Get image width
 * @return Image width (pixels)
 */
uint CImagePng::GetWidth (void)
{
    return (m_pIhdr) ? WORD_TO_LE(m_pIhdr->width) : 0;
}


/**
 * @brief  Get image height
 * @return Image height (pixels)
 */
uint CImagePng::GetHeight (void)
{
    return (m_pIhdr) ? WORD_TO_LE(m_pIhdr->height) : 0;
}


/**
 * @brief  Image (or frame) rendering
 * @param  renderer: Renderer
 * @return Number of processed pixels or negative error code
 */
int CImagePng::Render (CRenderer& renderer)
{
    if (!m_data || !m_pIhdr) return -1;
    if (!m_bytesInPixel || !m_bytesInLine) return -2;

    const uint samplesInPixel = getSamplesInPixel(m_pIhdr->colorType.value);
    if (!samplesInPixel) return -10;

    const TChunkHdr* pDataChunk = findChunk(PNG_CHUNK_IDAT, m_data, m_dataSize);
    if (!pDataChunk) return -11;
    const uint32 chunkDataLen = WORD_TO_LE(pDataChunk->len);
    if (chunkDataLen <= sizeof(TZlibHdr)) return -12;

    const TZlibHdr* pHdr = (const TZlibHdr*) pDataChunk->data;
    // Only deflate method is supported
    if (pHdr->cmf.method != 8) return -20;
    // According to specification window size must be less than 8
    if (pHdr->cmf.info > 7) return -21;
    // Checksum
    if ((pHdr->cmf.value * 256 + pHdr->flags.value) % 31) return -22;
    // Preset dictionary is not supported
    if (pHdr->flags.dict) return -23;

    const uint maxDataSize = m_bytesInLine * GetHeight();
    const uint winSize = 1 << (pHdr->cmf.info + 8);
    const uint outBuffSize = MIN(maxDataSize, winSize);

    // Context data creation
    // Context data size = descriptor size + buffer for 2 scanlines (pixels data + 1 byte filter type) + outBuffSize
    const uint contextBuffSize = sizeof(TContext) + m_bytesInLine * 2 + outBuffSize;
    TContext* pContext = (TContext*) malloc(contextBuffSize);
    if (!pContext) return -30;

    pContext->renderer = &renderer;
    pContext->pixelsCounter = 0;
    pContext->idatDesc.data = (const uint8*) (pHdr + 1);
    pContext->idatDesc.totalBits = (chunkDataLen - sizeof(TZlibHdr)) * 8;
    pContext->idatDesc.offset = 0;
    pContext->outBuff = (uint8*)pContext + contextBuffSize - outBuffSize;
    pContext->outBuffSize = outBuffSize;
    pContext->outOffsetWr = 0;
    pContext->treeLengths = nullptr;
    pContext->treeDist = nullptr;
    pContext->rowIndex = 0;
    pContext->lineDataCounter = 0;
    memset(pContext->linesData, 0x00, m_bytesInLine * 2);

    // IDAT chunk processing
    int isFinalBlock = 0;
    int result;

    while (!isFinalBlock)
    {
        result = -40;
        // Flag of last block (1 bit)
        isFinalBlock = getBits(&pContext->idatDesc, 1);
        if (isFinalBlock < 0) break;

        // Data block type (2 bits)
        // 00 - no compression
        // 01 - compressed with fixed Huffman codes
        // 10 - compressed with dynamic Huffman codes
        // 11 - reserved (error)
        int blockType = getBits(&pContext->idatDesc, 2);
        if (blockType < 0 || blockType == 3) break;

        if (blockType == 0)
        {
            result = processBlockUncompressed(pContext);
        }
        else
        {
            bool dynamicTree = (blockType == 2);
            result = processBlockHuffman(pContext, dynamicTree);
        }

        if (result < 0) break;
    }

    if (!result)
    {
        result = pContext->pixelsCounter;
    }

    free(pContext);

    return result;
}


/**
 * @brief  Uncompressed data block processing. Reads data from input stream and copies data to output.
 * @param  pContext: Pointer to context data
 * @return 0 or negative error code
 */
int CImagePng::processBlockUncompressed (TContext* pContext)
{
    if (!pContext) return -1;

    // Any bits of input up to the next byte boundary are ignored
    if (pContext->idatDesc.offset & 0x07) pContext->idatDesc.offset = (pContext->idatDesc.offset + 7) & 0x07;

    int result = -2;
    do
    {
        // len (2 bytes) + nLen (2 bytes) + data (> 0 bytes)
        int byte = getByte(&pContext->idatDesc);
        if (byte < 0) break;
        uint16 len = byte;
        byte = getByte(&pContext->idatDesc);
        if (byte < 0) break;
        len |= byte << 8;

        byte = getByte(&pContext->idatDesc);
        if (byte < 0) break;
        uint16 nLen = byte;
        byte = getByte(&pContext->idatDesc);
        if (byte < 0) break;
        nLen |= byte << 8;
        nLen = ~nLen;

        if (len != nLen) break;

        result = 0;
        while (len--)
        {
            byte = getByte(&pContext->idatDesc);
            if (byte < 0)
            {
                result = -3;
                break;
            }

            out(pContext, byte);
        }
    } while (0);

    return result;
}


/**
 * @brief  Compressed data block processing
 * @param  pContext: Pointer to context data
 * @param  dynamicTree: Type of decoding Huffman trees (dynamic or fixed)
 * @return 0 or negative error code
 */
int CImagePng::processBlockHuffman (TContext* pContext, bool dynamicTree)
{
    int res;

    if (dynamicTree)
    {
        res = buildDynamicTables(pContext);
    }
    else
    {
        res = buildFixedTables(pContext);
    }

    if (res < 0) return -1;

    res = processHuffmanData(pContext);
    destroyCodeTrees(pContext);

    return (res < 0) ? -2 : 0;
}


/**
 * @brief  Compressed by Huffman codes data processing (decompression)
 * @param  pContext: Pointer to context data
 * @return 0 or negative error code
 */
int CImagePng::processHuffmanData (TContext* pContext)
{
    if (!pContext || !pContext->treeLengths || !pContext->treeDist) return -1;

    bool success = false;
    for (;;)
    {
        int symbol = getCode(&pContext->idatDesc, pContext->treeLengths);
        if (symbol < 0) break;

        // 0..255 represent literal bytes
        if (symbol < 256)
        {
            out(pContext, symbol);
            continue;
        }

        // The value 256 indicates end-of-block
        if (symbol == 256)
        {
            success = true;
            break;
        }

        // Values 257..285 represent length codes
        symbol -= 257;
        if (symbol >= (int)ARR_COUNT(g_lengthBase)) break;

        uint len = g_lengthBase[symbol];
        uint extra = g_lengthExtraBits[symbol];
        if (extra)
        {
            int val = getBits(&pContext->idatDesc, extra);
            if (val < 0) break;
            len += val;
        }

        int dist = getCode(&pContext->idatDesc, pContext->treeDist);
        if (dist < 0 || dist >= (int)ARR_COUNT(g_distanceBase)) break;

        extra = g_distanceExtraBits[dist];
        dist = g_distanceBase[dist];
        if (extra)
        {
            int val = getBits(&pContext->idatDesc, extra);
            if (val < 0) break;
            dist += val;
        }

        if (!len || !dist) break;

        uint pos = (dist > pContext->outOffsetWr)
                       ? pContext->outOffsetWr + pContext->outBuffSize - dist
                       : pContext->outOffsetWr - dist;
        while (len--)
        {
            out(pContext, pContext->outBuff[pos]);
            if (++pos >= pContext->outBuffSize) pos -= pContext->outBuffSize;
        }
    }

    return (success) ? 0 : -10;
}


/**
 * @brief  Fixed Huffman code trees creating (for literals/lengths and distances)
 * @param  pContext: Pointer to context data
 * @return 0 or negative error code
 */
int CImagePng::buildFixedTables (TContext* pContext)
{
    // Put markers instead of pointers to allocated memory to be able to identefy fixed tree in getCode()
    pContext->treeLengths = PNG_FIXED_CODE_ID_LENGTHS;
    pContext->treeDist = PNG_FIXED_CODE_ID_DIST;

    return 0;
}


/**
 * @brief  Dynamic Huffman code trees creation (for literals/lengths and distances)
 * @param  pContext: Pointer to context data
 * @return 0 or negative error code
 */
int CImagePng::buildDynamicTables (TContext* pContext)
{
    destroyCodeTrees(pContext);

    TTree* codeTree = nullptr;
    uint16* pLitAndDistCodesLengths = nullptr;
    bool success = false;

    do
    {
        // RFC-1951
        // 5 Bits: HLIT, number of Literal/Length codes - 257 (257 - 286)
        // 5 Bits: HDIST, number of Distance codes - 1 (1 - 32)
        // 4 Bits: HCLEN, number of Code Length codes - 4 (4 - 19)
        int hlit = getBits(&pContext->idatDesc, 5);
        if (hlit < 0) break;
        int hdist = getBits(&pContext->idatDesc, 5);
        if (hdist < 0) break;
        int hclen = getBits(&pContext->idatDesc, 4);
        if (hclen < 0) break;

        // Set appropriate values of lengths
        hlit += 257;
        ++hdist;
        hclen += 4;

        if (hclen > (int)ARR_COUNT(g_codeLengths)) break;

        // Read lengths of codes to decode literal/length and distances alphabets
        {
            uint16 codeLengths[ARR_COUNT(g_codeLengths)];
            memset(codeLengths, 0x00, sizeof(codeLengths));

            success = true;
            for (int i = 0; i < hclen; ++i)
            {
                // Code lengths are interpreted as 3-bit integers
                int val = getBits(&pContext->idatDesc, 3);
                if (val < 0)
                {
                    success = false;
                    break;
                }

                codeLengths[g_codeLengths[i]] = val;
            }
            if (!success) break;
            success = false;

            // Build codes for alphabets decoding
            codeTree = buildTree(codeLengths, ARR_COUNT(codeLengths));
            if (!codeTree) break;
        }

        // Literal/length and distances alphabets decoding
        {
            pLitAndDistCodesLengths = (uint16*) malloc(sizeof(uint16) * (hlit + hdist));
            if (!pLitAndDistCodesLengths) break;

            success = true;
            for (int i = 0; i < hlit + hdist; )
            {
                int symbol = getCode(&pContext->idatDesc, codeTree);
                if (symbol < 0)
                {
                    success = false;
                    break;
                }

                int repeatCounter = 1;
                if (symbol < 16)
                {
                     // 0 - 15: Represent code lengths of 0 - 15
                }
                else if (symbol == 16)
                {
                    // 16: Copy the previous code length 3 - 6 times.
                    // The next 2 bits indicate repeat length (0 = 3, ... , 3 = 6)
                    if (!i) break;

                    repeatCounter = getBits(&pContext->idatDesc, 2);
                    if (repeatCounter < 0) break;

                    repeatCounter += 3;
                    symbol = pLitAndDistCodesLengths[i - 1];
                }
                else if (symbol == 17)
                {
                    // 17: Repeat a code length of 0 for 3 - 10 times. (3 bits of length)
                    repeatCounter = getBits(&pContext->idatDesc, 3);
                    if (repeatCounter < 0) break;
                    repeatCounter += 3;
                    symbol = 0;
                }
                else if (symbol == 18)
                {
                    // 18: Repeat a code length of 0 for 11 - 138 times (7 bits of length)
                    repeatCounter = getBits(&pContext->idatDesc, 7);
                    if (repeatCounter < 0) break;
                    repeatCounter += 11;
                    symbol = 0;
                }
                else
                {
                    success = false;
                    break;
                }

                while (repeatCounter--)
                {
                    pLitAndDistCodesLengths[i] = symbol;
                    ++i;
                }
            }

            // End of block symbol must be present (code 256)
            if (!success || !pLitAndDistCodesLengths[256]) break;
        }

        success = false;
        // Build lengths and distance codes trees and
        pContext->treeLengths = buildTree(pLitAndDistCodesLengths, hlit);
        if (!pContext->treeLengths) break;
        pContext->treeDist = buildTree(&pLitAndDistCodesLengths[hlit], hdist);
        if (!pContext->treeDist) break;

        success = true;
    } while (0);

    if (pLitAndDistCodesLengths)
    {
        free(pLitAndDistCodesLengths);
    }

    if (codeTree)
    {
        destroyTree(codeTree);
    }

    if (!success)
    {
        destroyCodeTrees(pContext);
    }

    return (success) ? 0 : -1;
}


/**
 * @brief  Huffman code trees destroying
 * @param  pContext: Pointer to context data
 * @return None
 */
void CImagePng::destroyCodeTrees (TContext* pContext)
{
    if (!pContext) return;

    if (pContext->treeLengths)
    {
        if (pContext->treeLengths != PNG_FIXED_CODE_ID_LENGTHS)
        {
            destroyTree(pContext->treeLengths);
        }
        pContext->treeLengths = nullptr;
    }

    if (pContext->treeDist)
    {
        if (pContext->treeDist != PNG_FIXED_CODE_ID_DIST)
        {
            destroyTree(pContext->treeDist);
        }
        pContext->treeDist = nullptr;
    }
}


/**
 * @brief  Output processing of decompressed symbol
 * @param  pContext: Pointer to context data
 * @param  byte: Decompressed byte
 * @return None
 */
void CImagePng::out (TContext* pContext, uint8 byte)
{
    if (!pContext || !pContext->outBuff || pContext->outOffsetWr >= pContext->outBuffSize) return;

    pContext->outBuff[pContext->outOffsetWr] = byte;
    ++pContext->outOffsetWr;
    if (pContext->outOffsetWr >= pContext->outBuffSize)
    {
        pContext->outOffsetWr -= pContext->outBuffSize;
    }

    const uint16 byteIndex = pContext->lineDataCounter;
    ++pContext->lineDataCounter;

    // Waiting for whole line data
    if (byteIndex != m_bytesInLine - 1) return;

    // Copy data from deflate ring buffer to line buffer
    uint8* pLine;
    uint8* pPrevLine;
    if (pContext->rowIndex & 0x01)
    {
        pLine = &pContext->linesData[m_bytesInLine];
        pPrevLine = pContext->linesData;
    }
    else
    {
        pLine = pContext->linesData;
        pPrevLine = &pContext->linesData[m_bytesInLine];
    }

    uint offset = (pContext->outOffsetWr >= m_bytesInLine)
                      ? pContext->outOffsetWr - m_bytesInLine
                      : pContext->outOffsetWr + pContext->outBuffSize - m_bytesInLine;

    for (uint i = 0; i < m_bytesInLine; ++i, ++offset)
    {
        if (offset >= pContext->outBuffSize) offset -= pContext->outBuffSize;

        pLine[i] = pContext->outBuff[offset];
    }

    pContext->lineDataCounter = 0;
    ++pContext->rowIndex;

    // Restore filterred data
    int res = unfilter(pLine, pPrevLine, m_bytesInLine, m_bytesInPixel);
    if (res < 0) return;

    // Send unfiltered data to output

    if (!m_pIhdr || (m_pIhdr->colorType.palette && !m_palette)) return;

    const uint samplesInPixel = getSamplesInPixel(m_pIhdr->colorType.value);
    if (samplesInPixel > sizeof(TColor::data)) return;

    const uint16 samplesInLine = samplesInPixel * GetWidth();
    const uint16 sampleMask = (uint32)(1 << m_pIhdr->bitDepth) - 1;

    ++pLine;    // Skip filter type byte

    // Color type   Allowed bit depths  Interpretation
    // ----------   ------------------  ---------------------
    // 0            1,2,4,8,16          Each pixel is a grayscale sample.
    // 2            8,16                Each pixel is an R,G,B triple.
    // 3            1,2,4,8             Each pixel is a palette index; a PLTE chunk must appear.
    // 4            8,16                Each pixel is a grayscale sample, followed by an alpha sample.
    // 6            8,16                Each pixel is an R,G,B triple, followed by an alpha sample.
    //
    // Bits meamings:
    // 1 (palette used), 2 (color used), and 4 (alpha channel used)

    TColor color = {};

    for (uint sampleIndex = 0; sampleIndex < samplesInLine; ++sampleIndex)
    {
        uint pos = sampleIndex * m_pIhdr->bitDepth;
        uint byte = pos / 8;
        uint bit = (m_pIhdr->bitDepth < 8) ? (8 - (pos + m_pIhdr->bitDepth)) % 8 : 0;
        uint8 val = (pLine[byte] >> bit) & sampleMask;

        // Scale value to [0-255] if necessary
        if (m_pIhdr->bitDepth < 8 && m_pIhdr->colorType.value != 3)
        {
            if (m_pIhdr->bitDepth == 1) val = (val) ? 0xff : 0x00;
            else                        val <<= 8 - m_pIhdr->bitDepth;
        }

        uint8 channelIndex = sampleIndex % samplesInPixel;
        switch (m_pIhdr->colorType.value)
        {
            case 0:
            case 4:
                if (channelIndex)
                {
                    color.a = val;
                }
                else
                {
                    color.r = val;
                    color.g = val;
                    color.b = val;
                }
                break;

            case 2:
            case 6:
                color.data[channelIndex] = val;
                break;

            case 3:
            {
                const uint8* pColor = &m_palette[val * 3];
                color.r = pColor[0];
                color.g = pColor[1];
                color.b = pColor[2];
                color.a = (m_alphaTable && val < m_alphaLen) ? m_alphaTable[val] : 0xff;
            }
            break;

            default: break;
        }

        if (channelIndex == samplesInPixel - 1)
        {
            // All pixels are fully visible if there is no alpha-channel in pixels data and simple transparency isn't used
            if (!m_pIhdr->colorType.alpha && !m_alphaLen) color.a = 0xff;

            const uint x = pContext->pixelsCounter % GetWidth();
            const uint y = pContext->pixelsCounter / GetWidth();
            pContext->renderer->PutPixel(x, y, color);
            ++pContext->pixelsCounter;
        }
    }
}


/**
 * @brief  Find chunk of particular type in image data
 * @param  type: Chunk type
 * @param  data: Image data
 * @param  size: Image data size
 * @return Pointer to found chunk or null
 */
const TChunkHdr* findChunk (uint32 type, const void* data, uint size)
{
    if (size < sizeof(g_sign) + sizeof(TChunkHdr) + PNG_CRC_SIZE) return nullptr;

    const TChunkHdr* ptr = (const TChunkHdr*) ((const uint8*)data + sizeof(g_sign));
    while (size)
    {
        const uint32 len = WORD_TO_LE(ptr->len);
        const uint chunkSize = sizeof(TChunkHdr) + len + PNG_CRC_SIZE;
        if (size < chunkSize) break;
        if (ptr->type == type) return ptr;

        ptr = (const TChunkHdr*) ((const uint8*)ptr + chunkSize);
        size -= chunkSize;
    }

    return nullptr;
}


/**
 * @brief  Check chunk data by CRC verification
 * @param  pChunk: Pointer to chunk
 * @return true - chunk data is valid
 */
bool isChunkValid (const TChunkHdr* pChunk)
{
    if (!pChunk) return false;

    const uint32 len = WORD_TO_LE(pChunk->len);
    uint32 crc = calcCrc32(&pChunk->type, sizeof(TChunkHdr::type) + len);
    const uint32* pChunkCrc = (const uint32*)(pChunk->data + len);

    return (crc == WORD_TO_LE(*pChunkCrc));
}


/**
 * @brief  CRC32 checksum calculation
 * @param  data: Pointer to data
 * @param  size: Data size
 * @return Value of CRC32 checksum or 0 on error
 */
uint32 calcCrc32 (const void* data, uint size)
{
    return crc32(data, size, 0xffffffff);
}


/**
 * @brief  Samples per pixel calculation
 * @param  colorType: Color type value from TPngHdr
 * @return Value of bytes in a pixel or 0
 */
uint getSamplesInPixel (uint8 colorType)
{
    // Color type   Allowed bit depths  Interpretation
    // ----------   ------------------  ---------------------
    // 0            1,2,4,8,16          Each pixel is a grayscale sample.
    // 2            8,16                Each pixel is an R,G,B triple.
    // 3            1,2,4,8             Each pixel is a palette index; a PLTE chunk must appear.
    // 4            8,16                Each pixel is a grayscale sample, followed by an alpha sample.
    // 6            8,16                Each pixel is an R,G,B triple, followed by an alpha sample.
    //
    // Bits meamings:
    // 1 (palette used), 2 (color used), and 4 (alpha channel used)

    switch (colorType)
    {
        case 0:
        case 3:     return 1;
        case 2:     return 3;
        case 4:     return 2;
        case 6:     return 4;
        default:    return 0;
    }
}


/**
 * @brief  Read Huffman code from the stream and convert it to symbol from the tree
 * @param  pDesc: Pointer to IDAT data processing descriptor
 * @param  pTree: Pointer to Huffman tree for symbols decoding
 * @return Symbol value or negative error code
 */
int getCode (TIdatDesc* pDesc, TTree* pTree)
{
    int code = 0;
    uint len = 0;
    uint symbolIndex = 0;
    int symbol = -1;
    bool fixedTree = (pTree == PNG_FIXED_CODE_ID_LENGTHS || pTree == PNG_FIXED_CODE_ID_DIST);
    bool isFixedDistancesTree = (pTree == PNG_FIXED_CODE_ID_DIST);

    while (len < PNG_CODE_MAX_BITS)
    {
        if (pDesc->offset >= pDesc->totalBits)
        {
            // End of IDAT chunk was reached. Switch to next chunk if there is additional one.
            if (!loadNextIdatChunk(pDesc))
            {
                symbol = -10;
                break;
            }
        }

        uint byte = pDesc->offset >> 3;
        uint bit = pDesc->offset & 0x07;

        code <<= 1;
        if (pDesc->data[byte] & (1 << bit)) code |= 1;
        ++pDesc->offset;
        ++len;

        if (fixedTree)
        {
            if (isFixedDistancesTree)
            {
                // Distance codes 0-31 are represented by (fixed-length) 5-bit codes
                if (len < 5) continue;
                if (code < 30) symbol = code;
                break;
            }
            else
            {
                // Codes lengths and start values described in g_fixedTreeLengths
                // Minimum code length is 7, maximum - 9
                if (len < 7) continue;
                if (len > 9) break;

                for (uint i = 0; i < ARR_COUNT(g_fixedTreeLengths); ++i)
                {
                    const TFixedTreeItem* pItem = &g_fixedTreeLengths[i];
                    if (
                        pItem->bitLen != len ||
                        code < pItem->startCode ||
                        code - pItem->startCode >= pItem->codeCount
                        ) continue;

                    symbol = pItem->startSymbol + code - pItem->startCode;
                    break;
                }
                if (symbol >= 0) break;
            }
        }
        else
        {
            const uint index = len - 1;
            const uint16 numOfCodes = pTree->counters[index];
            if (!numOfCodes) continue;

            const uint16 startCode = pTree->startCodes[index];
            if (code < startCode || code >= startCode + numOfCodes)
            {
                symbolIndex += numOfCodes;
                continue;
            }

            symbolIndex += code - startCode;
            if (symbolIndex >= pTree->count) break;

            symbol = pTree->symbols[symbolIndex];
            break;
        }
    }

    return symbol;
}


/**
 * @brief  Read bits from stream in direct order
 * @param  pDesc: Pointer to IDAT data processing descriptor
 * @param  len: Number of bits to read
 * @return Value or negative error code
 */
int getBits (TIdatDesc* pDesc, uint len)
{
    // Maximum read length is number of bits in `int` - 1 to be able to return negative value
    const uint maxBitLen = sizeof(int) * 8 - 1;
    if (len > maxBitLen) return -1;

    int val = 0;
    for (uint i = 0; i < len; ++i)
    {
        if (pDesc->offset >= pDesc->totalBits)
        {
            // End of IDAT chunk was reached. Switch to next chunk if there is additional one.
            if (!loadNextIdatChunk(pDesc))
            {
                val = -10;
                break;
            }
        }

        const uint byte = pDesc->offset >> 3;
        const uint bit = pDesc->offset & 0x07;

        if (pDesc->data[byte] & (1 << bit)) val |= 1 << i;
        ++pDesc->offset;
    }

    return val;
}


/**
 * @brief  Read byte from the stream
 * @param  pDesc: Pointer to IDAT data processing descriptor
 * @return Byte data or negative error code
 */
int getByte (TIdatDesc* pDesc)
{
    // Any bits of input up to the next byte boundary are ignored
    if (pDesc->offset & 0x07) pDesc->offset = (pDesc->offset + 7) & 0x07;

    // Stream must be byte-aligned
    if (pDesc->totalBits - pDesc->offset < sizeof(char) * 8)
    {
        // End of IDAT chunk was reached. Switch to next chunk if there is additional one.
        if (!loadNextIdatChunk(pDesc)) return -1;
    }

    if (pDesc->totalBits - pDesc->offset < sizeof(char) * 8) return -2;

    const uint byteIndex = pDesc->offset >> 3;
    pDesc->offset += sizeof(char) * 8;

    return (int)pDesc->data[byteIndex];
}


/**
 * @brief  Load stream data from next IDAT chunk
 * @param  pDesc: Pointer to IDAT data processing descriptor
 * @return true - success
 */
bool loadNextIdatChunk (TIdatDesc* pDesc)
{
    // The compressed datastream is then the concatenation of the contents of all the IDAT chunks.
    const TChunkHdr* pChunk = (const TChunkHdr*)(pDesc->data + pDesc->totalBits / 8 + PNG_CRC_SIZE);
    const uint chunkDataSize = WORD_TO_LE(pChunk->len);

    if (pChunk->type != PNG_CHUNK_IDAT || !chunkDataSize) return false;

    pDesc->data = pChunk->data;
    pDesc->totalBits = chunkDataSize * 8;
    pDesc->offset = 0;

    return true;
}


/**
 * @brief  Huffman code tree creation from code lengths values
 * @param  codeLengths: Pointer to code lengths data
 * @param  count: Number of values in code lengths array
 * @return Pointer to created tree or null on error
 */
TTree* buildTree (uint16* codeLengths, uint count)
{
    if (!codeLengths || !count) return nullptr;

    const uint treeBuffSize = sizeof(TTree) + count * sizeof(uint16);
    TTree* pTree = (TTree*) malloc(treeBuffSize);
    if (!pTree) return nullptr;

    memset(pTree, 0x00, treeBuffSize);

    // Step 1: count the number of codes for each code length
    uint16 bitLengthCount[PNG_CODE_MAX_BITS + 1];
    memset(bitLengthCount, 0x00, sizeof(bitLengthCount));

    for (uint i = 0; i < count; ++i)
    {
        uint16 len = codeLengths[i];
        if (!len || len > PNG_CODE_MAX_BITS) continue;

        bitLengthCount[len]++;
        pTree->count++;
    }

    if (!pTree->count)
    {
        free(pTree);
        return nullptr;
    }

    // Save counter values in tree descriptor.
    // Value for zero-length code is not used
    memcpy(pTree->counters, &bitLengthCount[1], sizeof(TTree::counters));

    // Step 2: find the numerical value of the smallest code for each code length
    uint16 code = 0;
    bitLengthCount[0] = 0;

    for (uint bits = 1; bits <= PNG_CODE_MAX_BITS; bits++)
    {
        code = (code + bitLengthCount[bits - 1]) << 1;
        // Save start code values for each code length group. Zero-length code is not used.
        pTree->startCodes[bits - 1] = code;
    }

    // Step 3: Assign numerical values to all codes, using consecutive values for all codes of the same length
    // with the base values determined at step 2.
    // This step was slightly modified to reduce memory size used for the tree.
    // Because codes in a group are consecutive, we can use only start value of a group and number of codes and not to
    // store each code separately (see pTree->startCodes and pTree->counters).

    // Fill in symbols data - index in `codeLengths` array
    memset(bitLengthCount, 0x00, sizeof(bitLengthCount));

    for (uint n = 0; n < count; n++)
    {
        uint8 len = codeLengths[n];
        if (!len) continue;

        // Code index calculation: summ of all previous numbers of codes + index in current group
        uint lenIndex = len - 1;
        uint codeIndex = 0;
        for (uint i = 0; i < lenIndex; ++i)
        {
            codeIndex += pTree->counters[i];
        }

        codeIndex += bitLengthCount[lenIndex];
        bitLengthCount[lenIndex]++;

        pTree->symbols[codeIndex] = n;
    }

    return pTree;
}


/**
 * @brief  Huffman code tree destroying
 * @param  pTree: Pointer to code tree
 * @return None
 */
void destroyTree (TTree* pTree)
{
    if (pTree) free(pTree);
}


/**
 * @brief  Filtered image data restoration
 * @param  pLine: Pointer to image scanline data
 * @param  pPrevLine: Pointer to previous image scanline data
 * @param  size: Number of bytes in a scanline
 * @param  bytesInPixel: Number of bytes in a pixel
 * @return 0 or negative error code
 */
int unfilter (uint8* pLine, const uint8* pPrevLine, uint size, uint bytesInPixel)
{
    if (!pLine || !pPrevLine || !size || !bytesInPixel) return -1;

    uint8 filterType = *pLine;
    // Filter type == None -> no data modifications are needed
    if (!filterType) return 0;

    --size;
    ++pLine;
    // Skip filter type
    ++pPrevLine;

    switch (filterType)
    {
        // Sub
        case 1:
            for (uint i = 0; i < size; ++i)
            {
                uint8 prevByte = (i >= bytesInPixel) ? pLine[i - bytesInPixel] : 0;
                pLine[i] += prevByte;
            }
            break;

        // Up
        case 2:
            for (uint i = 0; i < size; ++i)
            {
                uint8 priorByte = pPrevLine[i];
                pLine[i] += priorByte;
            }
            break;

        // Average
        case 3:
            for (uint i = 0; i < size; ++i)
            {
                uint8 prevByte = (i >= bytesInPixel) ? pLine[i - bytesInPixel] : 0;
                uint8 priorByte = pPrevLine[i];
                pLine[i] += ((prevByte + priorByte) >> 1) & 0xff;
            }
            break;

        // Paeth
        case 4:
            for (uint i = 0; i < size; ++i)
            {
                uint8 prevByte = (i >= bytesInPixel) ? pLine[i - bytesInPixel] : 0;
                uint8 aboveByte = pPrevLine[i];
                uint8 upperLeftByte = (i >= bytesInPixel) ? pPrevLine[i - bytesInPixel] : 0;

                int p = prevByte + aboveByte - upperLeftByte;
                int pa = abs(p - prevByte);
                int pb = abs(p - aboveByte);
                int pc = abs(p - upperLeftByte);

                if (pa <= pb && pa <= pc)   pLine[i] += prevByte & 0xff;
                else if (pb <= pc)          pLine[i] += aboveByte & 0xff;
                else                        pLine[i] += upperLeftByte & 0xff;
            }
            break;

        default: return -10;
    }

    return 0;
}
