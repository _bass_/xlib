//*****************************************************************************
//
// @brief   GIF images renderer
//          Only disposal method #0 is supported (full overwrite).
//          In other cases artifacts may appeared.
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/images/gif.h"
#include "memory/heap.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants and macros
// ----------------------------------------------------------------------------

    #define GIF_SIGN                "GIF"
    #define GIF_VERSION_87          "87a"
    #define GIF_VERSION_89          "89a"

    #define GIF_BLOCK_EXT           0x21
    #define GIF_BLOCK_IMG           0x2C
    #define GIF_BLOCK_END           0x3B

    #define GIF_EXT_GRAPHICS_CTRL   0xF9
    #define GIF_EXT_APPLICATION     0xFF
    #define GIF_EXT_PLAINTEXT       0x01
    #define GIF_EXT_COMMENT         0xFE

    #define EXTENSION_CODE(extType) ((uint16)GIF_BLOCK_EXT | extType << 8)

    #define GIF_CODE_MIN_BITS       3
    #define GIF_CODE_MAX_BITS       12
    #define GIF_CODE_TAB_SIZE       (1 << GIF_CODE_MAX_BITS)


// ----------------------------------------------------------------------------
// Local types
// ----------------------------------------------------------------------------

#pragma pack(1)
    // GIF image header
    struct TGifHdr
    {
        uint8   sign[3];
        uint8   version[3];
        uint16  width;
        uint16  height;
        struct
        {
            uint8   colorTabSize    : 3;
            uint8   colorsSorted    : 1;
            uint8   colorResolution : 3;
            uint8   globColorTab    : 1;
        } fields;
        uint8   bgColorIndex;
        uint8   aspectRatio;
    };

    // Color structure from palette
    struct TGifColor
    {
        uint8 r;
        uint8 g;
        uint8 b;
    };

    // GIF extension header
    struct TExtHdr
    {
        uint8 separator;
        uint8 label;
    };

    // Frame header
    struct CImageGif::TFrameHdr
    {
        uint8   separator;
        uint16  left;
        uint16  top;
        uint16  width;
        uint16  height;
        struct
        {
            uint8 colorTabSize    : 3;
            uint8 reserved        : 2;
            uint8 colorsSorted    : 1;
            uint8 interlace       : 1;
            uint8 localColorTab   : 1;
        } fields;
    };

    // Application extension data structure
    struct TExtAppData
    {
        uint8   size_1;
        uint8   appId[8];
        uint8   appCode[3];
        uint8   size_2;
        uint8   subId;
        uint16  loopCount;
    };

    // Graphics Control Extension header
    struct CImageGif::TExtGraphCtrl
    {
        struct
        {
            uint8 transparent   : 1;
            uint8 userInput     : 1;
            uint8 disposalMethod: 3;
            uint8 reserved      : 3;
        } fields;
        uint16  delay;
        uint8   transparentColorIndex;
    };


    // Code table item structure.
    // Full value of code is a chain of color indexes.
    // To reduce memory usage item is represented as a linked list of values connected by `prevCode`.
    struct TCodeTabItem
    {
        uint16  prevCode;   // Previous code value
        uint8   value;      // Current item value (color index)
    };

    // Code table descriptor
    struct CImageGif::TCodeTab
    {
        const TGifColor*    pColorTable;    // Pointer to color table
        uint16              initSize;       // Initial size of the code table
        uint16              count;          // Number of additional codes stored in the code table
        uint16              size;           // Maximum number of items to store (size of items[] array)
        TCodeTabItem        items[];
    };

#pragma pack()

    // Rendering process descriptor
    struct CImageGif::TRenderDesc
    {
        CRenderer&          renderer;
        uint                pixelCounter;           // Number of rendered pixels
        const TGifColor*    pColorTable;            // Pointer to color table
        uint16              numOfColors;            // Number of colors in the table
        uint16              transparentColorIndex;  // Transparent color index
        uint16              bgColorIndex;           // Background color index
        uint16              frameWidth;             // Current frame width
        uint8               disposalMethod;         // Disposal method for current frame
    };


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static bool probe (const void* data, uint size);
    static CImage* createObject (void);
    static uint16 getCode (const uint8* data, uint offset, uint16 codeWidth);
    static uint calcBlockSize (const uint8* data, uint size);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_IMG_FORMAT(CImage::E_IMG_FORMAT_GIF, probe, createObject);


/**
 * @brief   Base check if image data is valid GIF image
 * @param   data: Image data
 * @param   size: Image data size
 * @return  true - data is valid GIF image
 */
bool probe (const void* data, uint size)
{
    if (!data || size < sizeof(TGifHdr)) return false;

    const TGifHdr* pHdr = (const TGifHdr*) data;
    if ( memcmp(pHdr->sign, GIF_SIGN, sizeof(TGifHdr::sign)) ) return false;

    if (!pHdr->width || !pHdr->height) return false;

    return true;
}


/**
 * @brief  Image object creation
 * @param  None
 * @return Pointer to created object or null on error
 */
CImage* createObject (void)
{
    return new CImageGif();
}


/**
 * @brief Constructor
 * @param None
 */
CImageGif::CImageGif () :
    CImage(),
    m_imgData(nullptr),
    m_imgDataSize(0),
    m_frames(0),
    m_currFrame(0)
{
}


/**
 * @brief Constructor
 * @param None
 */
CImageGif::~CImageGif ()
{
}


/**
 * @brief  Load image data into the object
 * @param  data: Pointer to image data
 * @param  size: Image data size
 * @return 0 or negative error code
 */
int CImageGif::Load (const void* data, uint size)
{
    if (!probe(data, size)) return -1;

    m_imgData = data;
    m_imgDataSize = size;

    int count = findBlock(GIF_BLOCK_IMG, -1);
    if (count <= 0)
    {
        m_imgData = nullptr;
        m_imgDataSize = 0;
        return -2;
    }

    m_frames = (uint)count;
    m_currFrame = 0;

    return 0;
}


/**
 * @brief  Get image width
 * @return Image width (pixels)
 */
uint CImageGif::GetWidth (void)
{
    return (m_imgData) ? ((const TGifHdr*)m_imgData)->width : 0;
}


/**
 * @brief  Get image height
 * @return Image height (pixels)
 */
uint CImageGif::GetHeight (void)
{
    return (m_imgData) ? ((const TGifHdr*)m_imgData)->height : 0;
}


/**
 * @brief  Check if animated image is looped
 * @return true - animated image is looped
 */
bool CImageGif::IsLooped (void)
{
    const uint8* pExtDesc = nullptr;
    uint extDataSize = 0;
    int count = findBlock(EXTENSION_CODE(GIF_EXT_APPLICATION), 0, (const void**)&pExtDesc, &extDataSize);
    if (count > 0 && extDataSize - 2 >= sizeof(TExtAppData))
    {
        const TExtAppData* pExtApp = (const TExtAppData*)(pExtDesc + 1);
        if (
            pExtApp->size_1 == 0x0b &&
            !memcmp(pExtApp->appCode, "NETSCAPE", sizeof(TExtAppData::appId)) &&
            !memcmp(pExtApp->appCode, "2.0", sizeof(TExtAppData::appCode)) &&
            pExtApp->size_2 == 3
        ) {
            return (pExtApp->loopCount > 0);
        }
    }

    // Default value
    return true;
}


/**
 * @brief  Set frame number of animated image to render
 * @return Number of frames of animated image
 */
int CImageGif::SetFrame (uint index)
{
    if (index >= GetFrames()) return -1;

    m_currFrame = index;

    return 0;
}


/**
 * @brief  Get specified frame information
 * @return 0 or negative error code
 */
int CImageGif::GetFrameInfo (uint index, TFrameInfo* pInfo)
{
    if (!pInfo || index >= GetFrames()) return -1;

    const TExtGraphCtrl* pGraphExt = nullptr;
    const TFrameHdr* pFrameDesc = getFrame(index, nullptr, &pGraphExt);
    if (!pFrameDesc) return -10;

    pInfo->left = pFrameDesc->left;
    pInfo->top = pFrameDesc->top;
    pInfo->width = pFrameDesc->width;
    pInfo->height = pFrameDesc->height;
    pInfo->duration = (pGraphExt && pGraphExt->delay > 10) ? pGraphExt->delay * 10 : 100;

    return 0;
}


/**
 * @brief  Image (or frame) rendering
 * @param  renderer: Renderer
 * @return Number of processed pixels or negative error code
 */
int CImageGif::Render (CRenderer& renderer)
{
    if (!m_imgData) return -1;

    const TExtGraphCtrl* pGraphExt = nullptr;
    uint dataSize = 0;
    const TFrameHdr* pFrameDesc = getFrame(m_currFrame, &dataSize, &pGraphExt);
    if (!pFrameDesc) return -10;

    TRenderDesc renderDesc = {.renderer = renderer};

    const TGifHdr* pHdr = (const TGifHdr*) m_imgData;
    if (pHdr->fields.globColorTab)
    {
        renderDesc.pColorTable = (const TGifColor*) (pHdr + 1);
        renderDesc.numOfColors = 1 << (pHdr->fields.colorTabSize + 1);
        renderDesc.bgColorIndex = pHdr->bgColorIndex;
    }

    renderDesc.transparentColorIndex = ~0;
    if (pGraphExt)
    {
        if (pGraphExt->fields.transparent)
        {
            renderDesc.transparentColorIndex = pGraphExt->transparentColorIndex;
        }

        renderDesc.disposalMethod = pGraphExt->fields.disposalMethod;
    }

    int res = renderFrame(pFrameDesc, dataSize, &renderDesc);
    return (res < 0 || !renderDesc.pixelCounter) ? - 20 : renderDesc.pixelCounter;
}


/**
 * @brief  Find specified type block
 * @param  blockType: Block type (GIF_BLOCK_xx or result of EXTENSION_CODE())
 * @param  index: Block index to find. If default value -1 is used specified type blocks will be counted.
 * @param  pBlock: Variable to save pointer to found block. Not used in counting mode.
 * @param  pSize: Variable to save found block size. Not used in counting mode.
 * @return Number of found blocks or negative error code
 */
int CImageGif::findBlock (uint16 blockType, int index, const void** pBlock, uint* pSize)
{
    if (!m_imgData || !m_imgDataSize) return -1;

    const TGifHdr* pHdr = (const TGifHdr*) m_imgData;
    const uint8* ptr = (const uint8*) (pHdr + 1);
    uint size = m_imgDataSize;

    if (size < sizeof(TGifHdr)) return -2;
    size -= sizeof(TGifHdr);

    if (pHdr->fields.globColorTab)
    {
        uint numOfColors = 1 << (pHdr->fields.colorTabSize + 1);
        uint colorTabSize = sizeof(TGifColor) * numOfColors;
        if (size <= colorTabSize) return -3;

        ptr += colorTabSize;
        size -= colorTabSize;
    }

    int matchedBlocks = 0;
    while (size)
    {
        uint16 type = *ptr;
        uint headerSize;
        uint dataSize;

        if (type == GIF_BLOCK_EXT)
        {
            headerSize = sizeof(TExtHdr);
            const TExtHdr* pExtDesc = (const TExtHdr*) ptr;
            type = EXTENSION_CODE(pExtDesc->label);
        }
        else if (type == GIF_BLOCK_IMG)
        {
            const TFrameHdr* pDesc = (const TFrameHdr*) ptr;
            if (!pDesc->width || !pDesc->height) return -10;

            headerSize = sizeof(TFrameHdr);
            if (pDesc->fields.localColorTab)
            {
                const uint numOfColors = 1 << (pDesc->fields.colorTabSize + 1);
                const uint colorTabSize = sizeof(TGifColor) * numOfColors;
                headerSize += colorTabSize;
            }
            // Add minimum code size field to the header
            ++headerSize;
        }
        else if (type == GIF_BLOCK_END)
        {
            break;
        }
        else
        {
            // Unknown block
            return -11;
        }

        if (size < headerSize) return -12;

        dataSize = calcBlockSize(ptr + headerSize, size - headerSize);
        if (!dataSize) return -13;

        if (type == blockType)
        {
            ++matchedBlocks;

            if (!index)
            {
                if (pBlock) *pBlock = ptr;
                if (pSize) *pSize = dataSize;
                break;
            }

            --index;
        }

        ptr += headerSize + dataSize;
        size -= headerSize + dataSize;
    }

    return matchedBlocks;
}


/**
 * @brief  Get frame block
 * @param  frameIndex: Frame index to find
 * @param  pSize: Variable to save found block size
 * @param  pGraphDesc: Variable to save pointer to appropriate Graphic extension block
 * @return Pointer to frame image header or null on error
 */
const CImageGif::TFrameHdr* CImageGif::getFrame (uint frameIndex, uint* pSize, const TExtGraphCtrl** pGraphDesc)
{
    const TFrameHdr* pFrameDesc = nullptr;

    int count = findBlock(GIF_BLOCK_IMG, (int)frameIndex, (const void**)&pFrameDesc, pSize);
    if (count <= 0) return nullptr;

    if (pGraphDesc)
    {
        const TExtHdr* pExtDesc = nullptr;
        uint extDataSize = 0;
        count = findBlock(EXTENSION_CODE(GIF_EXT_GRAPHICS_CTRL), (int)frameIndex, (const void**)&pExtDesc, &extDataSize);
        // 2 - additional bytes in data block 'size' field and block terminator at the end
        if (count > 0 && (extDataSize - 2) >= sizeof(TExtGraphCtrl))
        {
            *pGraphDesc = (const TExtGraphCtrl*)( ((const uint8*)pExtDesc) + sizeof(TExtHdr) + 1 );
        }
    }

    return pFrameDesc;
}


/**
 * @brief  Render frame
 * @param  pDesc: Frame descriptor
 * @param  codesDataSize: Size of data block with codes
 * @param  pRenderDesc: Render descriptor
 * @return 0 or negative error code
 */
int CImageGif::renderFrame (const TFrameHdr* pDesc, uint codesDataSize, TRenderDesc* pRenderDesc)
{
    if (!pDesc || !codesDataSize || !pRenderDesc) return -1;
    if (pDesc->fields.interlace) return -2; // Unsupported

    const uint8* ptr = (const uint8*) (pDesc + 1);
    if (pDesc->fields.localColorTab)
    {
        pRenderDesc->pColorTable = (const TGifColor*)ptr;
        pRenderDesc->numOfColors = 1 << (pDesc->fields.colorTabSize + 1);

        const uint colorTabSize = sizeof(TGifColor) * pRenderDesc->numOfColors;
        ptr += colorTabSize;
    }
    if (!pRenderDesc->pColorTable || !pRenderDesc->numOfColors) return -3;

    // Replace with frame size and position instead of whole screen
    pRenderDesc->pixelCounter = 0;

    if (!pDesc->width) return -4;
    pRenderDesc->frameWidth = pDesc->width;

    const uint16 minCodeSize = *ptr++;
    const uint16 codeClear = pRenderDesc->numOfColors;
    const uint16 codeEnd = codeClear + 1;
    const uint16 initCodeTabSize = codeEnd + 1;
    uint16 codeWidth = minCodeSize + 1;
    if (codeWidth < GIF_CODE_MIN_BITS) return -5;

    uint maxCodesCount = codesDataSize * 8 / codeWidth;
    TCodeTab* pCodeTab = codeTableCreate(pRenderDesc->pColorTable, initCodeTabSize, maxCodesCount);
    if (!pCodeTab) return -10;

    uint16 prevCode = codeClear;
    uint16 partCode = 0;
    uint16 partCodeWidth = 0;

    while (1)
    {
        const uint blockSize = *ptr++;
        if (!blockSize) break;
        const uint maxOffset = blockSize * 8;

        for (uint offset = 0; offset < maxOffset; )
        {
            const uint availableWidth = maxOffset - offset;
            if (availableWidth < codeWidth)
            {
                partCode = getCode(ptr, offset, availableWidth);
                partCodeWidth = availableWidth;
                break;
            }

            uint16 code;
            if (!offset && partCodeWidth)
            {
                if (partCodeWidth >= codeWidth) break;

                uint16 restWidth = codeWidth - partCodeWidth;
                uint16 restCode = getCode(ptr, offset, restWidth);
                code = (restCode << partCodeWidth) | partCode;
                offset += restWidth;
                partCodeWidth = 0;
            }
            else
            {
                code = getCode(ptr, offset, codeWidth);
                offset += codeWidth;
            }

            if (code >= GIF_CODE_TAB_SIZE) break;

            if (code == codeEnd) break;
            if (code == codeClear)
            {
                codeTableClear(pCodeTab);
                codeWidth = minCodeSize + 1;
                prevCode = codeClear;
                continue;
            }

            if (prevCode == codeClear)
            {
                codeTableOut(pCodeTab, code, pRenderDesc);
                prevCode = code;
                continue;
            }

            uint16 k;
            if (codeTableIsCodeExists(pCodeTab, code))
            {
                k = codeTableGetFirst(pCodeTab, code);
                codeTableOut(pCodeTab, code, pRenderDesc);
            }
            else
            {
                k = codeTableGetFirst(pCodeTab, prevCode);
                codeTableOut(pCodeTab, prevCode, pRenderDesc);
                codeTableOut(pCodeTab, k, pRenderDesc);
            }

            uint16 codeIndex = codeTableAdd(pCodeTab, prevCode, k);
            prevCode = code;

            // Check if code bit-width must be increased
            const uint maxIndex = (1 << codeWidth) - 1;
            if (codeIndex == maxIndex && codeWidth < GIF_CODE_MAX_BITS)
            {
                ++codeWidth;
            }
        }

        ptr += blockSize;
    }

    codeTableDestroy(pCodeTab);

    return 0;
}


/**
 * @brief Save pixel with specified color index into render buffer
 * @param pRenderDesc: Render descriptor
 * @param colorIndex: Index of pixel color
 */
void CImageGif::putPixel (TRenderDesc* pRendDesc, uint8 colorIndex)
{
    if (!pRendDesc || colorIndex >= pRendDesc->numOfColors) return;

    TColor color = {};

    if (
        pRendDesc->transparentColorIndex < pRendDesc->numOfColors &&
        colorIndex == pRendDesc->transparentColorIndex
    ) {
        if (pRendDesc->bgColorIndex && pRendDesc->disposalMethod == 2)
        {
            const TGifColor* pColor = &pRendDesc->pColorTable[pRendDesc->bgColorIndex];
            color.r = pColor->r;
            color.g = pColor->g;
            color.b = pColor->b;
            color.a = 0xff;
        }
    }
    else
    {
        const TGifColor* pColor = &pRendDesc->pColorTable[colorIndex];
        color.r = pColor->r;
        color.g = pColor->g;
        color.b = pColor->b;
        color.a = 0xff;
    }

    const int16 x = pRendDesc->pixelCounter % pRendDesc->frameWidth;
    const int16 y = pRendDesc->pixelCounter / pRendDesc->frameWidth;
    pRendDesc->renderer.PutPixel(x, y, color);

    ++pRendDesc->pixelCounter;
}


/**
 * @brief  Code table creation
 * @param  pColorTable: Pointer to used color table
 * @param  initTabSize: Initial size of code table
 * @param  maxCodesCount: Maximum number of codes in the table
 * @return Pointer to created table descriptor or negative error code
 */
CImageGif::TCodeTab* CImageGif::codeTableCreate (const void* pColorTable, uint16 initTabSize, uint maxCodesCount)
{
    if (!pColorTable) return nullptr;

    uint itemsCount = (maxCodesCount > GIF_CODE_TAB_SIZE) ? GIF_CODE_TAB_SIZE : maxCodesCount;
    const uint dataSize = sizeof(TCodeTab) + itemsCount * sizeof(TCodeTabItem);

    TCodeTab* pTable = (TCodeTab*) malloc(dataSize);
    if (!pTable) return nullptr;

    pTable->pColorTable = (const TGifColor*)pColorTable;
    pTable->initSize = initTabSize;
    pTable->count = 0;
    pTable->size = itemsCount;
    memset(pTable->items, 0x00, itemsCount * sizeof(TCodeTabItem));

    return pTable;
}


/**
 * @brief Destroy code table
 * @param pDesc: Code table to destroy
 */
void CImageGif::codeTableDestroy (TCodeTab* pDesc)
{
    if (!pDesc) return;

    free(pDesc);
}


/**
 * @brief  Clear code table
 * @param  pDesc: Code table to destroy
 */
void CImageGif::codeTableClear (TCodeTab* pDesc)
{
    if (!pDesc) return;

    pDesc->count = 0;
}


/**
 * @brief  Add code into code table
 * @param  pDesc: Code table descriptor
 * @param  prevCode: Previous code in the sequence of codes
 * @param  value: Code to add (last value in list of values)
 * @return Added code value
 */
uint16 CImageGif::codeTableAdd (TCodeTab* pDesc, uint16 prevCode, uint16 value)
{
    if (!pDesc) return 0;
    if (pDesc->count >= pDesc->size) return 0;
    if (prevCode >= pDesc->initSize + pDesc->count) return 0;

    uint16 newIndex = pDesc->count;
    pDesc->items[newIndex].prevCode = prevCode;
    pDesc->items[newIndex].value = value;
    ++pDesc->count;

    return pDesc->initSize + newIndex;
}


/**
 * @brief  Read first value from sequence of code values
 * @param  pDesc: Code table descriptor
 * @param  code: Code of a sequence
 * @return First code value or 0 on error
 */
uint16 CImageGif::codeTableGetFirst (TCodeTab* pDesc, uint16 code)
{
    if (!pDesc) return 0;

    while (code < pDesc->initSize + pDesc->count)
    {
        if (code < pDesc->initSize) return code;

        uint16 index = code - pDesc->initSize;
        code = pDesc->items[index].prevCode;
    }

    return 0;
}


/**
 * @brief  Count number of codes in specified sequence
 * @param  pDesc: Code table descriptor
 * @param  code: Code value
 * @return Nuber of values in sequence
 */
uint CImageGif::codeTableCount (TCodeTab* pDesc, uint16 code)
{
    if (!pDesc) return 0;

    uint counter = 0;
    while (code < pDesc->initSize + pDesc->count)
    {
        ++counter;
        if (code < pDesc->initSize) break;

        uint16 index = code - pDesc->initSize;
        code = pDesc->items[index].prevCode;
    }

    return counter;
}


/**
 * @brief  Check if code exists in the table
 * @param  pDesc: Code table descriptor
 * @param  code: Code value
 * @return True - code presents in the table
 */
bool CImageGif::codeTableIsCodeExists (TCodeTab* pDesc, uint16 code)
{
    if (!pDesc) return false;
    if (code < pDesc->initSize) return true;

    uint16 itemIndex = code - pDesc->initSize;
    return (itemIndex < pDesc->count);
}


/**
 * @brief Output sequence of color indexes (putPixel() is called for each one)
 * @param pDesc: Code table descriptor
 * @param pRenderDesc: Render descriptor
 */
void CImageGif::codeTableOut (TCodeTab* pDesc, uint16 code, TRenderDesc* pRendDesc)
{
    if (!pDesc) return;

    if (code < pDesc->initSize)
    {
        putPixel(pRendDesc, code);
        return;
    }

    uint count = codeTableCount(pDesc, code);
    if (!count) return;

    uint16* values = (uint16*) malloc(count * sizeof(uint16));
    if (!values) return;

    // Fill array by values from the end to the begining (because of table structure - it's linked list)
    for (int i = count - 1; i >= 0; --i)
    {
        uint16 val;
        if (code < pDesc->initSize)
        {
            val = code;
        }
        else
        {
            uint16 itemIndex = code - pDesc->initSize;
            if (itemIndex >= pDesc->count) break;

            val = pDesc->items[itemIndex].value;
            code = pDesc->items[itemIndex].prevCode;
        }

        values[i] = val;
    }

    // Output pixel data in right order
    for (uint i = 0; i < count; ++i)
    {
        putPixel(pRendDesc, values[i]);
    }

    free(values);
}


/**
 * @brief  Get code from data stream
 * @param  data: Begining of data block
 * @param  offset: Code offset in bit-stream (number of bits from the begining of data block)
 * @param  codeWidth: Code size (number of bits)
 * @return Code value or 0xffff on error
 */
uint16 getCode (const uint8* data, uint offset, uint16 codeWidth)
{
    if (!data) return 0xffff;
    if (!codeWidth || codeWidth > GIF_CODE_MAX_BITS) return 0xffff;

    uint16 code = 0;
    for (uint i = 0; i < codeWidth; ++i, ++offset)
    {
        uint byte = offset >> 3;
        uint bit = offset & 0x07;

        if (data[byte] & (1 << bit)) code |= 1 << i;
    }

    return code;
}


/**
 * @brief  Data block total size calculation
 * @param  data: Pointer to data block (starts with block size)
 * @param  size: Data size (maximum size to process)
 * @return Size of specified block or 0 on error
 */
uint calcBlockSize (const uint8* data, uint size)
{
    if (!data || !size) return 0;

    uint dataSize = 0;
    while (size)
    {
        uint blockSize = *data;
        if (!blockSize) break;
        // Add length field to block size
        ++blockSize;

        if (size < blockSize)
        {
            size = 0;
            break;
        }

        dataSize += blockSize;
        data += blockSize;
        size -= blockSize;
    }

    // +1 - for block terminator 0x00
    return (!size || *data) ? 0 : (dataSize + 1);
}
