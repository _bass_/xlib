//*****************************************************************************
//
// @brief   Font renderer interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/font.h"
#include "misc/macros.h"
#include <string.h>


/**
 * @brief  Constructor
 * @param  width: Screen width (pixels)
 * @param  height: Screen height (pixels)
 */
IFont::IFont ()
{
}


/**
 * @brief  Destructor
 * @param  None
 */
IFont::~IFont ()
{
}


/**
 * @brief  Set font height
 * @param  height: Font height (pixels)
 * @return 0 or negative error code
 */
int IFont::SetHeight (uint height)
{
    UNUSED_VAR(height);
    return -1;
}


/**
 * @brief  Get space between symbols
 * @return Space between symbols (pixels)
 */
uint IFont::GetSpace (void)
{
    return 0;
}


/**
 * @brief  Get symbol width
 * @param  c: Symbol code
 * @param  pLen: Pointer to string variable length. If it is null or value is 0, whole string is processed.
 *               The variable will contain number of processed symbols if pointer wasn't null.
 * @return Symbol width (pixels)
 */
uint IFont::GetWidth (const char* str, uint* pLen)
{
    if (!str) return 0;

    uint len = strlen(str);
    if (pLen && *pLen && *pLen < len) len = *pLen;

    const uint space = GetSpace();
    uint width = 0;

    for (uint i = 0; i < len; ++i)
    {
        const char c = str[i];

        // Stop width calculation at the end of the line
        if (c == '\r' || c == '\n')
        {
            len = i + 1;
            break;
        }

        const uint w = GetWidth((int)c);
        if (!w) continue;

        if (width) width += space;
        width += w;
    }

    if (pLen) *pLen = len;

    return width;
}
