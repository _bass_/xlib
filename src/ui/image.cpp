//*****************************************************************************
//
// @brief   Base images functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/image.h"
#include <string.h>


/**
 * @brief  Constructor
 * @param  None
 */
CImage::CImage ()
{
}


/**
 * @brief  Constructor
 * @param  None
 */
CImage::~CImage ()
{
}


/**
 * @brief  Image object creation
 * @param  type: Image type
 * @return Pointer to created object or null on error
 */
CImage* CImage::CreateObject (EFormat type)
{
    SECTION_FOREACH(xLib_ui_img, const CImage::TFormatDesc*, itr)
    {
        const CImage::TFormatDesc* pDesc = *itr;

        if (pDesc->format != type) continue;
        if (!pDesc->hdlCreator) break;

        return pDesc->hdlCreator();
    }

    return nullptr;
}


/**
 * @brief  Image object creation
 * @param  data: Pointer to image data
 * @param  size: Image data size
 * @return Pointer to created object or null on error
 */
CImage* CImage::CreateObject (const void* data, uint size)
{
    if (!data || !size) return nullptr;

    SECTION_FOREACH(xLib_ui_img, const CImage::TFormatDesc*, itr)
    {
        const CImage::TFormatDesc* pDesc = *itr;

        if (!pDesc->hdlProbe || !pDesc->hdlProbe(data, size)) continue;
        if (!pDesc->hdlCreator) break;

        CImage* pObj = pDesc->hdlCreator();
        if (!pObj) break;

        if (pObj->Load(data, size) < 0)
        {
            delete pObj;
            break;
        }

        return pObj;
    }

    return nullptr;
}


int CImage::Crop (TColor* pData, uint width, uint height, uint x, uint y, uint resWidth, uint resHeight)
{
    if (!pData) return -1;
    if (x >= width || y >= height) return -2;

    if (resWidth >= width - x) resWidth = width - x - 1;
    if (resHeight >= height - y) resHeight = height - y - 1;

    const uint pixelsTotal = width * height;
    if (!x && !y && width == resWidth && height == resHeight) return pixelsTotal;

    TColor* pSrc = pData;
    TColor* pDst = pData;
    uint distance;

    for (uint i = 0; i < pixelsTotal; i += distance, pSrc += distance)
    {
        distance = 0;
        uint srcY = i / width;
        uint srcX = i % width;

        if (!distance && srcY < y) distance = (y - srcY) * width;
        if (!distance && srcX < x) distance = x - srcX;
        if (!distance && srcX >= x + resWidth) distance = width - srcX;
        if (!distance && srcY >= y + resHeight) break;

        if (distance) continue;

        memmove(pDst, pSrc, sizeof(TColor) * resWidth);
        distance = resWidth;
        pDst += distance;
    }

    return (uint8*)pDst - (uint8*)pData;
}
