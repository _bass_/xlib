//*****************************************************************************
//
// @brief   Area functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/area.h"
#include "misc/macros.h"


/**
 * @brief Default constructor (creates empty area)
 */
CArea::CArea (void):
    m_x0(0),
    m_y0(0),
    m_width(0),
    m_height(0)
{
}

/**
 * @brief Constructor
 * @param x0: X coordinate of start point (upper-left)
 * @param y0: Y coordinate of start point (upper-left)
 * @param width: Width of the area
 * @param height: Height of the area
 */
CArea::CArea (int16 x0, int16 y0, uint16 width, uint16 height) :
    m_x0(x0),
    m_y0(y0),
    m_width(width),
    m_height(height)
{
}


/**
 * @brief Check if the area has intersection with specified one
 * @param area: Area to check intersection with
 * @return True - Areas have intersection otherwise - false
 */
bool CArea::HasIntersection (const CArea& area) const
{
    if (!m_width || !m_height || !area.width() || !area.height()) return false;

    if (m_x0 < area.x0())
    {
        if (m_x0 + m_width < area.x0()) return false;
    }
    else
    {
        if (area.x0() + area.width() < m_x0) return false;
    }

    if (m_y0 < area.y0())
    {
        if (m_y0 + m_height < area.y0()) return false;
    }
    else
    {
        if (area.y0() + area.height() < m_y0) return false;
    }

    return true;
}


/**
 * @brief Check if the area contains the point
 * @param x: X coordinate of the point
 * @param y: Y coordinate of the point
 * @return True - The area includes the point, otherwise - false
 */
bool CArea::Contains (int16 x, int16 y) const
{
    if (!m_width || !m_height) return false;
    if (x < m_x0 || x > m_x0 + m_width) return false;
    if (y < m_y0 || y > m_y0 + m_height) return false;

    return true;
}


/**
 * @brief Find intersection with specified area
 * @param area: Area to find intersection with
 * @return Intersection area
 */
CArea CArea::Intersection (const CArea& area) const
{
    CArea intersection;

    if (HasIntersection(area))
    {
        intersection.SetX0( MAX(m_x0, area.x0()) );
        intersection.SetY0( MAX(m_y0, area.y0()) );

        int16 end = MIN(m_x0 + m_width, area.x0() + area.width());
        intersection.SetWidth(end - intersection.x0());

        end = MIN(m_y0 + m_height, area.y0() + area.height());
        intersection.SetHeight(end - intersection.y0());
    }

    return intersection;
}


/**
 * @brief Extend the area to include specified point
 * @param x: X coordinate of the point to include
 * @param y: Y coordinate of the point to include
 */
void CArea::Extend (int16 x, int16 y)
{
    if (m_width)
    {
        if (x < m_x0)
        {
            m_width += m_x0 - x;
            m_x0 = x;
        }
        else if (x > m_x0 + m_width)
        {
            m_width = x - m_x0;
        }
    }
    else
    {
        m_x0 = x;
        m_width = 1;
    }

    if (m_height)
    {
        if (y < m_y0)
        {
            m_height += m_y0 - y;
            m_y0 = y;
        }
        else if (y > m_y0 + m_height)
        {
            m_height = y - m_y0;
        }
    }
    else
    {
        m_y0 = y;
        m_height = 1;
    }
}


/**
 * @brief Join areas. Increase the area to include specified one.
 * @param area: Area to include
 */
void CArea::Join (const CArea& area)
{
    Extend(area.x0(), area.y0());

    int16 end = area.x0() + area.width();
    if (m_x0 + m_width < end)
    {
        m_width = end - m_x0;
    }

    end = area.y0() + area.height();
    if (m_y0 + m_height < end)
    {
        m_height = end - m_y0;
    }
}
