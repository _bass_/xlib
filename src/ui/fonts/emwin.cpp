//*****************************************************************************
//
// @brief   Font renderer for EmWin fonts
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/fonts/emwin.h"


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static const TEmwinCharInfo* getCharInfo (int c, const TEmwinFontDesc* pDesc);
    static uint getPixelsPerByte (TEmwinFontType type);


/**
 * @brief  Constructor
 * @param  pDesc: Pointer to font descriptor
 */
CFontEmwin::CFontEmwin (const TEmwinFontDesc* pDesc) :
    IFont(),
    m_pDesc(pDesc)
{
}


/**
 * @brief  Destructor
 * @param  None
 */
CFontEmwin::~CFontEmwin ()
{
}


/**
 * @brief  Get font height
 * @return Font height (pixels)
 */
uint CFontEmwin::GetHeight (void)
{
    if (!m_pDesc) return 0;

    return m_pDesc->height;
}


/**
 * @brief  Get symbol width
 * @param  c: Symbol code
 * @return Symbol width (pixels)
 */
uint CFontEmwin::GetWidth (int c)
{
    if (!m_pDesc) return 0;

    const TEmwinCharInfo* pInfo = getCharInfo(c, m_pDesc);
    return (pInfo) ? pInfo->width : 0;
}


/**
 * @brief  Render symbol
 * @param  renderer: Renderer
 * @param  c: Symbol code
 * @param  color: Text color
 * @return 0 or negative error code
 */
int CFontEmwin::Render (CRenderer& renderer, int c, TColor color)
{
    const TEmwinCharInfo* pInfo = getCharInfo(c, m_pDesc);
    if (!pInfo) return -1;

    const uint height = GetHeight();
    const uint pixelsCount = pInfo->width * height;

    const uint pixelsInbyte = getPixelsPerByte(m_pDesc->type);
    if (!pixelsInbyte) return -2;

    const uint bitsInPixel = 8 / pixelsInbyte;
    const uint8 mask = (uint8) ~(0xff << bitsInPixel);

    uint16 x = 0;
    uint16 y = 0;
    for (uint i = 0; i < pixelsCount; ++i, ++x)
    {
        if (x >= pInfo->width)
        {
            x = 0;
            ++y;
        }

        const uint bytePos = y * pInfo->bytesPerLine + x / pixelsInbyte;
        const uint pixelIndex = x % pixelsInbyte;
        uint maskOffset = (pixelsInbyte - pixelIndex - 1) * bitsInPixel;
        uint8_t factor = (pInfo->pData[bytePos] >> maskOffset) & mask;
        if (factor == 0) continue;

        TColor pxColor = color;
        if (factor != mask) pxColor.a = (color.a >> bitsInPixel) * factor;

        renderer.PutPixel(x, y, pxColor);
    }

    return 0;
}


/**
 * @brief  Get symbol descriptor
 * @param  c: Symbol code
 * @param  pDesc: Pointer to font descriptor
 * @return Pointer to symbol descriptor or null on error
 */
const TEmwinCharInfo* getCharInfo (int c, const TEmwinFontDesc* pDesc)
{
    if (!pDesc) return nullptr;

    for (const TEmwinFontProp* pProp = pDesc->pProp; pProp; pProp = pProp->pNext)
    {
        if (c < pProp->firstChar) break;
        if (c > pProp->lastChar) continue;

        return &pProp->pInfo[c - pProp->firstChar];
    }

    return nullptr;
}


/**
 * @brief  Get number of pixels in one byte of symbol's data
 * @param  type: Font type
 * @return Number of pixels in a byte or 0 on error
 */
uint getPixelsPerByte (TEmwinFontType type)
{
    switch (type)
    {
        case GUI_FONTTYPE_PROP_AA2: return 4;
        case GUI_FONTTYPE_PROP_AA4: return 2;
        default:                    return 0;
    }
}
