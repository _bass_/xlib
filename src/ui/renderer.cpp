//*****************************************************************************
//
// @brief   Renderer functionality
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "ui/renderer.h"
#include <string.h>


/**
 * @brief Constructor
 * @param buff: Pointer to buffer to store pixels data (canvas)
 * @param width: Canvas width (pixels)
 * @param height: Canvas height (pixels)
 */
CRenderer::CRenderer (TColor* buff, uint16 width, uint16 height):
    m_buff(buff),
    m_canvasWidth(width),
    m_canvasHeight(height),
    m_offsetX(0),
    m_offsetY(0),
    m_blendMode(E_BLEND_NONE)
{
    memset(m_buff, 0x00, width * height * sizeof(TColor));
}


/**
 * @brief Put pixel on the canvas
 * @param x: X-coordinate of pixel position
 * @param y: Y-coordinate of pixel position
 * @param color: Pixel color
 */
void CRenderer::PutPixel (int16 x, int16 y, TColor color)
{
    // Convert point coordinates into position on the canvas
    x += m_offsetX;
    y += m_offsetY;

    if (x < 0 || x >= m_canvasWidth || y < 0 || y >= m_canvasHeight) return;

    const uint index = y * m_canvasWidth + x;
    blend(&color, &m_buff[index]);
}


/**
 * @brief Fill rectangle with specified color
 * @param x: X-coordinate of top-left corner of the rectangle
 * @param y: Y-coordinate of top-left corner of the rectangle
 * @param w: Rectangle width
 * @param h: Rectangle height
 * @param color: Color to fill with
 */
void CRenderer::Fill (int16 x, int16 y, uint16 w, uint16 h, TColor color)
{
    if (!w || !h) return;

    // Convert to canvas coordinates
    x += m_offsetX;
    y += m_offsetY;
    if (x < 0 || y < 0) return;

    CArea canvas(0, 0, m_canvasWidth, m_canvasHeight);
    CArea area(x, y, w, h);

    area = canvas.Intersection(area);
    if (!area.width() || !area.height()) return;

    for (uint16 row = area.y0(); row < area.y0() + area.height(); ++row)
    {
        TColor* ptr = &m_buff[row * m_canvasWidth + area.x0()];
        uint16 count = area.width();
        while(count--)
        {
            blend(&color, ptr);
            ++ptr;
        }
    }
}


/**
 * @brief Colors blending
 * @param pSrc: Pointer to source
 * @param pDst: Pointer to destination
 */
void CRenderer::blend (const TColor* pSrc, TColor* pDst)
{
    if (!pSrc || !pDst) return;

    if (m_blendMode == E_BLEND_NONE)
    {
        *pDst = *pSrc;
        return;
    }

    const TColor* pBg;
    const TColor* pFg;

    if (m_blendMode == E_BLEND_ADD_BG)
    {
        pBg = pSrc;
        pFg = pDst;

        // Don't change opaque pixel
        if (pFg->a == 0xff) return;
        // Don't blend with invisible pixel
        if (pBg->a == 0) return;

        if (pFg->a == 0)
        {
            *pDst = *pSrc;
            return;
        }
    }
    else if (m_blendMode == E_BLEND_ADD_FG)
    {
        pBg = pDst;
        pFg = pSrc;

        // Don't change anything if it's a trying to add invisible pixel
        if (pFg->a == 0) return;

        if (pFg->a == 0xff || pBg->a == 0)
        {
            *pDst = *pSrc;
            return;
        }
    }
    else
    {
        // Unknown mode
        return;
    }

    uint alphaBg = (pBg->a * (256 - pFg->a)) >> 8;
    uint alphaRes = pFg->a + alphaBg;
    for (uint i = 0; i < 3; ++i)
    {
        pDst->data[i] = (pFg->data[i] * pFg->a + pBg->data[i] * alphaBg) / alphaRes;
    }
    pDst->a = alphaRes;
}
