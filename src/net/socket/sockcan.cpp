//*****************************************************************************
//
// @brief   CAN socket
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "net/socket/sockcan.h"
#include "kernel/kdebug.h"
#include "misc/macros.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CSocket* sockCreate (uint family, TSockType type);
    static int sockParser (TSkb* skb);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_SOCKET(NET_FAMILY_CAN, SOCK_TYPE_DGRAM, sockCreate, sockParser);
    REGISTER_DEBUG("sockcan", "SockCAN", NULL, NULL);


/**
  * @brief  Create socket object
  * @param  family: Network (socket) protocol family
  * @param  type: Socket type
  * @return Socket object or NULL on error
  */
CSocket* sockCreate (uint family, TSockType type)
{
    if (family != NET_FAMILY_CAN || type != SOCK_TYPE_DGRAM) return NULL;

    return new CSockCan();
}


/**
  * @brief  Parse socket buffer data (complete packet creation)
  * @param  skb: Socket packet descriptor
  * @return < 0 - brocken packet
  *         0 - packet successfully created
  *         > 0 - collection in progress (number of remaining data)
  */
int sockParser (TSkb* skb)
{
    if ( skb->dataSize < OFFSET(TCanFrame, data) ) return OFFSET(TCanFrame, data) - skb->dataSize;

    TCanFrame* frame = (TCanFrame*) skb->data;
    if ( frame->len > sizeof(TCanFrame::data) ) return -1;

    // Waiting for complete packet data receiving
    if (skb->dataSize < OFFSET(TCanFrame, data) + frame->len)
    {
        return OFFSET(TCanFrame, data) + frame->len - skb->dataSize;
    }

    // Receive address address checking isn't performed because all packets are sending broadcasted

    skb->data = (void*) frame->data;
    skb->dataSize = frame->len;

    // Save receiver address because socket might be in listening mode with addr = INADDR_ANY
    TAddrCan* pAddr = (TAddrCan*) &skb->addr;
    pAddr->family = NET_FAMILY_CAN;
    pAddr->addr = frame->addr;

    skb->socket = CSocket::Route(skb);

    return 0;
}


/**
  * @brief  Constructor
  * @param  None
  */
CSockCan::CSockCan () :
    CSocket(NET_FAMILY_CAN, SOCK_TYPE_DGRAM, SOCK_CAN_BUFF_PACK_COUNT),
    m_currBuffIndex(0)
{
}


/**
  * @brief  Send data to specified address
  * @param  pAddr: Recepient address
  * @param  data: Pointer to data
  * @param  size: Data size
  * @return Amount of sent data or negative error code
  */
int CSockCan::Send (const TNetAddr* pAddr, const void* data, uint size)
{
    if ( (size && !data) || size > sizeof(TCanFrame::data) ) return -1;

    const TAddrCan* pAddrCan = (const TAddrCan*) pAddr;
    if (!pAddrCan || pAddrCan->family != NET_FAMILY_CAN) return -2;

    CDevNet* pDev = route(pAddr);
    if (!pDev) return -3;

    TCanFrame frame;
    frame.addr = pAddrCan->addr;
    frame.len = size;
    memcpy(frame.data, data, size);

    TSkb skb =
    {
        .socket = this,
        .pDev = pDev,
        .data = (void*)&frame,
        .dataSize = (uint16) (OFFSET(TCanFrame, data) + frame.len),
        .buffSize = sizeof(frame),
        .addr = *( (const TNetAddr*)pAddrCan )
    };

    int res = pDev->Write(&skb);

    return (res == skb.dataSize) ? (int)size : -4;
}


/**
  * @brief  Incoming packets handler.
  *         Copying data to socket buffer, packet adding to incoming queue (m_qIn)
  * @param  skb: Socket packet descriptor
  */
void CSockCan::onReceivePacket (const TSkb* skb)
{
    const TAddrCan* pAddrCan = (const TAddrCan*) &skb->addr;

    Kernel::EnterCritical();
    {
        // Drop packet if there is no space in queue
        if ( m_qIn.IsFull() )
        {
            Kernel::ExitCritical();
            kdebug("DROP [id:%u, rtr:%u]", pAddrCan->addr.id, pAddrCan->addr.rtr);
            return;
        }

        // Copy skb to local one and put data in local buffer
        TSkb locSkb = *skb;

        if (locSkb.dataSize)
        {
            memcpy(m_buffRx[m_currBuffIndex], locSkb.data, locSkb.dataSize);
        }

        locSkb.data = m_buffRx[m_currBuffIndex];
        locSkb.buffSize = sizeof(m_buffRx[0]);

        ++m_currBuffIndex;
        if (m_currBuffIndex >= SOCK_CAN_BUFF_PACK_COUNT) m_currBuffIndex = 0;

        m_qIn.Add(&locSkb);
    }
    Kernel::ExitCritical();
}


/**
  * @brief  Checking for addresses matching for incoming packet
  * @param  addrRemote: Remote address
  * @param  addrLocal: Local address
  * @return true - addresses matched
  */
bool CSockCan::checkAddrIn (const TNetAddr* addrRemote, const TNetAddr* addrLocal)
{
    if (m_mode == E_SOCK_MODE_SERVER)
    {
        return (addrLocal && addrLocal->family == NET_FAMILY_CAN);
    }
    else if (m_mode == E_SOCK_MODE_CLIENT)
    {
        return (addrRemote && addrRemote->family == NET_FAMILY_CAN);
    }

    return false;
}


/**
  * @brief  Checking for addresses matching for outgoing packet
  * @param  addrRemote: Remote address
  * @param  pNetDev: Network interface
  * @return true - addresses matched
  */
bool CSockCan::checkAddrOut (const TNetAddr* addrRemote, CDevNet* pNetDev)
{
    UNUSED_VAR(addrRemote);

    TNetAddr devAddr = {};
    pNetDev->GetAddr(&devAddr);

    return (devAddr.family == NET_FAMILY_CAN);
}
