//*****************************************************************************
//
// @brief   TCP socket (lwip)
// @author  Vadim Mezhlumov
//
//*****************************************************************************
#pragma once

#include "net/socket/socktcp.h"
#include "kernel/kernel.h"
#include "kernel/kdebug.h"
#include "lwip/ip_addr.h"
#include "lwip/tcp.h"
#include "misc/macros.h"
#include "misc/math.h"
#include "misc/time.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    #if defined(CFG_SOCK_LORAST_BUFF_PACKET_COUNT)
        #define SOCK_TCP_BUFF_PACKET_COUNT              CFG_SOCK_TCP_BUFF_PACKET_COUNT
    #else
        #define SOCK_TCP_BUFF_PACKET_COUNT              1
    #endif


// ----------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------

    static CSocket* sockCreate (uint family, TSockType type);
    static int sockParser (TSkb* skb);


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    REGISTER_SOCKET(NET_FAMILY_INET, SOCK_TYPE_STREAM, sockCreate, sockParser);
    REGISTER_DEBUG("socktcp", "SockTcp", NULL, NULL);


/**
  * @brief  Create socket object
  * @param  family: Network (socket) protocol family
  * @param  type: Socket type
  * @return Socket object or NULL on error
  */
CSocket* sockCreate (uint family, TSockType type)
{
    if (family != NET_FAMILY_INET || type != SOCK_TYPE_STREAM) return NULL;
    return new CSockTcp();
}


/**
  * @brief  Parse socket buffer data (complete packet creation)
  * @param  skb: Socket packet descriptor
  * @return < 0 - brocken packet
  *         0 - packet successfully created
  *         > 0 - collection in progress (number of remaining data)
  */
int sockParser (TSkb* skb)
{
    // Isn't used
    return -1;
}


/**
  * @brief  Constructor
  * @param  None
  */
CSockTcp::CSockTcp () :
    CSocket(NET_FAMILY_INET, SOCK_TYPE_STREAM, SOCK_TCP_BUFF_PACKET_COUNT)
{
    m_conn = NULL;
    m_pNetBuff = NULL;
    m_offset = 0;
}


/**
  * @brief  Destructor
  * @param  None
  */
CSockTcp::~CSockTcp ()
{
    Disconnect();
}


/**
  * @brief  Send data to specified address
  * @param  pAddr: Recepient address
  * @param  data: Pointer to data
  * @param  size: Data size
  * @return Amount of sent data or negative error code
  */
int CSockTcp::Send (const TNetAddr* pAddr, const void* data, uint size)
{
    if (!data || !size) return -1;
    if (pAddr != &m_addrRemote || m_addrRemote.family != NET_FAMILY_INET) return -2;
    
    netconn_set_sendtimeout(m_conn, m_optTimeout * 1000);
    err_t result = netconn_write(m_conn, data, size, NETCONN_COPY);
    
    return (result == ERR_OK) ? size : result;
}


/**
  * @brief  Receive data
  * @param  pAddr: Pointer to variable to save sender address
  * @param  buff: Pointer to buffer for received data
  * @param  size: Buffer size
  * @return Amount of written into the buffer data or negative error code
  */
int CSockTcp::Recv (TNetAddr* pAddr, void* buff, uint size)
{
    if (!m_pNetBuff)
    {
        // Using timeout 1ms because 0 in lwIP means block forever
        uint timeout = (m_optFlags & SOCK_F_NONBLOCK) ? 1 : m_optTimeout * 1000;
        netconn_set_recvtimeout(m_conn, timeout);
        err_t result = netconn_recv(m_conn, &m_pNetBuff);
        m_offset = 0;
        
        if (result != ERR_OK)
        {
            if (timeout && result == ERR_TIMEOUT) return 0;
            
            m_pNetBuff = NULL;
            Disconnect();
            
            return NET_ERR;
        }
    }
    
    if (pAddr)
    {
        TAddrIp* pIp = (TAddrIp*)pAddr;
        pIp->family = NET_FAMILY_INET;
        pIp->addr = m_pNetBuff->addr.addr;
        pIp->port = m_pNetBuff->port;
    }
    
    uint packetSize = netbuf_len(m_pNetBuff);
    uint remain = packetSize - m_offset;
    remain = __MIN(remain, size);
    uint writeSize = 0;
    
    if (remain)
    {
        writeSize = netbuf_copy_partial(m_pNetBuff, buff, remain, m_offset);
    }
    
    m_offset += writeSize;
    
    if (!remain || m_offset >= packetSize)
    {
        netbuf_delete(m_pNetBuff);
        m_pNetBuff = NULL;
        m_offset = 0;
    }
    
    return writeSize;
}


/**
  * @brief  Setup connection with specified address
  * @param  addr: Remote address
  * @return 0 or negative error code
  */
int CSockTcp::Connect (const TNetAddr* addr)
{
    if (addr->family != NET_FAMILY_INET) return NET_ERR;
    
    if (!m_conn)
    {
        m_conn = netconn_new(NETCONN_TCP);
        if (!m_conn) return NET_ERR;
        m_conn->pcb.tcp->flags |= TF_NODELAY;
    }
    
    const TAddrIp* pIpAddr = (const TAddrIp*) addr;
    ip_addr_t remAddr = { .addr = pIpAddr->addr };
    err_t result = netconn_connect(m_conn, &remAddr, pIpAddr->port);
    
    if (result != ERR_OK)
    {
        uint32 timeStart = Kernel::GetJiffies();
        do
        {
            if (Disconnect() == NET_OK) break;
        } while ( !IsTimeout(timeStart, m_optTimeout * 1000) );
        
        return NET_ERR;
    }
    
    m_addrRemote = *addr;
        
    return NET_OK;
}


/**
  * @brief  Disconnect from remote address
  * @return 0 or negative error code
  */
int CSockTcp::Disconnect (void)
{
    if (!m_conn) return NET_OK;
    
    do
    {
        err_t result = netconn_close(m_conn);
        if (result != ERR_OK && result != ERR_CONN) break;
        
        result = netconn_delete(m_conn);
        if (result != ERR_OK) break;
        
        memset( &m_addrRemote, 0x00, sizeof(m_addrRemote) );
        m_conn = NULL;
        
        return NET_OK;
    } while (0);
    
    return NET_ERR;
}


/**
  * @brief  Bind socket to specified interface address
  * @param  addr: Interface address
  * @return 0 or negative error code
  */
int CSockTcp::Bind (const TNetAddr* addr)
{
    if (addr->family != NET_FAMILY_INET) return NET_ERR;
    
    if (!m_conn)
    {
        m_conn = netconn_new(NETCONN_TCP);
        if (!m_conn) return NET_ERR;
        m_conn->pcb.tcp->flags |= TF_NODELAY;
    }
    
    const TAddrIp* pIpAddr = (const TAddrIp*) addr;
    ip_addr_t addrStruct = { .addr = pIpAddr->addr };
    err_t result = netconn_bind(m_conn, &addrStruct, pIpAddr->port);
    
    if (result == ERR_OK)
    {
        m_addrLocal = *addr;
        return NET_OK;
    }
    else
    {
        return NET_ERR;
    }
}


/**
  * @brief  Incoming packets handler.
  *         Copying data to socket buffer, packet adding to incoming queue (m_qIn)
  * @param  skb: Socket packet descriptor
  */
void CSockTcp::onReceivePacket (const TSkb* skb)
{
    assert(1);
}


/**
  * @brief  Checking for addresses matching for incoming packet
  * @param  addrRemote: Remote address
  * @param  addrLocal: Local address
  * @return true - addresses matched
  */
bool CSockTcp::checkAddrIn (const TNetAddr* addrRemote, const TNetAddr* addrLocal)
{
    if (m_mode == E_SOCK_MODE_SERVER)
    {
        return (
                addrLocal &&
                addrLocal->family == NET_FAMILY_INET &&
                ((const TAddrIp*)addrLocal)->port == ((const TAddrIp*)&m_addrLocal)->port
        );
    }
    else if (m_mode == E_SOCK_MODE_CLIENT)
    {
        return (
                addrRemote &&
                addrRemote->family == NET_FAMILY_INET &&
                ((const TAddrIp*)addrRemote)->addr == ((const TAddrIp*)&m_addrRemote)->addr &&
                ((const TAddrIp*)addrRemote)->port == ((const TAddrIp*)&m_addrRemote)->port
        );
    }
    
    return false;
}


/**
  * @brief  Checking for addresses matching for outgoing packet
  * @param  addrRemote: Remote address
  * @param  pNetDev: Network interface
  * @return true - addresses matched
  */
bool CSockTcp::checkAddrOut (const TNetAddr* addrRemote, CDevNet* pNetDev)
{
    TNetAddr devAddr = {};
    pNetDev->GetAddr(&devAddr);
    
    if (devAddr.family != NET_FAMILY_INET) return false;
    
    // Searching for first matching device if m_addrLocal == ANY
    // or m_addrLocal == devAddr 
    if ( ((const TAddrIp*)&m_addrLocal)->addr == INADDR_ANY ) return true;
    if ( ((TAddrIp*)&devAddr)->addr == ((const TAddrIp*)&m_addrLocal)->addr ) return true;
    
    return false;
}
