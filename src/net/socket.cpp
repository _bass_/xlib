//*****************************************************************************
//
// @brief   Socket interface
// @author  Vadim Mezhlumov
//
//*****************************************************************************

#include "net/socket.h"
#include "dev/net.h"
#include "kernel/kernel.h"
#include "memory/heap.h"
#include "misc/macros.h"
#include "misc/math.h"
#include <string.h>


// ----------------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------------

    // Default read/write timeout (seconds)
    #define SOCK_DEFAULT_OPT_TIMEOUT            30

    // Bounds registered sockets section
    #define SOCK_DESC_ADDR_START                CC_SECTION_ADDR_START(xLib_sockDesc)
    #define SOCK_DESC_ADDR_END                  CC_SECTION_ADDR_END(xLib_sockDesc)


// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------

    // Created sockets list
    static CSocket* g_sockList = NULL;


/**
  * @brief  Constructor
  * @param  family: Network (socket) protocol family
  * @param  type: Socket type
  * @param  packetCount: Number of buffered packets
  */
CSocket::CSocket (uint family, TSockType type, uint packetCount) :
    m_netFamily(family),
    m_type(type),
    m_qIn( packetCount, sizeof(TSkb) ),
    m_mode(E_SOCK_MODE_UNKNOWN),
    m_optFlags(0),
    m_optTimeout(SOCK_DEFAULT_OPT_TIMEOUT),
    m_nextSock(NULL)
{
    // Add to socket list
    if (!g_sockList)
    {
        g_sockList = this;
        return;
    }

    CSocket* ptr = g_sockList;
    while (ptr->m_nextSock)
    {
        ptr = ptr->m_nextSock;
    }

    ptr->m_nextSock = this;
}


/**
  * @brief  Destructor
  * @param  None
  */
CSocket::~CSocket ()
{
    // Remove from socket list
    if (g_sockList == this)
    {
        g_sockList = m_nextSock;
        return;
    }

    CSocket* prev = g_sockList;
    while (prev->m_nextSock != this)
    {
        prev = prev->m_nextSock;
    }

    prev->m_nextSock = m_nextSock;
}


/**
  * @brief  Set socket option
  * @param  code: Option code
  * @param  value: Pointer to option value
  * @return 0 or negative error code
  */
int CSocket::SetOption (uint code, const void* value)
{
    if (!value) return -2;

    switch (code)
    {
        case SOCK_OPT_FLAGS:
            m_optFlags = *( (uint*)value );
        break;

        case SOCK_OPT_TIMEOUT:
            m_optTimeout = *( (uint*)value );
        break;

        default: return -1;
    }

    return 0;
}


/**
  * @brief  Get socket option value
  * @param  code: Option code
  * @param  value: Pointer to value buffer
  * @return 0 or negative error code
  */
int CSocket::GetOption (uint code, void* value) const
{
    if (!value) return -2;

    switch (code)
    {
        case SOCK_OPT_FLAGS:
            *( (uint*)value ) = m_optFlags;
        break;

        case SOCK_OPT_TIMEOUT:
            *( (uint*)value ) = m_optTimeout;
        break;

        default: return -1;
    }

    return 0;
}


/**
  * @brief  Read data from the socket
  * @param  buff: Pointer to buffer for received data
  * @param  size: Buffer size
  * @return Amount of written into the buffer data or negative error code
  */
int CSocket::Read (void* buff, uint size)
{
    return Recv(NULL, buff, size);
}


/**
  * @brief  Receive data
  * @param  pAddr: Pointer to variable to save sender address
  * @param  buff: Pointer to buffer for received data
  * @param  size: Buffer size
  * @return Amount of written into the buffer data or negative error code
  */
int CSocket::Recv (TNetAddr* pAddr, void* buff, uint size)
{
    if (m_optFlags & SOCK_F_NONBLOCK)
    {
        if ( !m_qIn.Count() ) return 0;
    }

    // Waiting for new incoming packets
    TSkb skb;
    int result = m_qIn.Peek(&skb, m_optTimeout * 1000);
    if (result < 0) return result;

    Kernel::EnterCritical();
    do
    {
        // Remove processed packet from the queue
        result = m_qIn.Get(&skb, m_optTimeout * 1000);

        if (result || !skb.data)
        {
            result = -1;
            break;
        }

        uint sizeToCopy = __MIN(skb.dataSize, size);
        if (sizeToCopy)
        {
            memcpy(buff, skb.data, sizeToCopy);
        }

        // If buffer size is less than packet data, the rest of the data is returned to the queue
        if (skb.dataSize > size)
        {
            skb.dataSize -= size;
            skb.data = (char*) skb.data + size;
            m_qIn.Add(&skb, true);
        }

        if (pAddr)
        {
            *pAddr = skb.addr;
        }

        result = sizeToCopy;

    } while (0);
    Kernel::ExitCritical();

    return result;
}


/**
  * @brief  Write data into the socket
  * @param  data: Pointer to data
  * @param  size: Data size
  * @return Amount of sent data or negative error code
  */
int CSocket::Write (const void* data, uint size)
{
    return Send(&m_addrRemote, data, size);
}


/**
  * @brief  Create socket object
  * @param  family: Network (socket) protocol family
  * @param  type: Socket type
  * @return Socket object or NULL on error
  */
CSocket* CSocket::Create (uint family, TSockType type)
{
    TSockDesc* pDesc = (TSockDesc*) SOCK_DESC_ADDR_START;

    for (; (void*)pDesc < SOCK_DESC_ADDR_END; ++pDesc)
    {
        if (pDesc->family == family && pDesc->type == type && pDesc->create)
        {
            return pDesc->create(family, type);
        }
    }

    return NULL;
}


/**
  * @brief  Get pointer to parser function
  * @param  family: Network (socket) protocol family
  * @return Pointer to parser or NULL on error
  */
TNetParser* CSocket::GetParser (uint family)
{
    TSockDesc* pDesc = (TSockDesc*) SOCK_DESC_ADDR_START;

    for (; (void*)pDesc < SOCK_DESC_ADDR_END; ++pDesc)
    {
        if (pDesc->family == family && pDesc->parser)
        {
            return pDesc->parser;
        }
    }

    return NULL;
}


/**
  * @brief  Find socket for receiving the packet
  * @param  skb: Packet descriptor
  * @return Socket object or NULL on error
  */
CSocket* CSocket::Route (TSkb* skb)
{
    if (!skb || !skb->pDev) return NULL;

    TNetAddr devAddr;
    skb->pDev->GetAddr(&devAddr);
    if (devAddr.family == NET_FAMILY_UNKNOWN) return NULL;

    for (CSocket* socket = g_sockList; socket; socket = socket->m_nextSock)
    {
        if (socket->m_netFamily != devAddr.family) continue;
        if ( socket->checkAddrIn(&skb->addr, &devAddr) ) return socket;
    }

    return NULL;
}


/**
  * @brief  Network interface searching for packet sending
  * @param  addrRemote: Remote address
  * @return Pointer to network device object or NULL on error
  */
CDevNet* CSocket::route (const TNetAddr* addrRemote)
{
    const TDevDesc* pEnd;
    const TDevDesc* ptr = GetDevList(&pEnd);

    for (; ptr < pEnd; ++ptr)
    {
        char name[4];
        *((uint32*)name) = ptr->id;
        name[3] = '\0';

        if ( strcmp(name, DEV_NAME_NET) ) continue;

        CDevNet* pNetDev = (CDevNet*) ptr->pDev;
        TNetAddr devAddr;
        pNetDev->GetAddr(&devAddr);
        if (devAddr.family != m_netFamily) continue;

        if ( checkAddrOut(addrRemote, pNetDev) ) return pNetDev;
    }

    return NULL;
}


/**
  * @brief  Polling handler
  * @param  None
  */
void CSocket::Poll (void)
{
}


/**
  * @brief  Setup connection with specified address
  * @param  addr: Remote address
  * @return 0 or negative error code
  */
int CSocket::Connect (const TNetAddr* addr)
{
    UNUSED_VAR(addr);
    return NET_ERR;
}


/**
  * @brief  Disconnect from remote address
  * @return 0 or negative error code
  */
int CSocket::Disconnect (void)
{
    return NET_ERR;
}


/**
  * @brief  Bind socket to specified interface address
  * @param  addr: Interface address
  * @return 0 or negative error code
  */
int CSocket::Bind (const TNetAddr* addr)
{
    if (addr->family != m_netFamily) return NET_ERR;

    m_addrLocal = *addr;

    return NET_OK;
}


/**
  * @brief  Setup socket for receiving incomming connections
  * @return 0 or negative error code
  */
int CSocket::Listen (void)
{
    m_mode = E_SOCK_MODE_SERVER;

    return NET_OK;
}


/**
  * @brief  Accept incoming connection
  * @param  pAddr: Pointer to variable to save client address
  * @return 0 or negative error code
  */
int CSocket::Accept (TNetAddr* pAddr)
{
    UNUSED_VAR(pAddr);
    return NET_ERR;
}


/**
  * @brief  Read address of remote client
  * @param  pAddr: Pointer to variable to save client address
  */
void CSocket::GetAddr (TNetAddr* pAddr) const
{
    *pAddr = m_addrRemote;
}
